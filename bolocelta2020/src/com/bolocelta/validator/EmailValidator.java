package com.bolocelta.validator;

import java.util.regex.Pattern;

import javax.faces.validator.ValidatorException;

public class EmailValidator {
	 
    private Pattern pattern;
  
    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                                                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
  
    public EmailValidator() {
        pattern = Pattern.compile(EMAIL_PATTERN);
    }
 
    public boolean validate(Object value) throws ValidatorException {
    	if(value.toString() == null){
    		return true;
    	}
        if(!pattern.matcher(value.toString()).matches()) {
            return false;
        }
        return true;
    }
     
}
