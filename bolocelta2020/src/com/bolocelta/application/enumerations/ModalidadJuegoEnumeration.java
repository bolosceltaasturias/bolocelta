package com.bolocelta.application.enumerations;

public class ModalidadJuegoEnumeration {
	
	public static final Integer MODALIDAD_INDIVIDUAL = 2;
	public static final Integer MODALIDAD_PAREJAS = 3;
	public static final Integer MODALIDAD_EQUIPOS = 1;

}
