package com.bolocelta.application.messages;

public class Message {
	
	private String severity;
	private String message;
	
	public Message() {
		// TODO Auto-generated constructor stub
	}
	
	public Message(String severity, String message){
		this.severity = severity;
		this.message = message;
	}

	public String getSeverity() {
		return severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
