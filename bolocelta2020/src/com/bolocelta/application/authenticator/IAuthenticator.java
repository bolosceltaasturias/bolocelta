package com.bolocelta.application.authenticator;

import java.io.Serializable;

public interface IAuthenticator extends Serializable {

	abstract void authenticate(String username, String password);

	public void unAuthenticate(String username);

}
