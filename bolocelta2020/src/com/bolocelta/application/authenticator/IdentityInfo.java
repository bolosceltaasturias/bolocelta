package com.bolocelta.application.authenticator;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;

import com.bolocelta.entities.usuarios.Usuarios;

public class IdentityInfo implements Serializable {

	private String username;
	private Date expirationDate;
	private Usuarios usuario;
	private HashMap<String, Boolean> permisosMap = new HashMap<>();
	

	public IdentityInfo() {
	}

	public IdentityInfo(String username, Date expirationDate, Usuarios usuario) {
		this.username = username;
		this.expirationDate = expirationDate;
		this.usuario = usuario;
	}

	private Date computeExpirationDate(Long expirationTime) {
		if (expirationTime > 0) {
			return new Date(expirationTime);
		}
		return null;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}
	

	public Usuarios getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuarios usuario) {
		this.usuario = usuario;
	}
	
	public HashMap<String, Boolean> getPermisosMap() {
		return permisosMap;
	}

	public void setPermisosMap(HashMap<String, Boolean> permisosMap) {
		this.permisosMap = permisosMap;
	}
	
	public boolean hasPermission(String object) {
		if(this.permisosMap.get(object) == null){
			return false;
		}
		return this.permisosMap.get(object) ;
	}

	public boolean isExpired() {
		if (expirationDate != null) {
			return expirationDate.before(new Date());
		}
		return false;
	}
	
	public Long getExpirationSeconds() {
		if (expirationDate != null) {
			return (expirationDate.getTime() - new Date().getTime()) / 1000;
		}
		return null;
	}
	
	
}