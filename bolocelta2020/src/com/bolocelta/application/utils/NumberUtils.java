package com.bolocelta.application.utils;

public class NumberUtils {

	private NumberUtils() {
	}

	// Check if given string is a number (digits only)
	public static boolean isNumber(String string) {
		return string.matches("^\\d+$");
	}

	// Check if given string is numeric (-+0..9(.)0...9)
	public static boolean isNumeric(String string) {
		return string.matches("^[-+]?\\d+(\\.\\d+)?$");
	}

	// Check if given string is number with dot separator and two decimals.
	public static boolean isNumberWith2Decimals(String string) {
		return string.matches("^\\d+\\.\\d{2}$");
	}

}