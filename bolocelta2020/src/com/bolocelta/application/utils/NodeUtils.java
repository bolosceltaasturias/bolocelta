package com.bolocelta.application.utils;

public class NodeUtils {

	private String jugador;
	private String equipo;

	public NodeUtils(String jugador, String equipo) {
		this.jugador = jugador;
		this.equipo = equipo;
	}

	public String getJugador() {
		return jugador;
	}

	public void setJugador(String jugador) {
		this.jugador = jugador;
	}

	public String getEquipo() {
		return equipo;
	}

	public void setEquipo(String equipo) {
		this.equipo = equipo;
	}

}
