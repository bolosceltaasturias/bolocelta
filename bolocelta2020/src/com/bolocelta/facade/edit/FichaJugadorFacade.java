package com.bolocelta.facade.edit;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.bolocelta.application.Redirect;
import com.bolocelta.application.session.SessionState;
import com.bolocelta.bbdd.constants.Activo;
import com.bolocelta.bbdd.createTable.CrearJugadores;
import com.bolocelta.bbdd.readTables.LeerJugadores;
import com.bolocelta.entities.Jugadores;

@Named
@ManagedBean
@SessionScoped
public class FichaJugadorFacade implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Redirect redirect;

	@Inject
	private SessionState sessionState;
	
	@Inject
	private FichaEquipoFacade fichaEquipoFacade;

	private LeerJugadores leerJugadores = new LeerJugadores();
	private CrearJugadores crearJugadores = new CrearJugadores();

	private Jugadores jugador = null;
	
	public boolean isNew(){
		if(this.jugador == null || (this.jugador != null && this.jugador.getRowNum() == null)){
			return true;
		}
		return false;
	}

	public void doSelected(Integer id) {
		this.jugador = (Jugadores) leerJugadores.read(id);
		redirect.getJugadorDetalle();
	}
	
	public void doCancel() {
		this.jugador = null;
		redirect.getMiEquipo();
	}
	
	public void doInsert() {
		if(crearJugadores.validate(this.jugador)){
			Long newRowNum = leerJugadores.getLastRowSheet();
			this.jugador.setRowNum(newRowNum+1);
			crearJugadores.preparateInsertRow(this.jugador);
			leerJugadores.refreshList();
			fichaEquipoFacade.doSelectedMyTeam();
		}
	}
	
	public void doSave() {
		if(crearJugadores.validate(this.jugador)){
			crearJugadores.preparateUpdateRow(this.jugador);
			fichaEquipoFacade.doSelectedMyTeam();
		}
	}
	
	public void doDelete() {
		this.jugador.setActivo(Activo.NO);
		crearJugadores.preparateDeleteLogicRow(this.jugador);
		fichaEquipoFacade.doSelectedMyTeam();
	}
	
	public void doCreate(){
		this.jugador = new Jugadores(fichaEquipoFacade.getEquipo().getId(), Activo.SI, Activo.NO, Activo.NO, Activo.NO, Activo.NO);
		redirect.getJugadorDetalle();
	}

	public Jugadores getJugador() {
		return jugador;
	}

	public void setJugador(Jugadores jugador) {
		this.jugador = jugador;
	}
	
}
