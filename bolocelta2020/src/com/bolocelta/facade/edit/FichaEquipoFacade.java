package com.bolocelta.facade.edit;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.bolocelta.application.Redirect;
import com.bolocelta.application.enumerations.CategoriasEnumeration;
import com.bolocelta.application.session.SessionState;
import com.bolocelta.bbdd.constants.Activo;
import com.bolocelta.bbdd.constants.Modalidad;
import com.bolocelta.bbdd.constants.structure.EstructuraEquipos;
import com.bolocelta.bbdd.createTable.CrearEquipos;
import com.bolocelta.bbdd.createTable.CrearJugadores;
import com.bolocelta.bbdd.readTables.LeerCampeonatoMasculinoParejasPrimera;
import com.bolocelta.bbdd.readTables.LeerCampeonatoMasculinoParejasSegunda;
import com.bolocelta.bbdd.readTables.LeerCampeonatoMasculinoParejasTercera;
import com.bolocelta.bbdd.readTables.LeerCategorias;
import com.bolocelta.bbdd.readTables.LeerEquipos;
import com.bolocelta.bbdd.readTables.LeerJugadores;
import com.bolocelta.entities.Categorias;
import com.bolocelta.entities.Equipos;
import com.bolocelta.entities.Jugadores;
import com.bolocelta.entities.Parejas;
import com.bolocelta.entities.ParticipantesParejas;
import com.bolocelta.facade.add.CampeonatoFemeninoIndividualAddFacade;
import com.bolocelta.facade.add.CampeonatoMasculinoIndividualPrimeraAddFacade;
import com.bolocelta.facade.add.CampeonatoMasculinoIndividualSegundaAddFacade;
import com.bolocelta.facade.add.CampeonatoMasculinoIndividualTerceraAddFacade;
import com.bolocelta.facade.add.CampeonatoMasculinoParejasPrimeraAddFacade;
import com.bolocelta.facade.add.CampeonatoMasculinoParejasSegundaAddFacade;
import com.bolocelta.facade.add.CampeonatoMasculinoParejasTerceraAddFacade;

@Named
@ManagedBean
@SessionScoped
public class FichaEquipoFacade implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Redirect redirect;
	
	@Inject
	private SessionState sessionState;
	//Individual
	@Inject
	private CampeonatoMasculinoIndividualPrimeraAddFacade campeonatoMasculinoIndividualPrimeraAddFacade;
	@Inject
	private CampeonatoMasculinoIndividualSegundaAddFacade campeonatoMasculinoIndividualSegundaAddFacade;
	@Inject
	private CampeonatoMasculinoIndividualTerceraAddFacade campeonatoMasculinoIndividualTerceraAddFacade;
	@Inject
	private CampeonatoFemeninoIndividualAddFacade campeonatoFemeninoIndividualAddFacade;
	//Parejas
	@Inject
	private CampeonatoMasculinoParejasPrimeraAddFacade campeonatoMasculinoParejasPrimeraAddFacade;
	@Inject
	private CampeonatoMasculinoParejasSegundaAddFacade campeonatoMasculinoParejasSegundaAddFacade;
	@Inject
	private CampeonatoMasculinoParejasTerceraAddFacade campeonatoMasculinoParejasTerceraAddFacade;
//	@Inject
//	private CampeonatoFemeninoParejasAddFacade campeonatoFemeninoParejasAddFacade;

	private LeerEquipos leerEquipos = new LeerEquipos();
	private LeerCategorias leerCategorias = new LeerCategorias();
	
	private LeerJugadores leerJugadores = new LeerJugadores();
	
	private CrearJugadores crearJugadores = new CrearJugadores();
	private CrearEquipos crearEquipos = new CrearEquipos();
	
	private LeerCampeonatoMasculinoParejasPrimera leerCampeonatoMasculinoParejasPrimera = new LeerCampeonatoMasculinoParejasPrimera();
	private LeerCampeonatoMasculinoParejasSegunda leerCampeonatoMasculinoParejasSegunda = new LeerCampeonatoMasculinoParejasSegunda();
	private LeerCampeonatoMasculinoParejasTercera leerCampeonatoMasculinoParejasTercera = new LeerCampeonatoMasculinoParejasTercera();
	
	private Equipos equipo = null;

	public Equipos getEquipo() {
		return equipo;
	}

	public void setEquipo(Equipos equipo) {
		this.equipo = equipo;
	}
	
	public Equipos getMiEquipo() {
		setEquipo((Equipos) sessionState.getUserEquipo());
		return equipo;
	}
	
	public void doSelectedMyTeam(){
		setEquipo((Equipos) sessionState.getUserEquipo());
		redirect.getMiEquipo();
	}
	
	public void doSelected(Integer id){
		this.equipo = (Equipos) leerEquipos.read(id, true);
		redirect.getEquipoDetalle();
	}
	
	public void doSelectedParejasJugador1(Integer id){
		this.jugador1 = leerJugadores.read(id);
		this.jugador1.setEquipo(leerEquipos.read(jugador1.getEquipoId()));
		redirect.getAddInscripcionParejas();
	}
	
	public void doSelectedParejasJugador2Equipo(Integer id){
		this.jugador2 = leerJugadores.read(id);
		this.jugador2.setEquipo(leerEquipos.read(jugador2.getEquipoId()));
		redirect.getAddInscripcionParejas();
	}
	
	public void doSelectedParejasJugador2OtrosEquipos(Integer id){
		this.jugador2 = leerJugadores.read(id);
		this.jugador2.setEquipo(leerEquipos.read(jugador2.getEquipoId()));
		redirect.getAddInscripcionParejas();
	}
	
	public void doSelectedCategoria(Integer id){
		this.categoria = leerCategorias.read(id);
		redirect.getAddInscripcionParejas();
	}
	
	public void doInscripcionIndividual(){
		redirect.getInscripcionIndividual();
	}
	
	public void doInscripcionParejas(){
		redirect.getInscripcionParejas();
	}
	
	public void addInscripcionParejas(){
		this.jugador1 = new Jugadores();
		this.jugador2 = new Jugadores();
		this.categoria = new Categorias();
		redirect.getAddInscripcionParejas();
	}
	
	public void doInsertParejaMasculina() {
		doInscribirPareja(this.jugador1, this.jugador2, this.categoria);
	}
	
	public void addCategoria(){
		redirect.getAddCategoria();
	}
	
	public void addJugador1Equipo(){
		redirect.getAddJugador1Equipo();
	}
	
	public void addJugador2Equipo(){
		redirect.getAddJugador2Equipo();
	}
	
	public void addJugador2OtroEquipo(){
		redirect.getAddJugador2OtroEquipo();
	}
	
	public Integer getMaxRowsPaginator(){
		return 8;
	}
	
	public boolean isEnabledPaginator(){
		return (getEquipo() != null && getEquipo().getJugadoresList() != null) ? (getEquipo().getJugadoresList().size() > getMaxRowsPaginator()) : false;
	}
	
	public void doCancel() {
		redirect.getMiEquipo();
	}
	
	public void doCancelFichaPareja() {
		redirect.getAddInscripcionParejas();
	}
	
	//Actualizar Email
	public void doActualizarEmail(){
		if(crearEquipos.validateEmail(getEquipo())){
			doActualizarEmail(getEquipo());
		}
	}
	
	private void doActualizarEmail(Equipos equipo) {
		if(equipo != null){
//			System.out.println("Actualizando email");
			//Actualizar email equipo
			crearEquipos.preparateUpdateEmail(equipo);
		}
	}
	
	//Inscripcion de jugador en campeonato individual
	
	public void doInscribirPrimera(Jugadores jugador){
		doInscribirJugador(jugador, CategoriasEnumeration.CATEGORIA_PRIMERA, Activo.SI);
	}
	
	public void doInscribirSegunda(Jugadores jugador){
		doInscribirJugador(jugador, CategoriasEnumeration.CATEGORIA_SEGUNDA, Activo.SI);
	}
	
	public void doInscribirTercera(Jugadores jugador){
		doInscribirJugador(jugador, CategoriasEnumeration.CATEGORIA_TERCERA, Activo.SI);
	}
	
	public void doInscribirFemenino(Jugadores jugador){
		doInscribirJugador(jugador, CategoriasEnumeration.CATEGORIA_FEMENINO, Activo.SI);
	}
	
	public void doDesinscribirPrimera(Jugadores jugador){
		doInscribirJugador(jugador, CategoriasEnumeration.CATEGORIA_PRIMERA, Activo.NO);
	}
	
	public void doDesinscribirSegunda(Jugadores jugador){
		doInscribirJugador(jugador, CategoriasEnumeration.CATEGORIA_SEGUNDA, Activo.NO);
	}
	
	public void doDesinscribirTercera(Jugadores jugador){
		doInscribirJugador(jugador, CategoriasEnumeration.CATEGORIA_TERCERA, Activo.NO);
	}
	
	public void doDesinscribirFemenino(Jugadores jugador){
		doInscribirJugador(jugador, CategoriasEnumeration.CATEGORIA_FEMENINO, Activo.NO);
	}
	
	public void doInscribirJugador(Jugadores jugador, Integer categoria, String activo) {
		if(jugador != null && categoria != null){
			
			if(categoria == 1){
//				System.out.println("Inscripcion en la categoria 1");
				
				//Actualizar ficha jugador
				jugador.setIndividualPrimera(activo);
				crearJugadores.preparateUpdateRow(jugador);
				//Realizar inscripcion en campeonato
				campeonatoMasculinoIndividualPrimeraAddFacade.doInscribirJugador(
						jugador, leerCategorias.read(categoria), 
						sessionState.getUser(), activo);
				
			}else if(categoria == 2){
//				System.out.println("Inscripcion en la categoria 2");
				
				//Actualizar ficha jugador
				jugador.setIndividualSegunda(activo);
				crearJugadores.preparateUpdateRow(jugador);
				//Realizar inscripcion en campeonato
				campeonatoMasculinoIndividualSegundaAddFacade.doInscribirJugador(
						jugador, leerCategorias.read(categoria), 
						sessionState.getUser(), activo);
				
			}else if(categoria == 3){
//				System.out.println("Inscripcion en la categoria 3");
				
				//Actualizar ficha jugador
				jugador.setIndividualTercera(activo);
				crearJugadores.preparateUpdateRow(jugador);
				//Realizar inscripcion en campeonato
				campeonatoMasculinoIndividualTerceraAddFacade.doInscribirJugador(
						jugador, leerCategorias.read(categoria), 
						sessionState.getUser(), activo);
				
			}else if(categoria == 4){
//				System.out.println("Inscripcion en la categoria 4");
				
				//Actualizar ficha jugador
				jugador.setIndividualFemenino(activo);
				crearJugadores.preparateUpdateRow(jugador);
				//Realizar inscripcion en campeonato
				campeonatoFemeninoIndividualAddFacade.doInscribirJugador(
						jugador, leerCategorias.read(categoria), 
						sessionState.getUser(), activo);
				
			}
			
			//redirect.getMiEquipo();
		}
		
	}
	
	
	// Filtrar los botones de inscripcion para el jugador
	
	public boolean isInscribibleMasculino(Jugadores jugador, Integer categoria){
		boolean inscribible = false;
		
		if(isMasculino(jugador) && isJugadorActivo(jugador)){
			if(categoria == 1 && !isInscritoIndividualPrimera(jugador) && !isInscritoIndividualOtraCategoria(jugador) && 
					(getEquipo().getCategoriaId() == 1 || getEquipo().getCategoriaId() == 2 || getEquipo().getCategoriaId() == 3)){
				inscribible = true;
			}else if(categoria == 2 && !isInscritoIndividualSegunda(jugador) && !isInscritoIndividualOtraCategoria(jugador) && 
						(getEquipo().getCategoriaId() == 2 || getEquipo().getCategoriaId() == 3)){
				inscribible = true;
			}else if(categoria == 3 && !isInscritoIndividualTercera(jugador) && !isInscritoIndividualOtraCategoria(jugador) && 
					(getEquipo().getCategoriaId() == 3)){
				inscribible = true;
			}
		}
		return inscribible;
	}
	
	public boolean isDesinscribibleMasculino(Jugadores jugador, Integer categoria){
		boolean inscribible = false;
		
		if(isMasculino(jugador) && isJugadorActivo(jugador)){
			if(categoria == 1 && isInscritoIndividualPrimera(jugador) && isInscritoIndividualOtraCategoria(jugador)){
				inscribible = true;
			}else if(categoria == 2 && isInscritoIndividualSegunda(jugador) && isInscritoIndividualOtraCategoria(jugador)){
				inscribible = true;
			}else if(categoria == 3 && isInscritoIndividualTercera(jugador) && isInscritoIndividualOtraCategoria(jugador)){
				inscribible = true;
			}
		}
		return inscribible;
	}
	
	public boolean isInscribibleFemenino(Jugadores jugador, Integer categoria){
		boolean inscribible = false;
		
		if(isFemenino(jugador) && isJugadorActivo(jugador)){
			if(categoria == 4 && !isInscritoIndividualFemenino(jugador)){
				inscribible = true;
			}
		}
		return inscribible;
	}
	
	public boolean isDesinscribibleFemenino(Jugadores jugador, Integer categoria){
		boolean inscribible = false;
		
		if(isFemenino(jugador) && isJugadorActivo(jugador)){
			if(categoria == 4 && isInscritoIndividualFemenino(jugador)){
				inscribible = true;
			}
		}
		return inscribible;
	}
	
	public boolean isMasculino(Jugadores jugador){
		if(jugador.getModalidad().equalsIgnoreCase(Modalidad.MASCULINO)){
			return true;
		}
		return false;
	}
	
	public boolean isFemenino(Jugadores jugador){
		if(jugador.getModalidad().equalsIgnoreCase(Modalidad.FEMENINO)){
			return true;
		}
		return false;
	}
	
	public boolean isInscritoIndividualPrimera(Jugadores jugador){
		if( jugador.getIndividualPrimera().equalsIgnoreCase(Activo.SI)){
			return true;
		}
		return false;
	}
	
	public boolean isInscritoIndividualSegunda(Jugadores jugador){
		if( jugador.getIndividualSegunda().equalsIgnoreCase(Activo.SI) ){
			return true;
		}
		return false;
	}
	
	public boolean isInscritoIndividualTercera(Jugadores jugador){
		if( jugador.getIndividualTercera().equalsIgnoreCase(Activo.SI) ){
			return true;
		}
		return false;
	}
	
	public boolean isInscritoIndividualFemenino(Jugadores jugador){
		if( jugador.getIndividualFemenino().equalsIgnoreCase(Activo.SI) ){
			return true;
		}
		return false;
	}
	
	public boolean isInscritoIndividualOtraCategoria(Jugadores jugador){
		if( jugador.getIndividualPrimera().equalsIgnoreCase(Activo.SI) || jugador.getIndividualSegunda().equalsIgnoreCase(Activo.SI) ||
			jugador.getIndividualTercera().equalsIgnoreCase(Activo.SI) || jugador.getIndividualFemenino().equalsIgnoreCase(Activo.SI) ){
			return true;
		}
		return false;
	}
	
	public boolean isJugadorActivo(Jugadores jugador){
		if( jugador.getActivo().equalsIgnoreCase(Activo.SI)){
			return true;
		}
		return false;
	}
	
	//Horarios preferentes
	
	public boolean isHorarioPreferenteSabado1100Si(){
		if(getEquipo().getHorarioPreferenteSabadoMañana().equalsIgnoreCase(Activo.SI)){
			return true;
		}
		return false;
	}
	
	public boolean isHorarioPreferenteSabado1100No(){
		if(getEquipo().getHorarioPreferenteSabadoMañana().equalsIgnoreCase(Activo.NO)){
			return true;
		}
		return false;
	}
	
	public void doHorarioPreferenteSabado1100(){
		if(getEquipo().getHorarioPreferenteSabadoMañana().equalsIgnoreCase(Activo.SI)){
			getEquipo().setHorarioPreferenteSabadoMañana(Activo.NO);
		}else if(getEquipo().getHorarioPreferenteSabadoMañana().equalsIgnoreCase(Activo.NO)){
			getEquipo().setHorarioPreferenteSabadoMañana(Activo.SI);
		}
		doActualizarHorarioPreferente(getEquipo(), EstructuraEquipos.COL_HORARIO_PSM);
	}
	
	public boolean isHorarioPreferenteSabado1600Si(){
		if(getEquipo().getHorarioPreferenteSabadoTarde().equalsIgnoreCase(Activo.SI)){
			return true;
		}
		return false;
	}
	
	public boolean isHorarioPreferenteSabado1600No(){
		if(getEquipo().getHorarioPreferenteSabadoTarde().equalsIgnoreCase(Activo.NO)){
			return true;
		}
		return false;
	}
	
	public void doHorarioPreferenteSabado1600(){
		if(getEquipo().getHorarioPreferenteSabadoTarde().equalsIgnoreCase(Activo.SI)){
			getEquipo().setHorarioPreferenteSabadoTarde(Activo.NO);
		}else if(getEquipo().getHorarioPreferenteSabadoTarde().equalsIgnoreCase(Activo.NO)){
			getEquipo().setHorarioPreferenteSabadoTarde(Activo.SI);
		}
		doActualizarHorarioPreferente(getEquipo(), EstructuraEquipos.COL_HORARIO_PST);
	}
	
	public boolean isHorarioPreferenteDomingo1100Si(){
		if(getEquipo().getHorarioPreferenteDomingoMañana().equalsIgnoreCase(Activo.SI)){
			return true;
		}
		return false;
	}
	
	public boolean isHorarioPreferenteDomingo1100No(){
		if(getEquipo().getHorarioPreferenteDomingoMañana().equalsIgnoreCase(Activo.NO)){
			return true;
		}
		return false;
	}
	
	public void doHorarioPreferenteDomingo1100(){
		if(getEquipo().getHorarioPreferenteDomingoMañana().equalsIgnoreCase(Activo.SI)){
			getEquipo().setHorarioPreferenteDomingoMañana(Activo.NO);
		}else if(getEquipo().getHorarioPreferenteDomingoMañana().equalsIgnoreCase(Activo.NO)){
			getEquipo().setHorarioPreferenteDomingoMañana(Activo.SI);
		}
		doActualizarHorarioPreferente(getEquipo(), EstructuraEquipos.COL_HORARIO_PDM);
	}
	
	public boolean isHorarioPreferenteDomingo1600Si(){
		if(getEquipo().getHorarioPreferenteDomingoTarde().equalsIgnoreCase(Activo.SI)){
			return true;
		}
		return false;
	}
	
	public boolean isHorarioPreferenteDomingo1600No(){
		if(getEquipo().getHorarioPreferenteDomingoTarde().equalsIgnoreCase(Activo.NO)){
			return true;
		}
		return false;
	}
	
	public void doHorarioPreferenteDomingo1600(){
		if(getEquipo().getHorarioPreferenteDomingoTarde().equalsIgnoreCase(Activo.SI)){
			getEquipo().setHorarioPreferenteDomingoTarde(Activo.NO);
		}else if(getEquipo().getHorarioPreferenteDomingoTarde().equalsIgnoreCase(Activo.NO)){
			getEquipo().setHorarioPreferenteDomingoTarde(Activo.SI);
		}
		doActualizarHorarioPreferente(getEquipo(), EstructuraEquipos.COL_HORARIO_PDT);
	}
	
	private void doActualizarHorarioPreferente(Equipos equipo, String colHorarioPreferente) {
		if(equipo != null && colHorarioPreferente != null){
//			System.out.println("Actualizando horario preferente " + colHorarioPreferente);
			//Actualizar email equipo
			crearEquipos.preparateUpdateHorarioPreferente(equipo, colHorarioPreferente);
		}
	}
	

	private List<Jugadores> jugadoresEquipoMasculinoList;
	private List<Jugadores> jugadoresEquipoFemeninoList;
	private List<Jugadores> jugadoresTodosEquiposMasculinoList;
	private List<Jugadores> jugadoresTodosEquiposFemeninoList;
	private List<Categorias> categoriasList;
	private List<Categorias> categoriasMasculinoList;
	private boolean parejaDiferentesEquipos = false;
	
	public boolean isParejaDiferentesEquipos() {
		return parejaDiferentesEquipos;
	}

	public void setParejaDiferentesEquipos(boolean parejaDiferentesEquipos) {
		this.parejaDiferentesEquipos = parejaDiferentesEquipos;
	}

	public List<Jugadores> getJugadoresEquipoMasculinoList(){
		if(jugadoresEquipoMasculinoList == null){
			jugadoresEquipoMasculinoList = new ArrayList<Jugadores>();
			
			for (Jugadores jugador : this.equipo.getJugadoresList()) {
				if(jugador.isMasculino() && jugador.getActivo().equalsIgnoreCase("SI")){
					jugadoresEquipoMasculinoList.add(jugador);
				}
			}
			
			//Ordenar el jugadores por nombre
			Collections.sort(jugadoresEquipoMasculinoList, new Comparator() {
				@Override
				public int compare(Object o1, Object o2) {
					Jugadores j1 = (Jugadores) o1;
					Jugadores j2 = (Jugadores) o2;
					
					int comp1 = j1.getNombreEquipo().compareTo(j2.getNombreEquipo());
					return comp1;
				}
			});
			
		}
		return jugadoresEquipoMasculinoList;
	}
	
	public List<Jugadores> getJugadoresEquipoFemeninoList(){
		if(jugadoresEquipoFemeninoList == null){
			jugadoresEquipoFemeninoList = new ArrayList<Jugadores>();
			
			for (Jugadores jugador : this.equipo.getJugadoresList()) {
				if(jugador.isFemenino() && jugador.getActivo().equalsIgnoreCase("SI")){
					jugadoresEquipoFemeninoList.add(jugador);
				}
			}
			
		}
		
		//Ordenar el jugadores por nombre
		Collections.sort(jugadoresEquipoFemeninoList, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				Jugadores j1 = (Jugadores) o1;
				Jugadores j2 = (Jugadores) o2;
				
				int comp1 = j1.getNombreEquipo().compareTo(j2.getNombreEquipo());
				return comp1;
			}
		});
		
		return jugadoresEquipoFemeninoList;
	}
	
	public List<Jugadores> getJugadoresTodosEquiposMasculinoList(){
		if(jugadoresTodosEquiposMasculinoList == null){
			jugadoresTodosEquiposMasculinoList = new ArrayList<Jugadores>();
			
			for (Jugadores jugador : (List<Jugadores>) leerJugadores.listResult()) {
				if(jugador.isMasculino() && jugador.getActivo().equalsIgnoreCase("SI")){
		    		if(leerEquipos != null){
		    			jugador.setEquipo(leerEquipos.read(jugador.getEquipoId()));
		    		}
					jugadoresTodosEquiposMasculinoList.add(jugador);
				}
			}
			
		}
		
		//Ordenar el jugadores por nombre
		Collections.sort(jugadoresTodosEquiposMasculinoList, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				Jugadores j1 = (Jugadores) o1;
				Jugadores j2 = (Jugadores) o2;
				
				int comp1 = j1.getNombreEquipo().compareTo(j2.getNombreEquipo());
				return comp1;
			}
		});
		
		return jugadoresTodosEquiposMasculinoList;
	}
	
	public List<Jugadores> getJugadoresTodosEquiposFemeninoList(){
		if(jugadoresTodosEquiposFemeninoList == null){
			jugadoresTodosEquiposFemeninoList = new ArrayList<Jugadores>();
			
			for (Jugadores jugador : (List<Jugadores>) leerJugadores.listResult()) {
				if(jugador.isFemenino() && jugador.getActivo().equalsIgnoreCase("SI")){
		    		if(leerEquipos != null){
		    			jugador.setEquipo(leerEquipos.read(jugador.getEquipoId()));
		    		}
					jugadoresTodosEquiposFemeninoList.add(jugador);
				}
			}
			
		}
		
		//Ordenar el jugadores por nombre
		Collections.sort(jugadoresTodosEquiposFemeninoList, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				Jugadores j1 = (Jugadores) o1;
				Jugadores j2 = (Jugadores) o2;
				
				int comp1 = j1.getNombreEquipo().compareTo(j2.getNombreEquipo());
				return comp1;
			}
		});
		
		return jugadoresTodosEquiposFemeninoList;
	}
	
	public List<Categorias> getCategoriasList(){
		if(categoriasList == null){
			categoriasList = new ArrayList<Categorias>();
			
			for (Categorias categoria : (List<Categorias>) leerCategorias.listResultParejas()) {
				categoriasList.add(categoria);
			}
			
		}
		
		return categoriasList;
	}
	
	public List<Categorias> getCategoriasMasculinoList(){
		if(categoriasMasculinoList == null){
			categoriasMasculinoList = new ArrayList<Categorias>();
			
			for (Categorias categoria : (List<Categorias>) leerCategorias.listResultParejas()) {
				if(categoria.getId() == 1 || categoria.getId() == 2 || categoria.getId() == 3){
					categoriasMasculinoList.add(categoria);
				}
			}
			
		}
		
		return categoriasMasculinoList;
	}
	
	private Jugadores jugador1;
	private Jugadores jugador2;
	private Categorias categoria;
	
	public Categorias getCategoria() {
		return categoria;
	}

	public void setCategoria(Categorias categoria) {
		this.categoria = categoria;
	}

	public Jugadores getJugador1() {
		return jugador1;
	}

	public void setJugador1(Jugadores jugador1) {
		this.jugador1 = jugador1;
	}

	public Jugadores getJugador2() {
		return jugador2;
	}

	public void setJugador2(Jugadores jugador2) {
		this.jugador2 = jugador2;
	}
	
	
	private List<ParticipantesParejas> resultListParticipantes = null;
	private List<ParticipantesParejas> resultListParticipantesByEquipo = null;
	
	public void refrescarParticipantesParejas(){
		this.resultListParticipantes = null;
		this.resultListParticipantesByEquipo = null;
		getResultListParticipantesByEquipo(getEquipo().getId());			
	}

	public List<ParticipantesParejas> getResultListParticipantes() {
		if(resultListParticipantes == null){
			resultListParticipantes = new ArrayList<>();
			resultListParticipantes.addAll((List<ParticipantesParejas>) leerCampeonatoMasculinoParejasPrimera.listResultParticipantesOrderByEquipo());
			resultListParticipantes.addAll((List<ParticipantesParejas>) leerCampeonatoMasculinoParejasSegunda.listResultParticipantesOrderByEquipo());
			resultListParticipantes.addAll((List<ParticipantesParejas>) leerCampeonatoMasculinoParejasTercera.listResultParticipantesOrderByEquipo());
		}
		return resultListParticipantes;
	}
	
	public List<ParticipantesParejas> getResultListParticipantesByEquipo(Integer idEquipo) {
		if(resultListParticipantesByEquipo == null){
			resultListParticipantesByEquipo = new ArrayList<>();

			if(resultListParticipantes == null){
				resultListParticipantes = getResultListParticipantes();
			}
			
			for (ParticipantesParejas participantesParejas : resultListParticipantes) {
				if(participantesParejas != null && participantesParejas.getPareja() != null && participantesParejas.getPareja().getId() != null){
					if(participantesParejas.getPareja().getJugador1().getEquipoId().equals(idEquipo) || participantesParejas.getPareja().getJugador2().getEquipoId().equals(idEquipo)){
						resultListParticipantesByEquipo.add(participantesParejas);
					}
				}
			}
		}
		
		return resultListParticipantesByEquipo;
	}
	
	
	//Inscripcion de jugador en campeonato parejas
	
	public void doInscribirPareja(Jugadores jugador1, Jugadores jugador2, Categorias categoria){
		if(jugador1 == null || jugador1.getId() == null){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El jugador 1 de la pareja es requerido.", null));
		} else if(jugador2 == null || jugador2.getId() == null){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El jugador 2 de la pareja es requerido.", null));
		} else if(categoria == null || categoria.getId() == null){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "La Categoria de la pareja es requerida.", null));
		} else if(
				((jugador1.getEquipo().getCategoriaId() == com.bolocelta.bbdd.constants.Categorias.PRIMERA || jugador2.getEquipo().getCategoriaId() == com.bolocelta.bbdd.constants.Categorias.PRIMERA) && (categoria.getId() == com.bolocelta.bbdd.constants.Categorias.SEGUNDA || categoria.getId() == com.bolocelta.bbdd.constants.Categorias.TERCERA))
				|| (jugador1.getEquipo().getCategoriaId() == com.bolocelta.bbdd.constants.Categorias.SEGUNDA || jugador2.getEquipo().getCategoriaId() == com.bolocelta.bbdd.constants.Categorias.SEGUNDA) && (categoria.getId() == com.bolocelta.bbdd.constants.Categorias.TERCERA)){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Uno de los jugadores pertenece a una categoria superior, no puede participar en esta categoria.", null));
		} else {
			
			Parejas parejaNew = new Parejas();
			parejaNew.setIdJugador1(jugador1.getId());
			parejaNew.setIdJugador2(jugador2.getId());
			parejaNew.setJugador1(jugador1);
			parejaNew.setJugador2(jugador2);
			
			ParticipantesParejas participantesParejasPrimera = leerCampeonatoMasculinoParejasPrimera.existeParejaComoParticipante(parejaNew);
			ParticipantesParejas participantesParejasSegunda = leerCampeonatoMasculinoParejasSegunda.existeParejaComoParticipante(parejaNew);
			ParticipantesParejas participantesParejasTercera = leerCampeonatoMasculinoParejasTercera.existeParejaComoParticipante(parejaNew);
			if(participantesParejasPrimera != null || participantesParejasSegunda != null || participantesParejasTercera != null){
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "La pareja ya esta inscrita.", null));
			}else{
				participantesParejasPrimera = leerCampeonatoMasculinoParejasPrimera.existeJugadoresParejaComoParticipante(parejaNew);
				participantesParejasSegunda = leerCampeonatoMasculinoParejasSegunda.existeJugadoresParejaComoParticipante(parejaNew);
				participantesParejasTercera = leerCampeonatoMasculinoParejasTercera.existeJugadoresParejaComoParticipante(parejaNew);

				if(participantesParejasPrimera != null || participantesParejasSegunda != null || participantesParejasTercera != null){
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Algún jugador de la pareja ya esta inscrito con otro jugador.", null));
				}else{
					doInscribirPareja(jugador1, jugador2, categoria, Activo.SI);
				}
			}
		}
	}
	
	public void doInscribirPareja(Jugadores jugador1, Jugadores jugador2, Categorias categoria, String activo) {
		if(jugador1 != null && jugador2 != null && categoria != null){
			
			if(categoria.getId() == 1){
				
				//Actualizar ficha pareja
				//Realizar inscripcion en campeonato
				campeonatoMasculinoParejasPrimeraAddFacade.doInscribirPareja(
						jugador1, jugador2, categoria, sessionState.getUser(), activo);
				
			}else if(categoria.getId() == 2){
				
				//Actualizar ficha pareja
				//Realizar inscripcion en campeonato
				campeonatoMasculinoParejasSegundaAddFacade.doInscribirPareja(
						jugador1, jugador2, categoria, sessionState.getUser(), activo);
				
			}else if(categoria.getId() == 3){

				//Actualizar ficha pareja
				//Realizar inscripcion en campeonato
				campeonatoMasculinoParejasTerceraAddFacade.doInscribirPareja(
						jugador1, jugador2, categoria, sessionState.getUser(), activo);
				
			}else if(categoria.getId() == 4){

				//Actualizar ficha pareja
				//Realizar inscripcion en campeonato
//				campeonatoMasculinoParejasTerceraAddFacade.doInscribirJugador(
//						jugador1, jugador2, categoria, sessionState.getUser(), activo);
				
			}else if(categoria.getId() == 8){

				//Actualizar ficha pareja
				//Realizar inscripcion en campeonato
//				campeonatoMasculinoParejasTerceraAddFacade.doInscribirJugador(
//						jugador1, jugador2, categoria, sessionState.getUser(), activo);
				
			}
			
			redirect.getMiEquipo();
		}
		
	}
	
	public void doDesinscribirPareja(Integer id, Integer idCategoria){
		if(idCategoria == com.bolocelta.bbdd.constants.Categorias.PRIMERA){
			ParticipantesParejas participantesParejasPrimera = leerCampeonatoMasculinoParejasPrimera.readParticipante(id);
			if(participantesParejasPrimera != null){
				campeonatoMasculinoParejasPrimeraAddFacade.doDesinscribirPareja(participantesParejasPrimera, sessionState.getUser(), Activo.NO);
			}else{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "La pareja no se puede desinscribir.", null));
			}
		}else if(idCategoria == com.bolocelta.bbdd.constants.Categorias.SEGUNDA){
			ParticipantesParejas participantesParejasSegunda = leerCampeonatoMasculinoParejasSegunda.readParticipante(id);
			if(participantesParejasSegunda != null){
				campeonatoMasculinoParejasSegundaAddFacade.doDesinscribirPareja(participantesParejasSegunda, sessionState.getUser(), Activo.NO);
			}else{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "La pareja no se puede desinscribir.", null));
			}
		}else if(idCategoria == com.bolocelta.bbdd.constants.Categorias.TERCERA){
			ParticipantesParejas participantesParejasTercera = leerCampeonatoMasculinoParejasTercera.readParticipante(id);
			if(participantesParejasTercera != null){
				campeonatoMasculinoParejasTerceraAddFacade.doDesinscribirPareja(participantesParejasTercera, sessionState.getUser(), Activo.NO);
			}else{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "La pareja no se puede desinscribir.", null));
			}
		}
			
	}
	

}
