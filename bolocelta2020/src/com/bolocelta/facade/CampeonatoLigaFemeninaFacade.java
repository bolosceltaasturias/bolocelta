package com.bolocelta.facade;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.bolocelta.application.session.SessionState;
import com.bolocelta.bbdd.constants.Activo;
import com.bolocelta.bbdd.createTable.CrearCampeonatoLigaFemenina;
import com.bolocelta.bbdd.readTables.LeerCampeonatoLigaFemenina;
import com.bolocelta.entities.CampeonatoLigaFemeninaCalendario;
import com.bolocelta.entities.CampeonatoLigaFemeninaClasificacion;

@Named
@ConversationScoped
@ManagedBean
public class CampeonatoLigaFemeninaFacade implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private SessionState sessionState;
	
	private LeerCampeonatoLigaFemenina leerCampeonatoLigaFemenina = new LeerCampeonatoLigaFemenina();
	
	private CrearCampeonatoLigaFemenina campeonatoLigaFemenina = new CrearCampeonatoLigaFemenina();
	
	private List<CampeonatoLigaFemeninaClasificacion> resultListClasificacion = null;
	private List<CampeonatoLigaFemeninaCalendario> resultListCalendario = null;

	public List<CampeonatoLigaFemeninaClasificacion> getResultListClasificacion() {
		if(resultListClasificacion == null){
			resultListClasificacion = (List<CampeonatoLigaFemeninaClasificacion>) leerCampeonatoLigaFemenina.listResultClasificacion();
		}
		return resultListClasificacion;
	}
	
	public List<CampeonatoLigaFemeninaCalendario> getResultListCalendario() {
		if(resultListCalendario == null){
			resultListCalendario = (List<CampeonatoLigaFemeninaCalendario>) leerCampeonatoLigaFemenina.listResultCalendario();
		}
		return resultListCalendario;
	}

	public Object selected() {
		// TODO Auto-generated method stub
		return null;
	}

	public Object read(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	public String insert(Object insert) {
		// TODO Auto-generated method stub
		return null;
	}

	public String update(Object update) {
		// TODO Auto-generated method stub
		return null;
	}

	public String delete(Object delete) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Integer getRowsPaginatorClasificacion(){
		return getResultListClasificacion().size();
	}
	
	public Integer getRowsPaginatorCalendario(){
		return getResultListCalendario().size()/11;
	}
	
	public Integer getTotalRowsClasificacion(){
		return getResultListClasificacion().size();
	}
	
	   public String getPermisoActualizarResultado(Integer idEquipo){
	    	if(sessionState != null && sessionState.getUserEquipo() != null && sessionState.getUserEquipo().getId() != null){
	    		if(sessionState.getUserEquipo().getId().equals(idEquipo)){
	    			return "BCMEGEARLF";
	    		}
	    	}
	    	return "SIN PERMISO";
	    }
	    
	    public String getPermisoConfirmarResultado(Integer idEquipo){
	    	if(sessionState != null && sessionState.getUserEquipo() != null && sessionState.getUserEquipo().getId() != null){
	    		if(sessionState.getUserEquipo().getId().equals(idEquipo)){
	    			return "BCMEGEARCLF";
	    		}
	    	}
	    	return "SIN PERMISO";
	    }
		
	    public void doActualizarResultadoLiga(CampeonatoLigaFemeninaCalendario cec){
	    	boolean error = false;
			if(cec.isModificable()){
				//Validaciones
				if(cec.getTirada() < 0 || cec.getSacada() < 0 || cec.getPuntos() < 0){
					error = true;
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "La tirada, sacada y puntos han de tenera valor mayor de 0.", null));
				}
				
				//Si todo correcto actualizar resultado
				if(!error){
					campeonatoLigaFemenina.actualizarResultadoCalendarioLiga(cec);
					leerCampeonatoLigaFemenina = new LeerCampeonatoLigaFemenina();
					resultListClasificacion = (List<CampeonatoLigaFemeninaClasificacion>) leerCampeonatoLigaFemenina.listResultClasificacion();
					resultListCalendario = (List<CampeonatoLigaFemeninaCalendario>) leerCampeonatoLigaFemenina.listResultCalendario();
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha actualizado el resultado de la jugadora " + cec.getJugadora().getNombre(), null));
				}
			}
			
		}
	    
		public void doConfirmarResultado(CampeonatoLigaFemeninaCalendario cec){
			boolean error = false;
			if(cec.isModificable()){
				//Validaciones
				if(cec.getTirada() < 0 || cec.getSacada() < 0 || cec.getPuntos() < 0){
					error = true;
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "La tirada, sacada y puntos han de tenera valor mayor de 0.", null));
				}
				
				//Si todo correcto confirmar resultado y actualizar clasificacion
				if(!error){
					CampeonatoLigaFemeninaClasificacion cecCla = recoveryClasificacionEquipo(cec.getJugadoraId());
					campeonatoLigaFemenina.actualizarClasificacion(cecCla, cec);
					cec.setActivo(Activo.NO_NUMBER);
					campeonatoLigaFemenina.actualizarResultadoCalendarioLigaConfirmar(cec);
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha confirmado el resultado de la jugadora " + cec.getJugadora().getNombre(), null));
					leerCampeonatoLigaFemenina = new LeerCampeonatoLigaFemenina();
					resultListClasificacion = (List<CampeonatoLigaFemeninaClasificacion>) leerCampeonatoLigaFemenina.listResultClasificacion();
				}
			}
			
			
		}
		
		public CampeonatoLigaFemeninaClasificacion recoveryClasificacionEquipo(Integer idJugadora){
			for (CampeonatoLigaFemeninaClasificacion cec : getResultListClasificacion()) {
				if(cec.getJugadoraId().equals(idJugadora)){
					return cec;
				}
			}
			return null;
		}	
		

}
