package com.bolocelta.facade;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;

import com.bolocelta.bbdd.readTables.LeerModalidades;
import com.bolocelta.entities.Modalidades;

@Named
@ConversationScoped
@ManagedBean
public class ModalidadesFacade implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private LeerModalidades leerModalidades = new LeerModalidades();
	
	private List<Modalidades> resultList = null;

	public List<Modalidades> getResultList() {
		if(resultList == null){
			resultList = (List<Modalidades>) leerModalidades.listResult();
		}
		return resultList;
	}

	public Object selected() {
		// TODO Auto-generated method stub
		return null;
	}

	public Object read(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	public String insert(Object insert) {
		// TODO Auto-generated method stub
		return null;
	}

	public String update(Object update) {
		// TODO Auto-generated method stub
		return null;
	}

	public String delete(Object delete) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Integer getMaxRowsPaginator(){
		return 10;
	}
	
	public boolean isEnabledPaginator(){
		return getResultList() != null ? (getResultList().size() > getMaxRowsPaginator()) : false;
	}	
	
	

}
