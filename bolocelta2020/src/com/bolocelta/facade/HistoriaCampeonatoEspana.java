package com.bolocelta.facade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.inject.Named;

import com.bolocelta.application.session.SessionState;
import com.bolocelta.bbdd.readTables.LeerHistoricoCampeonatosEspana;
import com.bolocelta.entities.HistoricoCampeonatosEspana;

@Named
@ConversationScoped
@ManagedBean
public class HistoriaCampeonatoEspana implements Serializable {
	
	@Inject
	private SessionState sessionState;
	
	private static final long serialVersionUID = 1L;
	
	private LeerHistoricoCampeonatosEspana leerHistoricoCampeonatosEspana = new LeerHistoricoCampeonatosEspana();
	
	private List<HistoricoCampeonatosEspana> resultListHistoricoCampeonatoEspana = null;
	private List<HistoricoCampeonatosEspana> resultListHistoricoCampeonatoEspanaIndividual = null;
	private List<HistoricoCampeonatosEspana> resultListHistoricoCampeonatoEspanaParejas = null;
	private List<HistoricoCampeonatosEspana> resultListHistoricoCampeonatoEspanaEquipos = null;
	
	private List<Integer> resultListAnyoFasesTabShow = null;
	
	public List<HistoricoCampeonatosEspana> getResultListHistoricoCampeonatoEspanaAll() {
		if(resultListHistoricoCampeonatoEspana == null){
			resultListHistoricoCampeonatoEspana = (List<HistoricoCampeonatosEspana>) leerHistoricoCampeonatosEspana.listResult();
		}
		return resultListHistoricoCampeonatoEspana;
	}

	public List<HistoricoCampeonatosEspana> getResultListHistoricoCampeonatoEspanaIndividual() {
		if(resultListHistoricoCampeonatoEspanaIndividual == null){
			resultListHistoricoCampeonatoEspanaIndividual = (List<HistoricoCampeonatosEspana>) leerHistoricoCampeonatosEspana.listResultIndividual();
		}
		return resultListHistoricoCampeonatoEspanaIndividual;
	}
	
	public List<HistoricoCampeonatosEspana> getResultListHistoricoCampeonatoEspanaParejas() {
		if(resultListHistoricoCampeonatoEspanaParejas == null){
			resultListHistoricoCampeonatoEspanaParejas = (List<HistoricoCampeonatosEspana>) leerHistoricoCampeonatosEspana.listResultParejas();
		}
		return resultListHistoricoCampeonatoEspanaParejas;
	}
	
	public List<HistoricoCampeonatosEspana> getResultListHistoricoCampeonatoEspanaEquipos() {
		if(resultListHistoricoCampeonatoEspanaEquipos == null){
			resultListHistoricoCampeonatoEspanaEquipos = (List<HistoricoCampeonatosEspana>) leerHistoricoCampeonatosEspana.listResultEquipos();
		}
		return resultListHistoricoCampeonatoEspanaEquipos;
	}
	
	public List<HistoricoCampeonatosEspana> getResultListHistoricoCampeonatoEspanaIndividualByAnyo(Integer anyo) {
		List<HistoricoCampeonatosEspana> result = new ArrayList<HistoricoCampeonatosEspana>();
		for (HistoricoCampeonatosEspana historicoCampeonatosEspana : getResultListHistoricoCampeonatoEspanaIndividual()) {
			if(historicoCampeonatosEspana.getAnyo().equals(anyo)){
				result.add(historicoCampeonatosEspana);
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(result, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				HistoricoCampeonatosEspana cec1 = (HistoricoCampeonatosEspana) o1;
				HistoricoCampeonatosEspana cec2 = (HistoricoCampeonatosEspana) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPosicion().compareTo(cec2.getPosicion());
					return rpuntos;
			}
		});
		
		return result;
	}
	
	public List<HistoricoCampeonatosEspana> getResultListHistoricoCampeonatoEspanaParejasByAnyo(Integer anyo) {
		List<HistoricoCampeonatosEspana> result = new ArrayList<HistoricoCampeonatosEspana>();
		for (HistoricoCampeonatosEspana historicoCampeonatosEspana : getResultListHistoricoCampeonatoEspanaParejas()) {
			if(historicoCampeonatosEspana.getAnyo().equals(anyo)){
				result.add(historicoCampeonatosEspana);
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(result, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				HistoricoCampeonatosEspana cec1 = (HistoricoCampeonatosEspana) o1;
				HistoricoCampeonatosEspana cec2 = (HistoricoCampeonatosEspana) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPosicion().compareTo(cec2.getPosicion());
					return rpuntos;
			}
		});
		
		return result;
	}
	
	public List<HistoricoCampeonatosEspana> getResultListHistoricoCampeonatoEspanaEquiposByAnyo(Integer anyo) {
		List<HistoricoCampeonatosEspana> result = new ArrayList<HistoricoCampeonatosEspana>();
		for (HistoricoCampeonatosEspana historicoCampeonatosEspana : getResultListHistoricoCampeonatoEspanaEquipos()) {
			if(historicoCampeonatosEspana.getAnyo().equals(anyo)){
				result.add(historicoCampeonatosEspana);
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(result, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				HistoricoCampeonatosEspana cec1 = (HistoricoCampeonatosEspana) o1;
				HistoricoCampeonatosEspana cec2 = (HistoricoCampeonatosEspana) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPosicion().compareTo(cec2.getPosicion());
					return rpuntos;
			}
		});
		
		return result;
	}
	

	
	public boolean isDataCampeonato() {
		if(getResultListAnyoTabShow().size() > 0){
			return true;
		}
		return false;
	}
	
	public List<Integer> getResultListAnyoTabShow() {
		if(resultListAnyoFasesTabShow == null){
			resultListAnyoFasesTabShow = new ArrayList<>();
			
			for (HistoricoCampeonatosEspana historicoCampeonatosEspana : getResultListHistoricoCampeonatoEspanaAll()) {
				if(!resultListAnyoFasesTabShow.contains(historicoCampeonatosEspana.getAnyo())){
					resultListAnyoFasesTabShow.add(historicoCampeonatosEspana.getAnyo());
				}
			}
			
		}
		
		//Ordenar la clasificacion
		Collections.sort(resultListAnyoFasesTabShow, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				Integer cec1 = (Integer) o1;
				Integer cec2 = (Integer) o2;
				//1. Ordenar por anyo descendente 
				int rpuntos = cec1.compareTo(cec2);
					return rpuntos;
			}
		});
		
		Collections.reverse(resultListAnyoFasesTabShow);
		
		return resultListAnyoFasesTabShow;
	}

	public Object selected() {
		// TODO Auto-generated method stub
		return null;
	}

	public Object read(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	public String insert(Object insert) {
		// TODO Auto-generated method stub
		return null;
	}

	public String update(Object update) {
		// TODO Auto-generated method stub
		return null;
	}

	public String delete(Object delete) {
		// TODO Auto-generated method stub
		return null;
	}
		

}
