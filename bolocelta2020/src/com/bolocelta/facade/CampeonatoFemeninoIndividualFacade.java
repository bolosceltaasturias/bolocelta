package com.bolocelta.facade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import com.bolocelta.application.session.SessionState;
import com.bolocelta.bbdd.constants.Activo;
import com.bolocelta.bbdd.constants.FasesModelo;
import com.bolocelta.bbdd.constants.FasesTabShow;
import com.bolocelta.bbdd.createTable.CrearCampeonatoFemeninoIndividual;
import com.bolocelta.bbdd.readTables.LeerCampeonatoFemeninoIndividual;
import com.bolocelta.entities.Fases;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseCF;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseFC;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseFF;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseI;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseOF;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseSF;
import com.bolocelta.entities.sorteos.individual.ClasificacionFaseI;

@Named
@ConversationScoped
@ManagedBean
public class CampeonatoFemeninoIndividualFacade implements Serializable {
	
	@Inject
	private SessionState sessionState;
	
	private static final long serialVersionUID = 1L;
	
	private LeerCampeonatoFemeninoIndividual leerCampeonatoFemeninoIndividual = new LeerCampeonatoFemeninoIndividual();
	private CrearCampeonatoFemeninoIndividual crearCampeonatoFemeninoIndividual = new CrearCampeonatoFemeninoIndividual();
	
	private List<ClasificacionFaseI> resultListClasificacionFaseI = null;
	private List<CalendarioFaseI> resultListCalendarioFaseI = null;
	private List<CalendarioFaseOF> resultListCalendarioFaseOF = null;
	private List<CalendarioFaseCF> resultListCalendarioFaseCF = null;
	private List<CalendarioFaseSF> resultListCalendarioFaseSF = null;
	private List<CalendarioFaseFC> resultListCalendarioFaseFC = null;
	private List<CalendarioFaseFF> resultListCalendarioFaseFF = null;
	
	private List<Fases> resultListFases = null;
	private List<FasesTabShow> resultListFasesTabShow = null;
	private Fases fase = null;
	
	private String observacionesCampeonato1;
	private String observacionesCampeonato2;
	private String observacionesCampeonato3;
	private String observacionesCampeonato4;
	private String observacionesCampeonato5;
	private String observacionesCampeonato6;
	private String observacionesCampeonato7;
	
    private TreeNode cuadro;
    

	public List<ClasificacionFaseI> getResultListClasificacionFaseI() {
		if(resultListClasificacionFaseI == null){
			resultListClasificacionFaseI = (List<ClasificacionFaseI>) leerCampeonatoFemeninoIndividual.listResultClasificacionFaseI();
		}
		return resultListClasificacionFaseI;
	}
	
	public List<ClasificacionFaseI> getResultListClasificacionFaseIByGrupo(String grupo) {
		List<ClasificacionFaseI> resultListClasificacionFaseIByGrupo = new ArrayList<ClasificacionFaseI>();
		if(resultListClasificacionFaseI == null){
			resultListClasificacionFaseI = (List<ClasificacionFaseI>) leerCampeonatoFemeninoIndividual.listResultClasificacionFaseI();
		}else{
			for (ClasificacionFaseI clasificacionFaseI : resultListClasificacionFaseI) {
				if(clasificacionFaseI.getGrupo().equalsIgnoreCase(grupo)){
					resultListClasificacionFaseIByGrupo.add(clasificacionFaseI);
				}
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(resultListClasificacionFaseIByGrupo, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				ClasificacionFaseI cec1 = (ClasificacionFaseI) o1;
				ClasificacionFaseI cec2 = (ClasificacionFaseI) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPt().compareTo(cec2.getPt());
				//2. Si hay empate ordenar por enfrentamiento directo
				if (rpuntos == 0){
					CalendarioFaseI enfrentamientoDirecto = getResultListEnfrentamientoByJugadoresGrupo(cec1.getGrupo(), cec1.getJugadorId(), cec2.getJugadorId());
					if(enfrentamientoDirecto == null){
						enfrentamientoDirecto = getResultListEnfrentamientoByJugadoresGrupo(cec1.getGrupo(), cec2.getJugadorId(), cec1.getJugadorId());
						rpuntos = enfrentamientoDirecto.getJuegosJugador2().compareTo(enfrentamientoDirecto.getJuegosJugador1());
					}else{
						rpuntos = enfrentamientoDirecto.getJuegosJugador1().compareTo(enfrentamientoDirecto.getJuegosJugador2());
					}
					
				}
				
				//3. Si hay empate en enfrentamiento directo, mirar average general
				if (rpuntos == 0) {
					Integer part1 = cec1.getPf() - cec1.getPc();
					Integer part2 = cec2.getPf() - cec2.getPc();
					int rdifpartidas = part1.compareTo(part2);
					if (rdifpartidas == 0) {
						return cec1.getPt().compareTo(cec2.getPt());
					}
					return rdifpartidas;
				}
				return rpuntos;
			}
		});
		
		Collections.reverse(resultListClasificacionFaseIByGrupo);
		
		return resultListClasificacionFaseIByGrupo;
	}
	
	public ClasificacionFaseI getResultListClasificacionFaseIByJugador(String grupo, Integer jugadorId) {
		if(resultListClasificacionFaseI == null){
			resultListClasificacionFaseI = (List<ClasificacionFaseI>) leerCampeonatoFemeninoIndividual.listResultClasificacionFaseI();
		}
		for (ClasificacionFaseI clasificacionFaseI : resultListClasificacionFaseI) {
			if(clasificacionFaseI.getGrupo().equalsIgnoreCase(grupo)){
				if(clasificacionFaseI.getJugadorId().equals(jugadorId)){
					return clasificacionFaseI;
				}
			}
		}
		return null;
	}
	
	public List<CalendarioFaseI> getResultListCalendarioFaseI() {
		if(resultListCalendarioFaseI == null){
			resultListCalendarioFaseI = (List<CalendarioFaseI>) leerCampeonatoFemeninoIndividual.listResultCalendarioFaseI();
		}
		return resultListCalendarioFaseI;
	}
	
	public List<CalendarioFaseOF> getResultListCalendarioFaseOF() {
		if(resultListCalendarioFaseOF == null){
			resultListCalendarioFaseOF = (List<CalendarioFaseOF>) leerCampeonatoFemeninoIndividual.listResultCalendarioFaseOF();
		}
		return resultListCalendarioFaseOF;
	}
	
	public List<CalendarioFaseCF> getResultListCalendarioFaseCF() {
		if(resultListCalendarioFaseCF == null){
			resultListCalendarioFaseCF = (List<CalendarioFaseCF>) leerCampeonatoFemeninoIndividual.listResultCalendarioFaseCF();
		}
		return resultListCalendarioFaseCF;
	}
	
	public List<CalendarioFaseSF> getResultListCalendarioFaseSF() {
		if(resultListCalendarioFaseSF == null){
			resultListCalendarioFaseSF = (List<CalendarioFaseSF>) leerCampeonatoFemeninoIndividual.listResultCalendarioFaseSF();
		}
		return resultListCalendarioFaseSF;
	}
	
	public List<CalendarioFaseFC> getResultListCalendarioFaseFC() {
		if(resultListCalendarioFaseFC == null){
			resultListCalendarioFaseFC = (List<CalendarioFaseFC>) leerCampeonatoFemeninoIndividual.listResultCalendarioFaseFC();
		}
		return resultListCalendarioFaseFC;
	}
	
	public List<CalendarioFaseFF> getResultListCalendarioFaseFF() {
		if(resultListCalendarioFaseFF == null){
			resultListCalendarioFaseFF = (List<CalendarioFaseFF>) leerCampeonatoFemeninoIndividual.listResultCalendarioFaseFF();
		}
		return resultListCalendarioFaseFF;
	}
	
	public List<CalendarioFaseI> getResultListCalendarioFaseIByGrupo(String grupo) {
		List<CalendarioFaseI> resultListCalendarioFaseIByGrupo = new ArrayList<CalendarioFaseI>();
		if(resultListCalendarioFaseI == null){
			resultListCalendarioFaseI = (List<CalendarioFaseI>) leerCampeonatoFemeninoIndividual.listResultCalendarioFaseI();
		}else{
			for (CalendarioFaseI calendarioFaseI : resultListCalendarioFaseI) {
				if(calendarioFaseI.getGrupo().equalsIgnoreCase(grupo)){
					resultListCalendarioFaseIByGrupo.add(calendarioFaseI);
				}
			}
		}
		
		//Ordenar la calendario
		Collections.sort(resultListCalendarioFaseIByGrupo, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				CalendarioFaseI cec1 = (CalendarioFaseI) o1;
				CalendarioFaseI cec2 = (CalendarioFaseI) o2;
				
				int rpuntos = cec1.getOrden().compareTo(cec2.getOrden());
				return rpuntos;
			}
		});
		
		Collections.reverseOrder();
		
		return resultListCalendarioFaseIByGrupo;
	}
	
	public boolean getResultListCalendarioFaseIByGrupoFinalizado(String grupo) {
		List<CalendarioFaseI> resultListCalendarioFaseIByGrupo = new ArrayList<CalendarioFaseI>();
		if(resultListCalendarioFaseI == null){
			resultListCalendarioFaseI = (List<CalendarioFaseI>) leerCampeonatoFemeninoIndividual.listResultCalendarioFaseI();
		}else{
			for (CalendarioFaseI calendarioFaseI : resultListCalendarioFaseI) {
				if(calendarioFaseI.getGrupo().equalsIgnoreCase(grupo)){
					resultListCalendarioFaseIByGrupo.add(calendarioFaseI);
				}
			}
		}
		
		for (CalendarioFaseI calendarioFaseI : resultListCalendarioFaseIByGrupo) {
			if(calendarioFaseI.isModificable()){
				return false;
			}
		}
		return true;
	}
	
	public CalendarioFaseI getResultListEnfrentamientoByJugadoresGrupo(String grupo, Integer jugador1Id, Integer jugador2Id) {
		if(resultListCalendarioFaseI == null){
			resultListCalendarioFaseI = (List<CalendarioFaseI>) leerCampeonatoFemeninoIndividual.listResultCalendarioFaseI();
		}
		for (CalendarioFaseI calendarioFaseI : resultListCalendarioFaseI) {
			if(calendarioFaseI.getGrupo().equalsIgnoreCase(grupo)){
				if(calendarioFaseI.getJugador1Id().equals(jugador1Id) && calendarioFaseI.getJugador2Id().equals(jugador2Id)){
					return calendarioFaseI;
				}
			}
		}
		return null;
	}
	
	public List<?> getResultListCalendarioByCruceDirecto(Integer numeroFase) {
		
		List<?> resultListCalendarioByCruceDirecto = new ArrayList<>();
		if(numeroFase.equals(FasesModelo.FASE_OF)){
			if(resultListCalendarioFaseOF == null){
				resultListCalendarioFaseOF = (List<CalendarioFaseOF>) obtenerCalendarioByCruceDirectoOF();
			}
			return resultListCalendarioFaseOF;
		}else if(numeroFase.equals(FasesModelo.FASE_CF)){
			if(resultListCalendarioFaseCF == null){
				resultListCalendarioFaseCF = (List<CalendarioFaseCF>) obtenerCalendarioByCruceDirectoCF();
			}
			return resultListCalendarioFaseCF;
		}else if(numeroFase.equals(FasesModelo.FASE_SF)){
			if(resultListCalendarioFaseSF == null){
				resultListCalendarioFaseSF = (List<CalendarioFaseSF>) obtenerCalendarioByCruceDirectoSF();
			}
			return resultListCalendarioFaseSF;
		}else if(numeroFase.equals(FasesModelo.FASE_FC)){
			if(resultListCalendarioFaseFC == null){
				resultListCalendarioFaseFC = (List<CalendarioFaseFC>) obtenerCalendarioByCruceDirectoFC();
			}
			return resultListCalendarioFaseFC;
		}else if(numeroFase.equals(FasesModelo.FASE_FF)){
			if(resultListCalendarioFaseFF == null){
				resultListCalendarioFaseFF = (List<CalendarioFaseFF>) obtenerCalendarioByCruceDirectoFF();
			}
			return resultListCalendarioFaseFF;
		}
		return resultListCalendarioByCruceDirecto;
	}
	
	public List<CalendarioFaseOF> obtenerCalendarioByCruceDirectoOF(){
		if(resultListCalendarioFaseOF == null){
			resultListCalendarioFaseOF = (List<CalendarioFaseOF>) getResultListCalendarioFaseOF();
		}
		//Ordenar la calendario
		Collections.sort(resultListCalendarioFaseOF, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				CalendarioFaseOF cec1 = (CalendarioFaseOF) o1;
				CalendarioFaseOF cec2 = (CalendarioFaseOF) o2;
				
				int rpuntos = cec1.getOrden().compareTo(cec2.getOrden());
				return rpuntos;
			}
		});
		
		Collections.reverseOrder();
		return resultListCalendarioFaseOF;
	}
	
	public List<CalendarioFaseCF> obtenerCalendarioByCruceDirectoCF(){
		if(resultListCalendarioFaseCF == null){
			resultListCalendarioFaseCF = (List<CalendarioFaseCF>) getResultListCalendarioFaseCF();
		}
		//Ordenar la calendario
		Collections.sort(resultListCalendarioFaseCF, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				CalendarioFaseCF cec1 = (CalendarioFaseCF) o1;
				CalendarioFaseCF cec2 = (CalendarioFaseCF) o2;
				
				int rpuntos = cec1.getOrden().compareTo(cec2.getOrden());
				return rpuntos;
			}
		});
		
		Collections.reverseOrder();
		return resultListCalendarioFaseCF;
	}
	
	public List<CalendarioFaseSF> obtenerCalendarioByCruceDirectoSF(){
		if(resultListCalendarioFaseSF == null){
			resultListCalendarioFaseSF = (List<CalendarioFaseSF>) getResultListCalendarioFaseSF();
		}
		//Ordenar la calendario
		Collections.sort(resultListCalendarioFaseSF, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				CalendarioFaseSF cec1 = (CalendarioFaseSF) o1;
				CalendarioFaseSF cec2 = (CalendarioFaseSF) o2;
				
				int rpuntos = cec1.getOrden().compareTo(cec2.getOrden());
				return rpuntos;
			}
		});
		
		Collections.reverseOrder();
		return resultListCalendarioFaseSF;
	}
	
	public List<CalendarioFaseFC> obtenerCalendarioByCruceDirectoFC(){
		if(resultListCalendarioFaseFC == null){
			resultListCalendarioFaseFC = (List<CalendarioFaseFC>) getResultListCalendarioFaseFC();
		}
		//Ordenar la calendario
		Collections.sort(resultListCalendarioFaseFC, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				CalendarioFaseFC cec1 = (CalendarioFaseFC) o1;
				CalendarioFaseFC cec2 = (CalendarioFaseFC) o2;
				
				int rpuntos = cec1.getOrden().compareTo(cec2.getOrden());
				return rpuntos;
			}
		});
		
		Collections.reverseOrder();
		return resultListCalendarioFaseFC;
	}
	
	public List<CalendarioFaseFF> obtenerCalendarioByCruceDirectoFF(){
		if(resultListCalendarioFaseFF == null){
			resultListCalendarioFaseFF = (List<CalendarioFaseFF>) getResultListCalendarioFaseFF();
		}
		//Ordenar la calendario
		Collections.sort(resultListCalendarioFaseFF, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				CalendarioFaseFF cec1 = (CalendarioFaseFF) o1;
				CalendarioFaseFF cec2 = (CalendarioFaseFF) o2;
				
				int rpuntos = cec1.getOrden().compareTo(cec2.getOrden());
				return rpuntos;
			}
		});
		
		Collections.reverseOrder();
		return resultListCalendarioFaseFF;
	}
	
	
	
	public List<Fases> getResultListFases() {
		if(resultListFases == null){
			resultListFases = (List<Fases>) leerCampeonatoFemeninoIndividual.listResultFases();
		}
		return resultListFases;
	}
	
	public boolean isDataCampeonato() {
		if(getResultListFasesTabShow().size() > 0){
			return true;
		}
		return false;
	}
	
	public List<FasesTabShow> getResultListFasesTabShow() {
		if(resultListFasesTabShow == null){
			resultListFasesTabShow = new ArrayList<>();
			for(Fases fase : getResultListFases()){
				if(fase.getActivo().equals(Activo.SI)){
					if(fase.getNumero().equals(FasesModelo.FASE_I)){
						List<FasesTabShow> fasesList = (List<FasesTabShow>) leerCampeonatoFemeninoIndividual.listResultGruposFaseI();
						resultListFasesTabShow.addAll(fasesList);
					}else if(fase.getNumero().equals(FasesModelo.FASE_II)){
						//De momento nada
					}else if(fase.getNumero().equals(FasesModelo.FASE_OF)){
						if(obtenerCalendarioByCruceDirectoOF() != null && obtenerCalendarioByCruceDirectoOF().size() > 0){
							resultListFasesTabShow.add(FasesTabShow.searchFaseTabShow(FasesModelo.FASE_OF, null));	
						}
					}else if(fase.getNumero().equals(FasesModelo.FASE_CF)){
						if(obtenerCalendarioByCruceDirectoCF() != null && obtenerCalendarioByCruceDirectoCF().size() > 0){
							resultListFasesTabShow.add(FasesTabShow.searchFaseTabShow(FasesModelo.FASE_CF, null));
						}
					}else if(fase.getNumero().equals(FasesModelo.FASE_SF)){
						if(obtenerCalendarioByCruceDirectoSF() != null && obtenerCalendarioByCruceDirectoSF().size() > 0){
							resultListFasesTabShow.add(FasesTabShow.searchFaseTabShow(FasesModelo.FASE_SF, null));
						}
					}else if(fase.getNumero().equals(FasesModelo.FASE_FC)){
						if(obtenerCalendarioByCruceDirectoFC() != null && obtenerCalendarioByCruceDirectoFC().size() > 0){
							resultListFasesTabShow.add(FasesTabShow.searchFaseTabShow(FasesModelo.FASE_FC, null));
						}
					}else if(fase.getNumero().equals(FasesModelo.FASE_FF)){
						if(obtenerCalendarioByCruceDirectoFF() != null && obtenerCalendarioByCruceDirectoFF().size() > 0){
							resultListFasesTabShow.add(FasesTabShow.searchFaseTabShow(FasesModelo.FASE_FF, null));
							//A�adir grafico
							resultListFasesTabShow.add(FasesTabShow.FASE_GRAPHIC);
						}
					}
				}
			}
		}
		return resultListFasesTabShow;
	}

	public Object selected() {
		// TODO Auto-generated method stub
		return null;
	}

	public Object read(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	public String insert(Object insert) {
		// TODO Auto-generated method stub
		return null;
	}

	public String update(Object update) {
		// TODO Auto-generated method stub
		return null;
	}

	public String delete(Object delete) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Integer getRowsPaginatorClasificacionFaseI(){
		return getResultListClasificacionFaseI().size();
	}
	
	public Integer getRowsPaginatorCalendarioFaseI(){
		return getResultListCalendarioFaseI().size()/2;
	}
	
	public Integer getTotalRowsClasificacionFaseI(){
		return getResultListClasificacionFaseI().size();
	}
	
	public Integer getRowsPaginatorClasificacionFaseIByGrupo(String grupo){
		return getResultListClasificacionFaseIByGrupo(grupo).size();
	}
	
	public Integer getRowsPaginatorCalendarioFaseIByGrupo(String grupo){
		if(getResultListCalendarioFaseIByGrupo(grupo).size() > 10){
			return getResultListCalendarioFaseIByGrupo(grupo).size()/2;
		}
		return getResultListCalendarioFaseIByGrupo(grupo).size();
	}

	public Fases getFase(Integer numeroFase) {
		this.fase = leerCampeonatoFemeninoIndividual.readFases(numeroFase);
		return this.fase;
	}

	public String getObservacionesCampeonato1() {
		if(this.observacionesCampeonato1 == null){
			this.observacionesCampeonato1 = leerCampeonatoFemeninoIndividual.getObservacionesCampeonato1();
			
		}
		return this.observacionesCampeonato1;
	}
	
	public String getObservacionesCampeonato2() {
		if(this.observacionesCampeonato2 == null){
			this.observacionesCampeonato2 = leerCampeonatoFemeninoIndividual.getObservacionesCampeonato2();
			
		}
		return this.observacionesCampeonato2;
	}
	
	public String getObservacionesCampeonato3() {
		if(this.observacionesCampeonato3 == null){
			this.observacionesCampeonato3 = leerCampeonatoFemeninoIndividual.getObservacionesCampeonato3();
			
		}
		return this.observacionesCampeonato3;
	}
	
	public String getObservacionesCampeonato4() {
		if(this.observacionesCampeonato4 == null){
			this.observacionesCampeonato4 = leerCampeonatoFemeninoIndividual.getObservacionesCampeonato4();
			
		}
		return this.observacionesCampeonato4;
	}
	
	public String getObservacionesCampeonato5() {
		if(this.observacionesCampeonato5 == null){
			this.observacionesCampeonato5 = leerCampeonatoFemeninoIndividual.getObservacionesCampeonato5();
			
		}
		return this.observacionesCampeonato5;
	}
	
	public String getObservacionesCampeonato6() {
		if(this.observacionesCampeonato6 == null){
			this.observacionesCampeonato6 = leerCampeonatoFemeninoIndividual.getObservacionesCampeonato6();
			
		}
		return this.observacionesCampeonato6;
	}
	
	public String getObservacionesCampeonato7() {
		if(this.observacionesCampeonato7 == null){
			this.observacionesCampeonato7 = leerCampeonatoFemeninoIndividual.getObservacionesCampeonato7();
			
		}
		return this.observacionesCampeonato7;
	}
	
	
 
    public TreeNode getCuadro() {
    	
    	if(cuadro == null){
    		
    		List<CalendarioFaseFF> calendarioFaseFFList = getResultListCalendarioFaseFF();
    		List<CalendarioFaseSF> calendarioFaseSFList = getResultListCalendarioFaseSF();
    		List<CalendarioFaseCF> calendarioFaseCFList = getResultListCalendarioFaseCF();
    		List<CalendarioFaseOF> calendarioFaseOFList = getResultListCalendarioFaseOF();
    		
    		Integer faseAnteriorCF = calendarioFaseCFList.get(0).getFaseAnterior();
    		
    		//CAMPEON
            String idNodeCampeonato = "CAMPEON";
            TreeNode nodeCampeonato = null;
            nodeCampeonato = recuperarNodoCalendarioCampeon(nodeCampeonato, calendarioFaseFFList, idNodeCampeonato, nodeCampeonato);
            nodeCampeonato.setExpanded(true);
    		
    		
    		
    		//FINALISTAS
            String idNodeFF1A = "SF1";
            TreeNode nodeFF1A = null;
            nodeFF1A = recuperarNodoFinalistas(nodeCampeonato, calendarioFaseSFList, idNodeFF1A, nodeFF1A);
            nodeFF1A.setExpanded(true);
            
            String idNodeFF1B = "SF2";
            TreeNode nodeFF1B = null;
            nodeFF1B = recuperarNodoFinalistas(nodeCampeonato, calendarioFaseSFList, idNodeFF1B, nodeFF1B);
            nodeFF1B.setExpanded(true);
            
    		//SEMIFINALISTAS
            String idNodeSF1A = "CFA";
            TreeNode nodeSF1A = null;
            nodeSF1A = recuperarNodoSemiFinalistas(nodeFF1A, calendarioFaseCFList, idNodeSF1A, nodeSF1A);
            nodeSF1A.setExpanded(true);
            
            String idNodeSF1B = "CFB";
            TreeNode nodeSF1B = null;
            nodeSF1B = recuperarNodoSemiFinalistas(nodeFF1A, calendarioFaseCFList, idNodeSF1B, nodeSF1B);
            nodeSF1B.setExpanded(true);
            
            String idNodeSF2A = "CFC";
            TreeNode nodeSF2A = null;
            nodeSF2A = recuperarNodoSemiFinalistas(nodeFF1B, calendarioFaseCFList, idNodeSF2A, nodeSF2A);
            nodeSF2A.setExpanded(true);
            
            String idNodeSF2B = "CFD";
            TreeNode nodeSF2B = null;
            nodeSF2B = recuperarNodoSemiFinalistas(nodeFF1B, calendarioFaseCFList, idNodeSF2B, nodeSF2B);
            nodeSF2B.setExpanded(true);
            
            
            //CUARTOS DE FINAL
            //PROCEDENCIA OCTAVOS DE FINAL
            
            if(faseAnteriorCF.equals(FasesModelo.FASE_OF)){
                String idNodeCF1A = "OFA";
                TreeNode nodeCF1A = null;
                nodeCF1A = recuperarNodoCuartosFinal(nodeSF1A, calendarioFaseOFList, idNodeCF1A, nodeCF1A);
                nodeCF1A.setExpanded(true);
                
                String idNodeCF1B = "OFB";
                TreeNode nodeCF1B = null;
                nodeCF1B = recuperarNodoCuartosFinal(nodeSF1A, calendarioFaseOFList, idNodeCF1B, nodeCF1B);
                nodeCF1B.setExpanded(true);
                
                
                
                String idNodeCF2A = "OFC";
                TreeNode nodeCF2A = null;
                nodeCF2A = recuperarNodoCuartosFinal(nodeSF1B, calendarioFaseOFList, idNodeCF2A, nodeCF2A);
                nodeCF2A.setExpanded(true);
                
                String idNodeCF2B = "OFD";
                TreeNode nodeCF2B = null;
                nodeCF2B = recuperarNodoCuartosFinal(nodeSF1B, calendarioFaseOFList, idNodeCF2B, nodeCF2B);
                nodeCF2B.setExpanded(true);
                
                


                String idNodeCF3A = "OFE";
                TreeNode nodeCF3A = null;
                nodeCF3A = recuperarNodoCuartosFinal(nodeSF2A, calendarioFaseOFList, idNodeCF3A, nodeCF3A);
                nodeCF3A.setExpanded(true);
                
                String idNodeCF3B = "OFF";
                TreeNode nodeCF3B = null;
                nodeCF3B = recuperarNodoCuartosFinal(nodeSF2A, calendarioFaseOFList, idNodeCF3B, nodeCF3B);
                nodeCF3B.setExpanded(true);
                
                
                
                String idNodeCF4A = "OFG";
                TreeNode nodeCF4A = null;
                nodeCF4A = recuperarNodoCuartosFinal(nodeSF2B, calendarioFaseOFList, idNodeCF4A, nodeCF4A);
                nodeCF4A.setExpanded(true);
                
                String idNodeCF4B = "OFH";
                TreeNode nodeCF4B = null;
                nodeCF4B = recuperarNodoCuartosFinal(nodeSF2B, calendarioFaseOFList, idNodeCF4B, nodeCF4B);
                nodeCF4B.setExpanded(true);
                
                
                //OCTAVOS DE FINAL
                //PROCEDENCIA GRUPOS
                
                String idNodeOF1A = "OFA";
                TreeNode nodeOF1A = null;
                nodeOF1A = recuperarNodoCalendarioOF(nodeCF1A, calendarioFaseOFList, idNodeOF1A, nodeOF1A, true, false);
                nodeOF1A.setExpanded(true);
                
                String idNodeOF1B = "OFA";
                TreeNode nodeOF1B = null;
                nodeOF1B = recuperarNodoCalendarioOF(nodeCF1A, calendarioFaseOFList, idNodeOF1B, nodeOF1B, false, true);
                nodeOF1B.setExpanded(true);
                
                String idNodeOF2A = "OFB";
                TreeNode nodeOF2A = null;
                nodeOF2A = recuperarNodoCalendarioOF(nodeCF1B, calendarioFaseOFList, idNodeOF2A, nodeOF2A, true, false);
                nodeOF2A.setExpanded(true);
                
                String idNodeOF2B = "OFB";
                TreeNode nodeOF2B = null;
                nodeOF2B = recuperarNodoCalendarioOF(nodeCF1B, calendarioFaseOFList, idNodeOF2B, nodeOF2B, false, true);
                nodeOF2B.setExpanded(true);
                
                
                
                
                
                
                
                String idNodeOF3A = "OFC";
                TreeNode nodeOF3A = null;
                nodeOF3A = recuperarNodoCalendarioOF(nodeCF2A, calendarioFaseOFList, idNodeOF3A, nodeOF3A, true, false);
                nodeOF3A.setExpanded(true);
                
                String idNodeOF3B = "OFC";
                TreeNode nodeOF3B = null;
                nodeOF3B = recuperarNodoCalendarioOF(nodeCF2A, calendarioFaseOFList, idNodeOF3B, nodeOF3B, false, true);
                nodeOF3B.setExpanded(true);
                
                String idNodeOF4A = "OFD";
                TreeNode nodeOF4A = null;
                nodeOF4A = recuperarNodoCalendarioOF(nodeCF2B, calendarioFaseOFList, idNodeOF4A, nodeOF4A, true, false);
                nodeOF4A.setExpanded(true);
                
                String idNodeOF4B = "OFD";
                TreeNode nodeOF4B = null;
                nodeOF4B = recuperarNodoCalendarioOF(nodeCF2B, calendarioFaseOFList, idNodeOF4B, nodeOF4B, false, true);
                nodeOF4B.setExpanded(true);
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                String idNodeOF5A = "OFE";
                TreeNode nodeOF5A = null;
                nodeOF5A = recuperarNodoCalendarioOF(nodeCF3A, calendarioFaseOFList, idNodeOF5A, nodeOF5A, true, false);
                nodeOF5A.setExpanded(true);
                
                String idNodeOF5B = "OFE";
                TreeNode nodeOF5B = null;
                nodeOF5B = recuperarNodoCalendarioOF(nodeCF3A, calendarioFaseOFList, idNodeOF5B, nodeOF5B, false, true);
                nodeOF5B.setExpanded(true);
                
                String idNodeOF6A = "OFF";
                TreeNode nodeOF6A = null;
                nodeOF6A = recuperarNodoCalendarioOF(nodeCF3B, calendarioFaseOFList, idNodeOF6A, nodeOF6A, true, false);
                nodeOF6A.setExpanded(true);
                
                String idNodeOF6B = "OFF";
                TreeNode nodeOF6B = null;
                nodeOF6B = recuperarNodoCalendarioOF(nodeCF3B, calendarioFaseOFList, idNodeOF6B, nodeOF6B, false, true);
                nodeOF6B.setExpanded(true);
                
                
                
                
                
                
                
                String idNodeOF7A = "OFG";
                TreeNode nodeOF7A = null;
                nodeOF7A = recuperarNodoCalendarioOF(nodeCF4A, calendarioFaseOFList, idNodeOF7A, nodeOF7A, true, false);
                nodeOF7A.setExpanded(true);
                
                String idNodeOF7B = "OFG";
                TreeNode nodeOF7B = null;
                nodeOF7B = recuperarNodoCalendarioOF(nodeCF4A, calendarioFaseOFList, idNodeOF7B, nodeOF7B, false, true);
                nodeOF7B.setExpanded(true);
                
                String idNodeOF8A = "OFH";
                TreeNode nodeOF8A = null;
                nodeOF8A = recuperarNodoCalendarioOF(nodeCF4B, calendarioFaseOFList, idNodeOF8A, nodeOF8A, true, false);
                nodeOF8A.setExpanded(true);
                
                String idNodeOF8B = "OFH";
                TreeNode nodeOF8B = null;
                nodeOF8B = recuperarNodoCalendarioOF(nodeCF4B, calendarioFaseOFList, idNodeOF8B, nodeOF8B, false, true);
                nodeOF8B.setExpanded(true);
                
                
                
            }else if(faseAnteriorCF.equals(FasesModelo.FASE_I)){
                String idNodeCF1A = "CFA";
                TreeNode nodeCF1A = null;
                nodeCF1A = recuperarNodoCalendarioGruposCF(nodeSF1A, calendarioFaseCFList, idNodeCF1A, nodeCF1A, true, false);
                nodeCF1A.setExpanded(true);
                
                String idNodeCF1B = "CFA";
                TreeNode nodeCF1B = null;
                nodeCF1B = recuperarNodoCalendarioGruposCF(nodeSF1A, calendarioFaseCFList, idNodeCF1B, nodeCF1B, false, true);
                nodeCF1B.setExpanded(true);
                
                
                
                String idNodeCF2A = "CFB";
                TreeNode nodeCF2A = null;
                nodeCF2A = recuperarNodoCalendarioGruposCF(nodeSF1B, calendarioFaseCFList, idNodeCF2A, nodeCF2A, true, false);
                nodeCF2A.setExpanded(true);
                
                String idNodeCF2B = "CFB";
                TreeNode nodeCF2B = null;
                nodeCF2B = recuperarNodoCalendarioGruposCF(nodeSF1B, calendarioFaseCFList, idNodeCF2B, nodeCF2B, false, true);
                nodeCF2B.setExpanded(true);
                
                


                String idNodeCF3A = "CFC";
                TreeNode nodeCF3A = null;
                nodeCF3A = recuperarNodoCalendarioGruposCF(nodeSF2A, calendarioFaseCFList, idNodeCF3A, nodeCF3A, true, false);
                nodeCF3A.setExpanded(true);
                
                String idNodeCF3B = "CFC";
                TreeNode nodeCF3B = null;
                nodeCF3B = recuperarNodoCalendarioGruposCF(nodeSF2A, calendarioFaseCFList, idNodeCF3B, nodeCF3B, false, true);
                nodeCF3B.setExpanded(true);
                
                
                
                String idNodeCF4A = "CFD";
                TreeNode nodeCF4A = null;
                nodeCF4A = recuperarNodoCalendarioGruposCF(nodeSF2B, calendarioFaseCFList, idNodeCF4A, nodeCF4A, true, false);
                nodeCF4A.setExpanded(true);
                
                String idNodeCF4B = "CFD";
                TreeNode nodeCF4B = null;
                nodeCF4B = recuperarNodoCalendarioGruposCF(nodeSF2B, calendarioFaseCFList, idNodeCF4B, nodeCF4B, false, true);
                nodeCF4B.setExpanded(true);
            }
            
            cuadro = nodeCampeonato;

            
    	}
    	
        return cuadro;
    }
    
	private TreeNode recuperarNodoCalendarioCampeon(TreeNode nodeTo, List<CalendarioFaseFF> calendarioFaseFFList,
			String idNode, TreeNode node) {
		for (CalendarioFaseFF calendarioFaseFF : calendarioFaseFFList) {
        	if( calendarioFaseFF.getJugador1Id() != null && calendarioFaseFF.getJugador1Id() > 0 && 
            		calendarioFaseFF.getJugador2Id() != null && calendarioFaseFF.getJugador2Id() > 0){
        			if(calendarioFaseFF.isGanaJugador() == 1){
        				node = new DefaultTreeNode(calendarioFaseFF.getJugador1().getNombreEquipoShowCuadro() + "!!! CAMPE�N !!!", node);
        			}else if(calendarioFaseFF.isGanaJugador() == 2){
        				node = new DefaultTreeNode(calendarioFaseFF.getJugador2().getNombreEquipoShowCuadro() + "!!! CAMPE�N !!!", node);
        			}
				}
		}
		if(node == null){
			node = new DefaultTreeNode(idNode, nodeTo);
		}
		return node;
	}
	
	private TreeNode recuperarNodoFinalistas(TreeNode nodeTo, List<CalendarioFaseSF> calendarioFaseSFList,
			String idNode, TreeNode node) {
	      for (CalendarioFaseSF calendarioFaseSF : calendarioFaseSFList) {
	    	if(calendarioFaseSF.getIdCruce().equals(idNode)){
	        	if( calendarioFaseSF.getJugador1Id() != null && calendarioFaseSF.getJugador1Id() > 0 && 
	        		calendarioFaseSF.getJugador2Id() != null && calendarioFaseSF.getJugador2Id() > 0){
	    			if(calendarioFaseSF.isGanaJugador() == 1){
	    				node = new DefaultTreeNode(calendarioFaseSF.getJugador1().getNombreEquipoShowCuadro(), nodeTo);
	    			}else if(calendarioFaseSF.isGanaJugador() == 2){
	    				node = new DefaultTreeNode(calendarioFaseSF.getJugador2().getNombreEquipoShowCuadro(), nodeTo);
	    			}
				}
	    	}
		}
		if(node == null){
			node = new DefaultTreeNode(idNode, nodeTo);
		}
		return node;
	}
	
	private TreeNode recuperarNodoSemiFinalistas(TreeNode nodeTo, List<CalendarioFaseCF> calendarioFaseCFList,
			String idNode, TreeNode node) {
	      for (CalendarioFaseCF calendarioFaseCF : calendarioFaseCFList) {
	    	if(calendarioFaseCF.getIdCruce().equals(idNode)){
	        	if( calendarioFaseCF.getJugador1Id() != null && calendarioFaseCF.getJugador1Id() > 0 && 
	        		calendarioFaseCF.getJugador2Id() != null && calendarioFaseCF.getJugador2Id() > 0){
	    			if(calendarioFaseCF.isGanaJugador() == 1){
	    				node = new DefaultTreeNode(calendarioFaseCF.getJugador1().getNombreEquipoShowCuadro(), nodeTo);
	    			}else if(calendarioFaseCF.isGanaJugador() == 2){
	    				node = new DefaultTreeNode(calendarioFaseCF.getJugador2().getNombreEquipoShowCuadro(), nodeTo);
	    			}
				}
	    	}
		}
		if(node == null){
			node = new DefaultTreeNode(idNode, nodeTo);
		}
		return node;
	}
	
	private TreeNode recuperarNodoCuartosFinal(TreeNode nodeTo, List<CalendarioFaseOF> calendarioFaseOFList,
			String idNode, TreeNode node) {
	      for (CalendarioFaseOF calendarioFaseOF : calendarioFaseOFList) {
	    	if(calendarioFaseOF.getIdCruce().equals(idNode)){
	        	if( calendarioFaseOF.getJugador1Id() != null && calendarioFaseOF.getJugador1Id() > 0 && 
	        		calendarioFaseOF.getJugador2Id() != null && calendarioFaseOF.getJugador2Id() > 0){
	    			if(calendarioFaseOF.isGanaJugador() == 1){
	    				node = new DefaultTreeNode(calendarioFaseOF.getJugador1().getNombreEquipoShowCuadro(), nodeTo);
	    			}else if(calendarioFaseOF.isGanaJugador() == 2){
	    				node = new DefaultTreeNode(calendarioFaseOF.getJugador2().getNombreEquipoShowCuadro(), nodeTo);
	    			}
				}
	    	}
		}
		if(node == null){
			node = new DefaultTreeNode(idNode, nodeTo);
		}
		return node;
	}
	
	private TreeNode recuperarNodoCalendarioGruposCF(TreeNode nodeTo, List<CalendarioFaseCF> calendarioFaseCFList,
			String idNode, TreeNode node, boolean jugador1, boolean jugador2) {
		for (CalendarioFaseCF calendarioFaseCF : calendarioFaseCFList) {
			if(jugador1){
				if(calendarioFaseCF.getIdCruce().equals(idNode)){
			    	if( calendarioFaseCF.getJugador1Id() != null && calendarioFaseCF.getJugador1Id() > 0){
						node = new DefaultTreeNode(calendarioFaseCF.getJugador1().getNombreEquipoShowCuadro(), nodeTo);
					}else{
						node = new DefaultTreeNode(calendarioFaseCF.getCruce1IdGrupo(), nodeTo);
					}					
				}
			}else if(jugador2){
				if(calendarioFaseCF.getIdCruce().equals(idNode)){
			    	if(calendarioFaseCF.getJugador2Id() != null && calendarioFaseCF.getJugador2Id() > 0){
						node = new DefaultTreeNode(calendarioFaseCF.getJugador2().getNombreEquipoShowCuadro(), nodeTo);
					}else{
						node = new DefaultTreeNode(calendarioFaseCF.getCruce2IdGrupo(), nodeTo);
					}
				}
			}
		}
		if(node == null){
			node = new DefaultTreeNode(idNode, nodeTo);
		}
		return node;
	}

	private TreeNode recuperarNodoCalendarioOF(TreeNode nodeTo, List<CalendarioFaseOF> calendarioFaseOFList,
			String idNode, TreeNode node, boolean jugador1, boolean jugador2) {
		for (CalendarioFaseOF calendarioFaseOF : calendarioFaseOFList) {
			if(jugador1){
				if(calendarioFaseOF.getIdCruce().equals(idNode)){
			    	if( calendarioFaseOF.getJugador1Id() != null && calendarioFaseOF.getJugador1Id() > 0){
						node = new DefaultTreeNode(calendarioFaseOF.getJugador1().getNombreEquipoShowCuadro(), nodeTo);
					}else{
						node = new DefaultTreeNode(calendarioFaseOF.getCruce1IdGrupo(), nodeTo);
					}					
				}
			}else if(jugador2){
				if(calendarioFaseOF.getIdCruce().equals(idNode)){
			    	if(calendarioFaseOF.getJugador2Id() != null && calendarioFaseOF.getJugador2Id() > 0){
						node = new DefaultTreeNode(calendarioFaseOF.getJugador2().getNombreEquipoShowCuadro(), nodeTo);
					}else{
						node = new DefaultTreeNode(calendarioFaseOF.getCruce2IdGrupo(), nodeTo);
					}
				}
			}
		}
		if(node == null){
			node = new DefaultTreeNode(idNode, nodeTo);
		}
		return node;
	}
    
    public String getPermisoGrupo(String grupo){
    	return "BCMEGEIFARG" + grupo;
    }
    
    public void doActualizarResultadoGrupos(CalendarioFaseI cf1){
    	
    	boolean error = false;
		if(cf1.isModificable()){
			
			Fases fase = getFase(FasesModelo.FASE_I);
			Integer numeroJuegosFase = fase.getNumeroJuegos();
			Integer numeroJuegosTotalFase = (numeroJuegosFase * 2) - 2;
			Integer numeroJuegosEmpate = numeroJuegosTotalFase / 2;
			
			//Validaciones
			if(cf1.getJuegosJugador1() > numeroJuegosFase || cf1.getJuegosJugador2() > numeroJuegosFase){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El maximo de juegos es " + numeroJuegosFase + "por jugador/partida.", null));
			}else if((cf1.getJuegosJugador1() + cf1.getJuegosJugador2()) > numeroJuegosTotalFase){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El maximo de juegos es para el enfrentamiento es " + numeroJuegosTotalFase + ".", null));
			}else if((cf1.getJuegosJugador1() + cf1.getJuegosJugador2()) < (numeroJuegosFase-1)){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El minimo de juegos es para el enfrentamiento es " + numeroJuegosFase + ".", null));
			}else if(cf1.getJuegosJugador1() < 0 || cf1.getJuegosJugador2() < 0){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No puede haber menos de 0 juegos", null));
			}else if((cf1.getJuegosJugador1() < (numeroJuegosEmpate-1) && cf1.getJuegosJugador2() < (numeroJuegosEmpate-1)) 
					|| (cf1.getJuegosJugador1() < (numeroJuegosEmpate-1) && cf1.getJuegosJugador2() < (numeroJuegosFase-1))
					|| (cf1.getJuegosJugador1() < (numeroJuegosFase-1) && cf1.getJuegosJugador2() < (numeroJuegosEmpate-1))){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Resultado incorrecto.", null));
			}
			
			//Si todo correcto actualizar resultado
			if(!error){
				ClasificacionFaseI clasificacionFaseIJugador1 = getResultListClasificacionFaseIByJugador(cf1.getGrupo(), cf1.getJugador1Id());
				ClasificacionFaseI clasificacionFaseIJugador2 = getResultListClasificacionFaseIByJugador(cf1.getGrupo(), cf1.getJugador2Id());
				crearCampeonatoFemeninoIndividual.actualizarResultadoCalendarioFaseI(cf1, clasificacionFaseIJugador1, clasificacionFaseIJugador2);
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha actualizado el resultado del Grupo " + cf1.getGrupo() + ".", null));
				
				leerCampeonatoFemeninoIndividual = new LeerCampeonatoFemeninoIndividual();
				
				//Si todos los resultados han sido modificados clasificar a siguiente fase
				clasificarSiguienteRonda(FasesModelo.FASE_I, cf1.getGrupo());

			}
		}
		
	}
    
    public void clasificarSiguienteRonda(Integer numerofaseActual, String grupo){
    	Fases faseActual = buscarSiguiente(numerofaseActual);
    	Fases faseSiguiente = buscarSiguiente(faseActual.getFaseSiguiente());
    	
    	if(grupo != null){
	    	if(faseSiguiente.getNumero().equals(FasesModelo.FASE_OF) && faseActual.getNumero().equals(FasesModelo.FASE_I)){
	    		boolean faseFinalizada = getResultListCalendarioFaseIByGrupoFinalizado(grupo);
	    		if(faseFinalizada){
	    			doClasificarToOctavosFinal(grupo);
	    		}
	    	}else if(faseSiguiente.getNumero().equals(FasesModelo.FASE_CF) && faseActual.getNumero().equals(FasesModelo.FASE_I)){
	    		boolean faseFinalizada = getResultListCalendarioFaseIByGrupoFinalizado(grupo);
	    		if(faseFinalizada){
	    			doClasificarToCuartosFinal(grupo);
	    		}
	    	}
    	}else if(grupo == null){
    		if(faseSiguiente.getNumero().equals(FasesModelo.FASE_CF) && faseActual.getNumero().equals(FasesModelo.FASE_OF)){
    			doClasificarToCuartosFinal();
	    	}else if(faseSiguiente.getNumero().equals(FasesModelo.FASE_SF) && faseActual.getNumero().equals(FasesModelo.FASE_CF)){
	    		doClasificarToSemifinal();
	    	}else if(faseSiguiente.getNumero().equals(FasesModelo.FASE_FC) && faseActual.getNumero().equals(FasesModelo.FASE_SF)){
	    		doClasificarToFinalConsolacion();
	    	}else if(faseSiguiente.getNumero().equals(FasesModelo.FASE_FF) && faseActual.getNumero().equals(FasesModelo.FASE_SF)){
	    		doClasificarToFinal();
	    	}
    	}

    }
    
    public Fases buscarSiguiente(Integer faseBuscar){
		for(Fases fase : getResultListFases()){
			if(fase.getActivo().equals(Activo.SI)){
				if(fase.getNumero().equals(faseBuscar)){
					return fase; 
				}else if(fase.getNumero().equals(faseBuscar)){
					//De momento nada
				}else if(fase.getNumero().equals(faseBuscar)){
					return fase;
				}else if(fase.getNumero().equals(faseBuscar)){
					return fase;
				}else if(fase.getNumero().equals(faseBuscar)){
					return fase;
				}else if(fase.getNumero().equals(faseBuscar)){
					return fase;
				}else if(fase.getNumero().equals(faseBuscar)){
					return fase;
				}
			}
		}
		return null;
    }
    
    public void doClasificarToOctavosFinal(String grupo){
    	//Recuperar la clasificacion del grupo
    	List<ClasificacionFaseI> clasificacionGrupoList = getResultListClasificacionFaseIByGrupo(grupo);
    	//Recuperar los octavos de final
    	List<CalendarioFaseOF> octavosFinalCalendarioList = getResultListCalendarioFaseOF();
    	//Buscar en la fase de octavos de final la posicion de los clasificados del grupo y asignarlos 
    	for (CalendarioFaseOF calendarioFaseOF : octavosFinalCalendarioList) {
			if(calendarioFaseOF.getGrupoProcedenciaJugador1().equalsIgnoreCase(grupo)){
				ClasificacionFaseI clasificadoPosicion = clasificacionGrupoList.get(calendarioFaseOF.getPosicionProcedenciaJugador1()-1);
				calendarioFaseOF.setJugador1Id(clasificadoPosicion.getJugadorId());
				calendarioFaseOF.setJugador1(clasificadoPosicion.getJugador());
				crearCampeonatoFemeninoIndividual.actualizarJugadorOctavosFinal(calendarioFaseOF, true, false);
			}
			if(calendarioFaseOF.getGrupoProcedenciaJugador2().equalsIgnoreCase(grupo)){
				ClasificacionFaseI clasificadoPosicion = clasificacionGrupoList.get(calendarioFaseOF.getPosicionProcedenciaJugador2()-1);
				calendarioFaseOF.setJugador2Id(clasificadoPosicion.getJugadorId());
				calendarioFaseOF.setJugador2(clasificadoPosicion.getJugador());
				crearCampeonatoFemeninoIndividual.actualizarJugadorOctavosFinal(calendarioFaseOF, false, true);
			}
		}
    	
    	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Clasificados a Octavos de Final jugadores del Grupo " + grupo + ".", null));
    	
    	
    }
    
    public void doClasificarToCuartosFinal(){
    	
    	//Recuperar la clasificacion de los octavos de final
    	List<CalendarioFaseOF> octavosFinalCalendarioList = getResultListCalendarioFaseOF();
    	//Recuperar los cuartos de final
    	List<CalendarioFaseCF> cuartosFinalCalendarioList = getResultListCalendarioFaseCF();
    	//Buscar en la fase de cuartos de final la posicion de los clasificados del grupo y asignarlos 
    	for (CalendarioFaseCF calendarioFaseCF : cuartosFinalCalendarioList) {
    		for (CalendarioFaseOF calendarioFaseOF : octavosFinalCalendarioList) {
				if(calendarioFaseCF.getGrupoProcedenciaJugador1().equalsIgnoreCase(calendarioFaseOF.getIdCruce())){
					if(calendarioFaseOF.isGanaJugador() == 1){
						calendarioFaseCF.setJugador1Id(calendarioFaseOF.getJugador1Id());
						calendarioFaseCF.setJugador1(calendarioFaseOF.getJugador1());
						crearCampeonatoFemeninoIndividual.actualizarJugadorCuartosFinal(calendarioFaseCF, true, false);
					}else if(calendarioFaseOF.isGanaJugador() == 2){
						calendarioFaseCF.setJugador1Id(calendarioFaseOF.getJugador2Id());
						calendarioFaseCF.setJugador1(calendarioFaseOF.getJugador2());
						crearCampeonatoFemeninoIndividual.actualizarJugadorCuartosFinal(calendarioFaseCF, true, false);
					}
				}else  if(calendarioFaseCF.getGrupoProcedenciaJugador2().equalsIgnoreCase(calendarioFaseOF.getIdCruce())){
					if(calendarioFaseOF.isGanaJugador() == 1){
						calendarioFaseCF.setJugador2Id(calendarioFaseOF.getJugador1Id());
						calendarioFaseCF.setJugador2(calendarioFaseOF.getJugador1());
						crearCampeonatoFemeninoIndividual.actualizarJugadorCuartosFinal(calendarioFaseCF, false, true);
					}else if(calendarioFaseOF.isGanaJugador() == 2){
						calendarioFaseCF.setJugador2Id(calendarioFaseOF.getJugador2Id());
						calendarioFaseCF.setJugador2(calendarioFaseOF.getJugador2());
						crearCampeonatoFemeninoIndividual.actualizarJugadorCuartosFinal(calendarioFaseCF, false, true);
					}
				}
    		}
		}
    	
    	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Clasificados a Cuartos de Final jugadores de los Octavos de Final.", null));
    	
    }
    
    public void doClasificarToCuartosFinal(String grupo){
    	
     	//Recuperar la clasificacion del grupo
    	List<ClasificacionFaseI> clasificacionGrupoList = getResultListClasificacionFaseIByGrupo(grupo);
    	//Recuperar los cuartos de final
    	List<CalendarioFaseCF> cuartosFinalCalendarioList = getResultListCalendarioFaseCF();
    	//Buscar en la fase de cuartos de final la posicion de los clasificados del grupo y asignarlos 
    	for (CalendarioFaseCF calendarioFaseCF : cuartosFinalCalendarioList) {
			if(calendarioFaseCF.getGrupoProcedenciaJugador1().equalsIgnoreCase(grupo)){
				ClasificacionFaseI clasificadoPosicion = clasificacionGrupoList.get(calendarioFaseCF.getPosicionProcedenciaJugador1()-1);
				calendarioFaseCF.setJugador1Id(clasificadoPosicion.getJugadorId());
				calendarioFaseCF.setJugador1(clasificadoPosicion.getJugador());
				crearCampeonatoFemeninoIndividual.actualizarJugadorCuartosFinal(calendarioFaseCF, true, false);
			}
			if(calendarioFaseCF.getGrupoProcedenciaJugador2().equalsIgnoreCase(grupo)){
				ClasificacionFaseI clasificadoPosicion = clasificacionGrupoList.get(calendarioFaseCF.getPosicionProcedenciaJugador2()-1);
				calendarioFaseCF.setJugador2Id(clasificadoPosicion.getJugadorId());
				calendarioFaseCF.setJugador2(clasificadoPosicion.getJugador());
				crearCampeonatoFemeninoIndividual.actualizarJugadorCuartosFinal(calendarioFaseCF, false, true);
			}
		}
    	
    	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Clasificados a Cuartos de Final jugadores del Grupo " + grupo + ".", null));
    	
    }
    
   public void doClasificarToSemifinal(){
    	
    	//Recuperar la clasificacion de los cuartos de final
    	List<CalendarioFaseCF> cuartosFinalCalendarioList = getResultListCalendarioFaseCF();
    	//Recuperar los semifinales
    	List<CalendarioFaseSF> semifinalCalendarioList = getResultListCalendarioFaseSF();
    	//Buscar en la fase de semifinal la posicion de los clasificados del grupo y asignarlos 
    	for (CalendarioFaseSF calendarioFaseSF : semifinalCalendarioList) {
    		for (CalendarioFaseCF calendarioFaseCF : cuartosFinalCalendarioList) {
				if(calendarioFaseSF.getGrupoProcedenciaJugador1().equalsIgnoreCase(calendarioFaseCF.getIdCruce())){
					if(calendarioFaseCF.isGanaJugador() == 1){
						calendarioFaseSF.setJugador1Id(calendarioFaseCF.getJugador1Id());
						calendarioFaseSF.setJugador1(calendarioFaseCF.getJugador1());
						crearCampeonatoFemeninoIndividual.actualizarJugadorSemiFinal(calendarioFaseSF, true, false);
					}else if(calendarioFaseCF.isGanaJugador() == 2){
						calendarioFaseSF.setJugador1Id(calendarioFaseCF.getJugador2Id());
						calendarioFaseSF.setJugador1(calendarioFaseCF.getJugador2());
						crearCampeonatoFemeninoIndividual.actualizarJugadorSemiFinal(calendarioFaseSF, true, false);
					}
				}else  if(calendarioFaseSF.getGrupoProcedenciaJugador2().equalsIgnoreCase(calendarioFaseCF.getIdCruce())){
					if(calendarioFaseCF.isGanaJugador() == 1){
						calendarioFaseSF.setJugador2Id(calendarioFaseCF.getJugador1Id());
						calendarioFaseSF.setJugador2(calendarioFaseCF.getJugador1());
						crearCampeonatoFemeninoIndividual.actualizarJugadorSemiFinal(calendarioFaseSF, false, true);
					}else if(calendarioFaseCF.isGanaJugador() == 2){
						calendarioFaseSF.setJugador2Id(calendarioFaseCF.getJugador2Id());
						calendarioFaseSF.setJugador2(calendarioFaseCF.getJugador2());
						crearCampeonatoFemeninoIndividual.actualizarJugadorSemiFinal(calendarioFaseSF, false, true);
					}
				}
    		}
		}
    	
    	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Clasificados a Semifinales jugadores de los Cuartos de Final.", null));
    	
    }
    
    public void doClasificarToSemifinal(String grupo){
    	

    	//Recuperar la clasificacion del grupo
    	List<ClasificacionFaseI> clasificacionGrupoList = getResultListClasificacionFaseIByGrupo(grupo);
    	//Recuperar los semifinal
    	List<CalendarioFaseSF> semifinalCalendarioList = getResultListCalendarioFaseSF();
    	//Buscar en la fase de semifinal la posicion de los clasificados del grupo y asignarlos 
    	for (CalendarioFaseSF calendarioFaseSF : semifinalCalendarioList) {
			if(calendarioFaseSF.getGrupoProcedenciaJugador1().equalsIgnoreCase(grupo)){
				ClasificacionFaseI clasificadoPosicion = clasificacionGrupoList.get(calendarioFaseSF.getPosicionProcedenciaJugador1()-1);
				calendarioFaseSF.setJugador1Id(clasificadoPosicion.getJugadorId());
				calendarioFaseSF.setJugador1(clasificadoPosicion.getJugador());
				crearCampeonatoFemeninoIndividual.actualizarJugadorSemiFinal(calendarioFaseSF, true, false);
			}
			if(calendarioFaseSF.getGrupoProcedenciaJugador2().equalsIgnoreCase(grupo)){
				ClasificacionFaseI clasificadoPosicion = clasificacionGrupoList.get(calendarioFaseSF.getPosicionProcedenciaJugador2()-1);
				calendarioFaseSF.setJugador2Id(clasificadoPosicion.getJugadorId());
				calendarioFaseSF.setJugador2(clasificadoPosicion.getJugador());
				crearCampeonatoFemeninoIndividual.actualizarJugadorSemiFinal(calendarioFaseSF, false, true);
			}
		}
    	
    	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Clasificados a Semifinales jugadores del Grupo " + grupo + ".", null));
    	
    }
    
    public void doClasificarToFinalConsolacion(){
    	
    	//Recuperar la clasificacion de las semifinales
    	List<CalendarioFaseSF> semifinalCalendarioList = getResultListCalendarioFaseSF();
    	//Recuperar los semifinales
    	List<CalendarioFaseFC> finalConsolacionCalendarioList = getResultListCalendarioFaseFC();
    	//Buscar en la fase de semifinal la posicion de los clasificados del grupo y asignarlos 
    	for (CalendarioFaseFC calendarioFaseFC : finalConsolacionCalendarioList) {
    		for (CalendarioFaseSF calendarioFaseSF : semifinalCalendarioList) {
				if(calendarioFaseFC.getGrupoProcedenciaJugador1().equalsIgnoreCase(calendarioFaseSF.getIdCruce())){
					if(calendarioFaseSF.isGanaJugador() == 1){
						calendarioFaseFC.setJugador1Id(calendarioFaseSF.getJugador2Id());
						calendarioFaseFC.setJugador1(calendarioFaseSF.getJugador2());
						crearCampeonatoFemeninoIndividual.actualizarJugadorFinalConsolacion(calendarioFaseFC, true, false);
					}else if(calendarioFaseSF.isGanaJugador() == 2){
						calendarioFaseFC.setJugador1Id(calendarioFaseSF.getJugador1Id());
						calendarioFaseFC.setJugador1(calendarioFaseSF.getJugador1());
						crearCampeonatoFemeninoIndividual.actualizarJugadorFinalConsolacion(calendarioFaseFC, true, false);
					}
				}else  if(calendarioFaseFC.getGrupoProcedenciaJugador2().equalsIgnoreCase(calendarioFaseSF.getIdCruce())){
					if(calendarioFaseSF.isGanaJugador() == 1){
						calendarioFaseFC.setJugador2Id(calendarioFaseSF.getJugador2Id());
						calendarioFaseFC.setJugador2(calendarioFaseSF.getJugador2());
						crearCampeonatoFemeninoIndividual.actualizarJugadorFinalConsolacion(calendarioFaseFC, false, true);
					}else if(calendarioFaseSF.isGanaJugador() == 2){
						calendarioFaseFC.setJugador2Id(calendarioFaseSF.getJugador1Id());
						calendarioFaseFC.setJugador2(calendarioFaseSF.getJugador1());
						crearCampeonatoFemeninoIndividual.actualizarJugadorFinalConsolacion(calendarioFaseFC, false, true);
					}
				}
    		}
		}	
    	
    	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Clasificados a Final de Consolacion jugadores de las Semifinales.", null));
    }
    
    public void doClasificarToFinal(){
    	
    	//Recuperar la clasificacion de las semifinales
    	List<CalendarioFaseSF> semifinalCalendarioList = getResultListCalendarioFaseSF();
    	//Recuperar los semifinales
    	List<CalendarioFaseFF> finalCalendarioList = getResultListCalendarioFaseFF();
    	//Buscar en la fase de semifinal la posicion de los clasificados del grupo y asignarlos 
    	for (CalendarioFaseFF calendarioFaseFF : finalCalendarioList) {
    		for (CalendarioFaseSF calendarioFaseSF : semifinalCalendarioList) {
				if(calendarioFaseFF.getGrupoProcedenciaJugador1().equalsIgnoreCase(calendarioFaseSF.getIdCruce())){
					if(calendarioFaseSF.isGanaJugador() == 1){
						calendarioFaseFF.setJugador1Id(calendarioFaseSF.getJugador1Id());
						calendarioFaseFF.setJugador1(calendarioFaseSF.getJugador1());
						crearCampeonatoFemeninoIndividual.actualizarJugadorFinal(calendarioFaseFF, true, false);
					}else if(calendarioFaseSF.isGanaJugador() == 2){
						calendarioFaseFF.setJugador1Id(calendarioFaseSF.getJugador2Id());
						calendarioFaseFF.setJugador1(calendarioFaseSF.getJugador2());
						crearCampeonatoFemeninoIndividual.actualizarJugadorFinal(calendarioFaseFF, true, false);
					}
				}else  if(calendarioFaseFF.getGrupoProcedenciaJugador2().equalsIgnoreCase(calendarioFaseSF.getIdCruce())){
					if(calendarioFaseSF.isGanaJugador() == 1){
						calendarioFaseFF.setJugador2Id(calendarioFaseSF.getJugador1Id());
						calendarioFaseFF.setJugador2(calendarioFaseSF.getJugador1());
						crearCampeonatoFemeninoIndividual.actualizarJugadorFinal(calendarioFaseFF, false, true);
					}else if(calendarioFaseSF.isGanaJugador() == 2){
						calendarioFaseFF.setJugador2Id(calendarioFaseSF.getJugador2Id());
						calendarioFaseFF.setJugador2(calendarioFaseSF.getJugador2());
						crearCampeonatoFemeninoIndividual.actualizarJugadorFinal(calendarioFaseFF, false, true);
					}
				}
    		}
		}
    	
    	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Clasificados a Final jugadores de las Semifinales.", null));
    }
    
    
    
    public void doActualizarResultadosEnfrentamientoDirecto(Integer fase, Object calendarioFase){
    	if(fase.equals(FasesModelo.FASE_OF)){
    		CalendarioFaseOF calendarioFaseOF = (CalendarioFaseOF) calendarioFase;
    		
        	boolean error = false;
        	if(calendarioFaseOF.isModificable() && calendarioFaseOF.isJugadoresEnFase()){
    			
    			Fases faseObject = getFase(fase);
    			
    			Integer numeroPartidasFase = faseObject.getNumeroPartidas();
    			
    			Integer numeroPartidasFaseMax = numeroPartidasFase;
    			
    			if(numeroPartidasFase > 1){
    				numeroPartidasFaseMax = numeroPartidasFase + 1;
    			}
    			
    			Integer partidasJugadas = calendarioFaseOF.getPartidasJugadas();
    			
    			Integer numeroJuegosFase = faseObject.getNumeroJuegos();
    			Integer numeroJuegosTotalFase = (numeroJuegosFase * 2) - 1;
    			Integer numeroJuegosEmpate = numeroJuegosTotalFase / 2;
    			
    			boolean resultadoPartidas1 = calendarioFaseOF.isResultadoPartidas1();
    			boolean resultadoPartidas2 = calendarioFaseOF.isResultadoPartidas2();
    			
    			boolean terceraPartidaJugada = calendarioFaseOF.isPartida3Jugada();
    			
    			Integer juegosJugador1P1 = calendarioFaseOF.getJuegosJugador1P1();
    			Integer juegosJugador1P2 = calendarioFaseOF.getJuegosJugador1P2();
    			Integer juegosJugador1P3 = calendarioFaseOF.getJuegosJugador1P3();
    			Integer juegosJugador2P1 = calendarioFaseOF.getJuegosJugador2P1();
    			Integer juegosJugador2P2 = calendarioFaseOF.getJuegosJugador2P2();
    			Integer juegosJugador2P3 = calendarioFaseOF.getJuegosJugador2P3();
    			
    			error = validacionesResultadoOFtoFF(error, numeroPartidasFase, partidasJugadas,
						numeroJuegosFase, numeroJuegosTotalFase, numeroJuegosEmpate, resultadoPartidas1, resultadoPartidas2,
						juegosJugador1P1, juegosJugador1P2, juegosJugador1P3, juegosJugador2P1,
						juegosJugador2P2, juegosJugador2P3, numeroPartidasFaseMax, terceraPartidaJugada);
    			
    			//Si todo correcto actualizar resultado
    			if(!error){
    	    		crearCampeonatoFemeninoIndividual.actualizarResultadosEnfrentamientoDirectoOF(calendarioFaseOF);
    	    		clasificarSiguienteRonda(FasesModelo.FASE_OF, null);
    	    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha actualizado el resultado de Octavos de Final.", null));
    	    		leerCampeonatoFemeninoIndividual = new LeerCampeonatoFemeninoIndividual();
    			}
    		}else if(!calendarioFaseOF.isJugadoresEnFase()){
    			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede modificar el resultado, no hay jugadores clasificados para la fase.", null));
    		}
    		
    	}else if(fase.equals(FasesModelo.FASE_CF)){
    		CalendarioFaseCF calendarioFaseCF = (CalendarioFaseCF) calendarioFase;
    		
        	boolean error = false;
        	if(calendarioFaseCF.isModificable() && calendarioFaseCF.isJugadoresEnFase()){
    			
    			Fases faseObject = getFase(fase);
    			
    			Integer numeroPartidasFase = faseObject.getNumeroPartidas();
    			
    			Integer numeroPartidasFaseMax = numeroPartidasFase;
    			
    			if(numeroPartidasFase > 1){
    				numeroPartidasFaseMax = numeroPartidasFase + 1;
    			}
    			
    			Integer partidasJugadas = calendarioFaseCF.getPartidasJugadas();
    			
    			Integer numeroJuegosFase = faseObject.getNumeroJuegos();
    			Integer numeroJuegosTotalFase = (numeroJuegosFase * 2) - 1;
    			Integer numeroJuegosEmpate = numeroJuegosTotalFase / 2;
    			
    			boolean resultadoPartidas1 = calendarioFaseCF.isResultadoPartidas1();
    			boolean resultadoPartidas2 = calendarioFaseCF.isResultadoPartidas2();
    			
    			boolean terceraPartidaJugada = calendarioFaseCF.isPartida3Jugada();
    			
    			Integer juegosJugador1P1 = calendarioFaseCF.getJuegosJugador1P1();
    			Integer juegosJugador1P2 = calendarioFaseCF.getJuegosJugador1P2();
    			Integer juegosJugador1P3 = calendarioFaseCF.getJuegosJugador1P3();
    			Integer juegosJugador2P1 = calendarioFaseCF.getJuegosJugador2P1();
    			Integer juegosJugador2P2 = calendarioFaseCF.getJuegosJugador2P2();
    			Integer juegosJugador2P3 = calendarioFaseCF.getJuegosJugador2P3();
    			
    			error = validacionesResultadoOFtoFF(error, numeroPartidasFase, partidasJugadas,
						numeroJuegosFase, numeroJuegosTotalFase, numeroJuegosEmpate, resultadoPartidas1, resultadoPartidas2,
						juegosJugador1P1, juegosJugador1P2, juegosJugador1P3, juegosJugador2P1,
						juegosJugador2P2, juegosJugador2P3, numeroPartidasFaseMax, terceraPartidaJugada);
    			
    			//Si todo correcto actualizar resultado
    			if(!error){
    	    		crearCampeonatoFemeninoIndividual.actualizarResultadosEnfrentamientoDirectoCF(calendarioFaseCF);
    	    		clasificarSiguienteRonda(FasesModelo.FASE_CF, null);
    	    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha actualizado el resultado de Cuartos de Final.", null));
    	    		leerCampeonatoFemeninoIndividual = new LeerCampeonatoFemeninoIndividual();
    			}
    		}else if(!calendarioFaseCF.isJugadoresEnFase()){
    			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede modificar el resultado, no hay jugadores clasificados para la fase.", null));
    		}
    		
    	}else if(fase.equals(FasesModelo.FASE_SF)){
    		CalendarioFaseSF calendarioFaseSF = (CalendarioFaseSF) calendarioFase;
    		
        	boolean error = false;
        	if(calendarioFaseSF.isModificable() && calendarioFaseSF.isJugadoresEnFase()){
    			
    			Fases faseObject = getFase(fase);
    			
    			Integer numeroPartidasFase = faseObject.getNumeroPartidas();
    			
    			Integer numeroPartidasFaseMax = numeroPartidasFase;
    			
    			if(numeroPartidasFase > 1){
    				numeroPartidasFaseMax = numeroPartidasFase + 1;
    			}
    			
    			Integer partidasJugadas = calendarioFaseSF.getPartidasJugadas();
    			
    			Integer numeroJuegosFase = faseObject.getNumeroJuegos();
    			Integer numeroJuegosTotalFase = (numeroJuegosFase * 2) - 1;
    			Integer numeroJuegosEmpate = numeroJuegosTotalFase / 2;
    			
    			boolean resultadoPartidas1 = calendarioFaseSF.isResultadoPartidas1();
    			boolean resultadoPartidas2 = calendarioFaseSF.isResultadoPartidas2();
    			
    			boolean terceraPartidaJugada = calendarioFaseSF.isPartida3Jugada();
    			
    			Integer juegosJugador1P1 = calendarioFaseSF.getJuegosJugador1P1();
    			Integer juegosJugador1P2 = calendarioFaseSF.getJuegosJugador1P2();
    			Integer juegosJugador1P3 = calendarioFaseSF.getJuegosJugador1P3();
    			Integer juegosJugador2P1 = calendarioFaseSF.getJuegosJugador2P1();
    			Integer juegosJugador2P2 = calendarioFaseSF.getJuegosJugador2P2();
    			Integer juegosJugador2P3 = calendarioFaseSF.getJuegosJugador2P3();
    			
    			error = validacionesResultadoOFtoFF(error, numeroPartidasFase, partidasJugadas,
						numeroJuegosFase, numeroJuegosTotalFase, numeroJuegosEmpate, resultadoPartidas1, resultadoPartidas2,
						juegosJugador1P1, juegosJugador1P2, juegosJugador1P3, juegosJugador2P1,
						juegosJugador2P2, juegosJugador2P3, numeroPartidasFaseMax, terceraPartidaJugada);
    			
    			//Si todo correcto actualizar resultado
    			if(!error){
    				crearCampeonatoFemeninoIndividual.actualizarResultadosEnfrentamientoDirectoSF(calendarioFaseSF);
    	    		clasificarSiguienteRonda(FasesModelo.FASE_SF, null);
    	    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha actualizado el resultado de Semifinales.", null));
    	    		leerCampeonatoFemeninoIndividual = new LeerCampeonatoFemeninoIndividual();
    			}
    		}else if(!calendarioFaseSF.isJugadoresEnFase()){
    			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede modificar el resultado, no hay jugadores clasificados para la fase.", null));
    		}
    		
    	}else if(fase.equals(FasesModelo.FASE_FC)){
    		CalendarioFaseFC calendarioFaseFC = (CalendarioFaseFC) calendarioFase;
    		
        	boolean error = false;
        	if(calendarioFaseFC.isModificable() && calendarioFaseFC.isJugadoresEnFase()){
    			
    			Fases faseObject = getFase(fase);
    			
    			Integer numeroPartidasFase = faseObject.getNumeroPartidas();
    			
    			Integer numeroPartidasFaseMax = numeroPartidasFase;
    			
    			if(numeroPartidasFase > 1){
    				numeroPartidasFaseMax = numeroPartidasFase + 1;
    			}
    			
    			Integer partidasJugadas = calendarioFaseFC.getPartidasJugadas();
    			
    			Integer numeroJuegosFase = faseObject.getNumeroJuegos();
    			Integer numeroJuegosTotalFase = (numeroJuegosFase * 2) - 1;
    			Integer numeroJuegosEmpate = numeroJuegosTotalFase / 2;
    			
    			boolean resultadoPartidas1 = calendarioFaseFC.isResultadoPartidas1();
    			boolean resultadoPartidas2 = calendarioFaseFC.isResultadoPartidas2();
    			
    			boolean terceraPartidaJugada = calendarioFaseFC.isPartida3Jugada();
    			
    			Integer juegosJugador1P1 = calendarioFaseFC.getJuegosJugador1P1();
    			Integer juegosJugador1P2 = calendarioFaseFC.getJuegosJugador1P2();
    			Integer juegosJugador1P3 = calendarioFaseFC.getJuegosJugador1P3();
    			Integer juegosJugador2P1 = calendarioFaseFC.getJuegosJugador2P1();
    			Integer juegosJugador2P2 = calendarioFaseFC.getJuegosJugador2P2();
    			Integer juegosJugador2P3 = calendarioFaseFC.getJuegosJugador2P3();
    			
    			error = validacionesResultadoOFtoFF(error, numeroPartidasFase, partidasJugadas,
						numeroJuegosFase, numeroJuegosTotalFase, numeroJuegosEmpate, resultadoPartidas1, resultadoPartidas2,
						juegosJugador1P1, juegosJugador1P2, juegosJugador1P3, juegosJugador2P1,
						juegosJugador2P2, juegosJugador2P3, numeroPartidasFaseMax, terceraPartidaJugada);
    			
    			//Si todo correcto actualizar resultado
    			if(!error){
    				crearCampeonatoFemeninoIndividual.actualizarResultadosEnfrentamientoDirectoFC(calendarioFaseFC);
    	    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha actualizado el resultado de Semifinales.", null));
    	    		leerCampeonatoFemeninoIndividual = new LeerCampeonatoFemeninoIndividual();
    			}
    		}else if(!calendarioFaseFC.isJugadoresEnFase()){
    			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede modificar el resultado, no hay jugadores clasificados para la fase.", null));
    		}
    		
    		
    	}else if(fase.equals(FasesModelo.FASE_FF)){
    		CalendarioFaseFF calendarioFaseFF = (CalendarioFaseFF) calendarioFase;
    		
        	boolean error = false;
        	if(calendarioFaseFF.isModificable() && calendarioFaseFF.isJugadoresEnFase()){
    			
    			Fases faseObject = getFase(fase);
    			
    			Integer numeroPartidasFase = faseObject.getNumeroPartidas();
    			
    			Integer numeroPartidasFaseMax = numeroPartidasFase;
    			
    			if(numeroPartidasFase > 1){
    				numeroPartidasFaseMax = numeroPartidasFase + 1;
    			}
    			
    			Integer partidasJugadas = calendarioFaseFF.getPartidasJugadas();
    			
    			Integer numeroJuegosFase = faseObject.getNumeroJuegos();
    			Integer numeroJuegosTotalFase = (numeroJuegosFase * 2) - 1;
    			Integer numeroJuegosEmpate = numeroJuegosTotalFase / 2;
    			
    			boolean resultadoPartidas1 = calendarioFaseFF.isResultadoPartidas1();
    			boolean resultadoPartidas2 = calendarioFaseFF.isResultadoPartidas2();
    			
    			boolean terceraPartidaJugada = calendarioFaseFF.isPartida3Jugada();
    			
    			Integer juegosJugador1P1 = calendarioFaseFF.getJuegosJugador1P1();
    			Integer juegosJugador1P2 = calendarioFaseFF.getJuegosJugador1P2();
    			Integer juegosJugador1P3 = calendarioFaseFF.getJuegosJugador1P3();
    			Integer juegosJugador2P1 = calendarioFaseFF.getJuegosJugador2P1();
    			Integer juegosJugador2P2 = calendarioFaseFF.getJuegosJugador2P2();
    			Integer juegosJugador2P3 = calendarioFaseFF.getJuegosJugador2P3();
    			
    			error = validacionesResultadoOFtoFF(error, numeroPartidasFase, partidasJugadas,
						numeroJuegosFase, numeroJuegosTotalFase, numeroJuegosEmpate, resultadoPartidas1, resultadoPartidas2,
						juegosJugador1P1, juegosJugador1P2, juegosJugador1P3, juegosJugador2P1,
						juegosJugador2P2, juegosJugador2P3, numeroPartidasFaseMax, terceraPartidaJugada);
    			
    			//Si todo correcto actualizar resultado
    			if(!error){
    	    		crearCampeonatoFemeninoIndividual.actualizarResultadosEnfrentamientoDirectoFF(calendarioFaseFF);
    	    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha actualizado el resultado de Final.", null));
    	    		leerCampeonatoFemeninoIndividual = new LeerCampeonatoFemeninoIndividual();
    			}
    		}else if(!calendarioFaseFF.isJugadoresEnFase()){
    			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede modificar el resultado, no hay jugadores clasificados para la fase.", null));
    		}

    	}

	}

	private boolean validacionesResultadoOFtoFF(boolean error,
			Integer numeroPartidasFase, Integer partidasJugadas, Integer numeroJuegosFase,
			Integer numeroJuegosTotalFase, Integer numeroJuegosEmpate, 
			boolean resultadoPartidas1, boolean resultadoPartidas2, 
			Integer juegosJugador1P1, Integer juegosJugador1P2, Integer juegosJugador1P3, 
			Integer juegosJugador2P1, Integer juegosJugador2P2, Integer juegosJugador2P3,
			Integer numeroPartidasFaseMax, boolean terceraPartidaJugada) {
		//Validaciones
		if(partidasJugadas < numeroPartidasFase || (partidasJugadas > numeroPartidasFase && partidasJugadas < numeroPartidasFaseMax)){
			error = true;
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El numero de partidas a jugar es " + numeroPartidasFase + " (ganadas).", null));
		}else if(numeroPartidasFase == 1){
			if(!resultadoPartidas1){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ha de indicar el resultado de la partida 1 o el resultado es incorrecto.", null));
			}else if(juegosJugador1P1 > numeroJuegosFase || juegosJugador2P1 > numeroJuegosFase){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El maximo de juegos de la partida 1 es " + numeroJuegosFase + " por jugador/partida.", null));
			}else if((juegosJugador1P1 + juegosJugador2P1) > numeroJuegosTotalFase){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El maximo de juegos de la partida 1 para el enfrentamiento es " + numeroJuegosTotalFase + ".", null));
			}else if((juegosJugador1P1 + juegosJugador2P1) < numeroJuegosFase){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El minimo de juegos de la partida 1 es para el enfrentamiento es " + numeroJuegosFase + ".", null));
			}else if(juegosJugador1P1 < 0 || juegosJugador2P1 < 0){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No puede haber menos de 0 juegos en la partida 1", null));
			}else if((juegosJugador1P1 < numeroJuegosEmpate && juegosJugador2P1 < numeroJuegosEmpate) 
					|| (juegosJugador1P1 < numeroJuegosEmpate && juegosJugador2P1 < numeroJuegosFase)
					|| (juegosJugador1P1 < numeroJuegosFase && juegosJugador2P1 < numeroJuegosEmpate)){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Resultado incorrecto en partida 1.", null));
			}else if((juegosJugador1P2 > 0 && juegosJugador2P2 > 0) 
					|| (juegosJugador1P3 > 0 && juegosJugador2P3 > 0)){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se pueden indicar valores en las partidas 2 y 3.", null));
			}
		}else if(numeroPartidasFase == 2){
			
			if(!resultadoPartidas2){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ha de indicar el resultado de la partida 1, 2 y 3 o el resultado es incorrecto. (2 partidas ganadas).", null));
			}else if(juegosJugador1P1 > numeroJuegosFase || juegosJugador2P1 > numeroJuegosFase){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El maximo de juegos de la partida 1 es " + numeroJuegosFase + " por jugador/partida.", null));
			}else if((juegosJugador1P1 + juegosJugador2P1) > numeroJuegosTotalFase){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El maximo de juegos de la partida 1 para el enfrentamiento es " + numeroJuegosTotalFase + ".", null));
			}else if((juegosJugador1P1 + juegosJugador2P1) < numeroJuegosFase){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El minimo de juegos de la partida 1 es para el enfrentamiento es " + numeroJuegosFase + ".", null));
			}else if(juegosJugador1P1 < 0 || juegosJugador2P1 < 0){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No puede haber menos de 0 juegos en la partida 1", null));
			}else if((juegosJugador1P1 < numeroJuegosEmpate && juegosJugador2P1 < numeroJuegosEmpate) 
					|| (juegosJugador1P1 < numeroJuegosEmpate && juegosJugador2P1 < numeroJuegosFase)
					|| (juegosJugador1P1 < numeroJuegosFase && juegosJugador2P1 < numeroJuegosEmpate)){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Resultado incorrecto en partida 1.", null));
			}else if(juegosJugador1P2 > numeroJuegosFase || juegosJugador2P2 > numeroJuegosFase){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El maximo de juegos de la partida 2 es " + numeroJuegosFase + " por jugador/partida.", null));
			}else if((juegosJugador1P2 + juegosJugador2P2) > numeroJuegosTotalFase){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El maximo de juegos de la partida 2 para el enfrentamiento es " + numeroJuegosTotalFase + ".", null));
			}else if((juegosJugador1P2 + juegosJugador2P2) < numeroJuegosFase){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El minimo de juegos de la partida 2 es para el enfrentamiento es " + numeroJuegosFase + ".", null));
			}else if(juegosJugador1P2 < 0 || juegosJugador2P2 < 0){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No puede haber menos de 0 juegos en la partida 2", null));
			}else if((juegosJugador1P2 < numeroJuegosEmpate && juegosJugador2P2 < numeroJuegosEmpate) 
					|| (juegosJugador1P2 < numeroJuegosEmpate && juegosJugador2P2 < numeroJuegosFase)
					|| (juegosJugador1P2 < numeroJuegosFase && juegosJugador2P2 < numeroJuegosEmpate)){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Resultado incorrecto en partida 2.", null));
			}
			if(terceraPartidaJugada){
				if(juegosJugador1P3 > numeroJuegosFase || juegosJugador2P3 > numeroJuegosFase){
					error = true;
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El maximo de juegos de la partida 3 es " + numeroJuegosFase + " por jugador/partida.", null));
				}else if((juegosJugador1P3 + juegosJugador2P3) > numeroJuegosTotalFase){
					error = true;
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El maximo de juegos de la partida 3 para el enfrentamiento es " + numeroJuegosTotalFase + ".", null));
				}else if((juegosJugador1P3 + juegosJugador2P3) < numeroJuegosFase){
					error = true;
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El minimo de juegos de la partida 3 es para el enfrentamiento es " + numeroJuegosFase + ".", null));
				}else if(juegosJugador1P3 < 0 || juegosJugador2P3 < 0){
					error = true;
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No puede haber menos de 0 juegos en la partida 3", null));
				}else if((juegosJugador1P3 < numeroJuegosEmpate && juegosJugador2P3 < numeroJuegosEmpate) 
						|| (juegosJugador1P3 < numeroJuegosEmpate && juegosJugador2P3 < numeroJuegosFase)
						|| (juegosJugador1P3 < numeroJuegosFase && juegosJugador2P3 < numeroJuegosEmpate)){
					error = true;
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Resultado incorrecto en partida 3.", null));
				}
			}else{
				if(juegosJugador1P3 > 0 && juegosJugador2P3 > 0){
					error = true;
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se pueden indicar valores en la partida 3.", null));
				}
			}
			
		}
		return error;
	}
    
    public boolean isActivoClasificarCuartosForGrupos(){
    	List<CalendarioFaseCF> calendarioFaseCFList = getResultListCalendarioFaseCF();
    	for (CalendarioFaseCF calendarioFaseCF : calendarioFaseCFList) {
			if(calendarioFaseCF.getFaseAnterior().equals(FasesModelo.FASE_I)){
				return true;
			}
		}
    	return false;
    }
    
    public boolean isActivoClasificarCuartosForOctavos(){
    	List<CalendarioFaseCF> calendarioFaseCFList = getResultListCalendarioFaseCF();
    	for (CalendarioFaseCF calendarioFaseCF : calendarioFaseCFList) {
			if(calendarioFaseCF.getFaseAnterior().equals(FasesModelo.FASE_OF)){
				return true;
			}
		}
    	return false;
    }
    
    public boolean isActivoClasificarSemifinalesForGrupos(){
    	List<CalendarioFaseSF> calendarioFaseSFList = getResultListCalendarioFaseSF();
    	for (CalendarioFaseSF calendarioFaseSF : calendarioFaseSFList) {
			if(calendarioFaseSF.getFaseAnterior().equals(FasesModelo.FASE_I)){
				return true;
			}
		}
    	return false;
    }
    
    public boolean isActivoClasificarSemifinalesForCuartos(){
    	List<CalendarioFaseSF> calendarioFaseSFList = getResultListCalendarioFaseSF();
    	for (CalendarioFaseSF calendarioFaseSF : calendarioFaseSFList) {
			if(calendarioFaseSF.getFaseAnterior().equals(FasesModelo.FASE_CF)){
				return true;
			}
		}
    	return false;
    }
    
    public String getExtractJugadorNode(Object node){
    	String array[] = ((String) node).split("#");
    	return array[0].toUpperCase();
    }
    
    public String getExtractEquipoNode(Object node){
    	String array[] = ((String) node).split("#");
    	if(array.length > 1){
    		return array[1];
    	}
    	return "";
    }
    
    public String getExtractCampeonNode(Object node){
    	String array[] = ((String) node).split("#");
    	if(array.length > 1){
    		return array[2];
    	}
    	return "";
    }
    
    public boolean isSorteoFinalizado(){
    	boolean sorteoFinalizado = leerCampeonatoFemeninoIndividual.isSorteoFinalizado();
    	boolean isAdmin = false;
    	if(sessionState != null && sessionState.getIdentityInfo() != null && sessionState.getIdentityInfo().getUsuario() != null){
    		isAdmin = sessionState.getIdentityInfo().getUsuario().getRolId() == 1;
    	}
    	return sorteoFinalizado || isAdmin;
	}
	

}
