package com.bolocelta.facade;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;

import com.bolocelta.bbdd.readTables.LeerNoticias;
import com.bolocelta.entities.Noticias;

@Named
@ConversationScoped
@ManagedBean
public class NoticiasFacade implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private LeerNoticias leerNoticias = new LeerNoticias();
	
	private List<Noticias> resultList = null;

	public List<Noticias> getResultList() {
		if(resultList == null){
			resultList = (List<Noticias>) leerNoticias.listResult();
		}
		return resultList;
	}

	public Object selected() {
		// TODO Auto-generated method stub
		return null;
	}

	public Object read(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	public String insert(Object insert) {
		// TODO Auto-generated method stub
		return null;
	}

	public String update(Object update) {
		// TODO Auto-generated method stub
		return null;
	}

	public String delete(Object delete) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Integer getMaxRowsPaginator(){
		return 4;
	}
	
	public boolean isEnabledPaginator(){
		return getResultList() != null ? (getResultList().size() > getMaxRowsPaginator()) : false;
	}	

}
