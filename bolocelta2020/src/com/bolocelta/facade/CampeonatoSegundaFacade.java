package com.bolocelta.facade;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.UploadedFile;

import com.bolocelta.application.enumerations.CategoriasEnumeration;
import com.bolocelta.application.session.SessionState;
import com.bolocelta.application.utils.Downloader;
import com.bolocelta.bbdd.constants.Activo;
import com.bolocelta.bbdd.constants.Ubicaciones;
import com.bolocelta.bbdd.createTable.CrearCampeonatoLiga;
import com.bolocelta.bbdd.readTables.LeerCampeonatoEquiposSegunda;
import com.bolocelta.entities.CampeonatoEquiposCalendario;
import com.bolocelta.entities.CampeonatoEquiposClasificacion;

@Named
@ConversationScoped
@ManagedBean
public class CampeonatoSegundaFacade implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private static final String CONTENT_TYPE_JPEG = "image/jpeg";
	private static final String CONTENT_TYPE_PNG = "image/png";
	
	@Inject
	private SessionState sessionState;
	
	private LeerCampeonatoEquiposSegunda leerCampeonatoEquiposSegunda = new LeerCampeonatoEquiposSegunda();
	
	private CrearCampeonatoLiga crearCampeonatoLiga = new CrearCampeonatoLiga();
	
	private List<CampeonatoEquiposClasificacion> resultListClasificacion = null;
	private List<CampeonatoEquiposCalendario> resultListCalendario = null;

	public List<CampeonatoEquiposClasificacion> getResultListClasificacion() {
		if(resultListClasificacion == null){
			resultListClasificacion = (List<CampeonatoEquiposClasificacion>) leerCampeonatoEquiposSegunda.listResultClasificacion();
		}
		return resultListClasificacion;
	}
	
	public List<CampeonatoEquiposCalendario> getResultListCalendario() {
		if(resultListCalendario == null){
			resultListCalendario = (List<CampeonatoEquiposCalendario>) leerCampeonatoEquiposSegunda.listResultCalendario();
		}
		return resultListCalendario;
	}

	public Object selected() {
		// TODO Auto-generated method stub
		return null;
	}

	public Object read(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	public String insert(Object insert) {
		// TODO Auto-generated method stub
		return null;
	}

	public String update(Object update) {
		// TODO Auto-generated method stub
		return null;
	}

	public String delete(Object delete) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Integer getRowsPaginatorClasificacion(){
		return getResultListClasificacion().size();
	}
	
	public Integer getRowsPaginatorCalendario(){
		return getResultListClasificacion().size()/2;
	}
	
	public Integer getTotalRowsClasificacion(){
		return getResultListClasificacion().size();
	}
	
	   public String getPermisoActualizarResultado(Integer idEquipo){
	    	if(sessionState != null && sessionState.getUserEquipo() != null && sessionState.getUserEquipo().getId() != null){
	    		if(sessionState.getUserEquipo().getId().equals(idEquipo)){
	    			return "BCMEGEARL2";
	    		}
	    	}
	    	return "SIN PERMISO";
	    }
	    
	    public String getPermisoConfirmarResultado(Integer idEquipo){
	    	if(sessionState != null && sessionState.getUserEquipo() != null && sessionState.getUserEquipo().getId() != null){
	    		if(sessionState.getUserEquipo().getId().equals(idEquipo)){
	    			return "BCMEGEARCL2";
	    		}
	    	}
	    	return "SIN PERMISO";
	    }
		
	    public void doActualizarResultadoLiga(CampeonatoEquiposCalendario cec){
	    	boolean error = false;
			if(cec.isModificable()){
				//Validaciones
				if(cec.getPgequipo1() > 7 || cec.getPgequipo2() > 7){
					error = true;
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El maximo de partidas es 7 por equipo.", null));
				}else if((cec.getPgequipo1() + cec.getPgequipo2()) > 12){
					error = true;
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El maximo de partidas es para el enfrentamiento es 12.", null));
				}else if((cec.getPgequipo1() + cec.getPgequipo2()) < 7){
					error = true;
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El minimo de partidas es para el enfrentamiento es 7.", null));
				}else if(cec.getPgequipo1() < 0 || cec.getPgequipo2() < 0){
					error = true;
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No puede haber menos de 0 partidas", null));
				}else if((cec.getPgequipo1() < 6 && cec.getPgequipo2() < 6) || (cec.getPgequipo1() < 6 && cec.getPgequipo2() < 7)
						|| (cec.getPgequipo1() < 7 && cec.getPgequipo2() < 6)){
					error = true;
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Resultado incorrecto.", null));
				}
				
				//Si todo correcto actualizar resultado
				if(!error){
					crearCampeonatoLiga.actualizarResultadoCalendarioLiga(cec);
					leerCampeonatoEquiposSegunda = new LeerCampeonatoEquiposSegunda();
					resultListClasificacion = (List<CampeonatoEquiposClasificacion>) leerCampeonatoEquiposSegunda.listResultClasificacion();
					resultListCalendario = (List<CampeonatoEquiposCalendario>) leerCampeonatoEquiposSegunda.listResultCalendario();
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha actualizado el resultado del enfrentamiento.", null));
				}
			}
			
		}
	    
		public void doConfirmarResultado(CampeonatoEquiposCalendario cec){
			boolean error = false;
			if(cec.isModificable()){
				//Validaciones
				if(cec.getPgequipo1() > 7 || cec.getPgequipo2() > 7){
					error = true;
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El maximo de partidas es 7 por equipo.", null));
				}else if((cec.getPgequipo1() + cec.getPgequipo2()) > 12){
					error = true;
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El maximo de partidas es para el enfrentamiento es 12.", null));
				}else if((cec.getPgequipo1() + cec.getPgequipo2()) < 7){
					error = true;
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El minimo de partidas es para el enfrentamiento es 7.", null));
				}else if(cec.getPgequipo1() < 0 || cec.getPgequipo2() < 0){
					error = true;
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No puede haber menos de 0 partidas", null));
				}else if((cec.getPgequipo1() < 6 && cec.getPgequipo2() < 6) || (cec.getPgequipo1() < 6 && cec.getPgequipo2() < 7)
						|| (cec.getPgequipo1() < 7 && cec.getPgequipo2() < 6)){
					error = true;
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Resultado incorrecto.", null));
				}
				
				//Si todo correcto confirmar resultado y actualizar clasificacion
				if(!error){
					CampeonatoEquiposClasificacion cecCla1 = recoveryClasificacionEquipo(cec.getEquipo1Id());
					CampeonatoEquiposClasificacion cecCla2 = recoveryClasificacionEquipo(cec.getEquipo2Id());
					crearCampeonatoLiga.actualizarClasificacion(cecCla1, cecCla2, cec);
					cec.setActivo(Activo.NO_NUMBER);
					crearCampeonatoLiga.actualizarResultadoCalendarioLigaConfirmar(cec);
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha confirmado el resultado del enfrentamiento.", null));
					leerCampeonatoEquiposSegunda = new LeerCampeonatoEquiposSegunda();
					resultListClasificacion = (List<CampeonatoEquiposClasificacion>) leerCampeonatoEquiposSegunda.listResultClasificacion();
					resultListCalendario = (List<CampeonatoEquiposCalendario>) leerCampeonatoEquiposSegunda.listResultCalendario();
				}
			}
			
			
		}
		
		public CampeonatoEquiposClasificacion recoveryClasificacionEquipo(Integer idEquipo){
			for (CampeonatoEquiposClasificacion cec : getResultListClasificacion()) {
				if(cec.getEquipoId() == idEquipo){
					return cec;
				}
			}
			return null;
		}	
		
		
		@Inject
		private Downloader downloader;
		
		public void doDownloadFile(CampeonatoEquiposCalendario cec){
			File file = searchFile(cec.getFoto(), cec.getCategoria().getId());
			try {
				String name = cec.getEquipo1().getNombre() + " - " + cec.getEquipo2().getNombre() + ".png";
				downloader.downloadResource(name, "image/png", file);
			} catch (FileNotFoundException e) {
				System.out.println("Error descargando fichero " + e.getMessage());
			}
		}
		
		public File searchFile(String nameFile, Integer categoria) {
			File folder = null;
			if(categoria == CategoriasEnumeration.CATEGORIA_PRIMERA){
				folder = new File(Ubicaciones.UBICACION_BBDD_CAMPEONATO_EQUIPOS_FOTOS_PRIMERA);
			}else if(categoria == CategoriasEnumeration.CATEGORIA_SEGUNDA){
				folder = new File(Ubicaciones.UBICACION_BBDD_CAMPEONATO_EQUIPOS_FOTOS_SEGUNDA);
			}else if(categoria == CategoriasEnumeration.CATEGORIA_TERCERA){
				folder = new File(Ubicaciones.UBICACION_BBDD_CAMPEONATO_EQUIPOS_FOTOS_TERCERA);
			}
			

			File[] files = folder.listFiles();

			for (File file : files) {
				if (file.getName().equalsIgnoreCase(nameFile)) {
					return file;
				}
			}
			return null;
		}
		
		
		UploadedFile file;
		String nameFile;
		String contentType;
		String extension;

		public UploadedFile getFile() {
			return file;
		}

		public void setFile(UploadedFile file) {
			if(file != null){
				if(file.getFileName() != null){
					this.nameFile = file.getFileName();
				}
				if(file.getContentType() != null){
					this.contentType = file.getContentType();
				}
				if(file.getContentType() != null && file.getContentType().equals(CONTENT_TYPE_JPEG)){
					this.extension = ".jpeg";
				}else if(file.getContentType() != null && file.getContentType().equals(CONTENT_TYPE_PNG)){
					this.extension = ".png";
				}
			}
			this.file = file;
		}

		public void subir(CampeonatoEquiposCalendario cec){
			if(this.file == null){
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El fichero es requerido.", null));
				System.out.println("El fichero es requerido. En el file");
			}else {
				try {
					if(this.file.getInputstream() == null){
						FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El fichero es requerido.", null));
						System.out.println("El fichero es requerido. No hay InputStream");
					}
				} catch (IOException e1) {
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Se ha producido un error al subir el fichero.", e1.getMessage()));
					System.out.println("El fichero es requerido. Excepcion en el InputStream");
					System.out.println(e1.getMessage());
				}
				
				
			    OutputStream out = null;
			    InputStream filecontent = null;
			    
				File folder = null;
				
				String name = cec.getEquipo1().getNombre() + " - " + cec.getEquipo2().getNombre() + this.extension;
				if(cec.getCategoriaId() == CategoriasEnumeration.CATEGORIA_PRIMERA){
					folder = new File(Ubicaciones.UBICACION_BBDD_CAMPEONATO_EQUIPOS_FOTOS_PRIMERA + name);
				}else if(cec.getCategoriaId() == CategoriasEnumeration.CATEGORIA_SEGUNDA){
					folder = new File(Ubicaciones.UBICACION_BBDD_CAMPEONATO_EQUIPOS_FOTOS_SEGUNDA + name);
				}else if(cec.getCategoriaId() == CategoriasEnumeration.CATEGORIA_TERCERA){
					folder = new File(Ubicaciones.UBICACION_BBDD_CAMPEONATO_EQUIPOS_FOTOS_TERCERA + name);
				}
		
			    try {
			        out = new FileOutputStream(folder);
			        filecontent = this.file.getInputstream();
		
			        int read = 0;
			        final byte[] bytes = new byte[1024];
		
			        while ((read = filecontent.read(bytes)) != -1) {
			            out.write(bytes, 0, read);
			        }
			        doActualizarNombreFotoPizarra(name, cec);
			    } catch (FileNotFoundException fne) {
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Se ha producido un error al subir el fichero.", fne.getMessage()));
					System.out.println("El fichero es requerido. Error en FileNotFoundException");
					System.out.println(fne.getMessage());
			    } catch (IOException e) {
			    	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Se ha producido un error al subir el fichero.", e.getMessage()));
					System.out.println("El fichero es requerido. Error en el IOException");
					System.out.println(e.getMessage());
				} finally {
			        if (out != null) {
			            try {
							out.close();
						} catch (IOException e) {
							FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Se ha producido un error al subir el fichero.", e.getMessage()));
							System.out.println("El fichero es requerido. IOException del out en el finally");
							System.out.println(e.getMessage());
						}
			        }
			        if (filecontent != null) {
			            try {
							filecontent.close();
						} catch (IOException e) {
							FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Se ha producido un error al subir el fichero.", e.getMessage()));
							System.out.println("El fichero es requerido. IOException en el close del file content");
							System.out.println(e.getMessage());
						}
			        }
			    }
			}
			
		}
		
		public void doActualizarNombreFotoPizarra(String nombre, CampeonatoEquiposCalendario cec){
			cec.setFoto(nombre);
			crearCampeonatoLiga.actualizarNombreFotoPizarra(cec);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha actualizado la foto del enfrentamiento.", null));
			leerCampeonatoEquiposSegunda = new LeerCampeonatoEquiposSegunda();
			resultListClasificacion = (List<CampeonatoEquiposClasificacion>) leerCampeonatoEquiposSegunda.listResultClasificacion();
			resultListCalendario = (List<CampeonatoEquiposCalendario>) leerCampeonatoEquiposSegunda.listResultCalendario();
			
		}

}
