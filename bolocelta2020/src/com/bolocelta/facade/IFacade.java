package com.bolocelta.facade;

import java.util.List;

public interface IFacade<A> {
	
	public List<A> resultList();
	
	public Object selected();
	
	public Object read(Integer id);
	
	public String insert(Object insert);
	
	public String update(Object update);
	
	public String delete(Object delete);
	
	public Integer getMaxRowsPaginator();

}
