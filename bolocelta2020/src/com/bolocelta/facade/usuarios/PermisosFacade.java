package com.bolocelta.facade.usuarios;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;

import com.bolocelta.bbdd.readTables.usuarios.LeerPermisos;
import com.bolocelta.entities.usuarios.Permisos;

@Named
@ConversationScoped
@ManagedBean
public class PermisosFacade implements Serializable {

	private static final long serialVersionUID = 1L;

	private LeerPermisos leerPermisos = new LeerPermisos();

	private List<Permisos> resultList = null;

	public List<Permisos> getResultList() {
		if (resultList == null) {
			resultList = (List<Permisos>) leerPermisos.listResult();
		}
		return resultList;
	}
	
	public List<Permisos> getResultListByRol(Integer rolId) {
		if (resultList == null) {
			resultList = (List<Permisos>) leerPermisos.listResultByRol(rolId);
		}
		return resultList;
	}

	public Object selected() {
		// TODO Auto-generated method stub
		return null;
	}

	public Object read(Integer id) {
		return leerPermisos.read(id);
	}

	public String insert(Object insert) {
		// TODO Auto-generated method stub
		return null;
	}

	public String update(Object update) {
		// TODO Auto-generated method stub
		return null;
	}

	public String delete(Object delete) {
		// TODO Auto-generated method stub
		return null;
	}

}
