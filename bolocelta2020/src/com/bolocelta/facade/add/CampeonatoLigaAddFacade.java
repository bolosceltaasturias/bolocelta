package com.bolocelta.facade.add;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.inject.Named;

import com.bolocelta.application.MenuView;
import com.bolocelta.application.Redirect;
import com.bolocelta.bbdd.createTable.CrearCampeonatoLiga;

@Named
@ManagedBean
@RequestScoped
public class CampeonatoLigaAddFacade implements Serializable{
	
	@Inject
	private Redirect redirect;
	
	@Inject
	private MenuView menuView;
	
	private static final long serialVersionUID = 1L;
	
	private CrearCampeonatoLiga crearCampeonatoLiga = new CrearCampeonatoLiga();
	
	public CampeonatoLigaAddFacade() {
	}

	public Integer getMaxRowsPaginator(){
		return 8;
	}
	
	public void doRealizarSorteoCampeonato(){
		
		crearCampeonatoLiga.realizarSorteo();

	}
	
	public void doValidarCalendario(){
		
		crearCampeonatoLiga.doValidarCalendario();

	}
	

}
