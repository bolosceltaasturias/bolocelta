package com.bolocelta.facade.add;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.bolocelta.application.MenuView;
import com.bolocelta.application.Redirect;
import com.bolocelta.application.utils.DateUtils;
import com.bolocelta.application.utils.NumberUtils;
import com.bolocelta.bbdd.constants.Activo;
import com.bolocelta.bbdd.constants.EstadosIndividualParejas;
import com.bolocelta.bbdd.constants.FasesModelo;
import com.bolocelta.bbdd.constants.GruposLetra;
import com.bolocelta.bbdd.constants.TipoEnfrentamiento;
import com.bolocelta.bbdd.constants.structure.EstructuraCampeonatoMasculinoIndividualPrimera;
import com.bolocelta.bbdd.createTable.CrearCampeonatoMasculinoIndividualPrimera;
import com.bolocelta.bbdd.readTables.LeerBoleras;
import com.bolocelta.bbdd.readTables.LeerCampeonatoMasculinoIndividualPrimera;
import com.bolocelta.entities.Boleras;
import com.bolocelta.entities.Categorias;
import com.bolocelta.entities.Configuracion;
import com.bolocelta.entities.Fases;
import com.bolocelta.entities.Jugadores;
import com.bolocelta.entities.ParticipantesIndividual;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseCF;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseFC;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseFF;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseI;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseII;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseOF;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseSF;
import com.bolocelta.entities.sorteos.individual.ClasificacionFaseI;
import com.bolocelta.entities.sorteos.individual.ClasificacionFaseII;
import com.bolocelta.entities.sorteos.individual.Grupos;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualCuartosFinal4Grupos;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualCuartosFinal4OF;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualEnfrentamientoCuartosFinal;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualEnfrentamientoFinal;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualEnfrentamientoFinalConsolacion;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualEnfrentamientoGruposFaseI;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualEnfrentamientoOctavosFinal;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualEnfrentamientoSemiFinal;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualFinal4SF;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualFinalConsolacion4SF;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualGrupos3;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualGrupos4;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualGrupos5;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualGrupos6;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualGrupos7;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualGrupos8;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualOctavosFinal4Grupos;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualOctavosFinal8Grupos;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualSemiFinal4CF;

@Named
@ManagedBean
@RequestScoped
public class CampeonatoMasculinoIndividualPrimeraAddFacade implements Serializable{
	
	@Inject
	private Redirect redirect;
	
	@Inject
	private MenuView menuView;
	
	private static final long serialVersionUID = 1L;
	
	private LeerCampeonatoMasculinoIndividualPrimera leerCampeonatoMasculinoIndividual = new LeerCampeonatoMasculinoIndividualPrimera();
	private CrearCampeonatoMasculinoIndividualPrimera crearCampeonatoMasculinoIndividual = new CrearCampeonatoMasculinoIndividualPrimera();
	
	private LeerBoleras leerBoleras = new LeerBoleras();
	
	private String diaFechaMaxInscripcion = null;
	private String mesFechaMaxInscripcion = null;
	private String anyoFechaMaxInscripcion = null;
	
	private Date fechaMaxInscripcion = null;
	private String estadoCampeonato = null;
	private String bolera = null;
	private String observacionesCampeonato1 = null;
	private String observacionesCampeonato2 = null;
	private String observacionesCampeonato3 = null;
	private String observacionesCampeonato4 = null;
	private String observacionesCampeonato5 = null;
	private String observacionesCampeonato6 = null;
	private String observacionesCampeonato7 = null;
	private String bolerasOcupadasFI = null;
	private String bolerasOcupadasOFCF = null;
	
	private List<Configuracion> resultListConfiguracion = null;
	private List<Fases> resultListFases = null;
	private List<ParticipantesIndividual> resultListParticipantesIndividual = null;
	
	public CampeonatoMasculinoIndividualPrimeraAddFacade() {
		Date fechaMaxInscripcion = leerCampeonatoMasculinoIndividual.getFechaMaxInscripcion();
		if(fechaMaxInscripcion != null){
			setDiaFechaMaxInscripcion(String.valueOf(fechaMaxInscripcion.getDate()));
			setMesFechaMaxInscripcion(String.valueOf(fechaMaxInscripcion.getMonth()+1));
			setAnyoFechaMaxInscripcion(String.valueOf(fechaMaxInscripcion.getYear()+1900));
		}
		setEstadoCampeonato(leerCampeonatoMasculinoIndividual.getEstadoCampeonato());
		setBolera(leerCampeonatoMasculinoIndividual.getBoleraFinal());
		setObservacionesCampeonato1(leerCampeonatoMasculinoIndividual.getObservacionesCampeonato1());
		setObservacionesCampeonato2(leerCampeonatoMasculinoIndividual.getObservacionesCampeonato2());
		setObservacionesCampeonato3(leerCampeonatoMasculinoIndividual.getObservacionesCampeonato3());
		setObservacionesCampeonato4(leerCampeonatoMasculinoIndividual.getObservacionesCampeonato4());
		setObservacionesCampeonato5(leerCampeonatoMasculinoIndividual.getObservacionesCampeonato5());
		setObservacionesCampeonato6(leerCampeonatoMasculinoIndividual.getObservacionesCampeonato6());
		setObservacionesCampeonato7(leerCampeonatoMasculinoIndividual.getObservacionesCampeonato7());
		setBolerasOcupadasFI(leerCampeonatoMasculinoIndividual.getBolerasOcupadasFI());
		setBolerasOcupadasOFCF(leerCampeonatoMasculinoIndividual.getBolerasOcupadasOFCF());
	}

	public List<Configuracion> getResultListConfiguracion() {
		if(resultListConfiguracion == null){
			resultListConfiguracion = (List<Configuracion>) leerCampeonatoMasculinoIndividual.listResultConfig();
		}
		return resultListConfiguracion;
	}
	
	public List<Fases> getResultListFases() {
		if(resultListFases == null){
			resultListFases = (List<Fases>) leerCampeonatoMasculinoIndividual.listResultFases();
		}
		return resultListFases;
	}
	
	public List<ParticipantesIndividual> getResultListParticipantes() {
		if(resultListParticipantesIndividual == null){
			resultListParticipantesIndividual = (List<ParticipantesIndividual>) leerCampeonatoMasculinoIndividual.listResultParticipantesOrderByEquipo();
		}
		return resultListParticipantesIndividual;
	}
	
	//NUEVO
	public List<ParticipantesIndividual> getResultListParticipantesByEquipos(Set<Integer> keySetequipoMasJugadoresQueGruposLibreGrupo) {
		if(resultListParticipantesIndividual == null){
			resultListParticipantesIndividual = (List<ParticipantesIndividual>) leerCampeonatoMasculinoIndividual.listResultParticipantesOrderByEquipo();
		}
		List<ParticipantesIndividual> resultListParticipantesIndividualMasJugadoresQueGrupos = new ArrayList<>();
		for (ParticipantesIndividual participantesIndividual : resultListParticipantesIndividual) {
			if(keySetequipoMasJugadoresQueGruposLibreGrupo.contains(participantesIndividual.getJugador().getEquipoId())){
				resultListParticipantesIndividualMasJugadoresQueGrupos.add(participantesIndividual);
			}
		}
		return resultListParticipantesIndividualMasJugadoresQueGrupos;
	}
	//F-NUEVO
	
	public Integer getTotalRowsParticipantes(){
		if(resultListParticipantesIndividual == null){
			getResultListParticipantes();
		}
		if(resultListParticipantesIndividual != null){
			return resultListParticipantesIndividual.size();
		}
		return 0;
	}
	
	public Integer getMaxRowsPaginator(){
		return 8;
	}
	
	public boolean isEnabledPaginatorParticipantes(){
		return (getResultListParticipantes() != null && getResultListParticipantes() != null) ? 
					(getResultListParticipantes().size() > getMaxRowsPaginator()) : false;
	}
	
	//Manejo de estados
	public boolean isEstadoCampeonatoSinCrear(){
		return leerCampeonatoMasculinoIndividual.isEstadoCampeonatoSinCrear();
	}
	
	public boolean isAbrirInscripciones(){
		return leerCampeonatoMasculinoIndividual.isAbrirInscripciones();
	}
	
	public boolean isCerrarInscripciones(){
		return leerCampeonatoMasculinoIndividual.isCerrarInscripciones();
	}
	
	public boolean isInscripcionesCerradas(){
		return leerCampeonatoMasculinoIndividual.isInscripcionesCerradas();
	}
	
	public boolean isFasesCerradas(){
		return leerCampeonatoMasculinoIndividual.isFasesCerradas();
	}
	
	public boolean isSorteoRealizado(){
		return leerCampeonatoMasculinoIndividual.isSorteoRealizado();
	}
	
	public boolean isSorteoFinalizado(){
		return leerCampeonatoMasculinoIndividual.isSorteoFinalizado();
	}
	
	//Acciones para los diferentes estados
	
	private boolean validateFecha() {
		if(getAnyoFechaMaxInscripcion() != null && getMesFechaMaxInscripcion() != null && getDiaFechaMaxInscripcion() != null){
			if(NumberUtils.isNumber(getAnyoFechaMaxInscripcion()) && NumberUtils.isNumber(getMesFechaMaxInscripcion()) && NumberUtils.isNumber(getDiaFechaMaxInscripcion())){
				Integer dia = Integer.valueOf(getDiaFechaMaxInscripcion());
				if(dia > 31){
					return false;
				}
				Integer mes = Integer.valueOf(getMesFechaMaxInscripcion());
				if(mes > 12){
					return false;
				}
				Integer anyo = Integer.valueOf(getAnyoFechaMaxInscripcion());
				String fecha = (dia < 10 ? "0" + dia : dia) + "/" + (mes < 10 ? "0" + mes : mes) + "/" + anyo;
				Date fechaConvert = DateUtils.convertStringToDate(fecha);
				if(fechaConvert == null){
					return false;
				}else{
					setFechaMaxInscripcion(fechaConvert);
					return true;
				}
			}
		}
		return false;
		
	}
	
	public void doCrearBaseCampeonato(){
		
		boolean error = validateFecha();
		if(error){
			crearCampeonatoMasculinoIndividual.crearBaseCampeonato(getFechaMaxInscripcion(), EstadosIndividualParejas.CONFIG_ESTADO_CREAR, getBolera());
			//Inicializar variables de nuevo
			leerCampeonatoMasculinoIndividual = new LeerCampeonatoMasculinoIndividualPrimera();
			//Redirigir
			redirect.redirect(menuView.PAG_OM_ADD_CAMPEONATO_MASCULINO_INDIVIDUAL_PRIMERA);
		}else{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "La Fecha max. inscripci�n no es valida.", null));
		}
	}
	
	public void doAbrirInscripcionesCampeonato(){
		crearCampeonatoMasculinoIndividual.actualizarEstadoConfiguracion(EstadosIndividualParejas.CONFIG_ESTADO_ABRIR_INSCRIPCIONES);
		//Inicializar variables de nuevo
		leerCampeonatoMasculinoIndividual = new LeerCampeonatoMasculinoIndividualPrimera();
		//Redirigir
		redirect.redirect(menuView.PAG_OM_ADD_CAMPEONATO_MASCULINO_INDIVIDUAL_PRIMERA);
	}
	
	public void doCerrarInscripcionesCampeonato(){
		crearCampeonatoMasculinoIndividual.actualizarEstadoConfiguracion(EstadosIndividualParejas.CONFIG_ESTADO_CERRAR_INSCRIPCIONES);
		//Inicializar variables de nuevo
		leerCampeonatoMasculinoIndividual = new LeerCampeonatoMasculinoIndividualPrimera();
		//Redirigir
		redirect.redirect(menuView.PAG_OM_ADD_CAMPEONATO_MASCULINO_INDIVIDUAL_PRIMERA);
	}
	
	public void doInscribirJugador(Jugadores jugador, Categorias categoria, String usuario, String activo){
		ParticipantesIndividual participantesIndividual = leerCampeonatoMasculinoIndividual.existeJugadorComoParticipante(jugador.getId());
		//Si existe se actualiza, sino se a�ade nuevo
		if(participantesIndividual == null){
			crearCampeonatoMasculinoIndividual.inscribirNewJugador(jugador, categoria, usuario, activo);
		}else{
			crearCampeonatoMasculinoIndividual.inscribirUpdateJugador(participantesIndividual, jugador, categoria, usuario, activo);
		}
		//Inicializar variables de nuevo
		leerCampeonatoMasculinoIndividual = new LeerCampeonatoMasculinoIndividualPrimera();
	}
	
	public Date getFechaMaxInscripcion() {
		return fechaMaxInscripcion;
	}

	public void setFechaMaxInscripcion(Date fechaMaxInscripcion) {
		this.fechaMaxInscripcion = fechaMaxInscripcion;
	}
	
	public String getBolera() {
		return bolera;
	}

	public void setBolera(String bolera) {
		this.bolera = bolera;
	}
	
	public String getNombreBolera(){
		if(this.bolera != null && this.bolera.length() > 0){
			return leerBoleras.read(Integer.valueOf(this.bolera)).getBolera();
		}
		return "";
	}
	
	public String getObservacionesCampeonato1() {
		return observacionesCampeonato1;
	}

	public void setObservacionesCampeonato1(String observacionesCampeonato1) {
		this.observacionesCampeonato1 = observacionesCampeonato1;
	}
	
	public String getObservacionesCampeonato2() {
		return observacionesCampeonato2;
	}

	public void setObservacionesCampeonato2(String observacionesCampeonato2) {
		this.observacionesCampeonato2 = observacionesCampeonato2;
	}
	
	public String getObservacionesCampeonato3() {
		return observacionesCampeonato3;
	}

	public void setObservacionesCampeonato3(String observacionesCampeonato3) {
		this.observacionesCampeonato3 = observacionesCampeonato3;
	}
	
	public String getObservacionesCampeonato4() {
		return observacionesCampeonato4;
	}

	public void setObservacionesCampeonato4(String observacionesCampeonato4) {
		this.observacionesCampeonato4 = observacionesCampeonato4;
	}
	
	public String getObservacionesCampeonato5() {
		return observacionesCampeonato5;
	}

	public void setObservacionesCampeonato5(String observacionesCampeonato5) {
		this.observacionesCampeonato5 = observacionesCampeonato5;
	}
	
	public String getObservacionesCampeonato6() {
		return observacionesCampeonato6;
	}

	public void setObservacionesCampeonato6(String observacionesCampeonato6) {
		this.observacionesCampeonato6 = observacionesCampeonato6;
	}
	
	public String getObservacionesCampeonato7() {
		return observacionesCampeonato7;
	}

	public void setObservacionesCampeonato7(String observacionesCampeonato7) {
		this.observacionesCampeonato7 = observacionesCampeonato7;
	}

	public String getEstadoCampeonato() {
		return estadoCampeonato;
	}

	public void setEstadoCampeonato(String estadoCampeonato) {
		this.estadoCampeonato = estadoCampeonato;
	}
	
	public String getDiaFechaMaxInscripcion() {
		return diaFechaMaxInscripcion;
	}

	public void setDiaFechaMaxInscripcion(String diaFechaMaxInscripcion) {
		this.diaFechaMaxInscripcion = diaFechaMaxInscripcion;
	}

	public String getMesFechaMaxInscripcion() {
		return mesFechaMaxInscripcion;
	}

	public void setMesFechaMaxInscripcion(String mesFechaMaxInscripcion) {
		this.mesFechaMaxInscripcion = mesFechaMaxInscripcion;
	}

	public String getAnyoFechaMaxInscripcion() {
		return anyoFechaMaxInscripcion;
	}

	public void setAnyoFechaMaxInscripcion(String anyoFechaMaxInscripcion) {
		this.anyoFechaMaxInscripcion = anyoFechaMaxInscripcion;
	}
	
	public void doCambiarActivarFase(Fases fase){
		if(fase.getActivo().equalsIgnoreCase(Activo.SI)){
			fase.setActivo(Activo.NO);
		}else if(fase.getActivo().equalsIgnoreCase(Activo.NO)){
			fase.setActivo(Activo.SI);
		}
		crearCampeonatoMasculinoIndividual.actualizarActivoFase(fase);
		leerCampeonatoMasculinoIndividual = new LeerCampeonatoMasculinoIndividualPrimera();
	}
	
	public void doActualizarDatos(Fases fase){
		crearCampeonatoMasculinoIndividual.actualizarFase(fase);
		leerCampeonatoMasculinoIndividual = new LeerCampeonatoMasculinoIndividualPrimera();
	}

	public void doCerrarFasesCampeonato(){
		//Inicializar variables de nuevo
		leerCampeonatoMasculinoIndividual = new LeerCampeonatoMasculinoIndividualPrimera();
		for(Fases fase : getResultListFases()){
			if(fase.getActivo().equals(Activo.SI)){
				if(fase.getNumero().equals(FasesModelo.FASE_I)){
					crearCampeonatoMasculinoIndividual.crearClasificacionFaseI();
					crearCampeonatoMasculinoIndividual.crearCalendarioFaseI();
				}else if(fase.getNumero().equals(FasesModelo.FASE_II)){
					crearCampeonatoMasculinoIndividual.crearCalendarioFaseII();
					crearCampeonatoMasculinoIndividual.crearClasificacionFaseII();
				}else if(fase.getNumero().equals(FasesModelo.FASE_OF)){
					crearCampeonatoMasculinoIndividual.crearCalendarioFaseOF();
				}else if(fase.getNumero().equals(FasesModelo.FASE_CF)){
					crearCampeonatoMasculinoIndividual.crearCalendarioFaseCF();
				}else if(fase.getNumero().equals(FasesModelo.FASE_SF)){
					crearCampeonatoMasculinoIndividual.crearCalendarioFaseSF();
				}else if(fase.getNumero().equals(FasesModelo.FASE_FC)){
					crearCampeonatoMasculinoIndividual.crearCalendarioFaseFC();
				}else if(fase.getNumero().equals(FasesModelo.FASE_FF)){
					crearCampeonatoMasculinoIndividual.crearCalendarioFaseFF();
				}
			}
		}
		crearCampeonatoMasculinoIndividual.actualizarEstadoConfiguracion(EstadosIndividualParejas.CONFIG_ESTADO_CERRAR_FASES);
		//Inicializar variables de nuevo
		leerCampeonatoMasculinoIndividual = new LeerCampeonatoMasculinoIndividualPrimera();
		//Redirigir
		redirect.redirect(menuView.PAG_OM_ADD_CAMPEONATO_MASCULINO_INDIVIDUAL_PRIMERA);
	}
	
	public void doRealizarSorteoCampeonato(){
		
		boolean sorteoRealizado = false;
		
		try {
		
			//Asignar las observaciones del campeonato
			EstructuraCampeonatoMasculinoIndividualPrimera ecmi = new EstructuraCampeonatoMasculinoIndividualPrimera();
			crearCampeonatoMasculinoIndividual.updateRow(ecmi.getEstructuraObservacionesCampeonato1(), ecmi.getEstructuraObservacionesCampeonato1().getFila(), observacionesCampeonato1);
			crearCampeonatoMasculinoIndividual.updateRow(ecmi.getEstructuraObservacionesCampeonato2(), ecmi.getEstructuraObservacionesCampeonato2().getFila(), observacionesCampeonato2);
			crearCampeonatoMasculinoIndividual.updateRow(ecmi.getEstructuraObservacionesCampeonato3(), ecmi.getEstructuraObservacionesCampeonato3().getFila(), observacionesCampeonato3);
			crearCampeonatoMasculinoIndividual.updateRow(ecmi.getEstructuraObservacionesCampeonato4(), ecmi.getEstructuraObservacionesCampeonato4().getFila(), observacionesCampeonato4);
			crearCampeonatoMasculinoIndividual.updateRow(ecmi.getEstructuraObservacionesCampeonato5(), ecmi.getEstructuraObservacionesCampeonato5().getFila(), observacionesCampeonato5);
			crearCampeonatoMasculinoIndividual.updateRow(ecmi.getEstructuraObservacionesCampeonato6(), ecmi.getEstructuraObservacionesCampeonato6().getFila(), observacionesCampeonato6);
			crearCampeonatoMasculinoIndividual.updateRow(ecmi.getEstructuraObservacionesCampeonato7(), ecmi.getEstructuraObservacionesCampeonato7().getFila(), observacionesCampeonato7);
			crearCampeonatoMasculinoIndividual.updateRow(ecmi.getEstructuraBolerasOcupadasFI(), ecmi.getEstructuraBolerasOcupadasFI().getFila(), bolerasOcupadasFI);
			crearCampeonatoMasculinoIndividual.updateRow(ecmi.getEstructuraBolerasOcupadasOFCF(), ecmi.getEstructuraBolerasOcupadasOFCF().getFila(), bolerasOcupadasOFCF);
			
			
			
			//Inicializar variables de nuevo
			leerCampeonatoMasculinoIndividual = new LeerCampeonatoMasculinoIndividualPrimera();
			//Crear los mapas para el sorteo
			//FaseI
			LinkedHashMap<Integer, CalendarioFaseI> calendarioFaseIMap = new LinkedHashMap<>();
			LinkedHashMap<Integer, ClasificacionFaseI> clasificacionFaseIMap = new LinkedHashMap<>();
			//FaseII
			LinkedHashMap<Integer, CalendarioFaseII> calendarioFaseIIMap = new LinkedHashMap<>();
			LinkedHashMap<Integer, ClasificacionFaseII> clasificacionFaseIIMap = new LinkedHashMap<>();
			//FaseOF
			LinkedHashMap<Integer, CalendarioFaseOF> calendarioFaseOFMap = new LinkedHashMap<>();
			//FaseCF
			LinkedHashMap<Integer, CalendarioFaseCF> calendarioFaseCFMap = new LinkedHashMap<>();
			//FaseSF
			LinkedHashMap<Integer, CalendarioFaseSF> calendarioFaseSFMap = new LinkedHashMap<>();
			//FaseFC
			LinkedHashMap<Integer, CalendarioFaseFC> calendarioFaseFCMap = new LinkedHashMap<>();
			//FaseFF
			LinkedHashMap<Integer, CalendarioFaseFF> calendarioFaseFFMap = new LinkedHashMap<>();
			
			//1. Obtener las boleras disponibles, en principio solo las federadas.
			LinkedHashMap<Integer, String> bolerasGrupoAsignadasMap = new LinkedHashMap<>();
			LeerBoleras leerBoleras = new LeerBoleras();
			List<Boleras> resultListBoleras = (List<Boleras>) leerBoleras.listResultFederadas();
			
			//Fase primera
			Integer numerofaseAnterior = 1;
			Integer numerofaseSiguiente = 1;
			
			//Bolera de la final
			Boleras boleraFinal = null;
			for (Configuracion configuracion : getResultListConfiguracion()) {
				if(configuracion.getNombre().equals("Bolera Final")){
					boleraFinal = leerBoleras.read(Integer.valueOf(configuracion.getValor()));
				}
			}
			
			//Bolera de la final
			LinkedHashMap<Integer, Boleras> bolerasOcupadasFIMap =  new LinkedHashMap<>();
			for (Configuracion configuracion : getResultListConfiguracion()) {
				if(configuracion.getNombre().equals("Boleras Ocupadas FI")){
					String bolerasOcupadas = configuracion.getValor();
					String[] bolerasOcupadasList = bolerasOcupadas.split("-");
					for (String bolera : bolerasOcupadasList) {
						Integer idBolera = Integer.valueOf(bolera);
						if(idBolera != null && idBolera > 0){
							Boleras boleraObject = leerBoleras.read(idBolera);
							if(boleraObject != null){
								bolerasOcupadasFIMap.put(idBolera, boleraObject);
							}
						}
					}
				}
			}
			
			//Bolera de la final
			LinkedHashMap<Integer, Boleras> bolerasOcupadasOFCFMap =  new LinkedHashMap<>();
			for (Configuracion configuracion : getResultListConfiguracion()) {
				if(configuracion.getNombre().equals("Boleras Ocupadas OFCF")){
					String bolerasOcupadas = configuracion.getValor();
					String[] bolerasOcupadasList = bolerasOcupadas.split("-");
					for (String bolera : bolerasOcupadasList) {
						Integer idBolera = Integer.valueOf(bolera);
						if(idBolera != null && idBolera > 0){
							Boleras boleraObject = leerBoleras.read(idBolera);
							if(boleraObject != null){
								bolerasOcupadasOFCFMap.put(idBolera, boleraObject);
							}
						}
					}
				}
			}
			
			//2. Definir la faseI
			for(Fases fase : getResultListFases()){
				if(fase.getActivo().equals(Activo.SI)){
					if(fase.getNumero().equals(FasesModelo.FASE_I)){
						if(numerofaseSiguiente.equals(fase.getNumero())){
						
							//Objetos
							//Grupos Fase I
							LinkedHashMap<String, Grupos> gruposFaseIMap = new LinkedHashMap<>();
							//Jugadores Asignados
							LinkedHashMap<Integer, ParticipantesIndividual> jugadoresAsignados = new LinkedHashMap<>();
							
							//2.1. Obtener el numero de grupos de la fase
							Integer numeroGruposFase = fase.getNumeroEnfrentamientos();
							Integer numeroGruposFaseAsignados = 0;
							
							//2.2. Obtener el numero de participantes para saber el numero de jugadores por grupo y los asignados al grupo para
							Integer numeroParticipantes = getResultListParticipantes().size();
							Integer jugadoresPorGrupoAsignados = 0;
							
							
							//NUEVO
							//Equipos mismos o mas jugadores que grupos (idEquipo, numeroJugadores)
							LinkedHashMap<Integer, Integer> equipoMasJugadoresQueGrupos = new LinkedHashMap<>();
							for (ParticipantesIndividual participante : getResultListParticipantes()) {
								if(equipoMasJugadoresQueGrupos.containsKey(participante.getJugador().getEquipoId())){
									Integer jugadoresEquipoValue = equipoMasJugadoresQueGrupos.get(participante.getJugador().getEquipoId());
									equipoMasJugadoresQueGrupos.put(participante.getJugador().getEquipoId(), jugadoresEquipoValue + 1 );
								}else{
									equipoMasJugadoresQueGrupos.put(participante.getJugador().getEquipoId(), 1);
								}
							}
							//Lista de equipos con mas jugadores que grupos
							LinkedHashMap<Integer, Integer> equipoMasJugadoresQueGruposLibre = (LinkedHashMap<Integer, Integer>) equipoMasJugadoresQueGrupos.clone();
							//Limpiar equipos que tengan menos jugadores que grupos
							Set<Integer> keySet = equipoMasJugadoresQueGrupos.keySet();
							for (Integer idEquipoKey : keySet) {
								Integer jugadoresEquipoValue = equipoMasJugadoresQueGrupos.get(idEquipoKey);
								if(jugadoresEquipoValue < numeroGruposFase){
									equipoMasJugadoresQueGruposLibre.remove(idEquipoKey);
								}
							}
							//F-NUEVO
							
							
							
							//2.3. Crear los grupos a participar
							Integer gruposCreados = 1;
							while(gruposCreados <= numeroGruposFase){
								Grupos grupos = new Grupos();
								grupos.setId(GruposLetra.getGruposLetraFaseI(gruposCreados));
								grupos.setCategoriaId(com.bolocelta.bbdd.constants.Categorias.PRIMERA);
								grupos.setFecha(fase.getFecha());
								grupos.setHora(fase.getHora());
								//Asignar bolera aleatoriamente
								Boleras boleraAsignar = null;
								boolean boleraAsignada = false;
								
								//Quito las boleras ocupadas
								List<Boleras> resultListBolerasLibres = new ArrayList<>();
								for (Boleras boleras : resultListBoleras) {
									if(!bolerasOcupadasFIMap.containsKey(boleras.getId())){
										resultListBolerasLibres.add(boleras);
									}
								}
								
								
								
								while(!boleraAsignada){
									Random rand = new Random();
									boleraAsignar = resultListBolerasLibres.get(rand.nextInt(resultListBolerasLibres.size()));
									//if(!boleraAsignar.getId().equals(boleraFinal.getId())){
										if(!bolerasOcupadasFIMap.containsKey(boleraAsignar.getId())){
											if(!bolerasGrupoAsignadasMap.containsKey(boleraAsignar.getId())){
												boleraAsignada = true;
											}
										}
									//}
								}
								grupos.setBoleraId(boleraAsignar.getId());
								
								
								//Obtener el numero de jugadores del grupo
								BigDecimal jugadoresPendientesAsignar = new BigDecimal(numeroParticipantes - jugadoresPorGrupoAsignados);
								BigDecimal gruposPendientesAsignar = new BigDecimal(numeroGruposFase - numeroGruposFaseAsignados);
								BigDecimal jugadoresPorGrupoCalculo = jugadoresPendientesAsignar.divide(gruposPendientesAsignar, RoundingMode.UP);
								grupos.setJugadores(jugadoresPorGrupoCalculo.intValue());
								
								//Agregar los jugadores para cada grupo a sorteo
								//A cada grupo sortear los jugadores, se obtienen por equipos de mayor numero de jugadores a menor
								//Limitaciones: 1-  Se han de repartir los jugadores de equipo por grupo, 
								//					si hay mas jugadores del equipo por grupo se asigna a alguno de los grupos
								Integer jugadoresAsignadosGrupo = 0;
								
								//NUEVO
								//Lista de equipos con mas jugadores que grupos para cada grupo
								LinkedHashMap<Integer, Integer> equipoMasJugadoresQueGruposLibreGrupo = (LinkedHashMap<Integer, Integer>) equipoMasJugadoresQueGruposLibre.clone();
								//F-NUEVO
								
								Integer reintentosEquipoRepetido = 0;
								while(jugadoresAsignadosGrupo < jugadoresPorGrupoCalculo.intValue()){
									
									Random rand = new Random();
									
									
									//NUEVO
									ParticipantesIndividual jugadorSorteo = null;
									
									//Primero buscar jugadores pertenecientes a los equipos que tienen los mismos o mas jugadores que grupos
									Set<Integer> keySetequipoMasJugadoresQueGruposLibreGrupo = equipoMasJugadoresQueGruposLibreGrupo.keySet();
									if(keySetequipoMasJugadoresQueGruposLibreGrupo.size() > 0){
										List<ParticipantesIndividual> list = getResultListParticipantesByEquipos(keySetequipoMasJugadoresQueGruposLibreGrupo);
										boolean jugadorCorrecto = false;
										while(!jugadorCorrecto){
											jugadorSorteo = list.get(rand.nextInt(list.size()));
											if(!jugadoresAsignados.containsKey(jugadorSorteo.getIdJugador())){
												equipoMasJugadoresQueGruposLibreGrupo.remove(jugadorSorteo.getJugador().getEquipoId());
												jugadorCorrecto = true;
											}
										}
									}else{
										boolean jugadorCorrecto = false;
										while(!jugadorCorrecto){
											jugadorSorteo = getResultListParticipantes().get(rand.nextInt(getResultListParticipantes().size()));
											if(!jugadoresAsignados.containsKey(jugadorSorteo.getIdJugador())){
												if(equipoMasJugadoresQueGruposLibreGrupo.size() > 0){
													equipoMasJugadoresQueGruposLibreGrupo.remove(jugadorSorteo.getJugador().getEquipoId());
												}
												jugadorCorrecto = true;
											}
										}
										
									}
									
									//F-NUEVO
									
									
									
									if(!grupos.getJugadoresGrupoList().containsKey(jugadorSorteo.getIdJugador())){
										if(!jugadoresAsignados.containsKey(jugadorSorteo.getIdJugador())){
											if(!grupos.isExisteJugadorMismoEquipo(jugadorSorteo.getJugador().getEquipoId(), equipoMasJugadoresQueGrupos, numeroGruposFase)){
												jugadorSorteo.setNumeroJugadorGrupo(jugadoresAsignadosGrupo + 1);
												grupos.getJugadoresGrupoList().put(jugadorSorteo.getIdJugador(), jugadorSorteo);
												jugadoresAsignadosGrupo++;
												jugadoresAsignados.put(jugadorSorteo.getIdJugador(), jugadorSorteo);
											}else{
												if(reintentosEquipoRepetido >50){
													jugadorSorteo.setNumeroJugadorGrupo(jugadoresAsignadosGrupo + 1);
													grupos.getJugadoresGrupoList().put(jugadorSorteo.getIdJugador(), jugadorSorteo);
													jugadoresAsignadosGrupo++;
													reintentosEquipoRepetido = 0;
													jugadoresAsignados.put(jugadorSorteo.getIdJugador(), jugadorSorteo);
												}else{
													reintentosEquipoRepetido++;
												}
											}
										}
									}
								}
								
								//Agregar a los mapas
								gruposFaseIMap.put(grupos.getId(), grupos);
								bolerasGrupoAsignadasMap.put(boleraAsignar.getId(), grupos.getId());
								
								//Actualizar variables
								gruposCreados++;
								numeroGruposFaseAsignados++;
								jugadoresPorGrupoAsignados += jugadoresPorGrupoCalculo.intValue();
								
								
							}
							
							//A�adir los datos a el calendario y clasificacion de la fase I
							List<CalendarioFaseI> calendarioFaseIList = new ArrayList<CalendarioFaseI>();
							List<ClasificacionFaseI> clasificacionFaseIList = new ArrayList<ClasificacionFaseI>();
							//Se recorren los grupos obtenidos
							//Se a�aden a la clasificacion
							for (Entry<String, Grupos> entry : gruposFaseIMap.entrySet()) {
							    Object value = entry.getValue();
							    Grupos grupo = (Grupos) value;
							    for (Entry<Integer, ParticipantesIndividual> entryPi : grupo.getJugadoresGrupoList().entrySet()) {
							    	Object valuePi = entryPi.getValue();
							    	ParticipantesIndividual participantesIndividual = (ParticipantesIndividual) valuePi;
							    	
							    	//Agrego a clasificacion el jugador para el grupo
							    	ClasificacionFaseI cf1 = new ClasificacionFaseI();
							    	cf1.setCategoriaId(com.bolocelta.bbdd.constants.Categorias.PRIMERA);
							    	cf1.setGrupo(grupo.getId());
							    	cf1.setId(participantesIndividual.getNumeroJugadorGrupo());
							    	cf1.setJugadorId(participantesIndividual.getIdJugador());
							    	cf1.setPj(0);
							    	cf1.setPg(0);
							    	cf1.setPe(0);
							    	cf1.setPp(0);
							    	cf1.setPf(0);
							    	cf1.setPc(0);
							    	cf1.setPt(0);
							    	clasificacionFaseIList.add(cf1);
							    }
							}
							//Se a�aden al calendario
							for (Entry<String, Grupos> entry : gruposFaseIMap.entrySet()) {
							    Object value = entry.getValue();
							    Grupos grupo = (Grupos) value;
							    //Recorro los enfrentamientos para el modelo del grupo
							    if(grupo.getJugadores().equals(8)){
							    	calendarioFaseIList = prepareCalendarioFaseIModeloGrupos8(calendarioFaseIList, grupo);
							    }else if(grupo.getJugadores().equals(7)){
							    	calendarioFaseIList = prepareCalendarioFaseIModeloGrupos7(calendarioFaseIList, grupo);
							    }else if(grupo.getJugadores().equals(6)){
							    	calendarioFaseIList = prepareCalendarioFaseIModeloGrupos6(calendarioFaseIList, grupo);
							    }else if(grupo.getJugadores().equals(5)){
							    	calendarioFaseIList = prepareCalendarioFaseIModeloGrupos5(calendarioFaseIList, grupo);
							    }else if(grupo.getJugadores().equals(4)){
							    	calendarioFaseIList = prepareCalendarioFaseIModeloGrupos4(calendarioFaseIList, grupo);
							    }else if(grupo.getJugadores().equals(3)){
							    	calendarioFaseIList = prepareCalendarioFaseIModeloGrupos3(calendarioFaseIList, grupo);
							    }
							    
							}
							
							//Insertar clasificacion y calendario fase I
							//Clasificacion
							for (ClasificacionFaseI clasificacionFaseI : clasificacionFaseIList) {
								crearCampeonatoMasculinoIndividual.insertarRowClasificacionFaseI(clasificacionFaseI.getInsertRow());
							}
							//Calendario
							for (CalendarioFaseI calendarioFaseI : calendarioFaseIList) {
								crearCampeonatoMasculinoIndividual.insertarRowCalendarioFaseI(calendarioFaseI.getInsertRow());
							}
							
							numerofaseSiguiente = fase.getFaseSiguiente();
						}
						
					}
				}
			}
			
			
			//3. Definir la faseII
			for(Fases fase : getResultListFases()){
				if(fase.getActivo().equals(Activo.SI)){
					if(fase.getNumero().equals(FasesModelo.FASE_II)){
						if(numerofaseSiguiente.equals(fase.getNumero())){
							
						}
					}
				}
			}
			
			//4. Definir la faseOF
			
			Boleras boleraFaseAnterior1 = null;
			Boleras boleraFaseAnterior2 = null;
			
			for(Fases fase : getResultListFases()){
				if(fase.getActivo().equals(Activo.SI)){
					if(fase.getNumero().equals(FasesModelo.FASE_OF)){
						if(numerofaseSiguiente.equals(fase.getNumero())){
							//Recuperar la fase anterior, para averiguar si era grupos escoger el modelo
							Fases faseAnterior = searchFase(numerofaseAnterior);
							//Asignar boleras aleatoriamente
							Boleras boleraAsignar1 = null;
							Boleras boleraAsignar2 = null;
							boolean boleraAsignada1 = false;
							boolean boleraAsignada2 = false;
							LinkedHashMap<Integer, String> bolerasGrupoAsignadasCFMap = new LinkedHashMap<>();
							while(!boleraAsignada1){
								Random rand = new Random();
								boleraAsignar1 = resultListBoleras.get(rand.nextInt(resultListBoleras.size()));
								if(!boleraAsignar1.getId().equals(boleraFinal.getId())){
									if(!bolerasOcupadasOFCFMap.containsKey(boleraAsignar1.getId())){
										if(!bolerasGrupoAsignadasCFMap.containsKey(boleraAsignar1.getId())){
											boleraAsignada1 = true;
											boleraFaseAnterior1 = boleraAsignar1;
										}
									}
								}
							}
							while(!boleraAsignada2){
								Random rand = new Random();
								boleraAsignar2 = resultListBoleras.get(rand.nextInt(resultListBoleras.size()));
								if(!boleraAsignar2.getId().equals(boleraFinal.getId())){
									if(!bolerasOcupadasOFCFMap.containsKey(boleraAsignar2.getId())){
										if(!bolerasGrupoAsignadasCFMap.containsKey(boleraAsignar2.getId())){
											boleraAsignada2 = true;
											boleraFaseAnterior2 = boleraAsignar2;
										}
									}
								}
							}
							//Si en la fase previa eran 4 grupos
							if(faseAnterior.getTipoEnfrentamiento().equals(TipoEnfrentamiento.GRUPOS)){
								if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra.FASE_I_II_G_4)){
									List<CalendarioFaseOF> calendarioFaseOFList = new ArrayList<CalendarioFaseOF>();
									calendarioFaseOFList = prepareCalendarioFaseOFModeloGrupos4(calendarioFaseOFList, faseAnterior, boleraAsignar1, boleraAsignar2);
									//Calendario
									for (CalendarioFaseOF calendarioFaseOF : calendarioFaseOFList) {
										crearCampeonatoMasculinoIndividual.insertarRowCalendarioFaseOF(calendarioFaseOF.getInsertRow());
									}
									numerofaseSiguiente = fase.getFaseSiguiente();
									numerofaseAnterior = fase.getNumero();
								}
								//Si en la fase previa eran 8 grupos
								else if(faseAnterior.getTipoEnfrentamiento().equals(TipoEnfrentamiento.GRUPOS)){
									if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra.FASE_I_II_G_8)){
										List<CalendarioFaseOF> calendarioFaseOFList = new ArrayList<CalendarioFaseOF>();
										calendarioFaseOFList = prepareCalendarioFaseOFModeloGrupos8(calendarioFaseOFList, faseAnterior, boleraAsignar1, boleraAsignar2);
										//Calendario
										for (CalendarioFaseOF calendarioFaseOF : calendarioFaseOFList) {
											crearCampeonatoMasculinoIndividual.insertarRowCalendarioFaseOF(calendarioFaseOF.getInsertRow());
										}
										numerofaseSiguiente = fase.getFaseSiguiente();
										numerofaseAnterior = fase.getNumero();
									}
								}
							}
							
						}
					}
				}
			}
			
			//5. Definir la faseCF
			for(Fases fase : getResultListFases()){
				if(fase.getActivo().equals(Activo.SI)){
					if(fase.getNumero().equals(FasesModelo.FASE_CF)){
						if(numerofaseSiguiente.equals(fase.getNumero())){
							//Recuperar la fase anterior, para averiguar si era grupos escoger el modelo
							Fases faseAnterior = searchFase(numerofaseAnterior);
							//Si en la fase previa eran 4 grupos
							if(faseAnterior.getTipoEnfrentamiento().equals(TipoEnfrentamiento.GRUPOS)){
								if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra.FASE_I_II_G_4)){
									
									//Asignar boleras aleatoriamente
									Boleras boleraAsignar1 = null;
									Boleras boleraAsignar2 = null;
									boolean boleraAsignada1 = false;
									boolean boleraAsignada2 = false;
									LinkedHashMap<Integer, String> bolerasGrupoAsignadasCFMap = new LinkedHashMap<>();
									while(!boleraAsignada1){
										Random rand = new Random();
										boleraAsignar1 = resultListBoleras.get(rand.nextInt(resultListBoleras.size()));
										if(!boleraAsignar1.getId().equals(boleraFinal.getId())){
											if(!bolerasOcupadasOFCFMap.containsKey(boleraAsignar1.getId())){
												if(!bolerasGrupoAsignadasCFMap.containsKey(boleraAsignar1.getId())){
													boleraAsignada1 = true;
												}
											}
										}
									}
									while(!boleraAsignada2){
										Random rand = new Random();
										boleraAsignar2 = resultListBoleras.get(rand.nextInt(resultListBoleras.size()));
										if(!boleraAsignar2.getId().equals(boleraFinal.getId())){
											if(!bolerasOcupadasOFCFMap.containsKey(boleraAsignar2.getId())){
												if(!bolerasGrupoAsignadasCFMap.containsKey(boleraAsignar2.getId())){
													boleraAsignada2 = true;
												}
											}
										}
									}
									
									List<CalendarioFaseCF> calendarioFaseCFList = new ArrayList<CalendarioFaseCF>();
									calendarioFaseCFList = prepareCalendarioFaseCFModeloGrupos4(calendarioFaseCFList, faseAnterior, boleraAsignar1, boleraAsignar2);
									//Calendario
									for (CalendarioFaseCF calendarioFaseCF : calendarioFaseCFList) {
										crearCampeonatoMasculinoIndividual.insertarRowCalendarioFaseCF(calendarioFaseCF.getInsertRow());
									}
									numerofaseSiguiente = fase.getFaseSiguiente();
									numerofaseAnterior = fase.getNumero();
								}
							}
							//Si en la fase previa eran Octavos de final
							else if(fase.getTipoEnfrentamiento().equals(TipoEnfrentamiento.DIRECTO)){
								if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra.FASE_OF)){
									List<CalendarioFaseCF> calendarioFaseCFList = new ArrayList<CalendarioFaseCF>();
									calendarioFaseCFList = prepareCalendarioFaseCFModeloOF(calendarioFaseCFList, faseAnterior, boleraFaseAnterior1, boleraFaseAnterior2);
									//Calendario
									for (CalendarioFaseCF calendarioFaseCF : calendarioFaseCFList) {
										crearCampeonatoMasculinoIndividual.insertarRowCalendarioFaseCF(calendarioFaseCF.getInsertRow());
									}
									numerofaseSiguiente = fase.getFaseSiguiente();
									numerofaseAnterior = fase.getNumero();
								}
							}
						}
					}
				}
			}
			//6. Definir la faseSF
			
			for(Fases fase : getResultListFases()){
				if(fase.getActivo().equals(Activo.SI)){
					if(fase.getNumero().equals(FasesModelo.FASE_SF)){
						if(numerofaseSiguiente.equals(fase.getNumero())){
							//Recuperar la fase anterior, para averiguar si era grupos escoger el modelo
							Fases faseAnterior = searchFase(numerofaseAnterior);
							//Si en la fase previa eran Cuartos de final
							if(fase.getTipoEnfrentamiento().equals(TipoEnfrentamiento.DIRECTO)){
								if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra.FASE_CF)){
									List<CalendarioFaseSF> calendarioFaseSFList = new ArrayList<CalendarioFaseSF>();
									calendarioFaseSFList = prepareCalendarioFaseSFModeloCF(calendarioFaseSFList, faseAnterior, boleraFinal);
									//Calendario
									for (CalendarioFaseSF calendarioFaseSF : calendarioFaseSFList) {
										crearCampeonatoMasculinoIndividual.insertarRowCalendarioFaseSF(calendarioFaseSF.getInsertRow());
									}
									numerofaseSiguiente = fase.getFaseSiguiente();
									numerofaseAnterior = fase.getNumero();
								}
							}
						}
					}
				}
			}
			
			//7. Definir la faseFC
			
			for(Fases fase : getResultListFases()){
				if(fase.getActivo().equals(Activo.SI)){
					if(fase.getNumero().equals(FasesModelo.FASE_FC)){
						if(numerofaseSiguiente.equals(fase.getNumero())){
							//Recuperar la fase anterior, para averiguar si era grupos escoger el modelo
							Fases faseAnterior = searchFase(numerofaseAnterior);
							//Si en la fase previa eran Semifinal
							if(fase.getTipoEnfrentamiento().equals(TipoEnfrentamiento.DIRECTO)){
								if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra.FASE_SF)){
									List<CalendarioFaseFC> calendarioFaseFCList = new ArrayList<CalendarioFaseFC>();
									calendarioFaseFCList = prepareCalendarioFaseFCModeloSF(calendarioFaseFCList, faseAnterior, boleraFinal);
									//Calendario
									for (CalendarioFaseFC calendarioFaseFC : calendarioFaseFCList) {
										crearCampeonatoMasculinoIndividual.insertarRowCalendarioFaseFC(calendarioFaseFC.getInsertRow());
									}
									numerofaseSiguiente = fase.getFaseSiguiente();
								}
							}
						}
					}
				}
			}
			
			//8. Definir la faseFF
			
			for(Fases fase : getResultListFases()){
				if(fase.getActivo().equals(Activo.SI)){
					if(fase.getNumero().equals(FasesModelo.FASE_FF)){
						if(numerofaseSiguiente.equals(fase.getNumero())){
							//Recuperar la fase anterior, para averiguar si era grupos escoger el modelo
							Fases faseAnterior = searchFase(numerofaseAnterior);
							//Si en la fase previa eran Semifinal
							if(fase.getTipoEnfrentamiento().equals(TipoEnfrentamiento.DIRECTO)){
								if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra.FASE_SF)){
									List<CalendarioFaseFF> calendarioFaseFFList = new ArrayList<CalendarioFaseFF>();
									calendarioFaseFFList = prepareCalendarioFaseFFModeloSF(calendarioFaseFFList, faseAnterior, boleraFinal);
									//Calendario
									for (CalendarioFaseFF calendarioFaseFF : calendarioFaseFFList) {
										crearCampeonatoMasculinoIndividual.insertarRowCalendarioFaseFF(calendarioFaseFF.getInsertRow());
										sorteoRealizado = true;
									}
									numerofaseSiguiente = fase.getFaseSiguiente();
								}
							}
						}
					}
				}
			}
		
			if(!sorteoRealizado){
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El Sorteo no se ha realizado.", null));
			}else{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "El Sorteo se ha realizado correctamente.", null));
				crearCampeonatoMasculinoIndividual.actualizarEstadoConfiguracion(EstadosIndividualParejas.CONFIG_ESTADO_SORTEADO_CAMPEONATO);
				//Inicializar variables de nuevo
				leerCampeonatoMasculinoIndividual = new LeerCampeonatoMasculinoIndividualPrimera();
				//Redirigir
				//redirect.redirect(menuView.PAG_OM_ADD_CAMPEONATO_MASCULINO_INDIVIDUAL_PRIMERA);
				
			}
			
			
		}catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El Sorteo no se ha realizado.", null));
		}
		
	}

	private List<CalendarioFaseI> prepareCalendarioFaseIModeloGrupos8(List<CalendarioFaseI> calendarioFaseIList, Grupos grupo) {
		ModeloIndividualGrupos8 modeloGrupos8 = new ModeloIndividualGrupos8();
		Integer id = 1;
		for (Entry<Integer, ModeloIndividualEnfrentamientoGruposFaseI> entryPi : modeloGrupos8.getEnfrentamientosGrupo().entrySet()) {
			Integer key = entryPi.getKey();
			Object valuePi = entryPi.getValue();
			ModeloIndividualEnfrentamientoGruposFaseI enfrentamiento = (ModeloIndividualEnfrentamientoGruposFaseI) valuePi;
			//Agregar calendario
			CalendarioFaseI calendarioFaseI = new CalendarioFaseI();
			calendarioFaseI.setActivo(Activo.SI);
			calendarioFaseI.setCategoriaId(com.bolocelta.bbdd.constants.Categorias.PRIMERA);
			calendarioFaseI.setGrupo(grupo.getId());
			calendarioFaseI.setId(id);
			calendarioFaseI.setJuegosJugador1(0);
			ParticipantesIndividual pi1 = grupo.searchJugadorGrupo(enfrentamiento.getJugador1(), grupo.getId());
			calendarioFaseI.setJugador1Id(pi1.getIdJugador());
			calendarioFaseI.setJuegosJugador2(0);
			ParticipantesIndividual pi2 = grupo.searchJugadorGrupo(enfrentamiento.getJugador2(), grupo.getId());
			calendarioFaseI.setJugador2Id(pi2.getIdJugador());
			calendarioFaseI.setOrden(key);
			calendarioFaseI.setInicia(enfrentamiento.getSorteo());
			calendarioFaseI.setBoleraId(grupo.getBoleraId());
			id++;
			calendarioFaseIList.add(calendarioFaseI);
		}
		return calendarioFaseIList;
	}
	
	private List<CalendarioFaseI> prepareCalendarioFaseIModeloGrupos7(List<CalendarioFaseI> calendarioFaseIList, Grupos grupo) {
		ModeloIndividualGrupos7 modeloGrupos = new ModeloIndividualGrupos7();
		Integer id = 1;
		for (Entry<Integer, ModeloIndividualEnfrentamientoGruposFaseI> entryPi : modeloGrupos.getEnfrentamientosGrupo().entrySet()) {
			Integer key = entryPi.getKey();
			Object valuePi = entryPi.getValue();
			ModeloIndividualEnfrentamientoGruposFaseI enfrentamiento = (ModeloIndividualEnfrentamientoGruposFaseI) valuePi;
			//Agregar calendario
			CalendarioFaseI calendarioFaseI = new CalendarioFaseI();
			calendarioFaseI.setActivo(Activo.SI);
			calendarioFaseI.setCategoriaId(com.bolocelta.bbdd.constants.Categorias.PRIMERA);
			calendarioFaseI.setGrupo(grupo.getId());
			calendarioFaseI.setId(id);
			calendarioFaseI.setJuegosJugador1(0);
			ParticipantesIndividual pi1 = grupo.searchJugadorGrupo(enfrentamiento.getJugador1(), grupo.getId());
			calendarioFaseI.setJugador1Id(pi1.getIdJugador());
			calendarioFaseI.setJuegosJugador2(0);
			ParticipantesIndividual pi2 = grupo.searchJugadorGrupo(enfrentamiento.getJugador2(), grupo.getId());
			calendarioFaseI.setJugador2Id(pi2.getIdJugador());
			calendarioFaseI.setOrden(key);
			calendarioFaseI.setInicia(enfrentamiento.getSorteo());
			calendarioFaseI.setBoleraId(grupo.getBoleraId());
			id++;
			calendarioFaseIList.add(calendarioFaseI);
		}
		return calendarioFaseIList;
	}
	
	private List<CalendarioFaseI> prepareCalendarioFaseIModeloGrupos6(List<CalendarioFaseI> calendarioFaseIList, Grupos grupo) {
		ModeloIndividualGrupos6 modeloGrupos = new ModeloIndividualGrupos6();
		Integer id = 1;
		for (Entry<Integer, ModeloIndividualEnfrentamientoGruposFaseI> entryPi : modeloGrupos.getEnfrentamientosGrupo().entrySet()) {
			Integer key = entryPi.getKey();
			Object valuePi = entryPi.getValue();
			ModeloIndividualEnfrentamientoGruposFaseI enfrentamiento = (ModeloIndividualEnfrentamientoGruposFaseI) valuePi;
			//Agregar calendario
			CalendarioFaseI calendarioFaseI = new CalendarioFaseI();
			calendarioFaseI.setActivo(Activo.SI);
			calendarioFaseI.setCategoriaId(com.bolocelta.bbdd.constants.Categorias.PRIMERA);
			calendarioFaseI.setGrupo(grupo.getId());
			calendarioFaseI.setId(id);
			calendarioFaseI.setJuegosJugador1(0);
			ParticipantesIndividual pi1 = grupo.searchJugadorGrupo(enfrentamiento.getJugador1(), grupo.getId());
			calendarioFaseI.setJugador1Id(pi1.getIdJugador());
			calendarioFaseI.setJuegosJugador2(0);
			ParticipantesIndividual pi2 = grupo.searchJugadorGrupo(enfrentamiento.getJugador2(), grupo.getId());
			calendarioFaseI.setJugador2Id(pi2.getIdJugador());
			calendarioFaseI.setOrden(key);
			calendarioFaseI.setInicia(enfrentamiento.getSorteo());
			calendarioFaseI.setBoleraId(grupo.getBoleraId());
			id++;
			calendarioFaseIList.add(calendarioFaseI);
		}
		return calendarioFaseIList;
	}
	
	private List<CalendarioFaseI> prepareCalendarioFaseIModeloGrupos5(List<CalendarioFaseI> calendarioFaseIList, Grupos grupo) {
		ModeloIndividualGrupos5 modeloGrupos = new ModeloIndividualGrupos5();
		Integer id = 1;
		for (Entry<Integer, ModeloIndividualEnfrentamientoGruposFaseI> entryPi : modeloGrupos.getEnfrentamientosGrupo().entrySet()) {
			Integer key = entryPi.getKey();
			Object valuePi = entryPi.getValue();
			ModeloIndividualEnfrentamientoGruposFaseI enfrentamiento = (ModeloIndividualEnfrentamientoGruposFaseI) valuePi;
			//Agregar calendario
			CalendarioFaseI calendarioFaseI = new CalendarioFaseI();
			calendarioFaseI.setActivo(Activo.SI);
			calendarioFaseI.setCategoriaId(com.bolocelta.bbdd.constants.Categorias.PRIMERA);
			calendarioFaseI.setGrupo(grupo.getId());
			calendarioFaseI.setId(id);
			calendarioFaseI.setJuegosJugador1(0);
			ParticipantesIndividual pi1 = grupo.searchJugadorGrupo(enfrentamiento.getJugador1(), grupo.getId());
			calendarioFaseI.setJugador1Id(pi1.getIdJugador());
			calendarioFaseI.setJuegosJugador2(0);
			ParticipantesIndividual pi2 = grupo.searchJugadorGrupo(enfrentamiento.getJugador2(), grupo.getId());
			calendarioFaseI.setJugador2Id(pi2.getIdJugador());
			calendarioFaseI.setOrden(key);
			calendarioFaseI.setInicia(enfrentamiento.getSorteo());
			calendarioFaseI.setBoleraId(grupo.getBoleraId());
			id++;
			calendarioFaseIList.add(calendarioFaseI);
		}
		return calendarioFaseIList;
	}
	
	private List<CalendarioFaseI> prepareCalendarioFaseIModeloGrupos4(List<CalendarioFaseI> calendarioFaseIList, Grupos grupo) {
		ModeloIndividualGrupos4 modeloGrupos = new ModeloIndividualGrupos4();
		Integer id = 1;
		for (Entry<Integer, ModeloIndividualEnfrentamientoGruposFaseI> entryPi : modeloGrupos.getEnfrentamientosGrupo().entrySet()) {
			Integer key = entryPi.getKey();
			Object valuePi = entryPi.getValue();
			ModeloIndividualEnfrentamientoGruposFaseI enfrentamiento = (ModeloIndividualEnfrentamientoGruposFaseI) valuePi;
			//Agregar calendario
			CalendarioFaseI calendarioFaseI = new CalendarioFaseI();
			calendarioFaseI.setActivo(Activo.SI);
			calendarioFaseI.setCategoriaId(com.bolocelta.bbdd.constants.Categorias.PRIMERA);
			calendarioFaseI.setGrupo(grupo.getId());
			calendarioFaseI.setId(id);
			calendarioFaseI.setJuegosJugador1(0);
			ParticipantesIndividual pi1 = grupo.searchJugadorGrupo(enfrentamiento.getJugador1(), grupo.getId());
			calendarioFaseI.setJugador1Id(pi1.getIdJugador());
			calendarioFaseI.setJuegosJugador2(0);
			ParticipantesIndividual pi2 = grupo.searchJugadorGrupo(enfrentamiento.getJugador2(), grupo.getId());
			calendarioFaseI.setJugador2Id(pi2.getIdJugador());
			calendarioFaseI.setOrden(key);
			calendarioFaseI.setInicia(enfrentamiento.getSorteo());
			calendarioFaseI.setBoleraId(grupo.getBoleraId());
			id++;
			calendarioFaseIList.add(calendarioFaseI);
		}
		return calendarioFaseIList;
	}
	
	private List<CalendarioFaseI> prepareCalendarioFaseIModeloGrupos3(List<CalendarioFaseI> calendarioFaseIList, Grupos grupo) {
		ModeloIndividualGrupos3 modeloGrupos = new ModeloIndividualGrupos3();
		Integer id = 1;
		for (Entry<Integer, ModeloIndividualEnfrentamientoGruposFaseI> entryPi : modeloGrupos.getEnfrentamientosGrupo().entrySet()) {
			Integer key = entryPi.getKey();
			Object valuePi = entryPi.getValue();
			ModeloIndividualEnfrentamientoGruposFaseI enfrentamiento = (ModeloIndividualEnfrentamientoGruposFaseI) valuePi;
			//Agregar calendario
			CalendarioFaseI calendarioFaseI = new CalendarioFaseI();
			calendarioFaseI.setActivo(Activo.SI);
			calendarioFaseI.setCategoriaId(com.bolocelta.bbdd.constants.Categorias.TERCERA);
			calendarioFaseI.setGrupo(grupo.getId());
			calendarioFaseI.setId(id);
			calendarioFaseI.setJuegosJugador1(0);
			ParticipantesIndividual pi1 = grupo.searchJugadorGrupo(enfrentamiento.getJugador1(), grupo.getId());
			calendarioFaseI.setJugador1Id(pi1.getIdJugador());
			calendarioFaseI.setJuegosJugador2(0);
			ParticipantesIndividual pi2 = grupo.searchJugadorGrupo(enfrentamiento.getJugador2(), grupo.getId());
			calendarioFaseI.setJugador2Id(pi2.getIdJugador());
			calendarioFaseI.setOrden(key);
			calendarioFaseI.setInicia(enfrentamiento.getSorteo());
			calendarioFaseI.setBoleraId(grupo.getBoleraId());
			id++;
			calendarioFaseIList.add(calendarioFaseI);
		}
		return calendarioFaseIList;
	}

	public Fases searchFase(Integer numeroFase){
		for(Fases fase : getResultListFases()){
			if(fase.getNumero().equals(numeroFase)){
				return fase;
			}
		}
		return null;
	}
	
	private List<CalendarioFaseOF> prepareCalendarioFaseOFModeloGrupos4(List<CalendarioFaseOF> calendarioFaseOFList, Fases faseAnterior, Boleras boleraAsignar1, Boleras boleraAsignar2) {
		ModeloIndividualOctavosFinal4Grupos modeloOctavosFinal4Grupos = new ModeloIndividualOctavosFinal4Grupos();
		Integer id = 1;

		for (Entry<Integer, ModeloIndividualEnfrentamientoOctavosFinal> entry : modeloOctavosFinal4Grupos.getEnfrentamientosOctavosFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloIndividualEnfrentamientoOctavosFinal enfrentamiento = (ModeloIndividualEnfrentamientoOctavosFinal) value;
			//Agregar calendario
			CalendarioFaseOF calendarioFaseOF = new CalendarioFaseOF();
			calendarioFaseOF.setId(id);
			calendarioFaseOF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias.PRIMERA);
			calendarioFaseOF.setNumeroEnfrentamiento(id);
			calendarioFaseOF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseOF.setJugador1Id(0);
			calendarioFaseOF.setJuegosJugador1P1(0);
			calendarioFaseOF.setJuegosJugador1P2(0);
			calendarioFaseOF.setJuegosJugador1P3(0);
			calendarioFaseOF.setJuegosJugador2P1(0);
			calendarioFaseOF.setJuegosJugador2P2(0);
			calendarioFaseOF.setJuegosJugador2P3(0);
			calendarioFaseOF.setJugador2Id(0);
			calendarioFaseOF.setOrden(key);
			calendarioFaseOF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseOF.setActivo(Activo.SI);
			calendarioFaseOF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseOF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseOF.setGrupoProcedenciaJugador1(enfrentamiento.getGrupoProcedenciaJugador1());
			calendarioFaseOF.setPosicionProcedenciaJugador1(enfrentamiento.getPosicionProcedenciaJugador1());
			calendarioFaseOF.setGrupoProcedenciaJugador2(enfrentamiento.getGrupoProcedenciaJugador2());
			calendarioFaseOF.setPosicionProcedenciaJugador2(enfrentamiento.getPosicionProcedenciaJugador2());
			
			//Asignar la bolera por parte del cuadro
			if(id > 0 && id <5 ){
				calendarioFaseOF.setBoleraId(boleraAsignar1.getId());
			}else if(id > 4 && id <9 ){
				calendarioFaseOF.setBoleraId(boleraAsignar2.getId());
			}
			id++;
			calendarioFaseOFList.add(calendarioFaseOF);
		}
		return calendarioFaseOFList;
	}
	
	private List<CalendarioFaseOF> prepareCalendarioFaseOFModeloGrupos8(List<CalendarioFaseOF> calendarioFaseOFList, Fases faseAnterior, Boleras boleraAsignar1, Boleras boleraAsignar2) {
		ModeloIndividualOctavosFinal8Grupos modeloOctavosFinal8Grupos = new ModeloIndividualOctavosFinal8Grupos();
		Integer id = 1;

		for (Entry<Integer, ModeloIndividualEnfrentamientoOctavosFinal> entry : modeloOctavosFinal8Grupos.getEnfrentamientosOctavosFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloIndividualEnfrentamientoOctavosFinal enfrentamiento = (ModeloIndividualEnfrentamientoOctavosFinal) value;
			//Agregar calendario
			CalendarioFaseOF calendarioFaseOF = new CalendarioFaseOF();
			calendarioFaseOF.setId(id);
			calendarioFaseOF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias.PRIMERA);
			calendarioFaseOF.setNumeroEnfrentamiento(id);
			calendarioFaseOF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseOF.setJugador1Id(0);
			calendarioFaseOF.setJuegosJugador1P1(0);
			calendarioFaseOF.setJuegosJugador1P2(0);
			calendarioFaseOF.setJuegosJugador1P3(0);
			calendarioFaseOF.setJuegosJugador2P1(0);
			calendarioFaseOF.setJuegosJugador2P2(0);
			calendarioFaseOF.setJuegosJugador2P3(0);
			calendarioFaseOF.setJugador2Id(0);
			calendarioFaseOF.setOrden(key);
			calendarioFaseOF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseOF.setActivo(Activo.SI);
			calendarioFaseOF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseOF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseOF.setGrupoProcedenciaJugador1(enfrentamiento.getGrupoProcedenciaJugador1());
			calendarioFaseOF.setPosicionProcedenciaJugador1(enfrentamiento.getPosicionProcedenciaJugador1());
			calendarioFaseOF.setGrupoProcedenciaJugador2(enfrentamiento.getGrupoProcedenciaJugador2());
			calendarioFaseOF.setPosicionProcedenciaJugador2(enfrentamiento.getPosicionProcedenciaJugador2());
			
			//Asignar la bolera por parte del cuadro
			if(id > 0 && id <5 ){
				calendarioFaseOF.setBoleraId(boleraAsignar1.getId());
			}else if(id > 4 && id <9 ){
				calendarioFaseOF.setBoleraId(boleraAsignar2.getId());
			}
			id++;
			calendarioFaseOFList.add(calendarioFaseOF);
		}
		return calendarioFaseOFList;
	}
	
	private List<CalendarioFaseCF> prepareCalendarioFaseCFModeloGrupos4(List<CalendarioFaseCF> calendarioFaseCFList, Fases faseAnterior, Boleras boleraAsignar1, Boleras boleraAsignar2) {
		ModeloIndividualCuartosFinal4Grupos modeloCuartosFinal4Grupos = new ModeloIndividualCuartosFinal4Grupos();
		Integer id = 1;
		
		for (Entry<Integer, ModeloIndividualEnfrentamientoCuartosFinal> entry : modeloCuartosFinal4Grupos.getEnfrentamientosCuartosFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloIndividualEnfrentamientoCuartosFinal enfrentamiento = (ModeloIndividualEnfrentamientoCuartosFinal) value;
			//Agregar calendario
			CalendarioFaseCF calendarioFaseCF = new CalendarioFaseCF();
			calendarioFaseCF.setId(id);
			calendarioFaseCF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias.PRIMERA);
			calendarioFaseCF.setNumeroEnfrentamiento(id);
			calendarioFaseCF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseCF.setJugador1Id(0);
			calendarioFaseCF.setJuegosJugador1P1(0);
			calendarioFaseCF.setJuegosJugador1P2(0);
			calendarioFaseCF.setJuegosJugador1P3(0);
			calendarioFaseCF.setJuegosJugador2P1(0);
			calendarioFaseCF.setJuegosJugador2P2(0);
			calendarioFaseCF.setJuegosJugador2P3(0);
			calendarioFaseCF.setJugador2Id(0);
			calendarioFaseCF.setOrden(key);
			calendarioFaseCF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseCF.setActivo(Activo.SI);
			calendarioFaseCF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseCF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseCF.setGrupoProcedenciaJugador1(enfrentamiento.getGrupoProcedenciaJugador1());
			calendarioFaseCF.setPosicionProcedenciaJugador1(enfrentamiento.getPosicionProcedenciaJugador1());
			calendarioFaseCF.setGrupoProcedenciaJugador2(enfrentamiento.getGrupoProcedenciaJugador2());
			calendarioFaseCF.setPosicionProcedenciaJugador2(enfrentamiento.getPosicionProcedenciaJugador2());
			
			//Asignar la bolera por parte del cuadro
			if(id > 0 && id <5 ){
				calendarioFaseCF.setBoleraId(boleraAsignar1.getId());
			}else if(id > 4 && id <9 ){
				calendarioFaseCF.setBoleraId(boleraAsignar2.getId());
			}
			id++;
			calendarioFaseCFList.add(calendarioFaseCF);
		}
		return calendarioFaseCFList;
	}
	
	private List<CalendarioFaseCF> prepareCalendarioFaseCFModeloOF(List<CalendarioFaseCF> calendarioFaseCFList, Fases faseAnterior, Boleras boleraAsignar1, Boleras boleraAsignar2) {
		ModeloIndividualCuartosFinal4OF modeloCuartosFinal4OF = new ModeloIndividualCuartosFinal4OF();
		Integer id = 1;
		
		for (Entry<Integer, ModeloIndividualEnfrentamientoCuartosFinal> entry : modeloCuartosFinal4OF.getEnfrentamientosCuartosFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloIndividualEnfrentamientoCuartosFinal enfrentamiento = (ModeloIndividualEnfrentamientoCuartosFinal) value;
			//Agregar calendario
			CalendarioFaseCF calendarioFaseCF = new CalendarioFaseCF();
			calendarioFaseCF.setId(id);
			calendarioFaseCF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias.PRIMERA);
			calendarioFaseCF.setNumeroEnfrentamiento(id);
			calendarioFaseCF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseCF.setJugador1Id(0);
			calendarioFaseCF.setJuegosJugador1P1(0);
			calendarioFaseCF.setJuegosJugador1P2(0);
			calendarioFaseCF.setJuegosJugador1P3(0);
			calendarioFaseCF.setJuegosJugador2P1(0);
			calendarioFaseCF.setJuegosJugador2P2(0);
			calendarioFaseCF.setJuegosJugador2P3(0);
			calendarioFaseCF.setJugador2Id(0);
			calendarioFaseCF.setOrden(key);
			calendarioFaseCF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseCF.setActivo(Activo.SI);
			calendarioFaseCF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseCF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseCF.setGrupoProcedenciaJugador1(enfrentamiento.getGrupoProcedenciaJugador1());
			calendarioFaseCF.setPosicionProcedenciaJugador1(enfrentamiento.getPosicionProcedenciaJugador1());
			calendarioFaseCF.setGrupoProcedenciaJugador2(enfrentamiento.getGrupoProcedenciaJugador2());
			calendarioFaseCF.setPosicionProcedenciaJugador2(enfrentamiento.getPosicionProcedenciaJugador2());
			
			//Asignar la bolera por parte del cuadro
			if(id > 0 && id <3 ){
				calendarioFaseCF.setBoleraId(boleraAsignar1.getId());
			}else if(id > 2 && id <5 ){
				calendarioFaseCF.setBoleraId(boleraAsignar2.getId());
			}
			id++;
			calendarioFaseCFList.add(calendarioFaseCF);
		}
		return calendarioFaseCFList;
	}
	
	private List<CalendarioFaseSF> prepareCalendarioFaseSFModeloCF(List<CalendarioFaseSF> calendarioFaseSFList, Fases faseAnterior, Boleras boleraFinal) {
		ModeloIndividualSemiFinal4CF modeloSemiFinal4CF = new ModeloIndividualSemiFinal4CF();
		Integer id = 1;
		
		for (Entry<Integer, ModeloIndividualEnfrentamientoSemiFinal> entry : modeloSemiFinal4CF.getEnfrentamientosSemiFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloIndividualEnfrentamientoSemiFinal enfrentamiento = (ModeloIndividualEnfrentamientoSemiFinal) value;
			//Agregar calendario
			CalendarioFaseSF calendarioFaseSF = new CalendarioFaseSF();
			calendarioFaseSF.setId(id);
			calendarioFaseSF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias.PRIMERA);
			calendarioFaseSF.setNumeroEnfrentamiento(id);
			calendarioFaseSF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseSF.setJugador1Id(0);
			calendarioFaseSF.setJuegosJugador1P1(0);
			calendarioFaseSF.setJuegosJugador1P2(0);
			calendarioFaseSF.setJuegosJugador1P3(0);
			calendarioFaseSF.setJuegosJugador2P1(0);
			calendarioFaseSF.setJuegosJugador2P2(0);
			calendarioFaseSF.setJuegosJugador2P3(0);
			calendarioFaseSF.setJugador2Id(0);
			calendarioFaseSF.setOrden(key);
			calendarioFaseSF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseSF.setActivo(Activo.SI);
			calendarioFaseSF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseSF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseSF.setGrupoProcedenciaJugador1(enfrentamiento.getGrupoProcedenciaJugador1());
			calendarioFaseSF.setPosicionProcedenciaJugador1(enfrentamiento.getPosicionProcedenciaJugador1());
			calendarioFaseSF.setGrupoProcedenciaJugador2(enfrentamiento.getGrupoProcedenciaJugador2());
			calendarioFaseSF.setPosicionProcedenciaJugador2(enfrentamiento.getPosicionProcedenciaJugador2());
			
			//Asignar la bolera por parte del cuadro
			calendarioFaseSF.setBoleraId(boleraFinal.getId());
			id++;
			calendarioFaseSFList.add(calendarioFaseSF);
		}
		return calendarioFaseSFList;
	}
	
	private List<CalendarioFaseFC> prepareCalendarioFaseFCModeloSF(List<CalendarioFaseFC> calendarioFaseFCList, Fases faseAnterior, Boleras boleraFinal) {
		ModeloIndividualFinalConsolacion4SF modeloFinalConsolacion4SF = new ModeloIndividualFinalConsolacion4SF();
		Integer id = 1;
		
		for (Entry<Integer, ModeloIndividualEnfrentamientoFinalConsolacion> entry : modeloFinalConsolacion4SF.getEnfrentamientosFinalConsolacion().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloIndividualEnfrentamientoFinalConsolacion enfrentamiento = (ModeloIndividualEnfrentamientoFinalConsolacion) value;
			//Agregar calendario
			CalendarioFaseFC calendarioFaseFC = new CalendarioFaseFC();
			calendarioFaseFC.setId(id);
			calendarioFaseFC.setCategoriaId(com.bolocelta.bbdd.constants.Categorias.PRIMERA);
			calendarioFaseFC.setNumeroEnfrentamiento(id);
			calendarioFaseFC.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseFC.setJugador1Id(0);
			calendarioFaseFC.setJuegosJugador1P1(0);
			calendarioFaseFC.setJuegosJugador1P2(0);
			calendarioFaseFC.setJuegosJugador1P3(0);
			calendarioFaseFC.setJuegosJugador2P1(0);
			calendarioFaseFC.setJuegosJugador2P2(0);
			calendarioFaseFC.setJuegosJugador2P3(0);
			calendarioFaseFC.setJugador2Id(0);
			calendarioFaseFC.setOrden(key);
			calendarioFaseFC.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseFC.setActivo(Activo.SI);
			calendarioFaseFC.setInicia(enfrentamiento.getSorteo());
			calendarioFaseFC.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseFC.setGrupoProcedenciaJugador1(enfrentamiento.getGrupoProcedenciaJugador1());
			calendarioFaseFC.setPosicionProcedenciaJugador1(enfrentamiento.getPosicionProcedenciaJugador1());
			calendarioFaseFC.setGrupoProcedenciaJugador2(enfrentamiento.getGrupoProcedenciaJugador2());
			calendarioFaseFC.setPosicionProcedenciaJugador2(enfrentamiento.getPosicionProcedenciaJugador2());
			
			//Asignar la bolera por parte del cuadro
			calendarioFaseFC.setBoleraId(boleraFinal.getId());
			id++;
			calendarioFaseFCList.add(calendarioFaseFC);
		}
		return calendarioFaseFCList;
	}
	
	private List<CalendarioFaseFF> prepareCalendarioFaseFFModeloSF(List<CalendarioFaseFF> calendarioFaseFFList, Fases faseAnterior, Boleras boleraFinal) {
		ModeloIndividualFinal4SF modeloFinal4SF = new ModeloIndividualFinal4SF();
		Integer id = 1;
		
		for (Entry<Integer, ModeloIndividualEnfrentamientoFinal> entry : modeloFinal4SF.getEnfrentamientosFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloIndividualEnfrentamientoFinal enfrentamiento = (ModeloIndividualEnfrentamientoFinal) value;
			//Agregar calendario
			CalendarioFaseFF calendarioFaseFF = new CalendarioFaseFF();
			calendarioFaseFF.setId(id);
			calendarioFaseFF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias.PRIMERA);
			calendarioFaseFF.setNumeroEnfrentamiento(id);
			calendarioFaseFF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseFF.setJugador1Id(0);
			calendarioFaseFF.setJuegosJugador1P1(0);
			calendarioFaseFF.setJuegosJugador1P2(0);
			calendarioFaseFF.setJuegosJugador1P3(0);
			calendarioFaseFF.setJuegosJugador2P1(0);
			calendarioFaseFF.setJuegosJugador2P2(0);
			calendarioFaseFF.setJuegosJugador2P3(0);
			calendarioFaseFF.setJugador2Id(0);
			calendarioFaseFF.setOrden(key);
			calendarioFaseFF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseFF.setActivo(Activo.SI);
			calendarioFaseFF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseFF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseFF.setGrupoProcedenciaJugador1(enfrentamiento.getGrupoProcedenciaJugador1());
			calendarioFaseFF.setPosicionProcedenciaJugador1(enfrentamiento.getPosicionProcedenciaJugador1());
			calendarioFaseFF.setGrupoProcedenciaJugador2(enfrentamiento.getGrupoProcedenciaJugador2());
			calendarioFaseFF.setPosicionProcedenciaJugador2(enfrentamiento.getPosicionProcedenciaJugador2());
			
			//Asignar la bolera por parte del cuadro
			calendarioFaseFF.setBoleraId(boleraFinal.getId());
			id++;
			calendarioFaseFFList.add(calendarioFaseFF);
		}
		return calendarioFaseFFList;
	}

	public String getBolerasOcupadasFI() {
		return bolerasOcupadasFI;
	}

	public void setBolerasOcupadasFI(String bolerasOcupadasFI) {
		this.bolerasOcupadasFI = bolerasOcupadasFI;
	}

	public String getBolerasOcupadasOFCF() {
		return bolerasOcupadasOFCF;
	}

	public void setBolerasOcupadasOFCF(String bolerasOcupadasOFCF) {
		this.bolerasOcupadasOFCF = bolerasOcupadasOFCF;
	}
	
	public void doBorrarSorteo(){
		crearCampeonatoMasculinoIndividual.borrarSorteo();
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "El Sorteo se ha borrado correctamente. Se puede volver a sortear.", null));
		//Inicializar variables de nuevo
		leerCampeonatoMasculinoIndividual = new LeerCampeonatoMasculinoIndividualPrimera();
		//Redirigir
		//redirect.redirect(menuView.PAG_OM_ADD_CAMPEONATO_MASCULINO_INDIVIDUAL_PRIMERA);
	}
	
	public void doConfirmarSorteoCampeonato(){
		crearCampeonatoMasculinoIndividual.actualizarEstadoConfiguracion(EstadosIndividualParejas.CONFIG_ESTADO_FINALIZADO);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "El Sorteo se ha confirmado correctamente.", null));
		//Inicializar variables de nuevo
		leerCampeonatoMasculinoIndividual = new LeerCampeonatoMasculinoIndividualPrimera();
		//Redirigir
		//redirect.redirect(menuView.PAG_OM_ADD_CAMPEONATO_MASCULINO_INDIVIDUAL_PRIMERA);		
	}
	
	

}
