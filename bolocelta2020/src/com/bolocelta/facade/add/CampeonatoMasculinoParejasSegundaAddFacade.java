package com.bolocelta.facade.add;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.bolocelta.application.MenuView;
import com.bolocelta.application.Redirect;
import com.bolocelta.application.utils.DateUtils;
import com.bolocelta.application.utils.NumberUtils;
import com.bolocelta.bbdd.constants.Activo;
import com.bolocelta.bbdd.constants.EstadosIndividualParejas;
import com.bolocelta.bbdd.constants.FasesModelo;
import com.bolocelta.bbdd.constants.GruposLetra;
import com.bolocelta.bbdd.constants.TipoEnfrentamiento;
import com.bolocelta.bbdd.constants.structure.EstructuraCampeonatoMasculinoParejasSegunda;
import com.bolocelta.bbdd.createTable.CrearCampeonatoMasculinoParejasSegunda;
import com.bolocelta.bbdd.readTables.LeerBoleras;
import com.bolocelta.bbdd.readTables.LeerCampeonatoMasculinoParejasPrimera;
import com.bolocelta.bbdd.readTables.LeerCampeonatoMasculinoParejasSegunda;
import com.bolocelta.bbdd.readTables.LeerCampeonatoMasculinoParejasTercera;
import com.bolocelta.entities.Boleras;
import com.bolocelta.entities.Categorias;
import com.bolocelta.entities.Configuracion;
import com.bolocelta.entities.Fases;
import com.bolocelta.entities.Jugadores;
import com.bolocelta.entities.Parejas;
import com.bolocelta.entities.ParticipantesParejas;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasCuartosFinal2Grupos;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasCuartosFinal4Grupos;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasCuartosFinal4OF;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasEnfrentamientoCuartosFinal;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasEnfrentamientoFinal;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasEnfrentamientoFinalConsolacion;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasEnfrentamientoGruposFaseI;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasEnfrentamientoOctavosFinal;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasEnfrentamientoSemiFinal;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasFinal4SF;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasFinalConsolacion4SF;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasGrupos3;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasGrupos4;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasGrupos5;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasGrupos6;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasGrupos7;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasGrupos8;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasOctavosFinal4Grupos;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasOctavosFinal8Grupos;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasSemiFinal4CF;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseCF;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseFC;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseFF;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseI;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseII;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseOF;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseSF;
import com.bolocelta.entities.sorteos.parejas.ClasificacionFaseI;
import com.bolocelta.entities.sorteos.parejas.ClasificacionFaseII;
import com.bolocelta.entities.sorteos.parejas.Grupos;
import com.bolocelta.facade.edit.FichaEquipoFacade;

@Named
@ManagedBean
@RequestScoped
public class CampeonatoMasculinoParejasSegundaAddFacade implements Serializable{
	
	private static final int PAREJA_EQUIPOS_MIXTO = 0;

	@Inject
	private Redirect redirect;
	
	@Inject
	private MenuView menuView;

	@Inject
	private FichaEquipoFacade fichaEquipoFacade;
	
	private static final long serialVersionUID = 1L;
	
	private LeerCampeonatoMasculinoParejasPrimera leerCampeonatoMasculinoParejasPrimera = new LeerCampeonatoMasculinoParejasPrimera();
	private CrearCampeonatoMasculinoParejasSegunda crearCampeonatoMasculinoParejas = new CrearCampeonatoMasculinoParejasSegunda();
	private LeerCampeonatoMasculinoParejasSegunda leerCampeonatoMasculinoParejasSegunda = new LeerCampeonatoMasculinoParejasSegunda();
	private LeerCampeonatoMasculinoParejasTercera leerCampeonatoMasculinoParejasTercera = new LeerCampeonatoMasculinoParejasTercera();
	
	
	private LeerBoleras leerBoleras = new LeerBoleras();
	
	private String diaFechaMaxInscripcion = null;
	private String mesFechaMaxInscripcion = null;
	private String anyoFechaMaxInscripcion = null;
	
	private Date fechaMaxInscripcion = null;
	private String estadoCampeonato = null;
	private String bolera = null;
	private String observacionesCampeonato1 = null;
	private String observacionesCampeonato2 = null;
	private String observacionesCampeonato3 = null;
	private String observacionesCampeonato4 = null;
	private String observacionesCampeonato5 = null;
	private String observacionesCampeonato6 = null;
	private String observacionesCampeonato7 = null;
	private String bolerasOcupadasFI = null;
	private String bolerasOcupadasOFCF = null;
	
	private List<Configuracion> resultListConfiguracion = null;
	private List<Fases> resultListFases = null;
	private List<ParticipantesParejas> resultListParticipantes = null;
	private List<ParticipantesParejas> resultListParticipantesByEquipo = null;
	
	public CampeonatoMasculinoParejasSegundaAddFacade() {
		Date fechaMaxInscripcion = leerCampeonatoMasculinoParejasSegunda.getFechaMaxInscripcion();
		if(fechaMaxInscripcion != null){
			setDiaFechaMaxInscripcion(String.valueOf(fechaMaxInscripcion.getDate()));
			setMesFechaMaxInscripcion(String.valueOf(fechaMaxInscripcion.getMonth()+1));
			setAnyoFechaMaxInscripcion(String.valueOf(fechaMaxInscripcion.getYear()+1900));
		}
		setEstadoCampeonato(leerCampeonatoMasculinoParejasSegunda.getEstadoCampeonato());
		setBolera(leerCampeonatoMasculinoParejasSegunda.getBoleraFinal());
		setObservacionesCampeonato1(leerCampeonatoMasculinoParejasSegunda.getObservacionesCampeonato1());
		setObservacionesCampeonato2(leerCampeonatoMasculinoParejasSegunda.getObservacionesCampeonato2());
		setObservacionesCampeonato3(leerCampeonatoMasculinoParejasSegunda.getObservacionesCampeonato3());
		setObservacionesCampeonato4(leerCampeonatoMasculinoParejasSegunda.getObservacionesCampeonato4());
		setObservacionesCampeonato5(leerCampeonatoMasculinoParejasSegunda.getObservacionesCampeonato5());
		setObservacionesCampeonato6(leerCampeonatoMasculinoParejasSegunda.getObservacionesCampeonato6());
		setObservacionesCampeonato7(leerCampeonatoMasculinoParejasSegunda.getObservacionesCampeonato7());
		setBolerasOcupadasFI(leerCampeonatoMasculinoParejasSegunda.getBolerasOcupadasFI());
		setBolerasOcupadasOFCF(leerCampeonatoMasculinoParejasSegunda.getBolerasOcupadasOFCF());
	}

	public List<Configuracion> getResultListConfiguracion() {
		if(resultListConfiguracion == null){
			resultListConfiguracion = (List<Configuracion>) leerCampeonatoMasculinoParejasSegunda.listResultConfig();
		}
		return resultListConfiguracion;
	}
	
	public List<Fases> getResultListFases() {
		if(resultListFases == null){
			resultListFases = (List<Fases>) leerCampeonatoMasculinoParejasSegunda.listResultFases();
		}
		return resultListFases;
	}
	
	public List<ParticipantesParejas> getResultListParticipantes() {
		if(resultListParticipantes == null){
			resultListParticipantes = (List<ParticipantesParejas>) leerCampeonatoMasculinoParejasSegunda.listResultParticipantesOrderByEquipo();
		}
		return resultListParticipantes;
	}
	
	public List<ParticipantesParejas> getResultListParticipantesByEquipo(Integer idEquipo) {
		if(resultListParticipantesByEquipo == null){
			
			resultListParticipantesByEquipo = new ArrayList<>();

			if(resultListParticipantes == null){
				resultListParticipantes = (List<ParticipantesParejas>) leerCampeonatoMasculinoParejasSegunda.listResultParticipantesOrderByEquipo();
			}
			
			for (ParticipantesParejas participantesParejas : resultListParticipantes) {
				if(participantesParejas != null && participantesParejas.getPareja() != null && participantesParejas.getPareja().getIdJugador1() != null && participantesParejas.getPareja().getIdJugador2() != null){
					if(participantesParejas.getPareja().getJugador1().getEquipoId().equals(idEquipo) || participantesParejas.getPareja().getJugador2().getEquipoId().equals(idEquipo)){
						resultListParticipantesByEquipo.add(participantesParejas);
					}
				}
			}
		}
		
		return resultListParticipantesByEquipo;
	}
	
	//NUEVO
	public List<ParticipantesParejas> getResultListParticipantesByEquipos(Set<Integer> keySetequipoMasParejasQueGruposLibreGrupo) {
		if(resultListParticipantes == null){
			resultListParticipantes = (List<ParticipantesParejas>) leerCampeonatoMasculinoParejasSegunda.listResultParticipantesOrderByEquipo();
		}
		List<ParticipantesParejas> resultListParticipantesParejasMasEquiposQueGrupos = new ArrayList<>();
		for (ParticipantesParejas participantesParejas : resultListParticipantes) {
			if(keySetequipoMasParejasQueGruposLibreGrupo.contains(participantesParejas.getPareja().getJugador1().getEquipoId()) && keySetequipoMasParejasQueGruposLibreGrupo.contains(participantesParejas.getPareja().getJugador2().getEquipoId())){
				resultListParticipantesParejasMasEquiposQueGrupos.add(participantesParejas);
			}
		}
		return resultListParticipantesParejasMasEquiposQueGrupos;
	}
	//F-NUEVO
	
	public Integer getTotalRowsParticipantes(){
		if(resultListParticipantes == null){
			getResultListParticipantes();
		}
		if(resultListParticipantes != null){
			return resultListParticipantes.size();
		}
		return 0;
	}
	
	public Integer getMaxRowsPaginator(){
		return 8;
	}
	
	public boolean isEnabledPaginatorParticipantes(){
		return (getResultListParticipantes() != null && getResultListParticipantes() != null) ? 
					(getResultListParticipantes().size() > getMaxRowsPaginator()) : false;
	}
	
	//Manejo de estados
	public boolean isEstadoCampeonatoSinCrear(){
		return leerCampeonatoMasculinoParejasSegunda.isEstadoCampeonatoSinCrear();
	}
	
	public boolean isAbrirInscripciones(){
		return leerCampeonatoMasculinoParejasSegunda.isAbrirInscripciones();
	}
	
	public boolean isCerrarInscripciones(){
		return leerCampeonatoMasculinoParejasSegunda.isCerrarInscripciones();
	}
	
	public boolean isInscripcionesCerradas(){
		return leerCampeonatoMasculinoParejasSegunda.isInscripcionesCerradas();
	}
	
	public boolean isFasesCerradas(){
		return leerCampeonatoMasculinoParejasSegunda.isFasesCerradas();
	}
	
	public boolean isSorteoRealizado(){
		return leerCampeonatoMasculinoParejasSegunda.isSorteoRealizado();
	}
	
	public boolean isSorteoFinalizado(){
		return leerCampeonatoMasculinoParejasSegunda.isSorteoFinalizado();
	}
	
	//Acciones para los diferentes estados
	
	private boolean validateFecha() {
		if(getAnyoFechaMaxInscripcion() != null && getMesFechaMaxInscripcion() != null && getDiaFechaMaxInscripcion() != null){
			if(NumberUtils.isNumber(getAnyoFechaMaxInscripcion()) && NumberUtils.isNumber(getMesFechaMaxInscripcion()) && NumberUtils.isNumber(getDiaFechaMaxInscripcion())){
				Integer dia = Integer.valueOf(getDiaFechaMaxInscripcion());
				if(dia > 31){
					return false;
				}
				Integer mes = Integer.valueOf(getMesFechaMaxInscripcion());
				if(mes > 12){
					return false;
				}
				Integer anyo = Integer.valueOf(getAnyoFechaMaxInscripcion());
				String fecha = (dia < 10 ? "0" + dia : dia) + "/" + (mes < 10 ? "0" + mes : mes) + "/" + anyo;
				Date fechaConvert = DateUtils.convertStringToDate(fecha);
				if(fechaConvert == null){
					return false;
				}else{
					setFechaMaxInscripcion(fechaConvert);
					return true;
				}
			}
		}
		return false;
		
	}
	
	public void doCrearBaseCampeonato(){
		
		boolean error = validateFecha();
		if(error){
			crearCampeonatoMasculinoParejas.crearBaseCampeonato(getFechaMaxInscripcion(), EstadosIndividualParejas.CONFIG_ESTADO_CREAR, getBolera());
			//Inicializar variables de nuevo
			leerCampeonatoMasculinoParejasSegunda = new LeerCampeonatoMasculinoParejasSegunda();
			//Redirigir
			redirect.redirect(menuView.PAG_OM_ADD_CAMPEONATO_MASCULINO_PAREJAS_SEGUNDA);
		}else{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "La Fecha max. inscripci�n no es valida.", null));
		}
	}
	
	public void doAbrirInscripcionesCampeonato(){
		crearCampeonatoMasculinoParejas.actualizarEstadoConfiguracion(EstadosIndividualParejas.CONFIG_ESTADO_ABRIR_INSCRIPCIONES);
		//Inicializar variables de nuevo
		leerCampeonatoMasculinoParejasSegunda = new LeerCampeonatoMasculinoParejasSegunda();
		//Redirigir
		redirect.redirect(menuView.PAG_OM_ADD_CAMPEONATO_MASCULINO_PAREJAS_SEGUNDA);
	}
	
	public void doCerrarInscripcionesCampeonato(){
		crearCampeonatoMasculinoParejas.actualizarEstadoConfiguracion(EstadosIndividualParejas.CONFIG_ESTADO_CERRAR_INSCRIPCIONES);
		//Inicializar variables de nuevo
		leerCampeonatoMasculinoParejasSegunda = new LeerCampeonatoMasculinoParejasSegunda();
		//Redirigir
		redirect.redirect(menuView.PAG_OM_ADD_CAMPEONATO_MASCULINO_PAREJAS_SEGUNDA);
	}
	
	public void doInscribirPareja(Jugadores jugador1, Jugadores jugador2, Categorias categoria, String usuario, String activo){
		
		Parejas parejaNew = new Parejas();
		parejaNew.setIdJugador1(jugador1.getId());
		parejaNew.setIdJugador2(jugador2.getId());
		parejaNew.setJugador1(jugador1);
		parejaNew.setJugador2(jugador2);
		
		crearCampeonatoMasculinoParejas.inscribirNewPareja(parejaNew, categoria, usuario, activo);
		//Inicializar variables de nuevo
		fichaEquipoFacade.refrescarParticipantesParejas();
		this.resultListParticipantesByEquipo = null;
		this.resultListParticipantes = null;
		leerCampeonatoMasculinoParejasSegunda = new LeerCampeonatoMasculinoParejasSegunda();
		getResultListParticipantesByEquipo(fichaEquipoFacade.getEquipo().getId());
		redirect.getInscripcionParejas();

	}
	
	public void doDesinscribirPareja(ParticipantesParejas pareja,String usuario, String activo){
		crearCampeonatoMasculinoParejas.desinscribirPareja(pareja, usuario, activo);
		//Inicializar variables de nuevo
		fichaEquipoFacade.refrescarParticipantesParejas();
		this.resultListParticipantesByEquipo = null;
		this.resultListParticipantes = null;
		leerCampeonatoMasculinoParejasSegunda = new LeerCampeonatoMasculinoParejasSegunda();
		getResultListParticipantesByEquipo(fichaEquipoFacade.getEquipo().getId());
		redirect.getInscripcionParejas();
	}
	
	public Date getFechaMaxInscripcion() {
		return fechaMaxInscripcion;
	}

	public void setFechaMaxInscripcion(Date fechaMaxInscripcion) {
		this.fechaMaxInscripcion = fechaMaxInscripcion;
	}
	
	public String getBolera() {
		return bolera;
	}

	public void setBolera(String bolera) {
		this.bolera = bolera;
	}
	
	public String getNombreBolera(){
		if(this.bolera != null && this.bolera.length() > 0){
			return leerBoleras.read(Integer.valueOf(this.bolera)).getBolera();
		}
		return "";
	}
	
	public String getObservacionesCampeonato1() {
		return observacionesCampeonato1;
	}

	public void setObservacionesCampeonato1(String observacionesCampeonato1) {
		this.observacionesCampeonato1 = observacionesCampeonato1;
	}
	
	public String getObservacionesCampeonato2() {
		return observacionesCampeonato2;
	}

	public void setObservacionesCampeonato2(String observacionesCampeonato2) {
		this.observacionesCampeonato2 = observacionesCampeonato2;
	}
	
	public String getObservacionesCampeonato3() {
		return observacionesCampeonato3;
	}

	public void setObservacionesCampeonato3(String observacionesCampeonato3) {
		this.observacionesCampeonato3 = observacionesCampeonato3;
	}
	
	public String getObservacionesCampeonato4() {
		return observacionesCampeonato4;
	}

	public void setObservacionesCampeonato4(String observacionesCampeonato4) {
		this.observacionesCampeonato4 = observacionesCampeonato4;
	}
	
	public String getObservacionesCampeonato5() {
		return observacionesCampeonato5;
	}

	public void setObservacionesCampeonato5(String observacionesCampeonato5) {
		this.observacionesCampeonato5 = observacionesCampeonato5;
	}
	
	public String getObservacionesCampeonato6() {
		return observacionesCampeonato6;
	}

	public void setObservacionesCampeonato6(String observacionesCampeonato6) {
		this.observacionesCampeonato6 = observacionesCampeonato6;
	}
	
	public String getObservacionesCampeonato7() {
		return observacionesCampeonato7;
	}

	public void setObservacionesCampeonato7(String observacionesCampeonato7) {
		this.observacionesCampeonato7 = observacionesCampeonato7;
	}

	public String getEstadoCampeonato() {
		return estadoCampeonato;
	}

	public void setEstadoCampeonato(String estadoCampeonato) {
		this.estadoCampeonato = estadoCampeonato;
	}
	
	public String getDiaFechaMaxInscripcion() {
		return diaFechaMaxInscripcion;
	}

	public void setDiaFechaMaxInscripcion(String diaFechaMaxInscripcion) {
		this.diaFechaMaxInscripcion = diaFechaMaxInscripcion;
	}

	public String getMesFechaMaxInscripcion() {
		return mesFechaMaxInscripcion;
	}

	public void setMesFechaMaxInscripcion(String mesFechaMaxInscripcion) {
		this.mesFechaMaxInscripcion = mesFechaMaxInscripcion;
	}

	public String getAnyoFechaMaxInscripcion() {
		return anyoFechaMaxInscripcion;
	}

	public void setAnyoFechaMaxInscripcion(String anyoFechaMaxInscripcion) {
		this.anyoFechaMaxInscripcion = anyoFechaMaxInscripcion;
	}
	
	public void doCambiarActivarFase(Fases fase){
		if(fase.getActivo().equalsIgnoreCase(Activo.SI)){
			fase.setActivo(Activo.NO);
		}else if(fase.getActivo().equalsIgnoreCase(Activo.NO)){
			fase.setActivo(Activo.SI);
		}
		crearCampeonatoMasculinoParejas.actualizarActivoFase(fase);
		leerCampeonatoMasculinoParejasSegunda = new LeerCampeonatoMasculinoParejasSegunda();
	}
	
	public void doActualizarDatos(Fases fase){
		crearCampeonatoMasculinoParejas.actualizarFase(fase);
		leerCampeonatoMasculinoParejasSegunda = new LeerCampeonatoMasculinoParejasSegunda();
	}

	public void doCerrarFasesCampeonato(){
		//Inicializar variables de nuevo
		leerCampeonatoMasculinoParejasSegunda = new LeerCampeonatoMasculinoParejasSegunda();
		for(Fases fase : getResultListFases()){
			if(fase.getActivo().equals(Activo.SI)){
				if(fase.getNumero().equals(FasesModelo.FASE_I)){
					crearCampeonatoMasculinoParejas.crearClasificacionFaseI();
					crearCampeonatoMasculinoParejas.crearCalendarioFaseI();
				}else if(fase.getNumero().equals(FasesModelo.FASE_II)){
					crearCampeonatoMasculinoParejas.crearCalendarioFaseII();
					crearCampeonatoMasculinoParejas.crearClasificacionFaseII();
				}else if(fase.getNumero().equals(FasesModelo.FASE_OF)){
					crearCampeonatoMasculinoParejas.crearCalendarioFaseOF();
				}else if(fase.getNumero().equals(FasesModelo.FASE_CF)){
					crearCampeonatoMasculinoParejas.crearCalendarioFaseCF();
				}else if(fase.getNumero().equals(FasesModelo.FASE_SF)){
					crearCampeonatoMasculinoParejas.crearCalendarioFaseSF();
				}else if(fase.getNumero().equals(FasesModelo.FASE_FC)){
					crearCampeonatoMasculinoParejas.crearCalendarioFaseFC();
				}else if(fase.getNumero().equals(FasesModelo.FASE_FF)){
					crearCampeonatoMasculinoParejas.crearCalendarioFaseFF();
				}
			}
		}
		crearCampeonatoMasculinoParejas.actualizarEstadoConfiguracion(EstadosIndividualParejas.CONFIG_ESTADO_CERRAR_FASES);
		//Inicializar variables de nuevo
		leerCampeonatoMasculinoParejasSegunda = new LeerCampeonatoMasculinoParejasSegunda();
		//Redirigir
		redirect.redirect(menuView.PAG_OM_ADD_CAMPEONATO_MASCULINO_PAREJAS_SEGUNDA);
	}
	
	public void doRealizarSorteoCampeonato(){
		
		boolean sorteoRealizado = false;
		
		try {
		
			//Asignar las observaciones del campeonato
			EstructuraCampeonatoMasculinoParejasSegunda ecmi = new EstructuraCampeonatoMasculinoParejasSegunda();
			crearCampeonatoMasculinoParejas.updateRow(ecmi.getEstructuraObservacionesCampeonato1(), ecmi.getEstructuraObservacionesCampeonato1().getFila(), observacionesCampeonato1);
			crearCampeonatoMasculinoParejas.updateRow(ecmi.getEstructuraObservacionesCampeonato2(), ecmi.getEstructuraObservacionesCampeonato2().getFila(), observacionesCampeonato2);
			crearCampeonatoMasculinoParejas.updateRow(ecmi.getEstructuraObservacionesCampeonato3(), ecmi.getEstructuraObservacionesCampeonato3().getFila(), observacionesCampeonato3);
			crearCampeonatoMasculinoParejas.updateRow(ecmi.getEstructuraObservacionesCampeonato4(), ecmi.getEstructuraObservacionesCampeonato4().getFila(), observacionesCampeonato4);
			crearCampeonatoMasculinoParejas.updateRow(ecmi.getEstructuraObservacionesCampeonato5(), ecmi.getEstructuraObservacionesCampeonato5().getFila(), observacionesCampeonato5);
			crearCampeonatoMasculinoParejas.updateRow(ecmi.getEstructuraObservacionesCampeonato6(), ecmi.getEstructuraObservacionesCampeonato6().getFila(), observacionesCampeonato6);
			crearCampeonatoMasculinoParejas.updateRow(ecmi.getEstructuraObservacionesCampeonato7(), ecmi.getEstructuraObservacionesCampeonato7().getFila(), observacionesCampeonato7);
			crearCampeonatoMasculinoParejas.updateRow(ecmi.getEstructuraBolerasOcupadasFI(), ecmi.getEstructuraBolerasOcupadasFI().getFila(), bolerasOcupadasFI);
			crearCampeonatoMasculinoParejas.updateRow(ecmi.getEstructuraBolerasOcupadasOFCF(), ecmi.getEstructuraBolerasOcupadasOFCF().getFila(), bolerasOcupadasOFCF);
			
			
			
			//Inicializar variables de nuevo
			leerCampeonatoMasculinoParejasSegunda = new LeerCampeonatoMasculinoParejasSegunda();
			//Crear los mapas para el sorteo
			//FaseI
			LinkedHashMap<Integer, CalendarioFaseI> calendarioFaseIMap = new LinkedHashMap<>();
			LinkedHashMap<Integer, ClasificacionFaseI> clasificacionFaseIMap = new LinkedHashMap<>();
			//FaseII
			LinkedHashMap<Integer, CalendarioFaseII> calendarioFaseIIMap = new LinkedHashMap<>();
			LinkedHashMap<Integer, ClasificacionFaseII> clasificacionFaseIIMap = new LinkedHashMap<>();
			//FaseOF
			LinkedHashMap<Integer, CalendarioFaseOF> calendarioFaseOFMap = new LinkedHashMap<>();
			//FaseCF
			LinkedHashMap<Integer, CalendarioFaseCF> calendarioFaseCFMap = new LinkedHashMap<>();
			//FaseSF
			LinkedHashMap<Integer, CalendarioFaseSF> calendarioFaseSFMap = new LinkedHashMap<>();
			//FaseFC
			LinkedHashMap<Integer, CalendarioFaseFC> calendarioFaseFCMap = new LinkedHashMap<>();
			//FaseFF
			LinkedHashMap<Integer, CalendarioFaseFF> calendarioFaseFFMap = new LinkedHashMap<>();
			
			//1. Obtener las boleras disponibles, en principio solo las federadas.
			LinkedHashMap<Integer, String> bolerasGrupoAsignadasMap = new LinkedHashMap<>();
			LeerBoleras leerBoleras = new LeerBoleras();
			List<Boleras> resultListBoleras = (List<Boleras>) leerBoleras.listResultFederadas();
			
			//Fase primera
			Integer numerofaseAnterior = 1;
			Integer numerofaseSiguiente = 1;
			
			//Bolera de la final
			Boleras boleraFinal = null;
			for (Configuracion configuracion : getResultListConfiguracion()) {
				if(configuracion.getNombre().equals("Bolera Final")){
					boleraFinal = leerBoleras.read(Integer.valueOf(configuracion.getValor()));
				}
			}
			
			//Bolera Ocupadas FI
			LinkedHashMap<Integer, Boleras> bolerasOcupadasFIMap =  new LinkedHashMap<>();
			for (Configuracion configuracion : getResultListConfiguracion()) {
				if(configuracion.getNombre().equals("Boleras Ocupadas FI")){
					String bolerasOcupadas = configuracion.getValor();
					String[] bolerasOcupadasList = bolerasOcupadas.split("-");
					for (String bolera : bolerasOcupadasList) {
						Integer idBolera = Integer.valueOf(bolera);
						if(idBolera != null && idBolera > 0){
							Boleras boleraObject = leerBoleras.read(idBolera);
							if(boleraObject != null){
								bolerasOcupadasFIMap.put(idBolera, boleraObject);
							}
						}
					}
				}
			}
			
			//Boleras Ocupadas OFCF
			LinkedHashMap<Integer, Boleras> bolerasOcupadasOFCFMap =  new LinkedHashMap<>();
			for (Configuracion configuracion : getResultListConfiguracion()) {
				if(configuracion.getNombre().equals("Boleras Ocupadas OFCF")){
					String bolerasOcupadas = configuracion.getValor();
					String[] bolerasOcupadasList = bolerasOcupadas.split("-");
					for (String bolera : bolerasOcupadasList) {
						Integer idBolera = Integer.valueOf(bolera);
						if(idBolera != null && idBolera > 0){
							Boleras boleraObject = leerBoleras.read(idBolera);
							if(boleraObject != null){
								bolerasOcupadasOFCFMap.put(idBolera, boleraObject);
							}
						}
					}
				}
			}
			
			//2. Definir la faseI
			for(Fases fase : getResultListFases()){
				if(fase.getActivo().equals(Activo.SI)){
					if(fase.getNumero().equals(FasesModelo.FASE_I)){
						if(numerofaseSiguiente.equals(fase.getNumero())){
						
							//Objetos
							//Grupos Fase I
							LinkedHashMap<String, Grupos> gruposFaseIMap = new LinkedHashMap<>();
							//Parejas Asignados
							LinkedHashMap<Integer, ParticipantesParejas> parejasAsignados = new LinkedHashMap<>();
							
							//2.1. Obtener el numero de grupos de la fase
							Integer numeroGruposFase = fase.getNumeroEnfrentamientos();
							Integer numeroGruposFaseAsignados = 0;
							
							//2.2. Obtener el numero de participantes para saber el numero de parejas por grupo y los asignados al grupo para
							Integer numeroParticipantes = getResultListParticipantes().size();
							Integer parejasPorGrupoAsignados = 0;
							
							
							//NUEVO
							//Equipos mismos o mas parejas que grupos (idEquipo, numeroParejas)
							LinkedHashMap<Integer, Integer> equipoMasParejasQueGrupos = new LinkedHashMap<>();
							for (ParticipantesParejas participante : getResultListParticipantes()) {
								
								if(participante.getPareja().getJugador1().getEquipoId().equals(participante.getPareja().getJugador2().getEquipoId())){
									if(equipoMasParejasQueGrupos.containsKey(participante.getPareja().getJugador1().getEquipoId())){
										Integer parejasEquipoValue = equipoMasParejasQueGrupos.get(participante.getPareja().getJugador1().getEquipoId());
										equipoMasParejasQueGrupos.put(participante.getPareja().getJugador1().getEquipoId(), parejasEquipoValue + 1 );
									}else{
										equipoMasParejasQueGrupos.put(participante.getPareja().getJugador1().getEquipoId(), 1);
									}
								}else{
									if(!equipoMasParejasQueGrupos.containsKey(PAREJA_EQUIPOS_MIXTO)){
										equipoMasParejasQueGrupos.put(PAREJA_EQUIPOS_MIXTO, 1);
									}
								}
								
							}
							//Lista de equipos con mas parejas que grupos
							LinkedHashMap<Integer, Integer> equipoMasParejasQueGruposLibre = (LinkedHashMap<Integer, Integer>) equipoMasParejasQueGrupos.clone();
							//Limpiar equipos que tengan menos parejas que grupos
							Set<Integer> keySet = equipoMasParejasQueGrupos.keySet();
							for (Integer idEquipoKey : keySet) {
								Integer parejaEquipoValue = equipoMasParejasQueGrupos.get(idEquipoKey);
								if(parejaEquipoValue < numeroGruposFase){
									equipoMasParejasQueGruposLibre.remove(idEquipoKey);
								}
							}
							//F-NUEVO
							
							
							
							//2.3. Crear los grupos a participar
							Integer gruposCreados = 1;
							while(gruposCreados <= numeroGruposFase){
								Grupos grupos = new Grupos();
								grupos.setId(GruposLetra.getGruposLetraFaseI(gruposCreados));
								grupos.setCategoriaId(com.bolocelta.bbdd.constants.Categorias.SEGUNDA);
								grupos.setFecha(fase.getFecha());
								grupos.setHora(fase.getHora());
								//Asignar bolera aleatoriamente
								Boleras boleraAsignar = null;
								boolean boleraAsignada = false;
								
								//Quito las boleras ocupadas
								List<Boleras> resultListBolerasLibres = new ArrayList<>();
								for (Boleras boleras : resultListBoleras) {
									if(!bolerasOcupadasFIMap.containsKey(boleras.getId())){
										resultListBolerasLibres.add(boleras);
									}
								}
								
								
								
								while(!boleraAsignada){
									Random rand = new Random();
									boleraAsignar = resultListBolerasLibres.get(rand.nextInt(resultListBolerasLibres.size()));
									//if(!boleraAsignar.getId().equals(boleraFinal.getId())){
										if(!bolerasOcupadasFIMap.containsKey(boleraAsignar.getId())){
											if(!bolerasGrupoAsignadasMap.containsKey(boleraAsignar.getId())){
												boleraAsignada = true;
											}
										}
									//}
								}
								grupos.setBoleraId(boleraAsignar.getId());
								
								
								//Obtener el numero de parejas del grupo
								BigDecimal parejasPendientesAsignar = new BigDecimal((numeroParticipantes) - parejasPorGrupoAsignados);
								BigDecimal gruposPendientesAsignar = new BigDecimal(numeroGruposFase - numeroGruposFaseAsignados);
								BigDecimal parejasPorGrupoCalculo = parejasPendientesAsignar.divide(gruposPendientesAsignar, RoundingMode.UP);
								grupos.setParejas(parejasPorGrupoCalculo.intValue());
								
								//Agregar los parejas para cada grupo a sorteo
								//A cada grupo sortear los parejas, se obtienen por equipos de mayor numero de parejas a menor
								//Limitaciones: 1-  Se han de repartir los parejas de equipo por grupo, 
								//					si hay mas parejas del equipo por grupo se asigna a alguno de los grupos
								Integer parejasAsignadosGrupo = 0;
								
								//NUEVO
								//Lista de equipos con mas parejas que grupos para cada grupo
								LinkedHashMap<Integer, Integer> equipoMasParejasQueGruposLibreGrupo = (LinkedHashMap<Integer, Integer>) equipoMasParejasQueGruposLibre.clone();
								//F-NUEVO
								
								Integer reintentosEquipoRepetido = 0;
								while(parejasAsignadosGrupo < parejasPorGrupoCalculo.intValue()){
									
									Random rand = new Random();
									
									
									//NUEVO
									ParticipantesParejas parejasSorteo = null;
									
									//Primero buscar parejas pertenecientes a los equipos que tienen los mismos o mas parejas que grupos
									Set<Integer> keySetequipoMasParejasQueGruposLibreGrupo = equipoMasParejasQueGruposLibreGrupo.keySet();
									if(keySetequipoMasParejasQueGruposLibreGrupo.size() > 0){
										List<ParticipantesParejas> list = getResultListParticipantesByEquipos(keySetequipoMasParejasQueGruposLibreGrupo);
										boolean parejaCorrecto = false;
										while(!parejaCorrecto){
											parejasSorteo = list.get(rand.nextInt(list.size()));
											if(!parejasAsignados.containsKey(parejasSorteo.getPareja().getId())){
												equipoMasParejasQueGruposLibreGrupo.remove(parejasSorteo.getPareja().getJugador1().getEquipoId());
												parejaCorrecto = true;
											}
										}
									}else{
										boolean parejaCorrecto = false;
										while(!parejaCorrecto){
											parejasSorteo = getResultListParticipantes().get(rand.nextInt(getResultListParticipantes().size()));
											if(!parejasAsignados.containsKey(parejasSorteo.getPareja().getId())){
												if(equipoMasParejasQueGruposLibreGrupo.size() > 0){
													equipoMasParejasQueGruposLibreGrupo.remove(parejasSorteo.getPareja().getJugador1().getEquipoId());
												}
												parejaCorrecto = true;
											}
										}
										
									}
									
									//F-NUEVO
									
									
									
									if(!grupos.getParejasGrupoList().containsKey(parejasSorteo.getPareja().getId())){
										if(!parejasAsignados.containsKey(parejasSorteo.getPareja().getId())){
											if( !grupos.isExisteParejaMismoEquipo(parejasSorteo.getPareja().getJugador1().getEquipoId(), equipoMasParejasQueGrupos, numeroGruposFase)){
												parejasSorteo.setNumeroParejaGrupo(parejasAsignadosGrupo + 1);
												grupos.getParejasGrupoList().put(parejasSorteo.getPareja().getId(), parejasSorteo);
												parejasAsignadosGrupo++;
												parejasAsignados.put(parejasSorteo.getPareja().getId(), parejasSorteo);
											}else{
												if(reintentosEquipoRepetido >50){
													parejasSorteo.setNumeroParejaGrupo(parejasAsignadosGrupo + 1);
													grupos.getParejasGrupoList().put(parejasSorteo.getPareja().getId(), parejasSorteo);
													parejasAsignadosGrupo++;
													reintentosEquipoRepetido = 0;
													parejasAsignados.put(parejasSorteo.getPareja().getId(), parejasSorteo);
												}else{
													reintentosEquipoRepetido++;
												}
											}
										}
									}
								}
								
								//Agregar a los mapas
								gruposFaseIMap.put(grupos.getId(), grupos);
								bolerasGrupoAsignadasMap.put(boleraAsignar.getId(), grupos.getId());
								
								//Actualizar variables
								gruposCreados++;
								numeroGruposFaseAsignados++;
								parejasPorGrupoAsignados += parejasPorGrupoCalculo.intValue();
								
								
							}
							
							//A�adir los datos a el calendario y clasificacion de la fase I
							List<CalendarioFaseI> calendarioFaseIList = new ArrayList<CalendarioFaseI>();
							List<ClasificacionFaseI> clasificacionFaseIList = new ArrayList<ClasificacionFaseI>();
							//Se recorren los grupos obtenidos
							//Se a�aden a la clasificacion
							for (Entry<String, Grupos> entry : gruposFaseIMap.entrySet()) {
							    Object value = entry.getValue();
							    Grupos grupo = (Grupos) value;
							    for (Entry<Integer, ParticipantesParejas> entryPi : grupo.getParejasGrupoList().entrySet()) {
							    	Object valuePi = entryPi.getValue();
							    	ParticipantesParejas participantesParejas = (ParticipantesParejas) valuePi;
							    	
							    	//Agrego a clasificacion el pareja para el grupo
							    	ClasificacionFaseI cf1 = new ClasificacionFaseI();
							    	cf1.setCategoriaId(com.bolocelta.bbdd.constants.Categorias.SEGUNDA);
							    	cf1.setGrupo(grupo.getId());
							    	cf1.setId(participantesParejas.getNumeroParejaGrupo());
							    	cf1.setParejaId(participantesParejas.getPareja().getId());
							    	cf1.setPj(0);
							    	cf1.setPg(0);
							    	cf1.setPe(0);
							    	cf1.setPp(0);
							    	cf1.setPf(0);
							    	cf1.setPc(0);
							    	cf1.setPt(0);
							    	clasificacionFaseIList.add(cf1);
							    }
							}
							//Se a�aden al calendario
							for (Entry<String, Grupos> entry : gruposFaseIMap.entrySet()) {
							    Object value = entry.getValue();
							    Grupos grupo = (Grupos) value;
							    //Recorro los enfrentamientos para el modelo del grupo
							    if(grupo.getParejas().equals(8)){
							    	calendarioFaseIList = prepareCalendarioFaseIModeloGrupos8(calendarioFaseIList, grupo);
							    }else if(grupo.getParejas().equals(7)){
							    	calendarioFaseIList = prepareCalendarioFaseIModeloGrupos7(calendarioFaseIList, grupo);
							    }else if(grupo.getParejas().equals(6)){
							    	calendarioFaseIList = prepareCalendarioFaseIModeloGrupos6(calendarioFaseIList, grupo);
							    }else if(grupo.getParejas().equals(5)){
							    	calendarioFaseIList = prepareCalendarioFaseIModeloGrupos5(calendarioFaseIList, grupo);
							    }else if(grupo.getParejas().equals(4)){
							    	calendarioFaseIList = prepareCalendarioFaseIModeloGrupos4(calendarioFaseIList, grupo);
							    }else if(grupo.getParejas().equals(3)){
							    	calendarioFaseIList = prepareCalendarioFaseIModeloGrupos3(calendarioFaseIList, grupo);
							    }
							    
							}
							
							//Insertar clasificacion y calendario fase I
							//Clasificacion
							for (ClasificacionFaseI clasificacionFaseI : clasificacionFaseIList) {
								crearCampeonatoMasculinoParejas.insertarRowClasificacionFaseI(clasificacionFaseI.getInsertRow());
							}
							//Calendario
							for (CalendarioFaseI calendarioFaseI : calendarioFaseIList) {
								crearCampeonatoMasculinoParejas.insertarRowCalendarioFaseI(calendarioFaseI.getInsertRow());
							}
							
							numerofaseSiguiente = fase.getFaseSiguiente();
						}
						
					}
				}
			}
			
			
			//3. Definir la faseII
			for(Fases fase : getResultListFases()){
				if(fase.getActivo().equals(Activo.SI)){
					if(fase.getNumero().equals(FasesModelo.FASE_II)){
						if(numerofaseSiguiente.equals(fase.getNumero())){
							
						}
					}
				}
			}
			
			//4. Definir la faseOF
			
			Boleras boleraFaseAnterior1 = null;
			Boleras boleraFaseAnterior2 = null;
			
			for(Fases fase : getResultListFases()){
				if(fase.getActivo().equals(Activo.SI)){
					if(fase.getNumero().equals(FasesModelo.FASE_OF)){
						if(numerofaseSiguiente.equals(fase.getNumero())){
							//Recuperar la fase anterior, para averiguar si era grupos escoger el modelo
							Fases faseAnterior = searchFase(numerofaseAnterior);
							//Asignar boleras aleatoriamente
							Boleras boleraAsignar1 = null;
							Boleras boleraAsignar2 = null;
							boolean boleraAsignada1 = false;
							boolean boleraAsignada2 = false;
							LinkedHashMap<Integer, String> bolerasGrupoAsignadasCFMap = new LinkedHashMap<>();
							while(!boleraAsignada1){
								Random rand = new Random();
								boleraAsignar1 = resultListBoleras.get(rand.nextInt(resultListBoleras.size()));
								if(!boleraAsignar1.getId().equals(boleraFinal.getId())){
									if(!bolerasOcupadasOFCFMap.containsKey(boleraAsignar1.getId())){
										if(!bolerasGrupoAsignadasCFMap.containsKey(boleraAsignar1.getId())){
											boleraAsignada1 = true;
											boleraFaseAnterior1 = boleraAsignar1;
										}
									}
								}
							}
							while(!boleraAsignada2){
								Random rand = new Random();
								boleraAsignar2 = resultListBoleras.get(rand.nextInt(resultListBoleras.size()));
								if(!boleraAsignar2.getId().equals(boleraFinal.getId())){
									if(!bolerasOcupadasOFCFMap.containsKey(boleraAsignar2.getId())){
										if(!bolerasGrupoAsignadasCFMap.containsKey(boleraAsignar2.getId())){
											boleraAsignada2 = true;
											boleraFaseAnterior2 = boleraAsignar2;
										}
									}
								}
							}
							//Si en la fase previa eran 4 grupos
							if(faseAnterior.getTipoEnfrentamiento().equals(TipoEnfrentamiento.GRUPOS)){
								if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra.FASE_I_II_G_4)){
									List<CalendarioFaseOF> calendarioFaseOFList = new ArrayList<CalendarioFaseOF>();
									calendarioFaseOFList = prepareCalendarioFaseOFModeloGrupos4(calendarioFaseOFList, faseAnterior, boleraAsignar1, boleraAsignar2);
									//Calendario
									for (CalendarioFaseOF calendarioFaseOF : calendarioFaseOFList) {
										crearCampeonatoMasculinoParejas.insertarRowCalendarioFaseOF(calendarioFaseOF.getInsertRow());
									}
									numerofaseSiguiente = fase.getFaseSiguiente();
									numerofaseAnterior = fase.getNumero();
								}
								//Si en la fase previa eran 8 grupos
								else if(faseAnterior.getTipoEnfrentamiento().equals(TipoEnfrentamiento.GRUPOS)){
									if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra.FASE_I_II_G_8)){
										List<CalendarioFaseOF> calendarioFaseOFList = new ArrayList<CalendarioFaseOF>();
										calendarioFaseOFList = prepareCalendarioFaseOFModeloGrupos8(calendarioFaseOFList, faseAnterior, boleraAsignar1, boleraAsignar2);
										//Calendario
										for (CalendarioFaseOF calendarioFaseOF : calendarioFaseOFList) {
											crearCampeonatoMasculinoParejas.insertarRowCalendarioFaseOF(calendarioFaseOF.getInsertRow());
										}
										numerofaseSiguiente = fase.getFaseSiguiente();
										numerofaseAnterior = fase.getNumero();
									}
								}
							}
							
						}
					}
				}
			}
			
			//5. Definir la faseCF
			for(Fases fase : getResultListFases()){
				if(fase.getActivo().equals(Activo.SI)){
					if(fase.getNumero().equals(FasesModelo.FASE_CF)){
						if(numerofaseSiguiente.equals(fase.getNumero())){
							//Recuperar la fase anterior, para averiguar si era grupos escoger el modelo
							Fases faseAnterior = searchFase(numerofaseAnterior);

							
							//Si en la fase previa eran 2 grupos
							if(faseAnterior.getTipoEnfrentamiento().equals(TipoEnfrentamiento.GRUPOS)){
								if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra.FASE_I_II_G_2)){
									
									//Asignar boleras aleatoriamente
									Boleras boleraAsignar1 = null;
									Boleras boleraAsignar2 = null;
									boolean boleraAsignada1 = false;
									boolean boleraAsignada2 = false;
									LinkedHashMap<Integer, String> bolerasGrupoAsignadasCFMap = new LinkedHashMap<>();
									while(!boleraAsignada1){
										Random rand = new Random();
										boleraAsignar1 = resultListBoleras.get(rand.nextInt(resultListBoleras.size()));
										if(!boleraAsignar1.getId().equals(boleraFinal.getId())){
											if(!bolerasOcupadasOFCFMap.containsKey(boleraAsignar1.getId())){
												if(!bolerasGrupoAsignadasCFMap.containsKey(boleraAsignar1.getId())){
													boleraAsignada1 = true;
												}
											}
										}
									}
									while(!boleraAsignada2){
										Random rand = new Random();
										boleraAsignar2 = resultListBoleras.get(rand.nextInt(resultListBoleras.size()));
										if(!boleraAsignar2.getId().equals(boleraFinal.getId())){
											if(!bolerasOcupadasOFCFMap.containsKey(boleraAsignar2.getId())){
												if(!bolerasGrupoAsignadasCFMap.containsKey(boleraAsignar2.getId())){
													boleraAsignada2 = true;
												}
											}
										}
									}
									
									List<CalendarioFaseCF> calendarioFaseCFList = new ArrayList<CalendarioFaseCF>();
									calendarioFaseCFList = prepareCalendarioFaseCFModeloGrupos2(calendarioFaseCFList, faseAnterior, boleraAsignar1, boleraAsignar2);
									//Calendario
									for (CalendarioFaseCF calendarioFaseCF : calendarioFaseCFList) {
										crearCampeonatoMasculinoParejas.insertarRowCalendarioFaseCF(calendarioFaseCF.getInsertRow());
									}
									numerofaseSiguiente = fase.getFaseSiguiente();
									numerofaseAnterior = fase.getNumero();
								}
							
							
								//Si en la fase previa eran 4 grupos
								else if(faseAnterior.getTipoEnfrentamiento().equals(TipoEnfrentamiento.GRUPOS)){
									if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra.FASE_I_II_G_4)){
										
										//Asignar boleras aleatoriamente
										Boleras boleraAsignar1 = null;
										Boleras boleraAsignar2 = null;
										boolean boleraAsignada1 = false;
										boolean boleraAsignada2 = false;
										LinkedHashMap<Integer, String> bolerasGrupoAsignadasCFMap = new LinkedHashMap<>();
										while(!boleraAsignada1){
											Random rand = new Random();
											boleraAsignar1 = resultListBoleras.get(rand.nextInt(resultListBoleras.size()));
											if(!boleraAsignar1.getId().equals(boleraFinal.getId())){
												if(!bolerasOcupadasOFCFMap.containsKey(boleraAsignar1.getId())){
													if(!bolerasGrupoAsignadasCFMap.containsKey(boleraAsignar1.getId())){
														boleraAsignada1 = true;
													}
												}
											}
										}
										while(!boleraAsignada2){
											Random rand = new Random();
											boleraAsignar2 = resultListBoleras.get(rand.nextInt(resultListBoleras.size()));
											if(!boleraAsignar2.getId().equals(boleraFinal.getId())){
												if(!bolerasOcupadasOFCFMap.containsKey(boleraAsignar2.getId())){
													if(!bolerasGrupoAsignadasCFMap.containsKey(boleraAsignar2.getId())){
														boleraAsignada2 = true;
													}
												}
											}
										}
										
										List<CalendarioFaseCF> calendarioFaseCFList = new ArrayList<CalendarioFaseCF>();
										calendarioFaseCFList = prepareCalendarioFaseCFModeloGrupos4(calendarioFaseCFList, faseAnterior, boleraAsignar1, boleraAsignar2);
										//Calendario
										for (CalendarioFaseCF calendarioFaseCF : calendarioFaseCFList) {
											crearCampeonatoMasculinoParejas.insertarRowCalendarioFaseCF(calendarioFaseCF.getInsertRow());
										}
										numerofaseSiguiente = fase.getFaseSiguiente();
										numerofaseAnterior = fase.getNumero();
									}
								}
								//Si en la fase previa eran Octavos de final
								else if(fase.getTipoEnfrentamiento().equals(TipoEnfrentamiento.DIRECTO)){
									if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra.FASE_OF)){
										List<CalendarioFaseCF> calendarioFaseCFList = new ArrayList<CalendarioFaseCF>();
										calendarioFaseCFList = prepareCalendarioFaseCFModeloOF(calendarioFaseCFList, faseAnterior, boleraFaseAnterior1, boleraFaseAnterior2);
										//Calendario
										for (CalendarioFaseCF calendarioFaseCF : calendarioFaseCFList) {
											crearCampeonatoMasculinoParejas.insertarRowCalendarioFaseCF(calendarioFaseCF.getInsertRow());
										}
										numerofaseSiguiente = fase.getFaseSiguiente();
										numerofaseAnterior = fase.getNumero();
									}
								}
							}
						}
					}
				}
			}
			//6. Definir la faseSF
			
			for(Fases fase : getResultListFases()){
				if(fase.getActivo().equals(Activo.SI)){
					if(fase.getNumero().equals(FasesModelo.FASE_SF)){
						if(numerofaseSiguiente.equals(fase.getNumero())){
							//Recuperar la fase anterior, para averiguar si era grupos escoger el modelo
							Fases faseAnterior = searchFase(numerofaseAnterior);
							//Si en la fase previa eran Cuartos de final
							if(fase.getTipoEnfrentamiento().equals(TipoEnfrentamiento.DIRECTO)){
								if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra.FASE_CF)){
									List<CalendarioFaseSF> calendarioFaseSFList = new ArrayList<CalendarioFaseSF>();
									calendarioFaseSFList = prepareCalendarioFaseSFModeloCF(calendarioFaseSFList, faseAnterior, boleraFinal);
									//Calendario
									for (CalendarioFaseSF calendarioFaseSF : calendarioFaseSFList) {
										crearCampeonatoMasculinoParejas.insertarRowCalendarioFaseSF(calendarioFaseSF.getInsertRow());
									}
									numerofaseSiguiente = fase.getFaseSiguiente();
									numerofaseAnterior = fase.getNumero();
								}
							}
						}
					}
				}
			}
			
			//7. Definir la faseFC
			
			for(Fases fase : getResultListFases()){
				if(fase.getActivo().equals(Activo.SI)){
					if(fase.getNumero().equals(FasesModelo.FASE_FC)){
						//if(numerofaseSiguiente.equals(fase.getNumero())){
							//Recuperar la fase anterior, para averiguar si era grupos escoger el modelo
							Fases faseAnterior = searchFase(numerofaseAnterior);
							//Si en la fase previa eran Semifinal
							if(fase.getTipoEnfrentamiento().equals(TipoEnfrentamiento.DIRECTO)){
								if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra.FASE_SF)){
									List<CalendarioFaseFC> calendarioFaseFCList = new ArrayList<CalendarioFaseFC>();
									calendarioFaseFCList = prepareCalendarioFaseFCModeloSF(calendarioFaseFCList, faseAnterior, boleraFinal);
									//Calendario
									for (CalendarioFaseFC calendarioFaseFC : calendarioFaseFCList) {
										crearCampeonatoMasculinoParejas.insertarRowCalendarioFaseFC(calendarioFaseFC.getInsertRow());
									}
									//numerofaseSiguiente = fase.getFaseSiguiente();
								}
							}
						//}
					}
				}
			}
			
			//8. Definir la faseFF
			
			for(Fases fase : getResultListFases()){
				if(fase.getActivo().equals(Activo.SI)){
					if(fase.getNumero().equals(FasesModelo.FASE_FF)){
						if(numerofaseSiguiente.equals(fase.getNumero())){
							//Recuperar la fase anterior, para averiguar si era grupos escoger el modelo
							Fases faseAnterior = searchFase(numerofaseAnterior);
							//Si en la fase previa eran Semifinal
							if(fase.getTipoEnfrentamiento().equals(TipoEnfrentamiento.DIRECTO)){
								if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra.FASE_SF)){
									List<CalendarioFaseFF> calendarioFaseFFList = new ArrayList<CalendarioFaseFF>();
									calendarioFaseFFList = prepareCalendarioFaseFFModeloSF(calendarioFaseFFList, faseAnterior, boleraFinal);
									//Calendario
									for (CalendarioFaseFF calendarioFaseFF : calendarioFaseFFList) {
										crearCampeonatoMasculinoParejas.insertarRowCalendarioFaseFF(calendarioFaseFF.getInsertRow());
										sorteoRealizado = true;
									}
									numerofaseSiguiente = fase.getFaseSiguiente();
								}
							}
						}
					}
				}
			}
		
			if(!sorteoRealizado){
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El Sorteo no se ha realizado.", null));
			}else{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "El Sorteo se ha realizado correctamente.", null));
				crearCampeonatoMasculinoParejas.actualizarEstadoConfiguracion(EstadosIndividualParejas.CONFIG_ESTADO_SORTEADO_CAMPEONATO);
				//Inicializar variables de nuevo
				leerCampeonatoMasculinoParejasSegunda = new LeerCampeonatoMasculinoParejasSegunda();
				//Redirigir
				//redirect.redirect(menuView.PAG_OM_ADD_CAMPEONATO_MASCULINO_PAREJAS_SEGUNDA);
				
			}
			
			
		}catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El Sorteo no se ha realizado.", null));
		}
		
	}

	private List<CalendarioFaseI> prepareCalendarioFaseIModeloGrupos8(List<CalendarioFaseI> calendarioFaseIList, Grupos grupo) {
		ModeloParejasGrupos8 modeloGrupos8 = new ModeloParejasGrupos8();
		Integer id = 1;
		for (Entry<Integer, ModeloParejasEnfrentamientoGruposFaseI> entryPi : modeloGrupos8.getEnfrentamientosGrupo().entrySet()) {
			Integer key = entryPi.getKey();
			Object valuePi = entryPi.getValue();
			ModeloParejasEnfrentamientoGruposFaseI enfrentamiento = (ModeloParejasEnfrentamientoGruposFaseI) valuePi;
			//Agregar calendario
			CalendarioFaseI calendarioFaseI = new CalendarioFaseI();
			calendarioFaseI.setActivo(Activo.SI);
			calendarioFaseI.setCategoriaId(com.bolocelta.bbdd.constants.Categorias.SEGUNDA);
			calendarioFaseI.setGrupo(grupo.getId());
			calendarioFaseI.setId(id);
			calendarioFaseI.setJuegosPareja1(0);
			ParticipantesParejas pi1 = grupo.searchParejaGrupo(enfrentamiento.getPareja1(), grupo.getId());
			calendarioFaseI.setPareja1Id(pi1.getPareja().getId());
			calendarioFaseI.setJuegosPareja2(0);
			ParticipantesParejas pi2 = grupo.searchParejaGrupo(enfrentamiento.getPareja2(), grupo.getId());
			calendarioFaseI.setPareja2Id(pi2.getPareja().getId());
			calendarioFaseI.setOrden(key);
			calendarioFaseI.setInicia(enfrentamiento.getSorteo());
			calendarioFaseI.setBoleraId(grupo.getBoleraId());
			id++;
			calendarioFaseIList.add(calendarioFaseI);
		}
		return calendarioFaseIList;
	}
	
	private List<CalendarioFaseI> prepareCalendarioFaseIModeloGrupos7(List<CalendarioFaseI> calendarioFaseIList, Grupos grupo) {
		ModeloParejasGrupos7 modeloGrupos = new ModeloParejasGrupos7();
		Integer id = 1;
		for (Entry<Integer, ModeloParejasEnfrentamientoGruposFaseI> entryPi : modeloGrupos.getEnfrentamientosGrupo().entrySet()) {
			Integer key = entryPi.getKey();
			Object valuePi = entryPi.getValue();
			ModeloParejasEnfrentamientoGruposFaseI enfrentamiento = (ModeloParejasEnfrentamientoGruposFaseI) valuePi;
			//Agregar calendario
			CalendarioFaseI calendarioFaseI = new CalendarioFaseI();
			calendarioFaseI.setActivo(Activo.SI);
			calendarioFaseI.setCategoriaId(com.bolocelta.bbdd.constants.Categorias.SEGUNDA);
			calendarioFaseI.setGrupo(grupo.getId());
			calendarioFaseI.setId(id);
			calendarioFaseI.setJuegosPareja1(0);
			ParticipantesParejas pi1 = grupo.searchParejaGrupo(enfrentamiento.getPareja1(), grupo.getId());
			calendarioFaseI.setPareja1Id(pi1.getPareja().getId());
			calendarioFaseI.setJuegosPareja2(0);
			ParticipantesParejas pi2 = grupo.searchParejaGrupo(enfrentamiento.getPareja2(), grupo.getId());
			calendarioFaseI.setPareja2Id(pi2.getPareja().getId());
			calendarioFaseI.setOrden(key);
			calendarioFaseI.setInicia(enfrentamiento.getSorteo());
			calendarioFaseI.setBoleraId(grupo.getBoleraId());
			id++;
			calendarioFaseIList.add(calendarioFaseI);
		}
		return calendarioFaseIList;
	}
	
	private List<CalendarioFaseI> prepareCalendarioFaseIModeloGrupos6(List<CalendarioFaseI> calendarioFaseIList, Grupos grupo) {
		ModeloParejasGrupos6 modeloGrupos = new ModeloParejasGrupos6();
		Integer id = 1;
		for (Entry<Integer, ModeloParejasEnfrentamientoGruposFaseI> entryPi : modeloGrupos.getEnfrentamientosGrupo().entrySet()) {
			Integer key = entryPi.getKey();
			Object valuePi = entryPi.getValue();
			ModeloParejasEnfrentamientoGruposFaseI enfrentamiento = (ModeloParejasEnfrentamientoGruposFaseI) valuePi;
			//Agregar calendario
			CalendarioFaseI calendarioFaseI = new CalendarioFaseI();
			calendarioFaseI.setActivo(Activo.SI);
			calendarioFaseI.setCategoriaId(com.bolocelta.bbdd.constants.Categorias.SEGUNDA);
			calendarioFaseI.setGrupo(grupo.getId());
			calendarioFaseI.setId(id);
			calendarioFaseI.setJuegosPareja1(0);
			ParticipantesParejas pi1 = grupo.searchParejaGrupo(enfrentamiento.getPareja1(), grupo.getId());
			calendarioFaseI.setPareja1Id(pi1.getPareja().getId());
			calendarioFaseI.setJuegosPareja2(0);
			ParticipantesParejas pi2 = grupo.searchParejaGrupo(enfrentamiento.getPareja2(), grupo.getId());
			calendarioFaseI.setPareja2Id(pi2.getPareja().getId());
			calendarioFaseI.setOrden(key);
			calendarioFaseI.setInicia(enfrentamiento.getSorteo());
			calendarioFaseI.setBoleraId(grupo.getBoleraId());
			id++;
			calendarioFaseIList.add(calendarioFaseI);
		}
		return calendarioFaseIList;
	}
	
	private List<CalendarioFaseI> prepareCalendarioFaseIModeloGrupos5(List<CalendarioFaseI> calendarioFaseIList, Grupos grupo) {
		ModeloParejasGrupos5 modeloGrupos = new ModeloParejasGrupos5();
		Integer id = 1;
		for (Entry<Integer, ModeloParejasEnfrentamientoGruposFaseI> entryPi : modeloGrupos.getEnfrentamientosGrupo().entrySet()) {
			Integer key = entryPi.getKey();
			Object valuePi = entryPi.getValue();
			ModeloParejasEnfrentamientoGruposFaseI enfrentamiento = (ModeloParejasEnfrentamientoGruposFaseI) valuePi;
			//Agregar calendario
			CalendarioFaseI calendarioFaseI = new CalendarioFaseI();
			calendarioFaseI.setActivo(Activo.SI);
			calendarioFaseI.setCategoriaId(com.bolocelta.bbdd.constants.Categorias.SEGUNDA);
			calendarioFaseI.setGrupo(grupo.getId());
			calendarioFaseI.setId(id);
			calendarioFaseI.setJuegosPareja1(0);
			ParticipantesParejas pi1 = grupo.searchParejaGrupo(enfrentamiento.getPareja1(), grupo.getId());
			calendarioFaseI.setPareja1Id(pi1.getPareja().getId());
			calendarioFaseI.setJuegosPareja2(0);
			ParticipantesParejas pi2 = grupo.searchParejaGrupo(enfrentamiento.getPareja2(), grupo.getId());
			calendarioFaseI.setPareja2Id(pi2.getPareja().getId());
			calendarioFaseI.setOrden(key);
			calendarioFaseI.setInicia(enfrentamiento.getSorteo());
			calendarioFaseI.setBoleraId(grupo.getBoleraId());
			id++;
			calendarioFaseIList.add(calendarioFaseI);
		}
		return calendarioFaseIList;
	}
	
	private List<CalendarioFaseI> prepareCalendarioFaseIModeloGrupos4(List<CalendarioFaseI> calendarioFaseIList, Grupos grupo) {
		ModeloParejasGrupos4 modeloGrupos = new ModeloParejasGrupos4();
		Integer id = 1;
		for (Entry<Integer, ModeloParejasEnfrentamientoGruposFaseI> entryPi : modeloGrupos.getEnfrentamientosGrupo().entrySet()) {
			Integer key = entryPi.getKey();
			Object valuePi = entryPi.getValue();
			ModeloParejasEnfrentamientoGruposFaseI enfrentamiento = (ModeloParejasEnfrentamientoGruposFaseI) valuePi;
			//Agregar calendario
			CalendarioFaseI calendarioFaseI = new CalendarioFaseI();
			calendarioFaseI.setActivo(Activo.SI);
			calendarioFaseI.setCategoriaId(com.bolocelta.bbdd.constants.Categorias.SEGUNDA);
			calendarioFaseI.setGrupo(grupo.getId());
			calendarioFaseI.setId(id);
			calendarioFaseI.setJuegosPareja1(0);
			ParticipantesParejas pi1 = grupo.searchParejaGrupo(enfrentamiento.getPareja1(), grupo.getId());
			calendarioFaseI.setPareja1Id(pi1.getPareja().getId());
			calendarioFaseI.setJuegosPareja2(0);
			ParticipantesParejas pi2 = grupo.searchParejaGrupo(enfrentamiento.getPareja2(), grupo.getId());
			calendarioFaseI.setPareja2Id(pi2.getPareja().getId());
			calendarioFaseI.setOrden(key);
			calendarioFaseI.setInicia(enfrentamiento.getSorteo());
			calendarioFaseI.setBoleraId(grupo.getBoleraId());
			id++;
			calendarioFaseIList.add(calendarioFaseI);
		}
		return calendarioFaseIList;
	}
	
	private List<CalendarioFaseI> prepareCalendarioFaseIModeloGrupos3(List<CalendarioFaseI> calendarioFaseIList, Grupos grupo) {
		ModeloParejasGrupos3 modeloGrupos = new ModeloParejasGrupos3();
		Integer id = 1;
		for (Entry<Integer, ModeloParejasEnfrentamientoGruposFaseI> entryPi : modeloGrupos.getEnfrentamientosGrupo().entrySet()) {
			Integer key = entryPi.getKey();
			Object valuePi = entryPi.getValue();
			ModeloParejasEnfrentamientoGruposFaseI enfrentamiento = (ModeloParejasEnfrentamientoGruposFaseI) valuePi;
			//Agregar calendario
			CalendarioFaseI calendarioFaseI = new CalendarioFaseI();
			calendarioFaseI.setActivo(Activo.SI);
			calendarioFaseI.setCategoriaId(com.bolocelta.bbdd.constants.Categorias.TERCERA);
			calendarioFaseI.setGrupo(grupo.getId());
			calendarioFaseI.setId(id);
			calendarioFaseI.setJuegosPareja1(0);
			ParticipantesParejas pi1 = grupo.searchParejaGrupo(enfrentamiento.getPareja1(), grupo.getId());
			calendarioFaseI.setPareja1Id(pi1.getPareja().getId());
			calendarioFaseI.setJuegosPareja2(0);
			ParticipantesParejas pi2 = grupo.searchParejaGrupo(enfrentamiento.getPareja2(), grupo.getId());
			calendarioFaseI.setPareja2Id(pi2.getPareja().getId());
			calendarioFaseI.setOrden(key);
			calendarioFaseI.setInicia(enfrentamiento.getSorteo());
			calendarioFaseI.setBoleraId(grupo.getBoleraId());
			id++;
			calendarioFaseIList.add(calendarioFaseI);
		}
		return calendarioFaseIList;
	}

	public Fases searchFase(Integer numeroFase){
		for(Fases fase : getResultListFases()){
			if(fase.getNumero().equals(numeroFase)){
				return fase;
			}
		}
		return null;
	}
	
	private List<CalendarioFaseOF> prepareCalendarioFaseOFModeloGrupos4(List<CalendarioFaseOF> calendarioFaseOFList, Fases faseAnterior, Boleras boleraAsignar1, Boleras boleraAsignar2) {
		ModeloParejasOctavosFinal4Grupos modeloOctavosFinal4Grupos = new ModeloParejasOctavosFinal4Grupos();
		Integer id = 1;

		for (Entry<Integer, ModeloParejasEnfrentamientoOctavosFinal> entry : modeloOctavosFinal4Grupos.getEnfrentamientosOctavosFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloParejasEnfrentamientoOctavosFinal enfrentamiento = (ModeloParejasEnfrentamientoOctavosFinal) value;
			//Agregar calendario
			CalendarioFaseOF calendarioFaseOF = new CalendarioFaseOF();
			calendarioFaseOF.setId(id);
			calendarioFaseOF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias.SEGUNDA);
			calendarioFaseOF.setNumeroEnfrentamiento(id);
			calendarioFaseOF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseOF.setPareja1Id(0);
			calendarioFaseOF.setJuegosPareja1P1(0);
			calendarioFaseOF.setJuegosPareja1P2(0);
			calendarioFaseOF.setJuegosPareja1P3(0);
			calendarioFaseOF.setJuegosPareja2P1(0);
			calendarioFaseOF.setJuegosPareja2P2(0);
			calendarioFaseOF.setJuegosPareja2P3(0);
			calendarioFaseOF.setPareja2Id(0);
			calendarioFaseOF.setOrden(key);
			calendarioFaseOF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseOF.setActivo(Activo.SI);
			calendarioFaseOF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseOF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseOF.setGrupoProcedenciaPareja1(enfrentamiento.getGrupoProcedenciaPareja1());
			calendarioFaseOF.setPosicionProcedenciaPareja1(enfrentamiento.getPosicionProcedenciaPareja1());
			calendarioFaseOF.setGrupoProcedenciaPareja2(enfrentamiento.getGrupoProcedenciaPareja2());
			calendarioFaseOF.setPosicionProcedenciaPareja2(enfrentamiento.getPosicionProcedenciaPareja2());
			
			//Asignar la bolera por parte del cuadro
			if(id > 0 && id <5 ){
				calendarioFaseOF.setBoleraId(boleraAsignar1.getId());
			}else if(id > 4 && id <9 ){
				calendarioFaseOF.setBoleraId(boleraAsignar2.getId());
			}
			id++;
			calendarioFaseOFList.add(calendarioFaseOF);
		}
		return calendarioFaseOFList;
	}
	
	private List<CalendarioFaseOF> prepareCalendarioFaseOFModeloGrupos8(List<CalendarioFaseOF> calendarioFaseOFList, Fases faseAnterior, Boleras boleraAsignar1, Boleras boleraAsignar2) {
		ModeloParejasOctavosFinal8Grupos modeloOctavosFinal8Grupos = new ModeloParejasOctavosFinal8Grupos();
		Integer id = 1;

		for (Entry<Integer, ModeloParejasEnfrentamientoOctavosFinal> entry : modeloOctavosFinal8Grupos.getEnfrentamientosOctavosFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloParejasEnfrentamientoOctavosFinal enfrentamiento = (ModeloParejasEnfrentamientoOctavosFinal) value;
			//Agregar calendario
			CalendarioFaseOF calendarioFaseOF = new CalendarioFaseOF();
			calendarioFaseOF.setId(id);
			calendarioFaseOF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias.SEGUNDA);
			calendarioFaseOF.setNumeroEnfrentamiento(id);
			calendarioFaseOF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseOF.setPareja1Id(0);
			calendarioFaseOF.setJuegosPareja1P1(0);
			calendarioFaseOF.setJuegosPareja1P2(0);
			calendarioFaseOF.setJuegosPareja1P3(0);
			calendarioFaseOF.setJuegosPareja2P1(0);
			calendarioFaseOF.setJuegosPareja2P2(0);
			calendarioFaseOF.setJuegosPareja2P3(0);
			calendarioFaseOF.setPareja2Id(0);
			calendarioFaseOF.setOrden(key);
			calendarioFaseOF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseOF.setActivo(Activo.SI);
			calendarioFaseOF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseOF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseOF.setGrupoProcedenciaPareja1(enfrentamiento.getGrupoProcedenciaPareja1());
			calendarioFaseOF.setPosicionProcedenciaPareja1(enfrentamiento.getPosicionProcedenciaPareja1());
			calendarioFaseOF.setGrupoProcedenciaPareja2(enfrentamiento.getGrupoProcedenciaPareja2());
			calendarioFaseOF.setPosicionProcedenciaPareja2(enfrentamiento.getPosicionProcedenciaPareja2());
			
			//Asignar la bolera por parte del cuadro
			if(id > 0 && id <5 ){
				calendarioFaseOF.setBoleraId(boleraAsignar1.getId());
			}else if(id > 4 && id <9 ){
				calendarioFaseOF.setBoleraId(boleraAsignar2.getId());
			}
			id++;
			calendarioFaseOFList.add(calendarioFaseOF);
		}
		return calendarioFaseOFList;
	}
	
	private List<CalendarioFaseCF> prepareCalendarioFaseCFModeloGrupos2(List<CalendarioFaseCF> calendarioFaseCFList, Fases faseAnterior, Boleras boleraAsignar1, Boleras boleraAsignar2) {
		ModeloParejasCuartosFinal2Grupos modeloCuartosFinal2Grupos = new ModeloParejasCuartosFinal2Grupos();
		Integer id = 1;
		
		for (Entry<Integer, ModeloParejasEnfrentamientoCuartosFinal> entry : modeloCuartosFinal2Grupos.getEnfrentamientosCuartosFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloParejasEnfrentamientoCuartosFinal enfrentamiento = (ModeloParejasEnfrentamientoCuartosFinal) value;
			//Agregar calendario
			CalendarioFaseCF calendarioFaseCF = new CalendarioFaseCF();
			calendarioFaseCF.setId(id);
			calendarioFaseCF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias.SEGUNDA);
			calendarioFaseCF.setNumeroEnfrentamiento(id);
			calendarioFaseCF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseCF.setPareja1Id(0);
			calendarioFaseCF.setJuegosPareja1P1(0);
			calendarioFaseCF.setJuegosPareja1P2(0);
			calendarioFaseCF.setJuegosPareja1P3(0);
			calendarioFaseCF.setJuegosPareja2P1(0);
			calendarioFaseCF.setJuegosPareja2P2(0);
			calendarioFaseCF.setJuegosPareja2P3(0);
			calendarioFaseCF.setPareja2Id(0);
			calendarioFaseCF.setOrden(key);
			calendarioFaseCF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseCF.setActivo(Activo.SI);
			calendarioFaseCF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseCF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseCF.setGrupoProcedenciaPareja1(enfrentamiento.getGrupoProcedenciaPareja1());
			calendarioFaseCF.setPosicionProcedenciaPareja1(enfrentamiento.getPosicionProcedenciaPareja1());
			calendarioFaseCF.setGrupoProcedenciaPareja2(enfrentamiento.getGrupoProcedenciaPareja2());
			calendarioFaseCF.setPosicionProcedenciaPareja2(enfrentamiento.getPosicionProcedenciaPareja2());
			
			//Asignar la bolera por parte del cuadro
			if(id > 0 && id <3 ){
				calendarioFaseCF.setBoleraId(boleraAsignar1.getId());
			}else if(id > 2 && id <5 ){
				calendarioFaseCF.setBoleraId(boleraAsignar2.getId());
			}
			id++;
			calendarioFaseCFList.add(calendarioFaseCF);
		}
		return calendarioFaseCFList;
	}
	
	private List<CalendarioFaseCF> prepareCalendarioFaseCFModeloGrupos4(List<CalendarioFaseCF> calendarioFaseCFList, Fases faseAnterior, Boleras boleraAsignar1, Boleras boleraAsignar2) {
		ModeloParejasCuartosFinal4Grupos modeloCuartosFinal4Grupos = new ModeloParejasCuartosFinal4Grupos();
		Integer id = 1;
		
		for (Entry<Integer, ModeloParejasEnfrentamientoCuartosFinal> entry : modeloCuartosFinal4Grupos.getEnfrentamientosCuartosFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloParejasEnfrentamientoCuartosFinal enfrentamiento = (ModeloParejasEnfrentamientoCuartosFinal) value;
			//Agregar calendario
			CalendarioFaseCF calendarioFaseCF = new CalendarioFaseCF();
			calendarioFaseCF.setId(id);
			calendarioFaseCF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias.SEGUNDA);
			calendarioFaseCF.setNumeroEnfrentamiento(id);
			calendarioFaseCF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseCF.setPareja1Id(0);
			calendarioFaseCF.setJuegosPareja1P1(0);
			calendarioFaseCF.setJuegosPareja1P2(0);
			calendarioFaseCF.setJuegosPareja1P3(0);
			calendarioFaseCF.setJuegosPareja2P1(0);
			calendarioFaseCF.setJuegosPareja2P2(0);
			calendarioFaseCF.setJuegosPareja2P3(0);
			calendarioFaseCF.setPareja2Id(0);
			calendarioFaseCF.setOrden(key);
			calendarioFaseCF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseCF.setActivo(Activo.SI);
			calendarioFaseCF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseCF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseCF.setGrupoProcedenciaPareja1(enfrentamiento.getGrupoProcedenciaPareja1());
			calendarioFaseCF.setPosicionProcedenciaPareja1(enfrentamiento.getPosicionProcedenciaPareja1());
			calendarioFaseCF.setGrupoProcedenciaPareja2(enfrentamiento.getGrupoProcedenciaPareja2());
			calendarioFaseCF.setPosicionProcedenciaPareja2(enfrentamiento.getPosicionProcedenciaPareja2());
			
			//Asignar la bolera por parte del cuadro
			if(id > 0 && id <3 ){
				calendarioFaseCF.setBoleraId(boleraAsignar1.getId());
			}else if(id > 2 && id <5 ){
				calendarioFaseCF.setBoleraId(boleraAsignar2.getId());
			}
			id++;
			calendarioFaseCFList.add(calendarioFaseCF);
		}
		return calendarioFaseCFList;
	}
	
	private List<CalendarioFaseCF> prepareCalendarioFaseCFModeloOF(List<CalendarioFaseCF> calendarioFaseCFList, Fases faseAnterior, Boleras boleraAsignar1, Boleras boleraAsignar2) {
		ModeloParejasCuartosFinal4OF modeloCuartosFinal4OF = new ModeloParejasCuartosFinal4OF();
		Integer id = 1;
		
		for (Entry<Integer, ModeloParejasEnfrentamientoCuartosFinal> entry : modeloCuartosFinal4OF.getEnfrentamientosCuartosFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloParejasEnfrentamientoCuartosFinal enfrentamiento = (ModeloParejasEnfrentamientoCuartosFinal) value;
			//Agregar calendario
			CalendarioFaseCF calendarioFaseCF = new CalendarioFaseCF();
			calendarioFaseCF.setId(id);
			calendarioFaseCF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias.SEGUNDA);
			calendarioFaseCF.setNumeroEnfrentamiento(id);
			calendarioFaseCF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseCF.setPareja1Id(0);
			calendarioFaseCF.setJuegosPareja1P1(0);
			calendarioFaseCF.setJuegosPareja1P2(0);
			calendarioFaseCF.setJuegosPareja1P3(0);
			calendarioFaseCF.setJuegosPareja2P1(0);
			calendarioFaseCF.setJuegosPareja2P2(0);
			calendarioFaseCF.setJuegosPareja2P3(0);
			calendarioFaseCF.setPareja2Id(0);
			calendarioFaseCF.setOrden(key);
			calendarioFaseCF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseCF.setActivo(Activo.SI);
			calendarioFaseCF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseCF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseCF.setGrupoProcedenciaPareja1(enfrentamiento.getGrupoProcedenciaPareja1());
			calendarioFaseCF.setPosicionProcedenciaPareja1(enfrentamiento.getPosicionProcedenciaPareja1());
			calendarioFaseCF.setGrupoProcedenciaPareja2(enfrentamiento.getGrupoProcedenciaPareja2());
			calendarioFaseCF.setPosicionProcedenciaPareja2(enfrentamiento.getPosicionProcedenciaPareja2());
			
			//Asignar la bolera por parte del cuadro
			if(id > 0 && id <5 ){
				calendarioFaseCF.setBoleraId(boleraAsignar1.getId());
			}else if(id > 4 && id <9 ){
				calendarioFaseCF.setBoleraId(boleraAsignar2.getId());
			}
			id++;
			calendarioFaseCFList.add(calendarioFaseCF);
		}
		return calendarioFaseCFList;
	}
	
	private List<CalendarioFaseSF> prepareCalendarioFaseSFModeloCF(List<CalendarioFaseSF> calendarioFaseSFList, Fases faseAnterior, Boleras boleraFinal) {
		ModeloParejasSemiFinal4CF modeloSemiFinal4CF = new ModeloParejasSemiFinal4CF();
		Integer id = 1;
		
		for (Entry<Integer, ModeloParejasEnfrentamientoSemiFinal> entry : modeloSemiFinal4CF.getEnfrentamientosSemiFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloParejasEnfrentamientoSemiFinal enfrentamiento = (ModeloParejasEnfrentamientoSemiFinal) value;
			//Agregar calendario
			CalendarioFaseSF calendarioFaseSF = new CalendarioFaseSF();
			calendarioFaseSF.setId(id);
			calendarioFaseSF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias.SEGUNDA);
			calendarioFaseSF.setNumeroEnfrentamiento(id);
			calendarioFaseSF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseSF.setPareja1Id(0);
			calendarioFaseSF.setJuegosPareja1P1(0);
			calendarioFaseSF.setJuegosPareja1P2(0);
			calendarioFaseSF.setJuegosPareja1P3(0);
			calendarioFaseSF.setJuegosPareja2P1(0);
			calendarioFaseSF.setJuegosPareja2P2(0);
			calendarioFaseSF.setJuegosPareja2P3(0);
			calendarioFaseSF.setPareja2Id(0);
			calendarioFaseSF.setOrden(key);
			calendarioFaseSF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseSF.setActivo(Activo.SI);
			calendarioFaseSF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseSF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseSF.setGrupoProcedenciaPareja1(enfrentamiento.getGrupoProcedenciaPareja1());
			calendarioFaseSF.setPosicionProcedenciaPareja1(enfrentamiento.getPosicionProcedenciaPareja1());
			calendarioFaseSF.setGrupoProcedenciaPareja2(enfrentamiento.getGrupoProcedenciaPareja2());
			calendarioFaseSF.setPosicionProcedenciaPareja2(enfrentamiento.getPosicionProcedenciaPareja2());
			
			//Asignar la bolera por parte del cuadro
			calendarioFaseSF.setBoleraId(boleraFinal.getId());
			id++;
			calendarioFaseSFList.add(calendarioFaseSF);
		}
		return calendarioFaseSFList;
	}
	
	private List<CalendarioFaseFC> prepareCalendarioFaseFCModeloSF(List<CalendarioFaseFC> calendarioFaseFCList, Fases faseAnterior, Boleras boleraFinal) {
		ModeloParejasFinalConsolacion4SF modeloFinalConsolacion4SF = new ModeloParejasFinalConsolacion4SF();
		Integer id = 1;
		
		for (Entry<Integer, ModeloParejasEnfrentamientoFinalConsolacion> entry : modeloFinalConsolacion4SF.getEnfrentamientosFinalConsolacion().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloParejasEnfrentamientoFinalConsolacion enfrentamiento = (ModeloParejasEnfrentamientoFinalConsolacion) value;
			//Agregar calendario
			CalendarioFaseFC calendarioFaseFC = new CalendarioFaseFC();
			calendarioFaseFC.setId(id);
			calendarioFaseFC.setCategoriaId(com.bolocelta.bbdd.constants.Categorias.SEGUNDA);
			calendarioFaseFC.setNumeroEnfrentamiento(id);
			calendarioFaseFC.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseFC.setPareja1Id(0);
			calendarioFaseFC.setJuegosPareja1P1(0);
			calendarioFaseFC.setJuegosPareja1P2(0);
			calendarioFaseFC.setJuegosPareja1P3(0);
			calendarioFaseFC.setJuegosPareja2P1(0);
			calendarioFaseFC.setJuegosPareja2P2(0);
			calendarioFaseFC.setJuegosPareja2P3(0);
			calendarioFaseFC.setPareja2Id(0);
			calendarioFaseFC.setOrden(key);
			calendarioFaseFC.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseFC.setActivo(Activo.SI);
			calendarioFaseFC.setInicia(enfrentamiento.getSorteo());
			calendarioFaseFC.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseFC.setGrupoProcedenciaPareja1(enfrentamiento.getGrupoProcedenciaPareja1());
			calendarioFaseFC.setPosicionProcedenciaPareja1(enfrentamiento.getPosicionProcedenciaPareja1());
			calendarioFaseFC.setGrupoProcedenciaPareja2(enfrentamiento.getGrupoProcedenciaPareja2());
			calendarioFaseFC.setPosicionProcedenciaPareja2(enfrentamiento.getPosicionProcedenciaPareja2());
			
			//Asignar la bolera por parte del cuadro
			calendarioFaseFC.setBoleraId(boleraFinal.getId());
			id++;
			calendarioFaseFCList.add(calendarioFaseFC);
		}
		return calendarioFaseFCList;
	}
	
	private List<CalendarioFaseFF> prepareCalendarioFaseFFModeloSF(List<CalendarioFaseFF> calendarioFaseFFList, Fases faseAnterior, Boleras boleraFinal) {
		ModeloParejasFinal4SF modeloFinal4SF = new ModeloParejasFinal4SF();
		Integer id = 1;
		
		for (Entry<Integer, ModeloParejasEnfrentamientoFinal> entry : modeloFinal4SF.getEnfrentamientosFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloParejasEnfrentamientoFinal enfrentamiento = (ModeloParejasEnfrentamientoFinal) value;
			//Agregar calendario
			CalendarioFaseFF calendarioFaseFF = new CalendarioFaseFF();
			calendarioFaseFF.setId(id);
			calendarioFaseFF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias.SEGUNDA);
			calendarioFaseFF.setNumeroEnfrentamiento(id);
			calendarioFaseFF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseFF.setPareja1Id(0);
			calendarioFaseFF.setJuegosPareja1P1(0);
			calendarioFaseFF.setJuegosPareja1P2(0);
			calendarioFaseFF.setJuegosPareja1P3(0);
			calendarioFaseFF.setJuegosPareja2P1(0);
			calendarioFaseFF.setJuegosPareja2P2(0);
			calendarioFaseFF.setJuegosPareja2P3(0);
			calendarioFaseFF.setPareja2Id(0);
			calendarioFaseFF.setOrden(key);
			calendarioFaseFF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseFF.setActivo(Activo.SI);
			calendarioFaseFF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseFF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseFF.setGrupoProcedenciaPareja1(enfrentamiento.getGrupoProcedenciaPareja1());
			calendarioFaseFF.setPosicionProcedenciaPareja1(enfrentamiento.getPosicionProcedenciaPareja1());
			calendarioFaseFF.setGrupoProcedenciaPareja2(enfrentamiento.getGrupoProcedenciaPareja2());
			calendarioFaseFF.setPosicionProcedenciaPareja2(enfrentamiento.getPosicionProcedenciaPareja2());
			
			//Asignar la bolera por parte del cuadro
			calendarioFaseFF.setBoleraId(boleraFinal.getId());
			id++;
			calendarioFaseFFList.add(calendarioFaseFF);
		}
		return calendarioFaseFFList;
	}

	public String getBolerasOcupadasFI() {
		return bolerasOcupadasFI;
	}

	public void setBolerasOcupadasFI(String bolerasOcupadasFI) {
		this.bolerasOcupadasFI = bolerasOcupadasFI;
	}

	public String getBolerasOcupadasOFCF() {
		return bolerasOcupadasOFCF;
	}

	public void setBolerasOcupadasOFCF(String bolerasOcupadasOFCF) {
		this.bolerasOcupadasOFCF = bolerasOcupadasOFCF;
	}
	
	public void doBorrarSorteo(){
		crearCampeonatoMasculinoParejas.borrarSorteo();
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "El Sorteo se ha borrado correctamente. Se puede volver a sortear.", null));
		//Inicializar variables de nuevo
		leerCampeonatoMasculinoParejasSegunda = new LeerCampeonatoMasculinoParejasSegunda();
		//Redirigir
		//redirect.redirect(menuView.PAG_OM_ADD_CAMPEONATO_MASCULINO_PAREJAS_SEGUNDA);
	}
	
	public void doConfirmarSorteoCampeonato(){
		crearCampeonatoMasculinoParejas.actualizarEstadoConfiguracion(EstadosIndividualParejas.CONFIG_ESTADO_FINALIZADO);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "El Sorteo se ha confirmado correctamente.", null));
		//Inicializar variables de nuevo
		leerCampeonatoMasculinoParejasSegunda = new LeerCampeonatoMasculinoParejasSegunda();
		//Redirigir
		//redirect.redirect(menuView.PAG_OM_ADD_CAMPEONATO_MASCULINO_PAREJAS_SEGUNDA);		
	}
	
	

}
