package com.bolocelta.transformer;

import org.apache.commons.csv.CSVRecord;
import org.apache.poi.ss.usermodel.Cell;

import com.bolocelta.bbdd.constants.Modalidad;
import com.bolocelta.entities.Jugadores;

public class JugadoresTransformer {

	public static Jugadores transformerObject(Jugadores jugador, Cell cell) {

		if (cell.getColumnIndex() == 0) {
			Double id = cell.getNumericCellValue();
			jugador.setId(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} else if (cell.getColumnIndex() == 1) {
			Double idEquipo = cell.getNumericCellValue();
			jugador.setEquipoId(idEquipo.intValue());
			//System.out.print(idEquipo.intValue() + " | ");
		} else if (cell.getColumnIndex() == 2) {
			jugador.setNombre(cell.getStringCellValue());
			//System.out.print(cell.getStringCellValue() + " | ");
		} else if (cell.getColumnIndex() == 3) {
			jugador.setApodo(cell.getStringCellValue());
			//System.out.print(cell.getStringCellValue() + " | ");
		} else if (cell.getColumnIndex() == 4) {
			
			try {
				Double id = cell.getNumericCellValue();
				jugador.setNumeroFicha(String.valueOf(id.intValue()));
				//System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				jugador.setNumeroFicha(cell.getStringCellValue());
				//System.out.print(cell.getStringCellValue() + " | ");
			}
		} else if (cell.getColumnIndex() == 5) {
			jugador.setEmail(cell.getStringCellValue());
			//System.out.print(cell.getStringCellValue() + " | ");
		} else if (cell.getColumnIndex() == 6) {
			try {
				Double id = cell.getNumericCellValue();
				jugador.setTelefono(String.valueOf(id.intValue()));
				//System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				jugador.setTelefono(cell.getStringCellValue());
				//System.out.print(cell.getStringCellValue() + " | ");
			}
		} else if (cell.getColumnIndex() == 7) {
			try {
				Double id = cell.getNumericCellValue();
				jugador.setEdad(Integer.valueOf(String.valueOf(id.intValue())));
				//System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				try {
					jugador.setEdad(Integer.valueOf(cell.getStringCellValue()));
					//System.out.print(cell.getStringCellValue() + " | ");
				}catch (Exception e1) {
					jugador.setEdad(null);
				}
			}
		} else if (cell.getColumnIndex() == 8) {
			jugador.setActivo(cell.getStringCellValue());
			//System.out.print(cell.getStringCellValue() + " | ");
		} else if (cell.getColumnIndex() == 9) {
			jugador.setModalidad(cell.getStringCellValue());
			if(jugador.getModalidad().equalsIgnoreCase(Modalidad.MASCULINO)){
				jugador.setMasculino(true);
				jugador.setFemenino(false);
			}else if(jugador.getModalidad().equalsIgnoreCase(Modalidad.FEMENINO)){
				jugador.setMasculino(false);
				jugador.setFemenino(true);
			}
			//System.out.print(cell.getStringCellValue() + " | ");
		} else if (cell.getColumnIndex() == 10) {
			jugador.setIndividualPrimera(cell.getStringCellValue());
			//System.out.print(cell.getStringCellValue() + " | ");
		} else if (cell.getColumnIndex() == 11) {
			jugador.setIndividualSegunda(cell.getStringCellValue());
			//System.out.print(cell.getStringCellValue() + " | ");
		} else if (cell.getColumnIndex() == 12) {
			jugador.setIndividualTercera(cell.getStringCellValue());
			//System.out.print(cell.getStringCellValue() + " | ");
		} else if (cell.getColumnIndex() == 13) {
			jugador.setIndividualFemenino(cell.getStringCellValue());
			//System.out.print(cell.getStringCellValue() + " | ");
		}
		

		return jugador;
	}
	
	
	
	public static Jugadores transformerObjectCsv(Jugadores jugador, CSVRecord row) {
		
        // Accessing Values by Column Index
        String col1 = row.get(0);
        String col2 = row.get(1);
        String col3 = row.get(2);
        String col4 = row.get(3);
        String col5 = row.get(4);
        String col6 = row.get(5);
        String col7 = row.get(6);
        String col8 = row.get(7);
        String col9 = row.get(8);
        String col10 = row.get(9);
        String col11 = row.get(10);
        String col12 = row.get(11);
        String col13 = row.get(12);
        String col14 = row.get(13);

		if (col1 != null && !col1.isEmpty()) {
			Double id = Double.valueOf(col1);
			jugador.setId(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col2 != null && !col2.isEmpty()) {
			Double id = Double.valueOf(col2);
			jugador.setEquipoId(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col3 != null && !col3.isEmpty()) {
			jugador.setNombre(col3.toUpperCase());
			//System.out.print(col3 + " | ");
		} 
		if (col4 != null && !col4.isEmpty()) {
			jugador.setApodo(col4.toUpperCase());
			//System.out.print(col4 + " | ");
		}
		if (col5 != null && !col5.isEmpty()) {
			try {
				Double id = Double.valueOf(col5);
				jugador.setNumeroFicha(String.valueOf(id.intValue()));
				//System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				jugador.setNumeroFicha(col5);
				//System.out.print(col5 + " | ");
			}
		}
		if (col6 != null && !col6.isEmpty()) {
			jugador.setEmail(col6);
			//System.out.print(col6 + " | ");
		}
		if (col7 != null && !col7.isEmpty()) {
			try {
				Double id = Double.valueOf(col7);
				jugador.setTelefono(String.valueOf(id.intValue()));
				//System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				jugador.setTelefono(col7);
				//System.out.print(col7 + " | ");
			}
		}
		if (col8 != null && !col8.isEmpty()) {
			try {
				Double id = Double.valueOf(col8);
				jugador.setEdad(Integer.valueOf(String.valueOf(id.intValue())));
				//System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				try {
					jugador.setEdad(Integer.valueOf(col8));
					//System.out.print(col8 + " | ");
				}catch (Exception e1) {
					jugador.setEdad(null);
				}
			}
		}
		if (col9 != null && !col9.isEmpty()) {
			jugador.setActivo(col9);
			//System.out.print(col9 + " | ");
		}
		if (col10 != null && !col10.isEmpty()) {
			jugador.setModalidad(col10);
			if(jugador.getModalidad().equalsIgnoreCase(Modalidad.MASCULINO)){
				jugador.setMasculino(true);
				jugador.setFemenino(false);
			}else if(jugador.getModalidad().equalsIgnoreCase(Modalidad.FEMENINO)){
				jugador.setMasculino(false);
				jugador.setFemenino(true);
			}
			//System.out.print(col10 + " | ");
		}
		if (col11 != null && !col11.isEmpty()) {
			jugador.setIndividualPrimera(col11);
			//System.out.print(col11 + " | ");
		}
		if (col12 != null && !col12.isEmpty()) {
			jugador.setIndividualSegunda(col12);
			//System.out.print(col12 + " | ");
		}
		if (col13 != null && !col13.isEmpty()) {
			jugador.setIndividualTercera(col13);
			//System.out.print(col13 + " | ");
		}
		if (col14 != null && !col14.isEmpty()) {
			jugador.setIndividualFemenino(col14);
			//System.out.print(col14 + " | ");
		}
		return jugador;
	}

}
