package com.bolocelta.transformer;

import org.apache.commons.csv.CSVRecord;

import com.bolocelta.entities.sorteos.liga.SorteoTuplaEnfrentamientosLiga;

public class SorteoTuplaEnfrentamientosLigaTransformer {

	public static SorteoTuplaEnfrentamientosLiga transformerObjectCsv(SorteoTuplaEnfrentamientosLiga sorteoTuplaEnfrentamientosLiga, CSVRecord row) {
		
        // Accessing Values by Column Index
        String col1 = row.get(0);
        String col2 = row.get(1);
        String col3 = row.get(2);
        String col4 = row.get(3);
        String col5 = row.get(4);
        String col6 = row.get(5);
        String col7 = row.get(6);
        String col8 = row.get(7);

		if (col1 != null && !col1.isEmpty()) {
			Double id = Double.valueOf(col1);
			sorteoTuplaEnfrentamientosLiga.setId(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col2 != null && !col2.isEmpty()) {
			Double id = Double.valueOf(col2);
			sorteoTuplaEnfrentamientosLiga.setCategoria(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col3 != null && !col3.isEmpty()) {
			Double id = Double.valueOf(col3);
			sorteoTuplaEnfrentamientosLiga.setJornadaIda(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col4 != null && !col4.isEmpty()) {
			Double id = Double.valueOf(col4);
			sorteoTuplaEnfrentamientosLiga.setJornadaVta(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col5 != null && !col5.isEmpty()) {
			Double id = Double.valueOf(col5);
			sorteoTuplaEnfrentamientosLiga.setCruce1Ida(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col6 != null && !col6.isEmpty()) {
			Double id = Double.valueOf(col6);
			sorteoTuplaEnfrentamientosLiga.setCruce2Ida(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col7 != null && !col7.isEmpty()) {
			Double id = Double.valueOf(col7);
			sorteoTuplaEnfrentamientosLiga.setCruce1Vta(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col8 != null && !col8.isEmpty()) {
			Double id = Double.valueOf(col8);
			sorteoTuplaEnfrentamientosLiga.setCruce2Vta(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 

		return sorteoTuplaEnfrentamientosLiga;
	}

}
