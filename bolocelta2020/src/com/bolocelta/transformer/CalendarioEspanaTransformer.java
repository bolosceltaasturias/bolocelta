package com.bolocelta.transformer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.csv.CSVRecord;
import org.apache.poi.ss.usermodel.Cell;

import com.bolocelta.entities.CalendarioEspana;

public class CalendarioEspanaTransformer {

	public static CalendarioEspana transformerObject(CalendarioEspana calendarioEspana, Cell cell) {

		if (cell.getColumnIndex() == 0) {
			Double id = cell.getNumericCellValue();
			calendarioEspana.setId(id.intValue());
//			System.out.print(id.intValue() + " | ");
		} else if (cell.getColumnIndex() == 1) {
			calendarioEspana.setFechaDesde(cell.getDateCellValue());
//			System.out.print(cell.getDateCellValue() + " | ");
		} else if (cell.getColumnIndex() == 2) {
			calendarioEspana.setFechaHasta(cell.getDateCellValue());
//			System.out.print(cell.getDateCellValue() + " | ");
		} else if (cell.getColumnIndex() == 3) {
			calendarioEspana.setNombreCampeonato(cell.getStringCellValue());
//			System.out.print(cell.getStringCellValue() + " | ");
		} else if (cell.getColumnIndex() == 4) {
			calendarioEspana.setLocalizacion(cell.getStringCellValue());
//			System.out.print(cell.getStringCellValue() + " | ");
		} else if (cell.getColumnIndex() == 5) {
			calendarioEspana.setProvincia(cell.getStringCellValue());
//			System.out.print(cell.getStringCellValue() + " | ");
		}

		return calendarioEspana;
	}
	
	public static CalendarioEspana transformerObjectCsv(CalendarioEspana calendarioEspana, CSVRecord row) {
		
        // Accessing Values by Column Index
        String col1 = row.get(0);
        String col2 = row.get(1);
        String col3 = row.get(2);
        String col4 = row.get(3);
        String col5 = row.get(4);
        String col6 = row.get(5);

		if (col1 != null && !col1.isEmpty()) {
			Double id = Double.valueOf(col1);
			calendarioEspana.setId(id.intValue());
//			System.out.print(id.intValue() + " | ");
		} 
		if (col2 != null && !col2.isEmpty()) {
			Date date;
			try {
				date = new SimpleDateFormat("dd/MM/yyyy").parse(col2);
			} catch (ParseException e) {
				date = null;
			}
			calendarioEspana.setFechaDesde(date);
//			System.out.print(col2 + " | ");
		} 
		if (col3 != null && !col3.isEmpty()) {
			Date date;
			try {
				date = new SimpleDateFormat("dd/MM/yyyy").parse(col3);
			} catch (ParseException e) {
				date = null;
			}
			calendarioEspana.setFechaHasta(date);
//			System.out.print(col3 + " | ");
		} 
		if (col4 != null && !col4.isEmpty()) {
			calendarioEspana.setNombreCampeonato(col4);
//			System.out.print(col4 + " | ");
		}
		if (col5 != null && !col5.isEmpty()) {
			calendarioEspana.setLocalizacion(col5);
//			System.out.print(col5 + " | ");
		}
		if (col6 != null && !col6.isEmpty()) {
			calendarioEspana.setProvincia(col6);
//			System.out.print(col6 + " | ");
		}
		return calendarioEspana;
	}

}
