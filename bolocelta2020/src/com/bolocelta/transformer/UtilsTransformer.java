package com.bolocelta.transformer;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;

import com.bolocelta.bbdd.constants.NombresTablas;

public class UtilsTransformer {
	
	public static String getNativeValueType(Cell cell){
		final int type = cell.getCellType();
		if (type == Cell.CELL_TYPE_BLANK) {
			return null;
		} else if (type == Cell.CELL_TYPE_STRING) {
			return NombresTablas.TD_TEXTO;
		} else if (type == Cell.CELL_TYPE_NUMERIC) {
			if (DateUtil.isCellDateFormatted(cell)) {
				return NombresTablas.TD_FECHA;
			} else {
				return NombresTablas.TD_DECIMAL;
			}
		} else if (type == Cell.CELL_TYPE_FORMULA) {
			if (cell.getCachedFormulaResultType() == Cell.CELL_TYPE_NUMERIC) {
				return NombresTablas.TD_DECIMAL;
			} else {
				return NombresTablas.TD_TEXTO;
			}
		} else {
			return NombresTablas.TD_TEXTO;
		}
	}

}
