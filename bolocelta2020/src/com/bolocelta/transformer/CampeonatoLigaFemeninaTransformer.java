package com.bolocelta.transformer;

import org.apache.commons.csv.CSVRecord;

import com.bolocelta.entities.CampeonatoLigaFemeninaCalendario;
import com.bolocelta.entities.CampeonatoLigaFemeninaClasificacion;

public class CampeonatoLigaFemeninaTransformer {

	public static CampeonatoLigaFemeninaClasificacion transformerObjectClasificacionCsv(CampeonatoLigaFemeninaClasificacion cec, CSVRecord row) {
		
        // Accessing Values by Column Index
        String col1 = row.get(0);
        String col2 = row.get(1);
        String col3 = row.get(2);
        String col4 = row.get(3);
        String col5 = row.get(4);

		if (col1 != null && !col1.isEmpty()) {
			Double id = Double.valueOf(col1);
			cec.setId(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col2 != null && !col2.isEmpty()) {
			Double id = Double.valueOf(col2);
			cec.setJugadoraId(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col3 != null && !col3.isEmpty()) {
			Double id = Double.valueOf(col3);
			cec.setPuntosTirada(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col4 != null && !col4.isEmpty()) {
			Double id = Double.valueOf(col4);
			cec.setPuntosSacada(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col5 != null && !col5.isEmpty()) {
			Double id = Double.valueOf(col5);
			cec.setPuntos(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		return cec;
	}
	
	
	public static CampeonatoLigaFemeninaCalendario transformerObjectCalendarioCsv(CampeonatoLigaFemeninaCalendario cec, CSVRecord row) {
		
        // Accessing Values by Column Index
        String col1 = row.get(0);
        String col2 = row.get(1);
        String col3 = row.get(2);
        String col4 = row.get(3);
        String col5 = row.get(4);
        String col6 = row.get(5);
        String col7 = row.get(6);
        String col8 = row.get(7);
        String col9 = row.get(8);
        String col10 = row.get(9);

		if (col1 != null && !col1.isEmpty()) {
			Double id = Double.valueOf(col1);
			cec.setId(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col2 != null && !col2.isEmpty()) {
			Double id = Double.valueOf(col2);
			cec.setJornada(id.intValue());
			//System.out.print(id.intValue() + " | ");
		}
		if (col3 != null && !col3.isEmpty()) {
			cec.setFecha(col3);
			//System.out.print(col3 + " | ");
		} 
		if (col4 != null && !col4.isEmpty()) {
			cec.setHora(col4);
			//System.out.print(col4 + " | ");
		} 
		if (col10 != null && !col10.isEmpty()) {
			Double id = Double.valueOf(col10);
			cec.setJugadoraId(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col5 != null && !col5.isEmpty()) {
			Double id = Double.valueOf(col5);
			cec.setTirada(id.intValue());
			//System.out.print(id.intValue() + " | ");
		}
		if (col6 != null && !col6.isEmpty()) {
			Double id = Double.valueOf(col6);
			cec.setSacada(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col7 != null && !col7.isEmpty()) {
			Double id = Double.valueOf(col7);
			cec.setPuntos(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col8 != null && !col8.isEmpty()) {
			Double id = Double.valueOf(col8);
			cec.setActivo(id.intValue());
			//System.out.print(id.intValue() + " | ");
		}
		if (col9 != null && !col9.isEmpty()) {
			Double id = Double.valueOf(col9);
			cec.setBoleraId(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		return cec;
	}

}
