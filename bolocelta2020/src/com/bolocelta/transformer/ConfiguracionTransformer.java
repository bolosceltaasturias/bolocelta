package com.bolocelta.transformer;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.apache.commons.csv.CSVRecord;
import org.apache.poi.ss.usermodel.Cell;

import com.bolocelta.bbdd.constants.NombresTablas;
import com.bolocelta.entities.Configuracion;

public class ConfiguracionTransformer extends UtilsTransformer{

	public static Configuracion transformerObject(Configuracion configuracion, Cell cell) {
		
		String typeData = getNativeValueType(cell);

		if (cell.getColumnIndex() == 0) {
			try {
				Double id = cell.getNumericCellValue();
				configuracion.setId(id.intValue());
				//System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				try {
					configuracion.setId(Integer.valueOf(cell.getStringCellValue()));
					//System.out.print(cell.getStringCellValue() + " | ");
				}catch (Exception e1) {
					configuracion.setId(null);
				}
			}
		} else if (cell.getColumnIndex() == 1) {
			configuracion.setNombre(cell.getStringCellValue());
			//System.out.print(cell.getStringCellValue() + " | ");
		} else if (cell.getColumnIndex() == 2) {
			if(typeData != null){
				if(typeData.equals(NombresTablas.TD_FECHA)){
					DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			        String strDate = dateFormat.format(cell.getDateCellValue());
					configuracion.setValor(strDate);
					//System.out.print(strDate + " | ");
				}else{
					configuracion.setValor(cell.getStringCellValue());
					//System.out.print(cell.getStringCellValue() + " | ");
				}
			}else{
				configuracion.setValor(null);
			}
			
		}

		return configuracion;
	}
	
	public static Configuracion transformerObjectCsv(Configuracion configuracion, CSVRecord row) {
		
        // Accessing Values by Column Index
        String col1 = row.get(0);
        String col2 = row.get(1);
        String col3 = row.get(2);

		if (col1 != null && !col1.isEmpty()) {
			try {
				Double id = Double.valueOf(col1);
				configuracion.setId(id.intValue());
				//System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				configuracion.setId(null);
			}
		}
		
		if (col2 != null && !col2.isEmpty()) {
			configuracion.setNombre(col2);
			//System.out.print(col2 + " | ");
		} 
		if (col3 != null && !col3.isEmpty()) {
			configuracion.setValor(col3);
			//System.out.print(col3 + " | ");
		} 
		return configuracion;
	}

}
