package com.bolocelta.transformer.usuarios;

import org.apache.commons.csv.CSVRecord;
import org.apache.poi.ss.usermodel.Cell;

import com.bolocelta.entities.usuarios.Usuarios;

public class UsuariosTransformer {

	public static Usuarios transformerObject(Usuarios usuario, Cell cell) {

		if (cell.getColumnIndex() == 0) {
			Double id = cell.getNumericCellValue();
			usuario.setId(id.intValue());
//			System.out.print(id.intValue() + " | ");
		} else if (cell.getColumnIndex() == 1) {
			Double id = cell.getNumericCellValue();
			usuario.setEquipo(id.intValue());
//			System.out.print(id.intValue() + " | ");
		} else if (cell.getColumnIndex() == 2) {
			usuario.setNombre(cell.getStringCellValue());
//			System.out.print(cell.getStringCellValue() + " | ");
		} else if (cell.getColumnIndex() == 3) {
			usuario.setApellido(cell.getStringCellValue());
//			System.out.print(cell.getStringCellValue() + " | ");
		} else if (cell.getColumnIndex() == 4) {
			usuario.setUser(cell.getStringCellValue());
//			System.out.print(cell.getStringCellValue() + " | ");
		} else if (cell.getColumnIndex() == 5) {
			usuario.setPass(cell.getStringCellValue());
//			System.out.print(cell.getStringCellValue() + " | ");
		} else if (cell.getColumnIndex() == 6) {
			Double id = cell.getNumericCellValue();
			usuario.setRolId(id.intValue());
//			System.out.print(id.intValue() + " | ");
		}

		return usuario;
	}
	
	public static Usuarios transformerObjectCsv(Usuarios usuario, CSVRecord row) {
		
        // Accessing Values by Column Index
        String col1 = row.get(0);
        String col2 = row.get(1);
        String col3 = row.get(2);
        String col4 = row.get(3);
        String col5 = row.get(4);
        String col6 = row.get(5);
        String col7 = row.get(6);

		if (col1 != null && !col1.isEmpty()) {
			Double id = Double.valueOf(col1);
			usuario.setId(id.intValue());
//			System.out.print(id.intValue() + " | ");
		} 
		if (col2 != null && !col2.isEmpty()) {
			Double id = Double.valueOf(col2);
			usuario.setEquipo(id.intValue());
//			System.out.print(id.intValue() + " | ");
		} 
		if (col3 != null && !col3.isEmpty()) {
			usuario.setNombre(col3);
//			System.out.print(col3 + " | ");
		}
		if (col4 != null && !col4.isEmpty()) {
			usuario.setApellido(col4);
//			System.out.print(col4 + " | ");
		}
		if (col5 != null && !col5.isEmpty()) {
			usuario.setUser(col5);
//			System.out.print(col5 + " | ");
		}
		if (col6 != null && !col6.isEmpty()) {
			usuario.setPass(col6);
//			System.out.print(col6 + " | ");
		}
		if (col7 != null && !col7.isEmpty()) {
			Double id = Double.valueOf(col7);
			usuario.setRolId(id.intValue());
//			System.out.print(id.intValue() + " | ");
		}

		return usuario;
	}

}
