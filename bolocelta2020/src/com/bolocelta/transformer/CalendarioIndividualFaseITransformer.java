package com.bolocelta.transformer;

import org.apache.commons.csv.CSVRecord;

import com.bolocelta.entities.sorteos.individual.CalendarioFaseI;

public class CalendarioIndividualFaseITransformer {

	
	public static CalendarioFaseI transformerObjectCsv(CalendarioFaseI calendarioFaseI, CSVRecord row) {
		
        // Accessing Values by Column Index
        String col1 = row.get(0);
        String col2 = row.get(1);
        String col3 = row.get(2);
        String col4 = row.get(3);
        String col5 = row.get(4);
        String col6 = row.get(5);
        String col7 = row.get(6);
        String col8 = row.get(7);
        String col9 = row.get(8);
        String col10 = row.get(9);
        String col11 = row.get(10);
        
		if (col1 != null && !col1.isEmpty()) {
			try {
				Double id = Double.valueOf(col1);
				calendarioFaseI.setId(id.intValue());
//				System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				calendarioFaseI.setId(null);
			}
		}
		if (col2 != null && !col2.isEmpty()) {
			try {
				Double id = Double.valueOf(col2);
				calendarioFaseI.setCategoriaId(id.intValue());
//				System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				calendarioFaseI.setCategoriaId(null);
			}
		} 
		if (col3 != null && !col3.isEmpty()) {
			calendarioFaseI.setGrupo(col3);
//			System.out.print(col3 + " | ");
		}
		if (col4 != null && !col4.isEmpty()) {
			try {
				Double id = Double.valueOf(col4);
				calendarioFaseI.setJugador1Id(id.intValue());
//				System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				calendarioFaseI.setId(null);
			}
		}
		if (col5 != null && !col5.isEmpty()) {
			try {
				Double id = Double.valueOf(col5);
				calendarioFaseI.setJuegosJugador1(id.intValue());
//				System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				calendarioFaseI.setJuegosJugador1(null);
			}
		} 
		if (col6 != null && !col6.isEmpty()) {
			try {
				Double id = Double.valueOf(col6);
				calendarioFaseI.setJuegosJugador2(id.intValue());
//				System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				calendarioFaseI.setJuegosJugador2(null);
			}
		} 
		if (col7 != null && !col7.isEmpty()) {
			try {
				Double id = Double.valueOf(col7);
				calendarioFaseI.setJugador2Id(id.intValue());
//				System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				calendarioFaseI.setJugador2Id(null);
			}
		} 
		if (col8 != null && !col8.isEmpty()) {
			try {
				Double id = Double.valueOf(col8);
				calendarioFaseI.setOrden(id.intValue());
//				System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				calendarioFaseI.setOrden(null);
			}
		}
		if (col9 != null && !col9.isEmpty()) {
			calendarioFaseI.setActivo(col9);
//			System.out.print(col9 + " | ");
		}
		if (col10 != null && !col10.isEmpty()) {
			calendarioFaseI.setInicia(col10);
//			System.out.print(col10 + " | ");
		}
		if (col11 != null && !col11.isEmpty()) {
			try {
				Double id = Double.valueOf(col11);
				calendarioFaseI.setBoleraId(id.intValue());
//				System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				calendarioFaseI.setBoleraId(null);
			}
		}
		return calendarioFaseI;
	}

}
