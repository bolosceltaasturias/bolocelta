package com.bolocelta.transformer;

import org.apache.commons.csv.CSVRecord;

import com.bolocelta.entities.Documentos;

public class DocumentosTransformer {

	public static Documentos transformerObjectCsv(Documentos documentos, CSVRecord row) {
		
        // Accessing Values by Column Index
        String col1 = row.get(0);
        String col2 = row.get(1);
        String col3 = row.get(2);

		if (col1 != null && !col1.isEmpty()) {
			Double id = Double.valueOf(col1);
			documentos.setId(id.intValue());
		} 
		if (col2 != null && !col2.isEmpty()) {
			documentos.setNombre(col2);
		} 
		if (col3 != null && !col3.isEmpty()) {
			documentos.setDescripcion(col3);
		} 

		return documentos;
	}

}
