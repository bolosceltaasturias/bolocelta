package com.bolocelta.transformer;

import org.apache.commons.csv.CSVRecord;
import org.apache.poi.ss.usermodel.Cell;

import com.bolocelta.entities.Equipos;

public class EquiposTransformer {

	public static Equipos transformerObject(Equipos equipo, Cell cell) {

		if (cell.getColumnIndex() == 0) {
			Double id = cell.getNumericCellValue();
			equipo.setId(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} else if (cell.getColumnIndex() == 1) {
			Double id = cell.getNumericCellValue();
			equipo.setCategoriaId(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} else if (cell.getColumnIndex() == 2) {
			equipo.setNombre(cell.getStringCellValue());
			//System.out.print(cell.getStringCellValue() + " | ");
		} else if (cell.getColumnIndex() == 3) {
			Double id = cell.getNumericCellValue();
			equipo.setBoleraId(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} else if (cell.getColumnIndex() == 4) {
			equipo.setLogo("/resources/escudos/" + cell.getStringCellValue());
			//System.out.print(cell.getStringCellValue() + " | ");
		}

		return equipo;
	}
	
	public static Equipos transformerObjectCsv(Equipos equipo, CSVRecord row) {
		
        // Accessing Values by Column Index
        String col1 = row.get(0);
        String col2 = row.get(1);
        String col3 = row.get(2);
        String col4 = row.get(3);
        String col5 = row.get(4);
        String col6 = row.get(5);
        String col7 = row.get(6);
        String col8 = row.get(7);
        String col9 = row.get(8);
        String col10 = row.get(9);
        String col11 = row.get(10);
        String col12 = row.get(11);

		if (col1 != null && !col1.isEmpty()) {
			Double id = Double.valueOf(col1);
			equipo.setId(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col2 != null && !col2.isEmpty()) {
			Double id = Double.valueOf(col2);
			equipo.setCategoriaId(id.intValue());
			//System.out.print(col2 + " | ");
		} 
		if (col3 != null && !col3.isEmpty()) {
			equipo.setNombre(col3);
			//System.out.print(col3 + " | ");
		} 
		if (col4 != null && !col4.isEmpty()) {
			Double id = Double.valueOf(col4);
			equipo.setBoleraId(id.intValue());
			//System.out.print(col4 + " | ");
		}
		if (col5 != null && !col5.isEmpty()) {
			equipo.setLogo("/resources/escudos/" + col5);
			//System.out.print(col5 + " | ");
		}
		if (col6 != null && !col6.isEmpty()) {
			equipo.setHorarioPreferenteSabadoMaņana(col6);
			//System.out.print(col6 + " | ");
		} 
		if (col7 != null && !col7.isEmpty()) {
			equipo.setHorarioPreferenteSabadoTarde(col7);
			//System.out.print(col7 + " | ");
		} 
		if (col8 != null && !col8.isEmpty()) {
			equipo.setHorarioPreferenteDomingoMaņana(col8);
			//System.out.print(col8 + " | ");
		} 
		if (col9 != null && !col9.isEmpty()) {
			equipo.setHorarioPreferenteDomingoTarde(col9);
			//System.out.print(col9 + " | ");
		}
		if (col10 != null && !col10.isEmpty()) {
			equipo.setEmail(col10);
			//System.out.print(col10 + " | ");
		}
		if (col11 != null && !col11.isEmpty()) {
			equipo.setLiga(col11);
			//System.out.print(col11 + " | ");
		}
		if (col12 != null && !col12.isEmpty()) {
			Double id = Double.valueOf(col12);
			equipo.setNoJugarHorarioOtroEquipo(id.intValue());
			//System.out.print(col12 + " | ");
		}

		return equipo;
	}

}
