package com.bolocelta.transformer;

import org.apache.commons.csv.CSVRecord;

import com.bolocelta.entities.HistoricoCampeonatosEspana;

public class HistoricoCampeonatoEspanaTransformer {
	
	public static HistoricoCampeonatosEspana transformerObjectCsv(HistoricoCampeonatosEspana historicoCE, CSVRecord row) {
		
        // Accessing Values by Column Index
        String col1 = row.get(0);
        String col2 = row.get(1);
        String col3 = row.get(2);
        String col4 = row.get(3);
        String col5 = row.get(4);
        String col6 = row.get(5);
        String col7 = row.get(6);

		if (col1 != null && !col1.isEmpty()) {
			Double id = Double.valueOf(col1);
			historicoCE.setId(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col2 != null && !col2.isEmpty()) {
			Double anyo = Double.valueOf(col2);
			historicoCE.setAnyo(anyo.intValue());
			//System.out.print(col2 + " | ");
		} 
		if (col3 != null && !col3.isEmpty()) {
			Double tipo = Double.valueOf(col3);
			historicoCE.setTipo(tipo.intValue());
			//System.out.print(col3 + " | ");
		} 
		if (col4 != null && !col4.isEmpty()) {
			historicoCE.setLocalidad(col4);
			//System.out.print(col4 + " | ");
		}
		if (col5 != null && !col5.isEmpty()) {
			historicoCE.setNombre(col5);
			//System.out.print(col5 + " | ");
		}
		if (col6 != null && !col6.isEmpty()) {
			Double posicion = Double.valueOf(col6);
			historicoCE.setPosicion(posicion.intValue());
			//System.out.print(col3 + " | ");
		} 
		if (col7 != null && !col7.isEmpty()) {
			historicoCE.setComunidad(col7);
			//System.out.print(col5 + " | ");
		}
		return historicoCE;
	}

}
