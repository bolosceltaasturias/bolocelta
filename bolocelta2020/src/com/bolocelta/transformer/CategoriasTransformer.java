package com.bolocelta.transformer;

import org.apache.commons.csv.CSVRecord;
import org.apache.poi.ss.usermodel.Cell;

import com.bolocelta.bbdd.constants.Modalidad;
import com.bolocelta.entities.Categorias;
import com.bolocelta.entities.Jugadores;

public class CategoriasTransformer {

	public static Categorias transformerObject(Categorias categoria, Cell cell) {

		if (cell.getColumnIndex() == 0) {
			Double id = cell.getNumericCellValue();
			categoria.setId(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} else if (cell.getColumnIndex() == 1) {
			categoria.setNombreCategoria(cell.getStringCellValue());
			//System.out.print(cell.getStringCellValue() + " | ");
		} else if (cell.getColumnIndex() == 2) {
			categoria.setIndividual(cell.getStringCellValue());
			//System.out.print(cell.getStringCellValue() + " | ");
		} else if (cell.getColumnIndex() == 3) {
			categoria.setParejas(cell.getStringCellValue());
			//System.out.print(cell.getStringCellValue() + " | ");
		}

		return categoria;
	}
	
	public static Categorias transformerObjectCsv(Categorias categoria, CSVRecord row) {
		
        // Accessing Values by Column Index
        String col1 = row.get(0);
        String col2 = row.get(1);
        String col3 = row.get(2);
        String col4 = row.get(3);
        String col5 = row.get(4);

		if (col1 != null && !col1.isEmpty()) {
			Double id = Double.valueOf(col1);
			categoria.setId(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col2 != null && !col2.isEmpty()) {
			categoria.setNombreCategoria(col2);
			//System.out.print(col2 + " | ");
		} 
		if (col3 != null && !col3.isEmpty()) {
			categoria.setIndividual(col3);
			//System.out.print(col3 + " | ");
		} 
		if (col4 != null && !col4.isEmpty()) {
			categoria.setParejas(col4);
			//System.out.print(col4 + " | ");
		}
		if (col5 != null && !col5.isEmpty()) {
			categoria.setLiga(col5);
			//System.out.print(col5 + " | ");
		}
		return categoria;
	}

}
