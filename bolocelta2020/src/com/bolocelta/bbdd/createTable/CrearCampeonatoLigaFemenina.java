package com.bolocelta.bbdd.createTable;

import com.bolocelta.bbdd.constants.NombresTablas;
import com.bolocelta.bbdd.constants.Ubicaciones;
import com.bolocelta.bbdd.constants.structure.Estructura;
import com.bolocelta.bbdd.constants.structure.EstructuraCampeonatoLigaFemenina;
import com.bolocelta.entities.CampeonatoLigaFemeninaCalendario;
import com.bolocelta.entities.CampeonatoLigaFemeninaClasificacion;

public class CrearCampeonatoLigaFemenina extends ACrearModificar {

	public void actualizarResultadoCalendarioLiga(CampeonatoLigaFemeninaCalendario cec) {
		Long fila = cec.getRowNum();
		
		EstructuraCampeonatoLigaFemenina eclf = new EstructuraCampeonatoLigaFemenina(
				NombresTablas.N_CSV_BBDD_CAMPEONATO_CAL_LIGA_FEMENINA,
				NombresTablas.N_CSV_BBDD_CAMPEONATO_CLA_LIGA_FEMENINA);
		for (Estructura ef : eclf.getCalendarioList()) {
			if (ef.getValor().equals(eclf.COL_CALENDARIO_TIRADA)) {
				updateRowCalendario(ef, fila, cec.getTirada(), cec);
			}else if (ef.getValor().equals(eclf.COL_CALENDARIO_SACADA)) {
				updateRowCalendario(ef, fila, cec.getSacada(), cec);
			}else if (ef.getValor().equals(eclf.COL_CALENDARIO_PUNTOS)) {
				updateRowCalendario(ef, fila, cec.getPuntos(), cec);
			}
		}
	
		
	}

	public void updateRowCalendario(Estructura update, Long fila, Object newValue, CampeonatoLigaFemeninaCalendario cec) {
		updateDataCsv(update.getExcel(), Ubicaciones.UBICACION_BBDD_CAMPEONATO_EQUIPOS, fila, update.getColumna(), newValue, update.getTipoDato());
	}
	
	public void actualizarClasificacion(CampeonatoLigaFemeninaClasificacion cecCla, CampeonatoLigaFemeninaCalendario cecCal) {
		
		Long filaCla = cecCla.getRowNum();
				
		if(cecCal.isModificable()){
		
			//Actualizar clasificacion de equipo 1
			EstructuraCampeonatoLigaFemenina ecl1 = new EstructuraCampeonatoLigaFemenina(
					NombresTablas.N_CSV_BBDD_CAMPEONATO_CAL_LIGA_FEMENINA,
					NombresTablas.N_CSV_BBDD_CAMPEONATO_CLA_LIGA_FEMENINA);
			for (Estructura ef : ecl1.getClasificacionList()) {
				if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA)) {
					cecCla.setPuntosTirada(cecCla.getPuntosTirada()+cecCal.getTirada());
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA)) {
					cecCla.setPuntosSacada(cecCla.getPuntosSacada()+cecCal.getSacada());
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS)) {
					cecCla.setPuntos(cecCla.getPuntos()+cecCal.getPuntos());
					updateRowClasificacion(ef, filaCla, cecCla.getPuntos(), cecCla);
				}
			}
		}
	}
	
	public void updateRowClasificacion(Estructura update, Long fila, Object newValue, CampeonatoLigaFemeninaClasificacion cec) {
		updateDataCsv(update.getExcel(), Ubicaciones.UBICACION_BBDD_CAMPEONATO_EQUIPOS, fila, update.getColumna(), newValue, update.getTipoDato());
	}
	
	public void actualizarResultadoCalendarioLigaConfirmar(CampeonatoLigaFemeninaCalendario cec) {
		Long fila = cec.getRowNum();
		EstructuraCampeonatoLigaFemenina ecl1 = new EstructuraCampeonatoLigaFemenina(
				NombresTablas.N_CSV_BBDD_CAMPEONATO_CAL_LIGA_FEMENINA,
				NombresTablas.N_CSV_BBDD_CAMPEONATO_CLA_LIGA_FEMENINA);
		for (Estructura ef : ecl1.getCalendarioList()) {
			if (ef.getValor().equals(ecl1.COL_CALENDARIO_ACTIVO)) {
				updateRowCalendario(ef, fila, cec.getActivo(), cec);
			}
		}
	}
	

}
