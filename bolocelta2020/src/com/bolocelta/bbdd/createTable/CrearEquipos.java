package com.bolocelta.bbdd.createTable;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import com.bolocelta.bbdd.constants.Ubicaciones;
import com.bolocelta.bbdd.constants.structure.Estructura;
import com.bolocelta.bbdd.constants.structure.EstructuraEquipos;
import com.bolocelta.entities.Equipos;
import com.bolocelta.entities.Jugadores;

public class CrearEquipos extends ACrearModificar {
	
	public void preparateUpdateHorarioPreferente(Equipos equipo, String colHorarioPreferente) {

		EstructuraEquipos eq = new EstructuraEquipos();

		Long fila = equipo.getRowNum();

		for (Estructura update : eq.getList()) {
			// Cabeceras
			if (update.getValor().equals(colHorarioPreferente) && update.getValor().equals(eq.COL_HORARIO_PSM)) {
				updateRow(update, fila, equipo.getHorarioPreferenteSabadoMaņana());
			}else if (update.getValor().equals(colHorarioPreferente) && update.getValor().equals(eq.COL_HORARIO_PST)) {
				updateRow(update, fila, equipo.getHorarioPreferenteSabadoTarde());
			}else if (update.getValor().equals(colHorarioPreferente) && update.getValor().equals(eq.COL_HORARIO_PDM)) {
				updateRow(update, fila, equipo.getHorarioPreferenteDomingoMaņana());
			}else if (update.getValor().equals(colHorarioPreferente) && update.getValor().equals(eq.COL_HORARIO_PDT)) {
				updateRow(update, fila, equipo.getHorarioPreferenteDomingoTarde());
			}
		}
	}

	public void preparateUpdateEmail(Equipos equipo) {

		EstructuraEquipos eq = new EstructuraEquipos();

		Long fila = equipo.getRowNum();

		for (Estructura update : eq.getList()) {
			// Cabeceras
			if (update.getValor().equals(eq.COL_EMAIL)) {
				updateRow(update, fila, equipo.getEmail());
			}
		}
	}

	private void updateRow(Estructura update, Long fila, Object newValue) {
		updateDataCsv(update.getExcel(), Ubicaciones.UBICACION_BBDD_ADMINISTRACION, fila, update.getColumna(), newValue, update.getTipoDato());
	}
	
	public boolean validateEmail(Equipos equipo){
		boolean valid = true;
		if(equipo.getEmail() != null && !equipo.getEmail().isEmpty() && !emailValidator.validate(equipo.getEmail())){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El email no es valido", null));
			valid = false;
		}
		return valid;
	}
	
}
