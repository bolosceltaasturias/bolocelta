package com.bolocelta.bbdd.createTable;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import com.bolocelta.bbdd.constants.Modalidad;
import com.bolocelta.bbdd.constants.Ubicaciones;
import com.bolocelta.bbdd.constants.structure.Estructura;
import com.bolocelta.bbdd.constants.structure.EstructuraJugadores;
import com.bolocelta.entities.Jugadores;

public class CrearJugadores extends ACrearModificar {
	
	public void preparateInsertRow(Jugadores jugador) {
		
		EstructuraJugadores ej = new EstructuraJugadores();
		
		if(jugador.isMasculino()){
			jugador.setModalidad(Modalidad.MASCULINO);
		}else if(jugador.isFemenino()){
			jugador.setModalidad(Modalidad.FEMENINO);
		}
		
		insertRow(ej.N_CSV_BBDD_JUGADORES, jugador.getInsertRow());

	}

	public void preparateUpdateRow(Jugadores jugador) {

		EstructuraJugadores ej = new EstructuraJugadores();
		
		Long fila = jugador.getRowNum();

		for (Estructura update : ej.getList()) {
			// Cabeceras
			if (update.getValor().equals(ej.COL_ID)) {
				updateRow(update, fila, fila);
			} else if (update.getValor().equals(ej.COL_EQUIPO)) {
				updateRow(update, fila, jugador.getEquipoId());
			} else if (update.getValor().equals(ej.COL_NOMBRE)) {
				updateRow(update, fila, jugador.getNombre());
			} else if (update.getValor().equals(ej.COL_APODO)) {
				updateRow(update, fila, jugador.getApodo());
			} else if (update.getValor().equals(ej.COL_NUM_FICHA)) {
				updateRow(update, fila, jugador.getNumeroFicha());
			} else if (update.getValor().equals(ej.COL_EMAIL)) {
				updateRow(update, fila, jugador.getEmail());
			} else if (update.getValor().equals(ej.COL_TELEFONO)) {
				updateRow(update, fila, jugador.getTelefono());
			} else if (update.getValor().equals(ej.COL_EDAD)) {
				updateRow(update, fila, jugador.getEdad());
			} else if (update.getValor().equals(ej.COL_ACTIVO)) {
				updateRow(update, fila, jugador.getActivo());
			} else if (update.getValor().equals(ej.COL_MODALIDAD)) {
				if(jugador.isMasculino()){
					jugador.setModalidad(Modalidad.MASCULINO);
				}else if(jugador.isFemenino()){
					jugador.setModalidad(Modalidad.FEMENINO);
				}
				updateRow(update, fila, jugador.getModalidad());
			} else if (update.getValor().equals(ej.COL_INDIV_PRI)) {
				updateRow(update, fila, jugador.getIndividualPrimera());
			} else if (update.getValor().equals(ej.COL_INDIV_SEG)) {
				updateRow(update, fila, jugador.getIndividualSegunda());
			} else if (update.getValor().equals(ej.COL_INDIV_TER)) {
				updateRow(update, fila, jugador.getIndividualTercera());
			} else if (update.getValor().equals(ej.COL_INDIV_FEM)) {
				updateRow(update, fila, jugador.getIndividualFemenino());
			}
		}
	}
	
	public void preparateDeleteLogicRow(Jugadores jugador) {

		EstructuraJugadores ej = new EstructuraJugadores();

		Long fila = jugador.getRowNum();

		for (Estructura update : ej.getList()) {
			// Cabeceras
			if (update.getValor().equals(ej.COL_ACTIVO)) {
				updateRow(update, fila, jugador.getActivo());
			}
		}
	}
	
	private void insertRow(String excel, String newRowData) {
		insertDataCsv(excel, Ubicaciones.UBICACION_BBDD_ADMINISTRACION, newRowData);
	}

	private void updateRow(Estructura update, Long fila, Object newValue) {
		updateDataCsv(update.getExcel(), Ubicaciones.UBICACION_BBDD_ADMINISTRACION, fila, update.getColumna(), newValue, update.getTipoDato());
	}
	
	public boolean validate(Jugadores jugador){
		boolean valid = true;
		if(jugador.getNombre() == null || jugador.getNombre().isEmpty()){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El nombre es requerido.", null));
			valid = false;
		}else if(jugador.getApodo() == null || jugador.getApodo().isEmpty()){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El apodo es requerido.", null));
			valid = false;
		}else if(jugador.getEdad() == null || jugador.getEdad() < 1){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "La edad es requerida.", null));
			valid = false;
		}else if(jugador.getNumeroFicha() == null || jugador.getNumeroFicha().isEmpty()){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El n�mero de ficha  es requerida.", null));
			valid = false;
		}else if(jugador.getTelefono() != null && !jugador.getTelefono().isEmpty() && !telefonoValidator.validate(jugador.getTelefono())){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El tel�fono no es valido", null));
			valid = false;
		}else if(jugador.getEmail() != null && !jugador.getEmail().isEmpty() && !emailValidator.validate(jugador.getEmail())){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El email no es valido", null));
			valid = false;
		}else if(!jugador.isMasculino() && !jugador.isFemenino()){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "La modalidad es requerida.", null));
			valid = false;
		}else if(jugador.isMasculino() && jugador.isFemenino()){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Solo es posible seleccionar una modalidad.", null));
			valid = false;
		}
		return valid;
	}

}
