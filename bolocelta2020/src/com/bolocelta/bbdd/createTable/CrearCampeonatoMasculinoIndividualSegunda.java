package com.bolocelta.bbdd.createTable;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.bolocelta.bbdd.constants.Activo;
import com.bolocelta.bbdd.constants.EstadosIndividualParejas;
import com.bolocelta.bbdd.constants.NombresTablas;
import com.bolocelta.bbdd.constants.Ubicaciones;
import com.bolocelta.bbdd.constants.structure.Estructura;
import com.bolocelta.bbdd.constants.structure.EstructuraCampeonatoMasculinoIndividualSegunda;
import com.bolocelta.bbdd.readTables.LeerCampeonatoMasculinoIndividualSegunda;
import com.bolocelta.entities.Categorias;
import com.bolocelta.entities.Fases;
import com.bolocelta.entities.Jugadores;
import com.bolocelta.entities.ParticipantesIndividual;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseCF;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseFC;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseFF;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseI;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseOF;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseSF;
import com.bolocelta.entities.sorteos.individual.ClasificacionFaseI;

public class CrearCampeonatoMasculinoIndividualSegunda extends ACrearModificar {
	
	LeerCampeonatoMasculinoIndividualSegunda leerCampeonatoMasculinoIndividualSegunda = new LeerCampeonatoMasculinoIndividualSegunda();

	private void crearExcel() {

	}
	
	private void crearHojaConfiguracion(Date fechaMaximaInscripcion, String estado, String bolera){
		createWorkBookCsv(NombresTablas.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CONFIG,
				Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
	}
	
	private void crearHojaFases(){
		createWorkBookCsv(NombresTablas.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_FASES,
				Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
	}
	
	private void crearHojaParticipantes(){
		createWorkBookCsv(NombresTablas.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_PARTICIPANTES,
				Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
	}
	
	private void crearHojaGrupos(){
		createWorkBookCsv(NombresTablas.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_GRUPOS,
				Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
	}
	
	private void crearHojaClasificacionFaseI(){
		createWorkBookCsv(NombresTablas.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CLA_FASE_I,
				Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
	}
	
	private void crearHojaCalendarioFaseI(){
		createWorkBookCsv(NombresTablas.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_I,
				Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
	}
	
	private void crearHojaClasificacionFaseII(){
		createWorkBookCsv(NombresTablas.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CLA_FASE_II,
				Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
	}
	
	private void crearHojaCalendarioFaseII(){
		createWorkBookCsv(NombresTablas.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_II,
				Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
	}
	
	private void crearHojaCalendarioFaseOF(){
		createWorkBookCsv(NombresTablas.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_OF,
				Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
	}
	
	private void crearHojaCalendarioFaseCF(){
		createWorkBookCsv(NombresTablas.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_CF,
				Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
	}
	
	private void crearHojaCalendarioFaseSF(){
		createWorkBookCsv(NombresTablas.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_SF,
				Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
	}
	
	private void crearHojaCalendarioFaseFC(){
		createWorkBookCsv(NombresTablas.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_FC,
				Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
	}
	
	private void crearHojaCalendarioFaseFF(){
		createWorkBookCsv(NombresTablas.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_FF,
				Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
	}
	
	private void insertRow(String excel, String newRowData) {
		insertDataCsv(excel, Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA, newRowData);
	}
	
	public void updateRow(Estructura update, Long fila, Object newValue) {
		updateDataCsv(update.getExcel(), Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA, fila, update.getColumna(), newValue, update.getTipoDato());
	}
	
	
	//Creacion de la base del campeonato
	public void crearBaseCampeonato(Date fechaMaximaInscripcion, String estado, String bolera){
		//Crear las hojas excel
		crearExcel();
		crearHojaConfiguracion(fechaMaximaInscripcion, estado, bolera);
		crearHojaFases();
		crearHojaParticipantes();
		
		
		//Crear la estructura de las hojas (cabeceras y registros por defecto)
		EstructuraCampeonatoMasculinoIndividualSegunda ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda();
		//Crear las estructuras de las hojas
		crearEstructuraHojas(ecmi);
		//Rellenar datos de configuración
		rellenarDatosConfiguracion(ecmi, fechaMaximaInscripcion, estado, bolera);
		//Rellenar todas las fases
		rellenarDatosFases(ecmi);
	}
	
	public void crearEstructuraHojas(EstructuraCampeonatoMasculinoIndividualSegunda ecmi) {
		//Configuracion
		for (String row : ecmi.getInsertCabeceraConfiguracionRow()) {
			insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CONFIG, row);
		}
		//Fases
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_FASES, ecmi.getInsertCabeceraFasesRow());
		//Participantes
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_PARTICIPANTES, ecmi.getInsertCabeceraParticipantesRow());
	}

	private void rellenarDatosConfiguracion(EstructuraCampeonatoMasculinoIndividualSegunda ecmi, Date fechaMaximaInscripcion, String estado, 
			String bolera) {
		//Actualizar la fecha
	    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy"); // date pattern
	    String fechaMaxInscripcion = simpleDateFormat.format(fechaMaximaInscripcion);
		updateRow(ecmi.getEstructuraFechaMaximaInscripcion(), ecmi.getEstructuraFechaMaximaInscripcion().getFila(), fechaMaxInscripcion);
		//Actualizar el estado
		updateRow(ecmi.getEstructuraEstadoCampeonato(), ecmi.getEstructuraEstadoCampeonato().getFila(), estado);
		updateRow(ecmi.getEstructuraBoleraFinal(), ecmi.getEstructuraBoleraFinal().getFila(), bolera);
	}
	
	public void actualizarEstadoConfiguracion(String estado) {
		EstructuraCampeonatoMasculinoIndividualSegunda ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda();
		//Actualizar el estado
		updateRow(ecmi.getEstructuraEstadoCampeonato(), ecmi.getEstructuraEstadoCampeonato().getFila(), estado);
	}
	
	private void rellenarDatosFases(EstructuraCampeonatoMasculinoIndividualSegunda ecmi) {
		for(String fase : ecmi.getEstructuraTodasFases()){
			insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_FASES, fase);
		}
	}
	
	public void inscribirNewJugador(Jugadores jugador, Categorias categoria, String usuario, String activo) {
		EstructuraCampeonatoMasculinoIndividualSegunda ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda();
    	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss"); // date pattern
    	String fecha = simpleDateFormat.format(new Date());
    	
    	Long newRowNum = leerCampeonatoMasculinoIndividualSegunda.lastRowSheet(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_PARTICIPANTES, Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
    	newRowNum++;
    	
	    String insert = newRowNum.toString() + ";" + jugador.getId().toString() + ";" + fecha + ";" + usuario + ";" + activo;
	    insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_PARTICIPANTES, insert);
	}
	
	public void inscribirUpdateJugador(ParticipantesIndividual participantesIndividual, Jugadores jugador, Categorias categoria, String usuario, String activo) {
		EstructuraCampeonatoMasculinoIndividualSegunda ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda();
		
		Long fila = participantesIndividual.getRowNum();
		
		for (Estructura eip : ecmi.getEstructuraInscribirParticipantes()) {
			
			// Cabeceras
			if (eip.getValor().equals(ecmi.COL_PARTICIPANTES_ID)) {
				updateRow(eip, fila, fila.toString());
			} else if (eip.getValor().equals(ecmi.COL_PARTICIPANTES_ID_JUGADOR)) {
				updateRow(eip, fila, jugador.getId().toString());
			} else if (eip.getValor().equals(ecmi.COL_PARTICIPANTES_USUARIO)) {
				updateRow(eip, fila, usuario);
			} else if (eip.getValor().equals(ecmi.COL_PARTICIPANTES_FECHA_INSCRIPCION)) {
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss"); // date pattern
			    String fecha = simpleDateFormat.format(new Date());
				updateRow(eip, fila, fecha);
			} else if (eip.getValor().equals(ecmi.COL_PARTICIPANTES_ACTIVO)) {
				updateRow(eip, fila, activo);
			}

		}
	}
	
	
	public void actualizarActivoFase(Fases fase) {
		EstructuraCampeonatoMasculinoIndividualSegunda ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda();
		Long fila = fase.getRowNum();
		for (Estructura ef : ecmi.getEstructuraFases()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_FASES_ACTIVO)) {
				updateRow(ef, fila, fase.getActivo());
			}
		}
	}
	
	public void actualizarFase(Fases fase) {
		EstructuraCampeonatoMasculinoIndividualSegunda ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda();
		Long fila = fase.getRowNum();
		for (Estructura ef : ecmi.getEstructuraFases()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_FASES_NUMERO_ENFRENTAMIENTOS_GRUPOS)) {
				updateRow(ef, fila, fase.getNumeroEnfrentamientos());
			}else if (ef.getValor().equals(ecmi.COL_FASES_NUMERO_PARTIDAS)) {
				updateRow(ef, fila, fase.getNumeroPartidas());
			}else if (ef.getValor().equals(ecmi.COL_FASES_NUMERO_JUEGOS)) {
				updateRow(ef, fila, fase.getNumeroJuegos());
			}else if (ef.getValor().equals(ecmi.COL_FASES_CLASIFICAN)) {
				updateRow(ef, fila, fase.getClasifican());
			}else if (ef.getValor().equals(ecmi.COL_FASES_FECHA)) {
				updateRow(ef, fila, fase.getFecha());
			}else if (ef.getValor().equals(ecmi.COL_FASES_HORA)) {
				updateRow(ef, fila, fase.getHora());
			}else if (ef.getValor().equals(ecmi.COL_FASES_FASE_SIGUIENTE)) {
				updateRow(ef, fila, fase.getFaseSiguiente());
			}
			
		}		
	}
	
	public void crearClasificacionFaseI(){
		EstructuraCampeonatoMasculinoIndividualSegunda ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CLA_FASE_I, Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CLA_FASE_I, Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		}else{
			crearHojaClasificacionFaseI();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CLA_FASE_I, ecmi.getInsertCabeceraClasificacionFaseIRow());
	}
	
	public void insertarRowClasificacionFaseI(String insert){
		EstructuraCampeonatoMasculinoIndividualSegunda ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CLA_FASE_I, insert);
	}
	
	public void crearCalendarioFaseI(){
		EstructuraCampeonatoMasculinoIndividualSegunda ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_I, Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_I, Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		}else{
			crearHojaCalendarioFaseI();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_I, ecmi.getInsertCabeceraCalendarioFaseIRow());
	}
	
	public void insertarRowCalendarioFaseI(String insert){
		EstructuraCampeonatoMasculinoIndividualSegunda ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_I, insert);
	}
	
	public void actualizarResultadoCalendarioFaseI(CalendarioFaseI calendarioFaseI, ClasificacionFaseI clasificacionFaseIJugador1, 
			ClasificacionFaseI clasificacionFaseIJugador2) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoIndividualSegunda ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda();
		Long fila = calendarioFaseI.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
		
		if(calendarioFaseI.isModificable()){
			for (Estructura ef : ecmi.getEstructuraCalendarioFaseI()) {
				// Cabeceras
				if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_I_JUEGOS_JUGADOR_1)) {
					updateRow(ef, fila, calendarioFaseI.getJuegosJugador1());
				}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_I_JUEGOS_JUGADOR_2)) {
					updateRow(ef, fila, calendarioFaseI.getJuegosJugador2());
				}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_I_ACTIVO)) {
					calendarioFaseI.setActivo(Activo.NO);
					updateRow(ef, fila, calendarioFaseI.getActivo());
				}
			}
			fila = clasificacionFaseIJugador1.getRowNum();
			//Actualizar resultado en clasificacion Jugador 1
			for (Estructura ef : ecmi.getEstructuraClasificacionFaseI()) {
				// Cabeceras
				if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PJ)) {
					clasificacionFaseIJugador1.setPj(clasificacionFaseIJugador1.getPj()+1);
					updateRow(ef, fila, clasificacionFaseIJugador1.getPj());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PG)) {
					if(calendarioFaseI.isGanaJugador1()){
						clasificacionFaseIJugador1.setPg(clasificacionFaseIJugador1.getPg()+1);
					}
					updateRow(ef, fila, clasificacionFaseIJugador1.getPg());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PE)) {
					if(calendarioFaseI.isEmpate()){
						clasificacionFaseIJugador1.setPe(clasificacionFaseIJugador1.getPe()+1);
					}
					updateRow(ef, fila, clasificacionFaseIJugador1.getPe());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PP)) {
					if(calendarioFaseI.isGanaJugador2()){
						clasificacionFaseIJugador1.setPp(clasificacionFaseIJugador1.getPp()+1);
					}
					updateRow(ef, fila, clasificacionFaseIJugador1.getPp());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PF)) {
					clasificacionFaseIJugador1.setPf(clasificacionFaseIJugador1.getPf()+calendarioFaseI.getJuegosJugador1());
					updateRow(ef, fila, clasificacionFaseIJugador1.getPf());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PC)) {
					clasificacionFaseIJugador1.setPc(clasificacionFaseIJugador1.getPc()+calendarioFaseI.getJuegosJugador2());
					updateRow(ef, fila, clasificacionFaseIJugador1.getPc());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PT)) {
					if(calendarioFaseI.isGanaJugador1()){
						clasificacionFaseIJugador1.setPt(clasificacionFaseIJugador1.getPt()+3);
					}else if(calendarioFaseI.isEmpate()){
						clasificacionFaseIJugador1.setPt(clasificacionFaseIJugador1.getPt()+1);
					}
					updateRow(ef, fila, clasificacionFaseIJugador1.getPt());
				}
			}
			fila = clasificacionFaseIJugador2.getRowNum();
			//Actualizar resultado en clasificacion Jugador 2
			for (Estructura ef : ecmi.getEstructuraClasificacionFaseI()) {
				// Cabeceras
				if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PJ)) {
					clasificacionFaseIJugador2.setPj(clasificacionFaseIJugador2.getPj()+1);
					updateRow(ef, fila, clasificacionFaseIJugador2.getPj());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PG)) {
					if(calendarioFaseI.isGanaJugador2()){
						clasificacionFaseIJugador2.setPg(clasificacionFaseIJugador2.getPg()+1);
					}
					updateRow(ef, fila, clasificacionFaseIJugador2.getPg());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PE)) {
					if(calendarioFaseI.isEmpate()){
						clasificacionFaseIJugador2.setPe(clasificacionFaseIJugador2.getPe()+1);
					}
					updateRow(ef, fila, clasificacionFaseIJugador2.getPe());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PP)) {
					if(calendarioFaseI.isGanaJugador1()){
						clasificacionFaseIJugador2.setPp(clasificacionFaseIJugador2.getPp()+1);
					}
					updateRow(ef, fila, clasificacionFaseIJugador2.getPp());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PF)) {
					clasificacionFaseIJugador2.setPf(clasificacionFaseIJugador2.getPf()+calendarioFaseI.getJuegosJugador2());
					updateRow(ef, fila, clasificacionFaseIJugador2.getPf());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PC)) {
					clasificacionFaseIJugador2.setPc(clasificacionFaseIJugador2.getPc()+calendarioFaseI.getJuegosJugador1());
					updateRow(ef, fila, clasificacionFaseIJugador2.getPc());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PT)) {
					if(calendarioFaseI.isGanaJugador2()){
						clasificacionFaseIJugador2.setPt(clasificacionFaseIJugador2.getPt()+3);
					}else if(calendarioFaseI.isEmpate()){
						clasificacionFaseIJugador2.setPt(clasificacionFaseIJugador2.getPt()+1);
					}
					updateRow(ef, fila, clasificacionFaseIJugador2.getPt());
				}
			}
		}
		
		
	}
	
	public void crearCalendarioFaseII(){
		EstructuraCampeonatoMasculinoIndividualSegunda ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_II, Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_II, Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		}else{
			crearHojaCalendarioFaseII();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_II, ecmi.getInsertCabeceraCalendarioFaseIIRow());
	}
	
	public void crearClasificacionFaseII(){
		EstructuraCampeonatoMasculinoIndividualSegunda ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CLA_FASE_II, Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CLA_FASE_II, Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		}else{
			crearHojaClasificacionFaseII();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CLA_FASE_II, ecmi.getInsertCabeceraClasificacionFaseIIRow());
	}
	
	public void crearCalendarioFaseOF(){
		EstructuraCampeonatoMasculinoIndividualSegunda ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_OF, Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_OF, Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		}else{
			crearHojaCalendarioFaseOF();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_OF, ecmi.getInsertCabeceraCalendarioFaseOFRow());
	}
	
	public void insertarRowCalendarioFaseOF(String insert){
		EstructuraCampeonatoMasculinoIndividualSegunda ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_OF, insert);
	}
	
	public void actualizarJugadorOctavosFinal(CalendarioFaseOF calendarioFaseOF, boolean jugador1, boolean jugador2) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoIndividualSegunda ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda();
		Long fila = calendarioFaseOF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura ef : ecmi.getEstructuraCalendarioOctavosFinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_JUGADOR_1) && jugador1) {
				updateRow(ef, fila, calendarioFaseOF.getJugador1Id());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_JUGADOR_2) && jugador2) {
				updateRow(ef, fila, calendarioFaseOF.getJugador2Id());
			}
		}
		
		
	}
	
	public void actualizarJugadorCuartosFinal(CalendarioFaseCF calendarioFaseCF, boolean jugador1, boolean jugador2) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoIndividualSegunda ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda();
		Long fila = calendarioFaseCF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura ef : ecmi.getEstructuraCalendarioCuartosFinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_JUGADOR_1) && jugador1) {
				updateRow(ef, fila, calendarioFaseCF.getJugador1Id());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_JUGADOR_2) && jugador2) {
				updateRow(ef, fila, calendarioFaseCF.getJugador2Id());
			}
		}
		
		
	}
	
	public void actualizarJugadorSemiFinal(CalendarioFaseSF calendarioFaseSF, boolean jugador1, boolean jugador2) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoIndividualSegunda ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda();
		Long fila = calendarioFaseSF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura ef : ecmi.getEstructuraCalendarioSemifinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUGADOR_1) && jugador1) {
				updateRow(ef, fila, calendarioFaseSF.getJugador1Id());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUGADOR_2) && jugador2) {
				updateRow(ef, fila, calendarioFaseSF.getJugador2Id());
			}
		}
		
		
	}
	
	public void actualizarJugadorFinalConsolacion(CalendarioFaseFC calendarioFaseFC, boolean jugador1, boolean jugador2) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoIndividualSegunda ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda();
		Long fila = calendarioFaseFC.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura ef : ecmi.getEstructuraCalendarioFinalConsolacion()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_JUGADOR_1) && jugador1) {
				updateRow(ef, fila, calendarioFaseFC.getJugador1Id());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_JUGADOR_2) && jugador2) {
				updateRow(ef, fila, calendarioFaseFC.getJugador2Id());
			}
		}
		
		
	}
	
	public void actualizarJugadorFinal(CalendarioFaseFF calendarioFaseFF, boolean jugador1, boolean jugador2) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoIndividualSegunda ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda();
		Long fila = calendarioFaseFF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura ef : ecmi.getEstructuraCalendarioFinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUGADOR_1) && jugador1) {
				updateRow(ef, fila, calendarioFaseFF.getJugador1Id());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUGADOR_2) && jugador2) {
				updateRow(ef, fila, calendarioFaseFF.getJugador2Id());
			}
		}
		
		
	}
	
	public void actualizarResultadosEnfrentamientoDirectoOF(CalendarioFaseOF calendarioFaseOF) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoIndividualSegunda ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda();
		Long fila = calendarioFaseOF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura ef : ecmi.getEstructuraCalendarioOctavosFinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_JUEGOS_JUGADOR_1_P1)) {
				updateRow(ef, fila, calendarioFaseOF.getJuegosJugador1P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_JUEGOS_JUGADOR_2_P1)) {
				updateRow(ef, fila, calendarioFaseOF.getJuegosJugador2P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_JUEGOS_JUGADOR_1_P2)) {
				updateRow(ef, fila, calendarioFaseOF.getJuegosJugador1P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_JUEGOS_JUGADOR_2_P2)) {
				updateRow(ef, fila, calendarioFaseOF.getJuegosJugador2P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_JUEGOS_JUGADOR_1_P3)) {
				updateRow(ef, fila, calendarioFaseOF.getJuegosJugador1P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_JUEGOS_JUGADOR_2_P3)) {
				updateRow(ef, fila, calendarioFaseOF.getJuegosJugador2P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_ACTIVO)) {
				calendarioFaseOF.setActivo(Activo.NO);
				updateRow(ef, fila, calendarioFaseOF.getActivo());
			}
		}
		
		
	}
	
	public void crearCalendarioFaseCF(){
		EstructuraCampeonatoMasculinoIndividualSegunda ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_CF, Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_CF, Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		}else{
			crearHojaCalendarioFaseCF();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_CF, ecmi.getInsertCabeceraCalendarioFaseCFRow());
	}
	
	public void insertarRowCalendarioFaseCF(String insert){
		EstructuraCampeonatoMasculinoIndividualSegunda ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_CF, insert);
	}
	
	public void actualizarResultadosEnfrentamientoDirectoCF(CalendarioFaseCF calendarioFaseCF) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoIndividualSegunda ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda();
		Long fila = calendarioFaseCF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura ef : ecmi.getEstructuraCalendarioCuartosFinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_1_P1)) {
				updateRow(ef, fila, calendarioFaseCF.getJuegosJugador1P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_2_P1)) {
				updateRow(ef, fila, calendarioFaseCF.getJuegosJugador2P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_1_P2)) {
				updateRow(ef, fila, calendarioFaseCF.getJuegosJugador1P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_2_P2)) {
				updateRow(ef, fila, calendarioFaseCF.getJuegosJugador2P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_1_P3)) {
				updateRow(ef, fila, calendarioFaseCF.getJuegosJugador1P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_2_P3)) {
				updateRow(ef, fila, calendarioFaseCF.getJuegosJugador2P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_ACTIVO)) {
				calendarioFaseCF.setActivo(Activo.NO);
				updateRow(ef, fila, calendarioFaseCF.getActivo());
			}
		}
		
		
	}
	
	public void crearCalendarioFaseSF(){
		EstructuraCampeonatoMasculinoIndividualSegunda ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_SF, Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_SF, Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		}else{
			crearHojaCalendarioFaseSF();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_SF, ecmi.getInsertCabeceraCalendarioFaseSFRow());
	}
	
	public void insertarRowCalendarioFaseSF(String insert){
		EstructuraCampeonatoMasculinoIndividualSegunda ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_SF, insert);
	}
	
	public void actualizarResultadosEnfrentamientoDirectoSF(CalendarioFaseSF calendarioFaseSF) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoIndividualSegunda ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda();
		Long fila = calendarioFaseSF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura ef : ecmi.getEstructuraCalendarioSemifinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_1_P1)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosJugador1P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_2_P1)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosJugador2P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_1_P2)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosJugador1P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_2_P2)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosJugador2P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_1_P3)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosJugador1P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_2_P3)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosJugador2P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_ACTIVO)) {
				calendarioFaseSF.setActivo(Activo.NO);
				updateRow(ef, fila, calendarioFaseSF.getActivo());
			}
		}
		
		
	}
	
	public void crearCalendarioFaseFC(){
		EstructuraCampeonatoMasculinoIndividualSegunda ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_FC, Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_FC, Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		}else{
			crearHojaCalendarioFaseFC();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_FC, ecmi.getInsertCabeceraCalendarioFaseFCRow());
	}
	
	public void insertarRowCalendarioFaseFC(String insert){
		EstructuraCampeonatoMasculinoIndividualSegunda ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_FC, insert);
	}
	
	public void actualizarResultadosEnfrentamientoDirectoFC(CalendarioFaseFC calendarioFaseFC) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoIndividualSegunda ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda();
		Long fila = calendarioFaseFC.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura ef : ecmi.getEstructuraCalendarioFinalConsolacion()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_JUEGOS_JUGADOR_1_P1)) {
				updateRow(ef, fila, calendarioFaseFC.getJuegosJugador1P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_JUEGOS_JUGADOR_2_P1)) {
				updateRow(ef, fila, calendarioFaseFC.getJuegosJugador2P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_JUEGOS_JUGADOR_1_P2)) {
				updateRow(ef, fila, calendarioFaseFC.getJuegosJugador1P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_JUEGOS_JUGADOR_2_P2)) {
				updateRow(ef, fila, calendarioFaseFC.getJuegosJugador2P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_JUEGOS_JUGADOR_1_P3)) {
				updateRow(ef, fila, calendarioFaseFC.getJuegosJugador1P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_JUEGOS_JUGADOR_2_P3)) {
				updateRow(ef, fila, calendarioFaseFC.getJuegosJugador2P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_ACTIVO)) {
				calendarioFaseFC.setActivo(Activo.NO);
				updateRow(ef, fila, calendarioFaseFC.getActivo());
			}
		}
		
		
	}
	
	public void crearCalendarioFaseFF(){
		EstructuraCampeonatoMasculinoIndividualSegunda ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_FF, Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_FF, Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		}else{
			crearHojaCalendarioFaseFF();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_FF, ecmi.getInsertCabeceraCalendarioFaseFFRow());
	}
	
	public void insertarRowCalendarioFaseFF(String insert){
		EstructuraCampeonatoMasculinoIndividualSegunda ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_FF, insert);
	}
	
	public void actualizarResultadosEnfrentamientoDirectoFF(CalendarioFaseFF calendarioFaseFF) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoIndividualSegunda ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda();
		Long fila = calendarioFaseFF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura ef : ecmi.getEstructuraCalendarioFinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_1_P1)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosJugador1P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_2_P1)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosJugador2P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_1_P2)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosJugador1P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_2_P2)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosJugador2P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_1_P3)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosJugador1P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_2_P3)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosJugador2P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_ACTIVO)) {
				calendarioFaseFF.setActivo(Activo.NO);
				updateRow(ef, fila, calendarioFaseFF.getActivo());
			}
		}
		
		
	}
	
	public void borrarSorteo(){
		deleteFile(NombresTablas.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_I, Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		deleteFile(NombresTablas.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CLA_FASE_I, Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		deleteFile(NombresTablas.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_II, Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		deleteFile(NombresTablas.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CLA_FASE_II, Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		deleteFile(NombresTablas.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_OF, Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		deleteFile(NombresTablas.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_CF, Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		deleteFile(NombresTablas.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_SF, Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		deleteFile(NombresTablas.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_FC, Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		deleteFile(NombresTablas.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_FF, Ubicaciones.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		actualizarEstadoConfiguracion(EstadosIndividualParejas.CONFIG_ESTADO_CERRAR_INSCRIPCIONES);
	}
	
	

}
