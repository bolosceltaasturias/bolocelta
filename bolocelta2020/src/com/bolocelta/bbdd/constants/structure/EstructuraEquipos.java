package com.bolocelta.bbdd.constants.structure;

import java.util.ArrayList;
import java.util.List;

import com.bolocelta.bbdd.constants.NombresTablas;

public class EstructuraEquipos extends NombresTablas{
	
	private List<Estructura> list = new ArrayList<Estructura>();
	
	
	//Filas
	private final Long F1 = new Long(1);
	
	//Columnas
	private final Integer C1 = 1;
	private final Integer C2 = 2;
	private final Integer C3 = 3;
	private final Integer C4 = 4;
	private final Integer C5 = 5;
	private final Integer C6 = 6;
	private final Integer C7 = 7;
	private final Integer C8 = 8;
	private final Integer C9 = 9;
	private final Integer C10 = 10;
	
	//Cabeceras
	public static final String COL_ID = "ID";
	public static final String COL_CATEGORIA = "CATEGORIA";
	public static final String COL_EQUIPO = "EQUIPO";
	public static final String COL_BOLERA = "BOLERA";
	public static final String COL_LOGO = "LOGO";
	public static final String COL_HORARIO_PSM = "HORARIO_PSM";
	public static final String COL_HORARIO_PST = "HORARIO_PST";
	public static final String COL_HORARIO_PDM = "HORARIO_PDM";
	public static final String COL_HORARIO_PDT = "HORARIO_PDT";
	public static final String COL_EMAIL = "EMAIL";
	
	//Propiedades
	
	//Estructura
	public EstructuraEquipos() {
		//Cabecera
		list.add(new Estructura(this.F1, this.C1 , COL_ID, this.N_CSV_BBDD_EQUIPOS, this.TD_ENTERO));
		list.add(new Estructura(this.F1, this.C2 , COL_CATEGORIA, this.N_CSV_BBDD_EQUIPOS, this.TD_ENTERO));
		list.add(new Estructura(this.F1, this.C3 , COL_EQUIPO, this.N_CSV_BBDD_EQUIPOS, this.TD_TEXTO));
		list.add(new Estructura(this.F1, this.C4 , COL_BOLERA, this.N_CSV_BBDD_EQUIPOS, this.TD_TEXTO));
		list.add(new Estructura(this.F1, this.C5 , COL_LOGO, this.N_CSV_BBDD_EQUIPOS, this.TD_TEXTO));
		list.add(new Estructura(this.F1, this.C6 , COL_HORARIO_PSM, this.N_CSV_BBDD_EQUIPOS, this.TD_TEXTO));
		list.add(new Estructura(this.F1, this.C7 , COL_HORARIO_PST, this.N_CSV_BBDD_EQUIPOS, this.TD_TEXTO));
		list.add(new Estructura(this.F1, this.C8 , COL_HORARIO_PDM, this.N_CSV_BBDD_EQUIPOS, this.TD_TEXTO));
		list.add(new Estructura(this.F1, this.C9 , COL_HORARIO_PDT, this.N_CSV_BBDD_EQUIPOS, this.TD_TEXTO));
		list.add(new Estructura(this.F1, this.C10 , COL_EMAIL, this.N_CSV_BBDD_EQUIPOS, this.TD_TEXTO));
	}
	
	public List<Estructura> getList(){
		return this.list;
	}
	

}
