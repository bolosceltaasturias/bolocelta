package com.bolocelta.bbdd.constants.structure;

public class Estructura {

	private Long fila;
	private Integer columna;
	private Object valor;
	private String sheet;
	private String excel;
	private String tipoDato;

	public Estructura() {
		// TODO Auto-generated constructor stub
	}
	
	public Estructura(Integer columna, Object valor, String sheet, String excel, String tipoDato) {
		this.columna = columna;
		this.valor = valor;
		this.sheet = sheet;
		this.excel = excel;
		this.tipoDato = tipoDato;
	}

	public Estructura(Long fila, Integer columna, Object valor, String sheet, String excel, String tipoDato) {
		this.fila = fila;
		this.columna = columna;
		this.valor = valor;
		this.sheet = sheet;
		this.excel = excel;
		this.tipoDato = tipoDato;
	}
	
	public Estructura(Integer columna, Object valor, String excel, String tipoDato) {
		this.columna = columna;
		this.valor = valor;
		this.excel = excel;
		this.tipoDato = tipoDato;
	}

	public Estructura(Long fila, Integer columna, Object valor, String excel, String tipoDato) {
		this.fila = fila;
		this.columna = columna;
		this.valor = valor;
		this.excel = excel;
		this.tipoDato = tipoDato;
	}

	public Long getFila() {
		return fila;
	}

	public void setFila(Long fila) {
		this.fila = fila;
	}

	public Integer getColumna() {
		return columna;
	}

	public void setColumna(Integer columna) {
		this.columna = columna;
	}

	public Object getValor() {
		return valor;
	}

	public void setValor(Object valor) {
		this.valor = valor;
	}

	public String getSheet() {
		return sheet;
	}

	public void setSheet(String sheet) {
		this.sheet = sheet;
	}

	public String getTipoDato() {
		return tipoDato;
	}

	public void setTipoDato(String tipoDato) {
		this.tipoDato = tipoDato;
	}

	public String getExcel() {
		return excel;
	}

	public void setExcel(String excel) {
		this.excel = excel;
	}

}
