package com.bolocelta.bbdd.constants.structure;

import java.util.ArrayList;
import java.util.List;

import com.bolocelta.bbdd.constants.NombresTablas;

public class EstructuraJugadores extends NombresTablas{
	
	private List<Estructura> list = new ArrayList<Estructura>();
	
	
	//Filas
	private final Long F1 = new Long(1);
	
	//Columnas
	private final Integer C1 = 1;
	private final Integer C2 = 2;
	private final Integer C3 = 3;
	private final Integer C4 = 4;
	private final Integer C5 = 5;
	private final Integer C6 = 6;
	private final Integer C7 = 7;
	private final Integer C8 = 8;
	private final Integer C9 = 9;
	private final Integer C10 = 10;
	private final Integer C11 = 11;
	private final Integer C12 = 12;
	private final Integer C13 = 13;
	private final Integer C14 = 14;
	
	//Cabeceras
	public static final String COL_ID = "ID";
	public static final String COL_EQUIPO = "EQUIPO";
	public static final String COL_NOMBRE = "NOMBRE";
	public static final String COL_APODO = "APODO";
	public static final String COL_NUM_FICHA = "NUM_FICHA";
	public static final String COL_EMAIL = "EMAIL";
	public static final String COL_TELEFONO = "TELEFONO";
	public static final String COL_EDAD = "EDAD";
	public static final String COL_ACTIVO = "ACTIVO";
	public static final String COL_MODALIDAD = "MODALIDAD";
	public static final String COL_INDIV_PRI = "INDIV. PRI";
	public static final String COL_INDIV_SEG = "INDIV. SEG";
	public static final String COL_INDIV_TER = "INDIV. TER";
	public static final String COL_INDIV_FEM = "INDIV. FEM";
	
	//Propiedades
	
	//Estructura
	public EstructuraJugadores() {
		//Cabecera
		list.add(new Estructura(this.F1, this.C1 , COL_ID, this.N_CSV_BBDD_JUGADORES, this.TD_ENTERO));
		list.add(new Estructura(this.F1, this.C2 , COL_EQUIPO, this.N_CSV_BBDD_JUGADORES, this.TD_ENTERO));
		list.add(new Estructura(this.F1, this.C3 , COL_NOMBRE, this.N_CSV_BBDD_JUGADORES, this.TD_TEXTO));
		list.add(new Estructura(this.F1, this.C4 , COL_APODO, this.N_CSV_BBDD_JUGADORES, this.TD_TEXTO));
		list.add(new Estructura(this.F1, this.C5 , COL_NUM_FICHA, this.N_CSV_BBDD_JUGADORES, this.TD_TEXTO));
		list.add(new Estructura(this.F1, this.C6 , COL_EMAIL, this.N_CSV_BBDD_JUGADORES, this.TD_TEXTO));
		list.add(new Estructura(this.F1, this.C7 , COL_TELEFONO, this.N_CSV_BBDD_JUGADORES, this.TD_TEXTO));
		list.add(new Estructura(this.F1, this.C8 , COL_EDAD, this.N_CSV_BBDD_JUGADORES, this.TD_ENTERO));
		list.add(new Estructura(this.F1, this.C9 , COL_ACTIVO, this.N_CSV_BBDD_JUGADORES, this.TD_TEXTO));
		list.add(new Estructura(this.F1, this.C10 , COL_MODALIDAD, this.N_CSV_BBDD_JUGADORES, this.TD_TEXTO));
		list.add(new Estructura(this.F1, this.C11 , COL_INDIV_PRI, this.N_CSV_BBDD_JUGADORES, this.TD_TEXTO));
		list.add(new Estructura(this.F1, this.C12 , COL_INDIV_SEG, this.N_CSV_BBDD_JUGADORES, this.TD_TEXTO));
		list.add(new Estructura(this.F1, this.C13 , COL_INDIV_TER, this.N_CSV_BBDD_JUGADORES, this.TD_TEXTO));
		list.add(new Estructura(this.F1, this.C14 , COL_INDIV_FEM, this.N_CSV_BBDD_JUGADORES, this.TD_TEXTO));
	}
	
	public List<Estructura> getList(){
		return this.list;
	}
	

}
