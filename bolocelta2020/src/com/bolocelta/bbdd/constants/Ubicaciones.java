package com.bolocelta.bbdd.constants;

public class Ubicaciones {
	
	public static final String UBICACION_BBDD_DEFAULT_WIN = "c:/Develop/Projects/BoloCelta/bbdd/2020/";
	public static final String UBICACION_BBDD_DEFAULT_LINUX = "/opt/bolocelta/bbdd/2020/";
	
	public static final String UBICACION_BBDD_ADMINISTRACION = getUbicacionBBDD() + "ADMINISTRACION/";
	public static final String UBICACION_BBDD_HISTORICO = getUbicacionBBDD() + "HISTORIA/";
	public static final String UBICACION_BBDD_CAMPEONATO_EQUIPOS = getUbicacionBBDD() + "CAMPEONATO_EQUIPOS/";
	public static final String UBICACION_BBDD_CAMPEONATO_EQUIPOS_FOTOS_PRIMERA = getUbicacionBBDD() + "CAMPEONATO_EQUIPOS/FOTOS/PRIMERA/";
	public static final String UBICACION_BBDD_CAMPEONATO_EQUIPOS_FOTOS_SEGUNDA = getUbicacionBBDD() + "CAMPEONATO_EQUIPOS/FOTOS/SEGUNDA/";
	public static final String UBICACION_BBDD_CAMPEONATO_EQUIPOS_FOTOS_TERCERA = getUbicacionBBDD() + "CAMPEONATO_EQUIPOS/FOTOS/TERCERA/";
	public static final String UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_PRIMERA = getUbicacionBBDD() + "CAMPEONATO_INDIVIDUAL/PRIMERA/";
	public static final String UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA = getUbicacionBBDD() + "CAMPEONATO_INDIVIDUAL/SEGUNDA/";
	public static final String UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_TERCERA = getUbicacionBBDD() + "CAMPEONATO_INDIVIDUAL/TERCERA/";
	public static final String UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA = getUbicacionBBDD() + "CAMPEONATO_PAREJAS/PRIMERA/";
	public static final String UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA = getUbicacionBBDD() + "CAMPEONATO_PAREJAS/SEGUNDA/";
	public static final String UBICACION_BBDD_CAMPEONATO_PAREJAS_TERCERA = getUbicacionBBDD() + "CAMPEONATO_PAREJAS/TERCERA/";
	public static final String UBICACION_BBDD_CAMPEONATO_PAREJAS_FEMENINO = getUbicacionBBDD() + "CAMPEONATO_PAREJAS/FEMENINO/";
	public static final String UBICACION_BBDD_CAMPEONATO_PAREJAS_MIXTO= getUbicacionBBDD() + "CAMPEONATO_PAREJAS/MIXTO/";
	public static final String UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO = getUbicacionBBDD() + "CAMPEONATO_INDIVIDUAL/FEMENINO/";
	public static final String UBICACION_BBDD_USUARIOS = getUbicacionBBDD() + "USUARIOS/";
	public static final String UBICACION_BBDD_DOCUMENTOS = getUbicacionBBDD() + "DOCUMENTOS/";
	public static final String UBICACION_BBDD_NOTICIAS = getUbicacionBBDD() + "NOTICIAS/";
	public static final String UBICACION_BBDD_GALERIA = getUbicacionBBDD() + "GALERIA/";
	
	public static String getUbicacionBBDD(){
		String os = System.getProperty("os.name");
		System.out.println("El sistema operativo es : " + os);
		if(os != null && os.contains("Windows")){
			return UBICACION_BBDD_DEFAULT_WIN;
		}else if(os != null && os.contains("Linux")){
			return UBICACION_BBDD_DEFAULT_LINUX;
		}
		return UBICACION_BBDD_DEFAULT_LINUX;
	}

}
