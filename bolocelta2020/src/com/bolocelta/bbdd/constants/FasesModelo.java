package com.bolocelta.bbdd.constants;

public class FasesModelo {
	
	public static final Integer FASE_I = 1;
	public static final Integer FASE_II = 2;
	public static final Integer FASE_OF = 3;
	public static final Integer FASE_CF = 4;
	public static final Integer FASE_SF = 5;
	public static final Integer FASE_FC = 6;
	public static final Integer FASE_FF = 7;
	
	public static final Integer FASE_GRAPHIC = 99;

}
