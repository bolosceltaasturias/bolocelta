package com.bolocelta.bbdd.constants;

public class Activo {
	
	public static final String SI = "Si";
	public static final String NO = "No";
	
	public static final Integer SI_NUMBER = 1;
	public static final Integer NO_NUMBER = 0;

}
