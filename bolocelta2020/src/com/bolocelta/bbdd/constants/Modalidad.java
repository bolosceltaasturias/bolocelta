package com.bolocelta.bbdd.constants;

public class Modalidad {
	
	public static final String MASCULINO = "MASCULINO";
	public static final String FEMENINO = "FEMENINO";
	
	public static final Integer EQUIPOS_LIGA = 1;

}
