package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.inject.Named;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.bolocelta.bbdd.constants.Activo;
import com.bolocelta.bbdd.constants.NombresTablas;
import com.bolocelta.bbdd.constants.Ubicaciones;
import com.bolocelta.entities.Boleras;
import com.bolocelta.transformer.BolerasTransformer;

@Named
public class LeerBoleras extends ALeer {
	
	HashMap<Integer, Boleras> result = null;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, Boleras> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas.N_CSV_BBDD_BOLERAS, Ubicaciones.UBICACION_BBDD_ADMINISTRACION);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	Boleras bolera = new Boleras();
		    	bolera = BolerasTransformer.transformerObjectCsv(bolera, row);
		    	result.put(bolera.getId(), bolera);
		    }
		}
	    return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Boleras read(Integer id) {
		if(this.result == null){
			result = (HashMap<Integer, Boleras>) readAll();
		}
		return this.result.get(id);
	}
	
	@Override
	public List<?> listResult() {
		List<Boleras> listResult = new ArrayList<Boleras>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    Boleras bolera = (Boleras) value;
		    if(bolera.getId() > 0){
		    	listResult.add(bolera);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultFederadas() {
		List<Boleras> listResult = new ArrayList<Boleras>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    Boleras bolera = (Boleras) value;
		    if(bolera.getId() > 0){
		    	if(bolera.getFederada().equalsIgnoreCase(Activo.SI)){
		    		listResult.add(bolera);
		    	}
		    }
		}
		return listResult;
	}

}
