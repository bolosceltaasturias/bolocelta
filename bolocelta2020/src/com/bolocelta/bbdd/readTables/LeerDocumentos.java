package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.inject.Named;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas;
import com.bolocelta.bbdd.constants.Ubicaciones;
import com.bolocelta.entities.Documentos;
import com.bolocelta.transformer.DocumentosTransformer;

@Named
public class LeerDocumentos extends ALeer {
	
	HashMap<Integer, Documentos> result = null;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, Documentos> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas.N_CSV_BBDD_DOCUMENTOS, Ubicaciones.UBICACION_BBDD_DOCUMENTOS);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	Documentos documentos = new Documentos();
		    	documentos = DocumentosTransformer.transformerObjectCsv(documentos, row);
		    	result.put(documentos.getId(), documentos);
		    }
		}
	    return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Documentos read(Integer id) {
		if(this.result == null){
			result = (HashMap<Integer, Documentos>) readAll();
		}
		return this.result.get(id);
	}
	
	@Override
	public List<?> listResult() {
		List<Documentos> listResult = new ArrayList<Documentos>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    Documentos documentos = (Documentos) value;
		    if(documentos.getId() > 0){
		    	listResult.add(documentos);
		    }
		}
		return listResult;
	}

}
