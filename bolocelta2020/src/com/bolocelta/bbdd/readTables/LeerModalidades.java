package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas;
import com.bolocelta.bbdd.constants.Ubicaciones;
import com.bolocelta.entities.Modalidades;
import com.bolocelta.transformer.ModalidadesTransformer;

public class LeerModalidades extends ALeer {
	
	HashMap<Integer, Modalidades> result = null;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, Modalidades> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas.N_CSV_BBDD_MODALIDADES, Ubicaciones.UBICACION_BBDD_ADMINISTRACION);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
	    		Modalidades modalidad = new Modalidades();
	    		modalidad = ModalidadesTransformer.transformerObjectCsv(modalidad, row);
		    	result.put(modalidad.getId(), modalidad);
		    }
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Modalidades read(Integer id) {
		if(this.result == null){
			this.result = (HashMap<Integer, Modalidades>) readAll();
		}
		return result.get(id);
	}

	@Override
	public List<?> listResult() {
		List<Modalidades> listResult = new ArrayList<Modalidades>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    Modalidades modalidad = (Modalidades) value;
		    if(modalidad.getId() > 0){
		    	listResult.add((Modalidades) modalidad);
		    }
		}
		return listResult;
	}

}
