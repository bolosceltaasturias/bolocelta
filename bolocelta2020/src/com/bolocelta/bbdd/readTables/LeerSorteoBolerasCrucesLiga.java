package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas;
import com.bolocelta.bbdd.constants.Ubicaciones;
import com.bolocelta.entities.sorteos.liga.SorteoBolerasCrucesLiga;
import com.bolocelta.transformer.SorteoBolerasCrucesLigaTransformer;

public class LeerSorteoBolerasCrucesLiga extends ALeer {

	private final String nameFile = NombresTablas.N_CSV_BBDD_SORTEO_LIGA_CALENDARIO_BOLERAS_CRUCE;
	private final String path = Ubicaciones.UBICACION_BBDD_ADMINISTRACION;
	
	HashMap<Integer, SorteoBolerasCrucesLiga> result = null;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, SorteoBolerasCrucesLiga> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(nameFile, path);
		if(csvParser != null){
			for (CSVRecord row : csvParser) {
		    	SorteoBolerasCrucesLiga sorteoBolerasCrucesLiga = new SorteoBolerasCrucesLiga();
	    		sorteoBolerasCrucesLiga = SorteoBolerasCrucesLigaTransformer.transformerObjectCsv(sorteoBolerasCrucesLiga, row);
		    	result.put(sorteoBolerasCrucesLiga.getId(), sorteoBolerasCrucesLiga);
		    }
		}
		return result;
	}
	
	public Long getLastRowSheet(){
		return lastRowSheet(nameFile, path);
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public SorteoBolerasCrucesLiga read(Integer id) {
		SorteoBolerasCrucesLiga sorteoBolerasCrucesLiga = null;
		if(sorteoBolerasCrucesLiga == null){
			if(this.result == null){
				this.result = (HashMap<Integer, SorteoBolerasCrucesLiga>) readAll();
			}
			sorteoBolerasCrucesLiga = result.get(id);
		}
		return sorteoBolerasCrucesLiga;
	}
	
	public void refreshList(){
		this.result = null;
	}
	
	@SuppressWarnings("unchecked")
	public List<SorteoBolerasCrucesLiga> readCategoria(Integer id) {
		List<SorteoBolerasCrucesLiga> sorteoBolerasCrucesLigaByCategoria = new ArrayList<>();
		List<SorteoBolerasCrucesLiga> allSorteoBolerasCrucesLiga = (List<SorteoBolerasCrucesLiga>) listResult();
		for (SorteoBolerasCrucesLiga sbcl : allSorteoBolerasCrucesLiga) {
			if(sbcl.getCategoria() != null && sbcl.getCategoria().equals(id)){
				sorteoBolerasCrucesLigaByCategoria.add(sbcl);
			}
		}
		return sorteoBolerasCrucesLigaByCategoria;
	}

	@Override
	public List<?> listResult() {
		List<SorteoBolerasCrucesLiga> listResult = new ArrayList<SorteoBolerasCrucesLiga>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    SorteoBolerasCrucesLiga sorteoBolerasCrucesLiga = (SorteoBolerasCrucesLiga) value;
		    if(sorteoBolerasCrucesLiga.getId() != null && sorteoBolerasCrucesLiga.getId() > 0){
		    	listResult.add((SorteoBolerasCrucesLiga) sorteoBolerasCrucesLiga);
		    }
		}
		return listResult;
	}

}
