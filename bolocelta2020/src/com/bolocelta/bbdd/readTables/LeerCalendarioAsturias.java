package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.Modalidad;
import com.bolocelta.bbdd.constants.NombresTablas;
import com.bolocelta.bbdd.constants.Ubicaciones;
import com.bolocelta.entities.CalendarioAsturias;
import com.bolocelta.entities.CalendarioEspana;
import com.bolocelta.transformer.CalendarioAsturiasTransformer;

public class LeerCalendarioAsturias extends ALeer {

	private LeerCategorias leerCategorias = new LeerCategorias();
	private LeerModalidades leerModalidades = new LeerModalidades();
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, CalendarioAsturias> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas.N_CSV_BBDD_CALENDARIO_AST, Ubicaciones.UBICACION_BBDD_ADMINISTRACION);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
	    		CalendarioAsturias calendarioAsturias = new CalendarioAsturias();
	    		calendarioAsturias = CalendarioAsturiasTransformer.transformerObjectCsv(calendarioAsturias, row);
		    	result.put(calendarioAsturias.getId(), calendarioAsturias);
		    }
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public CalendarioAsturias read(Integer id) {
		HashMap<Integer, CalendarioAsturias> result = (HashMap<Integer, CalendarioAsturias>) readAll();
		return result.get(id);
	}

	@Override
	public List<?> listResult() {
		List<CalendarioAsturias> listResult = new ArrayList<CalendarioAsturias>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    CalendarioAsturias calendarioAsturias = (CalendarioAsturias) value;
		    if(calendarioAsturias.getId() > 0){
		    	if(calendarioAsturias.getCategoriaId() != null){
		    		calendarioAsturias.setCategoria(leerCategorias.read(calendarioAsturias.getCategoriaId()));
		    	}
		    	if(calendarioAsturias.getModalidadId() != null){
		    		calendarioAsturias.setModalidad(leerModalidades.read(calendarioAsturias.getModalidadId()));
		    	}
		    	listResult.add((CalendarioAsturias) calendarioAsturias);
		    }
		}
		
		
		//Ordenar el calendario
		Collections.sort(listResult, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				CalendarioAsturias cec1 = (CalendarioAsturias) o1;
				CalendarioAsturias cec2 = (CalendarioAsturias) o2;
				
				int comp1 = cec1.getFechaDesde().compareTo(cec2.getFechaDesde());
				if (comp1 == 0) {
					int comp2 = cec1.getFechaHasta().compareTo(cec2.getFechaHasta());
					if (comp2 == 0) {
						return cec1.getId().compareTo(cec2.getId());
					}
					return comp2;
				}
				return comp1;
			}
		});
		
		
		return listResult;
	}
	
	public List<?> listResultLiga() {
		List<CalendarioAsturias> listResult = new ArrayList<CalendarioAsturias>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    CalendarioAsturias calendarioAsturias = (CalendarioAsturias) value;
		    if(calendarioAsturias.getId() > 0){
		    	if(calendarioAsturias.getModalidadId() == Modalidad.EQUIPOS_LIGA){
		    		if(calendarioAsturias.getCategoriaId() != null){
			    		calendarioAsturias.setCategoria(leerCategorias.read(calendarioAsturias.getCategoriaId()));
			    	}
			    	if(calendarioAsturias.getModalidadId() != null){
			    		calendarioAsturias.setModalidad(leerModalidades.read(calendarioAsturias.getModalidadId()));
			    	}
			    	listResult.add((CalendarioAsturias) calendarioAsturias);
		    	}
		    }
		}
		return listResult;
	}

}
