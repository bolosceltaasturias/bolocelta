package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas;
import com.bolocelta.bbdd.constants.Ubicaciones;
import com.bolocelta.entities.CalendarioEspana;
import com.bolocelta.entities.CampeonatoEquiposClasificacion;
import com.bolocelta.transformer.CalendarioEspanaTransformer;

public class LeerCalendarioEspana extends ALeer {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, CalendarioEspana> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas.N_CSV_BBDD_CALENDARIO_ESP, Ubicaciones.UBICACION_BBDD_ADMINISTRACION);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
	    		CalendarioEspana calendarioEspana = new CalendarioEspana();
	    		calendarioEspana = CalendarioEspanaTransformer.transformerObjectCsv(calendarioEspana, row);
		    	result.put(calendarioEspana.getId(), calendarioEspana);
		    }
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public CalendarioEspana read(Integer id) {
		HashMap<Integer, CalendarioEspana> result = (HashMap<Integer, CalendarioEspana>) readAll();
		return result.get(id);
	}

	@Override
	public List<?> listResult() {
		List<CalendarioEspana> listResult = new ArrayList<CalendarioEspana>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    CalendarioEspana calendarioEspana = (CalendarioEspana) value;
		    if(calendarioEspana.getId() > 0){
		    	listResult.add((CalendarioEspana) calendarioEspana);
		    }
		}
		
		
		
		//Ordenar el calendario
		Collections.sort(listResult, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				CalendarioEspana cec1 = (CalendarioEspana) o1;
				CalendarioEspana cec2 = (CalendarioEspana) o2;
				
				int comp1 = cec1.getFechaDesde().compareTo(cec2.getFechaDesde());
				if (comp1 == 0) {
					int comp2 = cec1.getFechaHasta().compareTo(cec2.getFechaHasta());
					if (comp2 == 0) {
						return cec1.getId().compareTo(cec2.getId());
					}
					return comp2;
				}
				return comp1;
			}
		});
		
		
		
		
		return listResult;
	}

}
