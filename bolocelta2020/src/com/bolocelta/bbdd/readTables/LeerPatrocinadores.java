package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.inject.Named;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas;
import com.bolocelta.bbdd.constants.Ubicaciones;
import com.bolocelta.entities.Patrocinadores;
import com.bolocelta.transformer.PatrocinadoresTransformer;

@Named
public class LeerPatrocinadores extends ALeer {
	
	HashMap<Integer, Patrocinadores> result = null;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, Patrocinadores> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas.N_CSV_BBDD_PATROCINADORES, Ubicaciones.UBICACION_BBDD_ADMINISTRACION);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	Patrocinadores patrocinador = new Patrocinadores();
		    	patrocinador = PatrocinadoresTransformer.transformerObjectCsv(patrocinador, row);
		    	result.put(patrocinador.getId(), patrocinador);
		    }
		}
	    return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Patrocinadores read(Integer id) {
		if(this.result == null){
			result = (HashMap<Integer, Patrocinadores>) readAll();
		}
		return this.result.get(id);
	}
	
	@Override
	public List<?> listResult() {
		List<Patrocinadores> listResult = new ArrayList<Patrocinadores>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    Patrocinadores patrocinador = (Patrocinadores) value;
		    if(patrocinador.getId() > 0){
		    	listResult.add(patrocinador);
		    }
		}
		return listResult;
	}

}
