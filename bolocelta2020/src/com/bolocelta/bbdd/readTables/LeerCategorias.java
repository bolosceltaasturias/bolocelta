package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.inject.Named;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.application.enumerations.ActivoEnumeration;
import com.bolocelta.bbdd.constants.NombresTablas;
import com.bolocelta.bbdd.constants.Ubicaciones;
import com.bolocelta.entities.Categorias;
import com.bolocelta.transformer.CategoriasTransformer;

@Named
public class LeerCategorias extends ALeer {

	HashMap<Integer, Categorias> result = null;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, Categorias> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas.N_CSV_BBDD_CATEGORIAS, Ubicaciones.UBICACION_BBDD_ADMINISTRACION);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
	    		Categorias categoria = new Categorias();
	    		categoria = CategoriasTransformer.transformerObjectCsv(categoria, row);
		    	result.put(categoria.getId(), categoria);
		    }
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Categorias read(Integer id) {
		if(this.result == null){
			this.result = (HashMap<Integer, Categorias>) readAll();
		}
		return result.get(id);
	}

	@Override
	public List<?> listResult() {
		List<Categorias> listResult = new ArrayList<Categorias>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    listResult.add((Categorias) value);
		}
		return listResult;
	}
	
	public List<?> listResultIndividual() {
		List<Categorias> listResult = new ArrayList<Categorias>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    Categorias categoria = (Categorias) value;
		    if(categoria.getIndividual().equalsIgnoreCase(ActivoEnumeration.SI)){
		    	listResult.add((Categorias) value);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultParejas() {
		List<Categorias> listResult = new ArrayList<Categorias>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    Categorias categoria = (Categorias) value;
		    if(categoria.getParejas().equalsIgnoreCase(ActivoEnumeration.SI)){
		    	listResult.add((Categorias) value);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultLiga() {
		List<Categorias> listResult = new ArrayList<Categorias>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    Categorias categoria = (Categorias) value;
//		    System.out.println(categoria.getId());
		    if(categoria.getLiga().equalsIgnoreCase(ActivoEnumeration.SI)){
		    	listResult.add((Categorias) value);
		    }
		}
		return listResult;
	}

}
