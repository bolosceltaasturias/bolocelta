package com.bolocelta.bbdd.readTables.usuarios;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.inject.Named;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas;
import com.bolocelta.bbdd.constants.Ubicaciones;
import com.bolocelta.bbdd.readTables.ALeer;
import com.bolocelta.entities.usuarios.Usuarios;
import com.bolocelta.transformer.usuarios.UsuariosTransformer;

@Named
public class LeerUsuarios extends ALeer {

	private LeerRoles leerRoles = new LeerRoles();

	HashMap<Integer, Usuarios> result = null;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, Usuarios> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas.N_CSV_BBDD_USUARIOS, Ubicaciones.UBICACION_BBDD_USUARIOS);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	Usuarios usuario = new Usuarios();
		    	usuario = UsuariosTransformer.transformerObjectCsv(usuario, row);
		    	result.put(usuario.getId(), usuario);
		    }
		}
	    return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Usuarios read(Integer id) {
		if (this.result == null) {
			result = (HashMap<Integer, Usuarios>) readAll();
		}
		return this.result.get(id);
	}

	@SuppressWarnings("unchecked")
	public Usuarios read(String user, String pass) {
		if (user != null && pass != null) {
			for (Object object : listResult()) {
				Usuarios usuario = (Usuarios) object;
				if (usuario.getUser().equals(user) && usuario.getPass().equals(pass)) {
					return usuario;
				}
			}
		}
		return null;
	}

	@Override
	public List<?> listResult() {
		List<Usuarios> listResult = new ArrayList<Usuarios>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
			Object value = entry.getValue();
			Usuarios usuario = (Usuarios) value;
			if (usuario.getId() > 0) {
				if (usuario.getRolId() != null) {
					usuario.setRol(leerRoles.read(usuario.getRolId()));
				}
				listResult.add(usuario);
			}
		}
		return listResult;
	}

}
