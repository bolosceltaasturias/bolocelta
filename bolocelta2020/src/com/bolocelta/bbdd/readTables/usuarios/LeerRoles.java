package com.bolocelta.bbdd.readTables.usuarios;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.inject.Named;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas;
import com.bolocelta.bbdd.constants.Ubicaciones;
import com.bolocelta.bbdd.readTables.ALeer;
import com.bolocelta.entities.usuarios.Roles;
import com.bolocelta.transformer.usuarios.RolesTransformer;

@Named
public class LeerRoles extends ALeer {
	
	HashMap<Integer, Roles> result = null;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, Roles> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas.N_CSV_BBDD_ROLES, Ubicaciones.UBICACION_BBDD_USUARIOS);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	Roles rol = new Roles();
		    	rol = RolesTransformer.transformerObjectCsv(rol, row);
		    	result.put(rol.getId(), rol);
		    }
		}
	    return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Roles read(Integer id) {
		if(this.result == null){
			result = (HashMap<Integer, Roles>) readAll();
		}
		return this.result.get(id);
	}
	
	@Override
	public List<?> listResult() {
		List<Roles> listResult = new ArrayList<Roles>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    Roles rol = (Roles) value;
		    if(rol.getId() > 0){
		    	listResult.add(rol);
		    }
		}
		return listResult;
	}

}
