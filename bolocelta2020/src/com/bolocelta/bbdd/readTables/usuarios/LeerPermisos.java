package com.bolocelta.bbdd.readTables.usuarios;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.inject.Named;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas;
import com.bolocelta.bbdd.constants.Ubicaciones;
import com.bolocelta.bbdd.readTables.ALeer;
import com.bolocelta.entities.usuarios.Permisos;
import com.bolocelta.transformer.usuarios.PermisosTransformer;

@Named
public class LeerPermisos extends ALeer {

	private LeerRoles leerRoles = new LeerRoles();

	HashMap<Integer, Permisos> result = null;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, Permisos> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas.N_CSV_BBDD_PERMISOS, Ubicaciones.UBICACION_BBDD_USUARIOS);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	Permisos permiso = new Permisos();
		    	permiso = PermisosTransformer.transformerObjectCsv(permiso, row);
		    	result.put(permiso.getId(), permiso);
		    }
		}
	    return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Permisos read(Integer id) {
		if (this.result == null) {
			result = (HashMap<Integer, Permisos>) readAll();
		}
		return this.result.get(id);
	}

	@Override
	public List<?> listResult() {
		List<Permisos> listResult = new ArrayList<Permisos>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
			Object value = entry.getValue();
			Permisos permiso = (Permisos) value;
			if (permiso.getId() > 0) {
				if (permiso.getRolId() != null) {
					permiso.setRol(leerRoles.read(permiso.getRolId()));
				}
				listResult.add(permiso);
			}
		}
		return listResult;
	}
	
	public List<?> listResultByRol(Integer rolId) {
		List<Permisos> listResult = new ArrayList<Permisos>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
			Object value = entry.getValue();
			Permisos permiso = (Permisos) value;
			if(permiso != null && permiso.getId() != null){
				if (permiso.getId() > 0) {
					if(permiso.getRolId()== rolId){
						if (permiso.getRolId() != null) {
							permiso.setRol(leerRoles.read(permiso.getRolId()));
						}
						listResult.add(permiso);
					}
				}
			}
		}
		return listResult;
	}

}
