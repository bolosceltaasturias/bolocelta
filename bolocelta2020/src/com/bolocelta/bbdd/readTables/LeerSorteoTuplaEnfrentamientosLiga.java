package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas;
import com.bolocelta.bbdd.constants.Ubicaciones;
import com.bolocelta.entities.sorteos.liga.SorteoTuplaEnfrentamientosLiga;
import com.bolocelta.transformer.SorteoTuplaEnfrentamientosLigaTransformer;

public class LeerSorteoTuplaEnfrentamientosLiga extends ALeer {

	private final String nameFile = NombresTablas.N_CSV_BBDD_SORTEO_LIGA_CALENDARIO_CRUCES;
	private final String path = Ubicaciones.UBICACION_BBDD_ADMINISTRACION;
	
	HashMap<Integer, SorteoTuplaEnfrentamientosLiga> result = null;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, SorteoTuplaEnfrentamientosLiga> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(nameFile, path);
		if(csvParser != null){    
			for (CSVRecord row : csvParser) {
		    	SorteoTuplaEnfrentamientosLiga sorteoTuplaEnfrentamientosLiga = new SorteoTuplaEnfrentamientosLiga();
	    		sorteoTuplaEnfrentamientosLiga = SorteoTuplaEnfrentamientosLigaTransformer.transformerObjectCsv(sorteoTuplaEnfrentamientosLiga, row);
		    	result.put(sorteoTuplaEnfrentamientosLiga.getId(), sorteoTuplaEnfrentamientosLiga);
		    }
		}
		return result;
	}
	
	public Long getLastRowSheet(){
		return lastRowSheet(nameFile, path);
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public SorteoTuplaEnfrentamientosLiga read(Integer id) {
		SorteoTuplaEnfrentamientosLiga sorteoTuplaEnfrentamientosLiga = null;
		if(sorteoTuplaEnfrentamientosLiga == null){
			if(this.result == null){
				this.result = (HashMap<Integer, SorteoTuplaEnfrentamientosLiga>) readAll();
			}
			sorteoTuplaEnfrentamientosLiga = result.get(id);
		}
		return sorteoTuplaEnfrentamientosLiga;
	}
	
	public void refreshList(){
		this.result = null;
	}
	
	@SuppressWarnings("unchecked")
	public List<SorteoTuplaEnfrentamientosLiga> readJornadaIda(Integer id) {
		List<SorteoTuplaEnfrentamientosLiga> sorteoTuplaEnfrentamientosLigaByCategoria = new ArrayList<>();
		List<SorteoTuplaEnfrentamientosLiga> allSorteoTuplaEnfrentamientosLiga = (List<SorteoTuplaEnfrentamientosLiga>) listResult();
		for (SorteoTuplaEnfrentamientosLiga stel : allSorteoTuplaEnfrentamientosLiga) {
			if(stel.getJornadaIda() != null && stel.getJornadaIda().equals(id)){
				sorteoTuplaEnfrentamientosLigaByCategoria.add(stel);
			}
		}
		return sorteoTuplaEnfrentamientosLigaByCategoria;
	}

	@Override
	public List<?> listResult() {
		List<SorteoTuplaEnfrentamientosLiga> listResult = new ArrayList<SorteoTuplaEnfrentamientosLiga>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    SorteoTuplaEnfrentamientosLiga sorteoTuplaEnfrentamientosLiga = (SorteoTuplaEnfrentamientosLiga) value;
		    if(sorteoTuplaEnfrentamientosLiga.getId() != null && sorteoTuplaEnfrentamientosLiga.getId() > 0){
		    	listResult.add((SorteoTuplaEnfrentamientosLiga) sorteoTuplaEnfrentamientosLiga);
		    }
		}
		return listResult;
	}

}
