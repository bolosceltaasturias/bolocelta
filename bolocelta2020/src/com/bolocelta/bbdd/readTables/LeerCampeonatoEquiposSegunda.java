package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas;
import com.bolocelta.bbdd.constants.Ubicaciones;
import com.bolocelta.entities.CampeonatoEquiposCalendario;
import com.bolocelta.entities.CampeonatoEquiposClasificacion;
import com.bolocelta.transformer.CampeonatoEquiposTransformer;

public class LeerCampeonatoEquiposSegunda extends ALeer {
	
	private LeerEquipos leerEquipos = new LeerEquipos();
	private LeerCategorias leerCategorias = new LeerCategorias();
	private LeerBoleras leerBoleras = new LeerBoleras();

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<Integer, ?> readAllClasificacion() {
		HashMap<Integer, CampeonatoEquiposClasificacion> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CLA_SEGUNDA, Ubicaciones.UBICACION_BBDD_CAMPEONATO_EQUIPOS);
		if(csvParser != null){
			for (CSVRecord row : csvParser) {
		    	CampeonatoEquiposClasificacion campeonatoEquiposClasificacion = new CampeonatoEquiposClasificacion();
		    	campeonatoEquiposClasificacion.setRowNum(row.getRecordNumber());
		    	campeonatoEquiposClasificacion = CampeonatoEquiposTransformer.transformerObjectClasificacionCsv(campeonatoEquiposClasificacion, row);
		    	result.put(campeonatoEquiposClasificacion.getId(), campeonatoEquiposClasificacion);
		    }
		}
		return result;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<Integer, ?> readAllCalendario() {
		HashMap<Integer, CampeonatoEquiposCalendario> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CAL_SEGUNDA, Ubicaciones.UBICACION_BBDD_CAMPEONATO_EQUIPOS);
		if(csvParser != null){
			for (CSVRecord row : csvParser) {
		    	CampeonatoEquiposCalendario campeonatoEquiposCalendario = new CampeonatoEquiposCalendario();
		    	campeonatoEquiposCalendario.setRowNum(row.getRecordNumber());
		    	campeonatoEquiposCalendario = CampeonatoEquiposTransformer.transformerObjectCalendarioCsv(campeonatoEquiposCalendario, row);
		    	result.put(campeonatoEquiposCalendario.getId(), campeonatoEquiposCalendario);
		    }
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public CampeonatoEquiposClasificacion readClasificacion(Integer id) {
		HashMap<Integer, CampeonatoEquiposClasificacion> result = (HashMap<Integer, CampeonatoEquiposClasificacion>) readAllClasificacion();
		return result.get(id);
	}
	
	@SuppressWarnings("unchecked")
	public CampeonatoEquiposCalendario readCalendario(Integer id) {
		HashMap<Integer, CampeonatoEquiposCalendario> result = (HashMap<Integer, CampeonatoEquiposCalendario>) readAllCalendario();
		return result.get(id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object read(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HashMap<Integer, ?> readAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<?> listResult() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public List<?> listResultClasificacion() {
		List<CampeonatoEquiposClasificacion> listResult = new ArrayList<CampeonatoEquiposClasificacion>();
		for (Entry<Integer, ?> entry : readAllClasificacion().entrySet()) {
		    Object value = entry.getValue();
		    CampeonatoEquiposClasificacion cec = (CampeonatoEquiposClasificacion) value;
		    if(cec.getId() > 0){
		    	if(cec.getEquipoId() != null){
		    		cec.setEquipo(leerEquipos.read(cec.getEquipoId()));
		    	}
		    	if(cec.getCategoriaId() != null){
		    		cec.setCategoria(leerCategorias.read(cec.getCategoriaId()));
		    	}
		    	listResult.add((CampeonatoEquiposClasificacion) cec);
		    }
		}
		
		//Ordenar la clasificacion
		Collections.sort(listResult, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				CampeonatoEquiposClasificacion cec1 = (CampeonatoEquiposClasificacion) o1;
				CampeonatoEquiposClasificacion cec2 = (CampeonatoEquiposClasificacion) o2;
				
				int rpuntos = cec1.getPuntos().compareTo(cec2.getPuntos());
				if (rpuntos == 0) {
					Integer part1 = cec1.getPartidasFavor() - cec1.getPartidasContra();
					Integer part2 = cec2.getPartidasFavor() - cec2.getPartidasContra();
					int rdifpartidas = part1.compareTo(part2);
					if (rdifpartidas == 0) {
						return cec1.getPartidasFavor().compareTo(cec2.getPartidasFavor());
					}
					return rdifpartidas;
				}
				return rpuntos;
			}
		});
		
		Collections.reverse(listResult);
		
		return listResult;
	}
	
	public List<?> listResultCalendario() {
		List<CampeonatoEquiposCalendario> listResult = new ArrayList<CampeonatoEquiposCalendario>();
		for (Entry<Integer, ?> entry : readAllCalendario().entrySet()) {
		    Object value = entry.getValue();
		    CampeonatoEquiposCalendario cec = (CampeonatoEquiposCalendario) value;
		    if(cec.getId() > 0){
		    	if(cec.getEquipo1Id() != null){
		    		cec.setEquipo1(leerEquipos.read(cec.getEquipo1Id()));
		    		if(cec.getEquipo1().getBoleraId() != null){
		    			cec.getEquipo1().setBolera(leerBoleras.read(cec.getEquipo1().getBoleraId()));
			    	}
		    	}
		    	if(cec.getEquipo2Id() != null){
		    		cec.setEquipo2(leerEquipos.read(cec.getEquipo2Id()));
		    		if(cec.getEquipo2().getBoleraId() != null){
		    			cec.getEquipo2().setBolera(leerBoleras.read(cec.getEquipo2().getBoleraId()));
			    	}
		    	}
		    	if(cec.getCategoriaId() != null){
		    		cec.setCategoria(leerCategorias.read(cec.getCategoriaId()));
		    	}
		    	if(cec.getPgequipo1() != null && cec.getPgequipo2() != null){
		    		if(cec.getPgequipo1() == 0 && cec.getPgequipo2() == 0){
		    			cec.setRequipo1("/resources/sinjugar.ico");
		    			cec.setRequipo2("/resources/sinjugar.ico");
		    		}else if(cec.getPgequipo1() > cec.getPgequipo2()){
		    			cec.setRequipo1("/resources/ganador.ico");
		    			cec.setRequipo2("/resources/derrota.ico");
		    		}else if(cec.getPgequipo1() < cec.getPgequipo2()){
		    			cec.setRequipo1("/resources/derrota.ico");
		    			cec.setRequipo2("/resources/ganador.ico");
		    		}else if(cec.getPgequipo1().equals(cec.getPgequipo2())){
		    			cec.setRequipo1("/resources/empate.ico");
		    			cec.setRequipo2("/resources/empate.ico");
		    		}
		    		
		    	}else{
		    		cec.setRequipo1("/resources/sinjugar.ico");
	    			cec.setRequipo2("/resources/sinjugar.ico");
		    	}
		    	listResult.add((CampeonatoEquiposCalendario) cec);
		    }
		}
		
		//Ordenar la clasificacion
		Collections.sort(listResult, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				CampeonatoEquiposCalendario cec1 = (CampeonatoEquiposCalendario) o1;
				CampeonatoEquiposCalendario cec2 = (CampeonatoEquiposCalendario) o2;
				
				int rjornada = cec1.getJornada().compareTo(cec2.getJornada());
				return rjornada;
			}
		});
		
		return listResult;
	}

}
