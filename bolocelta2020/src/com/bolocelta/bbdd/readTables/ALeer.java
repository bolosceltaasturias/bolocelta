package com.bolocelta.bbdd.readTables;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public abstract class ALeer implements ILeer {
	
	public Iterator<Row> readWorkBook(String nameFile, String path, String nameSheet) {

		try (FileInputStream file = new FileInputStream(new File(path + nameFile))) {
			// Leer archivo excel
			@SuppressWarnings("resource")
			XSSFWorkbook workbook = new XSSFWorkbook(file);
			// Obtener la hoja que se va leer
			XSSFSheet sheet = workbook.getSheet(nameSheet);
			// Obtener todas las filas de la hoja excel
			Iterator<Row> rowIterator = sheet.iterator();

			return rowIterator;

		} catch (Exception e) {
			System.out.println("ERROR. El libro " + nameFile + "/" + nameSheet + "no existe.");
		}

		return null;
	}
	
	public CSVParser readWorkBookCsv(String nameFile, String path) {
		
        try {
        	//System.out.println("El path es : " + path+nameFile);
        	//FileReader reader = new FileReader(path + nameFile);
        	//CSVFormat fmt = CSVFormat.EXCEL.withDelimiter(';');
            //CSVParser csvParser = new CSVParser(reader, fmt.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());
        	
        	File inputF = new File(path+nameFile);
            InputStream inputFS = new FileInputStream(inputF);
        	BufferedReader csvReader = new BufferedReader(new InputStreamReader(inputFS, "UTF-8"));
        	CSVFormat fmt = CSVFormat.EXCEL.withDelimiter(';');
        	CSVParser csvParser = new CSVParser(csvReader, fmt.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());
        	
            return csvParser;
		} catch (Exception e) {
			System.out.println("ERROR. El libro " + nameFile + " no existe.");
		}

		return null;
	}
	
	public List<Cell> convertRowToObject(Row row){
		//Se obtiene las celdas por fila
		Iterator<Cell> cellIterator = row.cellIterator();
		List<Cell> cellList = new ArrayList<>();
		//Se recorre cada celda
		while (cellIterator.hasNext()) {
			// Se obtiene la celda en específico y se la imprime
			cellList.add(cellIterator.next());
		}
		return cellList;
	}
	
	public Long lastRowSheet(String nameFile, String path){
		
        try {
        	FileReader reader = new FileReader(path + nameFile);
        	CSVFormat fmt = CSVFormat.EXCEL.withDelimiter(';');
            CSVParser csvParser = new CSVParser(reader, fmt.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());
            return (long) csvParser.getRecords().size();
		} catch (Exception e) {
			System.out.println("ERROR. El libro " + nameFile + "no existe.");
		}

		return null;		
	}
	

}
