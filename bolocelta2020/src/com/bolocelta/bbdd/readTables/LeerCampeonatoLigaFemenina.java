package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas;
import com.bolocelta.bbdd.constants.Ubicaciones;
import com.bolocelta.entities.CampeonatoLigaFemeninaCalendario;
import com.bolocelta.entities.CampeonatoLigaFemeninaClasificacion;
import com.bolocelta.transformer.CampeonatoLigaFemeninaTransformer;

public class LeerCampeonatoLigaFemenina extends ALeer {

	private LeerJugadores leerJugadores = new LeerJugadores();
	private LeerBoleras leerBoleras = new LeerBoleras();
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<Integer, ?> readAllClasificacion() {
		HashMap<Integer, CampeonatoLigaFemeninaClasificacion> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas.N_CSV_BBDD_CAMPEONATO_CLA_LIGA_FEMENINA, Ubicaciones.UBICACION_BBDD_CAMPEONATO_EQUIPOS);
		if(csvParser != null){
			for (CSVRecord row : csvParser) {
		    	CampeonatoLigaFemeninaClasificacion campeonatoLigaFemeninaClasificacion = new CampeonatoLigaFemeninaClasificacion();
		    	campeonatoLigaFemeninaClasificacion.setRowNum(row.getRecordNumber());
		    	campeonatoLigaFemeninaClasificacion = CampeonatoLigaFemeninaTransformer.transformerObjectClasificacionCsv(campeonatoLigaFemeninaClasificacion, row);
		    	result.put(campeonatoLigaFemeninaClasificacion.getId(), campeonatoLigaFemeninaClasificacion);
		    }
		}
		return result;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<Integer, ?> readAllCalendario() {
		HashMap<Integer, CampeonatoLigaFemeninaCalendario> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas.N_CSV_BBDD_CAMPEONATO_CAL_LIGA_FEMENINA, Ubicaciones.UBICACION_BBDD_CAMPEONATO_EQUIPOS);
		if(csvParser != null){
			for (CSVRecord row : csvParser) {
				CampeonatoLigaFemeninaCalendario campeonatoLigaFemeninaCalendario = new CampeonatoLigaFemeninaCalendario();
		    	campeonatoLigaFemeninaCalendario.setRowNum(row.getRecordNumber());
		    	campeonatoLigaFemeninaCalendario = CampeonatoLigaFemeninaTransformer.transformerObjectCalendarioCsv(campeonatoLigaFemeninaCalendario, row);
		    	result.put(campeonatoLigaFemeninaCalendario.getId(), campeonatoLigaFemeninaCalendario);
		    }
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public CampeonatoLigaFemeninaClasificacion readClasificacion(Integer id) {
		HashMap<Integer, CampeonatoLigaFemeninaClasificacion> result = (HashMap<Integer, CampeonatoLigaFemeninaClasificacion>) readAllClasificacion();
		return result.get(id);
	}
	
	@SuppressWarnings("unchecked")
	public CampeonatoLigaFemeninaCalendario readCalendario(Integer id) {
		HashMap<Integer, CampeonatoLigaFemeninaCalendario> result = (HashMap<Integer, CampeonatoLigaFemeninaCalendario>) readAllCalendario();
		return result.get(id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object read(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HashMap<Integer, ?> readAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<?> listResult() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public List<?> listResultClasificacion() {
		List<CampeonatoLigaFemeninaClasificacion> listResult = new ArrayList<CampeonatoLigaFemeninaClasificacion>();
		for (Entry<Integer, ?> entry : readAllClasificacion().entrySet()) {
		    Object value = entry.getValue();
		    CampeonatoLigaFemeninaClasificacion cec = (CampeonatoLigaFemeninaClasificacion) value;
		    if(cec.getId() > 0){
		    	if(cec.getJugadoraId() != null){
		    		cec.setJugadora(leerJugadores.read(cec.getJugadoraId()));
		    	}
		    	listResult.add((CampeonatoLigaFemeninaClasificacion) cec);
		    }
		}
		
		//Ordenar la clasificacion
		Collections.sort(listResult, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				CampeonatoLigaFemeninaClasificacion cec1 = (CampeonatoLigaFemeninaClasificacion) o1;
				CampeonatoLigaFemeninaClasificacion cec2 = (CampeonatoLigaFemeninaClasificacion) o2;
				
				int rpuntos = cec1.getPuntos().compareTo(cec2.getPuntos());
				if (rpuntos == 0) {
					Integer part1 = cec1.getTotalTantos();
					Integer part2 = cec2.getTotalTantos();
					int rdifpartidas = part1.compareTo(part2);
					if (rdifpartidas == 0) {
						String nomb1 = cec1.getJugadora().getNombre();
						String nomb2 = cec2.getJugadora().getNombre();
						int rdifnomb = nomb1.compareTo(nomb2);
						return rdifnomb;
					}
					if(rdifpartidas == -1) return 1;
					if(rdifpartidas == 1) return -1;
					return rdifpartidas;
				}
				return rpuntos;
			}
		});
		
		
		
		return listResult;
	}
	
	public List<?> listResultCalendario() {
		List<CampeonatoLigaFemeninaCalendario> listResult = new ArrayList<CampeonatoLigaFemeninaCalendario>();
		for (Entry<Integer, ?> entry : readAllCalendario().entrySet()) {
		    Object value = entry.getValue();
		    CampeonatoLigaFemeninaCalendario cec = (CampeonatoLigaFemeninaCalendario) value;
		    if(cec.getId() > 0){
		    	if(cec.getJugadoraId() != null){
		    		cec.setJugadora(leerJugadores.read(cec.getJugadoraId()));
		    		if(cec.getBoleraId() != null){
		    			cec.setBolera(leerBoleras.read(cec.getBoleraId()));
			    	}
		    	}
		    	listResult.add((CampeonatoLigaFemeninaCalendario) cec);
		    }
		}
		
		//Ordenar la clasificacion
		Collections.sort(listResult, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				CampeonatoLigaFemeninaCalendario cec1 = (CampeonatoLigaFemeninaCalendario) o1;
				CampeonatoLigaFemeninaCalendario cec2 = (CampeonatoLigaFemeninaCalendario) o2;
				
				int rjornada = cec1.getJornada().compareTo(cec2.getJornada());
				if (rjornada == 0) {
					Integer punt1 = cec1.getPuntos();
					Integer punt2 = cec2.getPuntos();
					int puntDif = punt1.compareTo(punt2);
					if (puntDif == 0) {
						String nomb1 = cec1.getJugadora().getNombre();
						String nomb2 = cec2.getJugadora().getNombre();
						int rdifnomb = nomb1.compareTo(nomb2);
						return rdifnomb;
					}
					return puntDif;
				}
				return rjornada;
			}
		});
		
		return listResult;
	}

}
