package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas;
import com.bolocelta.bbdd.constants.Ubicaciones;
import com.bolocelta.entities.Jugadores;
import com.bolocelta.transformer.JugadoresTransformer;

public class LeerJugadores extends ALeer {

	private final String nameFile = NombresTablas.N_CSV_BBDD_JUGADORES;
	private final String path = Ubicaciones.UBICACION_BBDD_ADMINISTRACION;
	
	HashMap<Integer, Jugadores> result = null;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, Jugadores> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(nameFile, path);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
	    		Jugadores jugador = new Jugadores();
	    		jugador.setRowNum(row.getRecordNumber());
	    		jugador = JugadoresTransformer.transformerObjectCsv(jugador, row);
		    	result.put(jugador.getId(), jugador);
		    }
		}
		return result;
	}
	
	public Long getLastRowSheet(){
		return lastRowSheet(nameFile, path);
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public Jugadores read(Integer id) {
		Jugadores jugador = null;
		if(jugador == null){
			if(this.result == null){
				this.result = (HashMap<Integer, Jugadores>) readAll();
			}
			jugador = result.get(id);
			if(jugador != null && jugador.getId() != null && jugador.getId() > 0){
//		    	if(jugador.getEquipoId() != null){
//		    		if(leerEquipos != null){
//		    			jugador.setEquipo(leerEquipos.read(jugador.getEquipoId()));
//		    		}
//		    	}
		    }
		}
		return jugador;
	}
	
	@SuppressWarnings("unchecked")
	public Jugadores read(Integer id, boolean readTeam) {
		Jugadores jugador = null;
		if(jugador == null){
			if(this.result == null){
				this.result = (HashMap<Integer, Jugadores>) readAll();
			}
			jugador = result.get(id);
			if(jugador != null && jugador.getId() != null && jugador.getId() > 0){
				if(readTeam){
			    	if(jugador.getEquipoId() != null){
			    		LeerEquipos leerEquipos = new LeerEquipos();
			    		if(leerEquipos != null){
			    			jugador.setEquipo(leerEquipos.read(jugador.getEquipoId()));
			    		}
			    	}
				}
		    }
		}
		return jugador;
	}
	
	public void refreshList(){
		this.result = null;
	}
	
	@SuppressWarnings("unchecked")
	public List<Jugadores> readEquipo(Integer id) {
		List<Jugadores> jugadoresEquipo = new ArrayList<>();
		List<Jugadores> allJugadores = (List<Jugadores>) listResult();
		for (Jugadores jugador : allJugadores) {
			if(jugador.getEquipoId() != null && jugador.getEquipoId().equals(id)){
				LeerEquipos leerEquipos = new LeerEquipos();
	    		if(leerEquipos != null){
	    			jugador.setEquipo(leerEquipos.read(jugador.getEquipoId()));
	    		}
	    		jugadoresEquipo.add(jugador);
			}
		}
		return jugadoresEquipo;
	}

	@Override
	public List<?> listResult() {
		List<Jugadores> listResult = new ArrayList<Jugadores>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    Jugadores jugador = (Jugadores) value;
		    if(jugador.getId() != null && jugador.getId() > 0){
//		    	if(jugador.getEquipoId() != null){
//		    		if(leerEquipos != null){
//		    			jugador.setEquipo(leerEquipos.read(jugador.getEquipoId()));
//		    		}
//		    	}
		    	listResult.add((Jugadores) jugador);
		    }
		}
		return listResult;
	}

}
