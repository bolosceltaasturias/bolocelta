package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.inject.Named;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas;
import com.bolocelta.bbdd.constants.Ubicaciones;
import com.bolocelta.entities.Noticias;
import com.bolocelta.transformer.NoticiasTransformer;

@Named
public class LeerNoticias extends ALeer {
	
	HashMap<Integer, Noticias> result = null;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, Noticias> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas.N_CSV_BBDD_NOTICIAS, Ubicaciones.UBICACION_BBDD_NOTICIAS);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	Noticias noticias = new Noticias();
		    	noticias = NoticiasTransformer.transformerObjectCsv(noticias, row);
		    	result.put(noticias.getId(), noticias);
		    }
		}
	    return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Noticias read(Integer id) {
		if(this.result == null){
			result = (HashMap<Integer, Noticias>) readAll();
		}
		return this.result.get(id);
	}
	
	@Override
	public List<?> listResult() {
		List<Noticias> listResult = new ArrayList<Noticias>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    Noticias noticias = (Noticias) value;
		    if(noticias.getId() > 0){
		    	listResult.add(noticias);
		    }
		}
		return listResult;
	}

}
