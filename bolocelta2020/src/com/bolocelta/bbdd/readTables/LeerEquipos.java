package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.bolocelta.bbdd.constants.Activo;
import com.bolocelta.bbdd.constants.NombresTablas;
import com.bolocelta.bbdd.constants.Ubicaciones;
import com.bolocelta.entities.Boleras;
import com.bolocelta.entities.Equipos;
import com.bolocelta.entities.Jugadores;
import com.bolocelta.transformer.BolerasTransformer;
import com.bolocelta.transformer.EquiposTransformer;

public class LeerEquipos extends ALeer {
	
	private LeerCategorias leerCategorias = new LeerCategorias();
	private LeerBoleras leerBoleras = new LeerBoleras();
	private LeerJugadores leerJugadores = new LeerJugadores();
	
	HashMap<Integer, Equipos> result = null;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, Equipos> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas.N_CSV_BBDD_EQUIPOS, Ubicaciones.UBICACION_BBDD_ADMINISTRACION);
		if(csvParser != null){    
			for (CSVRecord row : csvParser) {
	    		Equipos equipo = new Equipos();
	    		equipo.setRowNum(row.getRecordNumber());
	    		equipo = EquiposTransformer.transformerObjectCsv(equipo, row);
		    	result.put(equipo.getId(), equipo);
		    }
		}
	    return result;
	}
	
	public Object read(Integer id, boolean readAditional){
		Equipos equipo = null;
		if(equipo == null){
			if(this.result == null){
				this.result = (HashMap<Integer, Equipos>) readAll();
			}
			equipo = result.get(id);
			if(equipo != null && equipo.getId() > 0){
		    	if(equipo.getCategoriaId() != null){
		    		equipo.setCategoria(leerCategorias.read(equipo.getCategoriaId()));
		    	}
		    	if(equipo.getBoleraId() != null){
		    		equipo.setBolera(leerBoleras.read(equipo.getBoleraId()));
		    	}
		    	if(readAditional){
		    		equipo.setJugadoresList(leerJugadores.readEquipo(id));
		    		equipo.setJugadoresMap(new HashMap<>());
		    		for (Jugadores jugador : equipo.getJugadoresList()) {
						equipo.getJugadoresMap().put(jugador.getId(), jugador);
					}
		    	}
		    	
		    }
		}
		return equipo;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Equipos read(Integer id) {
		
		boolean readAditional = false;
		
		Equipos equipo = null;
		if(equipo == null){
			if(this.result == null){
				this.result = (HashMap<Integer, Equipos>) readAll();
			}
			equipo = result.get(id);
			if(equipo.getId() > 0){
		    	if(equipo.getCategoriaId() != null){
		    		equipo.setCategoria(leerCategorias.read(equipo.getCategoriaId()));
		    	}
		    	if(equipo.getBoleraId() != null){
		    		equipo.setBolera(leerBoleras.read(equipo.getBoleraId()));
		    	}
		    	if(readAditional){
		    		equipo.setJugadoresList(leerJugadores.readEquipo(id));
		    	}
		    	
		    }
		}
		return equipo;
	}

	@Override
	public List<?> listResult() {
		List<Equipos> listResult = new ArrayList<Equipos>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    Equipos equipo = (Equipos) value;
		    if(equipo.getId() > 0){
		    	if(equipo.getCategoriaId() != null){
		    		equipo.setCategoria(leerCategorias.read(equipo.getCategoriaId()));
		    	}
		    	if(equipo.getBoleraId() != null){
		    		equipo.setBolera(leerBoleras.read(equipo.getBoleraId()));
		    	}
		    	listResult.add((Equipos) equipo);
		    }
		}
		
		//Ordenar la clasificacion
		Collections.sort(listResult, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				Equipos pi1 = (Equipos) o1;
				Equipos pi2 = (Equipos) o2;
				
				int requipo = pi1.getCategoriaId().compareTo(pi2.getCategoriaId());
				if(requipo == 0){
					requipo = pi1.getNombre().compareTo(pi2.getNombre());
				}
				return requipo;
			}
		});
		
		return listResult;
	}
	
	public List<?> listResultLiga() {
		List<Equipos> listResult = new ArrayList<Equipos>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    Equipos equipo = (Equipos) value;
		    if(equipo.getId() > 0){
		    	if(equipo.getLiga().equalsIgnoreCase(Activo.SI)){
			    	if(equipo.getCategoriaId() != null){
			    		equipo.setCategoria(leerCategorias.read(equipo.getCategoriaId()));
			    	}
			    	if(equipo.getBoleraId() != null){
			    		equipo.setBolera(leerBoleras.read(equipo.getBoleraId()));
			    	}
			    	listResult.add((Equipos) equipo);
			    }
		    }
		}
		return listResult;
	}

}
