package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.inject.Named;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.application.enumerations.ModalidadJuegoEnumeration;
import com.bolocelta.bbdd.constants.NombresTablas;
import com.bolocelta.bbdd.constants.Ubicaciones;
import com.bolocelta.entities.HistoricoCampeonatosEspana;
import com.bolocelta.transformer.HistoricoCampeonatoEspanaTransformer;

@Named
public class LeerHistoricoCampeonatosEspana extends ALeer {

	HashMap<Integer, HistoricoCampeonatosEspana> result = null;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, HistoricoCampeonatosEspana> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas.N_CSV_BBDD_HISTORICO_ESPANA, Ubicaciones.UBICACION_BBDD_HISTORICO);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	HistoricoCampeonatosEspana historicoCE = new HistoricoCampeonatosEspana();
	    		historicoCE = HistoricoCampeonatoEspanaTransformer.transformerObjectCsv(historicoCE, row);
		    	result.put(historicoCE.getId(), historicoCE);
		    }
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public HistoricoCampeonatosEspana read(Integer id) {
		if(this.result == null){
			this.result = (HashMap<Integer, HistoricoCampeonatosEspana>) readAll();
		}
		return result.get(id);
	}

	@Override
	public List<?> listResult() {
		List<HistoricoCampeonatosEspana> listResult = new ArrayList<HistoricoCampeonatosEspana>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    listResult.add((HistoricoCampeonatosEspana) value);
		}
		return listResult;
	}
	
	public List<?> listResultIndividual() {
		List<HistoricoCampeonatosEspana> listResult = new ArrayList<HistoricoCampeonatosEspana>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    HistoricoCampeonatosEspana historicoCE = (HistoricoCampeonatosEspana) value;
		    if(historicoCE.getTipo() == ModalidadJuegoEnumeration.MODALIDAD_INDIVIDUAL){
		    	listResult.add((HistoricoCampeonatosEspana) value);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultParejas() {
		List<HistoricoCampeonatosEspana> listResult = new ArrayList<HistoricoCampeonatosEspana>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    HistoricoCampeonatosEspana historicoCE = (HistoricoCampeonatosEspana) value;
		    if(historicoCE.getTipo() == ModalidadJuegoEnumeration.MODALIDAD_PAREJAS){
		    	listResult.add((HistoricoCampeonatosEspana) value);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultEquipos() {
		List<HistoricoCampeonatosEspana> listResult = new ArrayList<HistoricoCampeonatosEspana>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    HistoricoCampeonatosEspana historicoCE = (HistoricoCampeonatosEspana) value;
		    if(historicoCE.getTipo() == ModalidadJuegoEnumeration.MODALIDAD_EQUIPOS){
		    	listResult.add((HistoricoCampeonatosEspana) value);
		    }
		}
		return listResult;
	}

}
