package com.bolocelta.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.bolocelta.bbdd.readTables.LeerJugadores;
import com.bolocelta.entities.Jugadores;

/**
 * The Class SelectOneMenuConverter.
 */
@FacesConverter("jugadoresConverter")
public class JugadoresConverter implements Converter {
	
	private LeerJugadores leerJugadores = new LeerJugadores();
 
    @Override
    public Object getAsObject(final FacesContext arg0, final UIComponent arg1, final String objectString) {
        if (objectString == null) {
            return null;
        }
 
        return fromSelect(arg1, objectString);
    }
 
    /**
     * Serialize.
     *
     * @param object
     *            the object
     * @return the string
     */
    private String serialize(final Object object) {
        if (object == null) {
            return null;
        }
        return String.valueOf(object.hashCode());
    }
 
    /**
     * From select.
     *
     * @param currentcomponent
     *            the currentcomponent
     * @param objectString
     *            the object string
     * @return the object
     */
    private Object fromSelect(final UIComponent currentcomponent, final String objectString) {
    	
    	
    	if(leerJugadores.listResult() != null){
    		for (Object object : leerJugadores.listResult()) {
    			Jugadores jugador = (Jugadores) object;
    			String serialize = serialize(jugador);
    			
    			if(serialize.equals(objectString)){
    				return jugador;
    			}
				
			}
    	}
    	
        return null;
    }
 
    @Override
    public String getAsString(final FacesContext arg0, final UIComponent arg1, final Object object) {
        return serialize(object);
    }
 
}