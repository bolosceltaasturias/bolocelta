package com.bolocelta.entities;

public class CampeonatoEquiposClasificacion implements Comparable<CampeonatoEquiposClasificacion>{

	private Integer id;
	private Integer equipoId;
	private Integer categoriaId;
	private Integer jugados;
	private Integer ganados;
	private Integer empatados;
	private Integer perdidos;
	private Integer partidasFavor;
	private Integer partidasContra;
	private Integer puntos;
	private Equipos equipo;
	private Categorias categoria;
	private Long rowNum;

	public CampeonatoEquiposClasificacion() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEquipoId() {
		return equipoId;
	}

	public void setEquipoId(Integer equipoId) {
		this.equipoId = equipoId;
	}

	public Integer getCategoriaId() {
		return categoriaId;
	}

	public void setCategoriaId(Integer categoriaId) {
		this.categoriaId = categoriaId;
	}

	public Integer getJugados() {
		return jugados;
	}

	public void setJugados(Integer jugados) {
		this.jugados = jugados;
	}

	public Integer getGanados() {
		return ganados;
	}

	public void setGanados(Integer ganados) {
		this.ganados = ganados;
	}

	public Integer getEmpatados() {
		return empatados;
	}

	public void setEmpatados(Integer empatados) {
		this.empatados = empatados;
	}

	public Integer getPerdidos() {
		return perdidos;
	}

	public void setPerdidos(Integer perdidos) {
		this.perdidos = perdidos;
	}

	public Integer getPartidasFavor() {
		return partidasFavor;
	}

	public void setPartidasFavor(Integer partidasFavor) {
		this.partidasFavor = partidasFavor;
	}

	public Integer getPartidasContra() {
		return partidasContra;
	}

	public void setPartidasContra(Integer partidasContra) {
		this.partidasContra = partidasContra;
	}

	public Integer getPuntos() {
		return puntos;
	}

	public void setPuntos(Integer puntos) {
		this.puntos = puntos;
	}

	public Equipos getEquipo() {
		return equipo;
	}

	public void setEquipo(Equipos equipo) {
		this.equipo = equipo;
	}

	public Categorias getCategoria() {
		return categoria;
	}

	public void setCategoria(Categorias categoria) {
		this.categoria = categoria;
	}

	@Override
	public int compareTo(CampeonatoEquiposClasificacion o) {
		return (int)(o.getPuntos() - o.getPartidasFavor());
	}
	
	public String getInsertRow(){
		return (this.id + ";" +
				this.categoriaId + ";" +
				this.equipoId + ";" +
				this.jugados + ";" +
				this.ganados + ";" +
				this.empatados + ";" +
				this.perdidos + ";" +
				this.partidasFavor + ";" +
				this.partidasContra + ";" +
				this.puntos
				);
	}
	
	public Long getRowNum() {
		return rowNum;
	}

	public void setRowNum(Long rowNum) {
		this.rowNum = rowNum;
	}

}
