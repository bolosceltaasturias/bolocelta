package com.bolocelta.entities;

public class JornadaCategoria {

	private Categorias categoria;
	private Integer jornada;
	private Integer jornadaVta;

	public JornadaCategoria() {
		// TODO Auto-generated constructor stub
	}

	public Categorias getCategoria() {
		return categoria;
	}

	public void setCategoria(Categorias categoria) {
		this.categoria = categoria;
	}

	public Integer getJornada() {
		return jornada;
	}

	public void setJornada(Integer jornada) {
		this.jornada = jornada;
	}
	
	public String getId(){
		return this.categoria.getId().toString() + "-" + this.jornada;
	}

	public Integer getJornadaVta() {
		return jornadaVta;
	}

	public void setJornadaVta(Integer jornadaVta) {
		this.jornadaVta = jornadaVta;
	}

}
