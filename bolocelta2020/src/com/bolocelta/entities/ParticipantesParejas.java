package com.bolocelta.entities;

public class ParticipantesParejas {

	private Parejas pareja;
	private String fechaInscripcion;
	private String usuarioInscripcion;
	private String activo;
	private Long rowNum;
	private Integer numeroParejaGrupo;
	private Integer categoria;

	public ParticipantesParejas() {
		// TODO Auto-generated constructor stub
		this.pareja = new Parejas();
	}

	public Parejas getPareja() {
		return pareja;
	}

	public void setPareja(Parejas pareja) {
		this.pareja = pareja;
	}

	public String getFechaInscripcion() {
		return fechaInscripcion;
	}

	public void setFechaInscripcion(String fechaInscripcion) {
		this.fechaInscripcion = fechaInscripcion;
	}

	public String getUsuarioInscripcion() {
		return usuarioInscripcion;
	}

	public void setUsuarioInscripcion(String usuarioInscripcion) {
		this.usuarioInscripcion = usuarioInscripcion;
	}

	public String getActivo() {
		return activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	public Long getRowNum() {
		return rowNum;
	}

	public void setRowNum(Long rowNum) {
		this.rowNum = rowNum;
	}
	
	public Equipos getEquipo1() {
		if(pareja.getJugador1() != null && pareja.getJugador1().getEquipo() != null){
			return pareja.getJugador1().getEquipo();
		}
		return null;
	}
	
	public Equipos getEquipo2() {
		if(pareja.getJugador2() != null && pareja.getJugador2().getEquipo() != null){
			return pareja.getJugador2().getEquipo();
		}
		return null;
	}

	public Integer getNumeroParejaGrupo() {
		return numeroParejaGrupo;
	}

	public void setNumeroParejaGrupo(Integer numeroParejaGrupo) {
		this.numeroParejaGrupo = numeroParejaGrupo;
	}

	public Integer getCategoria() {
		return categoria;
	}

	public void setCategoria(Integer categoria) {
		this.categoria = categoria;
	}
	
}
