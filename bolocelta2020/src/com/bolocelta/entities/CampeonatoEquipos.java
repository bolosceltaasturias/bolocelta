package com.bolocelta.entities;

import java.util.List;

public class CampeonatoEquipos {

	private List<CampeonatoEquiposClasificacion> clasificacion;
	private List<CampeonatoEquiposCalendario> calendario;

	public CampeonatoEquipos() {
		// TODO Auto-generated constructor stub
	}

	public List<CampeonatoEquiposClasificacion> getClasificacion() {
		return clasificacion;
	}

	public void setClasificacion(List<CampeonatoEquiposClasificacion> clasificacion) {
		this.clasificacion = clasificacion;
	}

	public List<CampeonatoEquiposCalendario> getCalendario() {
		return calendario;
	}

	public void setCalendario(List<CampeonatoEquiposCalendario> calendario) {
		this.calendario = calendario;
	}



}
