package com.bolocelta.entities.sorteos.modelosGrupos;

import java.util.LinkedHashMap;

import com.bolocelta.bbdd.constants.TipoSorteo;

public class ModeloIndividualGrupos8 {

	private String nombre = "Grupos 8";
	private LinkedHashMap<Integer, ModeloIndividualEnfrentamientoGruposFaseI> enfrentamientosGrupo = new LinkedHashMap<>();

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedHashMap<Integer, ModeloIndividualEnfrentamientoGruposFaseI> getEnfrentamientosGrupo() {
		return enfrentamientosGrupo;
	}

	public void setEnfrentamientosGrupo(LinkedHashMap<Integer, ModeloIndividualEnfrentamientoGruposFaseI> enfrentamientosGrupo) {
		this.enfrentamientosGrupo = enfrentamientosGrupo;
	}
	
	public ModeloIndividualGrupos8() {
		this.enfrentamientosGrupo.put(1, new ModeloIndividualEnfrentamientoGruposFaseI(1, 2, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(2, new ModeloIndividualEnfrentamientoGruposFaseI(3, 4, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(3, new ModeloIndividualEnfrentamientoGruposFaseI(5, 6, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(4, new ModeloIndividualEnfrentamientoGruposFaseI(7, 8, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(5, new ModeloIndividualEnfrentamientoGruposFaseI(4, 1, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(6, new ModeloIndividualEnfrentamientoGruposFaseI(2, 3, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(7, new ModeloIndividualEnfrentamientoGruposFaseI(6, 7, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(8, new ModeloIndividualEnfrentamientoGruposFaseI(8, 5, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(9, new ModeloIndividualEnfrentamientoGruposFaseI(1, 6, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(10, new ModeloIndividualEnfrentamientoGruposFaseI(7, 4, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(11, new ModeloIndividualEnfrentamientoGruposFaseI(5, 2, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(12, new ModeloIndividualEnfrentamientoGruposFaseI(3, 8, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(13, new ModeloIndividualEnfrentamientoGruposFaseI(2, 7, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(14, new ModeloIndividualEnfrentamientoGruposFaseI(4, 5, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(15, new ModeloIndividualEnfrentamientoGruposFaseI(6, 3, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(16, new ModeloIndividualEnfrentamientoGruposFaseI(8, 1, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(17, new ModeloIndividualEnfrentamientoGruposFaseI(5, 7, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(18, new ModeloIndividualEnfrentamientoGruposFaseI(2, 4, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(19, new ModeloIndividualEnfrentamientoGruposFaseI(6, 8, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(20, new ModeloIndividualEnfrentamientoGruposFaseI(1, 3, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(21, new ModeloIndividualEnfrentamientoGruposFaseI(4, 6, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(22, new ModeloIndividualEnfrentamientoGruposFaseI(8, 2, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(23, new ModeloIndividualEnfrentamientoGruposFaseI(3, 5, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(24, new ModeloIndividualEnfrentamientoGruposFaseI(7, 1, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(25, new ModeloIndividualEnfrentamientoGruposFaseI(2, 6, TipoSorteo.MONEDA));
		this.enfrentamientosGrupo.put(26, new ModeloIndividualEnfrentamientoGruposFaseI(4, 8, TipoSorteo.MONEDA));
		this.enfrentamientosGrupo.put(27, new ModeloIndividualEnfrentamientoGruposFaseI(1, 5, TipoSorteo.MONEDA));
		this.enfrentamientosGrupo.put(28, new ModeloIndividualEnfrentamientoGruposFaseI(3, 7, TipoSorteo.MONEDA));
	}

}
