package com.bolocelta.entities.sorteos.modelosGrupos;

import java.util.LinkedHashMap;

import com.bolocelta.bbdd.constants.CrucesCampeonatos;
import com.bolocelta.bbdd.constants.TipoSorteo;

public class ModeloIndividualCuartosFinal4OF {

	private String nombre = "Cuartos de Final";
	private LinkedHashMap<Integer, ModeloIndividualEnfrentamientoCuartosFinal> enfrentamientosCuartosFinal = new LinkedHashMap<>();

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedHashMap<Integer, ModeloIndividualEnfrentamientoCuartosFinal> getEnfrentamientosCuartosFinal() {
		return enfrentamientosCuartosFinal;
	}

	public void setEnfrentamientosCuartosFinal(LinkedHashMap<Integer, ModeloIndividualEnfrentamientoCuartosFinal> enfrentamientosCuartosFinal) {
		this.enfrentamientosCuartosFinal = enfrentamientosCuartosFinal;
	}
	
	public ModeloIndividualCuartosFinal4OF() {
		this.enfrentamientosCuartosFinal.put(1, new ModeloIndividualEnfrentamientoCuartosFinal(CrucesCampeonatos.CFA, 1, CrucesCampeonatos.OFA, CrucesCampeonatos.OFB, 0, 0, 0, 0, TipoSorteo.MONEDA, CrucesCampeonatos.SF1));
		this.enfrentamientosCuartosFinal.put(2, new ModeloIndividualEnfrentamientoCuartosFinal(CrucesCampeonatos.CFB, 1, CrucesCampeonatos.OFC, CrucesCampeonatos.OFD, 0, 0, 0, 0, TipoSorteo.MONEDA, CrucesCampeonatos.SF1));
		this.enfrentamientosCuartosFinal.put(3, new ModeloIndividualEnfrentamientoCuartosFinal(CrucesCampeonatos.CFC, 1, CrucesCampeonatos.OFE, CrucesCampeonatos.OFF, 0, 0, 0, 0, TipoSorteo.MONEDA, CrucesCampeonatos.SF2));
		this.enfrentamientosCuartosFinal.put(4, new ModeloIndividualEnfrentamientoCuartosFinal(CrucesCampeonatos.CFD, 1, CrucesCampeonatos.OFG, CrucesCampeonatos.OFH, 0, 0, 0, 0, TipoSorteo.MONEDA, CrucesCampeonatos.SF2));
	}

}
