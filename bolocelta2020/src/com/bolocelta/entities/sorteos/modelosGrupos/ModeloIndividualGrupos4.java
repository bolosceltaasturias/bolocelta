package com.bolocelta.entities.sorteos.modelosGrupos;

import java.util.LinkedHashMap;

import com.bolocelta.bbdd.constants.TipoSorteo;

public class ModeloIndividualGrupos4 {

	private String nombre = "Grupo 4";
	private LinkedHashMap<Integer, ModeloIndividualEnfrentamientoGruposFaseI> enfrentamientosGrupo = new LinkedHashMap<>();

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedHashMap<Integer, ModeloIndividualEnfrentamientoGruposFaseI> getEnfrentamientosGrupo() {
		return enfrentamientosGrupo;
	}

	public void setEnfrentamientosGrupo(LinkedHashMap<Integer, ModeloIndividualEnfrentamientoGruposFaseI> enfrentamientosGrupo) {
		this.enfrentamientosGrupo = enfrentamientosGrupo;
	}
	
	public ModeloIndividualGrupos4() {
		this.enfrentamientosGrupo.put(1, new ModeloIndividualEnfrentamientoGruposFaseI(1, 2, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(2, new ModeloIndividualEnfrentamientoGruposFaseI(3, 4, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(3, new ModeloIndividualEnfrentamientoGruposFaseI(4, 1, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(4, new ModeloIndividualEnfrentamientoGruposFaseI(2, 3, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(5, new ModeloIndividualEnfrentamientoGruposFaseI(1, 3, TipoSorteo.MONEDA));
		this.enfrentamientosGrupo.put(6, new ModeloIndividualEnfrentamientoGruposFaseI(2, 4, TipoSorteo.MONEDA));
	}

}
