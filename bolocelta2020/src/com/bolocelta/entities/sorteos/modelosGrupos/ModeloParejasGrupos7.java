package com.bolocelta.entities.sorteos.modelosGrupos;

import java.util.LinkedHashMap;

import com.bolocelta.bbdd.constants.TipoSorteo;

public class ModeloParejasGrupos7 {

	private String nombre = "Grupo 7";
	private LinkedHashMap<Integer, ModeloParejasEnfrentamientoGruposFaseI> enfrentamientosGrupo = new LinkedHashMap<>();

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedHashMap<Integer, ModeloParejasEnfrentamientoGruposFaseI> getEnfrentamientosGrupo() {
		return enfrentamientosGrupo;
	}

	public void setEnfrentamientosGrupo(LinkedHashMap<Integer, ModeloParejasEnfrentamientoGruposFaseI> enfrentamientosGrupo) {
		this.enfrentamientosGrupo = enfrentamientosGrupo;
	}
	
	public ModeloParejasGrupos7() {
		this.enfrentamientosGrupo.put(1, new ModeloParejasEnfrentamientoGruposFaseI(1, 2, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(2, new ModeloParejasEnfrentamientoGruposFaseI(3, 4, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(3, new ModeloParejasEnfrentamientoGruposFaseI(5, 6, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(4, new ModeloParejasEnfrentamientoGruposFaseI(7, 1, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(5, new ModeloParejasEnfrentamientoGruposFaseI(2, 3, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(6, new ModeloParejasEnfrentamientoGruposFaseI(4, 5, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(7, new ModeloParejasEnfrentamientoGruposFaseI(6, 7, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(8, new ModeloParejasEnfrentamientoGruposFaseI(3, 1, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(9, new ModeloParejasEnfrentamientoGruposFaseI(4, 2, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(10, new ModeloParejasEnfrentamientoGruposFaseI(7, 5, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(11, new ModeloParejasEnfrentamientoGruposFaseI(3, 6, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(12, new ModeloParejasEnfrentamientoGruposFaseI(1, 4, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(13, new ModeloParejasEnfrentamientoGruposFaseI(2, 7, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(14, new ModeloParejasEnfrentamientoGruposFaseI(5, 3, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(15, new ModeloParejasEnfrentamientoGruposFaseI(1, 6, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(16, new ModeloParejasEnfrentamientoGruposFaseI(4, 7, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(17, new ModeloParejasEnfrentamientoGruposFaseI(2, 5, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(18, new ModeloParejasEnfrentamientoGruposFaseI(6, 4, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(19, new ModeloParejasEnfrentamientoGruposFaseI(5, 1, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(20, new ModeloParejasEnfrentamientoGruposFaseI(7, 3, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(21, new ModeloParejasEnfrentamientoGruposFaseI(6, 2, TipoSorteo.LOCAL));
	}

}
