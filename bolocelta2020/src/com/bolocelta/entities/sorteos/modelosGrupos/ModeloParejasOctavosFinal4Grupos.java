package com.bolocelta.entities.sorteos.modelosGrupos;

import java.util.LinkedHashMap;

import com.bolocelta.bbdd.constants.CrucesCampeonatos;
import com.bolocelta.bbdd.constants.GruposLetra;
import com.bolocelta.bbdd.constants.TipoSorteo;

public class ModeloParejasOctavosFinal4Grupos {

	private String nombre = "Octavos de Final";
	private LinkedHashMap<Integer, ModeloParejasEnfrentamientoOctavosFinal> enfrentamientosOctavosFinal = new LinkedHashMap<>();

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedHashMap<Integer, ModeloParejasEnfrentamientoOctavosFinal> getEnfrentamientosOctavosFinal() {
		return enfrentamientosOctavosFinal;
	}

	public void setEnfrentamientosOctavosFinal(LinkedHashMap<Integer, ModeloParejasEnfrentamientoOctavosFinal> enfrentamientosOctavosFinal) {
		this.enfrentamientosOctavosFinal = enfrentamientosOctavosFinal;
	}
	
	public ModeloParejasOctavosFinal4Grupos() {
		this.enfrentamientosOctavosFinal.put(1, new ModeloParejasEnfrentamientoOctavosFinal(CrucesCampeonatos.OFA, 1, GruposLetra.FASE_I_G_A, GruposLetra.FASE_I_G_B, 1, 4, 0, 0, TipoSorteo.MONEDA, CrucesCampeonatos.CFA));
		this.enfrentamientosOctavosFinal.put(2, new ModeloParejasEnfrentamientoOctavosFinal(CrucesCampeonatos.OFB, 1, GruposLetra.FASE_I_G_C, GruposLetra.FASE_I_G_D, 2, 3, 0, 0, TipoSorteo.MONEDA, CrucesCampeonatos.CFA));
		this.enfrentamientosOctavosFinal.put(3, new ModeloParejasEnfrentamientoOctavosFinal(CrucesCampeonatos.OFC, 1, GruposLetra.FASE_I_G_D, GruposLetra.FASE_I_G_C, 1, 4, 0, 0, TipoSorteo.MONEDA, CrucesCampeonatos.CFB));
		this.enfrentamientosOctavosFinal.put(4, new ModeloParejasEnfrentamientoOctavosFinal(CrucesCampeonatos.OFD, 1, GruposLetra.FASE_I_G_B, GruposLetra.FASE_I_G_A, 2, 3, 0, 0, TipoSorteo.MONEDA, CrucesCampeonatos.CFB));
		this.enfrentamientosOctavosFinal.put(5, new ModeloParejasEnfrentamientoOctavosFinal(CrucesCampeonatos.OFE, 1, GruposLetra.FASE_I_G_C, GruposLetra.FASE_I_G_A, 1, 4, 0, 0, TipoSorteo.MONEDA, CrucesCampeonatos.CFC));
		this.enfrentamientosOctavosFinal.put(6, new ModeloParejasEnfrentamientoOctavosFinal(CrucesCampeonatos.OFF, 1, GruposLetra.FASE_I_G_D, GruposLetra.FASE_I_G_B, 2, 3, 0, 0, TipoSorteo.MONEDA, CrucesCampeonatos.CFC));
		this.enfrentamientosOctavosFinal.put(7, new ModeloParejasEnfrentamientoOctavosFinal(CrucesCampeonatos.OFG, 1, GruposLetra.FASE_I_G_B, GruposLetra.FASE_I_G_D, 1, 4, 0, 0, TipoSorteo.MONEDA, CrucesCampeonatos.CFD));
		this.enfrentamientosOctavosFinal.put(8, new ModeloParejasEnfrentamientoOctavosFinal(CrucesCampeonatos.OFH, 1, GruposLetra.FASE_I_G_A, GruposLetra.FASE_I_G_C, 2, 3, 0, 0, TipoSorteo.MONEDA, CrucesCampeonatos.CFD));
	}

}
