package com.bolocelta.entities.sorteos.modelosGrupos;

import java.util.LinkedHashMap;

import com.bolocelta.bbdd.constants.CrucesCampeonatos;
import com.bolocelta.bbdd.constants.TipoSorteo;

public class ModeloParejasFinalConsolacion4SF {

	private String nombre = "Final Consolacion";
	private LinkedHashMap<Integer, ModeloParejasEnfrentamientoFinalConsolacion> enfrentamientosFinalConsolacion = new LinkedHashMap<>();

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedHashMap<Integer, ModeloParejasEnfrentamientoFinalConsolacion> getEnfrentamientosFinalConsolacion() {
		return enfrentamientosFinalConsolacion;
	}

	public void setEnfrentamientosFinalConsolacion(LinkedHashMap<Integer, ModeloParejasEnfrentamientoFinalConsolacion> enfrentamientosFinalConsolacion) {
		this.enfrentamientosFinalConsolacion = enfrentamientosFinalConsolacion;
	}
	
	public ModeloParejasFinalConsolacion4SF() {
		this.enfrentamientosFinalConsolacion.put(1, new ModeloParejasEnfrentamientoFinalConsolacion(CrucesCampeonatos.FC1, 1, CrucesCampeonatos.SF1, CrucesCampeonatos.SF2, 0, 0, 0, 0, TipoSorteo.MONEDA, null));
	}

}
