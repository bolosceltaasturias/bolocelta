package com.bolocelta.entities.sorteos.modelosGrupos;

public class ModeloIndividualEnfrentamientoGruposFaseI {

	public Integer jugador1;
	public Integer jugador2;
	public String sorteo;

	public ModeloIndividualEnfrentamientoGruposFaseI(Integer jugador1, Integer jugador2, String sorteo) {
		this.jugador1 = jugador1;
		this.jugador2 = jugador2;
		this.sorteo = sorteo;
	}

	public Integer getJugador1() {
		return jugador1;
	}

	public void setJugador1(Integer jugador1) {
		this.jugador1 = jugador1;
	}

	public Integer getJugador2() {
		return jugador2;
	}

	public void setJugador2(Integer jugador2) {
		this.jugador2 = jugador2;
	}

	public String getSorteo() {
		return sorteo;
	}

	public void setSorteo(String sorteo) {
		this.sorteo = sorteo;
	}

}
