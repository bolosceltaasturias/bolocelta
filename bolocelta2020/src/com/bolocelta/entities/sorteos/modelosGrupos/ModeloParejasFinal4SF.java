package com.bolocelta.entities.sorteos.modelosGrupos;

import java.util.LinkedHashMap;

import com.bolocelta.bbdd.constants.CrucesCampeonatos;
import com.bolocelta.bbdd.constants.TipoSorteo;

public class ModeloParejasFinal4SF {

	private String nombre = "Final";
	private LinkedHashMap<Integer, ModeloParejasEnfrentamientoFinal> enfrentamientosFinal = new LinkedHashMap<>();

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedHashMap<Integer, ModeloParejasEnfrentamientoFinal> getEnfrentamientosFinal() {
		return enfrentamientosFinal;
	}

	public void setEnfrentamientosFinal(LinkedHashMap<Integer, ModeloParejasEnfrentamientoFinal> enfrentamientosFinal) {
		this.enfrentamientosFinal = enfrentamientosFinal;
	}
	
	public ModeloParejasFinal4SF() {
		this.enfrentamientosFinal.put(1, new ModeloParejasEnfrentamientoFinal(CrucesCampeonatos.FF1, 1, CrucesCampeonatos.SF1, CrucesCampeonatos.SF2, 0, 0, 0, 0, TipoSorteo.MONEDA, null));
	}

}
