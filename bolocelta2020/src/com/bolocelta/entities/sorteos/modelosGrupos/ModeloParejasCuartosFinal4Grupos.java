package com.bolocelta.entities.sorteos.modelosGrupos;

import java.util.LinkedHashMap;

import com.bolocelta.bbdd.constants.CrucesCampeonatos;
import com.bolocelta.bbdd.constants.GruposLetra;
import com.bolocelta.bbdd.constants.TipoSorteo;

public class ModeloParejasCuartosFinal4Grupos {

	private String nombre = "Cuartos de Final";
	private LinkedHashMap<Integer, ModeloParejasEnfrentamientoCuartosFinal> enfrentamientosCuartosFinal = new LinkedHashMap<>();

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedHashMap<Integer, ModeloParejasEnfrentamientoCuartosFinal> getEnfrentamientosCuartosFinal() {
		return enfrentamientosCuartosFinal;
	}

	public void setEnfrentamientosCuartosFinal(LinkedHashMap<Integer, ModeloParejasEnfrentamientoCuartosFinal> enfrentamientosCuartosFinal) {
		this.enfrentamientosCuartosFinal = enfrentamientosCuartosFinal;
	}
	
	public ModeloParejasCuartosFinal4Grupos() {
		this.enfrentamientosCuartosFinal.put(1, new ModeloParejasEnfrentamientoCuartosFinal(CrucesCampeonatos.CFA, 1, GruposLetra.FASE_I_G_A, GruposLetra.FASE_I_G_B, 1, 2, 0, 0, TipoSorteo.MONEDA, CrucesCampeonatos.SF1));
		this.enfrentamientosCuartosFinal.put(2, new ModeloParejasEnfrentamientoCuartosFinal(CrucesCampeonatos.CFB, 1, GruposLetra.FASE_I_G_B, GruposLetra.FASE_I_G_A, 1, 2, 0, 0, TipoSorteo.MONEDA, CrucesCampeonatos.SF1));
		this.enfrentamientosCuartosFinal.put(3, new ModeloParejasEnfrentamientoCuartosFinal(CrucesCampeonatos.CFC, 1, GruposLetra.FASE_I_G_C, GruposLetra.FASE_I_G_D, 1, 2, 0, 0, TipoSorteo.MONEDA, CrucesCampeonatos.SF2));
		this.enfrentamientosCuartosFinal.put(4, new ModeloParejasEnfrentamientoCuartosFinal(CrucesCampeonatos.CFD, 1, GruposLetra.FASE_I_G_D, GruposLetra.FASE_I_G_C, 1, 2, 0, 0, TipoSorteo.MONEDA, CrucesCampeonatos.SF2));
	}

}
