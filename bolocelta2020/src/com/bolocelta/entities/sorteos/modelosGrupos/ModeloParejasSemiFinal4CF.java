package com.bolocelta.entities.sorteos.modelosGrupos;

import java.util.LinkedHashMap;

import com.bolocelta.bbdd.constants.CrucesCampeonatos;
import com.bolocelta.bbdd.constants.TipoSorteo;

public class ModeloParejasSemiFinal4CF {

	private String nombre = "Semifinal";
	private LinkedHashMap<Integer, ModeloParejasEnfrentamientoSemiFinal> enfrentamientosSemiFinal = new LinkedHashMap<>();

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedHashMap<Integer, ModeloParejasEnfrentamientoSemiFinal> getEnfrentamientosSemiFinal() {
		return enfrentamientosSemiFinal;
	}

	public void setEnfrentamientosSemiFinal(LinkedHashMap<Integer, ModeloParejasEnfrentamientoSemiFinal> enfrentamientosSemiFinal) {
		this.enfrentamientosSemiFinal = enfrentamientosSemiFinal;
	}
	
	public ModeloParejasSemiFinal4CF() {
		this.enfrentamientosSemiFinal.put(1, new ModeloParejasEnfrentamientoSemiFinal(CrucesCampeonatos.SF1, 1, CrucesCampeonatos.CFA, CrucesCampeonatos.CFB, 0, 0, 0, 0, TipoSorteo.MONEDA, CrucesCampeonatos.FF1));
		this.enfrentamientosSemiFinal.put(2, new ModeloParejasEnfrentamientoSemiFinal(CrucesCampeonatos.SF2, 1, CrucesCampeonatos.CFC, CrucesCampeonatos.CFD, 0, 0, 0, 0, TipoSorteo.MONEDA, CrucesCampeonatos.FF1));
	}

}
