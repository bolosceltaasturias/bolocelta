package com.bolocelta.entities.sorteos.modelosGrupos;

import java.util.LinkedHashMap;

import com.bolocelta.bbdd.constants.TipoSorteo;

public class ModeloIndividualGrupos6 {

	private String nombre = "Grupo 6";
	private LinkedHashMap<Integer, ModeloIndividualEnfrentamientoGruposFaseI> enfrentamientosGrupo = new LinkedHashMap<>();

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedHashMap<Integer, ModeloIndividualEnfrentamientoGruposFaseI> getEnfrentamientosGrupo() {
		return enfrentamientosGrupo;
	}

	public void setEnfrentamientosGrupo(LinkedHashMap<Integer, ModeloIndividualEnfrentamientoGruposFaseI> enfrentamientosGrupo) {
		this.enfrentamientosGrupo = enfrentamientosGrupo;
	}
	
	public ModeloIndividualGrupos6() {
		this.enfrentamientosGrupo.put(1, new ModeloIndividualEnfrentamientoGruposFaseI(1, 2, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(2, new ModeloIndividualEnfrentamientoGruposFaseI(3, 4, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(3, new ModeloIndividualEnfrentamientoGruposFaseI(6, 5, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(4, new ModeloIndividualEnfrentamientoGruposFaseI(4, 1, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(5, new ModeloIndividualEnfrentamientoGruposFaseI(5, 3, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(6, new ModeloIndividualEnfrentamientoGruposFaseI(2, 6, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(7, new ModeloIndividualEnfrentamientoGruposFaseI(1, 3, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(8, new ModeloIndividualEnfrentamientoGruposFaseI(4, 6, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(9, new ModeloIndividualEnfrentamientoGruposFaseI(2, 5, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(10, new ModeloIndividualEnfrentamientoGruposFaseI(6, 1, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(11, new ModeloIndividualEnfrentamientoGruposFaseI(3, 2, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(12, new ModeloIndividualEnfrentamientoGruposFaseI(5, 4, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(13, new ModeloIndividualEnfrentamientoGruposFaseI(3, 6, TipoSorteo.MONEDA));
		this.enfrentamientosGrupo.put(14, new ModeloIndividualEnfrentamientoGruposFaseI(1, 5, TipoSorteo.MONEDA));
		this.enfrentamientosGrupo.put(15, new ModeloIndividualEnfrentamientoGruposFaseI(2, 4, TipoSorteo.MONEDA));
	}

}
