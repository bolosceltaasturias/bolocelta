package com.bolocelta.entities.sorteos.modelosGrupos;

import java.util.LinkedHashMap;

import com.bolocelta.bbdd.constants.TipoSorteo;

public class ModeloParejasGrupos6 {

	private String nombre = "Grupo 6";
	private LinkedHashMap<Integer, ModeloParejasEnfrentamientoGruposFaseI> enfrentamientosGrupo = new LinkedHashMap<>();

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedHashMap<Integer, ModeloParejasEnfrentamientoGruposFaseI> getEnfrentamientosGrupo() {
		return enfrentamientosGrupo;
	}

	public void setEnfrentamientosGrupo(LinkedHashMap<Integer, ModeloParejasEnfrentamientoGruposFaseI> enfrentamientosGrupo) {
		this.enfrentamientosGrupo = enfrentamientosGrupo;
	}
	
	public ModeloParejasGrupos6() {
		this.enfrentamientosGrupo.put(1, new ModeloParejasEnfrentamientoGruposFaseI(1, 2, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(2, new ModeloParejasEnfrentamientoGruposFaseI(3, 4, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(3, new ModeloParejasEnfrentamientoGruposFaseI(6, 5, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(4, new ModeloParejasEnfrentamientoGruposFaseI(4, 1, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(5, new ModeloParejasEnfrentamientoGruposFaseI(5, 3, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(6, new ModeloParejasEnfrentamientoGruposFaseI(2, 6, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(7, new ModeloParejasEnfrentamientoGruposFaseI(1, 3, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(8, new ModeloParejasEnfrentamientoGruposFaseI(4, 6, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(9, new ModeloParejasEnfrentamientoGruposFaseI(2, 5, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(10, new ModeloParejasEnfrentamientoGruposFaseI(6, 1, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(11, new ModeloParejasEnfrentamientoGruposFaseI(3, 2, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(12, new ModeloParejasEnfrentamientoGruposFaseI(5, 4, TipoSorteo.LOCAL));
		this.enfrentamientosGrupo.put(13, new ModeloParejasEnfrentamientoGruposFaseI(3, 6, TipoSorteo.MONEDA));
		this.enfrentamientosGrupo.put(14, new ModeloParejasEnfrentamientoGruposFaseI(1, 5, TipoSorteo.MONEDA));
		this.enfrentamientosGrupo.put(15, new ModeloParejasEnfrentamientoGruposFaseI(2, 4, TipoSorteo.MONEDA));
	}

}
