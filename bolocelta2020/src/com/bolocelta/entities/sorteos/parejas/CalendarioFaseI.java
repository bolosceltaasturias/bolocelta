package com.bolocelta.entities.sorteos.parejas;

import com.bolocelta.bbdd.constants.Activo;
import com.bolocelta.entities.Boleras;
import com.bolocelta.entities.Parejas;

public class CalendarioFaseI {

	private Integer id;
	private Integer categoriaId;
	private String grupo;
	private Integer pareja1Id;
	private Parejas pareja1;
	private Integer juegosPareja1;
	private Integer juegosPareja2;
	private Integer pareja2Id;
	private Parejas pareja2;
	private Integer orden;
	private String activo;
	private String inicia;
	private Long rowNum;
	private Integer boleraId;
	private Boleras bolera;
	private String requipo1;
	private String requipo2;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCategoriaId() {
		return categoriaId;
	}

	public void setCategoriaId(Integer categoriaId) {
		this.categoriaId = categoriaId;
	}

	public String getGrupo() {
		return grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

	public Integer getPareja1Id() {
		return pareja1Id;
	}

	public void setPareja1Id(Integer pareja1Id) {
		this.pareja1Id = pareja1Id;
	}

	public Integer getJuegosPareja1() {
		return juegosPareja1;
	}

	public void setJuegosPareja1(Integer juegosPareja1) {
		this.juegosPareja1 = juegosPareja1;
	}

	public Integer getJuegosPareja2() {
		return juegosPareja2;
	}

	public void setJuegosPareja2(Integer juegosPareja2) {
		this.juegosPareja2 = juegosPareja2;
	}

	public Integer getPareja2Id() {
		return pareja2Id;
	}

	public void setPareja2Id(Integer pareja2Id) {
		this.pareja2Id = pareja2Id;
	}

	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}

	public String getActivo() {
		return activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	public Long getRowNum() {
		return rowNum;
	}

	public void setRowNum(Long rowNum) {
		this.rowNum = rowNum;
	}

	public String getInicia() {
		return inicia;
	}

	public void setInicia(String inicia) {
		this.inicia = inicia;
	}
	
	public String getInsertRow(){
		return (this.id + ";" +
					this.categoriaId + ";" +
					this.grupo + ";" +
					this.pareja1Id + ";" +
					this.juegosPareja1 + ";" +
					this.juegosPareja2 + ";" +
					this.pareja2Id + ";" +
					this.orden + ";" +
					this.activo + ";" +
					this.inicia + ";" +
					this.boleraId
					);
	}

	public Parejas getPareja1() {
		return pareja1;
	}

	public void setPareja1(Parejas pareja1) {
		this.pareja1 = pareja1;
	}

	public Parejas getPareja2() {
		return pareja2;
	}

	public void setPareja2(Parejas pareja2) {
		this.pareja2 = pareja2;
	}

	public Integer getBoleraId() {
		return boleraId;
	}

	public void setBoleraId(Integer boleraId) {
		this.boleraId = boleraId;
	}

	public Boleras getBolera() {
		return bolera;
	}

	public void setBolera(Boleras bolera) {
		this.bolera = bolera;
	}

	public String getRequipo1() {
		return requipo1;
	}

	public void setRequipo1(String requipo1) {
		this.requipo1 = requipo1;
	}

	public String getRequipo2() {
		return requipo2;
	}

	public void setRequipo2(String requipo2) {
		this.requipo2 = requipo2;
	}
	
	public String getKey(){
		return this.id + "-" + this.categoriaId + "-" + this.grupo;
	}
	
	public boolean isModificable(){
		if(this.activo != null && this.activo.equals(Activo.SI)){
			return true;
		}
		return false;
	}
	
	public boolean isGanaPareja1(){
		if(this.juegosPareja1 > this.juegosPareja2){
			return true;
		}
		return false;
	}
	
	public boolean isEmpate(){
		if(this.juegosPareja1 == this.juegosPareja2){
			return true;
		}
		return false;
	}
	
	public boolean isGanaPareja2(){
		if(this.juegosPareja1 < this.juegosPareja2){
			return true;
		}
		return false;
	}
	
}
