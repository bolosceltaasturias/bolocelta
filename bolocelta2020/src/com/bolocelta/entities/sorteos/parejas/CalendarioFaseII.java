package com.bolocelta.entities.sorteos.parejas;

public class CalendarioFaseII {

	private Integer id;
	private Integer categoriaId;
	private String grupo;
	private Integer pareja1Id;
	private Integer juegosPareja1;
	private Integer juegosPareja2;
	private Integer pareja2Id;
	private Integer orden;
	private String activo;
	private Long rowNum;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCategoriaId() {
		return categoriaId;
	}

	public void setCategoriaId(Integer categoriaId) {
		this.categoriaId = categoriaId;
	}

	public String getGrupo() {
		return grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

	public Integer getPareja1Id() {
		return pareja1Id;
	}

	public void setPareja1Id(Integer pareja1Id) {
		this.pareja1Id = pareja1Id;
	}

	public Integer getJuegosPareja1() {
		return juegosPareja1;
	}

	public void setJuegosPareja1(Integer juegosPareja1) {
		this.juegosPareja1 = juegosPareja1;
	}

	public Integer getJuegosPareja2() {
		return juegosPareja2;
	}

	public void setJuegosPareja2(Integer juegosPareja2) {
		this.juegosPareja2 = juegosPareja2;
	}

	public Integer getPareja2Id() {
		return pareja2Id;
	}

	public void setPareja2Id(Integer pareja2Id) {
		this.pareja2Id = pareja2Id;
	}

	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}

	public String getActivo() {
		return activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	public Long getRowNum() {
		return rowNum;
	}

	public void setRowNum(Long rowNum) {
		this.rowNum = rowNum;
	}

}
