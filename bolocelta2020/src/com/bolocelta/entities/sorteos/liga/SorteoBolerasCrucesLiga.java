package com.bolocelta.entities.sorteos.liga;

import com.bolocelta.entities.Equipos;

public class SorteoBolerasCrucesLiga {

	private Integer id;
	private Integer categoria;
	private Integer numeroCruce;
	private Integer idBoleraPreferente;
	private Integer idBolera1;
	private Integer idBolera2;
	private Integer idBolera3;
	private Integer idBolera4;
	private Integer idBolera5;
	private Integer idBolera6;
	private Integer idBolera7;
	private Integer idBolera8;
	private Equipos equipo;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCategoria() {
		return categoria;
	}

	public void setCategoria(Integer categoria) {
		this.categoria = categoria;
	}

	public Integer getNumeroCruce() {
		return numeroCruce;
	}

	public void setNumeroCruce(Integer numeroCruce) {
		this.numeroCruce = numeroCruce;
	}

	public Integer getIdBoleraPreferente() {
		return idBoleraPreferente;
	}

	public void setIdBoleraPreferente(Integer idBoleraPreferente) {
		this.idBoleraPreferente = idBoleraPreferente;
	}

	public Integer getIdBolera1() {
		return idBolera1;
	}

	public void setIdBolera1(Integer idBolera1) {
		this.idBolera1 = idBolera1;
	}

	public Integer getIdBolera2() {
		return idBolera2;
	}

	public void setIdBolera2(Integer idBolera2) {
		this.idBolera2 = idBolera2;
	}

	public Integer getIdBolera3() {
		return idBolera3;
	}

	public void setIdBolera3(Integer idBolera3) {
		this.idBolera3 = idBolera3;
	}

	public Integer getIdBolera4() {
		return idBolera4;
	}

	public void setIdBolera4(Integer idBolera4) {
		this.idBolera4 = idBolera4;
	}

	public Integer getIdBolera5() {
		return idBolera5;
	}

	public void setIdBolera5(Integer idBolera5) {
		this.idBolera5 = idBolera5;
	}

	public Integer getIdBolera6() {
		return idBolera6;
	}

	public void setIdBolera6(Integer idBolera6) {
		this.idBolera6 = idBolera6;
	}

	public Integer getIdBolera7() {
		return idBolera7;
	}

	public void setIdBolera7(Integer idBolera7) {
		this.idBolera7 = idBolera7;
	}

	public Integer getIdBolera8() {
		return idBolera8;
	}

	public void setIdBolera8(Integer idBolera8) {
		this.idBolera8 = idBolera8;
	}
	
	public String getIdSearch() {
		return this.categoria +"-" + this.numeroCruce;
	}

	public Equipos getEquipo() {
		return equipo;
	}

	public void setEquipo(Equipos equipo) {
		this.equipo = equipo;
	}

}
