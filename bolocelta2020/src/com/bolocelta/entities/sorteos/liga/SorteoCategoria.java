package com.bolocelta.entities.sorteos.liga;

import java.util.HashMap;

import com.bolocelta.entities.Categorias;
import com.bolocelta.entities.Equipos;

public class SorteoCategoria {

	private Categorias categoria;
	private HashMap<Integer, Equipos> equiposPorCategoria = new HashMap<>();

	public Categorias getCategoria() {
		return categoria;
	}

	public void setCategoria(Categorias categoria) {
		this.categoria = categoria;
	}

	public HashMap<Integer, Equipos> getEquiposPorCategoria() {
		return equiposPorCategoria;
	}

	public void setEquiposPorCategoria(HashMap<Integer, Equipos> equiposPorCategoria) {
		this.equiposPorCategoria = equiposPorCategoria;
	}

}
