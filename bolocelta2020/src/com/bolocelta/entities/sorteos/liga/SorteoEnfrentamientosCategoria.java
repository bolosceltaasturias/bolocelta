package com.bolocelta.entities.sorteos.liga;

import java.util.HashMap;

import com.bolocelta.entities.CampeonatoEquiposCalendario;

public class SorteoEnfrentamientosCategoria {

	private Integer id;
	private HashMap<String, CampeonatoEquiposCalendario> campeonatoEquiposCalendario = new HashMap<>();
	private HashMap<String, Integer> campeonatoEquiposCalendarioTuplaBolera = new HashMap<>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public HashMap<String, CampeonatoEquiposCalendario> getCampeonatoEquiposCalendario() {
		return campeonatoEquiposCalendario;
	}

	public void setCampeonatoEquiposCalendario(
			HashMap<String, CampeonatoEquiposCalendario> campeonatoEquiposCalendario) {
		this.campeonatoEquiposCalendario = campeonatoEquiposCalendario;
	}
	
	public HashMap<String, Integer> getCampeonatoEquiposCalendarioTuplaBolera() {
		return campeonatoEquiposCalendarioTuplaBolera;
	}

	public void setCampeonatoEquiposCalendarioTuplaBolera(
			HashMap<String, Integer> campeonatoEquiposCalendarioTuplaBolera) {
		this.campeonatoEquiposCalendarioTuplaBolera = campeonatoEquiposCalendarioTuplaBolera;
	}

}
