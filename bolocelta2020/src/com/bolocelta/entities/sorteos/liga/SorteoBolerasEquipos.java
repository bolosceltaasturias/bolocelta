package com.bolocelta.entities.sorteos.liga;

import java.util.HashMap;

import com.bolocelta.entities.Boleras;
import com.bolocelta.entities.Equipos;

public class SorteoBolerasEquipos {

	private Boleras bolera;
	private HashMap<Integer, Equipos> equipos = new HashMap<>();

	public Boleras getBolera() {
		return bolera;
	}

	public void setBolera(Boleras bolera) {
		this.bolera = bolera;
	}

	public HashMap<Integer, Equipos> getEquipos() {
		return equipos;
	}

	public void setEquipos(HashMap<Integer, Equipos> equipos) {
		this.equipos = equipos;
	}
	
	public Integer getNumeroEquipos(){
		return this.equipos.size();
	}

}
