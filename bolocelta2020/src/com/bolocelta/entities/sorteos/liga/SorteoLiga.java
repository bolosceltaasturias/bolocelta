package com.bolocelta.entities.sorteos.liga;

import java.util.HashMap;
import java.util.LinkedHashMap;

import com.bolocelta.entities.CampeonatoEquiposCalendario;

public class SorteoLiga {
	
	private LinkedHashMap<String, SorteoBolerasCrucesLiga> sorteoBolerasCrucesLigaMap = new LinkedHashMap<>();
	private LinkedHashMap<Integer, SorteoTuplaEnfrentamientosLiga> sorteoTuplaEnfrentamientosLigaMap = new LinkedHashMap<>();
	
	

	private LinkedHashMap<Integer, SorteoCategoria> sorteoCategoriasMap = new LinkedHashMap<>();
	private LinkedHashMap<Integer, SorteoBolerasEquipos> sorteoBolerasEquiposMap = new LinkedHashMap<>();
	private HashMap<Integer, SorteoEnfrentamientosCategoria> sorteoEnfrentamientosCategoriaMap = new HashMap<>();
	private LinkedHashMap<String, SorteoJornadasCategoria> sorteoJornadasCategoriaMap = new LinkedHashMap<>();
	
	//Enfrentamientos jornada asignados
	private LinkedHashMap<String, CampeonatoEquiposCalendario> enfrentamientosJornadaAsignados = new LinkedHashMap<>();

	public LinkedHashMap<Integer, SorteoCategoria> getSorteoCategoriasMap() {
		return sorteoCategoriasMap;
	}

	public void setSorteoCategoriasMap(LinkedHashMap<Integer, SorteoCategoria> sorteoCategoriasMap) {
		this.sorteoCategoriasMap = sorteoCategoriasMap;
	}

	public LinkedHashMap<Integer, SorteoBolerasEquipos> getSorteoBolerasEquiposMap() {
		return sorteoBolerasEquiposMap;
	}

	public void setSorteoBolerasEquiposMap(LinkedHashMap<Integer, SorteoBolerasEquipos> sorteoBolerasEquiposMap) {
		this.sorteoBolerasEquiposMap = sorteoBolerasEquiposMap;
	}

	public HashMap<Integer, SorteoEnfrentamientosCategoria> getSorteoEnfrentamientosCategoriaMap() {
		return sorteoEnfrentamientosCategoriaMap;
	}

	public void setSorteoEnfrentamientosCategoriaMap(
			LinkedHashMap<Integer, SorteoEnfrentamientosCategoria> sorteoEnfrentamientosCategoriaMap) {
		this.sorteoEnfrentamientosCategoriaMap = sorteoEnfrentamientosCategoriaMap;
	}

	public LinkedHashMap<String, SorteoJornadasCategoria> getSorteoJornadasCategoriaMap() {
		return sorteoJornadasCategoriaMap;
	}

	public void setSorteoJornadasCategoriaMap(LinkedHashMap<String, SorteoJornadasCategoria> sorteoJornadasCategoriaMap) {
		this.sorteoJornadasCategoriaMap = sorteoJornadasCategoriaMap;
	}

	public LinkedHashMap<String, CampeonatoEquiposCalendario> getEnfrentamientosJornadaAsignados() {
		return enfrentamientosJornadaAsignados;
	}

	public void setEnfrentamientosJornadaAsignados(
			LinkedHashMap<String, CampeonatoEquiposCalendario> enfrentamientosJornadaAsignados) {
		this.enfrentamientosJornadaAsignados = enfrentamientosJornadaAsignados;
	}

	public LinkedHashMap<String, SorteoBolerasCrucesLiga> getSorteoBolerasCrucesLigaMap() {
		return sorteoBolerasCrucesLigaMap;
	}

	public void setSorteoBolerasCrucesLigaMap(LinkedHashMap<String, SorteoBolerasCrucesLiga> sorteoBolerasCrucesLigaMap) {
		this.sorteoBolerasCrucesLigaMap = sorteoBolerasCrucesLigaMap;
	}

	public LinkedHashMap<Integer, SorteoTuplaEnfrentamientosLiga> getSorteoTuplaEnfrentamientosLigaMap() {
		return sorteoTuplaEnfrentamientosLigaMap;
	}

	public void setSorteoTuplaEnfrentamientosLigaMap(
			LinkedHashMap<Integer, SorteoTuplaEnfrentamientosLiga> sorteoTuplaEnfrentamientosLigaMap) {
		this.sorteoTuplaEnfrentamientosLigaMap = sorteoTuplaEnfrentamientosLigaMap;
	}

}
