package com.bolocelta.entities.sorteos.liga;

import com.bolocelta.entities.Equipos;

public class SorteoTuplaEnfrentamientosLiga implements Cloneable{

	private Integer id;
	private Integer categoria;
	private Integer jornadaIda;
	private Integer jornadaVta;
	private Integer cruce1Ida;
	private Integer cruce2Ida;
	private Integer cruce1Vta;
	private Integer cruce2Vta;
	private Equipos equipo1;
	private Equipos equipo2;
	
	public String getIdSearchCruce1Ida(){
		return categoria + "-" + cruce1Ida;
	}
	
	public String getIdSearchCruce2Ida(){
		return categoria + "-" + cruce2Ida;
	}
	
	public String getIdSearchCruce1Vta(){
		return categoria + "-" + cruce1Vta;
	}
	
	public String getIdSearchCruce2Vta(){
		return categoria + "-" + cruce2Vta;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCategoria() {
		return categoria;
	}

	public void setCategoria(Integer categoria) {
		this.categoria = categoria;
	}

	public Integer getJornadaIda() {
		return jornadaIda;
	}

	public void setJornadaIda(Integer jornadaIda) {
		this.jornadaIda = jornadaIda;
	}

	public Integer getJornadaVta() {
		return jornadaVta;
	}

	public void setJornadaVta(Integer jornadaVta) {
		this.jornadaVta = jornadaVta;
	}

	public Integer getCruce1Ida() {
		return cruce1Ida;
	}

	public void setCruce1Ida(Integer cruce1Ida) {
		this.cruce1Ida = cruce1Ida;
	}

	public Integer getCruce2Ida() {
		return cruce2Ida;
	}

	public void setCruce2Ida(Integer cruce2Ida) {
		this.cruce2Ida = cruce2Ida;
	}

	public Integer getCruce1Vta() {
		return cruce1Vta;
	}

	public void setCruce1Vta(Integer cruce1Vta) {
		this.cruce1Vta = cruce1Vta;
	}

	public Integer getCruce2Vta() {
		return cruce2Vta;
	}

	public void setCruce2Vta(Integer cruce2Vta) {
		this.cruce2Vta = cruce2Vta;
	}

	public Equipos getEquipo1() {
		return equipo1;
	}

	public void setEquipo1(Equipos equipo1) {
		this.equipo1 = equipo1;
	}

	public Equipos getEquipo2() {
		return equipo2;
	}

	public void setEquipo2(Equipos equipo2) {
		this.equipo2 = equipo2;
	}
	
	public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }


}
