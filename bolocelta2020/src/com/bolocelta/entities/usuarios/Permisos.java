package com.bolocelta.entities.usuarios;

public class Permisos {

	private Integer id;
	private Integer rolId;
	private Roles rol;
	private String permiso;

	public Permisos() {
		// TODO Auto-generated constructor stub
	}

	public Permisos(Integer id, String permiso) {
		this.id = id;
		this.permiso = permiso;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPermiso() {
		return permiso;
	}

	public void setPermiso(String permiso) {
		this.permiso = permiso;
	}

	public Integer getRolId() {
		return rolId;
	}

	public void setRolId(Integer rolId) {
		this.rolId = rolId;
	}

	public Roles getRol() {
		return rol;
	}

	public void setRol(Roles rol) {
		this.rol = rol;
	}

}
