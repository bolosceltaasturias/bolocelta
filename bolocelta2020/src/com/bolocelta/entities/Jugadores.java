package com.bolocelta.entities;

public class Jugadores {

	private Integer id;
	private Integer equipoId;
	private Equipos equipo;
	private String nombre;
	private String apodo;
	private String numeroFicha;
	private String email;
	private String telefono;
	private Integer edad;
	private String activo;
	private Long rowNum;
	private String modalidad;
	private String individualPrimera;
	private String individualSegunda;
	private String individualTercera;
	private String individualFemenino;
	private boolean masculino;
	private boolean femenino;
	private String nombreEquipo;

	public Jugadores() {
		// TODO Auto-generated constructor stub
	}

	public Jugadores(Integer equipoId, String activo, String individualPrimera,
			String individualSegunda, String individualTercera, String individualFemenino) {
		this.equipoId = equipoId;
		this.activo = activo;
		this.individualPrimera = individualPrimera;
		this.individualSegunda = individualSegunda;
		this.individualTercera = individualTercera;
		this.individualFemenino = individualFemenino;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEquipoId() {
		return equipoId;
	}

	public void setEquipoId(Integer equipoId) {
		this.equipoId = equipoId;
	}

	public Equipos getEquipo() {
		return equipo;
	}

	public void setEquipo(Equipos equipo) {
		this.equipo = equipo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApodo() {
		return apodo;
	}

	public void setApodo(String apodo) {
		this.apodo = apodo;
	}

	public String getNumeroFicha() {
		return numeroFicha;
	}

	public void setNumeroFicha(String numeroFicha) {
		this.numeroFicha = numeroFicha;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}

	public String getActivo() {
		return activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	public Long getRowNum() {
		return rowNum;
	}

	public void setRowNum(Long rowNum) {
		this.rowNum = rowNum;
	}

	public String getModalidad() {
		return modalidad;
	}

	public void setModalidad(String modalidad) {
		this.modalidad = modalidad;
	}

	public String getIndividualPrimera() {
		return individualPrimera;
	}

	public void setIndividualPrimera(String individualPrimera) {
		this.individualPrimera = individualPrimera;
	}

	public String getIndividualSegunda() {
		return individualSegunda;
	}

	public void setIndividualSegunda(String individualSegunda) {
		this.individualSegunda = individualSegunda;
	}

	public String getIndividualTercera() {
		return individualTercera;
	}

	public void setIndividualTercera(String individualTercera) {
		this.individualTercera = individualTercera;
	}

	public String getIndividualFemenino() {
		return individualFemenino;
	}

	public void setIndividualFemenino(String individualFemenino) {
		this.individualFemenino = individualFemenino;
	}
	
	public String getApodoShow() {
		return apodo.toUpperCase();
	}

	public String getNombreShow() {
		String nombreShow = null;
		if (this.apodo != null && !this.apodo.isEmpty()) {
			nombreShow = this.apodo.toUpperCase();
		} else if (this.nombre != null && !this.nombre.isEmpty()) {
			nombreShow = this.nombre;
		}
		return nombreShow;
	}
	
	public String getNombreEquipo() {
		nombreEquipo = null;
		if (this.apodo != null && !this.apodo.isEmpty()) {
			nombreEquipo = this.apodo.toUpperCase();
		} else if (this.nombre != null && !this.nombre.isEmpty()) {
			nombreEquipo = this.nombre;
		}

		if (this.equipo != null && this.equipo.getNombre() != null && !this.equipo.getNombre().isEmpty()) {
			nombreEquipo += " (" + this.equipo.getNombre() + ")";
		}
		return nombreEquipo;
	}

	public String getNombreEquipoShow() {
		return getNombreEquipo();
	}
	
	public String getNombreEquipoShowCuadro() {
		String nombreEquipo = null;
		if (this.apodo != null && !this.apodo.isEmpty()) {
			nombreEquipo = this.apodo.toUpperCase();
		} else if (this.nombre != null && !this.nombre.isEmpty()) {
			nombreEquipo = this.nombre;
		}

		if (this.equipo != null && this.equipo.getNombre() != null && !this.equipo.getNombre().isEmpty()) {
			nombreEquipo += " # " + this.equipo.getNombre() + "";
			nombreEquipo += " # ";
		}
		return nombreEquipo;
	}
	

	public boolean isMasculino() {
		return masculino;
	}

	public void setMasculino(boolean masculino) {
		this.masculino = masculino;
	}

	public boolean isFemenino() {
		return femenino;
	}

	public void setFemenino(boolean femenino) {
		this.femenino = femenino;
	}
	
	public String getInsertRow(){
		return (this.rowNum.toString() + ";" +
				this.equipoId.toString() + ";" +
				this.nombre.toString() + ";" +
				this.apodo.toString() + ";" +
				this.numeroFicha.toString() + ";" +
				this.email.toString() + ";" +
				this.telefono.toString() + ";" +
				this.edad.toString() + ";" +
				this.activo.toString() + ";" +
				this.modalidad.toString() + ";" +
				this.individualPrimera.toString() + ";" +
				this.individualSegunda.toString() + ";" +
				this.individualTercera.toString() + ";" +
				this.individualFemenino.toString()
				);
	}
	
	@Override
	public int hashCode() {
		if(id == null){
			return 0;
		}
		return id;
	}

}
