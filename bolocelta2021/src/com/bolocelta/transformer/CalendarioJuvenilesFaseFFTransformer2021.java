package com.bolocelta.transformer;

import org.apache.commons.csv.CSVRecord;

import com.bolocelta.entities.sorteos.individual.CalendarioFaseFF2021;

public class CalendarioJuvenilesFaseFFTransformer2021 {

	
	public static CalendarioFaseFF2021 transformerObjectCsv(CalendarioFaseFF2021 calendarioFaseFF, CSVRecord row) {
		
        // Accessing Values by Column Index
        String col1 = row.get(0);
        String col2 = row.get(1);
        String col3 = row.get(2);
        String col4 = row.get(3);
        String col5 = row.get(4);
        String col6 = row.get(5);
        String col7 = row.get(6);
        String col8 = row.get(7);
        String col9 = row.get(8);
        String col10 = row.get(9);
        String col11 = row.get(10);
        String col12 = row.get(11);
        String col13 = row.get(12);
        String col14 = row.get(13);
        String col15 = row.get(14);
        String col16 = row.get(15);
        String col17 = row.get(16);
        String col18 = row.get(17);
        String col19 = row.get(18);
        String col20 = row.get(19);
        String col21 = row.get(20);
        String col22 = row.get(21);
        
		if (col1 != null && !col1.isEmpty()) {
			try {
				Double id = Double.valueOf(col1);
				calendarioFaseFF.setId(id.intValue());
//				System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				calendarioFaseFF.setId(null);
			}
		}
		if (col2 != null && !col2.isEmpty()) {
			try {
				Double id = Double.valueOf(col2);
				calendarioFaseFF.setCategoriaId(id.intValue());
//				System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				calendarioFaseFF.setCategoriaId(null);
			}
		} 
		if (col3 != null && !col3.isEmpty()) {
			calendarioFaseFF.setIdCruce(col3);
//			System.out.print(col3 + " | ");
		}
		if (col4 != null && !col4.isEmpty()) {
			try {
				Double id = Double.valueOf(col4);
				calendarioFaseFF.setNumeroEnfrentamiento(id.intValue());
//				System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				calendarioFaseFF.setNumeroEnfrentamiento(null);
			}
		}
		if (col5 != null && !col5.isEmpty()) {
			try {
				Double id = Double.valueOf(col5);
				calendarioFaseFF.setJugador1Id(id.intValue());
//				System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				calendarioFaseFF.setJugador1Id(null);
			}
		} 
		if (col6 != null && !col6.isEmpty()) {
			try {
				Double id = Double.valueOf(col6);
				calendarioFaseFF.setJuegosJugador1P1(id.intValue());
//				System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				calendarioFaseFF.setJuegosJugador1P1(null);
			}
		} 
		if (col7 != null && !col7.isEmpty()) {
			try {
				Double id = Double.valueOf(col7);
				calendarioFaseFF.setJuegosJugador1P2(id.intValue());
//				System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				calendarioFaseFF.setJuegosJugador1P2(null);
			}
		} 
		if (col8 != null && !col8.isEmpty()) {
			try {
				Double id = Double.valueOf(col8);
				calendarioFaseFF.setJuegosJugador1P3(id.intValue());
//				System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				calendarioFaseFF.setJuegosJugador1P3(null);
			}
		}
		if (col9 != null && !col9.isEmpty()) {
			try {
				Double id = Double.valueOf(col9);
				calendarioFaseFF.setJuegosJugador2P1(id.intValue());
//				System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				calendarioFaseFF.setJuegosJugador2P1(null);
			}
		} 
		if (col10 != null && !col10.isEmpty()) {
			try {
				Double id = Double.valueOf(col10);
				calendarioFaseFF.setJuegosJugador2P2(id.intValue());
//				System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				calendarioFaseFF.setJuegosJugador2P2(null);
			}
		} 
		if (col11 != null && !col11.isEmpty()) {
			try {
				Double id = Double.valueOf(col11);
				calendarioFaseFF.setJuegosJugador2P3(id.intValue());
//				System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				calendarioFaseFF.setJuegosJugador2P3(null);
			}
		}
		if (col12 != null && !col12.isEmpty()) {
			try {
				Double id = Double.valueOf(col12);
				calendarioFaseFF.setJugador2Id(id.intValue());
//				System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				calendarioFaseFF.setJugador2Id(null);
			}
		} 
		if (col13 != null && !col13.isEmpty()) {
			try {
				Double id = Double.valueOf(col13);
				calendarioFaseFF.setOrden(id.intValue());
//				System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				calendarioFaseFF.setOrden(null);
			}
		} 
		if (col14 != null && !col14.isEmpty()) {
			calendarioFaseFF.setCruceCF(col14);
//			System.out.print(col14 + " | ");
		}
		if (col15 != null && !col15.isEmpty()) {
			calendarioFaseFF.setActivo(col15);
//			System.out.print(col15 + " | ");
		}
		if (col16 != null && !col16.isEmpty()) {
			calendarioFaseFF.setInicia(col16);
//			System.out.print(col16 + " | ");
		}
		if (col17 != null && !col17.isEmpty()) {
			try {
				Double id = Double.valueOf(col17);
				calendarioFaseFF.setFaseAnterior(id.intValue());
//				System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				calendarioFaseFF.setFaseAnterior(null);
			}
		}
		if (col18 != null && !col18.isEmpty()) {
			calendarioFaseFF.setGrupoProcedenciaJugador1(col18);
//			System.out.print(col18 + " | ");
		}
		if (col19 != null && !col19.isEmpty()) {
			calendarioFaseFF.setGrupoProcedenciaJugador2(col19);
//			System.out.print(col19 + " | ");
		}
		if (col20 != null && !col20.isEmpty()) {
			try {
				Double id = Double.valueOf(col20);
				calendarioFaseFF.setPosicionProcedenciaJugador1(id.intValue());
//				System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				calendarioFaseFF.setPosicionProcedenciaJugador1(null);
			}
		}
		if (col21 != null && !col21.isEmpty()) {
			try {
				Double id = Double.valueOf(col21);
				calendarioFaseFF.setPosicionProcedenciaJugador2(id.intValue());
//				System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				calendarioFaseFF.setPosicionProcedenciaJugador2(null);
			}
		}
		if (col22 != null && !col22.isEmpty()) {
			try {
				Double id = Double.valueOf(col22);
				calendarioFaseFF.setBoleraId(id.intValue());
//				System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				calendarioFaseFF.setBoleraId(null);
			}
		}
		return calendarioFaseFF;
	}

}
