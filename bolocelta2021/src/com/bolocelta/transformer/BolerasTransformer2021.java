package com.bolocelta.transformer;

import org.apache.commons.csv.CSVRecord;
import org.apache.poi.ss.usermodel.Cell;

import com.bolocelta.entities.Boleras2021;

public class BolerasTransformer2021 {

	public static Boleras2021 transformerObject(Boleras2021 bolera, Cell cell) {

		if (cell.getColumnIndex() == 0) {
			Double id = cell.getNumericCellValue();
			bolera.setId(id.intValue());
//			System.out.print(id.intValue() + " | ");
		} else if (cell.getColumnIndex() == 1) {
			bolera.setBolera(cell.getStringCellValue());
//			System.out.print(cell.getStringCellValue() + " | ");
		} else if (cell.getColumnIndex() == 2) {
			bolera.setLocalizacion(cell.getStringCellValue());
//			System.out.print(cell.getStringCellValue() + " | ");
		} else if (cell.getColumnIndex() == 3) {
			bolera.setFederada(cell.getStringCellValue());
//			System.out.print(cell.getStringCellValue() + " | ");
		}

		return bolera;
	}
	
	public static Boleras2021 transformerObjectCsv(Boleras2021 bolera, CSVRecord row) {
		
        // Accessing Values by Column Index
        String col1 = row.get(0);
        String col2 = row.get(1);
        String col3 = row.get(2);
        String col4 = row.get(3);
        String col5 = row.get(4);

		if (col1 != null && !col1.isEmpty()) {
			Double id = Double.valueOf(col1);
			bolera.setId(id.intValue());
//			System.out.print(id.intValue() + " | ");
		} 
		if (col2 != null && !col2.isEmpty()) {
			bolera.setBolera(col2);
//			System.out.print(col2 + " | ");
		} 
		if (col3 != null && !col3.isEmpty()) {
			bolera.setLocalizacion(col3);
//			System.out.print(col3 + " | ");
		} 
		if (col4 != null && !col4.isEmpty()) {
			bolera.setFederada(col4);
//			System.out.print(col4 + " | ");
		}
		if (col5 != null && !col5.isEmpty()) {
			bolera.setUbicacion(col5);
//			System.out.print(col5 + " | ");
		}

		return bolera;
	}

}
