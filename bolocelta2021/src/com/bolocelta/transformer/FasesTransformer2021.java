package com.bolocelta.transformer;

import org.apache.commons.csv.CSVRecord;
import org.apache.poi.ss.usermodel.Cell;

import com.bolocelta.entities.Fases2021;

public class FasesTransformer2021 {

	public static Fases2021 transformerObject(Fases2021 fases, Cell cell) {

		if (cell.getColumnIndex() == 0) {
			try {
				Double id = cell.getNumericCellValue();
				fases.setId(id.intValue());
				//System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				try {
					fases.setId(Integer.valueOf(cell.getStringCellValue()));
					//System.out.print(cell.getStringCellValue() + " | ");
				}catch (Exception e1) {
					fases.setId(null);
				}
			}
		} else if (cell.getColumnIndex() == 1) {
			try {
				Double id = cell.getNumericCellValue();
				fases.setNumero(id.intValue());
				//System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				try {
					fases.setNumero(Integer.valueOf(cell.getStringCellValue()));
					//System.out.print(cell.getStringCellValue() + " | ");
				}catch (Exception e1) {
					fases.setNumero(null);
				}
			}
		} else if (cell.getColumnIndex() == 2) {
			fases.setNombre(cell.getStringCellValue());
			//System.out.print(cell.getStringCellValue() + " | ");
		} else if (cell.getColumnIndex() == 3) {
			fases.setEstado(cell.getStringCellValue());
			//System.out.print(cell.getStringCellValue() + " | ");
		} else if (cell.getColumnIndex() == 4) {
			fases.setActivo(cell.getStringCellValue());
			//System.out.print(cell.getStringCellValue() + " | ");
		}

		return fases;
	}
	
	public static Fases2021 transformerObjectCsv(Fases2021 fases, CSVRecord row) {
		
        // Accessing Values by Column Index
        String col1 = row.get(0);
        String col2 = row.get(1);
        String col3 = row.get(2);
        String col4 = row.get(3);
        String col5 = row.get(4);
        String col6 = row.get(5);
        String col7 = row.get(6);
        String col8 = row.get(7);
        String col9 = row.get(8);
        String col10 = row.get(9);
        String col11 = row.get(10);
        String col12 = row.get(11);
        String col13 = row.get(12);

		if (col1 != null && !col1.isEmpty()) {
			try {
				Double id = Double.valueOf(col1);
				fases.setId(id.intValue());
				//System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				fases.setId(null);
			}
		}
		if (col2 != null && !col2.isEmpty()) {
			try {
				Double id = Double.valueOf(col2);
				fases.setNumero(id.intValue());
				//System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				fases.setNumero(null);
			}
		} 
		if (col3 != null && !col3.isEmpty()) {
			fases.setNombre(col3);
			//System.out.print(col3 + " | ");
		}
		if (col4 != null && !col4.isEmpty()) {
			fases.setTipoEnfrentamiento(col4);
			//System.out.print(col4 + " | ");
		}
		if (col5 != null && !col5.isEmpty()) {
			try {
				Double id = Double.valueOf(col5);
				fases.setNumeroEnfrentamientos(id.intValue());
				//System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				fases.setNumero(null);
			}
		} 
		if (col6 != null && !col6.isEmpty()) {
			try {
				Double id = Double.valueOf(col6);
				fases.setNumeroPartidas(id.intValue());
				//System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				fases.setNumero(null);
			}
		} 
		if (col7 != null && !col7.isEmpty()) {
			try {
				Double id = Double.valueOf(col7);
				fases.setNumeroJuegos(id.intValue());
				//System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				fases.setNumero(null);
			}
		} 
		if (col8 != null && !col8.isEmpty()) {
			try {
				Double id = Double.valueOf(col8);
				fases.setClasifican(id.intValue());
				//System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				fases.setNumero(null);
			}
		}
		if (col9 != null && !col9.isEmpty()) {
			fases.setFecha(col9);
			//System.out.print(col9 + " | ");
		}
		if (col10 != null && !col10.isEmpty()) {
			fases.setHora(col10);
			//System.out.print(col10 + " | ");
		}
		if (col11 != null && !col11.isEmpty()) {
			fases.setEstado(col11);
			//System.out.print(col11 + " | ");
		}
		if (col12 != null && !col12.isEmpty()) {
			fases.setActivo(col12);
			//System.out.print(col12 + " | ");
		}
		if (col13 != null && !col13.isEmpty()) {
			try {
				Double id = Double.valueOf(col13);
				fases.setFaseSiguiente(id.intValue());
				//System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				fases.setFaseSiguiente(null);
			}
		}
		return fases;
	}

}
