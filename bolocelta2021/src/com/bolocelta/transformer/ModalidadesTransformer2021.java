package com.bolocelta.transformer;

import org.apache.commons.csv.CSVRecord;
import org.apache.poi.ss.usermodel.Cell;

import com.bolocelta.entities.Modalidades2021;

public class ModalidadesTransformer2021 {

	public static Modalidades2021 transformerObject(Modalidades2021 modalidad, Cell cell) {

		if (cell.getColumnIndex() == 0) {
			Double id = cell.getNumericCellValue();
			modalidad.setId(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} else if (cell.getColumnIndex() == 1) {
			modalidad.setModalidad(cell.getStringCellValue());
			//System.out.print(cell.getStringCellValue() + " | ");
		}

		return modalidad;
	}
	
	public static Modalidades2021 transformerObjectCsv(Modalidades2021 modalidad, CSVRecord row) {
		
        // Accessing Values by Column Index
        String col1 = row.get(0);
        String col2 = row.get(1);

		if (col1 != null && !col1.isEmpty()) {
			Double id = Double.valueOf(col1);
			modalidad.setId(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col2 != null && !col2.isEmpty()) {
			modalidad.setModalidad(col2);
			//System.out.print(col2 + " | ");
		} 

		return modalidad;
	}

}
