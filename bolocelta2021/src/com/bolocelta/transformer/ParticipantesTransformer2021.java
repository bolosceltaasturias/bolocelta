package com.bolocelta.transformer;

import org.apache.commons.csv.CSVRecord;

import com.bolocelta.entities.ParticipantesIndividual2021;
import com.bolocelta.entities.ParticipantesParejas2021;

public class ParticipantesTransformer2021 {

	public static ParticipantesIndividual2021 transformerObjectCsv(ParticipantesIndividual2021 participantesIndividual, CSVRecord row) {
		
        // Accessing Values by Column Index
        String col1 = row.get(0);
        String col2 = row.get(1);
        String col3 = row.get(2);
        String col4 = row.get(3);
        String col5 = row.get(4);

		if (col1 != null && !col1.isEmpty()) {
			try {
				Double id = Double.valueOf(col1);
				participantesIndividual.setId(id.intValue());
				//System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				participantesIndividual.setId(null);
			}
		}
		if (col2 != null && !col2.isEmpty()) {
			try {
				Double id = Double.valueOf(col2);
				participantesIndividual.setIdJugador(id.intValue());
				//System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				participantesIndividual.setIdJugador(null);
			}
		} 
		if (col3 != null && !col3.isEmpty()) {
			participantesIndividual.setFechaInscripcion(col3);
			//System.out.print(col3 + " | ");
		}
		if (col4 != null && !col4.isEmpty()) {
			participantesIndividual.setUsuarioInscripcion(col4);
			//System.out.print(col4 + " | ");
		}
		if (col5 != null && !col5.isEmpty()) {
			participantesIndividual.setActivo(col5);
			//System.out.print(col5 + " | ");
		}
		return participantesIndividual;
	}
	
	public static ParticipantesParejas2021 transformerObjectCsv(ParticipantesParejas2021 participantesParejas, CSVRecord row) {
		
        // Accessing Values by Column Index
        String col1 = row.get(0);
        String col2 = row.get(1);
        String col3 = row.get(2);
        String col4 = row.get(3);
        String col5 = row.get(4);
        String col6 = row.get(5);

		if (col1 != null && !col1.isEmpty()) {
			try {
				Double id = Double.valueOf(col1);
				participantesParejas.getPareja().setId(id.intValue());
				//System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				participantesParejas.getPareja().setId(null);
			}
		}
		if (col2 != null && !col2.isEmpty()) {
			try {
				Double id = Double.valueOf(col2);
				participantesParejas.getPareja().setIdJugador1(id.intValue());
				//System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				participantesParejas.getPareja().setIdJugador1(null);
			}
		} 
		if (col3 != null && !col3.isEmpty()) {
			try {
				Double id = Double.valueOf(col3);
				participantesParejas.getPareja().setIdJugador2(id.intValue());
				//System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				participantesParejas.getPareja().setIdJugador2(null);
			}
		} 
		
		if (col4 != null && !col4.isEmpty()) {
			participantesParejas.setFechaInscripcion(col4);
			//System.out.print(col3 + " | ");
		}
		if (col5 != null && !col5.isEmpty()) {
			participantesParejas.setUsuarioInscripcion(col5);
			//System.out.print(col4 + " | ");
		}
		if (col6 != null && !col6.isEmpty()) {
			participantesParejas.setActivo(col6);
			//System.out.print(col5 + " | ");
		}
		return participantesParejas;
	}

}
