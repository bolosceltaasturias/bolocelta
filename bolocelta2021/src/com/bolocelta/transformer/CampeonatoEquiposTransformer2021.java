package com.bolocelta.transformer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.csv.CSVRecord;
import org.apache.poi.ss.usermodel.Cell;

import com.bolocelta.entities.CampeonatoEquiposCalendario2021;
import com.bolocelta.entities.CampeonatoEquiposClasificacion2021;

public class CampeonatoEquiposTransformer2021 {

	public static CampeonatoEquiposClasificacion2021 transformerObjectClasificacion(CampeonatoEquiposClasificacion2021 cec, Cell cell) {

		if (cell.getColumnIndex() == 0) {
			Double value = cell.getNumericCellValue();
			cec.setId(value.intValue());
			//System.out.print(value.intValue() + " | ");
		} else if (cell.getColumnIndex() == 1) {
			Double value = cell.getNumericCellValue();
			cec.setCategoriaId(value.intValue());
			//System.out.print(value.intValue() + " | ");
		} else if (cell.getColumnIndex() == 2) {
			Double value = cell.getNumericCellValue();
			cec.setEquipoId(value.intValue());
			//System.out.print(value.intValue() + " | ");
		} else if (cell.getColumnIndex() == 3) {
			Double value = cell.getNumericCellValue();
			cec.setJugados(value.intValue());
			//System.out.print(value.intValue() + " | ");
		} else if (cell.getColumnIndex() == 4) {
			Double value = cell.getNumericCellValue();
			cec.setGanados(value.intValue());
			//System.out.print(value.intValue() + " | ");
		} else if (cell.getColumnIndex() == 5) {
			Double value = cell.getNumericCellValue();
			cec.setEmpatados(value.intValue());
			//System.out.print(value.intValue() + " | ");
		} else if (cell.getColumnIndex() == 6) {
			Double value = cell.getNumericCellValue();
			cec.setPerdidos(value.intValue());
			//System.out.print(value.intValue() + " | ");
		} else if (cell.getColumnIndex() == 7) {
			Double value = cell.getNumericCellValue();
			cec.setPartidasFavor(value.intValue());
			//System.out.print(value.intValue() + " | ");
		} else if (cell.getColumnIndex() == 8) {
			Double value = cell.getNumericCellValue();
			cec.setPartidasContra(value.intValue());
			//System.out.print(value.intValue() + " | ");
		} else if (cell.getColumnIndex() == 9) {
			Double value = cell.getNumericCellValue();
			cec.setPuntos(value.intValue());
			//System.out.print(value.intValue() + " | ");
		}

		return cec;
	}
	
	public static CampeonatoEquiposClasificacion2021 transformerObjectClasificacionCsv(CampeonatoEquiposClasificacion2021 cec, CSVRecord row) {
		
        // Accessing Values by Column Index
        String col1 = row.get(0);
        String col2 = row.get(1);
        String col3 = row.get(2);
        String col4 = row.get(3);
        String col5 = row.get(4);
        String col6 = row.get(5);
        String col7 = row.get(6);
        String col8 = row.get(7);
        String col9 = row.get(8);
        String col10 = row.get(9);

		if (col1 != null && !col1.isEmpty()) {
			Double id = Double.valueOf(col1);
			cec.setId(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col2 != null && !col2.isEmpty()) {
			Double id = Double.valueOf(col2);
			cec.setCategoriaId(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col3 != null && !col3.isEmpty()) {
			Double id = Double.valueOf(col3);
			cec.setEquipoId(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col4 != null && !col4.isEmpty()) {
			Double id = Double.valueOf(col4);
			cec.setJugados(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col5 != null && !col5.isEmpty()) {
			Double id = Double.valueOf(col5);
			cec.setGanados(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col6 != null && !col6.isEmpty()) {
			Double id = Double.valueOf(col6);
			cec.setEmpatados(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col7 != null && !col7.isEmpty()) {
			Double id = Double.valueOf(col7);
			cec.setPerdidos(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col8 != null && !col8.isEmpty()) {
			Double id = Double.valueOf(col8);
			cec.setPartidasFavor(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col9 != null && !col9.isEmpty()) {
			Double id = Double.valueOf(col9);
			cec.setPartidasContra(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col10 != null && !col10.isEmpty()) {
			Double id = Double.valueOf(col10);
			cec.setPuntos(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		return cec;
	}
	
	public static CampeonatoEquiposCalendario2021 transformerObjectCalendario(CampeonatoEquiposCalendario2021 cec, Cell cell) {

		if (cell.getColumnIndex() == 0) {
			Double value = cell.getNumericCellValue();
			cec.setId(value.intValue());
			//System.out.print(value.intValue() + " | ");
		} else if (cell.getColumnIndex() == 1) {
			Double value = cell.getNumericCellValue();
			cec.setCategoriaId(value.intValue());
			//System.out.print(value.intValue() + " | ");
		} else if (cell.getColumnIndex() == 2) {
			Double value = cell.getNumericCellValue();
			cec.setJornada(value.intValue());
			//System.out.print(value.intValue() + " | ");
		} else if (cell.getColumnIndex() == 3) {
			cec.setFecha(cell.getStringCellValue());
			//System.out.print(cell.getStringCellValue() + " | ");
		} else if (cell.getColumnIndex() == 4) {
			cec.setHora(cell.getStringCellValue());
			//System.out.print(cell.getStringCellValue() + " | ");
		} else if (cell.getColumnIndex() == 5) {
			Double value = cell.getNumericCellValue();
			cec.setPgequipo1(value.intValue());
			//System.out.print(value.intValue() + " | ");
		} else if (cell.getColumnIndex() == 6) {
			Double value = cell.getNumericCellValue();
			cec.setEquipo1Id(value.intValue());
			//System.out.print(value.intValue() + " | ");
		} else if (cell.getColumnIndex() == 7) {
			Double value = cell.getNumericCellValue();
			cec.setPgequipo2(value.intValue());
			//System.out.print(value.intValue() + " | ");
		} else if (cell.getColumnIndex() == 8) {
			Double value = cell.getNumericCellValue();
			cec.setEquipo2Id(value.intValue());
			//System.out.print(value.intValue() + " | ");
		} else if (cell.getColumnIndex() == 9) {
			Double value = cell.getNumericCellValue();
			cec.setActivo(value.intValue());
			//System.out.print(value.intValue() + " | ");
		}

		return cec;
	}
	
	public static CampeonatoEquiposCalendario2021 transformerObjectCalendarioCsv(CampeonatoEquiposCalendario2021 cec, CSVRecord row) {
		
        // Accessing Values by Column Index
        String col1 = row.get(0);
        String col2 = row.get(1);
        String col3 = row.get(2);
        String col4 = row.get(3);
        String col5 = row.get(4);
        String col6 = row.get(5);
        String col7 = row.get(6);
        String col8 = row.get(7);
        String col9 = row.get(8);
        String col10 = row.get(9);
        String col11 = row.get(10);
        String col12 = row.get(11);

		if (col1 != null && !col1.isEmpty()) {
			Double id = Double.valueOf(col1);
			cec.setId(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col2 != null && !col2.isEmpty()) {
			Double id = Double.valueOf(col2);
			cec.setCategoriaId(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col3 != null && !col3.isEmpty()) {
			Double id = Double.valueOf(col3);
			cec.setJornada(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col4 != null && !col4.isEmpty()) {
			cec.setFecha(col4);
			//System.out.print(col4 + " | ");
		} 
		if (col5 != null && !col5.isEmpty()) {
			cec.setHora(col5);
			//System.out.print(col5 + " | ");
		} 
		if (col6 != null && !col6.isEmpty()) {
			Double id = Double.valueOf(col6);
			cec.setPgequipo1(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col7 != null && !col7.isEmpty()) {
			Double id = Double.valueOf(col7);
			cec.setEquipo1Id(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col8 != null && !col8.isEmpty()) {
			Double id = Double.valueOf(col8);
			cec.setPgequipo2(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col9 != null && !col9.isEmpty()) {
			Double id = Double.valueOf(col9);
			cec.setEquipo2Id(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col10 != null && !col10.isEmpty()) {
			Double id = Double.valueOf(col10);
			cec.setActivo(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col11 != null && !col11.isEmpty()) {
			Double id = Double.valueOf(col11);
			cec.setIdVuelta(id.intValue());
			//System.out.print(id.intValue() + " | ");
		}
		if (col12 != null && !col12.isEmpty()) {
			cec.setFoto(col12);
			//System.out.print(id.intValue() + " | ");
		}  
		return cec;
	}

}
