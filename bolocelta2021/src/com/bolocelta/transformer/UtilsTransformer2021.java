package com.bolocelta.transformer;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;

import com.bolocelta.bbdd.constants.NombresTablas2021;

public class UtilsTransformer2021 {
	
	public static String getNativeValueType(Cell cell){
		final int type = cell.getCellType();
		if (type == Cell.CELL_TYPE_BLANK) {
			return null;
		} else if (type == Cell.CELL_TYPE_STRING) {
			return NombresTablas2021.TD_TEXTO;
		} else if (type == Cell.CELL_TYPE_NUMERIC) {
			if (DateUtil.isCellDateFormatted(cell)) {
				return NombresTablas2021.TD_FECHA;
			} else {
				return NombresTablas2021.TD_DECIMAL;
			}
		} else if (type == Cell.CELL_TYPE_FORMULA) {
			if (cell.getCachedFormulaResultType() == Cell.CELL_TYPE_NUMERIC) {
				return NombresTablas2021.TD_DECIMAL;
			} else {
				return NombresTablas2021.TD_TEXTO;
			}
		} else {
			return NombresTablas2021.TD_TEXTO;
		}
	}

}
