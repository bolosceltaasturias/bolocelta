package com.bolocelta.bbdd.createTable;

import com.bolocelta.bbdd.constants.NombresTablas2021;
import com.bolocelta.bbdd.constants.Ubicaciones2021;
import com.bolocelta.bbdd.constants.structure.Estructura2021;
import com.bolocelta.bbdd.constants.structure.EstructuraCampeonatoLigaInfantil2021;
import com.bolocelta.entities.CampeonatoLigaInfantilCalendario2021;

public class CrearCampeonatoLigaInfantil2021 extends ACrearModificar2021 {

	public void actualizarResultadoCalendarioLiga(CampeonatoLigaInfantilCalendario2021 cec) {
		Long fila = cec.getRowNum();
		
		EstructuraCampeonatoLigaInfantil2021 eclf = new EstructuraCampeonatoLigaInfantil2021(
				NombresTablas2021.N_CSV_BBDD_CAMPEONATO_CAL_LIGA_INFANTIL);
		for (Estructura2021 ef : eclf.getCalendarioList()) {
			if (ef.getValor().equals(eclf.COL_CALENDARIO_RONDA1)) {
				updateRowCalendario(ef, fila, cec.getRonda1(), cec);
			}else if (ef.getValor().equals(eclf.COL_CALENDARIO_RONDA2)) {
				updateRowCalendario(ef, fila, cec.getRonda2(), cec);
			}else if (ef.getValor().equals(eclf.COL_CALENDARIO_RONDA3)) {
				updateRowCalendario(ef, fila, cec.getRonda3(), cec);
			}else if (ef.getValor().equals(eclf.COL_CALENDARIO_RONDA4)) {
				updateRowCalendario(ef, fila, cec.getRonda4(), cec);
			}else if (ef.getValor().equals(eclf.COL_CALENDARIO_PUNTOS)) {
				updateRowCalendario(ef, fila, cec.getPuntos(), cec);
			}
		}
	
		
	}

	public void updateRowCalendario(Estructura2021 update, Long fila, Object newValue, CampeonatoLigaInfantilCalendario2021 cec) {
		updateDataCsv(update.getExcel(), Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_EQUIPOS, fila, update.getColumna(), newValue, update.getTipoDato());
	}
	
	public void actualizarResultadoCalendarioLigaConfirmar(CampeonatoLigaInfantilCalendario2021 cec) {
		Long fila = cec.getRowNum();
		EstructuraCampeonatoLigaInfantil2021 ecl1 = new EstructuraCampeonatoLigaInfantil2021(
				NombresTablas2021.N_CSV_BBDD_CAMPEONATO_CAL_LIGA_INFANTIL);
		for (Estructura2021 ef : ecl1.getCalendarioList()) {
			if (ef.getValor().equals(ecl1.COL_CALENDARIO_ACTIVO)) {
				updateRowCalendario(ef, fila, cec.getActivo(), cec);
			}
		}
	}
	

}
