package com.bolocelta.bbdd.createTable;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import com.bolocelta.bbdd.constants.Activo2021;
import com.bolocelta.bbdd.constants.HorarioJornadas2021;
import com.bolocelta.bbdd.constants.Modalidad2021;
import com.bolocelta.bbdd.constants.NombresTablas2021;
import com.bolocelta.bbdd.constants.Ubicaciones2021;
import com.bolocelta.bbdd.constants.structure.Estructura2021;
import com.bolocelta.bbdd.constants.structure.EstructuraCampeonatoLiga2021;
import com.bolocelta.bbdd.readTables.LeerBoleras2021;
import com.bolocelta.bbdd.readTables.LeerCalendarioAsturias2021;
import com.bolocelta.bbdd.readTables.LeerCampeonatoEquiposPrimera2021;
import com.bolocelta.bbdd.readTables.LeerCampeonatoEquiposSegunda2021;
import com.bolocelta.bbdd.readTables.LeerCampeonatoEquiposTercera2021;
import com.bolocelta.bbdd.readTables.LeerCategorias2021;
import com.bolocelta.bbdd.readTables.LeerEquipos2021;
import com.bolocelta.bbdd.readTables.LeerSorteoBolerasCrucesLiga2021;
import com.bolocelta.bbdd.readTables.LeerSorteoTuplaEnfrentamientosLiga2021;
import com.bolocelta.entities.Boleras2021;
import com.bolocelta.entities.CalendarioAsturias2021;
import com.bolocelta.entities.CampeonatoEquiposCalendario2021;
import com.bolocelta.entities.CampeonatoEquiposClasificacion2021;
import com.bolocelta.entities.Categorias2021;
import com.bolocelta.entities.Equipos2021;
import com.bolocelta.entities.JornadaCategoria2021;
import com.bolocelta.entities.Jugadores2021;
import com.bolocelta.entities.sorteos.liga.SorteoBolerasCrucesLiga2021;
import com.bolocelta.entities.sorteos.liga.SorteoBolerasEquipos2021;
import com.bolocelta.entities.sorteos.liga.SorteoCategoria2021;
import com.bolocelta.entities.sorteos.liga.SorteoEnfrentamientosCategoria2021;
import com.bolocelta.entities.sorteos.liga.SorteoJornadasCategoria2021;
import com.bolocelta.entities.sorteos.liga.SorteoLiga2021;
import com.bolocelta.entities.sorteos.liga.SorteoTuplaEnfrentamientosLiga2021;

public class CrearCampeonatoLiga2021 extends ACrearModificar2021 {

	private void crearCsv() {
		createWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CAL_PRIMERA, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_EQUIPOS);
		createWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CLA_PRIMERA, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_EQUIPOS);
		createWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CAL_SEGUNDA, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_EQUIPOS);
		createWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CLA_SEGUNDA, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_EQUIPOS);
		createWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CAL_TERCERA, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_EQUIPOS);
		createWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CLA_TERCERA, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_EQUIPOS);
	}
	
	public void borrarSorteo(){
		deleteFile(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CAL_PRIMERA, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_EQUIPOS);
		deleteFile(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CLA_PRIMERA, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_EQUIPOS);
		deleteFile(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CAL_SEGUNDA, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_EQUIPOS);
		deleteFile(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CLA_SEGUNDA, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_EQUIPOS);
		deleteFile(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CAL_TERCERA, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_EQUIPOS);
		deleteFile(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CLA_TERCERA, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_EQUIPOS);
	}	
	
	public void realizarSorteo(){
		
		try {
			
			if(!existFile(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CLA_PRIMERA, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_EQUIPOS)){
		
				//Crear los ficheros
				crearCsv();
				//Inserta las cabeceras
				EstructuraCampeonatoLiga2021 ecl1 = new EstructuraCampeonatoLiga2021(
						NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CAL_PRIMERA,
						NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CLA_PRIMERA);
				EstructuraCampeonatoLiga2021 ecl2 = new EstructuraCampeonatoLiga2021(
						NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CAL_SEGUNDA,
						NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CLA_SEGUNDA);
				EstructuraCampeonatoLiga2021 ecl3 = new EstructuraCampeonatoLiga2021(
						NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CAL_TERCERA,
						NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CLA_TERCERA);
				//Falta insertar las cabeceras
				preparateInsertRowCabecera(ecl1, ecl2, ecl3);
				//Realiza sorteo
				//Se han de preparar los datos del Sorteo
				//1. Recuperar las jornadas del calendario de asturias
				LeerCalendarioAsturias2021 leerCalendarioAsturias = new LeerCalendarioAsturias2021();
				List<CalendarioAsturias2021> resultListJornadas = (List<CalendarioAsturias2021>) leerCalendarioAsturias.listResultLiga();
				//2. Recuperar los equipos por categoria
				//3. Recuperar los equipos por bolera
				LeerEquipos2021 leerEquipos = new LeerEquipos2021();
				List<Equipos2021> resultListEquipos = (List<Equipos2021>) leerEquipos.listResultLiga();
				//4. Recuperar las categorias
				LeerCategorias2021 leerCategorias = new LeerCategorias2021();
				List<Categorias2021> resultListCategorias = (List<Categorias2021>) leerCategorias.listResultLiga();
				//4. Recuperar las boleras
				LeerBoleras2021 leerBoleras = new LeerBoleras2021();
				List<Boleras2021> resultListBoleras = (List<Boleras2021>) leerBoleras.listResult();
				//5. Recuperar las boleras por cruce
				LeerSorteoBolerasCrucesLiga2021 leerSorteoBolerasCrucesLiga = new LeerSorteoBolerasCrucesLiga2021();
				List<SorteoBolerasCrucesLiga2021> resultListSorteoBolerasCrucesLiga = (List<SorteoBolerasCrucesLiga2021>) leerSorteoBolerasCrucesLiga.listResult();
				//6. Recuperar las boleras por cruce
				LeerSorteoTuplaEnfrentamientosLiga2021 leerSorteoTuplaEnfrentamientosLiga = new LeerSorteoTuplaEnfrentamientosLiga2021();
				List<SorteoTuplaEnfrentamientosLiga2021> resultListSorteoTuplaEnfrentamientosLiga = (List<SorteoTuplaEnfrentamientosLiga2021>) leerSorteoTuplaEnfrentamientosLiga.listResult();
				
				
				
				
				//Prepara datos para sorteo
				SorteoLiga2021 sorteoLiga = prepareDataSorteoLiga(new SorteoLiga2021(), resultListCategorias, resultListEquipos, 
						resultListBoleras, resultListJornadas, resultListSorteoBolerasCrucesLiga, 
						resultListSorteoTuplaEnfrentamientosLiga);
				//Con los datos preparados se realiza el sorteo automatico
				//Primer se han de recuperar las jornadas
				sorteoLiga = prepararCalendario(sorteoLiga, resultListJornadas);
				//Insertar calendario por categoria
				for (Entry<String, ?> entryEnfrentamientosJornadaAsignados : sorteoLiga.getEnfrentamientosJornadaAsignados().entrySet()) {
					Object valueEnfrentamientosJornadaAsignados = entryEnfrentamientosJornadaAsignados.getValue();
					CampeonatoEquiposCalendario2021 cec = (CampeonatoEquiposCalendario2021) valueEnfrentamientosJornadaAsignados;
					EstructuraCampeonatoLiga2021 ecl = new EstructuraCampeonatoLiga2021();
					preparateInsertRow(ecl, cec);
				}
				//Insertar clasificacion para equipo por categoria
				for (Entry<Integer, ?> entrySorteoCategoria : sorteoLiga.getSorteoCategoriasMap().entrySet()) {
					Object valueSorteoCategoria = entrySorteoCategoria.getValue();
					SorteoCategoria2021 sorteoCategoria = (SorteoCategoria2021) valueSorteoCategoria;
					Integer id = 1;
					for (Entry<Integer, ?> entryEquiposPorCategoria : sorteoCategoria.getEquiposPorCategoria().entrySet()) {
						Object valueEquiposPorCategoria = entryEquiposPorCategoria.getValue();
						Equipos2021 equipo = (Equipos2021) valueEquiposPorCategoria;
						//Crear el equipo para la clasificacion de la categoria
						CampeonatoEquiposClasificacion2021 cec = new CampeonatoEquiposClasificacion2021();
						cec.setCategoria(equipo.getCategoria());
						cec.setCategoriaId(equipo.getCategoriaId());
						cec.setEmpatados(0);
						cec.setEquipo(equipo);
						cec.setEquipoId(equipo.getId());
						cec.setGanados(0);
						cec.setId(id);
						cec.setJugados(0);
						cec.setPartidasContra(0);
						cec.setPartidasFavor(0);
						cec.setPerdidos(0);
						cec.setPuntos(0);
						id++;
						EstructuraCampeonatoLiga2021 ecl = new EstructuraCampeonatoLiga2021();
						preparateInsertRow(ecl, cec);
					}
				}
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "El sorteo de liga se ha realizado correctamente. Se puede consultar en cada categoria.", null));
			}else{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "No se puede realizar el sorteo de liga, ya existe.", null));
			}
		} catch (Exception e) {
			e.printStackTrace();
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Se ha producido un error realizando el sorteo del campeonato de liga.", null));
		}
		
		
	}

	private SorteoLiga2021 prepararCalendario(SorteoLiga2021 sorteoLiga, List<CalendarioAsturias2021> resultListJornadas) {

		// Prepraracion del calendario
//		System.out.println();
		// Enfrentamientos de la categoria libres
		HashMap<String, CampeonatoEquiposCalendario2021> enfrentamientosJornadaLibres = new HashMap<>();
		// Jornadas en las que ya participa el equipo (id = equipo_jornada)
		LinkedHashMap<String, String> jornadasPorEquipoAsignadas = new LinkedHashMap<>();
		// Enfrentamientos jornada asignados
		LinkedHashMap<String, CampeonatoEquiposCalendario2021> enfrentamientosJornadaAsignados = new LinkedHashMap<>();
		// Extraer las jornadas libres
		enfrentamientosJornadaLibres = extraerEnfrentamientosJornadasLibres(sorteoLiga, enfrentamientosJornadaLibres);
		
		//Desordenar la tupla de enfrentamientos para que sean aleatorios
		LinkedHashMap<Integer, SorteoTuplaEnfrentamientosLiga2021> sorteoTuplaEnfrentamientosLigaMapDesordenado = new LinkedHashMap<>();
		Object[] sorteoTuplaEnfrentamientosLigaKeys = sorteoLiga.getSorteoTuplaEnfrentamientosLigaMap().keySet().toArray();
		while(sorteoTuplaEnfrentamientosLigaMapDesordenado.size() < sorteoLiga.getSorteoTuplaEnfrentamientosLigaMap().size()){
			// Get a random entry from the HashMap.
			Integer key = (Integer) sorteoTuplaEnfrentamientosLigaKeys[new Random().nextInt(sorteoTuplaEnfrentamientosLigaKeys.length)];
			if(!sorteoTuplaEnfrentamientosLigaMapDesordenado.containsKey(key)){
				sorteoTuplaEnfrentamientosLigaMapDesordenado.put(key, sorteoLiga.getSorteoTuplaEnfrentamientosLigaMap().get(key));
			}
		}
		
		//Recorrer la tupla de partidos
		for (Entry<Integer, SorteoTuplaEnfrentamientosLiga2021> entrySorteoTuplaEnfrentamientosLiga : sorteoLiga.getSorteoTuplaEnfrentamientosLigaMap().entrySet()) {
			SorteoTuplaEnfrentamientosLiga2021 stel = entrySorteoTuplaEnfrentamientosLiga.getValue();
			
			//Si nadie descansa se procesan los enfrentamientos de ida y vuelta
			if(stel.getEquipo1() != null && stel.getEquipo2() != null){
				
				// Recuperar las jornadas por categoria para obtener la fecha y calcular la disputa
				LinkedHashMap<String, SorteoJornadasCategoria2021> sorteoJornadasCategoriaMap = recuperarJornadasCategoria(sorteoLiga, stel.getCategoria());
				
				String idEnfrentamientoIda = stel.getEquipo1().getId() + "-" + stel.getEquipo2().getId();
				
				CampeonatoEquiposCalendario2021 cecIda = enfrentamientosJornadaLibres.get(idEnfrentamientoIda);
				
				String idEnfrentamientoVta = stel.getEquipo2().getId() + "-" + stel.getEquipo1().getId();
				
				CampeonatoEquiposCalendario2021 cecVta = enfrentamientosJornadaLibres.get(idEnfrentamientoVta);
				
				Integer jornadaIda = stel.getJornadaIda();
				Integer jornadaVta = stel.getJornadaVta();
				
				// Comprobar la disponibilidad de la bolera
				// 6.1.2 Recuperar la jornada de la categoria
				SorteoJornadasCategoria2021 sorteoJornadasCategoriaIda = recuperarJornadaCategoria(sorteoJornadasCategoriaMap, jornadaIda, stel.getCategoria());
				SorteoJornadasCategoria2021 sorteoJornadasCategoriaVta = recuperarJornadaCategoria(sorteoJornadasCategoriaMap, jornadaVta, stel.getCategoria());
				
				// 6.1.3 Comprobar disponibilidad de bolera
				// Se comprueba que en la bolera no hay mas de 3
				// enfrentamientos para la jornada
				// Si existen mas se ha de volver a procesar
				// buscando otra jornada
				boolean boleraDisponibleIda = false;
				LinkedHashMap<String, CampeonatoEquiposCalendario2021> boleraDisponibleIdaCec = existeDisponibilidadBolera(
						enfrentamientosJornadaAsignados, cecIda, sorteoJornadasCategoriaIda);
				if (boleraDisponibleIdaCec.size() < 3) {
					boleraDisponibleIda = true;
				}else{
					System.out.println("BOLERA OCUPADA");
				}
				
				boolean boleraDisponibleVta = false;
				LinkedHashMap<String, CampeonatoEquiposCalendario2021> boleraDisponibleVtaCec = existeDisponibilidadBolera(
						enfrentamientosJornadaAsignados, cecVta, sorteoJornadasCategoriaVta);
				if (boleraDisponibleVtaCec.size() < 3) {
					boleraDisponibleVta = true;
				}else{
					System.out.println("BOLERA OCUPADA");
				}
				
				if(boleraDisponibleIda && boleraDisponibleVta){
				
					//Si hay disponbilidad se asigna las jornadas con fecha y hora
					boolean disponibleBoleraSMIda = disponibilidadBoleraHorario(
							boleraDisponibleIdaCec, sorteoJornadasCategoriaIda, 1);
					boolean disponibleBoleraSTIda = disponibilidadBoleraHorario(
							boleraDisponibleIdaCec, sorteoJornadasCategoriaIda, 2);
					boolean disponibleBoleraDMIda = disponibilidadBoleraHorario(
							boleraDisponibleIdaCec, sorteoJornadasCategoriaIda, 3);
					boolean disponibleBoleraDTIda = disponibilidadBoleraHorario(
							boleraDisponibleIdaCec, sorteoJornadasCategoriaIda, 4);
					// 6.1.3.2 Se asigna la mejor fecha disponible
					// para la bolera y equipos por preferencia
					cecIda = asignarJornadaPorDisponibilidadPreferencia(cecIda, sorteoJornadasCategoriaIda,
							disponibleBoleraSMIda, disponibleBoleraSTIda, disponibleBoleraDMIda, disponibleBoleraDTIda,
							jornadaIda, enfrentamientosJornadaAsignados);
					
					//Si hay disponbilidad se asigna las jornadas con fecha y hora
					boolean disponibleBoleraSMVta = disponibilidadBoleraHorario(
							boleraDisponibleVtaCec, sorteoJornadasCategoriaVta, 1);
					boolean disponibleBoleraSTVta = disponibilidadBoleraHorario(
							boleraDisponibleVtaCec, sorteoJornadasCategoriaVta, 2);
					boolean disponibleBoleraDMVta = disponibilidadBoleraHorario(
							boleraDisponibleVtaCec, sorteoJornadasCategoriaVta, 3);
					boolean disponibleBoleraDTVta = disponibilidadBoleraHorario(
							boleraDisponibleVtaCec, sorteoJornadasCategoriaVta, 4);
					// 6.1.3.2 Se asigna la mejor fecha disponible
					// para la bolera y equipos por preferencia
					cecVta = asignarJornadaPorDisponibilidadPreferencia(cecVta, sorteoJornadasCategoriaVta,
							disponibleBoleraSMVta, disponibleBoleraSTVta, disponibleBoleraDMVta, disponibleBoleraDTVta,
							jornadaVta, enfrentamientosJornadaAsignados);
					
					//Almacenar las jornadas
					enfrentamientosJornadaAsignados.put(idEnfrentamientoIda, cecIda);
					enfrentamientosJornadaAsignados.put(idEnfrentamientoVta, cecVta);
//					System.out.println(
//							"|**** ENFRENTAMIENTO ****| " + " *|* ID ENFRENTAMIENTO :"
//									+ idEnfrentamientoIda + cecIda.getPrintCec());
//					System.out.println(
//							"|**** ENFRENTAMIENTO ****| " + " *|* ID ENFRENTAMIENTO :"
//									+ idEnfrentamientoVta + cecVta.getPrintCec());
				}else{
					System.out.println("NO HAY BOLERA DISPONIBLE");
				}
				
			}
			
		}
		
		sorteoLiga.getEnfrentamientosJornadaAsignados().putAll(enfrentamientosJornadaAsignados);

		return sorteoLiga;
	}
	
	private CalendarioAsturias2021 recuperarCalendarioAsturiasJornada(JornadaCategoria2021 jornadaCategoriaVta, List<CalendarioAsturias2021> resultListJornadas){
		for(CalendarioAsturias2021 calendarioAsturiasVta : resultListJornadas){
			if(jornadaCategoriaVta.getJornadaVta().equals(calendarioAsturiasVta.getId())){
				return calendarioAsturiasVta;
			}
		}
		return null;
	}

	private CampeonatoEquiposCalendario2021  asignarJornadaPorDisponibilidadPreferencia(CampeonatoEquiposCalendario2021 cecLibre,
			SorteoJornadasCategoria2021 sorteoJornadasCategoria, boolean disponibleBoleraSM, boolean disponibleBoleraST,
			boolean disponibleBoleraDM, boolean disponibleBoleraDT, Integer numeroJornada, 
			LinkedHashMap<String, CampeonatoEquiposCalendario2021> enfrentamientosJornadaAsignados) {
		
//		System.out.println(
//				"|**** ENFRENTAMIENTO ****| " + " *|* ID ENFRENTAMIENTO :" + cecLibre.getPrintCec());
		
		//Comprobar si no puede jugar en algun horario con otro equipo por tema de armadores
		Integer equipo1NoJugarHorarioOtroEquipo = cecLibre.getEquipo1().getNoJugarHorarioOtroEquipo();
		Integer equipo2NoJugarHorarioOtroEquipo = cecLibre.getEquipo2().getNoJugarHorarioOtroEquipo();
		boolean horarioSMCoincidente = disponibilidadHorarioNoCoincidenteOtrosEquipos(
								enfrentamientosJornadaAsignados, sorteoJornadasCategoria, 
								1, equipo1NoJugarHorarioOtroEquipo, equipo2NoJugarHorarioOtroEquipo, cecLibre, numeroJornada); 
		boolean horarioSTCoincidente = disponibilidadHorarioNoCoincidenteOtrosEquipos(
								enfrentamientosJornadaAsignados, sorteoJornadasCategoria, 
								2, equipo1NoJugarHorarioOtroEquipo, equipo2NoJugarHorarioOtroEquipo, cecLibre, numeroJornada); 
		boolean horarioDMCoincidente = disponibilidadHorarioNoCoincidenteOtrosEquipos(
								enfrentamientosJornadaAsignados, sorteoJornadasCategoria, 
								3, equipo1NoJugarHorarioOtroEquipo, equipo2NoJugarHorarioOtroEquipo, cecLibre, numeroJornada); 
		boolean horarioDTCoincidente = disponibilidadHorarioNoCoincidenteOtrosEquipos(
								enfrentamientosJornadaAsignados, sorteoJornadasCategoria, 
								4, equipo1NoJugarHorarioOtroEquipo, equipo2NoJugarHorarioOtroEquipo, cecLibre, numeroJornada); 
		
		
		//Primero si no hay enfrentamientos se escoge una preferencias horaria de los dos equipos comun, sino preferencia de equipo local
		//Se obtienen las preferencias del local y visitante
		boolean preferenciaSM1 = cecLibre.getEquipo1().getHorarioPreferenteSabadoMaņana().equalsIgnoreCase(Activo2021.SI); 
		boolean preferenciaST1 = cecLibre.getEquipo1().getHorarioPreferenteSabadoTarde().equalsIgnoreCase(Activo2021.SI);
		boolean preferenciaDM1 = cecLibre.getEquipo1().getHorarioPreferenteDomingoMaņana().equalsIgnoreCase(Activo2021.SI);
		boolean preferenciaDT1 = cecLibre.getEquipo1().getHorarioPreferenteDomingoTarde().equalsIgnoreCase(Activo2021.SI);
		boolean preferenciaSM2 = cecLibre.getEquipo2().getHorarioPreferenteSabadoMaņana().equalsIgnoreCase(Activo2021.SI); 
		boolean preferenciaST2 = cecLibre.getEquipo2().getHorarioPreferenteSabadoTarde().equalsIgnoreCase(Activo2021.SI);
		boolean preferenciaDM2 = cecLibre.getEquipo2().getHorarioPreferenteDomingoMaņana().equalsIgnoreCase(Activo2021.SI);
		boolean preferenciaDT2 = cecLibre.getEquipo2().getHorarioPreferenteDomingoTarde().equalsIgnoreCase(Activo2021.SI);
		//Si la bolera esta disponible en todas las fechas se asigna la mejor opcion de equipos
		//Si los dos tiene preferencia de sabado maņana se asigna por defecto
		//Sino se busca la fecha preferente de los dos empezando por sabado y domingo tarde, luego domingo maņana
		if(preferenciaSM1 && preferenciaSM2 && disponibleBoleraSM && horarioSMCoincidente){
			//Asignar enfrentamiento
			cecLibre = asignarJornada(cecLibre, numeroJornada, 
					sorteoJornadasCategoria.getFechaDesde(), HorarioJornadas2021.HORA_M);
		} else if(preferenciaST1 && preferenciaST2 && disponibleBoleraST && horarioSTCoincidente){
			//Asignar enfrentamiento
			cecLibre = asignarJornada(cecLibre, numeroJornada, 
					sorteoJornadasCategoria.getFechaDesde(), HorarioJornadas2021.HORA_T);
		} else if(preferenciaDT1 && preferenciaDT2 && disponibleBoleraDT && horarioDTCoincidente){
			//Asignar enfrentamiento
			cecLibre = asignarJornada(cecLibre, numeroJornada, 
					sorteoJornadasCategoria.getFechaHasta(), HorarioJornadas2021.HORA_T);
		} else if(preferenciaDM1 && preferenciaDM2 && disponibleBoleraDM && horarioDMCoincidente){
			//Asignar enfrentamiento
			cecLibre = asignarJornada(cecLibre, numeroJornada, 
					sorteoJornadasCategoria.getFechaHasta(), HorarioJornadas2021.HORA_M);
		} else {
			//Asignar enfrentamiento en funcion de equipo local por sabado tarde, domingo tarde y domingo maņana
			if(preferenciaST1 && preferenciaST2 && disponibleBoleraST && horarioSTCoincidente){
				//Asignar enfrentamiento
				cecLibre = asignarJornada(cecLibre, numeroJornada, 
						sorteoJornadasCategoria.getFechaDesde(), HorarioJornadas2021.HORA_T);
			} else if(preferenciaDT1 && preferenciaDT2 && disponibleBoleraDT && horarioDTCoincidente){
				//Asignar enfrentamiento
				cecLibre = asignarJornada(cecLibre, numeroJornada, 
						sorteoJornadasCategoria.getFechaHasta(), HorarioJornadas2021.HORA_T);
			} else if(preferenciaDM1 && preferenciaDM2 && disponibleBoleraDM && horarioDMCoincidente){
				//Asignar enfrentamiento
				cecLibre = asignarJornada(cecLibre, numeroJornada, 
						sorteoJornadasCategoria.getFechaHasta(), HorarioJornadas2021.HORA_M);
			} else if(preferenciaST1 && disponibleBoleraST && horarioSTCoincidente){
				//Asignar enfrentamiento
				cecLibre = asignarJornada(cecLibre, numeroJornada, 
						sorteoJornadasCategoria.getFechaDesde(), HorarioJornadas2021.HORA_T);
			} else if(preferenciaDT1 && disponibleBoleraDT && horarioDTCoincidente){
				//Asignar enfrentamiento
				cecLibre = asignarJornada(cecLibre, numeroJornada, 
						sorteoJornadasCategoria.getFechaHasta(), HorarioJornadas2021.HORA_T);
			} else if(preferenciaDM1 && disponibleBoleraDM && horarioDMCoincidente){
				//Asignar enfrentamiento
				cecLibre = asignarJornada(cecLibre, numeroJornada, 
						sorteoJornadasCategoria.getFechaHasta(), HorarioJornadas2021.HORA_M);
			} else if(disponibleBoleraST && horarioSTCoincidente){
				//Asignar enfrentamiento
				cecLibre = asignarJornada(cecLibre, numeroJornada, 
						sorteoJornadasCategoria.getFechaDesde(), HorarioJornadas2021.HORA_T);
			} else if(disponibleBoleraDT && horarioDTCoincidente){
				//Asignar enfrentamiento
				cecLibre = asignarJornada(cecLibre, numeroJornada, 
						sorteoJornadasCategoria.getFechaHasta(), HorarioJornadas2021.HORA_T);
			} else if(disponibleBoleraDM && horarioDMCoincidente){
				//Asignar enfrentamiento
				cecLibre = asignarJornada(cecLibre, numeroJornada, 
						sorteoJornadasCategoria.getFechaHasta(), HorarioJornadas2021.HORA_M);
			} else if(disponibleBoleraST){
				//Asignar enfrentamiento
				cecLibre = asignarJornada(cecLibre, numeroJornada, 
						sorteoJornadasCategoria.getFechaDesde(), HorarioJornadas2021.HORA_T);
			} else if(disponibleBoleraDT){
				//Asignar enfrentamiento
				cecLibre = asignarJornada(cecLibre, numeroJornada, 
						sorteoJornadasCategoria.getFechaHasta(), HorarioJornadas2021.HORA_T);
			} else if(disponibleBoleraDM){
				//Asignar enfrentamiento
				cecLibre = asignarJornada(cecLibre, numeroJornada, 
						sorteoJornadasCategoria.getFechaHasta(), HorarioJornadas2021.HORA_M);
			}
		}
		return cecLibre;
	}
	
	private boolean disponibilidadHorarioNoCoincidenteOtrosEquipos(LinkedHashMap<String, CampeonatoEquiposCalendario2021> enfrentamientosJornadaAsignados, 
			SorteoJornadasCategoria2021 sorteoJornadasCategoria, Integer horario, 
			Integer equipo1NoJugarHorarioOtroEquipo, Integer equipo2NoJugarHorarioOtroEquipo, 
			CampeonatoEquiposCalendario2021 cecLibre, Integer numeroJornada){
		boolean disponible = true;
		
		//Borrar parada para ver san roque y astures
		if(equipo1NoJugarHorarioOtroEquipo > 0 || equipo2NoJugarHorarioOtroEquipo > 0){
			Integer idEquipoSanRoque = 14;
			Integer idEquipoAstures = 11;
			if(cecLibre.getEquipo1().getId().equals(idEquipoSanRoque) || cecLibre.getEquipo2().getId().equals(idEquipoSanRoque) ||
					cecLibre.getEquipo1().getId().equals(idEquipoAstures) || cecLibre.getEquipo2().getId().equals(idEquipoAstures)){
//				System.out.println("PARADA PARA REVISAR");
			}
		}
		

		//Miro si el equipo 1 no puede jugar al mismo horario del equipo 2
		boolean revisarNoJugarEquipo1EnMismoHorario = false;
		if(equipo1NoJugarHorarioOtroEquipo > 0){
			if(!equipo1NoJugarHorarioOtroEquipo.equals(cecLibre.getEquipo2().getId())){
				revisarNoJugarEquipo1EnMismoHorario = true;
			}
			if(revisarNoJugarEquipo1EnMismoHorario){
				//Busco el horario del equipo 2 en la jornada asignada, si esta he de indicar que ese horario no es posible
				for (Entry<String, CampeonatoEquiposCalendario2021> entryEnfrentamientosJornadaAsignados : enfrentamientosJornadaAsignados.entrySet()) {
					CampeonatoEquiposCalendario2021 cecAsignado = entryEnfrentamientosJornadaAsignados.getValue();
					if(cecAsignado.getJornada().equals(numeroJornada) && 
							(cecAsignado.getEquipo1Id().equals(equipo1NoJugarHorarioOtroEquipo) ||
							 cecAsignado.getEquipo2Id().equals(equipo1NoJugarHorarioOtroEquipo))){
						if(cecAsignado.getFechaText().equals(sorteoJornadasCategoria.getFechaDesdeText())){
							if(horario == 1){
								if(cecAsignado.getHora().equals(HorarioJornadas2021.HORA_M)){
									disponible = false;
								}
							}
							if(horario == 2){
								if(cecAsignado.getHora().equals(HorarioJornadas2021.HORA_T)){
									disponible = false;
								}
							}
						}else if(cecAsignado.getFechaText().equals(sorteoJornadasCategoria.getFechaHastaText())){
							if(horario == 3){
								if(cecAsignado.getHora().equals(HorarioJornadas2021.HORA_M)){
									disponible = false;
								}
							}
							if(horario == 4){
								if(cecAsignado.getHora().equals(HorarioJornadas2021.HORA_T)){
									disponible = false;
								}
							}
						}
					}
				}
			}
		}
		
		//Miro si el equipo 2 no puede jugar al mismo horario del equipo 1
		boolean revisarNoJugarEquipo2EnMismoHorario = false;
		if(equipo2NoJugarHorarioOtroEquipo > 0){
			if(!equipo2NoJugarHorarioOtroEquipo.equals(cecLibre.getEquipo1().getId())){
				revisarNoJugarEquipo2EnMismoHorario = true;
			}
			if(revisarNoJugarEquipo2EnMismoHorario){
				//Busco el horario del equipo 2 en la jornada asignada, si esta he de indicar que ese horario no es posible
				for (Entry<String, CampeonatoEquiposCalendario2021> entryEnfrentamientosJornadaAsignados : enfrentamientosJornadaAsignados.entrySet()) {
					CampeonatoEquiposCalendario2021 cecAsignado = entryEnfrentamientosJornadaAsignados.getValue();
					if(cecAsignado.getJornada().equals(numeroJornada) && 
							(cecAsignado.getEquipo1Id().equals(equipo2NoJugarHorarioOtroEquipo) ||
							 cecAsignado.getEquipo2Id().equals(equipo2NoJugarHorarioOtroEquipo))){
						if(cecAsignado.getFechaText().equals(sorteoJornadasCategoria.getFechaDesdeText())){
							if(horario == 1){
								if(cecAsignado.getHora().equals(HorarioJornadas2021.HORA_M)){
									disponible = false;
								}
							}
							if(horario == 2){
								if(cecAsignado.getHora().equals(HorarioJornadas2021.HORA_T)){
									disponible = false;
								}
							}
						}else if(cecAsignado.getFechaText().equals(sorteoJornadasCategoria.getFechaHastaText())){
							if(horario == 3){
								if(cecAsignado.getHora().equals(HorarioJornadas2021.HORA_M)){
									disponible = false;
								}
							}
							if(horario == 4){
								if(cecAsignado.getHora().equals(HorarioJornadas2021.HORA_T)){
									disponible = false;
								}
							}
						}
					}
				}
			}
		}

		return disponible;
	}
		
	private boolean disponibilidadBoleraHorario(LinkedHashMap<String, CampeonatoEquiposCalendario2021> enfrentamientosJornadaAsignadosBolera, 
			SorteoJornadasCategoria2021 sorteoJornadasCategoria, Integer horario){
		boolean disponible = true;
		for (Entry<String, ?> entryEnfrentamientosJornadaAsignadosBolera : enfrentamientosJornadaAsignadosBolera.entrySet()) {
			Object valueEnfrentamientosJornadaAsignadosBolera = entryEnfrentamientosJornadaAsignadosBolera.getValue();
			CampeonatoEquiposCalendario2021 cecAsignadoBolera = (CampeonatoEquiposCalendario2021) valueEnfrentamientosJornadaAsignadosBolera;
			//Se comprueba fecha sabado o primer dia y hora, y posteriormente domingo o segundo dia y hora
			if(cecAsignadoBolera.getFechaText().equals(sorteoJornadasCategoria.getFechaDesdeText())){
				if(horario == 1){
					if(cecAsignadoBolera.getHora().equals(HorarioJornadas2021.HORA_M)){
						disponible = false;
					}
				}
				if(horario == 2){
					if(cecAsignadoBolera.getHora().equals(HorarioJornadas2021.HORA_T)){
						disponible = false;
					}
				}
			}else if(cecAsignadoBolera.getFechaText().equals(sorteoJornadasCategoria.getFechaHastaText())){
				if(horario == 3){
					if(cecAsignadoBolera.getHora().equals(HorarioJornadas2021.HORA_M)){
						disponible = false;
					}
				}
				if(horario == 4){
					if(cecAsignadoBolera.getHora().equals(HorarioJornadas2021.HORA_T)){
						disponible = false;
					}
				}
			}
		}
		return disponible;
	}

	private LinkedHashMap<String, CampeonatoEquiposCalendario2021> existeDisponibilidadBolera(
			LinkedHashMap<String, CampeonatoEquiposCalendario2021> enfrentamientosJornadaAsignados,
			CampeonatoEquiposCalendario2021 cecLibre, SorteoJornadasCategoria2021 sorteoJornadasCategoria) {
		
		LinkedHashMap<String, CampeonatoEquiposCalendario2021> enfrentamientosJornadaAsignadosBolera = new LinkedHashMap<>();
		//Se buscan los enfrentamientos asignados para la bolera
		for (Entry<String, ?> entryEnfrentamientosJornadaAsignados : enfrentamientosJornadaAsignados.entrySet()) {
			Object valueEnfrentamientosJornadaAsignados = entryEnfrentamientosJornadaAsignados.getValue();
			String key = entryEnfrentamientosJornadaAsignados.getKey();
			CampeonatoEquiposCalendario2021 cecAsignado = (CampeonatoEquiposCalendario2021) valueEnfrentamientosJornadaAsignados;
			if(cecAsignado.getEquipo1().getBoleraId().equals(cecLibre.getEquipo1().getBoleraId())){
				if(cecAsignado.getFechaText().equals(sorteoJornadasCategoria.getFechaDesdeText())
					|| cecAsignado.getFechaText().equals(sorteoJornadasCategoria.getFechaHastaText())){
					enfrentamientosJornadaAsignadosBolera.put(key, cecAsignado);
				}
			}
		}
		
		return enfrentamientosJornadaAsignadosBolera;
	}

	private SorteoJornadasCategoria2021 recuperarJornadaCategoria(
			LinkedHashMap<String, SorteoJornadasCategoria2021> sorteoJornadasCategoriaMap, Integer randomJornada, Integer categoria) {
		for (Entry<String, SorteoJornadasCategoria2021> entrySorteoJornadasCategoria : sorteoJornadasCategoriaMap.entrySet()) {
			SorteoJornadasCategoria2021 sorteoJornadasCategoria = (SorteoJornadasCategoria2021) entrySorteoJornadasCategoria.getValue();
			for (Entry<String, JornadaCategoria2021> entryJornadasCategoria : sorteoJornadasCategoria.getJornadasCategoriasMap().entrySet()) {
				JornadaCategoria2021 jornadaCategoria = (JornadaCategoria2021) entryJornadasCategoria.getValue();
				if(jornadaCategoria.getJornada().equals(randomJornada) && jornadaCategoria.getCategoria().getId().equals(categoria)){
					return sorteoJornadasCategoria;
				}
			}
		}
		return null;
	}
	
	private JornadaCategoria2021 recuperarJornadaVueltaCategoria(
			LinkedHashMap<String, SorteoJornadasCategoria2021> sorteoJornadasCategoriaMap, Integer randomJornada) {
		for (Entry<String, SorteoJornadasCategoria2021> entrySorteoJornadasCategoria : sorteoJornadasCategoriaMap.entrySet()) {
			SorteoJornadasCategoria2021 sorteoJornadasCategoria = (SorteoJornadasCategoria2021) entrySorteoJornadasCategoria.getValue();
			for (Entry<String, JornadaCategoria2021> entryJornadasCategoria : sorteoJornadasCategoria.getJornadasCategoriasMap().entrySet()) {
				JornadaCategoria2021 jornadaCategoria = (JornadaCategoria2021) entryJornadasCategoria.getValue();
				if(jornadaCategoria.getJornada().equals(randomJornada)){
					return jornadaCategoria;
				}
			}
		}
		return null;
	}
	
	private LinkedHashMap<String, SorteoJornadasCategoria2021> recuperarJornadasCategoria(SorteoLiga2021 sorteoLiga, Integer categoriaId) {
		LinkedHashMap<String, SorteoJornadasCategoria2021> sorteoJornadasCategoriaMap = 
				(LinkedHashMap<String, SorteoJornadasCategoria2021>) sorteoLiga.getSorteoJornadasCategoriaMap().clone();
		for (Entry<String, SorteoJornadasCategoria2021> entrySorteoJornadasCategoria : sorteoLiga.getSorteoJornadasCategoriaMap().entrySet()) {
			String key = entrySorteoJornadasCategoria.getKey();
			SorteoJornadasCategoria2021 sorteoJornadasCategoria = (SorteoJornadasCategoria2021) entrySorteoJornadasCategoria.getValue();
			boolean existeJornadaCategoria = false;
			for (Entry<String, JornadaCategoria2021> entryJornadasCategoria : sorteoJornadasCategoria.getJornadasCategoriasMap().entrySet()) {
				String keyJornadaCategoria = entryJornadasCategoria.getKey();
				JornadaCategoria2021 jornadaCategoria = (JornadaCategoria2021) entryJornadasCategoria.getValue();
				if(jornadaCategoria.getCategoria().getId().equals(categoriaId)){
					existeJornadaCategoria = true;
				}
			}
			if(!existeJornadaCategoria){
				sorteoJornadasCategoriaMap.remove(key);
			}
		}
		return sorteoJornadasCategoriaMap;
	}

	private LinkedHashMap<String, SorteoJornadasCategoria2021> recuperarJornadasCategoriaEquipo(SorteoLiga2021 sorteoLiga, Equipos2021 equipo) {
		LinkedHashMap<String, SorteoJornadasCategoria2021> sorteoJornadasCategoriaMap = 
				(LinkedHashMap<String, SorteoJornadasCategoria2021>) sorteoLiga.getSorteoJornadasCategoriaMap().clone();
		for (Entry<String, SorteoJornadasCategoria2021> entrySorteoJornadasCategoria : sorteoLiga.getSorteoJornadasCategoriaMap().entrySet()) {
			String key = entrySorteoJornadasCategoria.getKey();
			SorteoJornadasCategoria2021 sorteoJornadasCategoria = (SorteoJornadasCategoria2021) entrySorteoJornadasCategoria.getValue();
			boolean existeJornadaCategoria = false;
			for (Entry<String, JornadaCategoria2021> entryJornadasCategoria : sorteoJornadasCategoria.getJornadasCategoriasMap().entrySet()) {
				String keyJornadaCategoria = entryJornadasCategoria.getKey();
				JornadaCategoria2021 jornadaCategoria = (JornadaCategoria2021) entryJornadasCategoria.getValue();
				if(jornadaCategoria.getCategoria().getId().equals(equipo.getCategoriaId())){
					existeJornadaCategoria = true;
				}
			}
			if(!existeJornadaCategoria){
				sorteoJornadasCategoriaMap.remove(key);
			}
		}
		return sorteoJornadasCategoriaMap;
	}

	private HashMap<String, CampeonatoEquiposCalendario2021> extraerEnfrentamientosJornadasLibres(SorteoLiga2021 sorteoLiga,
			HashMap<String, CampeonatoEquiposCalendario2021> enfrentamientosJornadaLibres) {
		//Recorrer las Jornadas y obtengo todos los enfrentamientos libres
		for (Entry<String, ?> entrySorteoJornadasCategoria : sorteoLiga.getSorteoJornadasCategoriaMap().entrySet()) {
			Object valueSorteoJornadasCategoria = entrySorteoJornadasCategoria.getValue();
			SorteoJornadasCategoria2021 sorteoJornadasCategoria = (SorteoJornadasCategoria2021) valueSorteoJornadasCategoria;

			//Por cada jornada de categoria saco los enfrentamientos a jugar libres
			for (Entry<Integer, ?> entrySorteoEnfrentamientosCategoria : sorteoLiga.getSorteoEnfrentamientosCategoriaMap().entrySet()) {
				Object valueSorteoEnfrentamientosCategoria = entrySorteoEnfrentamientosCategoria.getValue();
				SorteoEnfrentamientosCategoria2021 sorteoEnfrentamientosCategoria = (SorteoEnfrentamientosCategoria2021) valueSorteoEnfrentamientosCategoria;
				for (Entry<String, ?> entryCampeonatoEquiposCalendario : sorteoEnfrentamientosCategoria.getCampeonatoEquiposCalendario().entrySet()) {
					Object valueCampeonatoEquiposCalendario = entryCampeonatoEquiposCalendario.getValue();
					CampeonatoEquiposCalendario2021 sorteoCampeonatoEquiposCalendario = (CampeonatoEquiposCalendario2021) valueCampeonatoEquiposCalendario;
					if(sorteoCampeonatoEquiposCalendario.getJornada() == null 
						&& sorteoCampeonatoEquiposCalendario.getFecha() == null
						&& sorteoCampeonatoEquiposCalendario.getHora() == null){
						String idEnfrentamiento = sorteoCampeonatoEquiposCalendario.getEquipo1().getId().toString() + "-" + 
													sorteoCampeonatoEquiposCalendario.getEquipo2().getId().toString();
						enfrentamientosJornadaLibres.put(idEnfrentamiento, sorteoCampeonatoEquiposCalendario);
					}
				}
				
			}
		}
		return enfrentamientosJornadaLibres;
	}
	
	private HashMap<String, CampeonatoEquiposCalendario2021> extraerEnfrentamientosJornadasLibresCategoria(Integer categoriaId,
			HashMap<String, CampeonatoEquiposCalendario2021> enfrentamientosJornadaLibres) {
		
		
		HashMap<String, CampeonatoEquiposCalendario2021> enfrentamientosJornadaLibresCategoria = 
				(HashMap<String, CampeonatoEquiposCalendario2021>) enfrentamientosJornadaLibres.clone();
	
		//Elimino todos los enfrentamientos que no son del equipo
		for (Entry<String, CampeonatoEquiposCalendario2021> entrySorteoJornadasCategoriaLibres : enfrentamientosJornadaLibres.entrySet()) {
			String key = entrySorteoJornadasCategoriaLibres.getKey();
			CampeonatoEquiposCalendario2021 cecLibre = (CampeonatoEquiposCalendario2021) entrySorteoJornadasCategoriaLibres.getValue();
			if(!cecLibre.getCategoriaId().equals(categoriaId)){
				enfrentamientosJornadaLibresCategoria.remove(key);
			}
		}

		return enfrentamientosJornadaLibresCategoria;
	}
	
	private LinkedHashMap<String, CampeonatoEquiposCalendario2021> extraerEnfrentamientosJornadasLibresEquipo(Equipos2021 equipo,
			LinkedHashMap<String, CampeonatoEquiposCalendario2021> enfrentamientosJornadaLibres) {
		
		
		LinkedHashMap<String, CampeonatoEquiposCalendario2021> enfrentamientosJornadaLibresEquipo = 
				(LinkedHashMap<String, CampeonatoEquiposCalendario2021>) enfrentamientosJornadaLibres.clone();
	
		//Elimino todos los enfrentamientos que no son del equipo
		for (Entry<String, CampeonatoEquiposCalendario2021> entrySorteoJornadasCategoriaLibres : enfrentamientosJornadaLibres.entrySet()) {
			String key = entrySorteoJornadasCategoriaLibres.getKey();
			CampeonatoEquiposCalendario2021 cecLibre = (CampeonatoEquiposCalendario2021) entrySorteoJornadasCategoriaLibres.getValue();
			boolean isLocal = cecLibre.getEquipo1Id().equals(equipo.getId());
			boolean isVisitante = cecLibre.getEquipo2Id().equals(equipo.getId());
			if(!isLocal && !isVisitante){
				enfrentamientosJornadaLibresEquipo.remove(key);
			}
		}

		return enfrentamientosJornadaLibresEquipo;
	}
	
	private boolean jornadaAsignadaEquipo(Equipos2021 equipo, LinkedHashMap<String, CampeonatoEquiposCalendario2021> enfrentamientosJornadaAsignadas, Integer jornada) {
	
		//Elimino todos los enfrentamientos que no son del equipo
		for (Entry<String, CampeonatoEquiposCalendario2021> entrySorteoJornadasCategoriaAsignadas : enfrentamientosJornadaAsignadas.entrySet()) {
			String key = entrySorteoJornadasCategoriaAsignadas.getKey();
			CampeonatoEquiposCalendario2021 cecLibre = (CampeonatoEquiposCalendario2021) entrySorteoJornadasCategoriaAsignadas.getValue();
			boolean isLocal = cecLibre.getEquipo1Id().equals(equipo.getId());
			boolean isVisitante = cecLibre.getEquipo2Id().equals(equipo.getId());
			boolean jornadaAsignada = cecLibre.getJornada().equals(jornada);
			if((isLocal || isVisitante) && jornadaAsignada){
				return true;
			}
		}
		return false;
	}

	private LinkedHashMap<String, CampeonatoEquiposCalendario2021> generarEnfrentamientosJornadaLibresIds(
			LinkedHashMap<String, CampeonatoEquiposCalendario2021> enfrentamientosJornadaLibres,
			LinkedHashMap<String, CampeonatoEquiposCalendario2021> enfrentamientosJornadaAsignados, JornadaCategoria2021 jornadaCategoria) {
		LinkedHashMap<String, CampeonatoEquiposCalendario2021> enfrentamientosJornadaLibresIds = new LinkedHashMap<>();
		//Copiar todos los enfrentamientos
		enfrentamientosJornadaLibresIds = (LinkedHashMap<String, CampeonatoEquiposCalendario2021>) enfrentamientosJornadaLibres.clone();
		//Quitar los asignados
		for (Entry<String, ?> entryEnfrentamientosJornadaAsignados : enfrentamientosJornadaAsignados.entrySet()) {
			enfrentamientosJornadaLibresIds.remove(entryEnfrentamientosJornadaAsignados.getKey());
		}
		//Quitar los de otras categorias
		for (Entry<String, ?> entryEnfrentamientosJornadaLibres : enfrentamientosJornadaLibresIds.entrySet()) {
			Object valueEnfrentamientosJornadaLibres = entryEnfrentamientosJornadaLibres.getValue();
			String key = entryEnfrentamientosJornadaLibres.getKey();
			CampeonatoEquiposCalendario2021 cecAsignado = (CampeonatoEquiposCalendario2021) valueEnfrentamientosJornadaLibres;
			if(!cecAsignado.getCategoriaId().equals(jornadaCategoria.getId())){
				enfrentamientosJornadaLibresIds.remove(key);
			}
		}
		
		return enfrentamientosJornadaLibresIds;
	}

	private CampeonatoEquiposCalendario2021 asignarJornada(CampeonatoEquiposCalendario2021 cecLibre, Integer jornada,
			Date fechaDesde, String hora) {
		cecLibre.setJornada(jornada);
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		cecLibre.setFecha(df.format(fechaDesde));
		cecLibre.setHora(hora);
		return cecLibre;
	}
	
	private String[] randomEquiposId(LinkedHashMap<Integer, Integer> equiposSinAsignar,
			LinkedHashMap<String, CampeonatoEquiposCalendario2021> enfrentamientosJornadaLibresIds){
		
		Random generator = new Random();
		Object[] values = equiposSinAsignar.values().toArray();
		Integer randomValue1 = 0;
		Integer randomValue2 = 0;
		boolean enfrentamientoCorreto = false;
		while(!enfrentamientoCorreto){
			while(randomValue1 == randomValue2){
				randomValue1 = (Integer) values[generator.nextInt(values.length)];
				randomValue2 = (Integer) values[generator.nextInt(values.length)];
				String idEnfrentamiento = randomValue1.toString() + "-" + randomValue2.toString();
				CampeonatoEquiposCalendario2021 cec = enfrentamientosJornadaLibresIds.get(idEnfrentamiento);
				if(enfrentamientosJornadaLibresIds.containsKey(idEnfrentamiento)){
					enfrentamientoCorreto = true;
				}else{
					randomValue1 = 0;
					randomValue2 = 0;
				}
			}
		}
		String[] randomEquipos = new String[2];
		randomEquipos[0] = randomValue1.toString();
		randomEquipos[1] = randomValue2.toString();
		return randomEquipos;
	}
	
	

	private SorteoLiga2021 prepareDataSorteoLiga(SorteoLiga2021 sorteoLiga, List<Categorias2021> resultListCategorias, 
			List<Equipos2021> resultListEquipos, List<Boleras2021> resultListBoleras, 
			List<CalendarioAsturias2021> resultListJornadas, 
			List<SorteoBolerasCrucesLiga2021> resultListSorteoBolerasCrucesLiga, 
			List<SorteoTuplaEnfrentamientosLiga2021> resultListSorteoTuplaEnfrentamientosLiga) {
		//Agrupar por objeto
		
		//1. Equipos por categoria
		for(Categorias2021 categoria : resultListCategorias){
			SorteoCategoria2021 sorteoCategoria = new SorteoCategoria2021();
			sorteoCategoria.setCategoria(categoria);
			for(Equipos2021 equipo : resultListEquipos){
				if(equipo.getCategoriaId().equals(categoria.getId())){
					sorteoCategoria.getEquiposPorCategoria().put(equipo.getId(), equipo);
				}
			}
			sorteoLiga.getSorteoCategoriasMap().put(categoria.getId(), sorteoCategoria);
		}
		
		//2. Equipos por bolera
		//Cuento los equipos por bolera y los ordeno por numero de equipos
		LinkedHashMap<Integer, Integer> sorteoBolerasEquiposCountMap = new LinkedHashMap<>();
		LinkedHashMap<Integer, SorteoBolerasEquipos2021> sorteoBolerasEquiposMap = new LinkedHashMap<>();
		for(Boleras2021 bolera : resultListBoleras){
			SorteoBolerasEquipos2021 sorteoBolerasEquipos = new SorteoBolerasEquipos2021();
			sorteoBolerasEquipos.setBolera(bolera);
			for(Equipos2021 equipo : resultListEquipos){
				if(equipo.getBoleraId().equals(bolera.getId())){
					sorteoBolerasEquipos.getEquipos().put(equipo.getId(), equipo);
				}
			}
			sorteoBolerasEquiposCountMap.put(bolera.getId(), sorteoBolerasEquipos.getEquipos().size());
			sorteoBolerasEquiposMap.put(bolera.getId(), sorteoBolerasEquipos);
		}
		Integer countBoleras = 0;
		while(countBoleras < sorteoBolerasEquiposCountMap.size()){
			//Boleras de 5 equipos
			for (Entry<Integer, Integer> entry : sorteoBolerasEquiposCountMap.entrySet()) {
				if(entry.getValue() == 5){
					SorteoBolerasEquipos2021 sorteoBolerasEquipos = sorteoBolerasEquiposMap.get(entry.getKey());
					sorteoLiga.getSorteoBolerasEquiposMap().put(entry.getKey(), sorteoBolerasEquipos);
					countBoleras++;
				}
			}
			//Boleras de 4 equipos
			for (Entry<Integer, Integer> entry : sorteoBolerasEquiposCountMap.entrySet()) {
				if(entry.getValue() == 4){
					SorteoBolerasEquipos2021 sorteoBolerasEquipos = sorteoBolerasEquiposMap.get(entry.getKey());
					sorteoLiga.getSorteoBolerasEquiposMap().put(entry.getKey(), sorteoBolerasEquipos);
					countBoleras++;
				}
			}
			//Boleras de 3 equipos
			for (Entry<Integer, Integer> entry : sorteoBolerasEquiposCountMap.entrySet()) {
				if(entry.getValue() == 3){
					SorteoBolerasEquipos2021 sorteoBolerasEquipos = sorteoBolerasEquiposMap.get(entry.getKey());
					sorteoLiga.getSorteoBolerasEquiposMap().put(entry.getKey(), sorteoBolerasEquipos);
					countBoleras++;
				}
			}
			//Boleras de 2 equipos
			for (Entry<Integer, Integer> entry : sorteoBolerasEquiposCountMap.entrySet()) {
				if(entry.getValue() == 2){
					SorteoBolerasEquipos2021 sorteoBolerasEquipos = sorteoBolerasEquiposMap.get(entry.getKey());
					sorteoLiga.getSorteoBolerasEquiposMap().put(entry.getKey(), sorteoBolerasEquipos);
					countBoleras++;
				}
			}
			//Boleras de 1 equipo
			for (Entry<Integer, Integer> entry : sorteoBolerasEquiposCountMap.entrySet()) {
				if(entry.getValue() == 1){
					SorteoBolerasEquipos2021 sorteoBolerasEquipos = sorteoBolerasEquiposMap.get(entry.getKey());
					sorteoLiga.getSorteoBolerasEquiposMap().put(entry.getKey(), sorteoBolerasEquipos);
					countBoleras++;
				}
			}
			//Boleras de 0 equipo
			for (Entry<Integer, Integer> entry : sorteoBolerasEquiposCountMap.entrySet()) {
				if(entry.getValue() == 0){
					SorteoBolerasEquipos2021 sorteoBolerasEquipos = sorteoBolerasEquiposMap.get(entry.getKey());
					sorteoLiga.getSorteoBolerasEquiposMap().put(entry.getKey(), sorteoBolerasEquipos);
					countBoleras++;
				}
			}
		}
		
		//3. Leer las boleras para cada cruce y asignar un equipo de la bolera y categoria
		LinkedHashMap<String, Equipos2021> sorteoBolerasCrucesLigaAsignadosMap = new LinkedHashMap<>();
		Integer totalBolerasCrucesAsignadas = 0;
		while(resultListSorteoBolerasCrucesLiga.size() > totalBolerasCrucesAsignadas){
			for (SorteoBolerasCrucesLiga2021 sbcl : resultListSorteoBolerasCrucesLiga) {
				Equipos2021 equipoAsignar = null;
				//Asignar equipo para la bolera y categoria
				if(isBoleraPreferente(sbcl)){
					equipoAsignar = buscarEquipoByBoleraCategoriaPreferente(resultListEquipos, resultListBoleras, sorteoBolerasCrucesLigaAsignadosMap,
							sbcl);
				}else if(isBoleraVacia(sbcl)){
					equipoAsignar = null;
				}else if(isBoleraTerna(sbcl)){
					equipoAsignar = buscarEquipoByBoleraCategoriaNoPreferente(resultListEquipos, resultListBoleras, sorteoBolerasCrucesLigaAsignadosMap,
							sbcl);
				}
				
				sbcl.setEquipo(equipoAsignar);
				sorteoBolerasCrucesLigaAsignadosMap.put(sbcl.getIdSearch(), equipoAsignar);
				sorteoLiga.getSorteoBolerasCrucesLigaMap().put(sbcl.getIdSearch(), sbcl);
				totalBolerasCrucesAsignadas++;
				
			}
			
		}
		
		//4. Leer la Tupla de enfrentamientos
		for (SorteoTuplaEnfrentamientosLiga2021 stel : resultListSorteoTuplaEnfrentamientosLiga) {
			
			//Localizar el equipo 1 y equipo 2 para el cruce
			//ha de pertenecer a la bolera indicada en los SorteoBolerasCrucesLiga
			//mirando la preferente en caso de solo tener una
			SorteoBolerasCrucesLiga2021 sbcl1Ida = sorteoLiga.getSorteoBolerasCrucesLigaMap().get(stel.getIdSearchCruce1Ida());
			SorteoBolerasCrucesLiga2021 sbcl2Ida = sorteoLiga.getSorteoBolerasCrucesLigaMap().get(stel.getIdSearchCruce2Ida());
			
			//Asignar equipo
			stel.setEquipo1(sbcl1Ida.getEquipo());
			stel.setEquipo2(sbcl2Ida.getEquipo());
			
			sorteoLiga.getSorteoTuplaEnfrentamientosLigaMap().put(stel.getId(), stel);
		}
		
		
		
		
		
		
		
		
		
		//Jornadas por categoria, con id del hashMap fechadesde_fechahasta
//		System.out.println();
		for(CalendarioAsturias2021 calendarioAsturias : resultListJornadas){
			if(calendarioAsturias.getModalidadId() == Modalidad2021.EQUIPOS_LIGA){
				SorteoJornadasCategoria2021 sorteoJornadasCategoria = new SorteoJornadasCategoria2021();
				String id = calendarioAsturias.getIdFechasJornada();
				sorteoJornadasCategoria.setId(id);
				sorteoJornadasCategoria.setFechaDesde(calendarioAsturias.getFechaDesde());
				sorteoJornadasCategoria.setFechaHasta(calendarioAsturias.getFechaHasta());
				if(!sorteoLiga.getSorteoJornadasCategoriaMap().containsKey(sorteoJornadasCategoria.getId())){
					JornadaCategoria2021 jornadaCategoria = new JornadaCategoria2021();
					jornadaCategoria.setCategoria(calendarioAsturias.getCategoria());
					jornadaCategoria.setJornada(calendarioAsturias.getJornada());
					jornadaCategoria.setJornadaVta(calendarioAsturias.getJornadaVuelta());
//					System.out.println("|**** JORNADAS POR CATEGORIA ****| " + " *|* ID :" + sorteoJornadasCategoria.getId() 
//										+ " *|* FECHA_DESDE :" + sorteoJornadasCategoria.getFechaDesdeText()
//										+ " *|* FECHA_HASTA :" + sorteoJornadasCategoria.getFechaHastaText()
//										+ " *|* JORNADA_CATEGORIA :" + jornadaCategoria.getId());
					sorteoJornadasCategoria.getJornadasCategoriasMap().put(jornadaCategoria.getId(), jornadaCategoria);
					sorteoLiga.getSorteoJornadasCategoriaMap().put(sorteoJornadasCategoria.getId(), sorteoJornadasCategoria);
				}else{
					sorteoJornadasCategoria = sorteoLiga.getSorteoJornadasCategoriaMap().get(sorteoJornadasCategoria.getId());
					JornadaCategoria2021 jornadaCategoria = new JornadaCategoria2021();
					jornadaCategoria.setCategoria(calendarioAsturias.getCategoria());
					jornadaCategoria.setJornada(calendarioAsturias.getJornada());
					jornadaCategoria.setJornadaVta(calendarioAsturias.getJornadaVuelta());
//					System.out.println("|**** JORNADAS POR CATEGORIA ****| " + " *|* ID :" + sorteoJornadasCategoria.getId() 
//										+ " *|* FECHA_DESDE :" + sorteoJornadasCategoria.getFechaDesdeText()
//										+ " *|* FECHA_HASTA :" + sorteoJornadasCategoria.getFechaHastaText()
//										+ " *|* JORNADA_CATEGORIA :" + jornadaCategoria.getId());
					sorteoJornadasCategoria.getJornadasCategoriasMap().put(jornadaCategoria.getId(), jornadaCategoria);
					sorteoLiga.getSorteoJornadasCategoriaMap().replace(sorteoJornadasCategoria.getId(), sorteoJornadasCategoria);
				}
			}
		}
		

		

		
		
		
		
		
		//4. Sacar todos los enfrentamientos
		//4.1. Se leen las categorias y se sacan todos los enfrentamientos por categoria, se guardan en el objeto sorteoEnfrentamientoCategoria
//		System.out.println();
		Integer countIdCec = 1;
		for (Entry<Integer, ?> entryCategoria : sorteoLiga.getSorteoCategoriasMap().entrySet()) {
			Object valueCategoria = entryCategoria.getValue();
			SorteoCategoria2021 sorteoCategoria = (SorteoCategoria2021) valueCategoria;
			//Crear el objeto de enfrentamientos para el equipo
			SorteoEnfrentamientosCategoria2021 sorteoEnfrentamientosCategoria = new SorteoEnfrentamientosCategoria2021();
			//Recorrer las categorias para sacar los enfrentamientos
			for (Entry<Integer, ?> entryEquipo1 : sorteoCategoria.getEquiposPorCategoria().entrySet()) {
				sorteoEnfrentamientosCategoria.setId(sorteoCategoria.getCategoria().getId());
				//Recuperar el equipo 1
				Object valueEquipo1 = entryEquipo1.getValue();
				Equipos2021 equipo1 = (Equipos2021) valueEquipo1;
				//Recorrer otros equipos
				for (Entry<Integer, ?> entryEquipo2 : sorteoCategoria.getEquiposPorCategoria().entrySet()) {
					Object valueEquipo2 = entryEquipo2.getValue();
					Equipos2021 equipo2 = (Equipos2021) valueEquipo2;
					if(equipo1.getId() != equipo2.getId()){
						//Obtener id de enfretamiento
						String idEnfrentamientoIda = equipo1.getId().toString() + "-" + equipo2.getId().toString();
						String idEnfrentamientoVta = equipo2.getId().toString() + "-" + equipo1.getId().toString();
						if(!sorteoEnfrentamientosCategoria.getCampeonatoEquiposCalendario().containsKey(idEnfrentamientoIda) &&
							!sorteoEnfrentamientosCategoria.getCampeonatoEquiposCalendario().containsKey(idEnfrentamientoIda)){
							//Crear enfrentamiento ida
							CampeonatoEquiposCalendario2021 cecIda = new CampeonatoEquiposCalendario2021();
							cecIda.setActivo(Activo2021.SI_NUMBER);
							cecIda.setCategoriaId(sorteoCategoria.getCategoria().getId());
							cecIda.setCategoria(sorteoCategoria.getCategoria());
							cecIda.setEquipo1(equipo1);
							cecIda.setEquipo1Id(equipo1.getId());
							cecIda.setEquipo2(equipo2);
							cecIda.setEquipo2Id(equipo2.getId());
							cecIda.setId(countIdCec);
							countIdCec++;
							cecIda.setIdVuelta(0);
							cecIda.setPgequipo1(0);
							cecIda.setPgequipo2(0);
							//Crear enfrentamiento vuelta
							CampeonatoEquiposCalendario2021 cecVta = new CampeonatoEquiposCalendario2021();
							cecVta.setActivo(Activo2021.SI_NUMBER);
							cecVta.setCategoriaId(sorteoCategoria.getCategoria().getId());
							cecVta.setCategoria(sorteoCategoria.getCategoria());
							cecVta.setEquipo1(equipo2);
							cecVta.setEquipo1Id(equipo2.getId());
							cecVta.setEquipo2(equipo1);
							cecVta.setEquipo2Id(equipo1.getId());
							cecVta.setId(countIdCec);
							countIdCec++;
							cecVta.setIdVuelta(0);
							cecVta.setPgequipo1(0);
							cecVta.setPgequipo2(0);
							cecIda.setIdVuelta(0);
							//Agregar enfrentamientos a sorteoEnfrentamientosCategoria
							sorteoEnfrentamientosCategoria.getCampeonatoEquiposCalendario().put(idEnfrentamientoIda, cecIda);
							sorteoEnfrentamientosCategoria.getCampeonatoEquiposCalendario().put(idEnfrentamientoVta, cecVta);
//							System.out.println("|**** ENFRENTAMIENTO ****| " + " *|* ID ENFRENTAMIENTO :" + idEnfrentamientoIda + cecIda.getPrintCec());
//							System.out.println("|**** ENFRENTAMIENTO ****| " + " *|* ID ENFRENTAMIENTO :" + idEnfrentamientoVta + cecVta.getPrintCec());
							
							
							
						}
					}
				}
			}
			sorteoLiga.getSorteoEnfrentamientosCategoriaMap().put(sorteoEnfrentamientosCategoria.getId(), sorteoEnfrentamientosCategoria);
			
			//Preparar tupla de encuentros valida por jornada
			
		}
		
		//Retorno sorteoLiga
		return sorteoLiga;
	}

	private boolean isBoleraPreferente(SorteoBolerasCrucesLiga2021 sbcl) {
		if(sbcl.getIdBoleraPreferente() != null && sbcl.getIdBoleraPreferente() > 0){
			return true;
		}
		return false;
	}
	
	private boolean isBoleraTerna(SorteoBolerasCrucesLiga2021 sbcl) {
		if(sbcl.getIdBoleraPreferente() != null && sbcl.getIdBoleraPreferente() == 0){
			if(
				(sbcl.getIdBolera1() != null && sbcl.getIdBolera1() > 0) ||
				(sbcl.getIdBolera2() != null && sbcl.getIdBolera2() > 0) ||
				(sbcl.getIdBolera3() != null && sbcl.getIdBolera3() > 0) ||
				(sbcl.getIdBolera4() != null && sbcl.getIdBolera4() > 0) ||
				(sbcl.getIdBolera5() != null && sbcl.getIdBolera5() > 0) ||
				(sbcl.getIdBolera6() != null && sbcl.getIdBolera6() > 0) ||
				(sbcl.getIdBolera7() != null && sbcl.getIdBolera7() > 0) ||
				(sbcl.getIdBolera8() != null && sbcl.getIdBolera8() > 0) 
			){
			return true;
			}
		}
		return false;
	}
	
	private boolean isBoleraVacia(SorteoBolerasCrucesLiga2021 sbcl) {
		if(sbcl.getIdBoleraPreferente() != null && sbcl.getIdBoleraPreferente() == 0){
			if(
				(sbcl.getIdBolera1() != null && sbcl.getIdBolera1() == 0) &&
				(sbcl.getIdBolera2() != null && sbcl.getIdBolera2() == 0) &&
				(sbcl.getIdBolera3() != null && sbcl.getIdBolera3() == 0) &&
				(sbcl.getIdBolera4() != null && sbcl.getIdBolera4() == 0) &&
				(sbcl.getIdBolera5() != null && sbcl.getIdBolera5() == 0) &&
				(sbcl.getIdBolera6() != null && sbcl.getIdBolera6() == 0) &&
				(sbcl.getIdBolera7() != null && sbcl.getIdBolera7() == 0) &&
				(sbcl.getIdBolera8() != null && sbcl.getIdBolera8() == 0) 
			){
			return true;
			}
		}
		return false;
	}
	
	private Equipos2021 buscarEquipoByBoleraCategoriaNoPreferente(List<Equipos2021> resultListEquipos, List<Boleras2021> resultListBoleras,
			LinkedHashMap<String, Equipos2021> sorteoBolerasCrucesLigaAsignadosMap, SorteoBolerasCrucesLiga2021 sbcl) {
		//Obtengo la terna de boleras
		List<Boleras2021> resultListBolerasTerna = new ArrayList();
		for(Boleras2021 bolera : resultListBoleras){
			if(sbcl.getIdBolera1() != null && sbcl.getIdBolera1() > 0 && bolera.getId().equals(sbcl.getIdBolera1())){
				resultListBolerasTerna.add(bolera);
			}
			if(sbcl.getIdBolera2() != null && sbcl.getIdBolera2() > 0 && bolera.getId().equals(sbcl.getIdBolera2())){
				resultListBolerasTerna.add(bolera);
			}
			if(sbcl.getIdBolera3() != null && sbcl.getIdBolera3() > 0 && bolera.getId().equals(sbcl.getIdBolera3())){
				resultListBolerasTerna.add(bolera);
			}
			if(sbcl.getIdBolera4() != null && sbcl.getIdBolera4() > 0 && bolera.getId().equals(sbcl.getIdBolera4())){
				resultListBolerasTerna.add(bolera);
			}
			if(sbcl.getIdBolera5() != null && sbcl.getIdBolera5() > 0 && bolera.getId().equals(sbcl.getIdBolera5())){
				resultListBolerasTerna.add(bolera);
			}
			if(sbcl.getIdBolera6() != null && sbcl.getIdBolera6() > 0 && bolera.getId().equals(sbcl.getIdBolera6())){
				resultListBolerasTerna.add(bolera);
			}
			if(sbcl.getIdBolera7() != null && sbcl.getIdBolera7() > 0 && bolera.getId().equals(sbcl.getIdBolera7())){
				resultListBolerasTerna.add(bolera);
			}
			if(sbcl.getIdBolera8() != null && sbcl.getIdBolera8() > 0 && bolera.getId().equals(sbcl.getIdBolera8())){
				resultListBolerasTerna.add(bolera);
			}
		}
		
		if(resultListBolerasTerna != null && resultListBolerasTerna.size() > 0){
			
			
			boolean isCorrectoEquipoAsignar = false;
			while(!isCorrectoEquipoAsignar){

				Random randBolera = new Random();
				int randomBoleraIndex = randBolera.nextInt(resultListBolerasTerna.size());
				Boleras2021 boleraRandom = resultListBolerasTerna.get(randomBoleraIndex);
				
				if(boleraRandom != null){
					List<Equipos2021> equiposBoleraCategoriaList = new ArrayList<Equipos2021>();
					for(Equipos2021 equipo : resultListEquipos){
						if(equipo.getBoleraId().equals(boleraRandom.getId())){
							if(equipo.getCategoriaId().equals(sbcl.getCategoria())){
								equiposBoleraCategoriaList.add(equipo);
							}
						}
					}
				
					if(equiposBoleraCategoriaList.size() > 0){
						Random rand = new Random();
						int randomIndex = rand.nextInt(equiposBoleraCategoriaList.size());
						Equipos2021 equipoRandom = equiposBoleraCategoriaList.get(randomIndex);
						if(!sorteoBolerasCrucesLigaAsignadosMap.containsKey(sbcl.getIdSearch())){
							boolean existeEquipoAsociado = false;
							for (Entry<String, Equipos2021> entryEquipo : sorteoBolerasCrucesLigaAsignadosMap.entrySet()) {
								Equipos2021 equipo = (Equipos2021) entryEquipo.getValue();
								if(equipoRandom != null && equipo != null){
									if(equipoRandom.getId().equals(equipo.getId())){
										existeEquipoAsociado = true;
									}
								}
							}
							if(!existeEquipoAsociado){
								return equipoRandom;
							}
						}
					}
				}
			}
			

		}
		return null;
	}

	private Equipos2021 buscarEquipoByBoleraCategoriaPreferente(List<Equipos2021> resultListEquipos, List<Boleras2021> resultListBoleras,
			LinkedHashMap<String, Equipos2021> sorteoBolerasCrucesLigaAsignadosMap, SorteoBolerasCrucesLiga2021 sbcl) {
		for(Boleras2021 bolera : resultListBoleras){
			if(bolera.getId().equals(sbcl.getIdBoleraPreferente())){
				List<Equipos2021> equiposBoleraCategoriaList = new ArrayList<Equipos2021>();
				for(Equipos2021 equipo : resultListEquipos){
					if(equipo.getBoleraId().equals(bolera.getId())){
						if(equipo.getCategoriaId().equals(sbcl.getCategoria())){
							equiposBoleraCategoriaList.add(equipo);
						}
					}
				}
				boolean isCorrectoEquipoAsignar = false;
				while(!isCorrectoEquipoAsignar){
					Random rand = new Random();
					int randomIndex = rand.nextInt(equiposBoleraCategoriaList.size());
					Equipos2021 equipoRandom = equiposBoleraCategoriaList.get(randomIndex);
					//Si no existe el equipo asignado en el cruce
					if(!sorteoBolerasCrucesLigaAsignadosMap.containsKey(sbcl.getIdSearch())){
						boolean existeEquipoAsociado = false;
						for (Entry<String, Equipos2021> entryEquipo : sorteoBolerasCrucesLigaAsignadosMap.entrySet()) {
							Equipos2021 equipo = (Equipos2021) entryEquipo.getValue();
							if(equipoRandom != null && equipo != null){
								if(equipoRandom.getId().equals(equipo.getId())){
									existeEquipoAsociado = true;
								}
							}
						}
						if(!existeEquipoAsociado){
							return equipoRandom;
						}
					}
				}
			}
		}
		return null;
	}
	
	public void preparateInsertRowCabecera(EstructuraCampeonatoLiga2021 ecl1, EstructuraCampeonatoLiga2021 ecl2, EstructuraCampeonatoLiga2021 ecl3) {
		insertRow(ecl1.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CAL_PRIMERA, ecl1.getInsertCabeceraCalendarioRow());
		insertRow(ecl2.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CAL_SEGUNDA, ecl2.getInsertCabeceraCalendarioRow());
		insertRow(ecl3.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CAL_TERCERA, ecl3.getInsertCabeceraCalendarioRow());
		insertRow(ecl1.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CLA_PRIMERA, ecl1.getInsertCabeceraClasificacionRow());
		insertRow(ecl2.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CLA_SEGUNDA, ecl2.getInsertCabeceraClasificacionRow());
		insertRow(ecl3.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CLA_TERCERA, ecl3.getInsertCabeceraClasificacionRow());
	}
	
	public void preparateInsertRow(EstructuraCampeonatoLiga2021 ecl, CampeonatoEquiposCalendario2021 cec) {
		if(cec.getCategoria().getId() == com.bolocelta.bbdd.constants.Categorias2021.PRIMERA){
			insertRow(ecl.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CAL_PRIMERA, cec.getInsertRow());
		}else if(cec.getCategoria().getId() == com.bolocelta.bbdd.constants.Categorias2021.SEGUNDA){
			insertRow(ecl.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CAL_SEGUNDA, cec.getInsertRow());
		}else if(cec.getCategoria().getId() == com.bolocelta.bbdd.constants.Categorias2021.TERCERA){
			insertRow(ecl.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CAL_TERCERA, cec.getInsertRow());
		}
	}
	
	public void preparateInsertRow(EstructuraCampeonatoLiga2021 ecl, CampeonatoEquiposClasificacion2021 cec) {
		if(cec.getCategoria().getId() == com.bolocelta.bbdd.constants.Categorias2021.PRIMERA){
			insertRow(ecl.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CLA_PRIMERA, cec.getInsertRow());
		}else if(cec.getCategoria().getId() == com.bolocelta.bbdd.constants.Categorias2021.SEGUNDA){
			insertRow(ecl.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CLA_SEGUNDA, cec.getInsertRow());
		}else if(cec.getCategoria().getId() == com.bolocelta.bbdd.constants.Categorias2021.TERCERA){
			insertRow(ecl.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CLA_TERCERA, cec.getInsertRow());
		}
	}
	
	private void insertRow(String excel, String newRowData) {
		insertDataCsv(excel, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_EQUIPOS, newRowData);
	}
	
	public void updateRowCalendario(Estructura2021 update, Long fila, Object newValue, CampeonatoEquiposCalendario2021 cec) {
		if(cec.getCategoria().getId() == com.bolocelta.bbdd.constants.Categorias2021.PRIMERA){
			updateDataCsv(update.getExcel(), Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_EQUIPOS, fila, update.getColumna(), newValue, update.getTipoDato());
		}else if(cec.getCategoria().getId() == com.bolocelta.bbdd.constants.Categorias2021.SEGUNDA){
			updateDataCsv(update.getExcel(), Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_EQUIPOS, fila, update.getColumna(), newValue, update.getTipoDato());
		}else if(cec.getCategoria().getId() == com.bolocelta.bbdd.constants.Categorias2021.TERCERA){
			updateDataCsv(update.getExcel(), Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_EQUIPOS, fila, update.getColumna(), newValue, update.getTipoDato());
		}
	}
	
	public void updateRowClasificacion(Estructura2021 update, Long fila, Object newValue, CampeonatoEquiposClasificacion2021 cec) {
		if(cec.getCategoria().getId() == com.bolocelta.bbdd.constants.Categorias2021.PRIMERA){
			updateDataCsv(update.getExcel(), Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_EQUIPOS, fila, update.getColumna(), newValue, update.getTipoDato());
		}else if(cec.getCategoria().getId() == com.bolocelta.bbdd.constants.Categorias2021.SEGUNDA){
			updateDataCsv(update.getExcel(), Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_EQUIPOS, fila, update.getColumna(), newValue, update.getTipoDato());
		}else if(cec.getCategoria().getId() == com.bolocelta.bbdd.constants.Categorias2021.TERCERA){
			updateDataCsv(update.getExcel(), Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_EQUIPOS, fila, update.getColumna(), newValue, update.getTipoDato());
		}
	}
	
	public void doValidarCalendario(){
		boolean errorEnSorteo = false;
		//Comprobar que el calendario es correcto
		List<CampeonatoEquiposCalendario2021> listResultAllCategorias = new ArrayList<CampeonatoEquiposCalendario2021>();
		//Agregar todos los enfrentamientos de liga
		LeerCampeonatoEquiposPrimera2021 leerCampeonatoEquiposPrimera = new LeerCampeonatoEquiposPrimera2021();
		List<CampeonatoEquiposCalendario2021> listResultCalendarioPrimera = (List<CampeonatoEquiposCalendario2021>) leerCampeonatoEquiposPrimera.listResultCalendario();
		listResultAllCategorias.addAll(listResultCalendarioPrimera);
		LeerCampeonatoEquiposSegunda2021 leerCampeonatoEquiposSegunda = new LeerCampeonatoEquiposSegunda2021();
		List<CampeonatoEquiposCalendario2021> listResultCalendarioSegunda = (List<CampeonatoEquiposCalendario2021>) leerCampeonatoEquiposSegunda.listResultCalendario();
		listResultAllCategorias.addAll(listResultCalendarioSegunda);
		LeerCampeonatoEquiposTercera2021 leerCampeonatoEquiposTercera = new LeerCampeonatoEquiposTercera2021();
		List<CampeonatoEquiposCalendario2021> listResultCalendarioTercera = (List<CampeonatoEquiposCalendario2021>) leerCampeonatoEquiposTercera.listResultCalendario();
		listResultAllCategorias.addAll(listResultCalendarioTercera);
		
		List<CampeonatoEquiposCalendario2021> listResultAllCategoriasSearch = new ArrayList<CampeonatoEquiposCalendario2021>();
		listResultAllCategoriasSearch.addAll(listResultAllCategorias);
		
		//1. Comprobar que no hay ninguna jornada asignada para la misma fecha y hora en la bolera
		for (CampeonatoEquiposCalendario2021 cec : listResultAllCategorias) {
			Integer idCec = cec.getId();
			for (CampeonatoEquiposCalendario2021 cecValidate : listResultAllCategoriasSearch) {
				if(!cecValidate.getId().equals(idCec)){
					if(cecValidate.getEquipo1().getBoleraId().equals(cec.getEquipo1().getBoleraId())){
						if(cecValidate.getFechaText().equals(cec.getFechaText())){
							if(cecValidate.getHora().equals(cec.getHora())){
								String message = "Hay enfrentamiento en la misma bolera, fecha y hora.";
								FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null));
								String message2 = "|**** ENFRENTAMIENTO CEC          ****| " + " *|* ID ENFRENTAMIENTO :" + cec.getPrintCec();
								FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message2, null));
								String message3 = "|**** ENFRENTAMIENTO CEC VALIDATE ****| " + " *|* ID ENFRENTAMIENTO :" + cecValidate.getPrintCec();
								FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message3, null));
								errorEnSorteo = true;
							}
						}
					}
				}
			}
			
		}
		//2. Comprobar que no haya coincidencia de horario y dia de Astures y San Roque por armador
//		Integer idEquipoSanRoque = 14;
//		Integer idEquipoAstures = 11;
//		//Comprobar que Astures no juegan cuando San Roque
//		for (CampeonatoEquiposCalendario2021 cec : listResultAllCategorias) {
//			if(cec.getEquipo1Id().equals(idEquipoSanRoque) || cec.getEquipo2Id().equals(idEquipoSanRoque)){
//				Integer idCec = cec.getId();
//				for (CampeonatoEquiposCalendario2021 cecValidate : listResultAllCategoriasSearch) {
//					if(!cecValidate.getId().equals(idCec)){
//						if(cecValidate.getEquipo1Id().equals(idEquipoAstures) || cecValidate.getEquipo2Id().equals(idEquipoAstures)){
//							if(!cecValidate.getEquipo1Id().equals(idEquipoSanRoque) && !cecValidate.getEquipo2Id().equals(idEquipoSanRoque)){
//								if(cecValidate.getFechaText().equals(cec.getFechaText())){
//									if(cecValidate.getHora().equals(cec.getHora())){
//										String message = "Hay enfrentamiento entre San Roque y Astures la misma fecha y hora.";
//										FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null));
//										String message2 = "|**** ENFRENTAMIENTO CEC          ****| " + " *|* ID ENFRENTAMIENTO :" + cec.getPrintCec();
//										FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message2, null));
//										String message3 = "|**** ENFRENTAMIENTO CEC VALIDATE ****| " + " *|* ID ENFRENTAMIENTO :" + cecValidate.getPrintCec();
//										FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message3, null));
//										errorEnSorteo = true;
//									}
//								}
//							}
//						}
//					}
//				}
//			}
//		}
		//Comprobar que San Roque no juegan cuando Astures 
//		for (CampeonatoEquiposCalendario2021 cec : listResultAllCategorias) {
//			if(cec.getEquipo1Id().equals(idEquipoAstures) || cec.getEquipo2Id().equals(idEquipoAstures)){
//				Integer idCec = cec.getId();
//				for (CampeonatoEquiposCalendario2021 cecValidate : listResultAllCategoriasSearch) {
//					if(!cecValidate.getId().equals(idCec)){
//						if(cecValidate.getEquipo1Id().equals(idEquipoSanRoque) || cecValidate.getEquipo2Id().equals(idEquipoSanRoque)){
//							if(!cecValidate.getEquipo1Id().equals(idEquipoAstures) && !cecValidate.getEquipo2Id().equals(idEquipoAstures)){
//								if(cecValidate.getFechaText().equals(cec.getFechaText())){
//									if(cecValidate.getHora().equals(cec.getHora())){
//										String message = "Hay enfrentamiento entre Astures y San Roque la misma fecha y hora.";
//										FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null));
//										String message2 = "|**** ENFRENTAMIENTO CEC          ****| " + " *|* ID ENFRENTAMIENTO :" + cec.getPrintCec();
//										FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message2, null));
//										String message3 = "|**** ENFRENTAMIENTO CEC VALIDATE ****| " + " *|* ID ENFRENTAMIENTO :" + cecValidate.getPrintCec();
//										FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message3, null));
//										errorEnSorteo = true;
//									}
//								}
//							}
//						}
//					}
//				}
//			}
//		}
		//3. Comprobar que no haya coincidencia de horario y dia de Panera A y Panera B por armador.
		Integer idEquipoLaPaneraA = 6;
		Integer idEquipoLaPaneraB = 16;
		//Comprobar que Panera A no juegan cuando Panera B
		for (CampeonatoEquiposCalendario2021 cec : listResultAllCategorias) {
			if(cec.getEquipo1Id().equals(idEquipoLaPaneraA) || cec.getEquipo2Id().equals(idEquipoLaPaneraA)){
				Integer idCec = cec.getId();
				for (CampeonatoEquiposCalendario2021 cecValidate : listResultAllCategoriasSearch) {
					if(!cecValidate.getId().equals(idCec)){
						if(cecValidate.getEquipo1Id().equals(idEquipoLaPaneraB) || cecValidate.getEquipo2Id().equals(idEquipoLaPaneraB)){
							if(!cecValidate.getEquipo1Id().equals(idEquipoLaPaneraA) && !cecValidate.getEquipo2Id().equals(idEquipoLaPaneraA)){
								if(cecValidate.getFechaText().equals(cec.getFechaText())){
									if(cecValidate.getHora().equals(cec.getHora())){
										if( (cec.getEquipo1Id().equals(idEquipoLaPaneraB) && cecValidate.getEquipo2Id().equals(idEquipoLaPaneraA)) ||
											(cec.getEquipo1Id().equals(idEquipoLaPaneraA) && cecValidate.getEquipo2Id().equals(idEquipoLaPaneraB))){
											String message = "Hay enfrentamiento entre Panera A y Panera B la misma fecha y hora.";
											FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null));
											String message2 = "|**** ENFRENTAMIENTO CEC          ****| " + " *|* ID ENFRENTAMIENTO :" + cec.getPrintCec();
											FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message2, null));
											String message3 = "|**** ENFRENTAMIENTO CEC VALIDATE ****| " + " *|* ID ENFRENTAMIENTO :" + cecValidate.getPrintCec();
											FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message3, null));
											errorEnSorteo = true;
										}
									}
								}
							}
						}
					}
				}
			}
		}
		//Comprobar que Panera B no juegan cuando Panera A 
		for (CampeonatoEquiposCalendario2021 cec : listResultAllCategorias) {
			if(cec.getEquipo1Id().equals(idEquipoLaPaneraB) || cec.getEquipo2Id().equals(idEquipoLaPaneraB)){
				Integer idCec = cec.getId();
				for (CampeonatoEquiposCalendario2021 cecValidate : listResultAllCategoriasSearch) {
					if(!cecValidate.getId().equals(idCec)){
						if(cecValidate.getEquipo1Id().equals(idEquipoLaPaneraA) || cecValidate.getEquipo2Id().equals(idEquipoLaPaneraA)){
							if(!cecValidate.getEquipo1Id().equals(idEquipoLaPaneraB) && !cecValidate.getEquipo2Id().equals(idEquipoLaPaneraB)){
								if(cecValidate.getFechaText().equals(cec.getFechaText())){
									if(cecValidate.getHora().equals(cec.getHora())){
										if( (cec.getEquipo1Id().equals(idEquipoLaPaneraB) && cecValidate.getEquipo2Id().equals(idEquipoLaPaneraA)) ||
											(cec.getEquipo1Id().equals(idEquipoLaPaneraA) && cecValidate.getEquipo2Id().equals(idEquipoLaPaneraB))){
											String message = "Hay enfrentamiento entre Panera B y Panera A la misma fecha y hora.";
											FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null));
											String message2 = "|**** ENFRENTAMIENTO CEC          ****| " + " *|* ID ENFRENTAMIENTO :" + cec.getPrintCec();
											FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message2, null));
											String message3 = "|**** ENFRENTAMIENTO CEC VALIDATE ****| " + " *|* ID ENFRENTAMIENTO :" + cecValidate.getPrintCec();
											FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message3, null));
											errorEnSorteo = true;
										}
									}
								}
							}
						}
					}
				}
			}
		}
		
		if(!errorEnSorteo){
			String message = "El sorteo es correcto. No hay incidencias en la validacion.";
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, message, null));
		}
	}
	
	public void actualizarResultadoCalendarioLigaConfirmar(CampeonatoEquiposCalendario2021 cec) {
		Long fila = cec.getRowNum();
		if(cec.getCategoriaId() == com.bolocelta.bbdd.constants.Categorias2021.PRIMERA){
			EstructuraCampeonatoLiga2021 ecl1 = new EstructuraCampeonatoLiga2021(
					NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CAL_PRIMERA,
					NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CLA_PRIMERA);
			for (Estructura2021 ef : ecl1.getCalendarioList()) {
				if (ef.getValor().equals(ecl1.COL_CALENDARIO_ACTIVO)) {
					updateRowCalendario(ef, fila, cec.getActivo(), cec);
				}
			}
		}else if(cec.getCategoriaId() == com.bolocelta.bbdd.constants.Categorias2021.SEGUNDA){
			EstructuraCampeonatoLiga2021 ecl2 = new EstructuraCampeonatoLiga2021(
					NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CAL_SEGUNDA,
					NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CLA_SEGUNDA);
			for (Estructura2021 ef : ecl2.getCalendarioList()) {
				if (ef.getValor().equals(ecl2.COL_CALENDARIO_ACTIVO)) {
					updateRowCalendario(ef, fila, cec.getActivo(), cec);
				}
			}
		}else if(cec.getCategoriaId() == com.bolocelta.bbdd.constants.Categorias2021.TERCERA){
			EstructuraCampeonatoLiga2021 ecl3 = new EstructuraCampeonatoLiga2021(
					NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CAL_TERCERA,
					NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CLA_TERCERA);
			for (Estructura2021 ef : ecl3.getCalendarioList()) {
				if (ef.getValor().equals(ecl3.COL_CALENDARIO_ACTIVO)) {
					updateRowCalendario(ef, fila, cec.getActivo(), cec);
				}
			}
		}
		
		
	}
	
	public void actualizarNombreFotoPizarra(CampeonatoEquiposCalendario2021 cec) {
		Long fila = cec.getRowNum();
		if(cec.getCategoriaId() == com.bolocelta.bbdd.constants.Categorias2021.PRIMERA){
			EstructuraCampeonatoLiga2021 ecl1 = new EstructuraCampeonatoLiga2021(
					NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CAL_PRIMERA,
					NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CLA_PRIMERA);
			for (Estructura2021 ef : ecl1.getCalendarioList()) {
				if (ef.getValor().equals(ecl1.COL_CALENDARIO_FOTO)) {
					updateRowCalendario(ef, fila, cec.getFoto(), cec);
				}
			}
		}else if(cec.getCategoriaId() == com.bolocelta.bbdd.constants.Categorias2021.SEGUNDA){
			EstructuraCampeonatoLiga2021 ecl2 = new EstructuraCampeonatoLiga2021(
					NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CAL_SEGUNDA,
					NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CLA_SEGUNDA);
			for (Estructura2021 ef : ecl2.getCalendarioList()) {
				if (ef.getValor().equals(ecl2.COL_CALENDARIO_FOTO)) {
					updateRowCalendario(ef, fila, cec.getFoto(), cec);
				}
			}
		}else if(cec.getCategoriaId() == com.bolocelta.bbdd.constants.Categorias2021.TERCERA){
			EstructuraCampeonatoLiga2021 ecl3 = new EstructuraCampeonatoLiga2021(
					NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CAL_TERCERA,
					NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CLA_TERCERA);
			for (Estructura2021 ef : ecl3.getCalendarioList()) {
				if (ef.getValor().equals(ecl3.COL_CALENDARIO_FOTO)) {
					updateRowCalendario(ef, fila, cec.getFoto(), cec);
				}
			}
		}
	}
	
	public void actualizarResultadoCalendarioLiga(CampeonatoEquiposCalendario2021 cec) {
		Long fila = cec.getRowNum();
		if(cec.getCategoriaId() == com.bolocelta.bbdd.constants.Categorias2021.PRIMERA){
			EstructuraCampeonatoLiga2021 ecl1 = new EstructuraCampeonatoLiga2021(
					NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CAL_PRIMERA,
					NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CLA_PRIMERA);
			for (Estructura2021 ef : ecl1.getCalendarioList()) {
				if (ef.getValor().equals(ecl1.COL_CALENDARIO_PGEQUIPO1)) {
					updateRowCalendario(ef, fila, cec.getPgequipo1(), cec);
				}else if (ef.getValor().equals(ecl1.COL_CALENDARIO_PGEQUIPO2)) {
					updateRowCalendario(ef, fila, cec.getPgequipo2(), cec);
				}
			}
		}else if(cec.getCategoriaId() == com.bolocelta.bbdd.constants.Categorias2021.SEGUNDA){
			EstructuraCampeonatoLiga2021 ecl2 = new EstructuraCampeonatoLiga2021(
					NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CAL_SEGUNDA,
					NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CLA_SEGUNDA);
			for (Estructura2021 ef : ecl2.getCalendarioList()) {
				if (ef.getValor().equals(ecl2.COL_CALENDARIO_PGEQUIPO1)) {
					updateRowCalendario(ef, fila, cec.getPgequipo1(), cec);
				}else if (ef.getValor().equals(ecl2.COL_CALENDARIO_PGEQUIPO2)) {
					updateRowCalendario(ef, fila, cec.getPgequipo2(), cec);
				}
			}
		}else if(cec.getCategoriaId() == com.bolocelta.bbdd.constants.Categorias2021.TERCERA){
			EstructuraCampeonatoLiga2021 ecl3 = new EstructuraCampeonatoLiga2021(
					NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CAL_TERCERA,
					NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CLA_TERCERA);
			for (Estructura2021 ef : ecl3.getCalendarioList()) {
				if (ef.getValor().equals(ecl3.COL_CALENDARIO_PGEQUIPO1)) {
					updateRowCalendario(ef, fila, cec.getPgequipo1(), cec);
				}else if (ef.getValor().equals(ecl3.COL_CALENDARIO_PGEQUIPO2)) {
					updateRowCalendario(ef, fila, cec.getPgequipo2(), cec);
				}
			}
		}
		
		
	}
	
	
	public void actualizarClasificacion(CampeonatoEquiposClasificacion2021 cec1, CampeonatoEquiposClasificacion2021 cec2, CampeonatoEquiposCalendario2021 cecCal) {
		
		Long filaClaEqu1 = cec1.getRowNum();
		Long filaClaEqu2 = cec2.getRowNum();
		
		
		
		if(cecCal.isModificable()){
		
			//Actualizar clasificacion de equipo 1
			if(cec1.getCategoriaId() == com.bolocelta.bbdd.constants.Categorias2021.PRIMERA){
				EstructuraCampeonatoLiga2021 ecl1 = new EstructuraCampeonatoLiga2021(
						NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CAL_PRIMERA,
						NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CLA_PRIMERA);
				for (Estructura2021 ef : ecl1.getClasificacionList()) {
					if (ef.getValor().equals(ecl1.COL_CLASIFICACION_J)) {
						cec1.setJugados(cec1.getJugados()+1);
						updateRowClasificacion(ef, filaClaEqu1, cec1.getJugados(), cec1);
					}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_G)) {
						if(cecCal.isGanaEquipo1()){
							cec1.setGanados(cec1.getGanados()+1);
						}
						updateRowClasificacion(ef, filaClaEqu1, cec1.getGanados(), cec1);
					}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_E)) {
						if(cecCal.isEmpate()){
							cec1.setEmpatados(cec1.getEmpatados()+1);
						}
						updateRowClasificacion(ef, filaClaEqu1, cec1.getEmpatados(), cec1);
					}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_P)) {
						if(cecCal.isGanaEquipo2()){
							cec1.setPerdidos(cec1.getPerdidos()+1);
						}
						updateRowClasificacion(ef, filaClaEqu1, cec1.getPerdidos(), cec1);
					}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PF)) {
						cec1.setPartidasFavor(cec1.getPartidasFavor()+cecCal.getPgequipo1());
						updateRowClasificacion(ef, filaClaEqu1, cec1.getPartidasFavor(), cec1);
					}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PC)) {
						cec1.setPartidasContra(cec1.getPartidasContra()+cecCal.getPgequipo2());
						updateRowClasificacion(ef, filaClaEqu1, cec1.getPartidasContra(), cec1);
					}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PT)) {
						if(cecCal.isGanaEquipo1()){
							cec1.setPuntos(cec1.getPuntos()+3);
						}else if(cecCal.isEmpate()){
							cec1.setPuntos(cec1.getPuntos()+1);
						}
						updateRowClasificacion(ef, filaClaEqu1, cec1.getPuntos(), cec1);
					}
				}
			}else if(cec1.getCategoriaId() == com.bolocelta.bbdd.constants.Categorias2021.SEGUNDA){
				EstructuraCampeonatoLiga2021 ecl2 = new EstructuraCampeonatoLiga2021(
						NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CAL_SEGUNDA,
						NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CLA_SEGUNDA);
				for (Estructura2021 ef : ecl2.getClasificacionList()) {
					if (ef.getValor().equals(ecl2.COL_CLASIFICACION_J)) {
						cec1.setJugados(cec1.getJugados()+1);
						updateRowClasificacion(ef, filaClaEqu1, cec1.getJugados(), cec1);
					}else if (ef.getValor().equals(ecl2.COL_CLASIFICACION_G)) {
						if(cecCal.isGanaEquipo1()){
							cec1.setGanados(cec1.getGanados()+1);
						}
						updateRowClasificacion(ef, filaClaEqu1, cec1.getGanados(), cec1);
					}else if (ef.getValor().equals(ecl2.COL_CLASIFICACION_E)) {
						if(cecCal.isEmpate()){
							cec1.setEmpatados(cec1.getEmpatados()+1);
						}
						updateRowClasificacion(ef, filaClaEqu1, cec1.getEmpatados(), cec1);
					}else if (ef.getValor().equals(ecl2.COL_CLASIFICACION_P)) {
						if(cecCal.isGanaEquipo2()){
							cec1.setPerdidos(cec1.getPerdidos()+1);
						}
						updateRowClasificacion(ef, filaClaEqu1, cec1.getPerdidos(), cec1);
					}else if (ef.getValor().equals(ecl2.COL_CLASIFICACION_PF)) {
						cec1.setPartidasFavor(cec1.getPartidasFavor()+cecCal.getPgequipo1());
						updateRowClasificacion(ef, filaClaEqu1, cec1.getPartidasFavor(), cec1);
					}else if (ef.getValor().equals(ecl2.COL_CLASIFICACION_PC)) {
						cec1.setPartidasContra(cec1.getPartidasContra()+cecCal.getPgequipo2());
						updateRowClasificacion(ef, filaClaEqu1, cec1.getPartidasContra(), cec1);
					}else if (ef.getValor().equals(ecl2.COL_CLASIFICACION_PT)) {
						if(cecCal.isGanaEquipo1()){
							cec1.setPuntos(cec1.getPuntos()+3);
						}else if(cecCal.isEmpate()){
							cec1.setPuntos(cec1.getPuntos()+1);
						}
						updateRowClasificacion(ef, filaClaEqu1, cec1.getPuntos(), cec1);
					}
				}
			}else if(cec1.getCategoriaId() == com.bolocelta.bbdd.constants.Categorias2021.TERCERA){
				EstructuraCampeonatoLiga2021 ecl3 = new EstructuraCampeonatoLiga2021(
						NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CAL_TERCERA,
						NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CLA_TERCERA);
				for (Estructura2021 ef : ecl3.getClasificacionList()) {
					if (ef.getValor().equals(ecl3.COL_CLASIFICACION_J)) {
						cec1.setJugados(cec1.getJugados()+1);
						updateRowClasificacion(ef, filaClaEqu1, cec1.getJugados(), cec1);
					}else if (ef.getValor().equals(ecl3.COL_CLASIFICACION_G)) {
						if(cecCal.isGanaEquipo1()){
							cec1.setGanados(cec1.getGanados()+1);
						}
						updateRowClasificacion(ef, filaClaEqu1, cec1.getGanados(), cec1);
					}else if (ef.getValor().equals(ecl3.COL_CLASIFICACION_E)) {
						if(cecCal.isEmpate()){
							cec1.setEmpatados(cec1.getEmpatados()+1);
						}
						updateRowClasificacion(ef, filaClaEqu1, cec1.getEmpatados(), cec1);
					}else if (ef.getValor().equals(ecl3.COL_CLASIFICACION_P)) {
						if(cecCal.isGanaEquipo2()){
							cec1.setPerdidos(cec1.getPerdidos()+1);
						}
						updateRowClasificacion(ef, filaClaEqu1, cec1.getPerdidos(), cec1);
					}else if (ef.getValor().equals(ecl3.COL_CLASIFICACION_PF)) {
						cec1.setPartidasFavor(cec1.getPartidasFavor()+cecCal.getPgequipo1());
						updateRowClasificacion(ef, filaClaEqu1, cec1.getPartidasFavor(), cec1);
					}else if (ef.getValor().equals(ecl3.COL_CLASIFICACION_PC)) {
						cec1.setPartidasContra(cec1.getPartidasContra()+cecCal.getPgequipo2());
						updateRowClasificacion(ef, filaClaEqu1, cec1.getPartidasContra(), cec1);
					}else if (ef.getValor().equals(ecl3.COL_CLASIFICACION_PT)) {
						if(cecCal.isGanaEquipo1()){
							cec1.setPuntos(cec1.getPuntos()+3);
						}else if(cecCal.isEmpate()){
							cec1.setPuntos(cec1.getPuntos()+1);
						}
						updateRowClasificacion(ef, filaClaEqu1, cec1.getPuntos(), cec1);
					}
				}
			}
			
			
			//Actualizar clasificacion de equipo 2
			if(cec2.getCategoriaId() == com.bolocelta.bbdd.constants.Categorias2021.PRIMERA){
				EstructuraCampeonatoLiga2021 ecl1 = new EstructuraCampeonatoLiga2021(
						NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CAL_PRIMERA,
						NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CLA_PRIMERA);
				for (Estructura2021 ef : ecl1.getClasificacionList()) {
					if (ef.getValor().equals(ecl1.COL_CLASIFICACION_J)) {
						cec2.setJugados(cec2.getJugados()+1);
						updateRowClasificacion(ef, filaClaEqu2, cec2.getJugados(), cec2);
					}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_G)) {
						if(cecCal.isGanaEquipo2()){
							cec2.setGanados(cec2.getGanados()+1);
						}
						updateRowClasificacion(ef, filaClaEqu2, cec2.getGanados(), cec2);
					}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_E)) {
						if(cecCal.isEmpate()){
							cec2.setEmpatados(cec2.getEmpatados()+1);
						}
						updateRowClasificacion(ef, filaClaEqu2, cec2.getEmpatados(), cec2);
					}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_P)) {
						if(cecCal.isGanaEquipo1()){
							cec2.setPerdidos(cec2.getPerdidos()+1);
						}
						updateRowClasificacion(ef, filaClaEqu2, cec2.getPerdidos(), cec2);
					}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PF)) {
						cec2.setPartidasFavor(cec2.getPartidasFavor()+cecCal.getPgequipo2());
						updateRowClasificacion(ef, filaClaEqu2, cec2.getPartidasFavor(), cec2);
					}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PC)) {
						cec2.setPartidasContra(cec2.getPartidasContra()+cecCal.getPgequipo1());
						updateRowClasificacion(ef, filaClaEqu2, cec2.getPartidasContra(), cec2);
					}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PT)) {
						if(cecCal.isGanaEquipo2()){
							cec2.setPuntos(cec2.getPuntos()+3);
						}else if(cecCal.isEmpate()){
							cec2.setPuntos(cec2.getPuntos()+1);
						}
						updateRowClasificacion(ef, filaClaEqu2, cec2.getPuntos(), cec2);
					}
				}
			}else if(cec2.getCategoriaId() == com.bolocelta.bbdd.constants.Categorias2021.SEGUNDA){
				EstructuraCampeonatoLiga2021 ecl2 = new EstructuraCampeonatoLiga2021(
						NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CAL_SEGUNDA,
						NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CLA_SEGUNDA);
				for (Estructura2021 ef : ecl2.getClasificacionList()) {
					if (ef.getValor().equals(ecl2.COL_CLASIFICACION_J)) {
						cec2.setJugados(cec2.getJugados()+1);
						updateRowClasificacion(ef, filaClaEqu2, cec2.getJugados(), cec2);
					}else if (ef.getValor().equals(ecl2.COL_CLASIFICACION_G)) {
						if(cecCal.isGanaEquipo2()){
							cec2.setGanados(cec2.getGanados()+1);
						}
						updateRowClasificacion(ef, filaClaEqu2, cec2.getGanados(), cec2);
					}else if (ef.getValor().equals(ecl2.COL_CLASIFICACION_E)) {
						if(cecCal.isEmpate()){
							cec2.setEmpatados(cec2.getEmpatados()+1);
						}
						updateRowClasificacion(ef, filaClaEqu2, cec2.getEmpatados(), cec2);
					}else if (ef.getValor().equals(ecl2.COL_CLASIFICACION_P)) {
						if(cecCal.isGanaEquipo1()){
							cec2.setPerdidos(cec2.getPerdidos()+1);
						}
						updateRowClasificacion(ef, filaClaEqu2, cec2.getPerdidos(), cec2);
					}else if (ef.getValor().equals(ecl2.COL_CLASIFICACION_PF)) {
						cec2.setPartidasFavor(cec2.getPartidasFavor()+cecCal.getPgequipo2());
						updateRowClasificacion(ef, filaClaEqu2, cec2.getPartidasFavor(), cec2);
					}else if (ef.getValor().equals(ecl2.COL_CLASIFICACION_PC)) {
						cec2.setPartidasContra(cec2.getPartidasContra()+cecCal.getPgequipo1());
						updateRowClasificacion(ef, filaClaEqu2, cec2.getPartidasContra(), cec2);
					}else if (ef.getValor().equals(ecl2.COL_CLASIFICACION_PT)) {
						if(cecCal.isGanaEquipo2()){
							cec2.setPuntos(cec2.getPuntos()+3);
						}else if(cecCal.isEmpate()){
							cec2.setPuntos(cec2.getPuntos()+1);
						}
						updateRowClasificacion(ef, filaClaEqu2, cec2.getPuntos(), cec2);
					}
				}
			}else if(cec2.getCategoriaId() == com.bolocelta.bbdd.constants.Categorias2021.TERCERA){
				EstructuraCampeonatoLiga2021 ecl3 = new EstructuraCampeonatoLiga2021(
						NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CAL_TERCERA,
						NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CLA_TERCERA);
				for (Estructura2021 ef : ecl3.getClasificacionList()) {
					if (ef.getValor().equals(ecl3.COL_CLASIFICACION_J)) {
						cec2.setJugados(cec2.getJugados()+1);
						updateRowClasificacion(ef, filaClaEqu2, cec2.getJugados(), cec2);
					}else if (ef.getValor().equals(ecl3.COL_CLASIFICACION_G)) {
						if(cecCal.isGanaEquipo2()){
							cec2.setGanados(cec2.getGanados()+1);
						}
						updateRowClasificacion(ef, filaClaEqu2, cec2.getGanados(), cec2);
					}else if (ef.getValor().equals(ecl3.COL_CLASIFICACION_E)) {
						if(cecCal.isEmpate()){
							cec2.setEmpatados(cec2.getEmpatados()+1);
						}
						updateRowClasificacion(ef, filaClaEqu2, cec2.getEmpatados(), cec2);
					}else if (ef.getValor().equals(ecl3.COL_CLASIFICACION_P)) {
						if(cecCal.isGanaEquipo1()){
							cec2.setPerdidos(cec2.getPerdidos()+1);
						}
						updateRowClasificacion(ef, filaClaEqu2, cec2.getPerdidos(), cec2);
					}else if (ef.getValor().equals(ecl3.COL_CLASIFICACION_PF)) {
						cec2.setPartidasFavor(cec2.getPartidasFavor()+cecCal.getPgequipo2());
						updateRowClasificacion(ef, filaClaEqu2, cec2.getPartidasFavor(), cec2);
					}else if (ef.getValor().equals(ecl3.COL_CLASIFICACION_PC)) {
						cec2.setPartidasContra(cec2.getPartidasContra()+cecCal.getPgequipo1());
						updateRowClasificacion(ef, filaClaEqu2, cec2.getPartidasContra(), cec2);
					}else if (ef.getValor().equals(ecl3.COL_CLASIFICACION_PT)) {
						if(cecCal.isGanaEquipo2()){
							cec2.setPuntos(cec2.getPuntos()+3);
						}else if(cecCal.isEmpate()){
							cec2.setPuntos(cec2.getPuntos()+1);
						}
						updateRowClasificacion(ef, filaClaEqu2, cec2.getPuntos(), cec2);
					}
				}
			}
		}
	}
		
		
		
		
	
	
	

}
