package com.bolocelta.bbdd.createTable;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFCreationHelper;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.bolocelta.bbdd.constants.NombresTablas2021;
import com.bolocelta.validator.EmailValidator2021;
import com.bolocelta.validator.TelefonoValidator2021;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

public abstract class ACrearModificar2021 implements ICrearModificar {
	
	public TelefonoValidator2021 telefonoValidator = new TelefonoValidator2021();
	public EmailValidator2021 emailValidator = new EmailValidator2021();
	
	public void createWorkBook(String nameFile, String path) {

		// Creamos el archivo donde almacenaremos la hoja
        // de calculo, recuerde usar la extension correcta,
        // en este caso .xlsx
        File archivo = new File(path + nameFile);

        // Creamos el libro de trabajo de Excel formato OOXML
        XSSFWorkbook workbook = new XSSFWorkbook();
        
     // Ahora guardaremos el archivo
        try {
            // Creamos el flujo de salida de datos,
            // apuntando al archivo donde queremos 
            // almacenar el libro de Excel
            FileOutputStream salida = new FileOutputStream(archivo);

            // Almacenamos el libro de 
            // Excel via ese 
            // flujo de datos
            workbook.write(salida);

            // Cerramos el libro para concluir operaciones
            workbook.close();


        } catch (FileNotFoundException ex) {
        	System.out.println("El fichero no se encuentra. Error creando workbookXLS.");
			System.out.println(ex.getMessage());
        } catch (IOException ex) {
        	System.out.println("Error creando workbookXLS.");
			System.out.println(ex.getMessage());
        }

	}
	
	public void createWorkBookCsv(String nameFile, String path) {
		try {
			BufferedWriter writer = Files.newBufferedWriter(Paths.get(path + nameFile));
			CSVFormat fmt = CSVFormat.EXCEL.withDelimiter(';');
			CSVPrinter csvPrinter = new CSVPrinter(writer, fmt);
			csvPrinter.flush();
		}catch (Exception e) {
			System.out.println("Error creando workbook CSV.");
			System.out.println(e.getMessage());
		}

	}
	
	public void createSheet(String nameFile, String path, String nameSheet) {
		
		
		try {
			FileInputStream inputStream = new FileInputStream(path + nameFile);
			Workbook workbook = WorkbookFactory.create(inputStream);

			Sheet sheet = workbook.createSheet(nameSheet);

			inputStream.close();

			FileOutputStream outputStream = new FileOutputStream(path + nameFile);
			workbook.write(outputStream);
			workbook.close();
			outputStream.close();
			
		} catch (Exception e) {
			System.out.println("Error creando hoja del excel.");
			System.out.println(e.getMessage());
		}
        
	}
	
	public void updateData(String nameFile, String path, String nameSheet, Long row, Integer cell, Object value, String tipoDato) {
		
		
		try {
			
			
			FileInputStream file = new FileInputStream(new File(path + nameFile));
	        
			// Leer archivo excel
			@SuppressWarnings("resource")
			XSSFWorkbook workbook = new XSSFWorkbook(file);
			// Obtener la hoja que se va leer
			XSSFSheet sheet = workbook.getSheet(nameSheet);
	        
			
			
			Row rowSheet = sheet.getRow(row.intValue());
			
			if(rowSheet == null){
				rowSheet = sheet.createRow(row.intValue());
			}
		        
			Cell cell2Update = rowSheet.getCell(cell-1);
			
			if(cell2Update == null){
				cell2Update = rowSheet.createCell(cell-1);
			}
	        
	        if(cell2Update != null) {
			          
	        	if(tipoDato.equals(NombresTablas2021.TD_ENTERO)){
	        		cell2Update.setCellType(CellType.NUMERIC);
					cell2Update.setCellValue((Integer) value);
				}else if(tipoDato.equals(NombresTablas2021.TD_DECIMAL)){
					cell2Update.setCellType(CellType.NUMERIC);
					cell2Update.setCellValue((Double) value);
				}else if(tipoDato.equals(NombresTablas2021.TD_TEXTO)){
					cell2Update.setCellType(CellType.STRING);
					cell2Update.setCellValue((String) value);
				}else if(tipoDato.equals(NombresTablas2021.TD_FECHA)){
					XSSFCreationHelper createHelper = workbook.getCreationHelper();
					XSSFCellStyle cellStyle = workbook.createCellStyle();
					cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd/MM/yyyy"));
					cell2Update.setCellStyle(cellStyle);
					cell2Update.setCellValue((Date) value);
				}
			        	
	        }

			file.close();

			FileOutputStream outputStream = new FileOutputStream(path + nameFile);
			workbook.write(outputStream);
			workbook.close();
			outputStream.close();
			
		} catch (Exception ex) {
			System.out.println("Error actualizacnodo linea en excel.");
			System.out.println(ex.getMessage());
		}
        
	}
	
	public void insertDataCsv(String nameFile, String path, String newRowData) {
		
		try {
			
			File inputFile = new File(path + nameFile);
			// Read existing file
			CSVReader reader = new CSVReader(new FileReader(inputFile), ';');
			List<String[]> csvBody = reader.readAll();
			reader.close();
			
			//Agregar new row
			String [] record = newRowData.split(";");
			csvBody.add(record);
	
	        // Write to CSV file which is open
	        CSVWriter writer = new CSVWriter(new FileWriter(inputFile), ';');
	        writer.writeAll(csvBody);
	        writer.flush();
	        writer.close();
			
//			File inputFile = new File(path + nameFile);
//			// Read existing file
//			CSVWriter  writer = new CSVWriter(new FileWriter(inputFile), ';');
//			// Preparate new row
//			String [] record = newRowData.split(";");
//			// Save record
//			writer.writeNext(record);
//			writer.close();
		} catch(IOException e) {
		    System.out.println(" There was an Error Reading the file. " + e.getMessage());
		}
        
	}
	
	public void updateDataCsv(String nameFile, String path, Long row, Integer cell, Object value, String tipoDato) {
		
		try {
			File inputFile = new File(path + nameFile);
			// Read existing file
			CSVReader reader = new CSVReader(new FileReader(inputFile), ';');
			List<String[]> csvBody = reader.readAll();
			// get CSV row column and replace with by using row and column
			for (int i = 0; i < csvBody.size(); i++) {
				if(i == row.intValue()){
					String[] strArray = csvBody.get(i);
					for (int j = 0; j < strArray.length; j++) {
						if(j == (cell-1)){
							if(value == null) value = "";
							csvBody.get(i)[j] = value.toString();
							break;
						}
					}
				}
			}
			reader.close();
	
	        // Write to CSV file which is open
	        CSVWriter writer = new CSVWriter(new FileWriter(inputFile), ';');
	        writer.writeAll(csvBody);
	        writer.flush();
	        writer.close();
		} catch(IOException e) {
		    System.out.println(" There was an Error Reading the file. " + e.getMessage());
		}
        
	}
	
	public boolean existFile(String nameFile, String path){
		File archivo = new File(path+nameFile);
		if (!archivo.exists()) {
		    return false;
		}
		return true;
	}
	
	public void vaciarFile(String nameFile, String path){
		File archivo = new File(path+nameFile);
		if (archivo.exists()) {
			try {
				BufferedWriter writer = Files.newBufferedWriter(Paths.get(path + nameFile));
				CSVFormat fmt = CSVFormat.EXCEL.withDelimiter(';');
				CSVPrinter csvPrinter = new CSVPrinter(writer, fmt);
				csvPrinter.flush();
				System.out.println("Fichero workbook CSV vaciado.");
			}catch (Exception e) {
				System.out.println("Error limpiando workbook CSV.");
				System.out.println(e.getMessage());
			}
		}
	}
	
	public boolean deleteFile(String nameFile, String path){
		File archivo = new File(path+nameFile);
		if (archivo.exists()) {
			archivo.delete();
//			boolean result = archivo.delete();
//			if(result){
//				System.out.println("Fichero workbook CSV borrado.");
//			}else{
//				vaciarFile(nameFile, path);
				return true;
//			}
//			return result; 
		}
		return false;
	}
	

}
