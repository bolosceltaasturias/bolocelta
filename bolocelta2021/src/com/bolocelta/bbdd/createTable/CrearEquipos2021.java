package com.bolocelta.bbdd.createTable;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import com.bolocelta.bbdd.constants.Ubicaciones2021;
import com.bolocelta.bbdd.constants.structure.Estructura2021;
import com.bolocelta.bbdd.constants.structure.EstructuraEquipos2021;
import com.bolocelta.entities.Equipos2021;
import com.bolocelta.entities.Jugadores2021;

public class CrearEquipos2021 extends ACrearModificar2021 {
	
	public void preparateUpdateHorarioPreferente(Equipos2021 equipo, String colHorarioPreferente) {

		EstructuraEquipos2021 eq = new EstructuraEquipos2021();

		Long fila = equipo.getRowNum();

		for (Estructura2021 update : eq.getList()) {
			// Cabeceras
			if (update.getValor().equals(colHorarioPreferente) && update.getValor().equals(eq.COL_HORARIO_PSM)) {
				updateRow(update, fila, equipo.getHorarioPreferenteSabadoMaņana());
			}else if (update.getValor().equals(colHorarioPreferente) && update.getValor().equals(eq.COL_HORARIO_PST)) {
				updateRow(update, fila, equipo.getHorarioPreferenteSabadoTarde());
			}else if (update.getValor().equals(colHorarioPreferente) && update.getValor().equals(eq.COL_HORARIO_PDM)) {
				updateRow(update, fila, equipo.getHorarioPreferenteDomingoMaņana());
			}else if (update.getValor().equals(colHorarioPreferente) && update.getValor().equals(eq.COL_HORARIO_PDT)) {
				updateRow(update, fila, equipo.getHorarioPreferenteDomingoTarde());
			}
		}
	}

	public void preparateUpdateEmail(Equipos2021 equipo) {

		EstructuraEquipos2021 eq = new EstructuraEquipos2021();

		Long fila = equipo.getRowNum();

		for (Estructura2021 update : eq.getList()) {
			// Cabeceras
			if (update.getValor().equals(eq.COL_EMAIL)) {
				updateRow(update, fila, equipo.getEmail());
			}
		}
	}

	private void updateRow(Estructura2021 update, Long fila, Object newValue) {
		updateDataCsv(update.getExcel(), Ubicaciones2021.UBICACION_BBDD_ADMINISTRACION, fila, update.getColumna(), newValue, update.getTipoDato());
	}
	
	public boolean validateEmail(Equipos2021 equipo){
		boolean valid = true;
		if(equipo.getEmail() != null && !equipo.getEmail().isEmpty() && !emailValidator.validate(equipo.getEmail())){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El email no es valido", null));
			valid = false;
		}
		return valid;
	}
	
}
