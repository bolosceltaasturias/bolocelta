package com.bolocelta.bbdd.createTable;

public interface ICrearModificar {
		
	public void createWorkBook(String nameFile, String path);
	
	public void createSheet(String nameFile, String path, String nameSheet);

	public void updateData(String nameFile, String path, String nameSheet, Long row, Integer cell, Object value, String tipoDato);
}
