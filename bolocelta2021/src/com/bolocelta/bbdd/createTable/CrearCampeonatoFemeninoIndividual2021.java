package com.bolocelta.bbdd.createTable;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.bolocelta.bbdd.constants.Activo2021;
import com.bolocelta.bbdd.constants.EstadosIndividualParejas2021;
import com.bolocelta.bbdd.constants.NombresTablas2021;
import com.bolocelta.bbdd.constants.Ubicaciones2021;
import com.bolocelta.bbdd.constants.structure.Estructura2021;
import com.bolocelta.bbdd.constants.structure.EstructuraCampeonatoFemeninoIndividual2021;
import com.bolocelta.bbdd.readTables.LeerCampeonatoFemeninoIndividual2021;
import com.bolocelta.entities.Categorias2021;
import com.bolocelta.entities.Fases2021;
import com.bolocelta.entities.Jugadores2021;
import com.bolocelta.entities.ParticipantesIndividual2021;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseCF2021;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseFC2021;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseFF2021;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseI2021;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseOF2021;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseSF2021;
import com.bolocelta.entities.sorteos.individual.ClasificacionFaseI2021;

public class CrearCampeonatoFemeninoIndividual2021 extends ACrearModificar2021 {

	LeerCampeonatoFemeninoIndividual2021 leerCampeonatoFemeninoIndividual = new LeerCampeonatoFemeninoIndividual2021();

	private void crearExcel() {

	}
	
	private void crearHojaConfiguracion(Date fechaMaximaInscripcion, String estado, String bolera){
		createWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CONFIG,
				Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO);
	}
	
	private void crearHojaFases(){
		createWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_FASES,
				Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO);
	}
	
	private void crearHojaParticipantes(){
		createWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_PARTICIPANTES,
				Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO);
	}
	
	private void crearHojaGrupos(){
		createWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_GRUPOS,
				Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO);
	}
	
	private void crearHojaClasificacionFaseI(){
		createWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CLA_FASE_I,
				Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO);
	}
	
	private void crearHojaCalendarioFaseI(){
		createWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CAL_FASE_I,
				Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO);
	}
	
	private void crearHojaClasificacionFaseII(){
		createWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CLA_FASE_II,
				Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO);
	}
	
	private void crearHojaCalendarioFaseII(){
		createWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CAL_FASE_II,
				Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO);
	}
	
	private void crearHojaCalendarioFaseOF(){
		createWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CAL_FASE_OF,
				Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO);
	}
	
	private void crearHojaCalendarioFaseCF(){
		createWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CAL_FASE_CF,
				Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO);
	}
	
	private void crearHojaCalendarioFaseSF(){
		createWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CAL_FASE_SF,
				Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO);
	}
	
	private void crearHojaCalendarioFaseFC(){
		createWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CAL_FASE_FC,
				Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO);
	}
	
	private void crearHojaCalendarioFaseFF(){
		createWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CAL_FASE_FF,
				Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO);
	}
	
	private void insertRow(String excel, String newRowData) {
		insertDataCsv(excel, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO, newRowData);
	}
	
	public void updateRow(Estructura2021 update, Long fila, Object newValue) {
		updateDataCsv(update.getExcel(), Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO, fila, update.getColumna(), newValue, update.getTipoDato());
	}
	
	
	//Creacion de la base del campeonato
	public void crearBaseCampeonato(Date fechaMaximaInscripcion, String estado, String bolera){
		//Crear las hojas excel
		crearExcel();
		crearHojaConfiguracion(fechaMaximaInscripcion, estado, bolera);
		crearHojaFases();
		crearHojaParticipantes();
		
		
		//Crear la estructura de las hojas (cabeceras y registros por defecto)
		EstructuraCampeonatoFemeninoIndividual2021 ecmi = new EstructuraCampeonatoFemeninoIndividual2021();
		//Crear las estructuras de las hojas
		crearEstructuraHojas(ecmi);
		//Rellenar datos de configuración
		rellenarDatosConfiguracion(ecmi, fechaMaximaInscripcion, estado, bolera);
		//Rellenar todas las fases
		rellenarDatosFases(ecmi);
	}
	
	public void crearEstructuraHojas(EstructuraCampeonatoFemeninoIndividual2021 ecmi) {
		//Configuracion
		for (String row : ecmi.getInsertCabeceraConfiguracionRow()) {
			insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CONFIG, row);
		}
		//Fases
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_FASES, ecmi.getInsertCabeceraFasesRow());
		//Participantes
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_PARTICIPANTES, ecmi.getInsertCabeceraParticipantesRow());
	}

	private void rellenarDatosConfiguracion(EstructuraCampeonatoFemeninoIndividual2021 ecmi, Date fechaMaximaInscripcion, String estado, 
			String bolera) {
		//Actualizar la fecha
	    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy"); // date pattern
	    String fechaMaxInscripcion = simpleDateFormat.format(fechaMaximaInscripcion);
		updateRow(ecmi.getEstructuraFechaMaximaInscripcion(), ecmi.getEstructuraFechaMaximaInscripcion().getFila(), fechaMaxInscripcion);
		//Actualizar el estado
		updateRow(ecmi.getEstructuraEstadoCampeonato(), ecmi.getEstructuraEstadoCampeonato().getFila(), estado);
		updateRow(ecmi.getEstructuraBoleraFinal(), ecmi.getEstructuraBoleraFinal().getFila(), bolera);
	}
	
	public void actualizarEstadoConfiguracion(String estado) {
		EstructuraCampeonatoFemeninoIndividual2021 ecmi = new EstructuraCampeonatoFemeninoIndividual2021();
		//Actualizar el estado
		updateRow(ecmi.getEstructuraEstadoCampeonato(), ecmi.getEstructuraEstadoCampeonato().getFila(), estado);
	}
	
	private void rellenarDatosFases(EstructuraCampeonatoFemeninoIndividual2021 ecmi) {
		for(String fase : ecmi.getEstructuraTodasFases()){
			insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_FASES, fase);
		}
	}
	
	public void inscribirNewJugador(Jugadores2021 jugador, Categorias2021 categoria, String usuario, String activo) {
		EstructuraCampeonatoFemeninoIndividual2021 ecmi = new EstructuraCampeonatoFemeninoIndividual2021();
    	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss"); // date pattern
    	String fecha = simpleDateFormat.format(new Date());
    	
    	Long newRowNum = leerCampeonatoFemeninoIndividual.lastRowSheet(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_PARTICIPANTES, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO);
    	newRowNum++;
    	
	    String insert = newRowNum.toString() + ";" + jugador.getId().toString() + ";" + fecha + ";" + usuario + ";" + activo;
	    insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_PARTICIPANTES, insert);
	}
	
	public void inscribirUpdateJugador(ParticipantesIndividual2021 participantesIndividual, Jugadores2021 jugador, Categorias2021 categoria, String usuario, String activo) {
		EstructuraCampeonatoFemeninoIndividual2021 ecmi = new EstructuraCampeonatoFemeninoIndividual2021();
		
		Long fila = participantesIndividual.getRowNum();
		
		for (Estructura2021 eip : ecmi.getEstructuraInscribirParticipantes()) {
			
			// Cabeceras
			if (eip.getValor().equals(ecmi.COL_PARTICIPANTES_ID)) {
				updateRow(eip, fila, fila.toString());
			} else if (eip.getValor().equals(ecmi.COL_PARTICIPANTES_ID_JUGADOR)) {
				updateRow(eip, fila, jugador.getId().toString());
			} else if (eip.getValor().equals(ecmi.COL_PARTICIPANTES_USUARIO)) {
				updateRow(eip, fila, usuario);
			} else if (eip.getValor().equals(ecmi.COL_PARTICIPANTES_FECHA_INSCRIPCION)) {
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss"); // date pattern
			    String fecha = simpleDateFormat.format(new Date());
				updateRow(eip, fila, fecha);
			} else if (eip.getValor().equals(ecmi.COL_PARTICIPANTES_ACTIVO)) {
				updateRow(eip, fila, activo);
			}

		}
	}
	
	
	public void actualizarActivoFase(Fases2021 fase) {
		EstructuraCampeonatoFemeninoIndividual2021 ecmi = new EstructuraCampeonatoFemeninoIndividual2021();
		Long fila = fase.getRowNum();
		for (Estructura2021 ef : ecmi.getEstructuraFases()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_FASES_ACTIVO)) {
				updateRow(ef, fila, fase.getActivo());
			}
		}
	}
	
	public void actualizarFase(Fases2021 fase) {
		EstructuraCampeonatoFemeninoIndividual2021 ecmi = new EstructuraCampeonatoFemeninoIndividual2021();
		Long fila = fase.getRowNum();
		for (Estructura2021 ef : ecmi.getEstructuraFases()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_FASES_NUMERO_ENFRENTAMIENTOS_GRUPOS)) {
				updateRow(ef, fila, fase.getNumeroEnfrentamientos());
			}else if (ef.getValor().equals(ecmi.COL_FASES_NUMERO_PARTIDAS)) {
				updateRow(ef, fila, fase.getNumeroPartidas());
			}else if (ef.getValor().equals(ecmi.COL_FASES_NUMERO_JUEGOS)) {
				updateRow(ef, fila, fase.getNumeroJuegos());
			}else if (ef.getValor().equals(ecmi.COL_FASES_CLASIFICAN)) {
				updateRow(ef, fila, fase.getClasifican());
			}else if (ef.getValor().equals(ecmi.COL_FASES_FECHA)) {
				updateRow(ef, fila, fase.getFecha());
			}else if (ef.getValor().equals(ecmi.COL_FASES_HORA)) {
				updateRow(ef, fila, fase.getHora());
			}else if (ef.getValor().equals(ecmi.COL_FASES_FASE_SIGUIENTE)) {
				updateRow(ef, fila, fase.getFaseSiguiente());
			}
			
		}		
	}
	
	public void crearClasificacionFaseI(){
		EstructuraCampeonatoFemeninoIndividual2021 ecmi = new EstructuraCampeonatoFemeninoIndividual2021();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CLA_FASE_I, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CLA_FASE_I, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO);
		}else{
			crearHojaClasificacionFaseI();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CLA_FASE_I, ecmi.getInsertCabeceraClasificacionFaseIRow());
	}
	
	public void insertarRowClasificacionFaseI(String insert){
		EstructuraCampeonatoFemeninoIndividual2021 ecmi = new EstructuraCampeonatoFemeninoIndividual2021();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CLA_FASE_I, insert);
	}
	
	public void crearCalendarioFaseI(){
		EstructuraCampeonatoFemeninoIndividual2021 ecmi = new EstructuraCampeonatoFemeninoIndividual2021();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CAL_FASE_I, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CAL_FASE_I, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO);
		}else{
			crearHojaCalendarioFaseI();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CAL_FASE_I, ecmi.getInsertCabeceraCalendarioFaseIRow());
	}
	
	public void insertarRowCalendarioFaseI(String insert){
		EstructuraCampeonatoFemeninoIndividual2021 ecmi = new EstructuraCampeonatoFemeninoIndividual2021();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CAL_FASE_I, insert);
	}
	
	public void actualizarResultadoCalendarioFaseI(CalendarioFaseI2021 calendarioFaseI, ClasificacionFaseI2021 clasificacionFaseIJugador1, 
			ClasificacionFaseI2021 clasificacionFaseIJugador2) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoFemeninoIndividual2021 ecmi = new EstructuraCampeonatoFemeninoIndividual2021();
		Long fila = calendarioFaseI.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
		
		if(calendarioFaseI.isModificable()){
			for (Estructura2021 ef : ecmi.getEstructuraCalendarioFaseI()) {
				// Cabeceras
				if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_I_JUEGOS_JUGADOR_1)) {
					updateRow(ef, fila, calendarioFaseI.getJuegosJugador1());
				}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_I_JUEGOS_JUGADOR_2)) {
					updateRow(ef, fila, calendarioFaseI.getJuegosJugador2());
				}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_I_ACTIVO)) {
					calendarioFaseI.setActivo(Activo2021.NO);
					updateRow(ef, fila, calendarioFaseI.getActivo());
				}
			}
			fila = clasificacionFaseIJugador1.getRowNum();
			//Actualizar resultado en clasificacion Jugador 1
			for (Estructura2021 ef : ecmi.getEstructuraClasificacionFaseI()) {
				// Cabeceras
				if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PJ)) {
					clasificacionFaseIJugador1.setPj(clasificacionFaseIJugador1.getPj()+1);
					updateRow(ef, fila, clasificacionFaseIJugador1.getPj());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PG)) {
					if(calendarioFaseI.isGanaJugador1()){
						clasificacionFaseIJugador1.setPg(clasificacionFaseIJugador1.getPg()+1);
					}
					updateRow(ef, fila, clasificacionFaseIJugador1.getPg());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PE)) {
					if(calendarioFaseI.isEmpate()){
						clasificacionFaseIJugador1.setPe(clasificacionFaseIJugador1.getPe()+1);
					}
					updateRow(ef, fila, clasificacionFaseIJugador1.getPe());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PP)) {
					if(calendarioFaseI.isGanaJugador2()){
						clasificacionFaseIJugador1.setPp(clasificacionFaseIJugador1.getPp()+1);
					}
					updateRow(ef, fila, clasificacionFaseIJugador1.getPp());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PF)) {
					clasificacionFaseIJugador1.setPf(clasificacionFaseIJugador1.getPf()+calendarioFaseI.getJuegosJugador1());
					updateRow(ef, fila, clasificacionFaseIJugador1.getPf());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PC)) {
					clasificacionFaseIJugador1.setPc(clasificacionFaseIJugador1.getPc()+calendarioFaseI.getJuegosJugador2());
					updateRow(ef, fila, clasificacionFaseIJugador1.getPc());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PT)) {
					if(calendarioFaseI.isGanaJugador1()){
						clasificacionFaseIJugador1.setPt(clasificacionFaseIJugador1.getPt()+3);
					}else if(calendarioFaseI.isEmpate()){
						clasificacionFaseIJugador1.setPt(clasificacionFaseIJugador1.getPt()+1);
					}
					updateRow(ef, fila, clasificacionFaseIJugador1.getPt());
				}
			}
			fila = clasificacionFaseIJugador2.getRowNum();
			//Actualizar resultado en clasificacion Jugador 2
			for (Estructura2021 ef : ecmi.getEstructuraClasificacionFaseI()) {
				// Cabeceras
				if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PJ)) {
					clasificacionFaseIJugador2.setPj(clasificacionFaseIJugador2.getPj()+1);
					updateRow(ef, fila, clasificacionFaseIJugador2.getPj());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PG)) {
					if(calendarioFaseI.isGanaJugador2()){
						clasificacionFaseIJugador2.setPg(clasificacionFaseIJugador2.getPg()+1);
					}
					updateRow(ef, fila, clasificacionFaseIJugador2.getPg());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PE)) {
					if(calendarioFaseI.isEmpate()){
						clasificacionFaseIJugador2.setPe(clasificacionFaseIJugador2.getPe()+1);
					}
					updateRow(ef, fila, clasificacionFaseIJugador2.getPe());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PP)) {
					if(calendarioFaseI.isGanaJugador1()){
						clasificacionFaseIJugador2.setPp(clasificacionFaseIJugador2.getPp()+1);
					}
					updateRow(ef, fila, clasificacionFaseIJugador2.getPp());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PF)) {
					clasificacionFaseIJugador2.setPf(clasificacionFaseIJugador2.getPf()+calendarioFaseI.getJuegosJugador2());
					updateRow(ef, fila, clasificacionFaseIJugador2.getPf());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PC)) {
					clasificacionFaseIJugador2.setPc(clasificacionFaseIJugador2.getPc()+calendarioFaseI.getJuegosJugador1());
					updateRow(ef, fila, clasificacionFaseIJugador2.getPc());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PT)) {
					if(calendarioFaseI.isGanaJugador2()){
						clasificacionFaseIJugador2.setPt(clasificacionFaseIJugador2.getPt()+3);
					}else if(calendarioFaseI.isEmpate()){
						clasificacionFaseIJugador2.setPt(clasificacionFaseIJugador2.getPt()+1);
					}
					updateRow(ef, fila, clasificacionFaseIJugador2.getPt());
				}
			}
		}
		
		
	}
	
	public void crearCalendarioFaseII(){
		EstructuraCampeonatoFemeninoIndividual2021 ecmi = new EstructuraCampeonatoFemeninoIndividual2021();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CAL_FASE_II, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CAL_FASE_II, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO);
		}else{
			crearHojaCalendarioFaseII();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CAL_FASE_II, ecmi.getInsertCabeceraCalendarioFaseIIRow());
	}
	
	public void crearClasificacionFaseII(){
		EstructuraCampeonatoFemeninoIndividual2021 ecmi = new EstructuraCampeonatoFemeninoIndividual2021();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CLA_FASE_II, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CLA_FASE_II, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO);
		}else{
			crearHojaClasificacionFaseII();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CLA_FASE_II, ecmi.getInsertCabeceraClasificacionFaseIIRow());
	}
	
	public void crearCalendarioFaseOF(){
		EstructuraCampeonatoFemeninoIndividual2021 ecmi = new EstructuraCampeonatoFemeninoIndividual2021();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CAL_FASE_OF, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CAL_FASE_OF, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO);
		}else{
			crearHojaCalendarioFaseOF();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CAL_FASE_OF, ecmi.getInsertCabeceraCalendarioFaseOFRow());
	}
	
	public void insertarRowCalendarioFaseOF(String insert){
		EstructuraCampeonatoFemeninoIndividual2021 ecmi = new EstructuraCampeonatoFemeninoIndividual2021();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CAL_FASE_OF, insert);
	}
	
	public void actualizarJugadorOctavosFinal(CalendarioFaseOF2021 calendarioFaseOF, boolean jugador1, boolean jugador2) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoFemeninoIndividual2021 ecmi = new EstructuraCampeonatoFemeninoIndividual2021();
		Long fila = calendarioFaseOF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2021 ef : ecmi.getEstructuraCalendarioOctavosFinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_JUGADOR_1) && jugador1) {
				updateRow(ef, fila, calendarioFaseOF.getJugador1Id());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_JUGADOR_2) && jugador2) {
				updateRow(ef, fila, calendarioFaseOF.getJugador2Id());
			}
		}
		
		
	}
	
	public void actualizarJugadorCuartosFinal(CalendarioFaseCF2021 calendarioFaseCF, boolean jugador1, boolean jugador2) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoFemeninoIndividual2021 ecmi = new EstructuraCampeonatoFemeninoIndividual2021();
		Long fila = calendarioFaseCF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2021 ef : ecmi.getEstructuraCalendarioCuartosFinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_JUGADOR_1) && jugador1) {
				updateRow(ef, fila, calendarioFaseCF.getJugador1Id());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_JUGADOR_2) && jugador2) {
				updateRow(ef, fila, calendarioFaseCF.getJugador2Id());
			}
		}
		
		
	}
	
	public void actualizarJugadorSemiFinal(CalendarioFaseSF2021 calendarioFaseSF, boolean jugador1, boolean jugador2) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoFemeninoIndividual2021 ecmi = new EstructuraCampeonatoFemeninoIndividual2021();
		Long fila = calendarioFaseSF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2021 ef : ecmi.getEstructuraCalendarioSemifinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUGADOR_1) && jugador1) {
				updateRow(ef, fila, calendarioFaseSF.getJugador1Id());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUGADOR_2) && jugador2) {
				updateRow(ef, fila, calendarioFaseSF.getJugador2Id());
			}
		}
		
		
	}
	
	public void actualizarJugadorFinalConsolacion(CalendarioFaseFC2021 calendarioFaseFC, boolean jugador1, boolean jugador2) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoFemeninoIndividual2021 ecmi = new EstructuraCampeonatoFemeninoIndividual2021();
		Long fila = calendarioFaseFC.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2021 ef : ecmi.getEstructuraCalendarioFinalConsolacion()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_JUGADOR_1) && jugador1) {
				updateRow(ef, fila, calendarioFaseFC.getJugador1Id());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_JUGADOR_2) && jugador2) {
				updateRow(ef, fila, calendarioFaseFC.getJugador2Id());
			}
		}
		
		
	}
	
	public void actualizarJugadorFinal(CalendarioFaseFF2021 calendarioFaseFF, boolean jugador1, boolean jugador2) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoFemeninoIndividual2021 ecmi = new EstructuraCampeonatoFemeninoIndividual2021();
		Long fila = calendarioFaseFF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2021 ef : ecmi.getEstructuraCalendarioFinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUGADOR_1) && jugador1) {
				updateRow(ef, fila, calendarioFaseFF.getJugador1Id());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUGADOR_2) && jugador2) {
				updateRow(ef, fila, calendarioFaseFF.getJugador2Id());
			}
		}
		
		
	}
	
	public void actualizarResultadosEnfrentamientoDirectoOF(CalendarioFaseOF2021 calendarioFaseOF) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoFemeninoIndividual2021 ecmi = new EstructuraCampeonatoFemeninoIndividual2021();
		Long fila = calendarioFaseOF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2021 ef : ecmi.getEstructuraCalendarioOctavosFinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_JUEGOS_JUGADOR_1_P1)) {
				updateRow(ef, fila, calendarioFaseOF.getJuegosJugador1P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_JUEGOS_JUGADOR_2_P1)) {
				updateRow(ef, fila, calendarioFaseOF.getJuegosJugador2P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_JUEGOS_JUGADOR_1_P2)) {
				updateRow(ef, fila, calendarioFaseOF.getJuegosJugador1P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_JUEGOS_JUGADOR_2_P2)) {
				updateRow(ef, fila, calendarioFaseOF.getJuegosJugador2P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_JUEGOS_JUGADOR_1_P3)) {
				updateRow(ef, fila, calendarioFaseOF.getJuegosJugador1P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_JUEGOS_JUGADOR_2_P3)) {
				updateRow(ef, fila, calendarioFaseOF.getJuegosJugador2P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_ACTIVO)) {
				calendarioFaseOF.setActivo(Activo2021.NO);
				updateRow(ef, fila, calendarioFaseOF.getActivo());
			}
		}
		
		
	}
	
	public void crearCalendarioFaseCF(){
		EstructuraCampeonatoFemeninoIndividual2021 ecmi = new EstructuraCampeonatoFemeninoIndividual2021();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CAL_FASE_CF, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CAL_FASE_CF, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO);
		}else{
			crearHojaCalendarioFaseCF();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CAL_FASE_CF, ecmi.getInsertCabeceraCalendarioFaseCFRow());
	}
	
	public void insertarRowCalendarioFaseCF(String insert){
		EstructuraCampeonatoFemeninoIndividual2021 ecmi = new EstructuraCampeonatoFemeninoIndividual2021();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CAL_FASE_CF, insert);
	}
	
	public void actualizarResultadosEnfrentamientoDirectoCF(CalendarioFaseCF2021 calendarioFaseCF) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoFemeninoIndividual2021 ecmi = new EstructuraCampeonatoFemeninoIndividual2021();
		Long fila = calendarioFaseCF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2021 ef : ecmi.getEstructuraCalendarioCuartosFinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_1_P1)) {
				updateRow(ef, fila, calendarioFaseCF.getJuegosJugador1P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_2_P1)) {
				updateRow(ef, fila, calendarioFaseCF.getJuegosJugador2P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_1_P2)) {
				updateRow(ef, fila, calendarioFaseCF.getJuegosJugador1P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_2_P2)) {
				updateRow(ef, fila, calendarioFaseCF.getJuegosJugador2P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_1_P3)) {
				updateRow(ef, fila, calendarioFaseCF.getJuegosJugador1P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_2_P3)) {
				updateRow(ef, fila, calendarioFaseCF.getJuegosJugador2P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_ACTIVO)) {
				calendarioFaseCF.setActivo(Activo2021.NO);
				updateRow(ef, fila, calendarioFaseCF.getActivo());
			}
		}
		
		
	}
	
	public void crearCalendarioFaseSF(){
		EstructuraCampeonatoFemeninoIndividual2021 ecmi = new EstructuraCampeonatoFemeninoIndividual2021();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CAL_FASE_SF, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CAL_FASE_SF, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO);
		}else{
			crearHojaCalendarioFaseSF();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CAL_FASE_SF, ecmi.getInsertCabeceraCalendarioFaseSFRow());
	}
	
	public void insertarRowCalendarioFaseSF(String insert){
		EstructuraCampeonatoFemeninoIndividual2021 ecmi = new EstructuraCampeonatoFemeninoIndividual2021();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CAL_FASE_SF, insert);
	}
	
	public void actualizarResultadosEnfrentamientoDirectoSF(CalendarioFaseSF2021 calendarioFaseSF) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoFemeninoIndividual2021 ecmi = new EstructuraCampeonatoFemeninoIndividual2021();
		Long fila = calendarioFaseSF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2021 ef : ecmi.getEstructuraCalendarioSemifinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_1_P1)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosJugador1P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_2_P1)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosJugador2P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_1_P2)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosJugador1P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_2_P2)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosJugador2P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_1_P3)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosJugador1P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_2_P3)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosJugador2P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_ACTIVO)) {
				calendarioFaseSF.setActivo(Activo2021.NO);
				updateRow(ef, fila, calendarioFaseSF.getActivo());
			}
		}
		
		
	}
	
	public void crearCalendarioFaseFC(){
		EstructuraCampeonatoFemeninoIndividual2021 ecmi = new EstructuraCampeonatoFemeninoIndividual2021();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CAL_FASE_FC, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CAL_FASE_FC, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO);
		}else{
			crearHojaCalendarioFaseFC();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CAL_FASE_FC, ecmi.getInsertCabeceraCalendarioFaseFCRow());
	}
	
	public void insertarRowCalendarioFaseFC(String insert){
		EstructuraCampeonatoFemeninoIndividual2021 ecmi = new EstructuraCampeonatoFemeninoIndividual2021();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CAL_FASE_FC, insert);
	}
	
	public void actualizarResultadosEnfrentamientoDirectoFC(CalendarioFaseFC2021 calendarioFaseFC) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoFemeninoIndividual2021 ecmi = new EstructuraCampeonatoFemeninoIndividual2021();
		Long fila = calendarioFaseFC.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2021 ef : ecmi.getEstructuraCalendarioFinalConsolacion()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_JUEGOS_JUGADOR_1_P1)) {
				updateRow(ef, fila, calendarioFaseFC.getJuegosJugador1P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_JUEGOS_JUGADOR_2_P1)) {
				updateRow(ef, fila, calendarioFaseFC.getJuegosJugador2P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_JUEGOS_JUGADOR_1_P2)) {
				updateRow(ef, fila, calendarioFaseFC.getJuegosJugador1P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_JUEGOS_JUGADOR_2_P2)) {
				updateRow(ef, fila, calendarioFaseFC.getJuegosJugador2P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_JUEGOS_JUGADOR_1_P3)) {
				updateRow(ef, fila, calendarioFaseFC.getJuegosJugador1P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_JUEGOS_JUGADOR_2_P3)) {
				updateRow(ef, fila, calendarioFaseFC.getJuegosJugador2P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_ACTIVO)) {
				calendarioFaseFC.setActivo(Activo2021.NO);
				updateRow(ef, fila, calendarioFaseFC.getActivo());
			}
		}
		
		
	}
	
	public void crearCalendarioFaseFF(){
		EstructuraCampeonatoFemeninoIndividual2021 ecmi = new EstructuraCampeonatoFemeninoIndividual2021();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CAL_FASE_FF, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CAL_FASE_FF, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO);
		}else{
			crearHojaCalendarioFaseFF();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CAL_FASE_FF, ecmi.getInsertCabeceraCalendarioFaseFFRow());
	}
	
	public void insertarRowCalendarioFaseFF(String insert){
		EstructuraCampeonatoFemeninoIndividual2021 ecmi = new EstructuraCampeonatoFemeninoIndividual2021();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CAL_FASE_FF, insert);
	}
	
	public void actualizarResultadosEnfrentamientoDirectoFF(CalendarioFaseFF2021 calendarioFaseFF) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoFemeninoIndividual2021 ecmi = new EstructuraCampeonatoFemeninoIndividual2021();
		Long fila = calendarioFaseFF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2021 ef : ecmi.getEstructuraCalendarioFinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_1_P1)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosJugador1P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_2_P1)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosJugador2P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_1_P2)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosJugador1P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_2_P2)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosJugador2P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_1_P3)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosJugador1P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_2_P3)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosJugador2P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_ACTIVO)) {
				calendarioFaseFF.setActivo(Activo2021.NO);
				updateRow(ef, fila, calendarioFaseFF.getActivo());
			}
		}
		
		
	}
	
	public void borrarSorteo(){
		deleteFile(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CAL_FASE_I, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO);
		deleteFile(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CLA_FASE_I, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO);
		deleteFile(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CAL_FASE_II, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO);
		deleteFile(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CLA_FASE_II, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO);
		deleteFile(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CAL_FASE_OF, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO);
		deleteFile(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CAL_FASE_CF, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO);
		deleteFile(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CAL_FASE_SF, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO);
		deleteFile(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CAL_FASE_FC, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO);
		deleteFile(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO_CAL_FASE_FF, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_FEMENINO);
		actualizarEstadoConfiguracion(EstadosIndividualParejas2021.CONFIG_ESTADO_CERRAR_INSCRIPCIONES);
	}	
	

}
