package com.bolocelta.bbdd.createTable;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import com.bolocelta.bbdd.constants.Modalidad2021;
import com.bolocelta.bbdd.constants.Ubicaciones2021;
import com.bolocelta.bbdd.constants.structure.Estructura2021;
import com.bolocelta.bbdd.constants.structure.EstructuraJugadores2021;
import com.bolocelta.entities.Jugadores2021;

public class CrearJugadores2021 extends ACrearModificar2021 {
	
	public void preparateInsertRow(Jugadores2021 jugador) {
		
		EstructuraJugadores2021 ej = new EstructuraJugadores2021();
		
		if(jugador.isMasculino()){
			jugador.setModalidad(Modalidad2021.MASCULINO);
		}else if(jugador.isFemenino()){
			jugador.setModalidad(Modalidad2021.FEMENINO);
		}
		
		insertRow(ej.N_CSV_BBDD_JUGADORES, jugador.getInsertRow());

	}

	public void preparateUpdateRow(Jugadores2021 jugador) {

		EstructuraJugadores2021 ej = new EstructuraJugadores2021();
		
		Long fila = jugador.getRowNum();

		for (Estructura2021 update : ej.getList()) {
			// Cabeceras
			if (update.getValor().equals(ej.COL_ID)) {
				updateRow(update, fila, fila);
			} else if (update.getValor().equals(ej.COL_EQUIPO)) {
				updateRow(update, fila, jugador.getEquipoId());
			} else if (update.getValor().equals(ej.COL_NOMBRE)) {
				updateRow(update, fila, jugador.getNombre());
			} else if (update.getValor().equals(ej.COL_APODO)) {
				updateRow(update, fila, jugador.getApodo());
			} else if (update.getValor().equals(ej.COL_NUM_FICHA)) {
				updateRow(update, fila, jugador.getNumeroFicha());
			} else if (update.getValor().equals(ej.COL_EMAIL)) {
				updateRow(update, fila, jugador.getEmail());
			} else if (update.getValor().equals(ej.COL_TELEFONO)) {
				updateRow(update, fila, jugador.getTelefono());
			} else if (update.getValor().equals(ej.COL_EDAD)) {
				updateRow(update, fila, jugador.getEdad());
			} else if (update.getValor().equals(ej.COL_ACTIVO)) {
				updateRow(update, fila, jugador.getActivo());
			} else if (update.getValor().equals(ej.COL_MODALIDAD)) {
				if(jugador.isMasculino()){
					jugador.setModalidad(Modalidad2021.MASCULINO);
				}else if(jugador.isFemenino()){
					jugador.setModalidad(Modalidad2021.FEMENINO);
				}
				updateRow(update, fila, jugador.getModalidad());
			} else if (update.getValor().equals(ej.COL_INDIV_PRI)) {
				updateRow(update, fila, jugador.getIndividualPrimera());
			} else if (update.getValor().equals(ej.COL_INDIV_SEG)) {
				updateRow(update, fila, jugador.getIndividualSegunda());
			} else if (update.getValor().equals(ej.COL_INDIV_TER)) {
				updateRow(update, fila, jugador.getIndividualTercera());
			} else if (update.getValor().equals(ej.COL_INDIV_FEM)) {
				updateRow(update, fila, jugador.getIndividualFemenino());
			} else if (update.getValor().equals(ej.COL_ACA_J1)) {
				updateRow(update, fila, jugador.getAcabonesJ1());
			} else if (update.getValor().equals(ej.COL_ACA_J2)) {
				updateRow(update, fila, jugador.getAcabonesJ2());
			} else if (update.getValor().equals(ej.COL_ACA_J3)) {
				updateRow(update, fila, jugador.getAcabonesJ3());
			} else if (update.getValor().equals(ej.COL_ACA_J4)) {
				updateRow(update, fila, jugador.getAcabonesJ4());
			} else if (update.getValor().equals(ej.COL_ACA_J5)) {
				updateRow(update, fila, jugador.getAcabonesJ5());
			} else if (update.getValor().equals(ej.COL_ACA_J6)) {
				updateRow(update, fila, jugador.getAcabonesJ6());
			} else if (update.getValor().equals(ej.COL_ACA_J7)) {
				updateRow(update, fila, jugador.getAcabonesJ7());
			} else if (update.getValor().equals(ej.COL_ACA_J8)) {
				updateRow(update, fila, jugador.getAcabonesJ8());
			} else if (update.getValor().equals(ej.COL_ACA_J9)) {
				updateRow(update, fila, jugador.getAcabonesJ9());
			} else if (update.getValor().equals(ej.COL_ACA_J10)) {
				updateRow(update, fila, jugador.getAcabonesJ10());
			} else if (update.getValor().equals(ej.COL_ACA_J11)) {
				updateRow(update, fila, jugador.getAcabonesJ11());
			} else if (update.getValor().equals(ej.COL_ACA_J12)) {
				updateRow(update, fila, jugador.getAcabonesJ12());
			} else if (update.getValor().equals(ej.COL_ACA_J13)) {
				updateRow(update, fila, jugador.getAcabonesJ13());
			} else if (update.getValor().equals(ej.COL_ACA_J14)) {
				updateRow(update, fila, jugador.getAcabonesJ14());
			}
		}
	}
	
	public void preparateDeleteLogicRow(Jugadores2021 jugador) {

		EstructuraJugadores2021 ej = new EstructuraJugadores2021();

		Long fila = jugador.getRowNum();

		for (Estructura2021 update : ej.getList()) {
			// Cabeceras
			if (update.getValor().equals(ej.COL_ACTIVO)) {
				updateRow(update, fila, jugador.getActivo());
			}
		}
	}
	
	private void insertRow(String excel, String newRowData) {
		insertDataCsv(excel, Ubicaciones2021.UBICACION_BBDD_ADMINISTRACION, newRowData);
	}

	private void updateRow(Estructura2021 update, Long fila, Object newValue) {
		updateDataCsv(update.getExcel(), Ubicaciones2021.UBICACION_BBDD_ADMINISTRACION, fila, update.getColumna(), newValue, update.getTipoDato());
	}
	
	public boolean validate(Jugadores2021 jugador){
		boolean valid = true;
		if(jugador.getNombre() == null || jugador.getNombre().isEmpty()){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El nombre es requerido.", null));
			valid = false;
		}else if(jugador.getApodo() == null || jugador.getApodo().isEmpty()){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El apodo es requerido.", null));
			valid = false;
		}else if(jugador.getEdad() == null || jugador.getEdad() < 1){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "La edad es requerida.", null));
			valid = false;
		}else if(jugador.getNumeroFicha() == null || jugador.getNumeroFicha().isEmpty()){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El n�mero de ficha  es requerida.", null));
			valid = false;
		}else if(jugador.getTelefono() != null && !jugador.getTelefono().isEmpty() && !telefonoValidator.validate(jugador.getTelefono())){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El tel�fono no es valido", null));
			valid = false;
		}else if(jugador.getEmail() != null && !jugador.getEmail().isEmpty() && !emailValidator.validate(jugador.getEmail())){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El email no es valido", null));
			valid = false;
		}else if(!jugador.isMasculino() && !jugador.isFemenino()){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "La modalidad es requerida.", null));
			valid = false;
		}else if(jugador.isMasculino() && jugador.isFemenino()){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Solo es posible seleccionar una modalidad.", null));
			valid = false;
		}
		return valid;
	}

}
