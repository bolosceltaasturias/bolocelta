package com.bolocelta.bbdd.createTable;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.bolocelta.bbdd.constants.Activo2021;
import com.bolocelta.bbdd.constants.EstadosIndividualParejas2021;
import com.bolocelta.bbdd.constants.NombresTablas2021;
import com.bolocelta.bbdd.constants.Ubicaciones2021;
import com.bolocelta.bbdd.constants.structure.Estructura2021;
import com.bolocelta.bbdd.constants.structure.EstructuraCampeonatoMasculinoParejasSegunda2021;
import com.bolocelta.bbdd.readTables.LeerCampeonatoMasculinoParejasSegunda2021;
import com.bolocelta.entities.Categorias2021;
import com.bolocelta.entities.Fases2021;
import com.bolocelta.entities.Parejas2021;
import com.bolocelta.entities.ParticipantesParejas2021;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseCF2021;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseFC2021;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseFF2021;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseI2021;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseOF2021;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseSF2021;
import com.bolocelta.entities.sorteos.parejas.ClasificacionFaseI2021;

public class CrearCampeonatoMasculinoParejasSegunda2021 extends ACrearModificar2021 {
	
	LeerCampeonatoMasculinoParejasSegunda2021 leerCampeonatoMasculinoParejasSegunda = new LeerCampeonatoMasculinoParejasSegunda2021();

	private void crearExcel() {

	}
	
	private void crearHojaConfiguracion(Date fechaMaximaInscripcion, String estado, String bolera){
		createWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CONFIG,
				Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA);
	}
	
	private void crearHojaFases(){
		createWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_FASES,
				Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA);
	}
	
	private void crearHojaParticipantes(){
		createWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_PARTICIPANTES,
				Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA);
	}
	
	private void crearHojaGrupos(){
		createWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_GRUPOS,
				Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA);
	}
	
	private void crearHojaClasificacionFaseI(){
		createWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CLA_FASE_I,
				Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA);
	}
	
	private void crearHojaCalendarioFaseI(){
		createWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CAL_FASE_I,
				Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA);
	}
	
	private void crearHojaClasificacionFaseII(){
		createWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CLA_FASE_II,
				Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA);
	}
	
	private void crearHojaCalendarioFaseII(){
		createWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CAL_FASE_II,
				Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA);
	}
	
	private void crearHojaCalendarioFaseOF(){
		createWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CAL_FASE_OF,
				Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA);
	}
	
	private void crearHojaCalendarioFaseCF(){
		createWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CAL_FASE_CF,
				Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA);
	}
	
	private void crearHojaCalendarioFaseSF(){
		createWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CAL_FASE_SF,
				Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA);
	}
	
	private void crearHojaCalendarioFaseFC(){
		createWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CAL_FASE_FC,
				Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA);
	}
	
	private void crearHojaCalendarioFaseFF(){
		createWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CAL_FASE_FF,
				Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA);
	}
	
	private void insertRow(String excel, String newRowData) {
		insertDataCsv(excel, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA, newRowData);
	}
	
	public void updateRow(Estructura2021 update, Long fila, Object newValue) {
		updateDataCsv(update.getExcel(), Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA, fila, update.getColumna(), newValue, update.getTipoDato());
	}
	
	
	//Creacion de la base del campeonato
	public void crearBaseCampeonato(Date fechaMaximaInscripcion, String estado, String bolera){
		//Crear las hojas excel
		crearExcel();
		crearHojaConfiguracion(fechaMaximaInscripcion, estado, bolera);
		crearHojaFases();
		crearHojaParticipantes();
		
		
		//Crear la estructura de las hojas (cabeceras y registros por defecto)
		EstructuraCampeonatoMasculinoParejasSegunda2021 ecmi = new EstructuraCampeonatoMasculinoParejasSegunda2021();
		//Crear las estructuras de las hojas
		crearEstructuraHojas(ecmi);
		//Rellenar datos de configuración
		rellenarDatosConfiguracion(ecmi, fechaMaximaInscripcion, estado, bolera);
		//Rellenar todas las fases
		rellenarDatosFases(ecmi);
	}
	
	public void crearEstructuraHojas(EstructuraCampeonatoMasculinoParejasSegunda2021 ecmi) {
		//Configuracion
		for (String row : ecmi.getInsertCabeceraConfiguracionRow()) {
			insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CONFIG, row);
		}
		//Fases
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_FASES, ecmi.getInsertCabeceraFasesRow());
		//Participantes
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_PARTICIPANTES, ecmi.getInsertCabeceraParticipantesRow());
	}

	private void rellenarDatosConfiguracion(EstructuraCampeonatoMasculinoParejasSegunda2021 ecmi, Date fechaMaximaInscripcion, String estado, 
			String bolera) {
		//Actualizar la fecha
	    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy"); // date pattern
	    String fechaMaxInscripcion = simpleDateFormat.format(fechaMaximaInscripcion);
		updateRow(ecmi.getEstructuraFechaMaximaInscripcion(), ecmi.getEstructuraFechaMaximaInscripcion().getFila(), fechaMaxInscripcion);
		//Actualizar el estado
		updateRow(ecmi.getEstructuraEstadoCampeonato(), ecmi.getEstructuraEstadoCampeonato().getFila(), estado);
		updateRow(ecmi.getEstructuraBoleraFinal(), ecmi.getEstructuraBoleraFinal().getFila(), bolera);
	}
	
	public void actualizarEstadoConfiguracion(String estado) {
		EstructuraCampeonatoMasculinoParejasSegunda2021 ecmi = new EstructuraCampeonatoMasculinoParejasSegunda2021();
		//Actualizar el estado
		updateRow(ecmi.getEstructuraEstadoCampeonato(), ecmi.getEstructuraEstadoCampeonato().getFila(), estado);
	}
	
	private void rellenarDatosFases(EstructuraCampeonatoMasculinoParejasSegunda2021 ecmi) {
		for(String fase : ecmi.getEstructuraTodasFases()){
			insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_FASES, fase);
		}
	}
	
	public void inscribirNewPareja(Parejas2021 pareja, Categorias2021 categoria, String usuario, String activo) {
		EstructuraCampeonatoMasculinoParejasSegunda2021 ecmi = new EstructuraCampeonatoMasculinoParejasSegunda2021();
    	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss"); // date pattern
    	String fecha = simpleDateFormat.format(new Date());
    	
    	Long newRowNum = leerCampeonatoMasculinoParejasSegunda.lastRowSheet(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_PARTICIPANTES, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA);
    	newRowNum++;
    	
	    String insert = newRowNum.toString() + ";" + pareja.getJugador1().getId().toString() + ";" + pareja.getJugador2().getId().toString() + ";" + fecha + ";" + usuario + ";" + activo;
	    insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_PARTICIPANTES, insert);
	}
	
	public void desinscribirPareja(ParticipantesParejas2021 pareja, String usuario, String activo) {
		EstructuraCampeonatoMasculinoParejasSegunda2021 ecmi = new EstructuraCampeonatoMasculinoParejasSegunda2021();
		
		Long fila = pareja.getRowNum();
		
		for (Estructura2021 eip : ecmi.getEstructuraInscribirParticipantes()) {
			
			// Cabeceras
			if (eip.getValor().equals(ecmi.COL_PARTICIPANTES_USUARIO)) {
				updateRow(eip, fila, usuario);
			} else if (eip.getValor().equals(ecmi.COL_PARTICIPANTES_FECHA_INSCRIPCION)) {
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss"); // date pattern
			    String fecha = simpleDateFormat.format(new Date());
				updateRow(eip, fila, fecha);
			} else if (eip.getValor().equals(ecmi.COL_PARTICIPANTES_ACTIVO)) {
				updateRow(eip, fila, activo);
			}

		}
	}
	
	
	public void actualizarActivoFase(Fases2021 fase) {
		EstructuraCampeonatoMasculinoParejasSegunda2021 ecmi = new EstructuraCampeonatoMasculinoParejasSegunda2021();
		Long fila = fase.getRowNum();
		for (Estructura2021 ef : ecmi.getEstructuraFases()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_FASES_ACTIVO)) {
				updateRow(ef, fila, fase.getActivo());
			}
		}
	}
	
	public void actualizarFase(Fases2021 fase) {
		EstructuraCampeonatoMasculinoParejasSegunda2021 ecmi = new EstructuraCampeonatoMasculinoParejasSegunda2021();
		Long fila = fase.getRowNum();
		for (Estructura2021 ef : ecmi.getEstructuraFases()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_FASES_NUMERO_ENFRENTAMIENTOS_GRUPOS)) {
				updateRow(ef, fila, fase.getNumeroEnfrentamientos());
			}else if (ef.getValor().equals(ecmi.COL_FASES_NUMERO_PARTIDAS)) {
				updateRow(ef, fila, fase.getNumeroPartidas());
			}else if (ef.getValor().equals(ecmi.COL_FASES_NUMERO_JUEGOS)) {
				updateRow(ef, fila, fase.getNumeroJuegos());
			}else if (ef.getValor().equals(ecmi.COL_FASES_CLASIFICAN)) {
				updateRow(ef, fila, fase.getClasifican());
			}else if (ef.getValor().equals(ecmi.COL_FASES_FECHA)) {
				updateRow(ef, fila, fase.getFecha());
			}else if (ef.getValor().equals(ecmi.COL_FASES_HORA)) {
				updateRow(ef, fila, fase.getHora());
			}else if (ef.getValor().equals(ecmi.COL_FASES_FASE_SIGUIENTE)) {
				updateRow(ef, fila, fase.getFaseSiguiente());
			}
			
		}		
	}
	
	public void crearClasificacionFaseI(){
		EstructuraCampeonatoMasculinoParejasSegunda2021 ecmi = new EstructuraCampeonatoMasculinoParejasSegunda2021();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CLA_FASE_I, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CLA_FASE_I, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA);
		}else{
			crearHojaClasificacionFaseI();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CLA_FASE_I, ecmi.getInsertCabeceraClasificacionFaseIRow());
	}
	
	public void insertarRowClasificacionFaseI(String insert){
		EstructuraCampeonatoMasculinoParejasSegunda2021 ecmi = new EstructuraCampeonatoMasculinoParejasSegunda2021();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CLA_FASE_I, insert);
	}
	
	public void crearCalendarioFaseI(){
		EstructuraCampeonatoMasculinoParejasSegunda2021 ecmi = new EstructuraCampeonatoMasculinoParejasSegunda2021();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CAL_FASE_I, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CAL_FASE_I, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA);
		}else{
			crearHojaCalendarioFaseI();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CAL_FASE_I, ecmi.getInsertCabeceraCalendarioFaseIRow());
	}
	
	public void insertarRowCalendarioFaseI(String insert){
		EstructuraCampeonatoMasculinoParejasSegunda2021 ecmi = new EstructuraCampeonatoMasculinoParejasSegunda2021();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CAL_FASE_I, insert);
	}
	
	public void actualizarResultadoCalendarioFaseI(CalendarioFaseI2021 calendarioFaseI, ClasificacionFaseI2021 clasificacionFaseIPareja1, 
			ClasificacionFaseI2021 clasificacionFaseIPareja2) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoParejasSegunda2021 ecmi = new EstructuraCampeonatoMasculinoParejasSegunda2021();
		Long fila = calendarioFaseI.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
		
		if(calendarioFaseI.isModificable()){
			for (Estructura2021 ef : ecmi.getEstructuraCalendarioFaseI()) {
				// Cabeceras
				if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_I_JUEGOS_PAREJA_1)) {
					updateRow(ef, fila, calendarioFaseI.getJuegosPareja1());
				}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_I_JUEGOS_PAREJA_2)) {
					updateRow(ef, fila, calendarioFaseI.getJuegosPareja2());
				}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_I_ACTIVO)) {
					calendarioFaseI.setActivo(Activo2021.NO);
					updateRow(ef, fila, calendarioFaseI.getActivo());
				}
			}
			fila = clasificacionFaseIPareja1.getRowNum();
			//Actualizar resultado en clasificacion Pareja 1
			for (Estructura2021 ef : ecmi.getEstructuraClasificacionFaseI()) {
				// Cabeceras
				if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PJ)) {
					clasificacionFaseIPareja1.setPj(clasificacionFaseIPareja1.getPj()+1);
					updateRow(ef, fila, clasificacionFaseIPareja1.getPj());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PG)) {
					if(calendarioFaseI.isGanaPareja1()){
						clasificacionFaseIPareja1.setPg(clasificacionFaseIPareja1.getPg()+1);
					}
					updateRow(ef, fila, clasificacionFaseIPareja1.getPg());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PE)) {
					if(calendarioFaseI.isEmpate()){
						clasificacionFaseIPareja1.setPe(clasificacionFaseIPareja1.getPe()+1);
					}
					updateRow(ef, fila, clasificacionFaseIPareja1.getPe());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PP)) {
					if(calendarioFaseI.isGanaPareja2()){
						clasificacionFaseIPareja1.setPp(clasificacionFaseIPareja1.getPp()+1);
					}
					updateRow(ef, fila, clasificacionFaseIPareja1.getPp());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PF)) {
					clasificacionFaseIPareja1.setPf(clasificacionFaseIPareja1.getPf()+calendarioFaseI.getJuegosPareja1());
					updateRow(ef, fila, clasificacionFaseIPareja1.getPf());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PC)) {
					clasificacionFaseIPareja1.setPc(clasificacionFaseIPareja1.getPc()+calendarioFaseI.getJuegosPareja2());
					updateRow(ef, fila, clasificacionFaseIPareja1.getPc());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PT)) {
					if(calendarioFaseI.isGanaPareja1()){
						clasificacionFaseIPareja1.setPt(clasificacionFaseIPareja1.getPt()+3);
					}else if(calendarioFaseI.isEmpate()){
						clasificacionFaseIPareja1.setPt(clasificacionFaseIPareja1.getPt()+1);
					}
					updateRow(ef, fila, clasificacionFaseIPareja1.getPt());
				}
			}
			fila = clasificacionFaseIPareja2.getRowNum();
			//Actualizar resultado en clasificacion Pareja 2
			for (Estructura2021 ef : ecmi.getEstructuraClasificacionFaseI()) {
				// Cabeceras
				if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PJ)) {
					clasificacionFaseIPareja2.setPj(clasificacionFaseIPareja2.getPj()+1);
					updateRow(ef, fila, clasificacionFaseIPareja2.getPj());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PG)) {
					if(calendarioFaseI.isGanaPareja2()){
						clasificacionFaseIPareja2.setPg(clasificacionFaseIPareja2.getPg()+1);
					}
					updateRow(ef, fila, clasificacionFaseIPareja2.getPg());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PE)) {
					if(calendarioFaseI.isEmpate()){
						clasificacionFaseIPareja2.setPe(clasificacionFaseIPareja2.getPe()+1);
					}
					updateRow(ef, fila, clasificacionFaseIPareja2.getPe());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PP)) {
					if(calendarioFaseI.isGanaPareja1()){
						clasificacionFaseIPareja2.setPp(clasificacionFaseIPareja2.getPp()+1);
					}
					updateRow(ef, fila, clasificacionFaseIPareja2.getPp());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PF)) {
					clasificacionFaseIPareja2.setPf(clasificacionFaseIPareja2.getPf()+calendarioFaseI.getJuegosPareja2());
					updateRow(ef, fila, clasificacionFaseIPareja2.getPf());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PC)) {
					clasificacionFaseIPareja2.setPc(clasificacionFaseIPareja2.getPc()+calendarioFaseI.getJuegosPareja1());
					updateRow(ef, fila, clasificacionFaseIPareja2.getPc());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PT)) {
					if(calendarioFaseI.isGanaPareja2()){
						clasificacionFaseIPareja2.setPt(clasificacionFaseIPareja2.getPt()+3);
					}else if(calendarioFaseI.isEmpate()){
						clasificacionFaseIPareja2.setPt(clasificacionFaseIPareja2.getPt()+1);
					}
					updateRow(ef, fila, clasificacionFaseIPareja2.getPt());
				}
			}
		}
		
		
	}
	
	public void crearCalendarioFaseII(){
		EstructuraCampeonatoMasculinoParejasSegunda2021 ecmi = new EstructuraCampeonatoMasculinoParejasSegunda2021();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CAL_FASE_II, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CAL_FASE_II, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA);
		}else{
			crearHojaCalendarioFaseII();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CAL_FASE_II, ecmi.getInsertCabeceraCalendarioFaseIIRow());
	}
	
	public void crearClasificacionFaseII(){
		EstructuraCampeonatoMasculinoParejasSegunda2021 ecmi = new EstructuraCampeonatoMasculinoParejasSegunda2021();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CLA_FASE_II, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CLA_FASE_II, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA);
		}else{
			crearHojaClasificacionFaseII();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CLA_FASE_II, ecmi.getInsertCabeceraClasificacionFaseIIRow());
	}
	
	public void crearCalendarioFaseOF(){
		EstructuraCampeonatoMasculinoParejasSegunda2021 ecmi = new EstructuraCampeonatoMasculinoParejasSegunda2021();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CAL_FASE_OF, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CAL_FASE_OF, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA);
		}else{
			crearHojaCalendarioFaseOF();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CAL_FASE_OF, ecmi.getInsertCabeceraCalendarioFaseOFRow());
	}
	
	public void insertarRowCalendarioFaseOF(String insert){
		EstructuraCampeonatoMasculinoParejasSegunda2021 ecmi = new EstructuraCampeonatoMasculinoParejasSegunda2021();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CAL_FASE_OF, insert);
	}
	
	public void actualizarParejaOctavosFinal(CalendarioFaseOF2021 calendarioFaseOF, boolean pareja1, boolean pareja2) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoParejasSegunda2021 ecmi = new EstructuraCampeonatoMasculinoParejasSegunda2021();
		Long fila = calendarioFaseOF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2021 ef : ecmi.getEstructuraCalendarioOctavosFinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_PAREJA_1) && pareja1) {
				updateRow(ef, fila, calendarioFaseOF.getPareja1Id());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_PAREJA_2) && pareja2) {
				updateRow(ef, fila, calendarioFaseOF.getPareja2Id());
			}
		}
		
		
	}
	
	public void actualizarParejaCuartosFinal(CalendarioFaseCF2021 calendarioFaseCF, boolean pareja1, boolean pareja2) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoParejasSegunda2021 ecmi = new EstructuraCampeonatoMasculinoParejasSegunda2021();
		Long fila = calendarioFaseCF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2021 ef : ecmi.getEstructuraCalendarioCuartosFinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_PAREJA_1) && pareja1) {
				updateRow(ef, fila, calendarioFaseCF.getPareja1Id());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_PAREJA_2) && pareja2) {
				updateRow(ef, fila, calendarioFaseCF.getPareja2Id());
			}
		}
		
		
	}
	
	public void actualizarParejaSemiFinal(CalendarioFaseSF2021 calendarioFaseSF, boolean pareja1, boolean pareja2) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoParejasSegunda2021 ecmi = new EstructuraCampeonatoMasculinoParejasSegunda2021();
		Long fila = calendarioFaseSF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2021 ef : ecmi.getEstructuraCalendarioSemifinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_PAREJA_1) && pareja1) {
				updateRow(ef, fila, calendarioFaseSF.getPareja1Id());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_PAREJA_2) && pareja2) {
				updateRow(ef, fila, calendarioFaseSF.getPareja2Id());
			}
		}
		
		
	}
	
	public void actualizarParejaFinalConsolacion(CalendarioFaseFC2021 calendarioFaseFC, boolean pareja1, boolean pareja2) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoParejasSegunda2021 ecmi = new EstructuraCampeonatoMasculinoParejasSegunda2021();
		Long fila = calendarioFaseFC.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2021 ef : ecmi.getEstructuraCalendarioFinalConsolacion()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_PAREJA_1) && pareja1) {
				updateRow(ef, fila, calendarioFaseFC.getPareja1Id());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_PAREJA_2) && pareja2) {
				updateRow(ef, fila, calendarioFaseFC.getPareja2Id());
			}
		}
		
		
	}
	
	public void actualizarParejaFinal(CalendarioFaseFF2021 calendarioFaseFF, boolean pareja1, boolean pareja2) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoParejasSegunda2021 ecmi = new EstructuraCampeonatoMasculinoParejasSegunda2021();
		Long fila = calendarioFaseFF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2021 ef : ecmi.getEstructuraCalendarioFinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_PAREJA_1) && pareja1) {
				updateRow(ef, fila, calendarioFaseFF.getPareja1Id());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_PAREJA_2) && pareja2) {
				updateRow(ef, fila, calendarioFaseFF.getPareja2Id());
			}
		}
		
		
	}
	
	public void actualizarResultadosEnfrentamientoDirectoOF(CalendarioFaseOF2021 calendarioFaseOF) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoParejasSegunda2021 ecmi = new EstructuraCampeonatoMasculinoParejasSegunda2021();
		Long fila = calendarioFaseOF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2021 ef : ecmi.getEstructuraCalendarioOctavosFinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_JUEGOS_PAREJA_1_P1)) {
				updateRow(ef, fila, calendarioFaseOF.getJuegosPareja1P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_JUEGOS_PAREJA_2_P1)) {
				updateRow(ef, fila, calendarioFaseOF.getJuegosPareja2P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_JUEGOS_PAREJA_1_P2)) {
				updateRow(ef, fila, calendarioFaseOF.getJuegosPareja1P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_JUEGOS_PAREJA_2_P2)) {
				updateRow(ef, fila, calendarioFaseOF.getJuegosPareja2P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_JUEGOS_PAREJA_1_P3)) {
				updateRow(ef, fila, calendarioFaseOF.getJuegosPareja1P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_JUEGOS_PAREJA_2_P3)) {
				updateRow(ef, fila, calendarioFaseOF.getJuegosPareja2P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_ACTIVO)) {
				calendarioFaseOF.setActivo(Activo2021.NO);
				updateRow(ef, fila, calendarioFaseOF.getActivo());
			}
		}
		
		
	}
	
	public void crearCalendarioFaseCF(){
		EstructuraCampeonatoMasculinoParejasSegunda2021 ecmi = new EstructuraCampeonatoMasculinoParejasSegunda2021();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CAL_FASE_CF, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CAL_FASE_CF, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA);
		}else{
			crearHojaCalendarioFaseCF();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CAL_FASE_CF, ecmi.getInsertCabeceraCalendarioFaseCFRow());
	}
	
	public void insertarRowCalendarioFaseCF(String insert){
		EstructuraCampeonatoMasculinoParejasSegunda2021 ecmi = new EstructuraCampeonatoMasculinoParejasSegunda2021();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CAL_FASE_CF, insert);
	}
	
	public void actualizarResultadosEnfrentamientoDirectoCF(CalendarioFaseCF2021 calendarioFaseCF) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoParejasSegunda2021 ecmi = new EstructuraCampeonatoMasculinoParejasSegunda2021();
		Long fila = calendarioFaseCF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2021 ef : ecmi.getEstructuraCalendarioCuartosFinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_JUEGOS_PAREJA_1_P1)) {
				updateRow(ef, fila, calendarioFaseCF.getJuegosPareja1P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_JUEGOS_PAREJA_2_P1)) {
				updateRow(ef, fila, calendarioFaseCF.getJuegosPareja2P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_JUEGOS_PAREJA_1_P2)) {
				updateRow(ef, fila, calendarioFaseCF.getJuegosPareja1P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_JUEGOS_PAREJA_2_P2)) {
				updateRow(ef, fila, calendarioFaseCF.getJuegosPareja2P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_JUEGOS_PAREJA_1_P3)) {
				updateRow(ef, fila, calendarioFaseCF.getJuegosPareja1P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_JUEGOS_PAREJA_2_P3)) {
				updateRow(ef, fila, calendarioFaseCF.getJuegosPareja2P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_ACTIVO)) {
				calendarioFaseCF.setActivo(Activo2021.NO);
				updateRow(ef, fila, calendarioFaseCF.getActivo());
			}
		}
		
		
	}
	
	public void crearCalendarioFaseSF(){
		EstructuraCampeonatoMasculinoParejasSegunda2021 ecmi = new EstructuraCampeonatoMasculinoParejasSegunda2021();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CAL_FASE_SF, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CAL_FASE_SF, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA);
		}else{
			crearHojaCalendarioFaseSF();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CAL_FASE_SF, ecmi.getInsertCabeceraCalendarioFaseSFRow());
	}
	
	public void insertarRowCalendarioFaseSF(String insert){
		EstructuraCampeonatoMasculinoParejasSegunda2021 ecmi = new EstructuraCampeonatoMasculinoParejasSegunda2021();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CAL_FASE_SF, insert);
	}
	
	public void actualizarResultadosEnfrentamientoDirectoSF(CalendarioFaseSF2021 calendarioFaseSF) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoParejasSegunda2021 ecmi = new EstructuraCampeonatoMasculinoParejasSegunda2021();
		Long fila = calendarioFaseSF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2021 ef : ecmi.getEstructuraCalendarioSemifinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_PAREJA_1_P1)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosPareja1P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_PAREJA_2_P1)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosPareja2P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_PAREJA_1_P2)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosPareja1P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_PAREJA_2_P2)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosPareja2P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_PAREJA_1_P3)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosPareja1P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_PAREJA_2_P3)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosPareja2P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_ACTIVO)) {
				calendarioFaseSF.setActivo(Activo2021.NO);
				updateRow(ef, fila, calendarioFaseSF.getActivo());
			}
		}
		
		
	}
	
	public void crearCalendarioFaseFC(){
		EstructuraCampeonatoMasculinoParejasSegunda2021 ecmi = new EstructuraCampeonatoMasculinoParejasSegunda2021();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CAL_FASE_FC, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CAL_FASE_FC, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA);
		}else{
			crearHojaCalendarioFaseFC();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CAL_FASE_FC, ecmi.getInsertCabeceraCalendarioFaseFCRow());
	}
	
	public void insertarRowCalendarioFaseFC(String insert){
		EstructuraCampeonatoMasculinoParejasSegunda2021 ecmi = new EstructuraCampeonatoMasculinoParejasSegunda2021();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CAL_FASE_FC, insert);
	}
	
	public void actualizarResultadosEnfrentamientoDirectoFC(CalendarioFaseFC2021 calendarioFaseFC) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoParejasSegunda2021 ecmi = new EstructuraCampeonatoMasculinoParejasSegunda2021();
		Long fila = calendarioFaseFC.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2021 ef : ecmi.getEstructuraCalendarioFinalConsolacion()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_JUEGOS_PAREJA_1_P1)) {
				updateRow(ef, fila, calendarioFaseFC.getJuegosPareja1P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_JUEGOS_PAREJA_2_P1)) {
				updateRow(ef, fila, calendarioFaseFC.getJuegosPareja2P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_JUEGOS_PAREJA_1_P2)) {
				updateRow(ef, fila, calendarioFaseFC.getJuegosPareja1P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_JUEGOS_PAREJA_2_P2)) {
				updateRow(ef, fila, calendarioFaseFC.getJuegosPareja2P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_JUEGOS_PAREJA_1_P3)) {
				updateRow(ef, fila, calendarioFaseFC.getJuegosPareja1P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_JUEGOS_PAREJA_2_P3)) {
				updateRow(ef, fila, calendarioFaseFC.getJuegosPareja2P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_ACTIVO)) {
				calendarioFaseFC.setActivo(Activo2021.NO);
				updateRow(ef, fila, calendarioFaseFC.getActivo());
			}
		}
		
		
	}
	
	public void crearCalendarioFaseFF(){
		EstructuraCampeonatoMasculinoParejasSegunda2021 ecmi = new EstructuraCampeonatoMasculinoParejasSegunda2021();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CAL_FASE_FF, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CAL_FASE_FF, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA);
		}else{
			crearHojaCalendarioFaseFF();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CAL_FASE_FF, ecmi.getInsertCabeceraCalendarioFaseFFRow());
	}
	
	public void insertarRowCalendarioFaseFF(String insert){
		EstructuraCampeonatoMasculinoParejasSegunda2021 ecmi = new EstructuraCampeonatoMasculinoParejasSegunda2021();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CAL_FASE_FF, insert);
	}
	
	public void actualizarResultadosEnfrentamientoDirectoFF(CalendarioFaseFF2021 calendarioFaseFF) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoParejasSegunda2021 ecmi = new EstructuraCampeonatoMasculinoParejasSegunda2021();
		Long fila = calendarioFaseFF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2021 ef : ecmi.getEstructuraCalendarioFinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_PAREJA_1_P1)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosPareja1P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_PAREJA_2_P1)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosPareja2P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_PAREJA_1_P2)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosPareja1P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_PAREJA_2_P2)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosPareja2P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_PAREJA_1_P3)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosPareja1P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_PAREJA_2_P3)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosPareja2P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_ACTIVO)) {
				calendarioFaseFF.setActivo(Activo2021.NO);
				updateRow(ef, fila, calendarioFaseFF.getActivo());
			}
		}
		
		
	}
	
	public void borrarSorteo(){
		deleteFile(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CAL_FASE_I, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA);
		deleteFile(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CLA_FASE_I, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA);
		deleteFile(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CAL_FASE_II, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA);
		deleteFile(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CLA_FASE_II, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA);
		deleteFile(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CAL_FASE_OF, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA);
		deleteFile(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CAL_FASE_CF, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA);
		deleteFile(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CAL_FASE_SF, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA);
		deleteFile(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CAL_FASE_FC, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA);
		deleteFile(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_SEGUNDA_CAL_FASE_FF, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_PAREJAS_SEGUNDA);
		actualizarEstadoConfiguracion(EstadosIndividualParejas2021.CONFIG_ESTADO_CERRAR_INSCRIPCIONES);
	}
	
	

}
