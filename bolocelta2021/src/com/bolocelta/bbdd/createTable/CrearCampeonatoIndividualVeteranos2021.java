package com.bolocelta.bbdd.createTable;

import com.bolocelta.bbdd.constants.Activo2021;
import com.bolocelta.bbdd.constants.NombresTablas2021;
import com.bolocelta.bbdd.constants.Ubicaciones2021;
import com.bolocelta.bbdd.constants.structure.Estructura2021;
import com.bolocelta.bbdd.constants.structure.EstructuraCampeonatoIndividualVeteranos2021;
import com.bolocelta.entities.CampeonatoVeteranosIndividualClasificacion2021;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseFF2021;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseSF2021;

public class CrearCampeonatoIndividualVeteranos2021 extends ACrearModificar2021 {
	
	public void actualizarClasificacionConfirmar(CampeonatoVeteranosIndividualClasificacion2021 cecCla) {
		
		Long filaCla = cecCla.getRowNum();
				
		if(cecCla.getActivo() == 1){
		
			//Actualizar clasificacion de equipo 1
			EstructuraCampeonatoIndividualVeteranos2021 ecl1 = new EstructuraCampeonatoIndividualVeteranos2021(
					NombresTablas2021.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CLASIFICACION);
			cecCla.setActivo(Activo2021.NO_NUMBER);
			for (Estructura2021 ef : ecl1.getClasificacionList()) {
				if (ef.getValor().equals(ecl1.COL_CLASIFICACION_ACTIVO)) {
					updateRowClasificacion(ef, filaCla, cecCla.getActivo(), cecCla);
				}
			}
		}
	}

	public void actualizarClasificacion(CampeonatoVeteranosIndividualClasificacion2021 cecCla) {
		
		Long filaCla = cecCla.getRowNum();
				
		if(cecCla.getActivo() == 1){
		
			//Actualizar clasificacion de equipo 1
			EstructuraCampeonatoIndividualVeteranos2021 ecl1 = new EstructuraCampeonatoIndividualVeteranos2021(
					NombresTablas2021.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CLASIFICACION);
			for (Estructura2021 ef : ecl1.getClasificacionList()) {
				if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA_1)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada1(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA_1)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada1(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA_2)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada2(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA_2)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada2(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA_3)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada3(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA_3)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada3(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA_4)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada4(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA_4)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada4(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA_5)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada5(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA_5)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada5(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA_6)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada6(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA_6)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada6(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA_7)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada7(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA_7)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada7(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA_8)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada8(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA_8)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada8(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA_9)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada9(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA_9)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada9(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA_10)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada10(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA_10)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada10(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA_11)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada11(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA_11)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada11(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA_12)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada12(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA_12)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada12(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA_13)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada13(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA_13)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada13(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA_14)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada14(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA_14)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada14(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA_15)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada15(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA_15)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada15(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA_16)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada16(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA_16)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada16(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA_17)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada17(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA_17)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada17(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA_18)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada18(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA_18)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada18(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA_19)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada19(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA_19)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada19(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA_20)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada20(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA_20)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada20(), cecCla);
				}
			}
		}
	}
	
	public void updateRowClasificacion(Estructura2021 update, Long fila, Object newValue, CampeonatoVeteranosIndividualClasificacion2021 cec) {
		updateDataCsv(update.getExcel(), Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS, fila, update.getColumna(), newValue, update.getTipoDato());
	}
	
	
	public void actualizarJugadorSemiFinal(CalendarioFaseSF2021 calendarioFaseSF, boolean jugador1, boolean jugador2) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoIndividualVeteranos2021 ecmi = new EstructuraCampeonatoIndividualVeteranos2021();
		Long fila = calendarioFaseSF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2021 ef : ecmi.getEstructuraCalendarioSemifinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUGADOR_1)) {
				updateRow(ef, fila, calendarioFaseSF.getJugador1Id());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUGADOR_2)) {
				updateRow(ef, fila, calendarioFaseSF.getJugador2Id());
			}
		}
		
		
	}
		
	public void actualizarJugadorFinal(CalendarioFaseFF2021 calendarioFaseFF, boolean jugador1, boolean jugador2) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoIndividualVeteranos2021 ecmi = new EstructuraCampeonatoIndividualVeteranos2021();
		Long fila = calendarioFaseFF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2021 ef : ecmi.getEstructuraCalendarioFinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUGADOR_1) && jugador1) {
				updateRow(ef, fila, calendarioFaseFF.getJugador1Id());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUGADOR_2) && jugador2) {
				updateRow(ef, fila, calendarioFaseFF.getJugador2Id());
			}
		}
		
		
	}

	public void updateRow(Estructura2021 update, Long fila, Object newValue) {
		updateDataCsv(update.getExcel(), Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS, fila, update.getColumna(), newValue, update.getTipoDato());
	}
	
	
	public void actualizarResultadosEnfrentamientoDirectoSF(CalendarioFaseSF2021 calendarioFaseSF) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoIndividualVeteranos2021 ecmi = new EstructuraCampeonatoIndividualVeteranos2021();
		Long fila = calendarioFaseSF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2021 ef : ecmi.getEstructuraCalendarioSemifinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_1_P1)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosJugador1P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_2_P1)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosJugador2P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_1_P2)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosJugador1P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_2_P2)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosJugador2P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_1_P3)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosJugador1P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_2_P3)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosJugador2P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_ACTIVO)) {
				calendarioFaseSF.setActivo(Activo2021.NO);
				updateRow(ef, fila, calendarioFaseSF.getActivo());
			}
		}
		
		
	}
	
	public void actualizarResultadosEnfrentamientoDirectoFF(CalendarioFaseFF2021 calendarioFaseFF) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoIndividualVeteranos2021 ecmi = new EstructuraCampeonatoIndividualVeteranos2021();
		Long fila = calendarioFaseFF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2021 ef : ecmi.getEstructuraCalendarioFinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_1_P1)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosJugador1P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_2_P1)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosJugador2P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_1_P2)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosJugador1P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_2_P2)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosJugador2P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_1_P3)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosJugador1P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_2_P3)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosJugador2P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_ACTIVO)) {
				calendarioFaseFF.setActivo(Activo2021.NO);
				updateRow(ef, fila, calendarioFaseFF.getActivo());
			}
		}
		
		
	}

}
