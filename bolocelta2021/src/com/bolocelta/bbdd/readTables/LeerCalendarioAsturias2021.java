package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.Modalidad2021;
import com.bolocelta.bbdd.constants.NombresTablas2021;
import com.bolocelta.bbdd.constants.Ubicaciones2021;
import com.bolocelta.entities.CalendarioAsturias2021;
import com.bolocelta.entities.CalendarioEspana2021;
import com.bolocelta.transformer.CalendarioAsturiasTransformer2021;

public class LeerCalendarioAsturias2021 extends ALeer2021 {

	private LeerCategorias2021 leerCategorias = new LeerCategorias2021();
	private LeerModalidades2021 leerModalidades = new LeerModalidades2021();
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, CalendarioAsturias2021> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CALENDARIO_AST, Ubicaciones2021.UBICACION_BBDD_ADMINISTRACION);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
	    		CalendarioAsturias2021 calendarioAsturias = new CalendarioAsturias2021();
	    		calendarioAsturias = CalendarioAsturiasTransformer2021.transformerObjectCsv(calendarioAsturias, row);
		    	result.put(calendarioAsturias.getId(), calendarioAsturias);
		    }
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public CalendarioAsturias2021 read(Integer id) {
		HashMap<Integer, CalendarioAsturias2021> result = (HashMap<Integer, CalendarioAsturias2021>) readAll();
		return result.get(id);
	}

	@Override
	public List<?> listResult() {
		List<CalendarioAsturias2021> listResult = new ArrayList<CalendarioAsturias2021>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    CalendarioAsturias2021 calendarioAsturias = (CalendarioAsturias2021) value;
		    if(calendarioAsturias.getId() > 0){
		    	if(calendarioAsturias.getCategoriaId() != null){
		    		calendarioAsturias.setCategoria(leerCategorias.read(calendarioAsturias.getCategoriaId()));
		    	}
		    	if(calendarioAsturias.getModalidadId() != null){
		    		calendarioAsturias.setModalidad(leerModalidades.read(calendarioAsturias.getModalidadId()));
		    	}
		    	listResult.add((CalendarioAsturias2021) calendarioAsturias);
		    }
		}
		
		
		//Ordenar el calendario
		Collections.sort(listResult, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				CalendarioAsturias2021 cec1 = (CalendarioAsturias2021) o1;
				CalendarioAsturias2021 cec2 = (CalendarioAsturias2021) o2;
				
				int comp1 = cec1.getFechaDesde().compareTo(cec2.getFechaDesde());
				if (comp1 == 0) {
					int comp2 = cec1.getFechaHasta().compareTo(cec2.getFechaHasta());
					if (comp2 == 0) {
						return cec1.getId().compareTo(cec2.getId());
					}
					return comp2;
				}
				return comp1;
			}
		});
		
		
		return listResult;
	}
	
	public List<?> listResultLiga() {
		List<CalendarioAsturias2021> listResult = new ArrayList<CalendarioAsturias2021>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    CalendarioAsturias2021 calendarioAsturias = (CalendarioAsturias2021) value;
		    if(calendarioAsturias.getId() > 0){
		    	if(calendarioAsturias.getModalidadId() == Modalidad2021.EQUIPOS_LIGA){
		    		if(calendarioAsturias.getCategoriaId() != null){
			    		calendarioAsturias.setCategoria(leerCategorias.read(calendarioAsturias.getCategoriaId()));
			    	}
			    	if(calendarioAsturias.getModalidadId() != null){
			    		calendarioAsturias.setModalidad(leerModalidades.read(calendarioAsturias.getModalidadId()));
			    	}
			    	listResult.add((CalendarioAsturias2021) calendarioAsturias);
		    	}
		    }
		}
		return listResult;
	}

}
