package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.inject.Named;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas2021;
import com.bolocelta.bbdd.constants.Ubicaciones2021;
import com.bolocelta.entities.Documentos2021;
import com.bolocelta.transformer.DocumentosTransformer2021;

@Named
public class LeerDocumentos2021 extends ALeer2021 {
	
	HashMap<Integer, Documentos2021> result = null;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, Documentos2021> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2021.N_CSV_BBDD_DOCUMENTOS, Ubicaciones2021.UBICACION_BBDD_DOCUMENTOS);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	Documentos2021 documentos = new Documentos2021();
		    	documentos = DocumentosTransformer2021.transformerObjectCsv(documentos, row);
		    	result.put(documentos.getId(), documentos);
		    }
		}
	    return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Documentos2021 read(Integer id) {
		if(this.result == null){
			result = (HashMap<Integer, Documentos2021>) readAll();
		}
		return this.result.get(id);
	}
	
	@Override
	public List<?> listResult() {
		List<Documentos2021> listResult = new ArrayList<Documentos2021>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    Documentos2021 documentos = (Documentos2021) value;
		    if(documentos.getId() > 0){
		    	listResult.add(documentos);
		    }
		}
		return listResult;
	}

}
