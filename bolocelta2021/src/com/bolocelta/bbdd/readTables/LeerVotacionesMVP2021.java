package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas2021;
import com.bolocelta.bbdd.constants.Ubicaciones2021;
import com.bolocelta.entities.PremiosMVP2021;
import com.bolocelta.entities.VotacionesMVP2021;
import com.bolocelta.transformer.VotacionesMVPTransformer2021;

public class LeerVotacionesMVP2021 extends ALeer2021 {

	private LeerJugadores2021 leerJugadores = new LeerJugadores2021();
	private LeerEquipos2021 leerEquipos = new LeerEquipos2021();
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<Integer, ?> readAllVotaciones() {
		HashMap<Integer, VotacionesMVP2021> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2021.N_CSV_BBDD_VOTACIONES_MVP, Ubicaciones2021.UBICACION_BBDD_PREMIOS);
		if(csvParser != null){
			for (CSVRecord row : csvParser) {
				VotacionesMVP2021 votacionesMVP2021 = new VotacionesMVP2021();
		    	votacionesMVP2021.setRowNum(row.getRecordNumber());
		    	votacionesMVP2021 = VotacionesMVPTransformer2021.transformerObjectCalendarioCsv(votacionesMVP2021, row);
		    	result.put(votacionesMVP2021.getId(), votacionesMVP2021);
		    }
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public VotacionesMVP2021 readPremio(Integer id) {
		HashMap<Integer, VotacionesMVP2021> result = (HashMap<Integer, VotacionesMVP2021>) readAllVotaciones();
		return result.get(id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object read(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HashMap<Integer, ?> readAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<?> listResult() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public List<?> listResultVotaciones(Integer categoria) {
		
		List<VotacionesMVP2021> listResult = new ArrayList<VotacionesMVP2021>();
		
		for (Entry<Integer, ?> entry : readAllVotaciones().entrySet()) {
		    Object value = entry.getValue();
		    VotacionesMVP2021 cec = (VotacionesMVP2021) value;
		    if(categoria == cec.getCategoria()){
		    	cec.setJugador(leerJugadores.read(cec.getIdJugador(), true));
		    	listResult.add(cec);
		    }
		}

		return listResult;
	}

}
