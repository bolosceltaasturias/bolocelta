package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.inject.Named;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.application.enumerations.ModalidadJuegoEnumeration2021;
import com.bolocelta.bbdd.constants.NombresTablas2021;
import com.bolocelta.bbdd.constants.Ubicaciones2021;
import com.bolocelta.entities.HistoricoCampeonatosEspana2021;
import com.bolocelta.transformer.HistoricoCampeonatoEspanaTransformer2021;

@Named
public class LeerHistoricoCampeonatosEspana2021 extends ALeer2021 {

	HashMap<Integer, HistoricoCampeonatosEspana2021> result = null;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, HistoricoCampeonatosEspana2021> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2021.N_CSV_BBDD_HISTORICO_ESPANA, Ubicaciones2021.UBICACION_BBDD_HISTORICO);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	HistoricoCampeonatosEspana2021 historicoCE = new HistoricoCampeonatosEspana2021();
	    		historicoCE = HistoricoCampeonatoEspanaTransformer2021.transformerObjectCsv(historicoCE, row);
		    	result.put(historicoCE.getId(), historicoCE);
		    }
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public HistoricoCampeonatosEspana2021 read(Integer id) {
		if(this.result == null){
			this.result = (HashMap<Integer, HistoricoCampeonatosEspana2021>) readAll();
		}
		return result.get(id);
	}

	@Override
	public List<?> listResult() {
		List<HistoricoCampeonatosEspana2021> listResult = new ArrayList<HistoricoCampeonatosEspana2021>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    listResult.add((HistoricoCampeonatosEspana2021) value);
		}
		return listResult;
	}
	
	public List<?> listResultEquipos() {
		List<HistoricoCampeonatosEspana2021> listResult = new ArrayList<HistoricoCampeonatosEspana2021>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    HistoricoCampeonatosEspana2021 historicoCE = (HistoricoCampeonatosEspana2021) value;
		    if(historicoCE.getTipo() == ModalidadJuegoEnumeration2021.MODALIDAD_EQUIPOS){
		    	listResult.add((HistoricoCampeonatosEspana2021) value);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultIndividual() {
		List<HistoricoCampeonatosEspana2021> listResult = new ArrayList<HistoricoCampeonatosEspana2021>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    HistoricoCampeonatosEspana2021 historicoCE = (HistoricoCampeonatosEspana2021) value;
		    if(historicoCE.getTipo() == ModalidadJuegoEnumeration2021.MODALIDAD_INDIVIDUAL){
		    	listResult.add((HistoricoCampeonatosEspana2021) value);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultParejas() {
		List<HistoricoCampeonatosEspana2021> listResult = new ArrayList<HistoricoCampeonatosEspana2021>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    HistoricoCampeonatosEspana2021 historicoCE = (HistoricoCampeonatosEspana2021) value;
		    if(historicoCE.getTipo() == ModalidadJuegoEnumeration2021.MODALIDAD_PAREJAS){
		    	listResult.add((HistoricoCampeonatosEspana2021) value);
		    }
		}
		return listResult;
	}
	

	public List<?> listResultIndividualFemenino() {
		List<HistoricoCampeonatosEspana2021> listResult = new ArrayList<HistoricoCampeonatosEspana2021>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    HistoricoCampeonatosEspana2021 historicoCA = (HistoricoCampeonatosEspana2021) value;
		    if(historicoCA.getTipo() == ModalidadJuegoEnumeration2021.MODALIDAD_INDIVIDUAL_FEMENINO){
		    	listResult.add((HistoricoCampeonatosEspana2021) value);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultParejasFemenino() {
		List<HistoricoCampeonatosEspana2021> listResult = new ArrayList<HistoricoCampeonatosEspana2021>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    HistoricoCampeonatosEspana2021 historicoCA = (HistoricoCampeonatosEspana2021) value;
		    if(historicoCA.getTipo() == ModalidadJuegoEnumeration2021.MODALIDAD_PAREJAS_FEMENINO){
		    	listResult.add((HistoricoCampeonatosEspana2021) value);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultMixto() {
		List<HistoricoCampeonatosEspana2021> listResult = new ArrayList<HistoricoCampeonatosEspana2021>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    HistoricoCampeonatosEspana2021 historicoCA = (HistoricoCampeonatosEspana2021) value;
		    if(historicoCA.getTipo() == ModalidadJuegoEnumeration2021.MODALIDAD_MIXTO){
		    	listResult.add((HistoricoCampeonatosEspana2021) value);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultVeteranos() {
		List<HistoricoCampeonatosEspana2021> listResult = new ArrayList<HistoricoCampeonatosEspana2021>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    HistoricoCampeonatosEspana2021 historicoCA = (HistoricoCampeonatosEspana2021) value;
		    if(historicoCA.getTipo() == ModalidadJuegoEnumeration2021.MODALIDAD_VETERANOS){
		    	listResult.add((HistoricoCampeonatosEspana2021) value);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultJuveniles() {
		List<HistoricoCampeonatosEspana2021> listResult = new ArrayList<HistoricoCampeonatosEspana2021>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    HistoricoCampeonatosEspana2021 historicoCA = (HistoricoCampeonatosEspana2021) value;
		    if(historicoCA.getTipo() == ModalidadJuegoEnumeration2021.MODALIDAD_JUVENILES){
		    	listResult.add((HistoricoCampeonatosEspana2021) value);
		    }
		}
		return listResult;
	}

}
