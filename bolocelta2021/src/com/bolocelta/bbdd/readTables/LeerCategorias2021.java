package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.inject.Named;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.application.enumerations.ActivoEnumeration2021;
import com.bolocelta.bbdd.constants.NombresTablas2021;
import com.bolocelta.bbdd.constants.Ubicaciones2021;
import com.bolocelta.entities.Categorias2021;
import com.bolocelta.transformer.CategoriasTransformer2021;

@Named
public class LeerCategorias2021 extends ALeer2021 {

	HashMap<Integer, Categorias2021> result = null;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, Categorias2021> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CATEGORIAS, Ubicaciones2021.UBICACION_BBDD_ADMINISTRACION);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
	    		Categorias2021 categoria = new Categorias2021();
	    		categoria = CategoriasTransformer2021.transformerObjectCsv(categoria, row);
		    	result.put(categoria.getId(), categoria);
		    }
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Categorias2021 read(Integer id) {
		if(this.result == null){
			this.result = (HashMap<Integer, Categorias2021>) readAll();
		}
		return result.get(id);
	}

	@Override
	public List<?> listResult() {
		List<Categorias2021> listResult = new ArrayList<Categorias2021>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    listResult.add((Categorias2021) value);
		}
		return listResult;
	}
	
	public List<?> listResultIndividual() {
		List<Categorias2021> listResult = new ArrayList<Categorias2021>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    Categorias2021 categoria = (Categorias2021) value;
		    if(categoria.getIndividual().equalsIgnoreCase(ActivoEnumeration2021.SI)){
		    	listResult.add((Categorias2021) value);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultParejas() {
		List<Categorias2021> listResult = new ArrayList<Categorias2021>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    Categorias2021 categoria = (Categorias2021) value;
		    if(categoria.getParejas().equalsIgnoreCase(ActivoEnumeration2021.SI)){
		    	listResult.add((Categorias2021) value);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultLiga() {
		List<Categorias2021> listResult = new ArrayList<Categorias2021>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    Categorias2021 categoria = (Categorias2021) value;
//		    System.out.println(categoria.getId());
		    if(categoria.getLiga().equalsIgnoreCase(ActivoEnumeration2021.SI)){
		    	listResult.add((Categorias2021) value);
		    }
		}
		return listResult;
	}

}
