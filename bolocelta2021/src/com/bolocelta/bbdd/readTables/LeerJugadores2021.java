package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas2021;
import com.bolocelta.bbdd.constants.Ubicaciones2021;
import com.bolocelta.entities.CampeonatoEquiposClasificacion2021;
import com.bolocelta.entities.Jugadores2021;
import com.bolocelta.transformer.JugadoresTransformer2021;

public class LeerJugadores2021 extends ALeer2021 {

	private final String nameFile = NombresTablas2021.N_CSV_BBDD_JUGADORES;
	private final String path = Ubicaciones2021.UBICACION_BBDD_ADMINISTRACION;
	
	HashMap<Integer, Jugadores2021> result = null;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, Jugadores2021> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(nameFile, path);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
	    		Jugadores2021 jugador = new Jugadores2021();
	    		jugador.setRowNum(row.getRecordNumber());
	    		jugador = JugadoresTransformer2021.transformerObjectCsv(jugador, row);
		    	result.put(jugador.getId(), jugador);
		    }
		}
		return result;
	}
	
	public Long getLastRowSheet(){
		return lastRowSheet(nameFile, path);
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public Jugadores2021 read(Integer id) {
		Jugadores2021 jugador = null;
		if(jugador == null){
			if(this.result == null){
				this.result = (HashMap<Integer, Jugadores2021>) readAll();
			}
			jugador = result.get(id);
			if(jugador != null && jugador.getId() != null && jugador.getId() > 0){
//		    	if(jugador.getEquipoId() != null){
//		    		if(leerEquipos != null){
//		    			jugador.setEquipo(leerEquipos.read(jugador.getEquipoId()));
//		    		}
//		    	}
		    }
		}
		return jugador;
	}
	
	@SuppressWarnings("unchecked")
	public Jugadores2021 read(Integer id, boolean readTeam) {
		Jugadores2021 jugador = null;
		if(jugador == null){
			if(this.result == null){
				this.result = (HashMap<Integer, Jugadores2021>) readAll();
			}
			jugador = result.get(id);
			if(jugador != null && jugador.getId() != null && jugador.getId() > 0){
				if(readTeam){
			    	if(jugador.getEquipoId() != null){
			    		LeerEquipos2021 leerEquipos = new LeerEquipos2021();
			    		if(leerEquipos != null){
			    			jugador.setEquipo(leerEquipos.read(jugador.getEquipoId()));
			    		}
			    	}
				}
		    }
		}
		return jugador;
	}
	
	public void refreshList(){
		this.result = null;
	}
	
	@SuppressWarnings("unchecked")
	public List<Jugadores2021> readEquipo(Integer id) {
		List<Jugadores2021> jugadoresEquipo = new ArrayList<>();
		List<Jugadores2021> allJugadores = (List<Jugadores2021>) listResult();
		for (Jugadores2021 jugador : allJugadores) {
			if(jugador.getEquipoId() != null && jugador.getEquipoId().equals(id)){
				LeerEquipos2021 leerEquipos = new LeerEquipos2021();
	    		if(leerEquipos != null){
	    			jugador.setEquipo(leerEquipos.read(jugador.getEquipoId()));
	    		}
	    		jugadoresEquipo.add(jugador);
			}
		}
		return jugadoresEquipo;
	}

	@Override
	public List<?> listResult() {
		List<Jugadores2021> listResult = new ArrayList<Jugadores2021>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    Jugadores2021 jugador = (Jugadores2021) value;
		    if(jugador.getId() != null && jugador.getId() > 0){
//		    	if(jugador.getEquipoId() != null){
//		    		if(leerEquipos != null){
//		    			jugador.setEquipo(leerEquipos.read(jugador.getEquipoId()));
//		    		}
//		    	}
		    	listResult.add((Jugadores2021) jugador);
		    }
		}
		return listResult;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Jugadores2021> readCategoria(Integer id) {
		List<Jugadores2021> jugadoresCategoria = new ArrayList<>();
		List<Jugadores2021> allJugadores = (List<Jugadores2021>) listResult();
		for (Jugadores2021 jugador : allJugadores) {
			LeerEquipos2021 leerEquipos = new LeerEquipos2021();
    		if(leerEquipos != null){
    			jugador.setEquipo(leerEquipos.read(jugador.getEquipoId()));
    		}
    		if(jugador.getEquipo().getCategoriaId().equals(id) && jugador.isMasculino()){
    			jugadoresCategoria.add(jugador);
    		}
		}
		
		
		
		
		
		//Ordenar la clasificacion
		Collections.sort(jugadoresCategoria, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				Jugadores2021 jug1 = (Jugadores2021) o1;
				Jugadores2021 jug2 = (Jugadores2021) o2;
				
				int total = jug1.getTotalAcabones().compareTo(jug2.getTotalAcabones());
				if (total == 0) {
					int promedio = jug1.getPromedioAcabones().compareTo(jug2.getPromedioAcabones());
					if (promedio == 0) {
						int nombre = jug2.getNombreEquipo().compareTo(jug1.getNombreEquipo());
						return nombre;
					}else{
						return promedio;
					}
				}
				return total;
			}
		});
		
		Collections.reverse(jugadoresCategoria);
		
		
		
		return jugadoresCategoria;
	}
}
