package com.bolocelta.bbdd.readTables.usuarios;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.inject.Named;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas2021;
import com.bolocelta.bbdd.constants.Ubicaciones2021;
import com.bolocelta.bbdd.readTables.ALeer2021;
import com.bolocelta.entities.usuarios.Roles2021;
import com.bolocelta.transformer.usuarios.RolesTransformer2021;

@Named
public class LeerRoles2021 extends ALeer2021 {
	
	HashMap<Integer, Roles2021> result = null;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, Roles2021> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2021.N_CSV_BBDD_ROLES, Ubicaciones2021.UBICACION_BBDD_USUARIOS);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	Roles2021 rol = new Roles2021();
		    	rol = RolesTransformer2021.transformerObjectCsv(rol, row);
		    	result.put(rol.getId(), rol);
		    }
		}
	    return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Roles2021 read(Integer id) {
		if(this.result == null){
			result = (HashMap<Integer, Roles2021>) readAll();
		}
		return this.result.get(id);
	}
	
	@Override
	public List<?> listResult() {
		List<Roles2021> listResult = new ArrayList<Roles2021>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    Roles2021 rol = (Roles2021) value;
		    if(rol.getId() > 0){
		    	listResult.add(rol);
		    }
		}
		return listResult;
	}

}
