package com.bolocelta.bbdd.readTables.usuarios;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.inject.Named;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas2021;
import com.bolocelta.bbdd.constants.Ubicaciones2021;
import com.bolocelta.bbdd.readTables.ALeer2021;
import com.bolocelta.entities.usuarios.Usuarios2021;
import com.bolocelta.transformer.usuarios.UsuariosTransformer2021;

@Named
public class LeerUsuarios2021 extends ALeer2021 {

	private LeerRoles2021 leerRoles = new LeerRoles2021();

	HashMap<Integer, Usuarios2021> result = null;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, Usuarios2021> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2021.N_CSV_BBDD_USUARIOS, Ubicaciones2021.UBICACION_BBDD_USUARIOS);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	Usuarios2021 usuario = new Usuarios2021();
		    	usuario = UsuariosTransformer2021.transformerObjectCsv(usuario, row);
		    	result.put(usuario.getId(), usuario);
		    }
		}
	    return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Usuarios2021 read(Integer id) {
		if (this.result == null) {
			result = (HashMap<Integer, Usuarios2021>) readAll();
		}
		return this.result.get(id);
	}

	@SuppressWarnings("unchecked")
	public Usuarios2021 read(String user, String pass) {
		if (user != null && pass != null) {
			for (Object object : listResult()) {
				Usuarios2021 usuario = (Usuarios2021) object;
				if (usuario.getUser().equals(user) && usuario.getPass().equals(pass)) {
					return usuario;
				}
			}
		}
		return null;
	}

	@Override
	public List<?> listResult() {
		List<Usuarios2021> listResult = new ArrayList<Usuarios2021>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
			Object value = entry.getValue();
			Usuarios2021 usuario = (Usuarios2021) value;
			if (usuario.getId() > 0) {
				if (usuario.getRolId() != null) {
					usuario.setRol(leerRoles.read(usuario.getRolId()));
				}
				listResult.add(usuario);
			}
		}
		return listResult;
	}

}
