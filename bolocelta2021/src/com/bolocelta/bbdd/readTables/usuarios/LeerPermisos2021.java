package com.bolocelta.bbdd.readTables.usuarios;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.inject.Named;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas2021;
import com.bolocelta.bbdd.constants.Ubicaciones2021;
import com.bolocelta.bbdd.readTables.ALeer2021;
import com.bolocelta.entities.usuarios.Permisos2021;
import com.bolocelta.transformer.usuarios.PermisosTransformer2021;

@Named
public class LeerPermisos2021 extends ALeer2021 {

	private LeerRoles2021 leerRoles = new LeerRoles2021();

	HashMap<Integer, Permisos2021> result = null;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, Permisos2021> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2021.N_CSV_BBDD_PERMISOS, Ubicaciones2021.UBICACION_BBDD_USUARIOS);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	Permisos2021 permiso = new Permisos2021();
		    	permiso = PermisosTransformer2021.transformerObjectCsv(permiso, row);
		    	result.put(permiso.getId(), permiso);
		    }
		}
	    return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Permisos2021 read(Integer id) {
		if (this.result == null) {
			result = (HashMap<Integer, Permisos2021>) readAll();
		}
		return this.result.get(id);
	}

	@Override
	public List<?> listResult() {
		List<Permisos2021> listResult = new ArrayList<Permisos2021>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
			Object value = entry.getValue();
			Permisos2021 permiso = (Permisos2021) value;
			if (permiso.getId() > 0) {
				if (permiso.getRolId() != null) {
					permiso.setRol(leerRoles.read(permiso.getRolId()));
				}
				listResult.add(permiso);
			}
		}
		return listResult;
	}
	
	public List<?> listResultByRol(Integer rolId) {
		List<Permisos2021> listResult = new ArrayList<Permisos2021>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
			Object value = entry.getValue();
			Permisos2021 permiso = (Permisos2021) value;
			if(permiso != null && permiso.getId() != null){
				if (permiso.getId() > 0) {
					if(permiso.getRolId()== rolId){
						if (permiso.getRolId() != null) {
							permiso.setRol(leerRoles.read(permiso.getRolId()));
						}
						listResult.add(permiso);
					}
				}
			}
		}
		return listResult;
	}

}
