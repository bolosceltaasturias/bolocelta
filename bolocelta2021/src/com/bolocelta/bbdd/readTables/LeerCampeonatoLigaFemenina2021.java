package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas2021;
import com.bolocelta.bbdd.constants.Ubicaciones2021;
import com.bolocelta.entities.CampeonatoLigaFemeninaCalendario2021;
import com.bolocelta.entities.CampeonatoLigaFemeninaClasificacion2021;
import com.bolocelta.transformer.CampeonatoLigaFemeninaTransformer2021;

public class LeerCampeonatoLigaFemenina2021 extends ALeer2021 {

	private LeerJugadores2021 leerJugadores = new LeerJugadores2021();
	private LeerBoleras2021 leerBoleras = new LeerBoleras2021();
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<Integer, ?> readAllClasificacion() {
		HashMap<Integer, CampeonatoLigaFemeninaClasificacion2021> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_CLA_LIGA_FEMENINA, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_EQUIPOS);
		if(csvParser != null){
			for (CSVRecord row : csvParser) {
		    	CampeonatoLigaFemeninaClasificacion2021 campeonatoLigaFemeninaClasificacion = new CampeonatoLigaFemeninaClasificacion2021();
		    	campeonatoLigaFemeninaClasificacion.setRowNum(row.getRecordNumber());
		    	campeonatoLigaFemeninaClasificacion = CampeonatoLigaFemeninaTransformer2021.transformerObjectClasificacionCsv(campeonatoLigaFemeninaClasificacion, row);
		    	result.put(campeonatoLigaFemeninaClasificacion.getId(), campeonatoLigaFemeninaClasificacion);
		    }
		}
		return result;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<Integer, ?> readAllCalendario() {
		HashMap<Integer, CampeonatoLigaFemeninaCalendario2021> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_CAL_LIGA_FEMENINA, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_EQUIPOS);
		if(csvParser != null){
			for (CSVRecord row : csvParser) {
				CampeonatoLigaFemeninaCalendario2021 campeonatoLigaFemeninaCalendario = new CampeonatoLigaFemeninaCalendario2021();
		    	campeonatoLigaFemeninaCalendario.setRowNum(row.getRecordNumber());
		    	campeonatoLigaFemeninaCalendario = CampeonatoLigaFemeninaTransformer2021.transformerObjectCalendarioCsv(campeonatoLigaFemeninaCalendario, row);
		    	result.put(campeonatoLigaFemeninaCalendario.getId(), campeonatoLigaFemeninaCalendario);
		    }
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public CampeonatoLigaFemeninaClasificacion2021 readClasificacion(Integer id) {
		HashMap<Integer, CampeonatoLigaFemeninaClasificacion2021> result = (HashMap<Integer, CampeonatoLigaFemeninaClasificacion2021>) readAllClasificacion();
		return result.get(id);
	}
	
	@SuppressWarnings("unchecked")
	public CampeonatoLigaFemeninaCalendario2021 readCalendario(Integer id) {
		HashMap<Integer, CampeonatoLigaFemeninaCalendario2021> result = (HashMap<Integer, CampeonatoLigaFemeninaCalendario2021>) readAllCalendario();
		return result.get(id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object read(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HashMap<Integer, ?> readAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<?> listResult() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public List<?> listResultClasificacion() {
		List<CampeonatoLigaFemeninaClasificacion2021> listResult = new ArrayList<CampeonatoLigaFemeninaClasificacion2021>();
		for (Entry<Integer, ?> entry : readAllClasificacion().entrySet()) {
		    Object value = entry.getValue();
		    CampeonatoLigaFemeninaClasificacion2021 cec = (CampeonatoLigaFemeninaClasificacion2021) value;
		    if(cec.getId() > 0){
		    	if(cec.getJugadoraId() != null){
		    		cec.setJugadora(leerJugadores.read(cec.getJugadoraId()));
		    	}
		    	listResult.add((CampeonatoLigaFemeninaClasificacion2021) cec);
		    }
		}
		
		//Ordenar la clasificacion
		Collections.sort(listResult, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				CampeonatoLigaFemeninaClasificacion2021 cec1 = (CampeonatoLigaFemeninaClasificacion2021) o1;
				CampeonatoLigaFemeninaClasificacion2021 cec2 = (CampeonatoLigaFemeninaClasificacion2021) o2;
				
				int rpuntos = cec1.getPuntos().compareTo(cec2.getPuntos());
				if (rpuntos == 0) {
					Integer part1 = cec1.getTotalTantos();
					Integer part2 = cec2.getTotalTantos();
					int rdifpartidas = part1.compareTo(part2);
					if (rdifpartidas == 0) {
						String nomb1 = cec1.getJugadora().getNombre();
						String nomb2 = cec2.getJugadora().getNombre();
						int rdifnomb = nomb1.compareTo(nomb2);
						return rdifnomb;
					}
					if(rdifpartidas == -1) return 1;
					if(rdifpartidas == 1) return -1;
					return rdifpartidas;
				}
				return rpuntos;
			}
		});
		
		
		
		return listResult;
	}
	
	public List<?> listResultCalendario() {
		List<CampeonatoLigaFemeninaCalendario2021> listResult = new ArrayList<CampeonatoLigaFemeninaCalendario2021>();
		for (Entry<Integer, ?> entry : readAllCalendario().entrySet()) {
		    Object value = entry.getValue();
		    CampeonatoLigaFemeninaCalendario2021 cec = (CampeonatoLigaFemeninaCalendario2021) value;
		    if(cec.getId() > 0){
		    	if(cec.getJugadoraId() != null){
		    		cec.setJugadora(leerJugadores.read(cec.getJugadoraId()));
		    		if(cec.getBoleraId() != null){
		    			cec.setBolera(leerBoleras.read(cec.getBoleraId()));
			    	}
		    	}
		    	listResult.add((CampeonatoLigaFemeninaCalendario2021) cec);
		    }
		}
		
		//Ordenar la clasificacion
		Collections.sort(listResult, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				CampeonatoLigaFemeninaCalendario2021 cec1 = (CampeonatoLigaFemeninaCalendario2021) o1;
				CampeonatoLigaFemeninaCalendario2021 cec2 = (CampeonatoLigaFemeninaCalendario2021) o2;
				
				int rjornada = cec1.getJornada().compareTo(cec2.getJornada());
				if (rjornada == 0) {
					Integer punt1 = cec1.getPuntos();
					Integer punt2 = cec2.getPuntos();
					int puntDif = punt1.compareTo(punt2);
					if (puntDif == 0) {
						String nomb1 = cec1.getJugadora().getNombre();
						String nomb2 = cec2.getJugadora().getNombre();
						int rdifnomb = nomb1.compareTo(nomb2);
						return rdifnomb;
					}
					return puntDif;
				}
				return rjornada;
			}
		});
		
		return listResult;
	}

}
