package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas2021;
import com.bolocelta.bbdd.constants.Ubicaciones2021;
import com.bolocelta.entities.CampeonatoJuvenilesIndividualClasificacion2021;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseCF2021;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseFF2021;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseSF2021;
import com.bolocelta.transformer.CalendarioIndividualFaseCFTransformer2021;
import com.bolocelta.transformer.CalendarioIndividualFaseFFTransformer2021;
import com.bolocelta.transformer.CalendarioIndividualFaseSFTransformer2021;
import com.bolocelta.transformer.CampeonatoJuvenilesIndividualTransformer2021;

public class LeerCampeonatoJuvenilesIndividual2021 extends ALeer2021 {

	private LeerJugadores2021 leerJugadores = new LeerJugadores2021();
	private LeerBoleras2021 leerBoleras = new LeerBoleras2021();
	
	HashMap<String, CalendarioFaseCF2021> resultCalendarioFaseCF = null;
	HashMap<String, CalendarioFaseSF2021> resultCalendarioFaseSF = null;
	HashMap<String, CalendarioFaseFF2021> resultCalendarioFaseFF = null;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<Integer, ?> readAllClasificacion() {
		HashMap<Integer, CampeonatoJuvenilesIndividualClasificacion2021> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CLASIFICACION, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES);
		if(csvParser != null){
			for (CSVRecord row : csvParser) {
				CampeonatoJuvenilesIndividualClasificacion2021 campeonatoJuvenilesIndividualClasificacion2021 = new CampeonatoJuvenilesIndividualClasificacion2021();
				campeonatoJuvenilesIndividualClasificacion2021.setRowNum(row.getRecordNumber());
				campeonatoJuvenilesIndividualClasificacion2021 = CampeonatoJuvenilesIndividualTransformer2021.transformerObjectClasificacionCsv(campeonatoJuvenilesIndividualClasificacion2021, row);
				campeonatoJuvenilesIndividualClasificacion2021.setJugador(leerJugadores.read(campeonatoJuvenilesIndividualClasificacion2021.getJugadorId(), true));
		    	result.put(campeonatoJuvenilesIndividualClasificacion2021.getId(), campeonatoJuvenilesIndividualClasificacion2021);
		    }
		}
		return result;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<String, CalendarioFaseCF2021> readCalendarioFaseCF() {
		resultCalendarioFaseCF = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_CF, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	CalendarioFaseCF2021 calendarioFaseCF = new CalendarioFaseCF2021();
		    	calendarioFaseCF.setRowNum(row.getRecordNumber());
		    	calendarioFaseCF = CalendarioIndividualFaseCFTransformer2021.transformerObjectCsv(calendarioFaseCF, row);
		    	resultCalendarioFaseCF.put(calendarioFaseCF.getIdCruce(), calendarioFaseCF);
		    }
		}
		return resultCalendarioFaseCF;
	}
	
	@SuppressWarnings("unchecked")
	public CalendarioFaseCF2021 readCalendarioFaseCF(Integer id) {
		if(this.resultCalendarioFaseCF == null){
			resultCalendarioFaseCF = (HashMap<String, CalendarioFaseCF2021>) readCalendarioFaseCF();
		}
		if(this.resultCalendarioFaseCF != null){
			return this.resultCalendarioFaseCF.get(id);
		}
		return null;
	}
	
	public List<CalendarioFaseCF2021> listResultCalendarioFaseCF() {
		List<CalendarioFaseCF2021> listResult = new ArrayList<CalendarioFaseCF2021>();
		for (Entry<String, CalendarioFaseCF2021> entry : readCalendarioFaseCF().entrySet()) {
		    Object value = entry.getValue();
		    CalendarioFaseCF2021 calendarioFaseCF = (CalendarioFaseCF2021) value;
		    if(calendarioFaseCF.getId() > 0){
		    	calendarioFaseCF.setJugador1(leerJugadores.read(calendarioFaseCF.getJugador1Id(), true));
		    	calendarioFaseCF.setJugador2(leerJugadores.read(calendarioFaseCF.getJugador2Id(), true));
		    	calendarioFaseCF.setBolera(leerBoleras.read(calendarioFaseCF.getBoleraId()));
		    	//Calcular icono victoria, empate y derrota
		    	Integer resultadoPartida1 =  calendarioFaseCF.getJuegosJugador1P1() - calendarioFaseCF.getJuegosJugador2P1();
		    	Integer resultadoPartida2 =  calendarioFaseCF.getJuegosJugador1P2() - calendarioFaseCF.getJuegosJugador2P2();
		    	Integer resultadoPartida3 =  calendarioFaseCF.getJuegosJugador1P3() - calendarioFaseCF.getJuegosJugador2P3();
		    	
		    	//Sin Jugar
		    	if(resultadoPartida1 == 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
	    			calendarioFaseCF.setRequipo1("/resources/sinjugar.ico");
	    			calendarioFaseCF.setRequipo2("/resources/sinjugar.ico");
		    	}else
		    	//Ganador 1 solo una partida
		    	if(resultadoPartida1 > 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
		    		calendarioFaseCF.setRequipo1("/resources/ganador.ico");
	    			calendarioFaseCF.setRequipo2("/resources/derrota.ico");
		    	}else 
		    	//Ganador 2 solo una partida
		    	if(resultadoPartida1 < 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
		    		calendarioFaseCF.setRequipo1("/resources/derrota.ico");
	    			calendarioFaseCF.setRequipo2("/resources/ganador.ico");
		    	}else 
		    	//Ganador 1 dos partidas
		    	if( (resultadoPartida1 > 0 && resultadoPartida2 > 0) ||
	    			(resultadoPartida1 > 0 && resultadoPartida3 > 0) ||
	    			(resultadoPartida2 > 0 && resultadoPartida3 > 0)){
		    		calendarioFaseCF.setRequipo1("/resources/ganador.ico");
	    			calendarioFaseCF.setRequipo2("/resources/derrota.ico");
		    	}else
		    	//Ganador 2 dos partidas
		    	if( (resultadoPartida1 < 0 && resultadoPartida2 < 0) ||
	    			(resultadoPartida1 < 0 && resultadoPartida3 < 0) ||
	    			(resultadoPartida2 < 0 && resultadoPartida3 < 0)){
		    		calendarioFaseCF.setRequipo1("/resources/derrota.ico");
	    			calendarioFaseCF.setRequipo2("/resources/ganador.ico");
		    	}else{
		    		calendarioFaseCF.setRequipo1("/resources/sinjugar.ico");
		    		calendarioFaseCF.setRequipo2("/resources/sinjugar.ico");
		    	}

		    	listResult.add(calendarioFaseCF);
		    }
		}
		return listResult;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<String, CalendarioFaseSF2021> readCalendarioFaseSF() {
		resultCalendarioFaseSF = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_SF, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	CalendarioFaseSF2021 calendarioFaseSF = new CalendarioFaseSF2021();
		    	calendarioFaseSF.setRowNum(row.getRecordNumber());
		    	calendarioFaseSF = CalendarioIndividualFaseSFTransformer2021.transformerObjectCsv(calendarioFaseSF, row);
		    	resultCalendarioFaseSF.put(calendarioFaseSF.getIdCruce(), calendarioFaseSF);
		    }
		}
		return resultCalendarioFaseSF;
	}
	
	@SuppressWarnings("unchecked")
	public CalendarioFaseSF2021 readCalendarioFaseSF(Integer id) {
		if(this.resultCalendarioFaseSF == null){
			resultCalendarioFaseSF = (HashMap<String, CalendarioFaseSF2021>) readCalendarioFaseSF();
		}
		if(this.resultCalendarioFaseSF != null){
			return this.resultCalendarioFaseSF.get(id);
		}
		return null;
	}
	
	public List<CalendarioFaseSF2021> listResultCalendarioFaseSF() {
		List<CalendarioFaseSF2021> listResult = new ArrayList<CalendarioFaseSF2021>();
		for (Entry<String, CalendarioFaseSF2021> entry : readCalendarioFaseSF().entrySet()) {
		    Object value = entry.getValue();
		    CalendarioFaseSF2021 calendarioFaseSF = (CalendarioFaseSF2021) value;
		    if(calendarioFaseSF.getId() > 0){
		    	calendarioFaseSF.setJugador1(leerJugadores.read(calendarioFaseSF.getJugador1Id(), true));
		    	calendarioFaseSF.setJugador2(leerJugadores.read(calendarioFaseSF.getJugador2Id(), true));
		    	calendarioFaseSF.setBolera(leerBoleras.read(calendarioFaseSF.getBoleraId()));
		    	//Calcular icono victoria, empate y derrota
		    	Integer resultadoPartida1 =  calendarioFaseSF.getJuegosJugador1P1() - calendarioFaseSF.getJuegosJugador2P1();
		    	Integer resultadoPartida2 =  calendarioFaseSF.getJuegosJugador1P2() - calendarioFaseSF.getJuegosJugador2P2();
		    	Integer resultadoPartida3 =  calendarioFaseSF.getJuegosJugador1P3() - calendarioFaseSF.getJuegosJugador2P3();
		    	
		    	//Sin Jugar
		    	if(resultadoPartida1 == 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
	    			calendarioFaseSF.setRequipo1("/resources/sinjugar.ico");
	    			calendarioFaseSF.setRequipo2("/resources/sinjugar.ico");
		    	}else
		    	//Ganador 1 solo una partida
		    	if(resultadoPartida1 > 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
		    		calendarioFaseSF.setRequipo1("/resources/ganador.ico");
	    			calendarioFaseSF.setRequipo2("/resources/derrota.ico");
		    	}else 
		    	//Ganador 2 solo una partida
		    	if(resultadoPartida1 < 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
		    		calendarioFaseSF.setRequipo1("/resources/derrota.ico");
	    			calendarioFaseSF.setRequipo2("/resources/ganador.ico");
		    	}else 
		    	//Ganador 1 dos partidas
		    	if( (resultadoPartida1 > 0 && resultadoPartida2 > 0) ||
	    			(resultadoPartida1 > 0 && resultadoPartida3 > 0) ||
	    			(resultadoPartida2 > 0 && resultadoPartida3 > 0)){
		    		calendarioFaseSF.setRequipo1("/resources/ganador.ico");
	    			calendarioFaseSF.setRequipo2("/resources/derrota.ico");
		    	}else
		    	//Ganador 2 dos partidas
		    	if( (resultadoPartida1 < 0 && resultadoPartida2 < 0) ||
	    			(resultadoPartida1 < 0 && resultadoPartida3 < 0) ||
	    			(resultadoPartida2 < 0 && resultadoPartida3 < 0)){
		    		calendarioFaseSF.setRequipo1("/resources/derrota.ico");
	    			calendarioFaseSF.setRequipo2("/resources/ganador.ico");
		    	}else{
		    		calendarioFaseSF.setRequipo1("/resources/sinjugar.ico");
		    		calendarioFaseSF.setRequipo2("/resources/sinjugar.ico");
		    	}

		    	listResult.add(calendarioFaseSF);
		    }
		}
		return listResult;
	}
	
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<String, CalendarioFaseFF2021> readCalendarioFaseFF() {
		resultCalendarioFaseFF = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_FF, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	CalendarioFaseFF2021 calendarioFaseFF = new CalendarioFaseFF2021();
		    	calendarioFaseFF.setRowNum(row.getRecordNumber());
		    	calendarioFaseFF = CalendarioIndividualFaseFFTransformer2021.transformerObjectCsv(calendarioFaseFF, row);
		    	resultCalendarioFaseFF.put(calendarioFaseFF.getIdCruce(), calendarioFaseFF);
		    }
		}
		return resultCalendarioFaseFF;
	}
	
	@SuppressWarnings("unchecked")
	public CalendarioFaseFF2021 readCalendarioFaseFF(Integer id) {
		if(this.resultCalendarioFaseFF == null){
			resultCalendarioFaseFF = (HashMap<String, CalendarioFaseFF2021>) readCalendarioFaseFF();
		}
		if(this.resultCalendarioFaseFF != null){
			return this.resultCalendarioFaseFF.get(id);
		}
		return null;
	}
	
	public List<CalendarioFaseFF2021> listResultCalendarioFaseFF() {
		List<CalendarioFaseFF2021> listResult = new ArrayList<CalendarioFaseFF2021>();
		for (Entry<String, CalendarioFaseFF2021> entry : readCalendarioFaseFF().entrySet()) {
		    Object value = entry.getValue();
		    CalendarioFaseFF2021 calendarioFaseFF = (CalendarioFaseFF2021) value;
		    if(calendarioFaseFF.getId() > 0){
		    	calendarioFaseFF.setJugador1(leerJugadores.read(calendarioFaseFF.getJugador1Id(), true));
		    	calendarioFaseFF.setJugador2(leerJugadores.read(calendarioFaseFF.getJugador2Id(), true));
		    	calendarioFaseFF.setBolera(leerBoleras.read(calendarioFaseFF.getBoleraId()));
		    	//Calcular icono victoria, empate y derrota
		    	Integer resultadoPartida1 =  calendarioFaseFF.getJuegosJugador1P1() - calendarioFaseFF.getJuegosJugador2P1();
		    	Integer resultadoPartida2 =  calendarioFaseFF.getJuegosJugador1P2() - calendarioFaseFF.getJuegosJugador2P2();
		    	Integer resultadoPartida3 =  calendarioFaseFF.getJuegosJugador1P3() - calendarioFaseFF.getJuegosJugador2P3();
		    	
		    	//Sin Jugar
		    	if(resultadoPartida1 == 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
	    			calendarioFaseFF.setRequipo1("/resources/sinjugar.ico");
	    			calendarioFaseFF.setRequipo2("/resources/sinjugar.ico");
		    	}else
		    	//Ganador 1 solo una partida
		    	if(resultadoPartida1 > 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
		    		calendarioFaseFF.setRequipo1("/resources/ganador.ico");
	    			calendarioFaseFF.setRequipo2("/resources/derrota.ico");
		    	}else 
		    	//Ganador 2 solo una partida
		    	if(resultadoPartida1 < 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
		    		calendarioFaseFF.setRequipo1("/resources/derrota.ico");
	    			calendarioFaseFF.setRequipo2("/resources/ganador.ico");
		    	}else 
		    	//Ganador 1 dos partidas
		    	if( (resultadoPartida1 > 0 && resultadoPartida2 > 0) ||
	    			(resultadoPartida1 > 0 && resultadoPartida3 > 0) ||
	    			(resultadoPartida2 > 0 && resultadoPartida3 > 0)){
		    		calendarioFaseFF.setRequipo1("/resources/ganador.ico");
	    			calendarioFaseFF.setRequipo2("/resources/derrota.ico");
		    	}else
		    	//Ganador 2 dos partidas
		    	if( (resultadoPartida1 < 0 && resultadoPartida2 < 0) ||
	    			(resultadoPartida1 < 0 && resultadoPartida3 < 0) ||
	    			(resultadoPartida2 < 0 && resultadoPartida3 < 0)){
		    		calendarioFaseFF.setRequipo1("/resources/derrota.ico");
	    			calendarioFaseFF.setRequipo2("/resources/ganador.ico");
		    	}else{
		    		calendarioFaseFF.setRequipo1("/resources/sinjugar.ico");
		    		calendarioFaseFF.setRequipo2("/resources/sinjugar.ico");
		    	}

		    	listResult.add(calendarioFaseFF);
		    }
		}
		return listResult;
	}
	
	
	
	
	@SuppressWarnings("unchecked")
	public CampeonatoJuvenilesIndividualClasificacion2021 readClasificacion(Integer id) {
		HashMap<Integer, CampeonatoJuvenilesIndividualClasificacion2021> result = (HashMap<Integer, CampeonatoJuvenilesIndividualClasificacion2021>) readAllClasificacion();
		return result.get(id);
	}
	
	@SuppressWarnings("unchecked")
	public CalendarioFaseSF2021 readCalendarioSemifinales(Integer id) {
		HashMap<String, CalendarioFaseSF2021> result = (HashMap<String, CalendarioFaseSF2021>) readCalendarioFaseSF();
		return result.get(id);
	}
	
	@SuppressWarnings("unchecked")
	public CalendarioFaseFF2021 readCalendarioFinal(Integer id) {
		HashMap<String, CalendarioFaseFF2021> result = (HashMap<String, CalendarioFaseFF2021>) readCalendarioFaseFF();
		return result.get(id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object read(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HashMap<Integer, ?> readAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<?> listResult() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public List<?> listResultClasificacion() {
		List<CampeonatoJuvenilesIndividualClasificacion2021> listResult = new ArrayList<CampeonatoJuvenilesIndividualClasificacion2021>();
		for (Entry<Integer, ?> entry : readAllClasificacion().entrySet()) {
		    Object value = entry.getValue();
		    CampeonatoJuvenilesIndividualClasificacion2021 cec = (CampeonatoJuvenilesIndividualClasificacion2021) value;
		    if(cec.getId() > 0){
		    	if(cec.getJugadorId() != null){
		    		cec.setJugador(leerJugadores.read(cec.getJugadorId()));
		    	}
		    	listResult.add((CampeonatoJuvenilesIndividualClasificacion2021) cec);
		    }
		}
		
		//Ordenar la clasificacion
		Collections.sort(listResult, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				CampeonatoJuvenilesIndividualClasificacion2021 cec1 = (CampeonatoJuvenilesIndividualClasificacion2021) o1;
				CampeonatoJuvenilesIndividualClasificacion2021 cec2 = (CampeonatoJuvenilesIndividualClasificacion2021) o2;
				
				Integer part1 = cec1.getTotalPuntosFinal();
				Integer part2 = cec2.getTotalPuntosFinal();
				int rdifpartidas = part1.compareTo(part2);
				if(rdifpartidas == -1) return 1;
				if(rdifpartidas == 1) return -1;
				return rdifpartidas;
				
			}
		});
		
		
		
		return listResult;
	}

}
