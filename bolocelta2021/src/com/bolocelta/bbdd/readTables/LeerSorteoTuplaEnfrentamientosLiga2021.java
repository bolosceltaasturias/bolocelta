package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas2021;
import com.bolocelta.bbdd.constants.Ubicaciones2021;
import com.bolocelta.entities.sorteos.liga.SorteoTuplaEnfrentamientosLiga2021;
import com.bolocelta.transformer.SorteoTuplaEnfrentamientosLigaTransformer2021;

public class LeerSorteoTuplaEnfrentamientosLiga2021 extends ALeer2021 {

	private final String nameFile = NombresTablas2021.N_CSV_BBDD_SORTEO_LIGA_CALENDARIO_CRUCES;
	private final String path = Ubicaciones2021.UBICACION_BBDD_ADMINISTRACION;
	
	HashMap<Integer, SorteoTuplaEnfrentamientosLiga2021> result = null;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, SorteoTuplaEnfrentamientosLiga2021> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(nameFile, path);
		if(csvParser != null){    
			for (CSVRecord row : csvParser) {
		    	SorteoTuplaEnfrentamientosLiga2021 sorteoTuplaEnfrentamientosLiga = new SorteoTuplaEnfrentamientosLiga2021();
	    		sorteoTuplaEnfrentamientosLiga = SorteoTuplaEnfrentamientosLigaTransformer2021.transformerObjectCsv(sorteoTuplaEnfrentamientosLiga, row);
		    	result.put(sorteoTuplaEnfrentamientosLiga.getId(), sorteoTuplaEnfrentamientosLiga);
		    }
		}
		return result;
	}
	
	public Long getLastRowSheet(){
		return lastRowSheet(nameFile, path);
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public SorteoTuplaEnfrentamientosLiga2021 read(Integer id) {
		SorteoTuplaEnfrentamientosLiga2021 sorteoTuplaEnfrentamientosLiga = null;
		if(sorteoTuplaEnfrentamientosLiga == null){
			if(this.result == null){
				this.result = (HashMap<Integer, SorteoTuplaEnfrentamientosLiga2021>) readAll();
			}
			sorteoTuplaEnfrentamientosLiga = result.get(id);
		}
		return sorteoTuplaEnfrentamientosLiga;
	}
	
	public void refreshList(){
		this.result = null;
	}
	
	@SuppressWarnings("unchecked")
	public List<SorteoTuplaEnfrentamientosLiga2021> readJornadaIda(Integer id) {
		List<SorteoTuplaEnfrentamientosLiga2021> sorteoTuplaEnfrentamientosLigaByCategoria = new ArrayList<>();
		List<SorteoTuplaEnfrentamientosLiga2021> allSorteoTuplaEnfrentamientosLiga = (List<SorteoTuplaEnfrentamientosLiga2021>) listResult();
		for (SorteoTuplaEnfrentamientosLiga2021 stel : allSorteoTuplaEnfrentamientosLiga) {
			if(stel.getJornadaIda() != null && stel.getJornadaIda().equals(id)){
				sorteoTuplaEnfrentamientosLigaByCategoria.add(stel);
			}
		}
		return sorteoTuplaEnfrentamientosLigaByCategoria;
	}

	@Override
	public List<?> listResult() {
		List<SorteoTuplaEnfrentamientosLiga2021> listResult = new ArrayList<SorteoTuplaEnfrentamientosLiga2021>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    SorteoTuplaEnfrentamientosLiga2021 sorteoTuplaEnfrentamientosLiga = (SorteoTuplaEnfrentamientosLiga2021) value;
		    if(sorteoTuplaEnfrentamientosLiga.getId() != null && sorteoTuplaEnfrentamientosLiga.getId() > 0){
		    	listResult.add((SorteoTuplaEnfrentamientosLiga2021) sorteoTuplaEnfrentamientosLiga);
		    }
		}
		return listResult;
	}

}
