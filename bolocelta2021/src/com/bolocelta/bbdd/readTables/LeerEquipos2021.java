package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.Activo2021;
import com.bolocelta.bbdd.constants.Categorias2021;
import com.bolocelta.bbdd.constants.NombresTablas2021;
import com.bolocelta.bbdd.constants.Ubicaciones2021;
import com.bolocelta.entities.Equipos2021;
import com.bolocelta.entities.Jugadores2021;
import com.bolocelta.transformer.EquiposTransformer2021;

public class LeerEquipos2021 extends ALeer2021 {
	
	private LeerCategorias2021 leerCategorias = new LeerCategorias2021();
	private LeerBoleras2021 leerBoleras = new LeerBoleras2021();
	private LeerJugadores2021 leerJugadores = new LeerJugadores2021();
	
	HashMap<Integer, Equipos2021> result = null;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, Equipos2021> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2021.N_CSV_BBDD_EQUIPOS, Ubicaciones2021.UBICACION_BBDD_ADMINISTRACION);
		if(csvParser != null){    
			for (CSVRecord row : csvParser) {
	    		Equipos2021 equipo = new Equipos2021();
	    		equipo.setRowNum(row.getRecordNumber());
	    		equipo = EquiposTransformer2021.transformerObjectCsv(equipo, row);
		    	result.put(equipo.getId(), equipo);
		    }
		}
	    return result;
	}
	
	public Object read(Integer id, boolean readAditional){
		Equipos2021 equipo = null;
		if(equipo == null){
			if(this.result == null){
				this.result = (HashMap<Integer, Equipos2021>) readAll();
			}
			equipo = result.get(id);
			if(equipo != null && equipo.getId() > 0){
		    	if(equipo.getCategoriaId() != null){
		    		equipo.setCategoria(leerCategorias.read(equipo.getCategoriaId()));
		    	}
		    	if(equipo.getBoleraId() != null){
		    		equipo.setBolera(leerBoleras.read(equipo.getBoleraId()));
		    	}
		    	if(readAditional){
		    		equipo.setJugadoresList(leerJugadores.readEquipo(id));
		    		equipo.setJugadoresMap(new HashMap<>());
		    		for (Jugadores2021 jugador : equipo.getJugadoresList()) {
						equipo.getJugadoresMap().put(jugador.getId(), jugador);
					}
		    	}
		    	
		    }
		}
		return equipo;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Equipos2021 read(Integer id) {
		
		boolean readAditional = false;
		
		Equipos2021 equipo = null;
		if(equipo == null){
			if(this.result == null){
				this.result = (HashMap<Integer, Equipos2021>) readAll();
			}
			equipo = result.get(id);
			if(equipo.getId() > 0){
		    	if(equipo.getCategoriaId() != null){
		    		equipo.setCategoria(leerCategorias.read(equipo.getCategoriaId()));
		    	}
		    	if(equipo.getBoleraId() != null){
		    		equipo.setBolera(leerBoleras.read(equipo.getBoleraId()));
		    	}
		    	if(readAditional){
		    		equipo.setJugadoresList(leerJugadores.readEquipo(id));
		    	}
		    	
		    }
		}
		return equipo;
	}

	@Override
	public List<?> listResult() {
		List<Equipos2021> listResult = new ArrayList<Equipos2021>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    Equipos2021 equipo = (Equipos2021) value;
		    if(equipo.getId() > 0){
		    	if(equipo.getCategoriaId() != null){
		    		equipo.setCategoria(leerCategorias.read(equipo.getCategoriaId()));
		    	}
		    	if(equipo.getBoleraId() != null){
		    		equipo.setBolera(leerBoleras.read(equipo.getBoleraId()));
		    	}
		    	listResult.add((Equipos2021) equipo);
		    }
		}
		
		//Ordenar la clasificacion
		Collections.sort(listResult, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				Equipos2021 pi1 = (Equipos2021) o1;
				Equipos2021 pi2 = (Equipos2021) o2;
				
				int requipo = pi1.getCategoriaId().compareTo(pi2.getCategoriaId());
				if(requipo == 0){
					requipo = pi1.getNombre().compareTo(pi2.getNombre());
				}
				return requipo;
			}
		});
		
		return listResult;
	}
	
	public List<?> listResultVotaciones() {
		List<Equipos2021> listResult = new ArrayList<Equipos2021>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    Equipos2021 equipo = (Equipos2021) value;
		    System.out.println("EQUIPO: " + equipo.getNombre());
		    if(equipo.getId() > 0){
		    	if(equipo.getCategoriaId() != null){
		    		equipo.setCategoria(leerCategorias.read(equipo.getCategoriaId()));
		    	}
		    	if(equipo.getBoleraId() != null){
		    		equipo.setBolera(leerBoleras.read(equipo.getBoleraId()));
		    	}
		    	if(equipo.getCategoriaId() == Categorias2021.PRIMERA || equipo.getCategoriaId() == Categorias2021.SEGUNDA || equipo.getCategoriaId() == Categorias2021.TERCERA){
		    		if(equipo.getBoleraId() != 0){
		    			listResult.add((Equipos2021) equipo);
		    		}
		    	}
		    }
		}
		
		//Ordenar la clasificacion
		Collections.sort(listResult, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				Equipos2021 pi1 = (Equipos2021) o1;
				Equipos2021 pi2 = (Equipos2021) o2;
				
				int requipo = pi1.getCategoriaId().compareTo(pi2.getCategoriaId());
				if(requipo == 0){
					requipo = pi1.getNombre().compareTo(pi2.getNombre());
				}
				return requipo;
			}
		});
		
		return listResult;
	}
	
	public List<?> listResultLiga() {
		List<Equipos2021> listResult = new ArrayList<Equipos2021>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    Equipos2021 equipo = (Equipos2021) value;
		    if(equipo.getId() > 0){
		    	if(equipo.getLiga().equalsIgnoreCase(Activo2021.SI)){
			    	if(equipo.getCategoriaId() != null){
			    		equipo.setCategoria(leerCategorias.read(equipo.getCategoriaId()));
			    	}
			    	if(equipo.getBoleraId() != null){
			    		equipo.setBolera(leerBoleras.read(equipo.getBoleraId()));
			    	}
			    	listResult.add((Equipos2021) equipo);
			    }
		    }
		}
		return listResult;
	}

}
