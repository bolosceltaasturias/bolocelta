package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas2021;
import com.bolocelta.bbdd.constants.Ubicaciones2021;
import com.bolocelta.entities.Modalidades2021;
import com.bolocelta.transformer.ModalidadesTransformer2021;

public class LeerModalidades2021 extends ALeer2021 {
	
	HashMap<Integer, Modalidades2021> result = null;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, Modalidades2021> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2021.N_CSV_BBDD_MODALIDADES, Ubicaciones2021.UBICACION_BBDD_ADMINISTRACION);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
	    		Modalidades2021 modalidad = new Modalidades2021();
	    		modalidad = ModalidadesTransformer2021.transformerObjectCsv(modalidad, row);
		    	result.put(modalidad.getId(), modalidad);
		    }
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Modalidades2021 read(Integer id) {
		if(this.result == null){
			this.result = (HashMap<Integer, Modalidades2021>) readAll();
		}
		return result.get(id);
	}

	@Override
	public List<?> listResult() {
		List<Modalidades2021> listResult = new ArrayList<Modalidades2021>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    Modalidades2021 modalidad = (Modalidades2021) value;
		    if(modalidad.getId() > 0){
		    	listResult.add((Modalidades2021) modalidad);
		    }
		}
		return listResult;
	}

}
