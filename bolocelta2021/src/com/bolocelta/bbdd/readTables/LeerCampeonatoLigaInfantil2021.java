package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas2021;
import com.bolocelta.bbdd.constants.Ubicaciones2021;
import com.bolocelta.entities.CampeonatoLigaFemeninaCalendario2021;
import com.bolocelta.entities.CampeonatoLigaInfantilCalendario2021;
import com.bolocelta.entities.CampeonatoLigaInfantilClasificacion2021;
import com.bolocelta.transformer.CampeonatoLigaInfantilTransformer2021;

public class LeerCampeonatoLigaInfantil2021 extends ALeer2021 {

	private LeerJugadores2021 leerJugadores = new LeerJugadores2021();
	private LeerBoleras2021 leerBoleras = new LeerBoleras2021();
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<Integer, ?> readAllCalendario() {
		HashMap<Integer, CampeonatoLigaInfantilCalendario2021> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_CAL_LIGA_INFANTIL, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_EQUIPOS);
		if(csvParser != null){
			for (CSVRecord row : csvParser) {
				CampeonatoLigaInfantilCalendario2021 campeonatoLigaInfantilCalendario2021 = new CampeonatoLigaInfantilCalendario2021();
		    	campeonatoLigaInfantilCalendario2021.setRowNum(row.getRecordNumber());
		    	campeonatoLigaInfantilCalendario2021 = CampeonatoLigaInfantilTransformer2021.transformerObjectCalendarioCsv(campeonatoLigaInfantilCalendario2021, row);
		    	result.put(campeonatoLigaInfantilCalendario2021.getId(), campeonatoLigaInfantilCalendario2021);
		    }
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public CampeonatoLigaFemeninaCalendario2021 readCalendario(Integer id) {
		HashMap<Integer, CampeonatoLigaFemeninaCalendario2021> result = (HashMap<Integer, CampeonatoLigaFemeninaCalendario2021>) readAllCalendario();
		return result.get(id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object read(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HashMap<Integer, ?> readAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<?> listResult() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public List<?> listResultClasificacion(String categoria) {
		HashMap<String, String> mapJugadores = new HashMap<>();
		for (Entry<Integer, ?> entry : readAllCalendario().entrySet()) {
		    Object value = entry.getValue();
		    CampeonatoLigaInfantilCalendario2021 cec = (CampeonatoLigaInfantilCalendario2021) value;
		    if(categoria.equalsIgnoreCase(cec.getCategoria())){
			    if(!mapJugadores.containsKey(cec.getNombre())){
			    	mapJugadores.put(cec.getNombre(), cec.getNombre());
			    }
		    }
		}
		
		List<CampeonatoLigaInfantilClasificacion2021> listResult = new ArrayList<CampeonatoLigaInfantilClasificacion2021>();
		
		for (Entry<String, ?> entry : mapJugadores.entrySet()) {
		    Object value = entry.getValue();
		    String nombre = (String) value;
			
			
			CampeonatoLigaInfantilClasificacion2021 clic = new CampeonatoLigaInfantilClasificacion2021();
			
			for (Entry<Integer, ?> entry2 : readAllCalendario().entrySet()) {
			    Object valueList = entry2.getValue();
			    CampeonatoLigaInfantilCalendario2021 cec = (CampeonatoLigaInfantilCalendario2021) valueList;
			    if(nombre.equals(cec.getNombre())){
			    	clic.setNombre(cec.getNombre());
			    	clic.setCategoria(cec.getCategoria());
			    	clic.setPuntos(clic.getPuntos() + cec.getPuntos());
			    	clic.setTantos(clic.getTantos() + cec.getTotalTantos());
			    	if(cec.isContarFinal()){
				    	clic.setPuntosFinal(clic.getPuntosFinal() + cec.getPuntos());
				    	clic.setTantosFinal(clic.getTantosFinal() + cec.getTotalTantos());			    		
			    	}
			    }
			}
			
			listResult.add(clic);
		}

		//Ordenar la clasificacion
		Collections.sort(listResult, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				CampeonatoLigaInfantilClasificacion2021 cec1 = (CampeonatoLigaInfantilClasificacion2021) o1;
				CampeonatoLigaInfantilClasificacion2021 cec2 = (CampeonatoLigaInfantilClasificacion2021) o2;
				
				int rpuntos = cec1.getPuntosFinal().compareTo(cec2.getPuntosFinal());
				if (rpuntos == 0) {
					Integer part1 = cec1.getTantosFinal();
					Integer part2 = cec2.getTantosFinal();
					int rdifpartidas = part1.compareTo(part2);
					if (rdifpartidas == 0) {
						String nomb1 = cec1.getNombre();
						String nomb2 = cec2.getNombre();
						int rdifnomb = nomb1.compareTo(nomb2);
						return rdifnomb;
					}
					if(rdifpartidas == -1) return 1;
					if(rdifpartidas == 1) return -1;
					return rdifpartidas;
				}
				return rpuntos;
			}
		});
		
		
		
		return listResult;
	}
	
	public List<?> listResultCalendario() {
		List<CampeonatoLigaInfantilCalendario2021> listResult = new ArrayList<CampeonatoLigaInfantilCalendario2021>();
		for (Entry<Integer, ?> entry : readAllCalendario().entrySet()) {
		    Object value = entry.getValue();
		    CampeonatoLigaInfantilCalendario2021 cec = (CampeonatoLigaInfantilCalendario2021) value;
		    if(cec.getId() > 0){
	    		if(cec.getBoleraId() != null){
	    			cec.setBolera(leerBoleras.read(cec.getBoleraId()));
		    	}
	    		listResult.add((CampeonatoLigaInfantilCalendario2021) cec);
		    }
		}
		
		if(listResult != null && !listResult.isEmpty()){
		
			//Ordenar la clasificacion
			Collections.sort(listResult, new Comparator() {
				@Override
				public int compare(Object o1, Object o2) {
					CampeonatoLigaInfantilCalendario2021 cec1 = (CampeonatoLigaInfantilCalendario2021) o1;
					CampeonatoLigaInfantilCalendario2021 cec2 = (CampeonatoLigaInfantilCalendario2021) o2;
					
					int rjornada = cec1.getJornada().compareTo(cec2.getJornada());
					if (rjornada == 0) {

						String cat1 = cec1.getCategoria();
						String cat2 = cec2.getCategoria();
						
						int rdifcat = cat1.compareTo(cat2);
						if(rdifcat == 0){
							Integer punt1 = cec1.getPuntos();
							Integer punt2 = cec2.getPuntos();
							int puntDif = punt1.compareTo(punt2);
							if (puntDif == 0) {
								Integer tant1 = cec1.getTotalTantos();
								Integer tant2 = cec2.getTotalTantos();
								int tantDif = punt1.compareTo(punt2);
								if (puntDif == 0) {
									String nomb1 = cec1.getNombre();
									String nomb2 = cec2.getNombre();
									int rdifnomb = nomb1.compareTo(nomb2);
									return rdifnomb;
								}
								return tantDif;
							}
							return puntDif;
						}
						return rdifcat;
					}
					return rjornada;
					
				}
			});
		
		}
		
		return listResult;
	}

}
