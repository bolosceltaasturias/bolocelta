package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas2021;
import com.bolocelta.bbdd.constants.Ubicaciones2021;
import com.bolocelta.entities.CalendarioEspana2021;
import com.bolocelta.entities.CampeonatoEquiposClasificacion2021;
import com.bolocelta.transformer.CalendarioEspanaTransformer2021;

public class LeerCalendarioEspana2021 extends ALeer2021 {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, CalendarioEspana2021> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CALENDARIO_ESP, Ubicaciones2021.UBICACION_BBDD_ADMINISTRACION);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
	    		CalendarioEspana2021 calendarioEspana = new CalendarioEspana2021();
	    		calendarioEspana = CalendarioEspanaTransformer2021.transformerObjectCsv(calendarioEspana, row);
		    	result.put(calendarioEspana.getId(), calendarioEspana);
		    }
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public CalendarioEspana2021 read(Integer id) {
		HashMap<Integer, CalendarioEspana2021> result = (HashMap<Integer, CalendarioEspana2021>) readAll();
		return result.get(id);
	}

	@Override
	public List<?> listResult() {
		List<CalendarioEspana2021> listResult = new ArrayList<CalendarioEspana2021>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    CalendarioEspana2021 calendarioEspana = (CalendarioEspana2021) value;
		    if(calendarioEspana.getId() > 0){
		    	listResult.add((CalendarioEspana2021) calendarioEspana);
		    }
		}
		
		
		
		//Ordenar el calendario
		Collections.sort(listResult, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				CalendarioEspana2021 cec1 = (CalendarioEspana2021) o1;
				CalendarioEspana2021 cec2 = (CalendarioEspana2021) o2;
				
				int comp1 = cec1.getFechaDesde().compareTo(cec2.getFechaDesde());
				if (comp1 == 0) {
					int comp2 = cec1.getFechaHasta().compareTo(cec2.getFechaHasta());
					if (comp2 == 0) {
						return cec1.getId().compareTo(cec2.getId());
					}
					return comp2;
				}
				return comp1;
			}
		});
		
		
		
		
		return listResult;
	}

}
