package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.inject.Named;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas2021;
import com.bolocelta.bbdd.constants.Ubicaciones2021;
import com.bolocelta.entities.Noticias2021;
import com.bolocelta.transformer.NoticiasTransformer2021;

@Named
public class LeerNoticias2021 extends ALeer2021 {
	
	HashMap<Integer, Noticias2021> result = null;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, Noticias2021> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2021.N_CSV_BBDD_NOTICIAS, Ubicaciones2021.UBICACION_BBDD_NOTICIAS);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	Noticias2021 noticias = new Noticias2021();
		    	noticias = NoticiasTransformer2021.transformerObjectCsv(noticias, row);
		    	result.put(noticias.getId(), noticias);
		    }
		}
	    return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Noticias2021 read(Integer id) {
		if(this.result == null){
			result = (HashMap<Integer, Noticias2021>) readAll();
		}
		return this.result.get(id);
	}
	
	@Override
	public List<?> listResult() {
		List<Noticias2021> listResult = new ArrayList<Noticias2021>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    Noticias2021 noticias = (Noticias2021) value;
		    if(noticias.getId() > 0){
		    	listResult.add(noticias);
		    }
		}
		return listResult;
	}

}
