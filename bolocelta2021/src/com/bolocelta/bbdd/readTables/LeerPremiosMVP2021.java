package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas2021;
import com.bolocelta.bbdd.constants.Ubicaciones2021;
import com.bolocelta.entities.CampeonatoLigaFemeninaCalendario2021;
import com.bolocelta.entities.CampeonatoLigaInfantilCalendario2021;
import com.bolocelta.entities.CampeonatoLigaInfantilClasificacion2021;
import com.bolocelta.entities.Categorias2021;
import com.bolocelta.entities.PremiosMVP2021;
import com.bolocelta.transformer.PremiosMVPTransformer2021;

public class LeerPremiosMVP2021 extends ALeer2021 {

	private LeerJugadores2021 leerJugadores = new LeerJugadores2021();
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<Integer, ?> readAllPremios() {
		HashMap<Integer, PremiosMVP2021> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2021.N_CSV_BBDD_PREMIOS_MVP, Ubicaciones2021.UBICACION_BBDD_PREMIOS);
		if(csvParser != null){
			for (CSVRecord row : csvParser) {
				PremiosMVP2021 premiosMVP2021 = new PremiosMVP2021();
		    	premiosMVP2021.setRowNum(row.getRecordNumber());
		    	premiosMVP2021 = PremiosMVPTransformer2021.transformerObjectCalendarioCsv(premiosMVP2021, row);
		    	result.put(premiosMVP2021.getId(), premiosMVP2021);
		    }
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public PremiosMVP2021 readPremio(Integer id) {
		HashMap<Integer, PremiosMVP2021> result = (HashMap<Integer, PremiosMVP2021>) readAllPremios();
		return result.get(id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object read(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HashMap<Integer, ?> readAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<?> listResult() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public List<?> listResultPremios(Integer categoria) {
		
		List<PremiosMVP2021> listResult = new ArrayList<PremiosMVP2021>();
		
		for (Entry<Integer, ?> entry : readAllPremios().entrySet()) {
		    Object value = entry.getValue();
		    PremiosMVP2021 cec = (PremiosMVP2021) value;
		    if(categoria == cec.getCategoria()){
		    	cec.setJugador(leerJugadores.read(cec.getIdJugador(), true));
		    	listResult.add(cec);
		    }
		}

		return listResult;
	}

}
