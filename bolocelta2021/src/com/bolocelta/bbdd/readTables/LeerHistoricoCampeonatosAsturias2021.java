package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.inject.Named;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.application.enumerations.ModalidadJuegoEnumeration2021;
import com.bolocelta.bbdd.constants.NombresTablas2021;
import com.bolocelta.bbdd.constants.Ubicaciones2021;
import com.bolocelta.entities.HistoricoCampeonatosAsturias2021;
import com.bolocelta.transformer.HistoricoCampeonatoAsturiasTransformer2021;

@Named
public class LeerHistoricoCampeonatosAsturias2021 extends ALeer2021 {

	HashMap<Integer, HistoricoCampeonatosAsturias2021> result = null;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, HistoricoCampeonatosAsturias2021> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2021.N_CSV_BBDD_HISTORICO_ASTURIAS, Ubicaciones2021.UBICACION_BBDD_HISTORICO);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	HistoricoCampeonatosAsturias2021 historicoCA = new HistoricoCampeonatosAsturias2021();
	    		historicoCA = HistoricoCampeonatoAsturiasTransformer2021.transformerObjectCsv(historicoCA, row);
		    	result.put(historicoCA.getId(), historicoCA);
		    }
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public HistoricoCampeonatosAsturias2021 read(Integer id) {
		if(this.result == null){
			this.result = (HashMap<Integer, HistoricoCampeonatosAsturias2021>) readAll();
		}
		return result.get(id);
	}

	@Override
	public List<?> listResult() {
		List<HistoricoCampeonatosAsturias2021> listResult = new ArrayList<HistoricoCampeonatosAsturias2021>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    listResult.add((HistoricoCampeonatosAsturias2021) value);
		}
		return listResult;
	}
	
	public List<?> listResultEquipos() {
		List<HistoricoCampeonatosAsturias2021> listResult = new ArrayList<HistoricoCampeonatosAsturias2021>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    HistoricoCampeonatosAsturias2021 historicoCA = (HistoricoCampeonatosAsturias2021) value;
		    if(historicoCA.getTipo() == ModalidadJuegoEnumeration2021.MODALIDAD_EQUIPOS){
		    	listResult.add((HistoricoCampeonatosAsturias2021) value);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultIndividual() {
		List<HistoricoCampeonatosAsturias2021> listResult = new ArrayList<HistoricoCampeonatosAsturias2021>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    HistoricoCampeonatosAsturias2021 historicoCA = (HistoricoCampeonatosAsturias2021) value;
		    if(historicoCA.getTipo() == ModalidadJuegoEnumeration2021.MODALIDAD_INDIVIDUAL){
		    	listResult.add((HistoricoCampeonatosAsturias2021) value);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultParejas() {
		List<HistoricoCampeonatosAsturias2021> listResult = new ArrayList<HistoricoCampeonatosAsturias2021>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    HistoricoCampeonatosAsturias2021 historicoCA = (HistoricoCampeonatosAsturias2021) value;
		    if(historicoCA.getTipo() == ModalidadJuegoEnumeration2021.MODALIDAD_PAREJAS){
		    	listResult.add((HistoricoCampeonatosAsturias2021) value);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultIndividualFemenino() {
		List<HistoricoCampeonatosAsturias2021> listResult = new ArrayList<HistoricoCampeonatosAsturias2021>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    HistoricoCampeonatosAsturias2021 historicoCA = (HistoricoCampeonatosAsturias2021) value;
		    if(historicoCA.getTipo() == ModalidadJuegoEnumeration2021.MODALIDAD_INDIVIDUAL_FEMENINO){
		    	listResult.add((HistoricoCampeonatosAsturias2021) value);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultParejasFemenino() {
		List<HistoricoCampeonatosAsturias2021> listResult = new ArrayList<HistoricoCampeonatosAsturias2021>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    HistoricoCampeonatosAsturias2021 historicoCA = (HistoricoCampeonatosAsturias2021) value;
		    if(historicoCA.getTipo() == ModalidadJuegoEnumeration2021.MODALIDAD_PAREJAS_FEMENINO){
		    	listResult.add((HistoricoCampeonatosAsturias2021) value);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultMixto() {
		List<HistoricoCampeonatosAsturias2021> listResult = new ArrayList<HistoricoCampeonatosAsturias2021>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    HistoricoCampeonatosAsturias2021 historicoCA = (HistoricoCampeonatosAsturias2021) value;
		    if(historicoCA.getTipo() == ModalidadJuegoEnumeration2021.MODALIDAD_MIXTO){
		    	listResult.add((HistoricoCampeonatosAsturias2021) value);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultVeteranos() {
		List<HistoricoCampeonatosAsturias2021> listResult = new ArrayList<HistoricoCampeonatosAsturias2021>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    HistoricoCampeonatosAsturias2021 historicoCA = (HistoricoCampeonatosAsturias2021) value;
		    if(historicoCA.getTipo() == ModalidadJuegoEnumeration2021.MODALIDAD_VETERANOS){
		    	listResult.add((HistoricoCampeonatosAsturias2021) value);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultJuveniles() {
		List<HistoricoCampeonatosAsturias2021> listResult = new ArrayList<HistoricoCampeonatosAsturias2021>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    HistoricoCampeonatosAsturias2021 historicoCA = (HistoricoCampeonatosAsturias2021) value;
		    if(historicoCA.getTipo() == ModalidadJuegoEnumeration2021.MODALIDAD_JUVENILES){
		    	listResult.add((HistoricoCampeonatosAsturias2021) value);
		    }
		}
		return listResult;
	}


}
