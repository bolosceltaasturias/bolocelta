package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas2021;
import com.bolocelta.bbdd.constants.Ubicaciones2021;
import com.bolocelta.entities.CampeonatoEquiposCalendario2021;
import com.bolocelta.entities.CampeonatoEquiposClasificacion2021;
import com.bolocelta.transformer.CampeonatoEquiposTransformer2021;

public class LeerCampeonatoEquiposTercera2021 extends ALeer2021 {

	private LeerEquipos2021 leerEquipos = new LeerEquipos2021();
	private LeerCategorias2021 leerCategorias = new LeerCategorias2021();
	private LeerBoleras2021 leerBoleras = new LeerBoleras2021();
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<Integer, ?> readAllClasificacion() {
		HashMap<Integer, CampeonatoEquiposClasificacion2021> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CLA_TERCERA, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_EQUIPOS);
		if(csvParser != null){
			for (CSVRecord row : csvParser) {
		    	CampeonatoEquiposClasificacion2021 campeonatoEquiposClasificacion = new CampeonatoEquiposClasificacion2021();
		    	campeonatoEquiposClasificacion.setRowNum(row.getRecordNumber());
		    	campeonatoEquiposClasificacion = CampeonatoEquiposTransformer2021.transformerObjectClasificacionCsv(campeonatoEquiposClasificacion, row);
		    	result.put(campeonatoEquiposClasificacion.getId(), campeonatoEquiposClasificacion);
		    }
		}
		return result;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<Integer, ?> readAllCalendario() {
		HashMap<Integer, CampeonatoEquiposCalendario2021> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2021.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CAL_TERCERA, Ubicaciones2021.UBICACION_BBDD_CAMPEONATO_EQUIPOS);
		if(csvParser != null){
			for (CSVRecord row : csvParser) {
		    	CampeonatoEquiposCalendario2021 campeonatoEquiposCalendario = new CampeonatoEquiposCalendario2021();
		    	campeonatoEquiposCalendario.setRowNum(row.getRecordNumber());
		    	campeonatoEquiposCalendario = CampeonatoEquiposTransformer2021.transformerObjectCalendarioCsv(campeonatoEquiposCalendario, row);
		    	result.put(campeonatoEquiposCalendario.getId(), campeonatoEquiposCalendario);
		    }
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public CampeonatoEquiposClasificacion2021 readClasificacion(Integer id) {
		HashMap<Integer, CampeonatoEquiposClasificacion2021> result = (HashMap<Integer, CampeonatoEquiposClasificacion2021>) readAllClasificacion();
		return result.get(id);
	}
	
	@SuppressWarnings("unchecked")
	public CampeonatoEquiposCalendario2021 readCalendario(Integer id) {
		HashMap<Integer, CampeonatoEquiposCalendario2021> result = (HashMap<Integer, CampeonatoEquiposCalendario2021>) readAllCalendario();
		return result.get(id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object read(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HashMap<Integer, ?> readAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<?> listResult() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public List<?> listResultClasificacion() {
		List<CampeonatoEquiposClasificacion2021> listResult = new ArrayList<CampeonatoEquiposClasificacion2021>();
		for (Entry<Integer, ?> entry : readAllClasificacion().entrySet()) {
		    Object value = entry.getValue();
		    CampeonatoEquiposClasificacion2021 cec = (CampeonatoEquiposClasificacion2021) value;
		    if(cec.getId() > 0){
		    	if(cec.getEquipoId() != null){
		    		cec.setEquipo(leerEquipos.read(cec.getEquipoId()));
		    	}
		    	if(cec.getCategoriaId() != null){
		    		cec.setCategoria(leerCategorias.read(cec.getCategoriaId()));
		    	}
		    	listResult.add((CampeonatoEquiposClasificacion2021) cec);
		    }
		}
		
		//Ordenar la clasificacion
		Collections.sort(listResult, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				CampeonatoEquiposClasificacion2021 cec1 = (CampeonatoEquiposClasificacion2021) o1;
				CampeonatoEquiposClasificacion2021 cec2 = (CampeonatoEquiposClasificacion2021) o2;
				
				int rpuntos = cec1.getPuntos().compareTo(cec2.getPuntos());
				if (rpuntos == 0) {
					Integer part1 = cec1.getPartidasFavor() - cec1.getPartidasContra();
					Integer part2 = cec2.getPartidasFavor() - cec2.getPartidasContra();
					int rdifpartidas = part1.compareTo(part2);
					if (rdifpartidas == 0) {
						return cec1.getPartidasFavor().compareTo(cec2.getPartidasFavor());
					}
					return rdifpartidas;
				}
				return rpuntos;
			}
		});
		
		Collections.reverse(listResult);
		
		return listResult;
	}
	
	public List<?> listResultCalendario() {
		List<CampeonatoEquiposCalendario2021> listResult = new ArrayList<CampeonatoEquiposCalendario2021>();
		for (Entry<Integer, ?> entry : readAllCalendario().entrySet()) {
		    Object value = entry.getValue();
		    CampeonatoEquiposCalendario2021 cec = (CampeonatoEquiposCalendario2021) value;
		    if(cec.getId() > 0){
		    	if(cec.getEquipo1Id() != null){
		    		cec.setEquipo1(leerEquipos.read(cec.getEquipo1Id()));
		    		if(cec.getEquipo1().getBoleraId() != null){
		    			cec.getEquipo1().setBolera(leerBoleras.read(cec.getEquipo1().getBoleraId()));
			    	}
		    	}
		    	if(cec.getEquipo2Id() != null){
		    		cec.setEquipo2(leerEquipos.read(cec.getEquipo2Id()));
		    		if(cec.getEquipo2().getBoleraId() != null){
		    			cec.getEquipo2().setBolera(leerBoleras.read(cec.getEquipo2().getBoleraId()));
			    	}
		    	}
		    	if(cec.getCategoriaId() != null){
		    		cec.setCategoria(leerCategorias.read(cec.getCategoriaId()));
		    	}
		    	if(cec.getPgequipo1() != null && cec.getPgequipo2() != null){
		    		if(cec.getPgequipo1() == 0 && cec.getPgequipo2() == 0){
		    			cec.setRequipo1("/resources/sinjugar.ico");
		    			cec.setRequipo2("/resources/sinjugar.ico");
		    		}else if(cec.getPgequipo1() > cec.getPgequipo2()){
		    			cec.setRequipo1("/resources/ganador.ico");
		    			cec.setRequipo2("/resources/derrota.ico");
		    		}else if(cec.getPgequipo1() < cec.getPgequipo2()){
		    			cec.setRequipo1("/resources/derrota.ico");
		    			cec.setRequipo2("/resources/ganador.ico");
		    		}else if(cec.getPgequipo1().equals(cec.getPgequipo2())){
		    			cec.setRequipo1("/resources/empate.ico");
		    			cec.setRequipo2("/resources/empate.ico");
		    		}
		    		
		    	}else{
		    		cec.setRequipo1("/resources/sinjugar.ico");
	    			cec.setRequipo2("/resources/sinjugar.ico");
		    	}
		    	listResult.add((CampeonatoEquiposCalendario2021) cec);
		    }
		}
		
		//Ordenar la clasificacion
		Collections.sort(listResult, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				CampeonatoEquiposCalendario2021 cec1 = (CampeonatoEquiposCalendario2021) o1;
				CampeonatoEquiposCalendario2021 cec2 = (CampeonatoEquiposCalendario2021) o2;
				
				int rjornada = cec1.getJornada().compareTo(cec2.getJornada());
				return rjornada;
			}
		});
		
		return listResult;
	}

}
