package com.bolocelta.bbdd.readTables;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Row;

public interface ILeer2021 {
	
	public HashMap<Integer, ?> readAll();
	
	public Object read(Integer id);
	
	public Iterator<Row> readWorkBook(String nameFile, String path, String nameSheet);
	
	public Object convertRowToObject(Row row);
	
	public List<?> listResult();

}
