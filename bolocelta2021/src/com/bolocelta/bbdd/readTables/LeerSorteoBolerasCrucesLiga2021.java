package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas2021;
import com.bolocelta.bbdd.constants.Ubicaciones2021;
import com.bolocelta.entities.sorteos.liga.SorteoBolerasCrucesLiga2021;
import com.bolocelta.transformer.SorteoBolerasCrucesLigaTransformer2021;

public class LeerSorteoBolerasCrucesLiga2021 extends ALeer2021 {

	private final String nameFile = NombresTablas2021.N_CSV_BBDD_SORTEO_LIGA_CALENDARIO_BOLERAS_CRUCE;
	private final String path = Ubicaciones2021.UBICACION_BBDD_ADMINISTRACION;
	
	HashMap<Integer, SorteoBolerasCrucesLiga2021> result = null;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, SorteoBolerasCrucesLiga2021> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(nameFile, path);
		if(csvParser != null){
			for (CSVRecord row : csvParser) {
		    	SorteoBolerasCrucesLiga2021 sorteoBolerasCrucesLiga = new SorteoBolerasCrucesLiga2021();
	    		sorteoBolerasCrucesLiga = SorteoBolerasCrucesLigaTransformer2021.transformerObjectCsv(sorteoBolerasCrucesLiga, row);
		    	result.put(sorteoBolerasCrucesLiga.getId(), sorteoBolerasCrucesLiga);
		    }
		}
		return result;
	}
	
	public Long getLastRowSheet(){
		return lastRowSheet(nameFile, path);
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public SorteoBolerasCrucesLiga2021 read(Integer id) {
		SorteoBolerasCrucesLiga2021 sorteoBolerasCrucesLiga = null;
		if(sorteoBolerasCrucesLiga == null){
			if(this.result == null){
				this.result = (HashMap<Integer, SorteoBolerasCrucesLiga2021>) readAll();
			}
			sorteoBolerasCrucesLiga = result.get(id);
		}
		return sorteoBolerasCrucesLiga;
	}
	
	public void refreshList(){
		this.result = null;
	}
	
	@SuppressWarnings("unchecked")
	public List<SorteoBolerasCrucesLiga2021> readCategoria(Integer id) {
		List<SorteoBolerasCrucesLiga2021> sorteoBolerasCrucesLigaByCategoria = new ArrayList<>();
		List<SorteoBolerasCrucesLiga2021> allSorteoBolerasCrucesLiga = (List<SorteoBolerasCrucesLiga2021>) listResult();
		for (SorteoBolerasCrucesLiga2021 sbcl : allSorteoBolerasCrucesLiga) {
			if(sbcl.getCategoria() != null && sbcl.getCategoria().equals(id)){
				sorteoBolerasCrucesLigaByCategoria.add(sbcl);
			}
		}
		return sorteoBolerasCrucesLigaByCategoria;
	}

	@Override
	public List<?> listResult() {
		List<SorteoBolerasCrucesLiga2021> listResult = new ArrayList<SorteoBolerasCrucesLiga2021>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    SorteoBolerasCrucesLiga2021 sorteoBolerasCrucesLiga = (SorteoBolerasCrucesLiga2021) value;
		    if(sorteoBolerasCrucesLiga.getId() != null && sorteoBolerasCrucesLiga.getId() > 0){
		    	listResult.add((SorteoBolerasCrucesLiga2021) sorteoBolerasCrucesLiga);
		    }
		}
		return listResult;
	}

}
