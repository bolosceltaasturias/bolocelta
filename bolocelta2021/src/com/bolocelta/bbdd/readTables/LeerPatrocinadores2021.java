package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.inject.Named;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas2021;
import com.bolocelta.bbdd.constants.Ubicaciones2021;
import com.bolocelta.entities.Patrocinadores2021;
import com.bolocelta.transformer.PatrocinadoresTransformer2021;

@Named
public class LeerPatrocinadores2021 extends ALeer2021 {
	
	HashMap<Integer, Patrocinadores2021> result = null;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, Patrocinadores2021> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2021.N_CSV_BBDD_PATROCINADORES, Ubicaciones2021.UBICACION_BBDD_ADMINISTRACION);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	Patrocinadores2021 patrocinador = new Patrocinadores2021();
		    	patrocinador = PatrocinadoresTransformer2021.transformerObjectCsv(patrocinador, row);
		    	result.put(patrocinador.getId(), patrocinador);
		    }
		}
	    return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Patrocinadores2021 read(Integer id) {
		if(this.result == null){
			result = (HashMap<Integer, Patrocinadores2021>) readAll();
		}
		return this.result.get(id);
	}
	
	@Override
	public List<?> listResult() {
		List<Patrocinadores2021> listResult = new ArrayList<Patrocinadores2021>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    Patrocinadores2021 patrocinador = (Patrocinadores2021) value;
		    if(patrocinador.getId() > 0){
		    	listResult.add(patrocinador);
		    }
		}
		return listResult;
	}

}
