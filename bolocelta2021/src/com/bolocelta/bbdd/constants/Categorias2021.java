package com.bolocelta.bbdd.constants;

public class Categorias2021 {
	
	public static final Integer PRIMERA = 1;
	public static final Integer SEGUNDA = 2;
	public static final Integer TERCERA = 3;
	
	public static final Integer FEMENINO = 4;
	public static final Integer MIXTO = 8;
	
	public static final Integer VETERANOS = 11;
	public static final Integer JUVENILES = 12;

}
