package com.bolocelta.bbdd.constants;

public class GruposLetra2021 {
	
	public static String FASE_I_G_A = "A";
	public static String FASE_I_G_B = "B";
	public static String FASE_I_G_C = "C";
	public static String FASE_I_G_D = "D";
	public static String FASE_I_G_E = "E";
	public static String FASE_I_G_F = "F";
	public static String FASE_I_G_G = "G";
	public static String FASE_I_G_H = "H";
	
	public static String FASE_II_G_I = "I";
	public static String FASE_II_G_J = "J";
	public static String FASE_II_G_K = "K";
	public static String FASE_II_G_L = "L";
	public static String FASE_II_G_M = "M";
	public static String FASE_II_G_N = "N";
	public static String FASE_II_G_O = "O";
	public static String FASE_II_G_P = "P";
	
	public static String getGruposLetraFaseI(Integer numeroGrupo){
		if(numeroGrupo == 1){
			return FASE_I_G_A;
		}else if(numeroGrupo == 2){
			return FASE_I_G_B;
		}else if(numeroGrupo == 3){
			return FASE_I_G_C;
		}else if(numeroGrupo == 4){
			return FASE_I_G_D;
		}else if(numeroGrupo == 5){
			return FASE_I_G_E;
		}else if(numeroGrupo == 6){
			return FASE_I_G_F;
		}else if(numeroGrupo == 7){
			return FASE_I_G_G;
		}else if(numeroGrupo == 8){
			return FASE_I_G_H;
		}
		return null;
	}
	
	public static String getGruposLetraFaseII(Integer numeroGrupo){
		if(numeroGrupo == 1){
			return FASE_II_G_I;
		}else if(numeroGrupo == 2){
			return FASE_II_G_J;
		}else if(numeroGrupo == 3){
			return FASE_II_G_K;
		}else if(numeroGrupo == 4){
			return FASE_II_G_L;
		}else if(numeroGrupo == 5){
			return FASE_II_G_M;
		}else if(numeroGrupo == 6){
			return FASE_II_G_N;
		}else if(numeroGrupo == 7){
			return FASE_II_G_O;
		}else if(numeroGrupo == 8){
			return FASE_II_G_P;
		}
		return null;
	}
	
	public static Integer FASE_I_II_G_2 = 2;
	public static Integer FASE_I_II_G_4 = 4;
	public static Integer FASE_I_II_G_5 = 5;
	public static Integer FASE_I_II_G_6 = 6;
	public static Integer FASE_I_II_G_7 = 7;
	public static Integer FASE_I_II_G_8 = 8;
	
	public static Integer FASE_OF = 8;
	public static Integer FASE_CF = 4;
	public static Integer FASE_SF = 2;
	public static Integer FASE_FC = 1;
	public static Integer FASE_FF = 1;
	

}
