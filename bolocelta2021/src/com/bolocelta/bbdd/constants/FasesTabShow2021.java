package com.bolocelta.bbdd.constants;

public class FasesTabShow2021 {
	
	private Integer fase;
	private String grupo;
	private String nombre;
	private boolean faseGrupo;
	
	public FasesTabShow2021(Integer fase, String grupo, String nombre, boolean faseGrupo) {
		this.fase = fase;
		this.grupo = grupo;
		this.nombre = nombre;
		this.faseGrupo = faseGrupo;
	}

	public static FasesTabShow2021 FASE_I_G_A = new FasesTabShow2021(FasesModelo2021.FASE_I, GruposLetra2021.FASE_I_G_A, "Fase I: Grupo A", true);
	public static FasesTabShow2021 FASE_I_G_B = new FasesTabShow2021(FasesModelo2021.FASE_I, GruposLetra2021.FASE_I_G_B, "Fase I: Grupo B", true);
	public static FasesTabShow2021 FASE_I_G_C = new FasesTabShow2021(FasesModelo2021.FASE_I, GruposLetra2021.FASE_I_G_C, "Fase I: Grupo C", true);
	public static FasesTabShow2021 FASE_I_G_D = new FasesTabShow2021(FasesModelo2021.FASE_I, GruposLetra2021.FASE_I_G_D, "Fase I: Grupo D", true);
	public static FasesTabShow2021 FASE_I_G_E = new FasesTabShow2021(FasesModelo2021.FASE_I, GruposLetra2021.FASE_I_G_E, "Fase I: Grupo E", true);
	public static FasesTabShow2021 FASE_I_G_F = new FasesTabShow2021(FasesModelo2021.FASE_I, GruposLetra2021.FASE_I_G_F, "Fase I: Grupo F", true);
	public static FasesTabShow2021 FASE_I_G_G = new FasesTabShow2021(FasesModelo2021.FASE_I, GruposLetra2021.FASE_I_G_G, "Fase I: Grupo G", true);
	public static FasesTabShow2021 FASE_I_G_H = new FasesTabShow2021(FasesModelo2021.FASE_I, GruposLetra2021.FASE_I_G_H, "Fase I: Grupo H", true);
	
	public static FasesTabShow2021 FASE_II_G_I = new FasesTabShow2021(FasesModelo2021.FASE_II, GruposLetra2021.FASE_II_G_I, "Fase II: Grupo I", true);
	public static FasesTabShow2021 FASE_II_G_J = new FasesTabShow2021(FasesModelo2021.FASE_II, GruposLetra2021.FASE_II_G_J, "Fase II: Grupo J", true);
	public static FasesTabShow2021 FASE_II_G_K = new FasesTabShow2021(FasesModelo2021.FASE_II, GruposLetra2021.FASE_II_G_K, "Fase II: Grupo K", true);
	public static FasesTabShow2021 FASE_II_G_L = new FasesTabShow2021(FasesModelo2021.FASE_II, GruposLetra2021.FASE_II_G_L, "Fase II: Grupo L", true);
	public static FasesTabShow2021 FASE_II_G_M = new FasesTabShow2021(FasesModelo2021.FASE_II, GruposLetra2021.FASE_II_G_M, "Fase II: Grupo M", true);
	public static FasesTabShow2021 FASE_II_G_N = new FasesTabShow2021(FasesModelo2021.FASE_II, GruposLetra2021.FASE_II_G_N, "Fase II: Grupo N", true);
	public static FasesTabShow2021 FASE_II_G_O = new FasesTabShow2021(FasesModelo2021.FASE_II, GruposLetra2021.FASE_II_G_O, "Fase II: Grupo O", true);
	public static FasesTabShow2021 FASE_II_G_P = new FasesTabShow2021(FasesModelo2021.FASE_II, GruposLetra2021.FASE_II_G_P, "Fase II: Grupo P", true);
	
	public static FasesTabShow2021 FASE_OF = new FasesTabShow2021(FasesModelo2021.FASE_OF, "OF", "Octavos Final", false);
	public static FasesTabShow2021 FASE_CF = new FasesTabShow2021(FasesModelo2021.FASE_CF, "CF", "Cuartos Final", false);
	public static FasesTabShow2021 FASE_SF = new FasesTabShow2021(FasesModelo2021.FASE_SF, "SF", "SemiFinal", false);
	public static FasesTabShow2021 FASE_FC = new FasesTabShow2021(FasesModelo2021.FASE_FC, "FC", "Final Consolacion", false);
	public static FasesTabShow2021 FASE_FF = new FasesTabShow2021(FasesModelo2021.FASE_FF, "FC", "Final", false);
	
	public static FasesTabShow2021 FASE_GRAPHIC = new FasesTabShow2021(FasesModelo2021.FASE_GRAPHIC, "GR", "Cuadro", false);
	
	public static FasesTabShow2021 searchFaseTabShow(Integer numeroFase, String grupo){
		if(numeroFase == FasesModelo2021.FASE_I){
			if(grupo.equals(GruposLetra2021.FASE_I_G_A)){
				return FASE_I_G_A;
			}else if(grupo.equals(GruposLetra2021.FASE_I_G_B)){
				return FASE_I_G_B;
			}else if(grupo.equals(GruposLetra2021.FASE_I_G_C)){
				return FASE_I_G_C;
			}else if(grupo.equals(GruposLetra2021.FASE_I_G_D)){
				return FASE_I_G_D;
			}else if(grupo.equals(GruposLetra2021.FASE_I_G_E)){
				return FASE_I_G_E;
			}else if(grupo.equals(GruposLetra2021.FASE_I_G_F)){
				return FASE_I_G_F;
			}else if(grupo.equals(GruposLetra2021.FASE_I_G_G)){
				return FASE_I_G_G;
			}else if(grupo.equals(GruposLetra2021.FASE_I_G_H)){
				return FASE_I_G_H;
			}
		}else if(numeroFase == FasesModelo2021.FASE_II){
			if(grupo.equals(GruposLetra2021.FASE_II_G_I)){
				return FASE_II_G_I;
			}else if(grupo.equals(GruposLetra2021.FASE_II_G_J)){
				return FASE_II_G_J;
			}else if(grupo.equals(GruposLetra2021.FASE_II_G_K)){
				return FASE_II_G_K;
			}else if(grupo.equals(GruposLetra2021.FASE_II_G_L)){
				return FASE_II_G_L;
			}else if(grupo.equals(GruposLetra2021.FASE_II_G_M)){
				return FASE_II_G_M;
			}else if(grupo.equals(GruposLetra2021.FASE_II_G_N)){
				return FASE_II_G_N;
			}else if(grupo.equals(GruposLetra2021.FASE_II_G_O)){
				return FASE_II_G_O;
			}else if(grupo.equals(GruposLetra2021.FASE_II_G_P)){
				return FASE_II_G_P;
			}
		}else if(numeroFase == FasesModelo2021.FASE_OF){
			return FASE_OF;
		}else if(numeroFase == FasesModelo2021.FASE_CF){
			return FASE_CF;
		}else if(numeroFase == FasesModelo2021.FASE_SF){
			return FASE_SF;
		}else if(numeroFase == FasesModelo2021.FASE_FC){
			return FASE_FC;
		}else if(numeroFase == FasesModelo2021.FASE_FF){
			return FASE_FF;
		}
		return null;
	}

	public Integer getFase() {
		return fase;
	}

	public void setFase(Integer fase) {
		this.fase = fase;
	}

	public String getGrupo() {
		return grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public boolean isFaseGrupo() {
		return faseGrupo;
	}

	public void setFaseGrupo(boolean faseGrupo) {
		this.faseGrupo = faseGrupo;
	}
	

}
