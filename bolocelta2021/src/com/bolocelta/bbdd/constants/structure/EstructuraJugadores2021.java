package com.bolocelta.bbdd.constants.structure;

import java.util.ArrayList;
import java.util.List;

import com.bolocelta.bbdd.constants.NombresTablas2021;

public class EstructuraJugadores2021 extends NombresTablas2021{
	
	private List<Estructura2021> list = new ArrayList<Estructura2021>();
	
	
	//Filas
	private final Long F1 = new Long(1);
	
	//Columnas
	private final Integer C1 = 1;
	private final Integer C2 = 2;
	private final Integer C3 = 3;
	private final Integer C4 = 4;
	private final Integer C5 = 5;
	private final Integer C6 = 6;
	private final Integer C7 = 7;
	private final Integer C8 = 8;
	private final Integer C9 = 9;
	private final Integer C10 = 10;
	private final Integer C11 = 11;
	private final Integer C12 = 12;
	private final Integer C13 = 13;
	private final Integer C14 = 14;
	private final Integer C15 = 15;
	private final Integer C16 = 16;
	private final Integer C17 = 17;
	private final Integer C18 = 18;
	private final Integer C19 = 19;
	private final Integer C20 = 20;
	private final Integer C21 = 21;
	private final Integer C22 = 22;
	private final Integer C23 = 23;
	private final Integer C24 = 24;
	private final Integer C25 = 25;
	private final Integer C26 = 26;
	private final Integer C27 = 27;
	private final Integer C28 = 28;
	
	//Cabeceras
	public static final String COL_ID = "ID";
	public static final String COL_EQUIPO = "EQUIPO";
	public static final String COL_NOMBRE = "NOMBRE";
	public static final String COL_APODO = "APODO";
	public static final String COL_NUM_FICHA = "NUM_FICHA";
	public static final String COL_EMAIL = "EMAIL";
	public static final String COL_TELEFONO = "TELEFONO";
	public static final String COL_EDAD = "EDAD";
	public static final String COL_ACTIVO = "ACTIVO";
	public static final String COL_MODALIDAD = "MODALIDAD";
	public static final String COL_INDIV_PRI = "INDIV. PRI";
	public static final String COL_INDIV_SEG = "INDIV. SEG";
	public static final String COL_INDIV_TER = "INDIV. TER";
	public static final String COL_INDIV_FEM = "INDIV. FEM";
	public static final String COL_ACA_J1  = "J1";
	public static final String COL_ACA_J2  = "J2";
	public static final String COL_ACA_J3  = "J3";
	public static final String COL_ACA_J4  = "J4";
	public static final String COL_ACA_J5  = "J5";
	public static final String COL_ACA_J6  = "J6";
	public static final String COL_ACA_J7  = "J7";
	public static final String COL_ACA_J8  = "J8";
	public static final String COL_ACA_J9  = "J9";
	public static final String COL_ACA_J10 = "J10";
	public static final String COL_ACA_J11 = "J11";
	public static final String COL_ACA_J12 = "J12";
	public static final String COL_ACA_J13 = "J13";
	public static final String COL_ACA_J14 = "J14";
	
	//Propiedades
	
	//Estructura
	public EstructuraJugadores2021() {
		//Cabecera
		list.add(new Estructura2021(this.F1, this.C1 , COL_ID, this.N_CSV_BBDD_JUGADORES, this.TD_ENTERO));
		list.add(new Estructura2021(this.F1, this.C2 , COL_EQUIPO, this.N_CSV_BBDD_JUGADORES, this.TD_ENTERO));
		list.add(new Estructura2021(this.F1, this.C3 , COL_NOMBRE, this.N_CSV_BBDD_JUGADORES, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C4 , COL_APODO, this.N_CSV_BBDD_JUGADORES, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C5 , COL_NUM_FICHA, this.N_CSV_BBDD_JUGADORES, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C6 , COL_EMAIL, this.N_CSV_BBDD_JUGADORES, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C7 , COL_TELEFONO, this.N_CSV_BBDD_JUGADORES, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C8 , COL_EDAD, this.N_CSV_BBDD_JUGADORES, this.TD_ENTERO));
		list.add(new Estructura2021(this.F1, this.C9 , COL_ACTIVO, this.N_CSV_BBDD_JUGADORES, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C10 , COL_MODALIDAD, this.N_CSV_BBDD_JUGADORES, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C11 , COL_INDIV_PRI, this.N_CSV_BBDD_JUGADORES, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C12 , COL_INDIV_SEG, this.N_CSV_BBDD_JUGADORES, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C13 , COL_INDIV_TER, this.N_CSV_BBDD_JUGADORES, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C14 , COL_INDIV_FEM, this.N_CSV_BBDD_JUGADORES, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C15 , COL_ACA_J1 , this.N_CSV_BBDD_JUGADORES, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C16 , COL_ACA_J2 , this.N_CSV_BBDD_JUGADORES, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C17 , COL_ACA_J3 , this.N_CSV_BBDD_JUGADORES, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C18 , COL_ACA_J4 , this.N_CSV_BBDD_JUGADORES, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C19 , COL_ACA_J5 , this.N_CSV_BBDD_JUGADORES, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C20 , COL_ACA_J6 , this.N_CSV_BBDD_JUGADORES, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C21 , COL_ACA_J7 , this.N_CSV_BBDD_JUGADORES, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C22 , COL_ACA_J8 , this.N_CSV_BBDD_JUGADORES, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C23 , COL_ACA_J9 , this.N_CSV_BBDD_JUGADORES, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C24 , COL_ACA_J10, this.N_CSV_BBDD_JUGADORES, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C25 , COL_ACA_J11, this.N_CSV_BBDD_JUGADORES, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C26 , COL_ACA_J12, this.N_CSV_BBDD_JUGADORES, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C27 , COL_ACA_J13, this.N_CSV_BBDD_JUGADORES, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C28 , COL_ACA_J14, this.N_CSV_BBDD_JUGADORES, this.TD_TEXTO));
	}
	
	public List<Estructura2021> getList(){
		return this.list;
	}
	

}
