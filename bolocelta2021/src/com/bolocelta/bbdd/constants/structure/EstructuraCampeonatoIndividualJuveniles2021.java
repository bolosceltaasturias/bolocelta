package com.bolocelta.bbdd.constants.structure;

import java.util.ArrayList;
import java.util.List;

import com.bolocelta.bbdd.constants.NombresTablas2021;

public class EstructuraCampeonatoIndividualJuveniles2021 extends NombresTablas2021{
	
	private List<Estructura2021> listClasificacion = new ArrayList<Estructura2021>();
	
	
	//Filas
	private final Long F1 = new Long(0);
	
	//Columnas
	private final Integer C1 = 1;
	private final Integer C2 = 2;
	private final Integer C3 = 3;
	private final Integer C4 = 4;
	private final Integer C5 = 5;
	private final Integer C6 = 6;
	private final Integer C7 = 7;
	private final Integer C8 = 8;
	private final Integer C9 = 9;
	private final Integer C10 = 10;
	private final Integer C11 = 11;
	private final Integer C12 = 12;
	private final Integer C13 = 13;
	private final Integer C14 = 14;
	private final Integer C15 = 15;
	private final Integer C16 = 16;
	private final Integer C17 = 17;
	private final Integer C18 = 18;
	private final Integer C19 = 19;
	private final Integer C20 = 20;
	private final Integer C21 = 21;
	private final Integer C22 = 22;
	private final Integer C23 = 23;
	private final Integer C24 = 24;
	private final Integer C25 = 25;
	private final Integer C26 = 26;
	private final Integer C27 = 27;
	private final Integer C28 = 28;
	private final Integer C29 = 29;
	private final Integer C30 = 30;
	private final Integer C31 = 31;
	private final Integer C32 = 32;
	private final Integer C33 = 33;
	private final Integer C34 = 34;
	private final Integer C35 = 35;
	private final Integer C36 = 36;
	private final Integer C37 = 37;
	private final Integer C38 = 38;
	private final Integer C39 = 39;
	private final Integer C40 = 40;
	private final Integer C41 = 41;
	private final Integer C42 = 42;
	private final Integer C43 = 43;
	private final Integer C44 = 44;
	private final Integer C45 = 45;
	private final Integer C46 = 46;

	//Cabeceras Clasificacion
	public final String COL_CLASIFICACION_ID = "ID";
	public final String COL_CLASIFICACION_FECHA = "FECHA";
	public final String COL_CLASIFICACION_HORA = "HORA";
	public final String COL_CLASIFICACION_BOLERA = "BOLERA";
	public final String COL_CLASIFICACION_ID_JUGADOR = "ID_JUGADOR";
	public final String COL_CLASIFICACION_PUNTOS_TIRADA_1  = "T1";
	public final String COL_CLASIFICACION_PUNTOS_SACADA_1  = "S1";
	public final String COL_CLASIFICACION_PUNTOS_TIRADA_2  = "T2";
	public final String COL_CLASIFICACION_PUNTOS_SACADA_2  = "S2";
	public final String COL_CLASIFICACION_PUNTOS_TIRADA_3  = "T3";
	public final String COL_CLASIFICACION_PUNTOS_SACADA_3  = "S3";
	public final String COL_CLASIFICACION_PUNTOS_TIRADA_4  = "T4";
	public final String COL_CLASIFICACION_PUNTOS_SACADA_4  = "S4";
	public final String COL_CLASIFICACION_PUNTOS_TIRADA_5  = "T5";
	public final String COL_CLASIFICACION_PUNTOS_SACADA_5  = "S5";
	public final String COL_CLASIFICACION_PUNTOS_TIRADA_6  = "T6";
	public final String COL_CLASIFICACION_PUNTOS_SACADA_6  = "S6";
	public final String COL_CLASIFICACION_PUNTOS_TIRADA_7  = "T7";
	public final String COL_CLASIFICACION_PUNTOS_SACADA_7  = "S7";
	public final String COL_CLASIFICACION_PUNTOS_TIRADA_8  = "T8";
	public final String COL_CLASIFICACION_PUNTOS_SACADA_8  = "S8";
	public final String COL_CLASIFICACION_PUNTOS_TIRADA_9  = "T9";
	public final String COL_CLASIFICACION_PUNTOS_SACADA_9  = "S9";
	public final String COL_CLASIFICACION_PUNTOS_TIRADA_10 = "T10";
	public final String COL_CLASIFICACION_PUNTOS_SACADA_10 = "S10";
	public final String COL_CLASIFICACION_PUNTOS_TIRADA_11 = "T11";
	public final String COL_CLASIFICACION_PUNTOS_SACADA_11 = "S11";
	public final String COL_CLASIFICACION_PUNTOS_TIRADA_12 = "T12";
	public final String COL_CLASIFICACION_PUNTOS_SACADA_12 = "S12";
	public final String COL_CLASIFICACION_PUNTOS_TIRADA_13 = "T13";
	public final String COL_CLASIFICACION_PUNTOS_SACADA_13 = "S13";
	public final String COL_CLASIFICACION_PUNTOS_TIRADA_14 = "T14";
	public final String COL_CLASIFICACION_PUNTOS_SACADA_14 = "S14";
	public final String COL_CLASIFICACION_PUNTOS_TIRADA_15 = "T15";
	public final String COL_CLASIFICACION_PUNTOS_SACADA_15 = "S15";
	public final String COL_CLASIFICACION_PUNTOS_TIRADA_16 = "T16";
	public final String COL_CLASIFICACION_PUNTOS_SACADA_16 = "S16";
	public final String COL_CLASIFICACION_PUNTOS_TIRADA_17 = "T17";
	public final String COL_CLASIFICACION_PUNTOS_SACADA_17 = "S17";
	public final String COL_CLASIFICACION_PUNTOS_TIRADA_18 = "T18";
	public final String COL_CLASIFICACION_PUNTOS_SACADA_18 = "S18";
	public final String COL_CLASIFICACION_PUNTOS_TIRADA_19 = "T19";
	public final String COL_CLASIFICACION_PUNTOS_SACADA_19 = "S19";
	public final String COL_CLASIFICACION_PUNTOS_TIRADA_20 = "T20";
	public final String COL_CLASIFICACION_PUNTOS_SACADA_20 = "S20";
	public final String COL_CLASIFICACION_ACTIVO = "ACTIVO";
	
	public EstructuraCampeonatoIndividualJuveniles2021() {
	}
	
	public EstructuraCampeonatoIndividualJuveniles2021(String excelClasificacion) {
		dataClasificacion(excelClasificacion);
	}
	
	private void dataClasificacion(String excelClasificacion) {
		//Cabecera - Clasificacion
		listClasificacion.add(new Estructura2021(this.F1, this.C1, COL_CLASIFICACION_ID, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C2, COL_CLASIFICACION_FECHA, excelClasificacion, this.TD_TEXTO));
		listClasificacion.add(new Estructura2021(this.F1, this.C3, COL_CLASIFICACION_HORA, excelClasificacion, this.TD_TEXTO));
		listClasificacion.add(new Estructura2021(this.F1, this.C4, COL_CLASIFICACION_BOLERA, excelClasificacion, this.TD_TEXTO));
		listClasificacion.add(new Estructura2021(this.F1, this.C5, COL_CLASIFICACION_ID_JUGADOR, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C6, COL_CLASIFICACION_PUNTOS_TIRADA_1, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C7, COL_CLASIFICACION_PUNTOS_SACADA_1, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C8, COL_CLASIFICACION_PUNTOS_TIRADA_2, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C9, COL_CLASIFICACION_PUNTOS_SACADA_2, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C10, COL_CLASIFICACION_PUNTOS_TIRADA_3, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C11, COL_CLASIFICACION_PUNTOS_SACADA_3, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C12, COL_CLASIFICACION_PUNTOS_TIRADA_4, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C13, COL_CLASIFICACION_PUNTOS_SACADA_4, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C14, COL_CLASIFICACION_PUNTOS_TIRADA_5, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C15, COL_CLASIFICACION_PUNTOS_SACADA_5, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C16, COL_CLASIFICACION_PUNTOS_TIRADA_6, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C17, COL_CLASIFICACION_PUNTOS_SACADA_6, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C18, COL_CLASIFICACION_PUNTOS_TIRADA_7, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C19, COL_CLASIFICACION_PUNTOS_SACADA_7, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C20, COL_CLASIFICACION_PUNTOS_TIRADA_8, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C21, COL_CLASIFICACION_PUNTOS_SACADA_8, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C22, COL_CLASIFICACION_PUNTOS_TIRADA_9, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C23, COL_CLASIFICACION_PUNTOS_SACADA_9, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C24, COL_CLASIFICACION_PUNTOS_TIRADA_10, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C25, COL_CLASIFICACION_PUNTOS_SACADA_10, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C26, COL_CLASIFICACION_PUNTOS_TIRADA_11, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C27, COL_CLASIFICACION_PUNTOS_SACADA_11, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C28, COL_CLASIFICACION_PUNTOS_TIRADA_12, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C29, COL_CLASIFICACION_PUNTOS_SACADA_12, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C30, COL_CLASIFICACION_PUNTOS_TIRADA_13, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C31, COL_CLASIFICACION_PUNTOS_SACADA_13, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C32, COL_CLASIFICACION_PUNTOS_TIRADA_14, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C33, COL_CLASIFICACION_PUNTOS_SACADA_14, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C34, COL_CLASIFICACION_PUNTOS_TIRADA_15, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C35, COL_CLASIFICACION_PUNTOS_SACADA_15, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C36, COL_CLASIFICACION_PUNTOS_TIRADA_16, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C37, COL_CLASIFICACION_PUNTOS_SACADA_16, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C38, COL_CLASIFICACION_PUNTOS_TIRADA_17, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C39, COL_CLASIFICACION_PUNTOS_SACADA_17, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C40, COL_CLASIFICACION_PUNTOS_TIRADA_18, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C41, COL_CLASIFICACION_PUNTOS_SACADA_18, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C42, COL_CLASIFICACION_PUNTOS_TIRADA_19, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C43, COL_CLASIFICACION_PUNTOS_SACADA_19, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C44, COL_CLASIFICACION_PUNTOS_TIRADA_20, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C45, COL_CLASIFICACION_PUNTOS_SACADA_20, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2021(this.F1, this.C46, COL_CLASIFICACION_ACTIVO, excelClasificacion, this.TD_ENTERO));		
		
	}
	
	public List<Estructura2021> getClasificacionList(){
		return this.listClasificacion;
	}
	
	
	public String getInsertCabeceraClasificacionRow(){
		return (this.COL_CLASIFICACION_ID + ";" +
				this.COL_CLASIFICACION_FECHA + ";" +
				this.COL_CLASIFICACION_HORA + ";" +
				this.COL_CLASIFICACION_BOLERA + ";" +
				this.COL_CLASIFICACION_ID_JUGADOR + ";" +
				this.COL_CLASIFICACION_PUNTOS_TIRADA_1+ ";" +  
				this.COL_CLASIFICACION_PUNTOS_SACADA_1+ ";" +  
				this.COL_CLASIFICACION_PUNTOS_TIRADA_2+ ";" +  
				this.COL_CLASIFICACION_PUNTOS_SACADA_2+ ";" +  
				this.COL_CLASIFICACION_PUNTOS_TIRADA_3+ ";" +  
				this.COL_CLASIFICACION_PUNTOS_SACADA_3+ ";" +  
				this.COL_CLASIFICACION_PUNTOS_TIRADA_4+ ";" +  
				this.COL_CLASIFICACION_PUNTOS_SACADA_4+ ";" +  
				this.COL_CLASIFICACION_PUNTOS_TIRADA_5+ ";" +  
				this.COL_CLASIFICACION_PUNTOS_SACADA_5+ ";" +  
				this.COL_CLASIFICACION_PUNTOS_TIRADA_6+ ";" +  
				this.COL_CLASIFICACION_PUNTOS_SACADA_6+ ";" +  
				this.COL_CLASIFICACION_PUNTOS_TIRADA_7+ ";" +  
				this.COL_CLASIFICACION_PUNTOS_SACADA_7+ ";" +  
				this.COL_CLASIFICACION_PUNTOS_TIRADA_8+ ";" +  
				this.COL_CLASIFICACION_PUNTOS_SACADA_8+ ";" +  
				this.COL_CLASIFICACION_PUNTOS_TIRADA_9+ ";" +  
				this.COL_CLASIFICACION_PUNTOS_SACADA_9+ ";" +  
				this.COL_CLASIFICACION_PUNTOS_TIRADA_10+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_SACADA_10+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_TIRADA_11+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_SACADA_11+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_TIRADA_12+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_SACADA_12+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_TIRADA_13+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_SACADA_13+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_TIRADA_14+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_SACADA_14+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_TIRADA_15+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_SACADA_15+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_TIRADA_16+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_SACADA_16+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_TIRADA_17+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_SACADA_17+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_TIRADA_18+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_SACADA_18+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_TIRADA_19+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_SACADA_19+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_TIRADA_20+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_SACADA_20+ ";" + 
				this.COL_CLASIFICACION_ACTIVO
				);
	}
	
	//Cabeceras Fases cF - Calendario
	public final String COL_CALENDARIO_FASE_CF_ID = "ID";
	public final String COL_CALENDARIO_FASE_CF_CATEGORIA = "CATEGORIA";
	public final String COL_CALENDARIO_FASE_CF_IDCRUCE = "ID CRUCE";
	public final String COL_CALENDARIO_FASE_CF_NUM_ENFRENTAMIENTO = "NUM_ENFRENTAMIENTO";
	public final String COL_CALENDARIO_FASE_CF_JUGADOR_1 = "JUGADOR 1";
	public final String COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_1_P1 = "JUG. JUGADOR 1 P1";
	public final String COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_1_P2 = "JUG. JUGADOR 1 P2";
	public final String COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_1_P3 = "JUG. JUGADOR 1 P3";
	public final String COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_2_P1 = "JUG. JUGADOR 2 P1";
	public final String COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_2_P2 = "JUG. JUGADOR 2 P2";
	public final String COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_2_P3 = "JUG. JUGADOR 2 P3";
	public final String COL_CALENDARIO_FASE_CF_JUGADOR_2 = "JUGADOR 2";
	public final String COL_CALENDARIO_FASE_CF_ORDEN = "ORDEN";
	public final String COL_CALENDARIO_FASE_CF_CRUCE_CF = "CRUCE_CF";
	public final String COL_CALENDARIO_FASE_CF_ACTIVO = "ACTIVO";
	public final String COL_CALENDARIO_FASE_CF_INICIA = "INICIA";
	public final String COL_CALENDARIO_FASE_CF_FASE_ANTERIOR = "FASE ANTERIOR";
	public final String COL_CALENDARIO_FASE_CF_GRUPO_PROCEDENCIA_JUGADOR_1 = "GR PROC JUG 1";
	public final String COL_CALENDARIO_FASE_CF_GRUPO_PROCEDENCIA_JUGADOR_2 = "GR PROC JUG 2";
	public final String COL_CALENDARIO_FASE_CF_POSICION_PROCEDENCIA_JUGADOR_1 = "POS PROC JUG 1";
	public final String COL_CALENDARIO_FASE_CF_POSICION_PROCEDENCIA_JUGADOR_2 = "POS PROC JUG 2";
	public final String COL_CALENDARIO_FASE_CF_BOLERA = "BOLERA";
	
	
	public String getInsertCabeceraCalendarioFaseCFRow(){
		return (this.COL_CALENDARIO_FASE_CF_ID + ";" +
				this.COL_CALENDARIO_FASE_CF_CATEGORIA + ";" +
				this.COL_CALENDARIO_FASE_CF_IDCRUCE + ";" +
				this.COL_CALENDARIO_FASE_CF_NUM_ENFRENTAMIENTO + ";" +
				this.COL_CALENDARIO_FASE_CF_JUGADOR_1 + ";" +
				this.COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_1_P1 + ";" +
				this.COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_1_P2 + ";" +
				this.COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_1_P3 + ";" +
				this.COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_2_P1 + ";" +
				this.COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_2_P2 + ";" +
				this.COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_2_P3 + ";" +
				this.COL_CALENDARIO_FASE_CF_JUGADOR_2 + ";" +
				this.COL_CALENDARIO_FASE_CF_ORDEN + ";" +
				this.COL_CALENDARIO_FASE_CF_CRUCE_CF + ";" +
				this.COL_CALENDARIO_FASE_CF_ACTIVO + ";" +
				this.COL_CALENDARIO_FASE_CF_INICIA + ";" +
				this.COL_CALENDARIO_FASE_CF_FASE_ANTERIOR + ";" +
				this.COL_CALENDARIO_FASE_CF_GRUPO_PROCEDENCIA_JUGADOR_1 + ";" +
				this.COL_CALENDARIO_FASE_CF_GRUPO_PROCEDENCIA_JUGADOR_2 + ";" +
				this.COL_CALENDARIO_FASE_CF_POSICION_PROCEDENCIA_JUGADOR_1 + ";" +
				this.COL_CALENDARIO_FASE_CF_POSICION_PROCEDENCIA_JUGADOR_2 + ";" +
				this.COL_CALENDARIO_FASE_CF_BOLERA
				);
	}
	
	
	//Cabeceras Fases SF - Calendario
	public final String COL_CALENDARIO_FASE_SF_ID = "ID";
	public final String COL_CALENDARIO_FASE_SF_CATEGORIA = "CATEGORIA";
	public final String COL_CALENDARIO_FASE_SF_IDCRUCE = "ID CRUCE";
	public final String COL_CALENDARIO_FASE_SF_NUM_ENFRENTAMIENTO = "NUM_ENFRENTAMIENTO";
	public final String COL_CALENDARIO_FASE_SF_JUGADOR_1 = "JUGADOR 1";
	public final String COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_1_P1 = "JUG. JUGADOR 1 P1";
	public final String COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_1_P2 = "JUG. JUGADOR 1 P2";
	public final String COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_1_P3 = "JUG. JUGADOR 1 P3";
	public final String COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_2_P1 = "JUG. JUGADOR 2 P1";
	public final String COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_2_P2 = "JUG. JUGADOR 2 P2";
	public final String COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_2_P3 = "JUG. JUGADOR 2 P3";
	public final String COL_CALENDARIO_FASE_SF_JUGADOR_2 = "JUGADOR 2";
	public final String COL_CALENDARIO_FASE_SF_ORDEN = "ORDEN";
	public final String COL_CALENDARIO_FASE_SF_CRUCE_CF = "CRUCE_CF";
	public final String COL_CALENDARIO_FASE_SF_ACTIVO = "ACTIVO";
	public final String COL_CALENDARIO_FASE_SF_INICIA = "INICIA";
	public final String COL_CALENDARIO_FASE_SF_FASE_ANTERIOR = "FASE ANTERIOR";
	public final String COL_CALENDARIO_FASE_SF_GRUPO_PROCEDENCIA_JUGADOR_1 = "GR PROC JUG 1";
	public final String COL_CALENDARIO_FASE_SF_GRUPO_PROCEDENCIA_JUGADOR_2 = "GR PROC JUG 2";
	public final String COL_CALENDARIO_FASE_SF_POSICION_PROCEDENCIA_JUGADOR_1 = "POS PROC JUG 1";
	public final String COL_CALENDARIO_FASE_SF_POSICION_PROCEDENCIA_JUGADOR_2 = "POS PROC JUG 2";
	public final String COL_CALENDARIO_FASE_SF_BOLERA = "BOLERA";
	
	
	public String getInsertCabeceraCalendarioFaseSFRow(){
		return (this.COL_CALENDARIO_FASE_SF_ID + ";" +
				this.COL_CALENDARIO_FASE_SF_CATEGORIA + ";" +
				this.COL_CALENDARIO_FASE_SF_IDCRUCE + ";" +
				this.COL_CALENDARIO_FASE_SF_NUM_ENFRENTAMIENTO + ";" +
				this.COL_CALENDARIO_FASE_SF_JUGADOR_1 + ";" +
				this.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_1_P1 + ";" +
				this.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_1_P2 + ";" +
				this.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_1_P3 + ";" +
				this.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_2_P1 + ";" +
				this.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_2_P2 + ";" +
				this.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_2_P3 + ";" +
				this.COL_CALENDARIO_FASE_SF_JUGADOR_2 + ";" +
				this.COL_CALENDARIO_FASE_SF_ORDEN + ";" +
				this.COL_CALENDARIO_FASE_SF_CRUCE_CF + ";" +
				this.COL_CALENDARIO_FASE_SF_ACTIVO + ";" +
				this.COL_CALENDARIO_FASE_SF_INICIA + ";" +
				this.COL_CALENDARIO_FASE_SF_FASE_ANTERIOR + ";" +
				this.COL_CALENDARIO_FASE_SF_GRUPO_PROCEDENCIA_JUGADOR_1 + ";" +
				this.COL_CALENDARIO_FASE_SF_GRUPO_PROCEDENCIA_JUGADOR_2 + ";" +
				this.COL_CALENDARIO_FASE_SF_POSICION_PROCEDENCIA_JUGADOR_1 + ";" +
				this.COL_CALENDARIO_FASE_SF_POSICION_PROCEDENCIA_JUGADOR_2 + ";" +
				this.COL_CALENDARIO_FASE_SF_BOLERA
				);
	}
	
	//Cabeceras Fases FF - Calendario
	public final String COL_CALENDARIO_FASE_FF_ID = "ID";
	public final String COL_CALENDARIO_FASE_FF_CATEGORIA = "CATEGORIA";
	public final String COL_CALENDARIO_FASE_FF_IDCRUCE = "ID CRUCE";
	public final String COL_CALENDARIO_FASE_FF_NUM_ENFRENTAMIENTO = "NUM_ENFRENTAMIENTO";
	public final String COL_CALENDARIO_FASE_FF_JUGADOR_1 = "JUGADOR 1";
	public final String COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_1_P1 = "JUG. JUGADOR 1 P1";
	public final String COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_1_P2 = "JUG. JUGADOR 1 P2";
	public final String COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_1_P3 = "JUG. JUGADOR 1 P3";
	public final String COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_2_P1 = "JUG. JUGADOR 2 P1";
	public final String COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_2_P2 = "JUG. JUGADOR 2 P2";
	public final String COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_2_P3 = "JUG. JUGADOR 2 P3";
	public final String COL_CALENDARIO_FASE_FF_JUGADOR_2 = "JUGADOR 2";
	public final String COL_CALENDARIO_FASE_FF_ORDEN = "ORDEN";
	public final String COL_CALENDARIO_FASE_FF_CRUCE_CF = "CRUCE_CF";
	public final String COL_CALENDARIO_FASE_FF_ACTIVO = "ACTIVO";
	public final String COL_CALENDARIO_FASE_FF_INICIA = "INICIA";
	public final String COL_CALENDARIO_FASE_FF_FASE_ANTERIOR = "FASE ANTERIOR";
	public final String COL_CALENDARIO_FASE_FF_GRUPO_PROCEDENCIA_JUGADOR_1 = "GR PROC JUG 1";
	public final String COL_CALENDARIO_FASE_FF_GRUPO_PROCEDENCIA_JUGADOR_2 = "GR PROC JUG 2";
	public final String COL_CALENDARIO_FASE_FF_POSICION_PROCEDENCIA_JUGADOR_1 = "POS PROC JUG 1";
	public final String COL_CALENDARIO_FASE_FF_POSICION_PROCEDENCIA_JUGADOR_2 = "POS PROC JUG 2";
	public final String COL_CALENDARIO_FASE_FF_BOLERA = "BOLERA";
	
	public String getInsertCabeceraCalendarioFaseFFRow(){
		return (this.COL_CALENDARIO_FASE_FF_ID + ";" +
				this.COL_CALENDARIO_FASE_FF_CATEGORIA + ";" +
				this.COL_CALENDARIO_FASE_FF_IDCRUCE + ";" +
				this.COL_CALENDARIO_FASE_FF_NUM_ENFRENTAMIENTO + ";" +
				this.COL_CALENDARIO_FASE_FF_JUGADOR_1 + ";" +
				this.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_1_P1 + ";" +
				this.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_1_P2 + ";" +
				this.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_1_P3 + ";" +
				this.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_2_P1 + ";" +
				this.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_2_P2 + ";" +
				this.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_2_P3 + ";" +
				this.COL_CALENDARIO_FASE_FF_JUGADOR_2 + ";" +
				this.COL_CALENDARIO_FASE_FF_ORDEN + ";" +
				this.COL_CALENDARIO_FASE_FF_CRUCE_CF + ";" +
				this.COL_CALENDARIO_FASE_FF_ACTIVO + ";" +
				this.COL_CALENDARIO_FASE_FF_INICIA + ";" +
				this.COL_CALENDARIO_FASE_FF_FASE_ANTERIOR + ";" +
				this.COL_CALENDARIO_FASE_FF_GRUPO_PROCEDENCIA_JUGADOR_1 + ";" +
				this.COL_CALENDARIO_FASE_FF_GRUPO_PROCEDENCIA_JUGADOR_2 + ";" +
				this.COL_CALENDARIO_FASE_FF_POSICION_PROCEDENCIA_JUGADOR_1 + ";" +
				this.COL_CALENDARIO_FASE_FF_POSICION_PROCEDENCIA_JUGADOR_2 + ";" +
				this.COL_CALENDARIO_FASE_FF_BOLERA
				);
	}
	
	public List<Estructura2021> getEstructuraCalendarioCuartosFinal(){
		List<Estructura2021> list = new ArrayList<>();
		
		list.add(new Estructura2021(this.F1, this.C1 , COL_CALENDARIO_FASE_SF_ID, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_CF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C2 , COL_CALENDARIO_FASE_SF_CATEGORIA, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_CF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C3 , COL_CALENDARIO_FASE_SF_IDCRUCE, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_CF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C4 , COL_CALENDARIO_FASE_SF_NUM_ENFRENTAMIENTO, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_CF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C5 , COL_CALENDARIO_FASE_SF_JUGADOR_1, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_CF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C6 , COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_1_P1, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_CF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C7 , COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_1_P2, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_CF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C8 , COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_1_P3, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_CF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C9 , COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_2_P1, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_CF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C10 , COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_2_P2, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_CF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C11 , COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_2_P3, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_CF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C12 , COL_CALENDARIO_FASE_SF_JUGADOR_2, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_CF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C13 , COL_CALENDARIO_FASE_SF_ORDEN, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_CF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C14 , COL_CALENDARIO_FASE_SF_CRUCE_CF, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_CF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C15 , COL_CALENDARIO_FASE_SF_ACTIVO, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_CF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C16 , COL_CALENDARIO_FASE_SF_INICIA, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_CF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C17 , COL_CALENDARIO_FASE_SF_FASE_ANTERIOR, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_CF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C18 , COL_CALENDARIO_FASE_SF_GRUPO_PROCEDENCIA_JUGADOR_1, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_CF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C19 , COL_CALENDARIO_FASE_SF_GRUPO_PROCEDENCIA_JUGADOR_2, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_CF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C20 , COL_CALENDARIO_FASE_SF_POSICION_PROCEDENCIA_JUGADOR_1, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_CF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C21 , COL_CALENDARIO_FASE_SF_POSICION_PROCEDENCIA_JUGADOR_2, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_CF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C22 , COL_CALENDARIO_FASE_SF_BOLERA, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_CF, this.TD_TEXTO));
		
		return list; 
	}
	
	public List<Estructura2021> getEstructuraCalendarioSemifinal(){
		List<Estructura2021> list = new ArrayList<>();
		
		list.add(new Estructura2021(this.F1, this.C1 , COL_CALENDARIO_FASE_SF_ID, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C2 , COL_CALENDARIO_FASE_SF_CATEGORIA, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C3 , COL_CALENDARIO_FASE_SF_IDCRUCE, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C4 , COL_CALENDARIO_FASE_SF_NUM_ENFRENTAMIENTO, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C5 , COL_CALENDARIO_FASE_SF_JUGADOR_1, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C6 , COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_1_P1, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C7 , COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_1_P2, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C8 , COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_1_P3, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C9 , COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_2_P1, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C10 , COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_2_P2, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C11 , COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_2_P3, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C12 , COL_CALENDARIO_FASE_SF_JUGADOR_2, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C13 , COL_CALENDARIO_FASE_SF_ORDEN, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C14 , COL_CALENDARIO_FASE_SF_CRUCE_CF, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C15 , COL_CALENDARIO_FASE_SF_ACTIVO, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C16 , COL_CALENDARIO_FASE_SF_INICIA, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C17 , COL_CALENDARIO_FASE_SF_FASE_ANTERIOR, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C18 , COL_CALENDARIO_FASE_SF_GRUPO_PROCEDENCIA_JUGADOR_1, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C19 , COL_CALENDARIO_FASE_SF_GRUPO_PROCEDENCIA_JUGADOR_2, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C20 , COL_CALENDARIO_FASE_SF_POSICION_PROCEDENCIA_JUGADOR_1, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C21 , COL_CALENDARIO_FASE_SF_POSICION_PROCEDENCIA_JUGADOR_2, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C22 , COL_CALENDARIO_FASE_SF_BOLERA, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_SF, this.TD_TEXTO));
		
		return list; 
	}
	
	
	public List<Estructura2021> getEstructuraCalendarioFinal(){
		List<Estructura2021> list = new ArrayList<>();
		
		list.add(new Estructura2021(this.F1, this.C1 , COL_CALENDARIO_FASE_FF_ID, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C2 , COL_CALENDARIO_FASE_FF_CATEGORIA, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C3 , COL_CALENDARIO_FASE_FF_IDCRUCE, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C4 , COL_CALENDARIO_FASE_FF_NUM_ENFRENTAMIENTO, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C5 , COL_CALENDARIO_FASE_FF_JUGADOR_1, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C6 , COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_1_P1, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C7 , COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_1_P2, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C8 , COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_1_P3, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C9 , COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_2_P1, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C10 , COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_2_P2, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C11 , COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_2_P3, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C12 , COL_CALENDARIO_FASE_FF_JUGADOR_2, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C13 , COL_CALENDARIO_FASE_FF_ORDEN, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C14 , COL_CALENDARIO_FASE_FF_CRUCE_CF, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C15 , COL_CALENDARIO_FASE_FF_ACTIVO, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C16 , COL_CALENDARIO_FASE_FF_INICIA, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C17 , COL_CALENDARIO_FASE_FF_FASE_ANTERIOR, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C18 , COL_CALENDARIO_FASE_FF_GRUPO_PROCEDENCIA_JUGADOR_1, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C19 , COL_CALENDARIO_FASE_FF_GRUPO_PROCEDENCIA_JUGADOR_2, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C20 , COL_CALENDARIO_FASE_FF_POSICION_PROCEDENCIA_JUGADOR_1, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C21 , COL_CALENDARIO_FASE_FF_POSICION_PROCEDENCIA_JUGADOR_2, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2021(this.F1, this.C22 , COL_CALENDARIO_FASE_FF_BOLERA, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_FF, this.TD_TEXTO));		
		return list; 
	}
	
}
