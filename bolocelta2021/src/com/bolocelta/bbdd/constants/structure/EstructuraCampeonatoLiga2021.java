package com.bolocelta.bbdd.constants.structure;

import java.util.ArrayList;
import java.util.List;

import com.bolocelta.bbdd.constants.NombresTablas2021;

public class EstructuraCampeonatoLiga2021 extends NombresTablas2021{
	
	private List<Estructura2021> listCalendario = new ArrayList<Estructura2021>();
	private List<Estructura2021> listClasificacion = new ArrayList<Estructura2021>();
	
	
	//Filas
	private final Long F1 = new Long(0);
	
	//Columnas
	private final Integer C1 = 1;
	private final Integer C2 = 2;
	private final Integer C3 = 3;
	private final Integer C4 = 4;
	private final Integer C5 = 5;
	private final Integer C6 = 6;
	private final Integer C7 = 7;
	private final Integer C8 = 8;
	private final Integer C9 = 9;
	private final Integer C10 = 10;
	private final Integer C11 = 11;
	private final Integer C12 = 12;
	
	//Cabeceras Calendario
	public final String COL_CALENDARIO_ID = "ID";
	public final String COL_CALENDARIO_CATEGORIA = "CATEGORIA";
	public final String COL_CALENDARIO_JORNADA = "JORNADA";
	public final String COL_CALENDARIO_FECHA = "FECHA";
	public final String COL_CALENDARIO_HORA = "HORA";
	public final String COL_CALENDARIO_PGEQUIPO1 = "PGEQUIPO1";
	public final String COL_CALENDARIO_EQUIPO1 = "EQUIPO1";
	public final String COL_CALENDARIO_PGEQUIPO2 = "PGEQUIPO2";
	public final String COL_CALENDARIO_EQUIPO2 = "EQUIPO2";
	public final String COL_CALENDARIO_ACTIVO = "ACTIVO";
	public final String COL_CALENDARIO_ID_VUELTA = "IDVUELTA";
	public final String COL_CALENDARIO_FOTO = "FOTO";
	
	//Cabeceras Clasificacion
	public final String COL_CLASIFICACION_ID = "ID";
	public final String COL_CLASIFICACION_CATEGORIA = "CATEGORIA";
	public final String COL_CLASIFICACION_EQUIPO = "EQUIPO";
	public final String COL_CLASIFICACION_J = "J";
	public final String COL_CLASIFICACION_G = "G";
	public final String COL_CLASIFICACION_E = "E";
	public final String COL_CLASIFICACION_P = "P";
	public final String COL_CLASIFICACION_PF = "PF";
	public final String COL_CLASIFICACION_PC = "PC";
	public final String COL_CLASIFICACION_PT = "PT";
	
	public EstructuraCampeonatoLiga2021() {
	}
	
	public EstructuraCampeonatoLiga2021(String excelCalendario, String excelClasificacion) {
		dataCalendario(excelCalendario);
		dataClasificacion(excelClasificacion);
	}

	private void dataCalendario(String excelCalendario) {
		//Cabecera - Calendario
		listCalendario.add(new Estructura2021(this.F1, this.C1, COL_CALENDARIO_ID, excelCalendario, this.TD_TEXTO));
		listCalendario.add(new Estructura2021(this.F1, this.C2, COL_CALENDARIO_CATEGORIA, excelCalendario, this.TD_TEXTO));
		listCalendario.add(new Estructura2021(this.F1, this.C3, COL_CALENDARIO_JORNADA, excelCalendario, this.TD_TEXTO));
		listCalendario.add(new Estructura2021(this.F1, this.C4, COL_CALENDARIO_FECHA, excelCalendario, this.TD_TEXTO));
		listCalendario.add(new Estructura2021(this.F1, this.C5, COL_CALENDARIO_HORA, excelCalendario, this.TD_TEXTO));
		listCalendario.add(new Estructura2021(this.F1, this.C6, COL_CALENDARIO_PGEQUIPO1, excelCalendario, this.TD_TEXTO));
		listCalendario.add(new Estructura2021(this.F1, this.C7, COL_CALENDARIO_EQUIPO1, excelCalendario, this.TD_TEXTO));
		listCalendario.add(new Estructura2021(this.F1, this.C8, COL_CALENDARIO_PGEQUIPO2, excelCalendario, this.TD_TEXTO));
		listCalendario.add(new Estructura2021(this.F1, this.C9, COL_CALENDARIO_EQUIPO2, excelCalendario, this.TD_TEXTO));
		listCalendario.add(new Estructura2021(this.F1, this.C10, COL_CALENDARIO_ACTIVO, excelCalendario, this.TD_TEXTO));
		listCalendario.add(new Estructura2021(this.F1, this.C11, COL_CALENDARIO_ID_VUELTA, excelCalendario, this.TD_TEXTO));
		listCalendario.add(new Estructura2021(this.F1, this.C12, COL_CALENDARIO_FOTO, excelCalendario, this.TD_TEXTO));
	}
	
	private void dataClasificacion(String excelClasificacion) {
		//Cabecera - Clasificacion
		listClasificacion.add(new Estructura2021(this.F1, this.C1, COL_CLASIFICACION_ID, excelClasificacion, this.TD_TEXTO));
		listClasificacion.add(new Estructura2021(this.F1, this.C2, COL_CLASIFICACION_CATEGORIA, excelClasificacion, this.TD_TEXTO));
		listClasificacion.add(new Estructura2021(this.F1, this.C3, COL_CLASIFICACION_EQUIPO, excelClasificacion, this.TD_TEXTO));
		listClasificacion.add(new Estructura2021(this.F1, this.C4, COL_CLASIFICACION_J, excelClasificacion, this.TD_TEXTO));
		listClasificacion.add(new Estructura2021(this.F1, this.C5, COL_CLASIFICACION_G, excelClasificacion, this.TD_TEXTO));
		listClasificacion.add(new Estructura2021(this.F1, this.C6, COL_CLASIFICACION_E, excelClasificacion, this.TD_TEXTO));
		listClasificacion.add(new Estructura2021(this.F1, this.C7, COL_CLASIFICACION_P, excelClasificacion, this.TD_TEXTO));
		listClasificacion.add(new Estructura2021(this.F1, this.C8, COL_CLASIFICACION_PF, excelClasificacion, this.TD_TEXTO));
		listClasificacion.add(new Estructura2021(this.F1, this.C9, COL_CLASIFICACION_PC, excelClasificacion, this.TD_TEXTO));
		listClasificacion.add(new Estructura2021(this.F1, this.C10, COL_CLASIFICACION_PT, excelClasificacion, this.TD_TEXTO));
	}
	
	public List<Estructura2021> getCalendarioList(){
		return this.listCalendario;
	}
	
	public List<Estructura2021> getClasificacionList(){
		return this.listClasificacion;
	}
	
	public String getInsertCabeceraCalendarioRow(){
		return (this.COL_CALENDARIO_ID + ";" +
				this.COL_CALENDARIO_CATEGORIA + ";" +
				this.COL_CALENDARIO_JORNADA + ";" +
				this.COL_CALENDARIO_FECHA + ";" +
				this.COL_CALENDARIO_HORA + ";" +
				this.COL_CALENDARIO_PGEQUIPO1 + ";" +
				this.COL_CALENDARIO_EQUIPO1 + ";" +
				this.COL_CALENDARIO_PGEQUIPO2 + ";" +
				this.COL_CALENDARIO_EQUIPO2 + ";" +
				this.COL_CALENDARIO_ACTIVO + ";" +
				this.COL_CALENDARIO_ID_VUELTA + ";" +
				this.COL_CALENDARIO_FOTO 
				);
	}
	
	public String getInsertCabeceraClasificacionRow(){
		return (this.COL_CLASIFICACION_ID + ";" +
				this.COL_CLASIFICACION_CATEGORIA + ";" +
				this.COL_CLASIFICACION_EQUIPO + ";" +
				this.COL_CLASIFICACION_J + ";" +
				this.COL_CLASIFICACION_G + ";" +
				this.COL_CLASIFICACION_E + ";" +
				this.COL_CLASIFICACION_P + ";" +
				this.COL_CLASIFICACION_PF + ";" +
				this.COL_CLASIFICACION_PC + ";" +
				this.COL_CLASIFICACION_PT 
				);
	}
	
}
