package com.bolocelta.bbdd.constants.structure;

import java.util.ArrayList;
import java.util.List;

import com.bolocelta.bbdd.constants.NombresTablas2021;

public class EstructuraCampeonatoLigaInfantil2021 extends NombresTablas2021{
	
	private List<Estructura2021> listCalendario = new ArrayList<Estructura2021>();
	
	//Filas
	private final Long F1 = new Long(0);
	
	//Columnas
	private final Integer C1 = 1;
	private final Integer C2 = 2;
	private final Integer C3 = 3;
	private final Integer C4 = 4;
	private final Integer C5 = 5;
	private final Integer C6 = 6;
	private final Integer C7 = 7;
	private final Integer C8 = 8;
	private final Integer C9 = 9;
	private final Integer C10 = 10;
	private final Integer C11 = 11;
	private final Integer C12 = 12;
	private final Integer C13 = 13;
	
	//Cabeceras Calendario
	public final String COL_CALENDARIO_ID = "ID";
	public final String COL_CALENDARIO_JORNADA = "JORNADA";
	public final String COL_CALENDARIO_FECHA = "FECHA";
	public final String COL_CALENDARIO_HORA = "HORA";
	public final String COL_CALENDARIO_RONDA1 = "RONDA1";
	public final String COL_CALENDARIO_RONDA2 = "RONDA2";
	public final String COL_CALENDARIO_RONDA3 = "RONDA3";
	public final String COL_CALENDARIO_RONDA4 = "RONDA4";
	public final String COL_CALENDARIO_PUNTOS = "PUNTOS";
	public final String COL_CALENDARIO_ACTIVO = "ACTIVO";
	public final String COL_CALENDARIO_BOLERA = "BOLERA";
	public final String COL_CALENDARIO_NOMBRE = "NOMBRE";
	public final String COL_CALENDARIO_CATEGORIA = "CATEGORIA";
	
	public EstructuraCampeonatoLigaInfantil2021() {
	}
	
	public EstructuraCampeonatoLigaInfantil2021(String excelCalendario) {
		dataCalendario(excelCalendario);
	}

	private void dataCalendario(String excelCalendario) {
		//Cabecera - Calendario
		listCalendario.add(new Estructura2021(this.F1, this.C1, COL_CALENDARIO_ID, excelCalendario, this.TD_TEXTO));
		listCalendario.add(new Estructura2021(this.F1, this.C2, COL_CALENDARIO_JORNADA, excelCalendario, this.TD_TEXTO));
		listCalendario.add(new Estructura2021(this.F1, this.C3, COL_CALENDARIO_FECHA, excelCalendario, this.TD_TEXTO));
		listCalendario.add(new Estructura2021(this.F1, this.C4, COL_CALENDARIO_HORA, excelCalendario, this.TD_TEXTO));
		listCalendario.add(new Estructura2021(this.F1, this.C5, COL_CALENDARIO_RONDA1, excelCalendario, this.TD_TEXTO));
		listCalendario.add(new Estructura2021(this.F1, this.C6, COL_CALENDARIO_RONDA2, excelCalendario, this.TD_TEXTO));
		listCalendario.add(new Estructura2021(this.F1, this.C7, COL_CALENDARIO_RONDA3, excelCalendario, this.TD_TEXTO));
		listCalendario.add(new Estructura2021(this.F1, this.C8, COL_CALENDARIO_RONDA4, excelCalendario, this.TD_TEXTO));
		listCalendario.add(new Estructura2021(this.F1, this.C9, COL_CALENDARIO_PUNTOS, excelCalendario, this.TD_TEXTO));
		listCalendario.add(new Estructura2021(this.F1, this.C10, COL_CALENDARIO_ACTIVO, excelCalendario, this.TD_TEXTO));
		listCalendario.add(new Estructura2021(this.F1, this.C11, COL_CALENDARIO_BOLERA, excelCalendario, this.TD_TEXTO));
		listCalendario.add(new Estructura2021(this.F1, this.C12, COL_CALENDARIO_NOMBRE, excelCalendario, this.TD_TEXTO));
		listCalendario.add(new Estructura2021(this.F1, this.C13, COL_CALENDARIO_CATEGORIA, excelCalendario, this.TD_TEXTO));
	}
	
	public List<Estructura2021> getCalendarioList(){
		return this.listCalendario;
	}
	
	public String getInsertCabeceraCalendarioRow(){
		return (this.COL_CALENDARIO_ID + ";" +
				this.COL_CALENDARIO_JORNADA + ";" +
				this.COL_CALENDARIO_FECHA + ";" +
				this.COL_CALENDARIO_HORA + ";" +
				this.COL_CALENDARIO_RONDA1 + ";" +
				this.COL_CALENDARIO_RONDA2 + ";" +
				this.COL_CALENDARIO_RONDA3 + ";" +
				this.COL_CALENDARIO_RONDA4 + ";" +
				this.COL_CALENDARIO_PUNTOS + ";" +
				this.COL_CALENDARIO_ACTIVO + ";" +
				this.COL_CALENDARIO_BOLERA + ";" +
				this.COL_CALENDARIO_NOMBRE + ";" +
				this.COL_CALENDARIO_CATEGORIA + ";"
				);
	}

	
}
