package com.bolocelta.bbdd.constants.structure;

import java.util.ArrayList;
import java.util.List;

import com.bolocelta.bbdd.constants.NombresTablas2021;

public class EstructuraCampeonatoLigaFemenina2021 extends NombresTablas2021{
	
	private List<Estructura2021> listCalendario = new ArrayList<Estructura2021>();
	private List<Estructura2021> listClasificacion = new ArrayList<Estructura2021>();
	
	
	//Filas
	private final Long F1 = new Long(0);
	
	//Columnas
	private final Integer C1 = 1;
	private final Integer C2 = 2;
	private final Integer C3 = 3;
	private final Integer C4 = 4;
	private final Integer C5 = 5;
	private final Integer C6 = 6;
	private final Integer C7 = 7;
	private final Integer C8 = 8;
	private final Integer C9 = 9;
	private final Integer C10 = 10;
	
	//Cabeceras Calendario
	public final String COL_CALENDARIO_ID = "ID";
	public final String COL_CALENDARIO_JORNADA = "JORNADA";
	public final String COL_CALENDARIO_FECHA = "FECHA";
	public final String COL_CALENDARIO_HORA = "HORA";
	public final String COL_CALENDARIO_TIRADA = "TIRADA";
	public final String COL_CALENDARIO_SACADA = "SACADA";
	public final String COL_CALENDARIO_PUNTOS = "PUNTOS";
	public final String COL_CALENDARIO_ACTIVO = "ACTIVO";
	public final String COL_CALENDARIO_BOLERA = "BOLERA";
	public final String COL_CALENDARIO_ID_JUGADORA = "ID_JUGADORA";
	
	//Cabeceras Clasificacion
	public final String COL_CLASIFICACION_ID = "ID";
	public final String COL_CLASIFICACION_ID_JUGADORA = "ID_JUGADORA";
	public final String COL_CLASIFICACION_PUNTOS_TIRADA = "PT";
	public final String COL_CLASIFICACION_PUNTOS_SACADA = "PS";
	public final String COL_CLASIFICACION_PUNTOS = "PUNTOS";
	
	public EstructuraCampeonatoLigaFemenina2021() {
	}
	
	public EstructuraCampeonatoLigaFemenina2021(String excelCalendario, String excelClasificacion) {
		dataCalendario(excelCalendario);
		dataClasificacion(excelClasificacion);
	}

	private void dataCalendario(String excelCalendario) {
		//Cabecera - Calendario
		listCalendario.add(new Estructura2021(this.F1, this.C1, COL_CALENDARIO_ID, excelCalendario, this.TD_TEXTO));
		listCalendario.add(new Estructura2021(this.F1, this.C2, COL_CALENDARIO_JORNADA, excelCalendario, this.TD_TEXTO));
		listCalendario.add(new Estructura2021(this.F1, this.C3, COL_CALENDARIO_FECHA, excelCalendario, this.TD_TEXTO));
		listCalendario.add(new Estructura2021(this.F1, this.C4, COL_CALENDARIO_HORA, excelCalendario, this.TD_TEXTO));
		listCalendario.add(new Estructura2021(this.F1, this.C5, COL_CALENDARIO_TIRADA, excelCalendario, this.TD_TEXTO));
		listCalendario.add(new Estructura2021(this.F1, this.C6, COL_CALENDARIO_SACADA, excelCalendario, this.TD_TEXTO));
		listCalendario.add(new Estructura2021(this.F1, this.C7, COL_CALENDARIO_PUNTOS, excelCalendario, this.TD_TEXTO));
		listCalendario.add(new Estructura2021(this.F1, this.C8, COL_CALENDARIO_ACTIVO, excelCalendario, this.TD_TEXTO));
		listCalendario.add(new Estructura2021(this.F1, this.C9, COL_CALENDARIO_BOLERA, excelCalendario, this.TD_TEXTO));
		listCalendario.add(new Estructura2021(this.F1, this.C10, COL_CALENDARIO_ID_JUGADORA, excelCalendario, this.TD_TEXTO));
	}
	
	private void dataClasificacion(String excelClasificacion) {
		//Cabecera - Clasificacion
		listClasificacion.add(new Estructura2021(this.F1, this.C1, COL_CLASIFICACION_ID, excelClasificacion, this.TD_TEXTO));
		listClasificacion.add(new Estructura2021(this.F1, this.C2, COL_CLASIFICACION_ID_JUGADORA, excelClasificacion, this.TD_TEXTO));
		listClasificacion.add(new Estructura2021(this.F1, this.C3, COL_CLASIFICACION_PUNTOS_TIRADA, excelClasificacion, this.TD_TEXTO));
		listClasificacion.add(new Estructura2021(this.F1, this.C4, COL_CLASIFICACION_PUNTOS_SACADA, excelClasificacion, this.TD_TEXTO));
		listClasificacion.add(new Estructura2021(this.F1, this.C5, COL_CLASIFICACION_PUNTOS, excelClasificacion, this.TD_TEXTO));
	}
	
	public List<Estructura2021> getCalendarioList(){
		return this.listCalendario;
	}
	
	public List<Estructura2021> getClasificacionList(){
		return this.listClasificacion;
	}
	
	public String getInsertCabeceraCalendarioRow(){
		return (this.COL_CALENDARIO_ID + ";" +
				this.COL_CALENDARIO_JORNADA + ";" +
				this.COL_CALENDARIO_FECHA + ";" +
				this.COL_CALENDARIO_HORA + ";" +
				this.COL_CALENDARIO_TIRADA + ";" +
				this.COL_CALENDARIO_SACADA + ";" +
				this.COL_CALENDARIO_PUNTOS + ";" +
				this.COL_CALENDARIO_ACTIVO + ";" +
				this.COL_CALENDARIO_BOLERA + ";" +
				this.COL_CALENDARIO_ID_JUGADORA + ";"
				);
	}
	
	public String getInsertCabeceraClasificacionRow(){
		return (this.COL_CLASIFICACION_ID + ";" +
				this.COL_CLASIFICACION_ID_JUGADORA + ";" +
				this.COL_CLASIFICACION_PUNTOS_TIRADA + ";" +
				this.COL_CLASIFICACION_PUNTOS_SACADA + ";" +
				this.COL_CLASIFICACION_PUNTOS + ";"
				);
	}
	
}
