package com.bolocelta.application.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils2021 {
	
	private static DateFormat Df_DDMMYYYY = new SimpleDateFormat("dd/MM/yyyy");
	private static DateFormat Df_DDMMYYYYHHMMSS = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

	public static Date convertStringToDate(String date) {
		if (date == null || date.isEmpty()) {
			return null;
		}
		try {
			return Df_DDMMYYYY.parse(date);
		} catch (ParseException e) {
			System.out.println(e.getMessage());
			System.out.println(e);
		}
		return null;
	}
	
}
