package com.bolocelta.application.session;

import java.io.Serializable;

import javax.inject.Inject;
import javax.inject.Named;

import com.bolocelta.entities.Equipos2021;
import com.bolocelta.facade.EquiposFacade2021;

@Named
@SuppressWarnings("serial")
public class SessionState extends AbstractSessionState implements Serializable{
	
	@Inject
	private EquiposFacade2021 equiposFacade;
	
	public String getUser(){
		return getIdentityInfo() != null ? getIdentityInfo().getUsuario().getUser() : null;
	}
	
	public String getUserName(){
		return getIdentityInfo() != null ? getIdentityInfo().getUsuario().getNombreCompleto() : null;
	}
	
	public boolean hasPermission(String permiso){
		return getIdentityInfo() != null ? getIdentityInfo().hasPermission(permiso) : false;
	}
	
	public Equipos2021 getUserEquipo(){
		if(getIdentityInfo() != null && getIdentityInfo().getUsuario() != null && getIdentityInfo().getUsuario().getEquipo() != null){
			return (Equipos2021) equiposFacade.read(getIdentityInfo().getUsuario().getEquipo());
		}
		return null;
	}


}
