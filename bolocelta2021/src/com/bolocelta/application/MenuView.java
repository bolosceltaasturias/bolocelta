package com.bolocelta.application;

import java.io.Serializable;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;

@ManagedBean
@ApplicationScoped
public class MenuView implements Serializable{
	
	@Inject
	private Redirect redirect;
	
	public static String PAG_HOME = "index.xhtml";
	
	private static String PAG_DEFAULT = "default.xhtml";
	
	public static String PAG_LOGIN = "login.xhtml";
	
	public static String PAG_OM_DIR  = "web/";
	
	public static String PAG_OM_HISTORIA = "historia.xhtml";
	
	public static String PAG_OM_CONTACTO = "contacto.xhtml";
	
	public static String PAG_OM_NOTICIAS = "noticias.xhtml";
	
	public static String PAG_OM_PATROCINADORES = "patrocinadores.xhtml";
	
	public static String PAG_OM_BOLERAS_LIST = "boleras.xhtml";
	
	public static String PAG_OM_EQUIPOS_LIST = "equipos.xhtml";
	
	public static String PAG_OM_CATEGORIAS_LIST = "categorias.xhtml";
	
	public static String PAG_OM_MODALIDADES_LIST = "modalidades.xhtml";
	
	public static String PAG_OM_CALENDARIO_LIST = "calendario.xhtml";
	
	public static String PAG_OM_CALENDARIO_ESPANA_LIST = "calendarioEspana.xhtml";
	
	public static String PAG_OM_CALENDARIO_ASTURIAS_LIST = "calendarioAsturias.xhtml";
	
	public static String PAG_OM_CAMPEONATO_EQUIPOS_PRIMERA_LIST = "campeonatoEquiposPrimera.xhtml";
	
	public static String PAG_OM_CAMPEONATO_EQUIPOS_SEGUNDA_LIST = "campeonatoEquiposSegunda.xhtml";
	
	public static String PAG_OM_CAMPEONATO_EQUIPOS_TERCERA_LIST = "campeonatoEquiposTercera.xhtml";
	
	private String CONTEXT_PATH = null;
	
	public static String PAG_OM_DIR_WEB  = "web/";
	
	//Gestion equipo
	public static String PAG_OM_EDIT_FICHA_EQUIPO = "fichaEquipo.xhtml";
	public static String PAG_OM_FICHA_EQUIPO = "fichaEquipoView.xhtml";
	public static String PAG_OM_ADD_JUGADOR_CAMPEONATO_INDIVIDUAL = "inscripcionIndividual.xhtml";
	public static String PAG_OM_ADD_PAREJA_CAMPEONATO_INDIVIDUAL = "inscripcionParejas.xhtml";
	public static String PAG_OM_ADD_PAREJA_CAMPEONATO_MIXTO = "inscripcionParejasMixto.xhtml";
	public static String PAG_OM_ADD_PAREJA_CAMPEONATO_FEMENINO = "inscripcionParejasFemenino.xhtml";
	
	//Gestion Jugadores
	public static String PAG_OM_EDIT_FICHA_JUGADOR = "fichaJugador.xhtml";
	
	//Gestion Parejas
	public static String PAG_OM_EDIT_FICHA_PAREJA = "fichaPareja.xhtml";
	public static String PAG_OM_ADD_JUGADOR_1_PAREJA_EQUIPO = "jugadores1Equipo.xhtml";
	public static String PAG_OM_ADD_JUGADOR_2_PAREJA_EQUIPO = "jugadores2Equipo.xhtml";
	public static String PAG_OM_ADD_JUGADOR_PAREJA_OTRO_EQUIPO = "jugadoresOtrosEquipos.xhtml";
	public static String PAG_OM_EDIT_FICHA_PAREJA_MIXTO = "fichaParejaMixto.xhtml";
	public static String PAG_OM_ADD_JUGADOR_1_PAREJA_EQUIPO_MIXTO = "jugadores1EquipoMixto.xhtml";
	public static String PAG_OM_ADD_JUGADOR_2_PAREJA_EQUIPO_MIXTO = "jugadores2EquipoMixto.xhtml";
	public static String PAG_OM_ADD_JUGADOR_PAREJA_OTRO_EQUIPO_MIXTO = "jugadoresOtrosEquiposMixto.xhtml";
	public static String PAG_OM_EDIT_FICHA_PAREJA_FEMENINO = "fichaParejasFemenino.xhtml";
	public static String PAG_OM_ADD_JUGADOR_1_PAREJA_EQUIPO_FEMENINO = "jugadores1EquipoParejasFemenino.xhtml";
	public static String PAG_OM_ADD_JUGADOR_2_PAREJA_EQUIPO_FEMENINO = "jugadores2EquipoParejasFemenino.xhtml";
	public static String PAG_OM_ADD_JUGADOR_PAREJA_OTRO_EQUIPO_FEMENINO = "jugadoresOtrosEquiposParejasFemenino.xhtml";
	
	
	//Campeonato individual
	public static String PAG_OM_ADD_CAMPEONATO_MASCULINO_INDIVIDUAL_PRIMERA = "campeonatoMasculinoIndividualPrimeraAdd.xhtml";
	public static String PAG_OM_ADD_CAMPEONATO_MASCULINO_INDIVIDUAL_SEGUNDA = "campeonatoMasculinoIndividualSegundaAdd.xhtml";
	public static String PAG_OM_ADD_CAMPEONATO_MASCULINO_INDIVIDUAL_TERCERA = "campeonatoMasculinoIndividualTerceraAdd.xhtml";
	public static String PAG_OM_ADD_CAMPEONATO_FEMENINO_INDIVIDUAL = "campeonatoFemeninoIndividualAdd.xhtml";
	
	//Campeonato parejas
	public static String PAG_OM_ADD_CAMPEONATO_MASCULINO_PAREJAS_PRIMERA = "campeonatoMasculinoParejasPrimeraAdd.xhtml";
	public static String PAG_OM_ADD_CAMPEONATO_MASCULINO_PAREJAS_SEGUNDA = "campeonatoMasculinoParejasSegundaAdd.xhtml";
	public static String PAG_OM_ADD_CAMPEONATO_MASCULINO_PAREJAS_TERCERA = "campeonatoMasculinoParejasTerceraAdd.xhtml";
	public static String PAG_OM_ADD_CAMPEONATO_FEMENINO_PAREJAS = "campeonatoParejasFemeninoAdd.xhtml";
	public static String PAG_OM_ADD_CAMPEONATO_MIXTO_PAREJAS = "campeonatoMixtoParejasAdd.xhtml";
	
	//Campeonato liga
	public static String PAG_OM_ADD_CAMPEONATO_LIGA = "campeonatoLigaAdd.xhtml";

	//Categorias
	public static String PAG_OM_ADD_CATEGORIA_PAREJA = "categorias.xhtml";
	
	
	public MenuView() {
		contextPath();
	}
			
	private String contextPath(){
		if(this.CONTEXT_PATH == null){
//			HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
//			String url = req.getRequestURL().toString();
//			this.CONTEXT_PATH = url.substring(0, url.length() - req.getRequestURI().length()) + req.getContextPath() + "/";
		}
		return this.CONTEXT_PATH;
	}

	public String getUrlInicio(){
		return contextPath() + PAG_OM_DIR_WEB + this.PAG_HOME;
	}
	
	public String getUrlLogin(){
		return contextPath() + PAG_OM_DIR_WEB + this.PAG_LOGIN;
	}
	
	public String getUrlBolerasList(){
		return contextPath() + this.PAG_OM_DIR + this.PAG_OM_BOLERAS_LIST;
	}
	
	public String getUrlEquiposList(){
		return contextPath() + this.PAG_OM_DIR + this.PAG_OM_EQUIPOS_LIST;
	}
	
	public String getUrlCategoriasList(){
		return contextPath() + this.PAG_OM_DIR + this.PAG_OM_CATEGORIAS_LIST;
	}
	
	public String getUrlModalidadesList(){
		return contextPath() + this.PAG_OM_DIR + this.PAG_OM_MODALIDADES_LIST;
	}
	
	public String getUrlCalendarioEspanaList(){
		return contextPath() + this.PAG_OM_DIR + this.PAG_OM_CALENDARIO_ESPANA_LIST;
	}
	
	public String getUrlCalendarioAsturiasList(){
		return contextPath() + this.PAG_OM_DIR + this.PAG_OM_CALENDARIO_ASTURIAS_LIST;
	}
	
	public String getUrlCampeonatoEquiposPrimeraList(){
		return contextPath() + this.PAG_OM_DIR + this.PAG_OM_CAMPEONATO_EQUIPOS_PRIMERA_LIST;
	}
	
	public String getUrlCampeonatoEquiposSegundaList(){
		return contextPath() + this.PAG_OM_DIR + this.PAG_OM_CAMPEONATO_EQUIPOS_SEGUNDA_LIST;
	}
	
	public String getUrlCampeonatoEquiposTerceraList(){
		return contextPath() + this.PAG_OM_DIR + this.PAG_OM_CAMPEONATO_EQUIPOS_TERCERA_LIST;
	}
	
	public String getUrlCampeonatoPrimeraIndividualAdd(){
		return contextPath() + this.PAG_OM_DIR + this.PAG_OM_ADD_CAMPEONATO_MASCULINO_INDIVIDUAL_PRIMERA;
	}
	
	public String getUrlCampeonatoSegundaIndividualAdd(){
		return contextPath() + this.PAG_OM_DIR + this.PAG_OM_ADD_CAMPEONATO_MASCULINO_INDIVIDUAL_SEGUNDA;
	}
	
	public String getUrlCampeonatoTerceraIndividualAdd(){
		return contextPath() + this.PAG_OM_DIR + this.PAG_OM_ADD_CAMPEONATO_MASCULINO_INDIVIDUAL_TERCERA;
	}
	
	public String getUrlCampeonatoFemeninoIndividualAdd(){
		return contextPath() + this.PAG_OM_DIR + this.PAG_OM_ADD_CAMPEONATO_FEMENINO_INDIVIDUAL;
	}
	
	public String getUrlCampeonatoPrimeraParejasAdd(){
		return contextPath() + this.PAG_OM_DIR + this.PAG_OM_ADD_CAMPEONATO_MASCULINO_PAREJAS_PRIMERA;
	}
	
	public String getUrlCampeonatoLigaAdd(){
		return contextPath() + this.PAG_OM_DIR + this.PAG_OM_ADD_CAMPEONATO_LIGA;
	}
	
	public String getUrlFichaEquipo(){
		return contextPath() + this.PAG_OM_DIR + this.PAG_OM_EDIT_FICHA_EQUIPO;
	}
	
	
	public void doHome(){
		redirect.getHome();
	}
	
	public void doAccessHistoria(){
		redirect.getHistoria();
	}
	
	public void doAccessContacto(){
		redirect.getContacto();
	}
	
	public void doAccessBoleras(){
		redirect.getBoleras();
	}
	
	public void doAccessCategorias(){
		redirect.getCategorias();
	}
	
	public void doAccessEquipos(){
		redirect.getEquipos();
	}
	
	public void doAccessModalidades(){
		redirect.getModalidad();
	}
	
	public void doAccessCalendario(){
		redirect.getCalendario();
	}
	
	public void doAccessCalendarioAsturias(){
		redirect.getCalendarioAsturias();
	}
	
	public void doAccessCalendarioEspana(){
		redirect.getCalendarioEspana();
	}
	
	public void doAccessCampeonatoEquiposPrimera(){
		redirect.getCampeonatoEquiposPrimera();
	}
	
	public void doAccessCampeonatoEquiposSegunda(){
		redirect.getCampeonatoEquiposSegunda();
	}
	
	public void doAccessCampeonatoEquiposTercera(){
		redirect.getCampeonatoEquiposTercera();
	}
	
	public void doAccessCampeonatoFemenino(){
		redirect.getCampeonatoFemenino();
	}
	
	public void doAccessCampeonatoIndividualPrimera(){
		redirect.getCampeonatoIndividualPrimera();
	}
	
	public void doAccessCampeonatoIndividualSegunda(){
		redirect.getCampeonatoIndividualSegunda();
	}
	
	public void doAccessCampeonatoIndividualTercera(){
		redirect.getCampeonatoIndividualTercera();
	}
	
	public void doAccessCampeonatoIndividualFemenino(){
		redirect.getCampeonatoIndividualFemenino();
	}
	
	public void doAccessCampeonatoParejasPrimera(){
		redirect.getCampeonatoParejasPrimera();
	}
	
	public void doAccessCampeonatoParejasSegunda(){
		redirect.getCampeonatoParejasSegunda();
	}
	
	public void doAccessCampeonatoParejasTercera(){
		redirect.getCampeonatoParejasTercera();
	}
	
	public void doAccessCampeonatoParejasFemenino(){
		redirect.getCampeonatoParejasFemenino();
	}
	
	public void doAccessCampeonatoParejasMixto(){
		redirect.getCampeonatoParejasMixto();
	}
	
	public void doAccessNoticias(){
		redirect.getNoticias();
	}
	
	public void doAccessPatrocinadores(){
		redirect.getPatrocinadores();
	}
	
	public void doAccessDocumentos(){
		redirect.getCampeonatoParejasMixto();
	}
	
	public void doAccessGaleria(){
		redirect.getCampeonatoParejasMixto();
	}
	
	public void doAccessTV(){
		redirect.getCampeonatoParejasMixto();
	}
	
	public void doAccessCampeonatoMasculinoIndividualPrimera(){
		redirect.doAccessCampeonatoMasculinoIndividualPrimera();
	}
	
	public void doAccessCampeonatoMasculinoIndividualSegunda(){
		redirect.doAccessCampeonatoMasculinoIndividualSegunda();
	}
	
	public void doAccessCampeonatoMasculinoIndividualTercera(){
		redirect.doAccessCampeonatoMasculinoIndividualTercera();
	}
	
	public void doAccessCampeonatoMasculinoIndividualFemenino(){
		redirect.doAccessCampeonatoMasculinoIndividualFemenino();
	}
	
	public void doAccessCampeonatoLiga(){
		redirect.doAccessCampeonatoLiga();
	}
	
	public void doAccessCampeonatoMasculinoParejasPrimera(){
		redirect.doAccessCampeonatoMasculinoParejasPrimera();
	}
	
	public void doAccessCampeonatoMasculinoParejasSegunda(){
		redirect.doAccessCampeonatoMasculinoParejasSegunda();
	}
	
	public void doAccessCampeonatoMasculinoParejasTercera(){
		redirect.doAccessCampeonatoMasculinoParejasTercera();
	}
	
	public void doAccessCampeonatoMasculinoParejasFemenino(){
		redirect.doAccessCampeonatoMasculinoParejasFemenino();
	}
	
	public void doAccessCampeonatoMixtoParejas(){
		redirect.doAccessCampeonatoParejasMixto();
	}
	
	public void doAppFacebook(){
		redirect.redirectExternal("https://www.facebook.com/asociacionbolocelta");
	}
	
	public void doAppTwitter(){
		redirect.redirectExternal("https://twitter.com/BcAsociacion");
	}
	
	public void doAppInstagram(){
		redirect.redirectExternal("https://www.instagram.com/asociacionbolocelta/");
	}
	
	public void doAppYoutube(){
		redirect.redirectExternal("https://www.youtube.com/channel/UCB_ucotzzXfjTyK_GlPx47g");
	}
	
	public void doAppLigaSmartLive(){
		redirect.redirectExternal("https://www.laligasports.es/retransmisiones");
	}
	
	public void doAppVinx(){
		redirect.redirectExternal("http://www.vinx.es/");
	}
	
	public void doAppRedirect(String url){
		redirect.redirectExternal(url);
	}
	

}
