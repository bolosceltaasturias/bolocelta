package com.bolocelta.application.enumerations;

public class CategoriasEnumeration2021 {
	
	public static final Integer CATEGORIA_PRIMERA = 1;
	public static final Integer CATEGORIA_SEGUNDA = 2;
	public static final Integer CATEGORIA_TERCERA = 3;
	public static final Integer CATEGORIA_FEMENINO = 4;
	public static final Integer CATEGORIA_MIXTO = 8;

}
