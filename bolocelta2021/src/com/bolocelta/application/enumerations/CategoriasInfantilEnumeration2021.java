package com.bolocelta.application.enumerations;

public class CategoriasInfantilEnumeration2021 {
	
	public static final String CATEGORIA_MINI = "MINI";
	public static final String CATEGORIA_BENJAMIN = "BENJAMIN";
	public static final String CATEGORIA_ALEVIN = "ALEVIN";
	public static final String CATEGORIA_INFANTIL = "INFANTIL";
	public static final String CATEGORIA_CADETE = "CADETE";

}
