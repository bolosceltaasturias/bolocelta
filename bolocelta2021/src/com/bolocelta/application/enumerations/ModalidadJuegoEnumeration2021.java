package com.bolocelta.application.enumerations;

public class ModalidadJuegoEnumeration2021 {
	
	public static final Integer MODALIDAD_EQUIPOS = 1;
	public static final Integer MODALIDAD_INDIVIDUAL = 2;
	public static final Integer MODALIDAD_PAREJAS = 3;
	public static final Integer MODALIDAD_INDIVIDUAL_FEMENINO = 4;
	public static final Integer MODALIDAD_PAREJAS_FEMENINO = 5;
	public static final Integer MODALIDAD_MIXTO = 6;
	public static final Integer MODALIDAD_VETERANOS = 7;
	public static final Integer MODALIDAD_JUVENILES = 8;

}
