package com.bolocelta.entities;

public class ParticipantesIndividual2021 {

	private Integer id;
	private Integer idJugador;
	private Jugadores2021 jugador;
	private String fechaInscripcion;
	private String usuarioInscripcion;
	private String activo;
	private Long rowNum;
	private Integer numeroJugadorGrupo;

	public ParticipantesIndividual2021() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdJugador() {
		return idJugador;
	}

	public void setIdJugador(Integer idJugador) {
		this.idJugador = idJugador;
	}

	public Jugadores2021 getJugador() {
		return jugador;
	}

	public void setJugador(Jugadores2021 jugador) {
		this.jugador = jugador;
	}

	public String getFechaInscripcion() {
		return fechaInscripcion;
	}

	public void setFechaInscripcion(String fechaInscripcion) {
		this.fechaInscripcion = fechaInscripcion;
	}

	public String getUsuarioInscripcion() {
		return usuarioInscripcion;
	}

	public void setUsuarioInscripcion(String usuarioInscripcion) {
		this.usuarioInscripcion = usuarioInscripcion;
	}

	public String getActivo() {
		return activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	public Long getRowNum() {
		return rowNum;
	}

	public void setRowNum(Long rowNum) {
		this.rowNum = rowNum;
	}
	
	public Equipos2021 getEquipo() {
		if(jugador != null && jugador.getEquipo() != null){
			return jugador.getEquipo();
		}
		return null;
	}

	public Integer getNumeroJugadorGrupo() {
		return numeroJugadorGrupo;
	}

	public void setNumeroJugadorGrupo(Integer numeroJugadorGrupo) {
		this.numeroJugadorGrupo = numeroJugadorGrupo;
	}

}
