package com.bolocelta.entities.usuarios;

public class Usuarios2021 {

	private Integer id;
	private Integer equipo;
	private String nombre;
	private String apellido;
	private String user;
	private String pass;
	private Integer rolId;
	private Roles2021 rol;

	public Usuarios2021() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEquipo() {
		return equipo;
	}

	public void setEquipo(Integer equipo) {
		this.equipo = equipo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public Roles2021 getRol() {
		return rol;
	}

	public void setRol(Roles2021 rol) {
		this.rol = rol;
	}

	public Integer getRolId() {
		return rolId;
	}

	public void setRolId(Integer rolId) {
		this.rolId = rolId;
	}
	
	public String getNombreCompleto(){
		if(this.nombre != null && this.apellido != null){
			return this.nombre + " " + this.apellido;
		}
		return null;
	}

}
