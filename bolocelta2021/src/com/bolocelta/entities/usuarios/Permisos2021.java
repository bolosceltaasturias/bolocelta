package com.bolocelta.entities.usuarios;

public class Permisos2021 {

	private Integer id;
	private Integer rolId;
	private Roles2021 rol;
	private String permiso;

	public Permisos2021() {
		// TODO Auto-generated constructor stub
	}

	public Permisos2021(Integer id, String permiso) {
		this.id = id;
		this.permiso = permiso;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPermiso() {
		return permiso;
	}

	public void setPermiso(String permiso) {
		this.permiso = permiso;
	}

	public Integer getRolId() {
		return rolId;
	}

	public void setRolId(Integer rolId) {
		this.rolId = rolId;
	}

	public Roles2021 getRol() {
		return rol;
	}

	public void setRol(Roles2021 rol) {
		this.rol = rol;
	}

}
