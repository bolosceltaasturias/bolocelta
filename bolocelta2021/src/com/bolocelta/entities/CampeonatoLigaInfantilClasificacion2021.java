package com.bolocelta.entities;

public class CampeonatoLigaInfantilClasificacion2021 {

	private Integer puntos;
	private Integer tantos;
	private String nombre;
	private String categoria;
	private Integer puntosFinal;
	private Integer tantosFinal;

	public CampeonatoLigaInfantilClasificacion2021() {
		this.puntos = 0;
		this.tantos = 0;
		this.puntosFinal = 0;
		this.tantosFinal = 0;
		// TODO Auto-generated constructor stub
	}

	public Integer getPuntos() {
		return puntos;
	}

	public void setPuntos(Integer puntos) {
		this.puntos = puntos;
	}

	public Integer getTantos() {
		return tantos;
	}

	public void setTantos(Integer tantos) {
		this.tantos = tantos;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	
	public Integer getPuntosFinal() {
		return puntosFinal;
	}

	public void setPuntosFinal(Integer puntosFinal) {
		this.puntosFinal = puntosFinal;
	}
	
	public Integer getTantosFinal() {
		return tantosFinal;
	}

	public void setTantosFinal(Integer tantosFinal) {
		this.tantosFinal = tantosFinal;
	}
	
}
