package com.bolocelta.entities;

import java.util.HashMap;
import java.util.Map.Entry;

public class VotacionesMVP2021 {

	private Long rowNum;
	private Integer id;
	private Integer categoria;
	private Integer idJugador;
	private Jugadores2021 jugador;
	private String esVeterano;
	private String esJuvenil;
	private HashMap<Integer, Integer> equipoPuntos;
	
	public Integer getTotalPuntos(){
		Integer totalPuntos = 0;
		for (Entry<Integer, Integer> entry : equipoPuntos.entrySet()) {
		    Object value = entry.getValue();
		    Integer puntosEquipo = Integer.valueOf(value.toString());
		    totalPuntos += puntosEquipo;
	    }
		return totalPuntos;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public Long getRowNum() {
		return rowNum;
	}

	public void setRowNum(Long rowNum) {
		this.rowNum = rowNum;
	}
	
	public Integer getCategoria() {
		return categoria;
	}

	public void setCategoria(Integer categoria) {
		this.categoria = categoria;
	}

	public Integer getIdJugador() {
		return idJugador;
	}

	public void setIdJugador(Integer idJugador) {
		this.idJugador = idJugador;
	}
	
	public Jugadores2021 getJugador() {
		return jugador;
	}

	public void setJugador(Jugadores2021 jugador) {
		this.jugador = jugador;
	}

	public String getEsVeterano() {
		return esVeterano;
	}

	public void setEsVeterano(String esVeterano) {
		this.esVeterano = esVeterano;
	}

	public String getEsJuvenil() {
		return esJuvenil;
	}

	public void setEsJuvenil(String esJuvenil) {
		this.esJuvenil = esJuvenil;
	}
	
	public HashMap<Integer, Integer> getEquipoPuntos() {
		return equipoPuntos;
	}

	public void setEquipoPuntos(HashMap<Integer, Integer> equipoPuntos) {
		this.equipoPuntos = equipoPuntos;
	}
	
}
