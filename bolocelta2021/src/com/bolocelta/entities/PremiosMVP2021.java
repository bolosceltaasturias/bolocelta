package com.bolocelta.entities;

public class PremiosMVP2021 {

	private Long rowNum;
	private Integer id;
	private Integer categoria;
	private Integer idJugador;
	private Jugadores2021 jugador;
	private String esVeterano;
	private String esJuvenil;
	private Integer pia;
	private Integer ppa;
	private Integer pea;
	private Integer pma;
	private Integer paa;
	private Integer pva;
	private Integer pja;
	private Integer plf;
	private Integer pie;
	private Integer ppe;
	private Integer pee;
	private Integer pme;
	private Integer pve;
	private Integer pje;
	
	public Integer getTotalPuntos(){
		return this.pia + this.ppa + this.pea + this.pma + this.paa + this.pva + this.pja + this.plf + this.pie + this.ppe + this.pee + this.pme + this.pve + this.pje;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public Long getRowNum() {
		return rowNum;
	}

	public void setRowNum(Long rowNum) {
		this.rowNum = rowNum;
	}
	
	public Integer getCategoria() {
		return categoria;
	}

	public void setCategoria(Integer categoria) {
		this.categoria = categoria;
	}

	public Integer getIdJugador() {
		return idJugador;
	}

	public void setIdJugador(Integer idJugador) {
		this.idJugador = idJugador;
	}
	
	public Jugadores2021 getJugador() {
		return jugador;
	}

	public void setJugador(Jugadores2021 jugador) {
		this.jugador = jugador;
	}

	public String getEsVeterano() {
		return esVeterano;
	}

	public void setEsVeterano(String esVeterano) {
		this.esVeterano = esVeterano;
	}

	public String getEsJuvenil() {
		return esJuvenil;
	}

	public void setEsJuvenil(String esJuvenil) {
		this.esJuvenil = esJuvenil;
	}

	public Integer getPia() {
		return pia;
	}

	public void setPia(Integer pia) {
		this.pia = pia;
	}

	public Integer getPpa() {
		return ppa;
	}

	public void setPpa(Integer ppa) {
		this.ppa = ppa;
	}

	public Integer getPea() {
		return pea;
	}

	public void setPea(Integer pea) {
		this.pea = pea;
	}

	public Integer getPma() {
		return pma;
	}

	public void setPma(Integer pma) {
		this.pma = pma;
	}

	public Integer getPaa() {
		return paa;
	}

	public void setPaa(Integer paa) {
		this.paa = paa;
	}

	public Integer getPva() {
		return pva;
	}

	public void setPva(Integer pva) {
		this.pva = pva;
	}

	public Integer getPja() {
		return pja;
	}

	public void setPja(Integer pja) {
		this.pja = pja;
	}

	public Integer getPlf() {
		return plf;
	}

	public void setPlf(Integer plf) {
		this.plf = plf;
	}

	public Integer getPie() {
		return pie;
	}

	public void setPie(Integer pie) {
		this.pie = pie;
	}

	public Integer getPpe() {
		return ppe;
	}

	public void setPpe(Integer ppe) {
		this.ppe = ppe;
	}

	public Integer getPee() {
		return pee;
	}

	public void setPee(Integer pee) {
		this.pee = pee;
	}

	public Integer getPme() {
		return pme;
	}

	public void setPme(Integer pme) {
		this.pme = pme;
	}

	public Integer getPve() {
		return pve;
	}

	public void setPve(Integer pve) {
		this.pve = pve;
	}

	public Integer getPje() {
		return pje;
	}

	public void setPje(Integer pje) {
		this.pje = pje;
	}

}
