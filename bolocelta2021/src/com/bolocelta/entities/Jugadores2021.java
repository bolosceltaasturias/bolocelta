package com.bolocelta.entities;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Jugadores2021 {

	private Integer id;
	private Integer equipoId;
	private Equipos2021 equipo;
	private String nombre;
	private String apodo;
	private String numeroFicha;
	private String email;
	private String telefono;
	private Integer edad;
	private String activo;
	private Long rowNum;
	private String modalidad;
	private String individualPrimera;
	private String individualSegunda;
	private String individualTercera;
	private String individualFemenino;
	private boolean masculino;
	private boolean femenino;
	private String nombreEquipo;
	
	private String acabonesJ1;
	private String acabonesJ2;
	private String acabonesJ3;
	private String acabonesJ4;
	private String acabonesJ5;
	private String acabonesJ6;
	private String acabonesJ7;
	private String acabonesJ8;
	private String acabonesJ9;
	private String acabonesJ10;
	private String acabonesJ11;
	private String acabonesJ12;
	private String acabonesJ13;
	private String acabonesJ14;

	public Jugadores2021() {
		// TODO Auto-generated constructor stub
	}

	public Jugadores2021(Integer equipoId, String activo, String individualPrimera,
			String individualSegunda, String individualTercera, String individualFemenino, String acabones) {
		this.equipoId = equipoId;
		this.activo = activo;
		this.individualPrimera = individualPrimera;
		this.individualSegunda = individualSegunda;
		this.individualTercera = individualTercera;
		this.individualFemenino = individualFemenino;
		this.acabonesJ1  = acabones;
		this.acabonesJ2  = acabones;
		this.acabonesJ3  = acabones;
		this.acabonesJ4  = acabones;
		this.acabonesJ5  = acabones;
		this.acabonesJ6  = acabones;
		this.acabonesJ7  = acabones;
		this.acabonesJ8  = acabones;
		this.acabonesJ9  = acabones;
		this.acabonesJ10 = acabones;
		this.acabonesJ11 = acabones;
		this.acabonesJ12 = acabones;
		this.acabonesJ13 = acabones;
		this.acabonesJ14 = acabones;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEquipoId() {
		return equipoId;
	}

	public void setEquipoId(Integer equipoId) {
		this.equipoId = equipoId;
	}

	public Equipos2021 getEquipo() {
		return equipo;
	}

	public void setEquipo(Equipos2021 equipo) {
		this.equipo = equipo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApodo() {
		return apodo;
	}

	public void setApodo(String apodo) {
		this.apodo = apodo;
	}

	public String getNumeroFicha() {
		return numeroFicha;
	}

	public void setNumeroFicha(String numeroFicha) {
		this.numeroFicha = numeroFicha;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}

	public String getActivo() {
		return activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	public Long getRowNum() {
		return rowNum;
	}

	public void setRowNum(Long rowNum) {
		this.rowNum = rowNum;
	}

	public String getModalidad() {
		return modalidad;
	}

	public void setModalidad(String modalidad) {
		this.modalidad = modalidad;
	}

	public String getIndividualPrimera() {
		return individualPrimera;
	}

	public void setIndividualPrimera(String individualPrimera) {
		this.individualPrimera = individualPrimera;
	}

	public String getIndividualSegunda() {
		return individualSegunda;
	}

	public void setIndividualSegunda(String individualSegunda) {
		this.individualSegunda = individualSegunda;
	}

	public String getIndividualTercera() {
		return individualTercera;
	}

	public void setIndividualTercera(String individualTercera) {
		this.individualTercera = individualTercera;
	}

	public String getIndividualFemenino() {
		return individualFemenino;
	}

	public void setIndividualFemenino(String individualFemenino) {
		this.individualFemenino = individualFemenino;
	}
	
	public String getApodoShow() {
		return apodo.toUpperCase();
	}

	public String getNombreShow() {
		String nombreShow = null;
		if (this.apodo != null && !this.apodo.isEmpty()) {
			nombreShow = this.apodo.toUpperCase();
		} else if (this.nombre != null && !this.nombre.isEmpty()) {
			nombreShow = this.nombre;
		}
		return nombreShow;
	}
	
	public String getNombreEquipo() {
		nombreEquipo = null;
		if (this.apodo != null && !this.apodo.isEmpty()) {
			nombreEquipo = this.apodo.toUpperCase();
		} else if (this.nombre != null && !this.nombre.isEmpty()) {
			nombreEquipo = this.nombre;
		}

		if (this.equipo != null && this.equipo.getNombre() != null && !this.equipo.getNombre().isEmpty()) {
			nombreEquipo += " (" + this.equipo.getNombre() + ")";
		}
		return nombreEquipo;
	}

	public String getNombreEquipoShow() {
		return getNombreEquipo();
	}
	
	public String getNombreEquipoShowCuadro() {
		String nombreEquipo = null;
		if (this.apodo != null && !this.apodo.isEmpty()) {
			nombreEquipo = this.apodo.toUpperCase();
		} else if (this.nombre != null && !this.nombre.isEmpty()) {
			nombreEquipo = this.nombre;
		}

		if (this.equipo != null && this.equipo.getNombre() != null && !this.equipo.getNombre().isEmpty()) {
			nombreEquipo += " # " + this.equipo.getNombre() + "";
			nombreEquipo += " # ";
		}
		return nombreEquipo;
	}
	

	public boolean isMasculino() {
		return masculino;
	}

	public void setMasculino(boolean masculino) {
		this.masculino = masculino;
	}

	public boolean isFemenino() {
		return femenino;
	}

	public void setFemenino(boolean femenino) {
		this.femenino = femenino;
	}
	
	public String getInsertRow(){
		return (this.rowNum.toString() + ";" +
				this.equipoId.toString() + ";" +
				this.nombre.toString() + ";" +
				this.apodo.toString() + ";" +
				this.numeroFicha.toString() + ";" +
				this.email.toString() + ";" +
				this.telefono.toString() + ";" +
				this.edad.toString() + ";" +
				this.activo.toString() + ";" +
				this.modalidad.toString() + ";" +
				this.individualPrimera.toString() + ";" +
				this.individualSegunda.toString() + ";" +
				this.individualTercera.toString() + ";" +
				this.individualFemenino.toString() + ";" +
				"-" + ";" +
				"-" + ";" +
				"-" + ";" +
				"-" + ";" +
				"-" + ";" +
				"-" + ";" +
				"-" + ";" +
				"-" + ";" +
				"-" + ";" +
				"-" + ";" +
				"-" + ";" +
				"-" + ";" +
				"-" + ";" +
				"-"
				);
	}
	
	@Override
	public int hashCode() {
		if(id == null){
			return 0;
		}
		return id;
	}
	
	
	public String getAcabonesJ1() {
		return acabonesJ1;
	}

	public void setAcabonesJ1(String acabonesJ1) {
		this.acabonesJ1 = acabonesJ1;
	}

	public String getAcabonesJ2() {
		return acabonesJ2;
	}

	public void setAcabonesJ2(String acabonesJ2) {
		this.acabonesJ2 = acabonesJ2;
	}

	public String getAcabonesJ3() {
		return acabonesJ3;
	}

	public void setAcabonesJ3(String acabonesJ3) {
		this.acabonesJ3 = acabonesJ3;
	}

	public String getAcabonesJ4() {
		return acabonesJ4;
	}

	public void setAcabonesJ4(String acabonesJ4) {
		this.acabonesJ4 = acabonesJ4;
	}

	public String getAcabonesJ5() {
		return acabonesJ5;
	}

	public void setAcabonesJ5(String acabonesJ5) {
		this.acabonesJ5 = acabonesJ5;
	}

	public String getAcabonesJ6() {
		return acabonesJ6;
	}

	public void setAcabonesJ6(String acabonesJ6) {
		this.acabonesJ6 = acabonesJ6;
	}

	public String getAcabonesJ7() {
		return acabonesJ7;
	}

	public void setAcabonesJ7(String acabonesJ7) {
		this.acabonesJ7 = acabonesJ7;
	}

	public String getAcabonesJ8() {
		return acabonesJ8;
	}

	public void setAcabonesJ8(String acabonesJ8) {
		this.acabonesJ8 = acabonesJ8;
	}

	public String getAcabonesJ9() {
		return acabonesJ9;
	}

	public void setAcabonesJ9(String acabonesJ9) {
		this.acabonesJ9 = acabonesJ9;
	}

	public String getAcabonesJ10() {
		return acabonesJ10;
	}

	public void setAcabonesJ10(String acabonesJ10) {
		this.acabonesJ10 = acabonesJ10;
	}

	public String getAcabonesJ11() {
		return acabonesJ11;
	}

	public void setAcabonesJ11(String acabonesJ11) {
		this.acabonesJ11 = acabonesJ11;
	}

	public String getAcabonesJ12() {
		return acabonesJ12;
	}

	public void setAcabonesJ12(String acabonesJ12) {
		this.acabonesJ12 = acabonesJ12;
	}

	public String getAcabonesJ13() {
		return acabonesJ13;
	}

	public void setAcabonesJ13(String acabonesJ13) {
		this.acabonesJ13 = acabonesJ13;
	}

	public String getAcabonesJ14() {
		return acabonesJ14;
	}

	public void setAcabonesJ14(String acabonesJ14) {
		this.acabonesJ14 = acabonesJ14;
	}
	
	public Integer getTotalAcabones(){
		Integer totalAcabones = 0;
		
		if(isNumeric(this.acabonesJ1)){
			totalAcabones += Integer.valueOf(this.acabonesJ1);
		}
		if(isNumeric(this.acabonesJ2)){
			totalAcabones += Integer.valueOf(this.acabonesJ2);
		}
		if(isNumeric(this.acabonesJ3)){
			totalAcabones += Integer.valueOf(this.acabonesJ3);
		}
		if(isNumeric(this.acabonesJ4)){
			totalAcabones += Integer.valueOf(this.acabonesJ4);
		}
		if(isNumeric(this.acabonesJ5)){
			totalAcabones += Integer.valueOf(this.acabonesJ5);
		}
		if(isNumeric(this.acabonesJ6)){
			totalAcabones += Integer.valueOf(this.acabonesJ6);
		}
		if(isNumeric(this.acabonesJ7)){
			totalAcabones += Integer.valueOf(this.acabonesJ7);
		}
		if(isNumeric(this.acabonesJ8)){
			totalAcabones += Integer.valueOf(this.acabonesJ8);
		}
		if(isNumeric(this.acabonesJ9)){
			totalAcabones += Integer.valueOf(this.acabonesJ9);
		}
		if(isNumeric(this.acabonesJ10)){
			totalAcabones += Integer.valueOf(this.acabonesJ10);
		}
		if(isNumeric(this.acabonesJ11)){
			totalAcabones += Integer.valueOf(this.acabonesJ11);
		}
		if(isNumeric(this.acabonesJ12)){
			totalAcabones += Integer.valueOf(this.acabonesJ12);
		}
		if(isNumeric(this.acabonesJ13)){
			totalAcabones += Integer.valueOf(this.acabonesJ13);
		}
		if(isNumeric(this.acabonesJ14)){
			totalAcabones += Integer.valueOf(this.acabonesJ14);
		}
		
		return totalAcabones;
	}
	
	public BigDecimal getPromedioAcabones(){
		BigDecimal promedio = BigDecimal.ZERO;
		Integer jornadasJugadas = 0;
		Integer totalAcabones = getTotalAcabones();
		
		if(isNumeric(this.acabonesJ1)){
			jornadasJugadas++;
		}
		if(isNumeric(this.acabonesJ2)){
			jornadasJugadas++;
		}
		if(isNumeric(this.acabonesJ3)){
			jornadasJugadas++;
		}
		if(isNumeric(this.acabonesJ4)){
			jornadasJugadas++;
		}
		if(isNumeric(this.acabonesJ5)){
			jornadasJugadas++;
		}
		if(isNumeric(this.acabonesJ6)){
			jornadasJugadas++;
		}
		if(isNumeric(this.acabonesJ7)){
			jornadasJugadas++;
		}
		if(isNumeric(this.acabonesJ8)){
			jornadasJugadas++;
		}
		if(isNumeric(this.acabonesJ9)){
			jornadasJugadas++;
		}
		if(isNumeric(this.acabonesJ10)){
			jornadasJugadas++;
		}
		if(isNumeric(this.acabonesJ11)){
			jornadasJugadas++;
		}
		if(isNumeric(this.acabonesJ12)){
			jornadasJugadas++;
		}
		if(isNumeric(this.acabonesJ13)){
			jornadasJugadas++;
		}
		if(isNumeric(this.acabonesJ14)){
			jornadasJugadas++;
		}
		
		if(jornadasJugadas > 0 && totalAcabones > 0){
			promedio = new BigDecimal(totalAcabones).divide(new BigDecimal(jornadasJugadas), 2, RoundingMode.HALF_UP);
		}

		return promedio.setScale(2);
	}

	private static boolean isNumeric(String cadena){
		try {
			Integer.parseInt(cadena);
			return true;
		} catch (NumberFormatException nfe){
			return false;
		}
	}
	
}
