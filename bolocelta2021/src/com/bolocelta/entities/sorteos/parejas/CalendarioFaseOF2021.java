package com.bolocelta.entities.sorteos.parejas;

import com.bolocelta.bbdd.constants.Activo2021;
import com.bolocelta.entities.Boleras2021;
import com.bolocelta.entities.Parejas2021;

public class CalendarioFaseOF2021 {

	private Integer id;
	private Integer categoriaId;
	private String idCruce;
	private Integer numeroEnfrentamiento;
	private Parejas2021 pareja1;
	private Integer pareja1Id;
	private Integer juegosPareja1P1;
	private Integer juegosPareja1P2;
	private Integer juegosPareja1P3;
	private Integer juegosPareja2P1;
	private Integer juegosPareja2P2;
	private Integer juegosPareja2P3;
	private Integer pareja2Id;
	private Parejas2021 pareja2;
	private Integer orden;
	private String cruceCF;
	private String activo;
	private Long rowNum;
	private String inicia;
	private Integer faseAnterior;
	private String grupoProcedenciaPareja1;
	private String grupoProcedenciaPareja2;
	private Integer posicionProcedenciaPareja1;
	private Integer posicionProcedenciaPareja2;
	private Integer boleraId;
	private Boleras2021 bolera;
	private String requipo1;
	private String requipo2;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCategoriaId() {
		return categoriaId;
	}

	public void setCategoriaId(Integer categoriaId) {
		this.categoriaId = categoriaId;
	}

	public Integer getNumeroEnfrentamiento() {
		return numeroEnfrentamiento;
	}

	public void setNumeroEnfrentamiento(Integer numeroEnfrentamiento) {
		this.numeroEnfrentamiento = numeroEnfrentamiento;
	}

	public Integer getPareja1Id() {
		return pareja1Id;
	}

	public void setPareja1Id(Integer pareja1Id) {
		this.pareja1Id = pareja1Id;
	}

	public Integer getJuegosPareja1P1() {
		return juegosPareja1P1;
	}

	public void setJuegosPareja1P1(Integer juegosPareja1P1) {
		this.juegosPareja1P1 = juegosPareja1P1;
	}

	public Integer getJuegosPareja1P2() {
		return juegosPareja1P2;
	}

	public void setJuegosPareja1P2(Integer juegosPareja1P2) {
		this.juegosPareja1P2 = juegosPareja1P2;
	}

	public Integer getJuegosPareja1P3() {
		return juegosPareja1P3;
	}

	public void setJuegosPareja1P3(Integer juegosPareja1P3) {
		this.juegosPareja1P3 = juegosPareja1P3;
	}

	public Integer getJuegosPareja2P1() {
		return juegosPareja2P1;
	}

	public void setJuegosPareja2P1(Integer juegosPareja2P1) {
		this.juegosPareja2P1 = juegosPareja2P1;
	}

	public Integer getJuegosPareja2P2() {
		return juegosPareja2P2;
	}

	public void setJuegosPareja2P2(Integer juegosPareja2P2) {
		this.juegosPareja2P2 = juegosPareja2P2;
	}

	public Integer getJuegosPareja2P3() {
		return juegosPareja2P3;
	}

	public void setJuegosPareja2P3(Integer juegosPareja2P3) {
		this.juegosPareja2P3 = juegosPareja2P3;
	}

	public Integer getPareja2Id() {
		return pareja2Id;
	}

	public void setPareja2Id(Integer pareja2Id) {
		this.pareja2Id = pareja2Id;
	}

	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}

	public String getCruceCF() {
		return cruceCF;
	}

	public void setCruceCF(String cruceCF) {
		this.cruceCF = cruceCF;
	}

	public String getActivo() {
		return activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	public Long getRowNum() {
		return rowNum;
	}

	public void setRowNum(Long rowNum) {
		this.rowNum = rowNum;
	}

	public Integer getFaseAnterior() {
		return faseAnterior;
	}

	public void setFaseAnterior(Integer faseAnterior) {
		this.faseAnterior = faseAnterior;
	}

	public String getGrupoProcedenciaPareja1() {
		return grupoProcedenciaPareja1;
	}

	public void setGrupoProcedenciaPareja1(String grupoProcedenciaPareja1) {
		this.grupoProcedenciaPareja1 = grupoProcedenciaPareja1;
	}

	public String getGrupoProcedenciaPareja2() {
		return grupoProcedenciaPareja2;
	}

	public void setGrupoProcedenciaPareja2(String grupoProcedenciaPareja2) {
		this.grupoProcedenciaPareja2 = grupoProcedenciaPareja2;
	}

	public Integer getPosicionProcedenciaPareja1() {
		return posicionProcedenciaPareja1;
	}

	public void setPosicionProcedenciaPareja1(Integer posicionProcedenciaPareja1) {
		this.posicionProcedenciaPareja1 = posicionProcedenciaPareja1;
	}

	public Integer getPosicionProcedenciaPareja2() {
		return posicionProcedenciaPareja2;
	}

	public void setPosicionProcedenciaPareja2(Integer posicionProcedenciaPareja2) {
		this.posicionProcedenciaPareja2 = posicionProcedenciaPareja2;
	}

	public String getInicia() {
		return inicia;
	}

	public void setInicia(String inicia) {
		this.inicia = inicia;
	}

	public Integer getBoleraId() {
		return boleraId;
	}

	public void setBoleraId(Integer boleraId) {
		this.boleraId = boleraId;
	}

	public String getIdCruce() {
		return idCruce;
	}

	public void setIdCruce(String idCruce) {
		this.idCruce = idCruce;
	}
	
	public String getCruce1IdGrupo(){
		return "GRUPO: " + this.getGrupoProcedenciaPareja1() + this.getPosicionProcedenciaPareja1();
	}
	
	public String getCruce2IdGrupo(){
		return "GRUPO: " + this.getGrupoProcedenciaPareja2() + this.getPosicionProcedenciaPareja2();
	}
	
	public String getInsertRow(){
		return (this.id + ";" +
					this.categoriaId + ";" +
					this.idCruce + ";" +
					this.numeroEnfrentamiento + ";" +
					this.pareja1Id + ";" +
					this.juegosPareja1P1 + ";" +
					this.juegosPareja1P2 + ";" +
					this.juegosPareja1P3 + ";" +
					this.juegosPareja2P1 + ";" +
					this.juegosPareja2P2 + ";" +
					this.juegosPareja2P3 + ";" +
					this.pareja2Id + ";" +
					this.orden + ";" +
					this.cruceCF + ";" +
					this.activo + ";" +
					this.inicia + ";" +
					this.faseAnterior + ";" +
					this.grupoProcedenciaPareja1 + ";" +
					this.grupoProcedenciaPareja2 + ";" +
					this.posicionProcedenciaPareja1 + ";" +
					this.posicionProcedenciaPareja2 + ";" +
					this.boleraId
					);
	}

	public Parejas2021 getPareja1() {
		return pareja1;
	}

	public void setPareja1(Parejas2021 pareja1) {
		this.pareja1 = pareja1;
	}

	public Parejas2021 getPareja2() {
		return pareja2;
	}

	public void setPareja2(Parejas2021 pareja2) {
		this.pareja2 = pareja2;
	}

	public Boleras2021 getBolera() {
		return bolera;
	}

	public void setBolera(Boleras2021 bolera) {
		this.bolera = bolera;
	}

	public String getRequipo1() {
		return requipo1;
	}

	public void setRequipo1(String requipo1) {
		this.requipo1 = requipo1;
	}

	public String getRequipo2() {
		return requipo2;
	}

	public void setRequipo2(String requipo2) {
		this.requipo2 = requipo2;
	}
	
	public String getOrigenPareja1(){
		return "GRUPO: " + this.grupoProcedenciaPareja1 + this.posicionProcedenciaPareja1;
	}

	public String getOrigenPareja2(){
		return "GRUPO: " + this.grupoProcedenciaPareja2 + this.posicionProcedenciaPareja2;
	}
	
	public String getPartida1(){
		return this.juegosPareja1P1.toString() + " - " + this.juegosPareja2P1.toString();
	}
	
	public String getPartida2(){
		return this.juegosPareja1P2.toString() + " - " + this.juegosPareja2P2.toString();
	}
	
	public String getPartida3(){
		return this.juegosPareja1P3.toString() + " - " + this.juegosPareja2P3.toString();
	}
	
	public boolean isModificable(){
		if(this.activo != null && this.activo.equals(Activo2021.SI)){
			return true;
		}
		return false;
	}
	
	public boolean isParejaEnFase(){
		if(this.pareja1Id > 0 && this.pareja2Id > 0){
			return true;
		}
		return false;
	}
	
	public Integer isGanaPareja(){
		//Calcular victoria, empate y derrota
    	Integer resultadoPartida1 =  getJuegosPareja1P1() - getJuegosPareja2P1();
    	Integer resultadoPartida2 =  getJuegosPareja1P2() - getJuegosPareja2P2();
    	Integer resultadoPartida3 =  getJuegosPareja1P3() - getJuegosPareja2P3();
    	
    	//Sin Jugar
    	if(resultadoPartida1 == 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
    		return 0;
    	}else
    	//Ganador 1 solo una partida
    	if(resultadoPartida1 > 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
    		return 1;
    	}else 
    	//Ganador 2 solo una partida
    	if(resultadoPartida1 < 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
    		return 2;
    	}else 
    	//Ganador 1 dos partidas
    	if( (resultadoPartida1 > 0 && resultadoPartida2 > 0) ||
			(resultadoPartida1 > 0 && resultadoPartida3 > 0) ||
			(resultadoPartida2 > 0 && resultadoPartida3 > 0)){
    		return 1;
    	}else
    	//Ganador 2 dos partidas
    	if( (resultadoPartida1 < 0 && resultadoPartida2 < 0) ||
			(resultadoPartida1 < 0 && resultadoPartida3 < 0) ||
			(resultadoPartida2 < 0 && resultadoPartida3 < 0)){
    		return 2;
    	}else{
    		return 0;
    	}
	}
	
	public String getPartidasPareja1(){
		return this.juegosPareja1P1.toString() + " - " + this.juegosPareja1P2.toString() + " - " + this.juegosPareja1P3.toString();
	}
	
	public String getPartidasPareja2(){
		return this.juegosPareja2P1.toString() + " - " + this.juegosPareja2P2.toString() + " - " + this.juegosPareja2P3.toString();
	}
	
	public boolean isPartida1Jugada(){
		if(this.juegosPareja1P1 > 0 || this.juegosPareja2P1 > 0){
			return true;
		}
		return false;
	}
	
	public boolean isPartida2Jugada(){
		if(this.juegosPareja1P2 > 0 || this.juegosPareja2P2 > 0){
			return true;
		}
		return false;
	}
	
	public boolean isPartida3Jugada(){
		if(this.juegosPareja1P3 > 0 || this.juegosPareja2P3 > 0){
			return true;
		}
		return false;
	}
	
	public Integer getPartidasJugadas(){
		Integer partidasJugadas = 0;
		if(isPartida1Jugada()) partidasJugadas++;
		if(isPartida2Jugada()) partidasJugadas++;
		if(isPartida3Jugada()) partidasJugadas++;
		return partidasJugadas;
	}
	
	public boolean isResultadoPartidas1(){
		if(isPartida1Jugada()){
			if(isPartida1FOtoFFGanaPareja() > 0){
				return true;
			}
		}
		return false;
	}
	
	public boolean isResultadoPartidas2(){
		if(isPartida1Jugada() && isPartida2Jugada() && !isPartida3Jugada()){
			if(
				(isPartida1FOtoFFGanaPareja() > 0 && isPartida2FOtoFFGanaPareja() > 0) &&
				(isPartida1FOtoFFGanaPareja() == isPartida2FOtoFFGanaPareja())
			){
				return true;
			}
		}else if(isPartida1Jugada() && isPartida2Jugada() && isPartida3Jugada()){
			if(
					(isPartida1FOtoFFGanaPareja() > 0 && isPartida2FOtoFFGanaPareja() > 0 && isPartida3FOtoFFGanaPareja() > 0) &&
					(isPartida1FOtoFFGanaPareja() == isPartida2FOtoFFGanaPareja() || 
					 isPartida1FOtoFFGanaPareja() == isPartida3FOtoFFGanaPareja() || 
					 isPartida2FOtoFFGanaPareja() == isPartida3FOtoFFGanaPareja())
			){
				return true;
			}
		}
		return false;
	}
	
	public Integer isPartida1FOtoFFGanaPareja(){
		//Calcular victoria, empate y derrota
    	Integer resultadoPartida1 =  getJuegosPareja1P1() - getJuegosPareja2P1();
    	
    	//Sin Jugar
    	if(resultadoPartida1 == 0){
    		return 0;
    	}else
    	//Ganador 1 solo una partida
    	if(resultadoPartida1 > 0){
    		return 1;
    	}else 
    	//Ganador 2 solo una partida
    	if(resultadoPartida1 < 0){
    		return 2;
    	}
    	return 0;
	}
	
	public Integer isPartida2FOtoFFGanaPareja(){
		//Calcular victoria, empate y derrota
    	Integer resultadoPartida2 =  getJuegosPareja1P2() - getJuegosPareja2P2();
    	
    	//Sin Jugar
    	if(resultadoPartida2 == 0){
    		return 0;
    	}else
    	//Ganador 1 solo una partida
    	if(resultadoPartida2 > 0){
    		return 1;
    	}else 
    	//Ganador 2 solo una partida
    	if(resultadoPartida2 < 0){
    		return 2;
    	}
    	return 0;
	}
	
	public Integer isPartida3FOtoFFGanaPareja(){
		//Calcular victoria, empate y derrota
    	Integer resultadoPartida3 =  getJuegosPareja1P3() - getJuegosPareja2P3();
    	
    	//Sin Jugar
    	if(resultadoPartida3 == 0){
    		return 0;
    	}else
    	//Ganador 1 solo una partida
    	if(resultadoPartida3 > 0){
    		return 1;
    	}else 
    	//Ganador 2 solo una partida
    	if(resultadoPartida3 < 0){
    		return 2;
    	}
    	return 0;
	}
	

}
