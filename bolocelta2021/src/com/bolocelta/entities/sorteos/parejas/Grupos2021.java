package com.bolocelta.entities.sorteos.parejas;

import java.util.LinkedHashMap;
import java.util.Map.Entry;

import com.bolocelta.entities.ParticipantesParejas2021;

public class Grupos2021 {

	private String id;
	private Integer categoriaId;
	private Integer boleraId;
	private String fecha;
	private String hora;
	private Integer parejas;
	private LinkedHashMap<Integer, ParticipantesParejas2021> parejasGrupoList = new LinkedHashMap();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getCategoriaId() {
		return categoriaId;
	}

	public void setCategoriaId(Integer categoriaId) {
		this.categoriaId = categoriaId;
	}

	public Integer getBoleraId() {
		return boleraId;
	}

	public void setBoleraId(Integer boleraId) {
		this.boleraId = boleraId;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public Integer getParejas() {
		return parejas;
	}

	public void setParejas(Integer parejas) {
		this.parejas = parejas;
	}

	public LinkedHashMap<Integer, ParticipantesParejas2021> getParejasGrupoList() {
		return parejasGrupoList;
	}

	public void setParejasGrupoList(LinkedHashMap<Integer, ParticipantesParejas2021> parejasGrupoList) {
		this.parejasGrupoList = parejasGrupoList;
	}
	
	public boolean isExisteParejaMismoEquipo(Integer idEquipo, LinkedHashMap<Integer, Integer> equipoMasParejasQueGrupos, Integer numeroGruposFase){
		
		Integer contParejasEquiposGrupo = 0;
		for (Entry<Integer, ParticipantesParejas2021> entry : this.parejasGrupoList.entrySet()) {
			Object value = entry.getValue();
			ParticipantesParejas2021 pi = (ParticipantesParejas2021) value;
			if(pi.getPareja().getJugador1().getEquipoId().equals(idEquipo) && pi.getPareja().getJugador2().getEquipoId().equals(idEquipo)){
				contParejasEquiposGrupo++;
			}
		}
		
		if(contParejasEquiposGrupo > 0){
			if(contParejasEquiposGrupo == 1){
				
				//Si hay una Pareja de un equipo en el grupo, 
				//pero el numero de parejas del equipo es mayor del numero de grupos, 
				//permitir asignar hasta 2 parejas en el mismo grupo
				
				if(equipoMasParejasQueGrupos.get(idEquipo) != null){
					Integer numeroParejas = equipoMasParejasQueGrupos.get(idEquipo);
					if(numeroParejas > numeroGruposFase){
						if((numeroParejas - numeroGruposFase) > 1){
							return false;
						}
					}
				}
				
			}
			return true;
		}
		return false;
	}
	
	public ParticipantesParejas2021 searchParejaGrupo(Integer numeroParejaGrupo, String grupo){
		for (Entry<Integer, ParticipantesParejas2021> entry : this.parejasGrupoList.entrySet()) {
			Object value = entry.getValue();
			ParticipantesParejas2021 pi = (ParticipantesParejas2021) value;
			if(pi.getNumeroParejaGrupo().equals(numeroParejaGrupo) && this.id.equals(grupo)){
				return pi;
			}
		}
		return null;
	}

}
