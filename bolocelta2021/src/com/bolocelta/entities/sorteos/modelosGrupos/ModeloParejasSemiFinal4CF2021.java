package com.bolocelta.entities.sorteos.modelosGrupos;

import java.util.LinkedHashMap;

import com.bolocelta.bbdd.constants.CrucesCampeonatos2021;
import com.bolocelta.bbdd.constants.TipoSorteo2021;

public class ModeloParejasSemiFinal4CF2021 {

	private String nombre = "Semifinal";
	private LinkedHashMap<Integer, ModeloParejasEnfrentamientoSemiFinal2021> enfrentamientosSemiFinal = new LinkedHashMap<>();

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedHashMap<Integer, ModeloParejasEnfrentamientoSemiFinal2021> getEnfrentamientosSemiFinal() {
		return enfrentamientosSemiFinal;
	}

	public void setEnfrentamientosSemiFinal(LinkedHashMap<Integer, ModeloParejasEnfrentamientoSemiFinal2021> enfrentamientosSemiFinal) {
		this.enfrentamientosSemiFinal = enfrentamientosSemiFinal;
	}
	
	public ModeloParejasSemiFinal4CF2021() {
		this.enfrentamientosSemiFinal.put(1, new ModeloParejasEnfrentamientoSemiFinal2021(CrucesCampeonatos2021.SF1, 1, CrucesCampeonatos2021.CFA, CrucesCampeonatos2021.CFB, 0, 0, 0, 0, TipoSorteo2021.MONEDA, CrucesCampeonatos2021.FF1));
		this.enfrentamientosSemiFinal.put(2, new ModeloParejasEnfrentamientoSemiFinal2021(CrucesCampeonatos2021.SF2, 1, CrucesCampeonatos2021.CFC, CrucesCampeonatos2021.CFD, 0, 0, 0, 0, TipoSorteo2021.MONEDA, CrucesCampeonatos2021.FF1));
	}

}
