package com.bolocelta.entities.sorteos.modelosGrupos;

import java.util.LinkedHashMap;

import com.bolocelta.bbdd.constants.CrucesCampeonatos2021;
import com.bolocelta.bbdd.constants.TipoSorteo2021;

public class ModeloIndividualFinalConsolacion4SF2021 {

	private String nombre = "Final Consolacion";
	private LinkedHashMap<Integer, ModeloIndividualEnfrentamientoFinalConsolacion2021> enfrentamientosFinalConsolacion = new LinkedHashMap<>();

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedHashMap<Integer, ModeloIndividualEnfrentamientoFinalConsolacion2021> getEnfrentamientosFinalConsolacion() {
		return enfrentamientosFinalConsolacion;
	}

	public void setEnfrentamientosFinalConsolacion(LinkedHashMap<Integer, ModeloIndividualEnfrentamientoFinalConsolacion2021> enfrentamientosFinalConsolacion) {
		this.enfrentamientosFinalConsolacion = enfrentamientosFinalConsolacion;
	}
	
	public ModeloIndividualFinalConsolacion4SF2021() {
		this.enfrentamientosFinalConsolacion.put(1, new ModeloIndividualEnfrentamientoFinalConsolacion2021(CrucesCampeonatos2021.FC1, 1, CrucesCampeonatos2021.SF1, CrucesCampeonatos2021.SF2, 0, 0, 0, 0, TipoSorteo2021.MONEDA, null));
	}

}
