package com.bolocelta.entities.sorteos.modelosGrupos;

import java.util.LinkedHashMap;

import com.bolocelta.bbdd.constants.CrucesCampeonatos2021;
import com.bolocelta.bbdd.constants.GruposLetra2021;
import com.bolocelta.bbdd.constants.TipoSorteo2021;

public class ModeloIndividualOctavosFinal8Grupos2021 {

	private String nombre = "Octavos de Final";
	private LinkedHashMap<Integer, ModeloIndividualEnfrentamientoOctavosFinal2021> enfrentamientosOctavosFinal = new LinkedHashMap<>();

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedHashMap<Integer, ModeloIndividualEnfrentamientoOctavosFinal2021> getEnfrentamientosOctavosFinal() {
		return enfrentamientosOctavosFinal;
	}

	public void setEnfrentamientosOctavosFinal(LinkedHashMap<Integer, ModeloIndividualEnfrentamientoOctavosFinal2021> enfrentamientosOctavosFinal) {
		this.enfrentamientosOctavosFinal = enfrentamientosOctavosFinal;
	}
	
	public ModeloIndividualOctavosFinal8Grupos2021() {
		this.enfrentamientosOctavosFinal.put(1, new ModeloIndividualEnfrentamientoOctavosFinal2021(CrucesCampeonatos2021.OFA, 1, GruposLetra2021.FASE_I_G_A, GruposLetra2021.FASE_I_G_E, 1, 2, 0, 0, TipoSorteo2021.MONEDA, CrucesCampeonatos2021.CFA));
		this.enfrentamientosOctavosFinal.put(2, new ModeloIndividualEnfrentamientoOctavosFinal2021(CrucesCampeonatos2021.OFB, 1, GruposLetra2021.FASE_I_G_B, GruposLetra2021.FASE_I_G_F, 1, 2, 0, 0, TipoSorteo2021.MONEDA, CrucesCampeonatos2021.CFA));
		this.enfrentamientosOctavosFinal.put(3, new ModeloIndividualEnfrentamientoOctavosFinal2021(CrucesCampeonatos2021.OFC, 1, GruposLetra2021.FASE_I_G_C, GruposLetra2021.FASE_I_G_G, 1, 2, 0, 0, TipoSorteo2021.MONEDA, CrucesCampeonatos2021.CFB));
		this.enfrentamientosOctavosFinal.put(4, new ModeloIndividualEnfrentamientoOctavosFinal2021(CrucesCampeonatos2021.OFD, 1, GruposLetra2021.FASE_I_G_D, GruposLetra2021.FASE_I_G_H, 1, 2, 0, 0, TipoSorteo2021.MONEDA, CrucesCampeonatos2021.CFB));
		this.enfrentamientosOctavosFinal.put(5, new ModeloIndividualEnfrentamientoOctavosFinal2021(CrucesCampeonatos2021.OFE, 1, GruposLetra2021.FASE_I_G_E, GruposLetra2021.FASE_I_G_A, 1, 2, 0, 0, TipoSorteo2021.MONEDA, CrucesCampeonatos2021.CFC));
		this.enfrentamientosOctavosFinal.put(6, new ModeloIndividualEnfrentamientoOctavosFinal2021(CrucesCampeonatos2021.OFF, 1, GruposLetra2021.FASE_I_G_F, GruposLetra2021.FASE_I_G_B, 1, 2, 0, 0, TipoSorteo2021.MONEDA, CrucesCampeonatos2021.CFC));
		this.enfrentamientosOctavosFinal.put(7, new ModeloIndividualEnfrentamientoOctavosFinal2021(CrucesCampeonatos2021.OFG, 1, GruposLetra2021.FASE_I_G_G, GruposLetra2021.FASE_I_G_C, 1, 2, 0, 0, TipoSorteo2021.MONEDA, CrucesCampeonatos2021.CFD));
		this.enfrentamientosOctavosFinal.put(8, new ModeloIndividualEnfrentamientoOctavosFinal2021(CrucesCampeonatos2021.OFH, 1, GruposLetra2021.FASE_I_G_H, GruposLetra2021.FASE_I_G_D, 1, 2, 0, 0, TipoSorteo2021.MONEDA, CrucesCampeonatos2021.CFD));
	}

}
