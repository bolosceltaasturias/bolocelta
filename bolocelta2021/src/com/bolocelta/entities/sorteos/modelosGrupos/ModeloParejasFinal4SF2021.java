package com.bolocelta.entities.sorteos.modelosGrupos;

import java.util.LinkedHashMap;

import com.bolocelta.bbdd.constants.CrucesCampeonatos2021;
import com.bolocelta.bbdd.constants.TipoSorteo2021;

public class ModeloParejasFinal4SF2021 {

	private String nombre = "Final";
	private LinkedHashMap<Integer, ModeloParejasEnfrentamientoFinal2021> enfrentamientosFinal = new LinkedHashMap<>();

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedHashMap<Integer, ModeloParejasEnfrentamientoFinal2021> getEnfrentamientosFinal() {
		return enfrentamientosFinal;
	}

	public void setEnfrentamientosFinal(LinkedHashMap<Integer, ModeloParejasEnfrentamientoFinal2021> enfrentamientosFinal) {
		this.enfrentamientosFinal = enfrentamientosFinal;
	}
	
	public ModeloParejasFinal4SF2021() {
		this.enfrentamientosFinal.put(1, new ModeloParejasEnfrentamientoFinal2021(CrucesCampeonatos2021.FF1, 1, CrucesCampeonatos2021.SF1, CrucesCampeonatos2021.SF2, 0, 0, 0, 0, TipoSorteo2021.MONEDA, null));
	}

}
