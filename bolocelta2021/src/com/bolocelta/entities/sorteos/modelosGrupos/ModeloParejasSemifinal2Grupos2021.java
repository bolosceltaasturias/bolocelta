package com.bolocelta.entities.sorteos.modelosGrupos;

import java.util.LinkedHashMap;

import com.bolocelta.bbdd.constants.CrucesCampeonatos2021;
import com.bolocelta.bbdd.constants.GruposLetra2021;
import com.bolocelta.bbdd.constants.TipoSorteo2021;

public class ModeloParejasSemifinal2Grupos2021 {

	private String nombre = "Semifinal";
	private LinkedHashMap<Integer, ModeloParejasEnfrentamientoSemiFinal2021> enfrentamientosSemiFinal = new LinkedHashMap<>();

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedHashMap<Integer, ModeloParejasEnfrentamientoSemiFinal2021> getEnfrentamientosSemiFinal() {
		return enfrentamientosSemiFinal;
	}

	public void getEnfrentamientosSemifinal(LinkedHashMap<Integer, ModeloParejasEnfrentamientoSemiFinal2021> enfrentamientosSemiFinal) {
		this.enfrentamientosSemiFinal = enfrentamientosSemiFinal;
	}
	
	public ModeloParejasSemifinal2Grupos2021() {
		this.enfrentamientosSemiFinal.put(1, new ModeloParejasEnfrentamientoSemiFinal2021(CrucesCampeonatos2021.SF1, 1, GruposLetra2021.FASE_I_G_A, GruposLetra2021.FASE_I_G_B, 1, 2, 0, 0, TipoSorteo2021.MONEDA, CrucesCampeonatos2021.FF1));
		this.enfrentamientosSemiFinal.put(2, new ModeloParejasEnfrentamientoSemiFinal2021(CrucesCampeonatos2021.SF2, 1, GruposLetra2021.FASE_I_G_B, GruposLetra2021.FASE_I_G_A, 1, 2, 0, 0, TipoSorteo2021.MONEDA, CrucesCampeonatos2021.FF1));
	}

}
