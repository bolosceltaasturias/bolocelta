package com.bolocelta.entities.sorteos.modelosGrupos;

import java.util.LinkedHashMap;

import com.bolocelta.bbdd.constants.TipoSorteo2021;

public class ModeloIndividualGrupos72021 {

	private String nombre = "Grupo 7";
	private LinkedHashMap<Integer, ModeloIndividualEnfrentamientoGruposFaseI2021> enfrentamientosGrupo = new LinkedHashMap<>();

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedHashMap<Integer, ModeloIndividualEnfrentamientoGruposFaseI2021> getEnfrentamientosGrupo() {
		return enfrentamientosGrupo;
	}

	public void setEnfrentamientosGrupo(LinkedHashMap<Integer, ModeloIndividualEnfrentamientoGruposFaseI2021> enfrentamientosGrupo) {
		this.enfrentamientosGrupo = enfrentamientosGrupo;
	}
	
	public ModeloIndividualGrupos72021() {
		this.enfrentamientosGrupo.put(1, new ModeloIndividualEnfrentamientoGruposFaseI2021(1, 2, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(2, new ModeloIndividualEnfrentamientoGruposFaseI2021(3, 4, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(3, new ModeloIndividualEnfrentamientoGruposFaseI2021(5, 6, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(4, new ModeloIndividualEnfrentamientoGruposFaseI2021(7, 1, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(5, new ModeloIndividualEnfrentamientoGruposFaseI2021(2, 3, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(6, new ModeloIndividualEnfrentamientoGruposFaseI2021(4, 5, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(7, new ModeloIndividualEnfrentamientoGruposFaseI2021(6, 7, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(8, new ModeloIndividualEnfrentamientoGruposFaseI2021(3, 1, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(9, new ModeloIndividualEnfrentamientoGruposFaseI2021(4, 2, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(10, new ModeloIndividualEnfrentamientoGruposFaseI2021(7, 5, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(11, new ModeloIndividualEnfrentamientoGruposFaseI2021(3, 6, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(12, new ModeloIndividualEnfrentamientoGruposFaseI2021(1, 4, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(13, new ModeloIndividualEnfrentamientoGruposFaseI2021(2, 7, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(14, new ModeloIndividualEnfrentamientoGruposFaseI2021(5, 3, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(15, new ModeloIndividualEnfrentamientoGruposFaseI2021(1, 6, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(16, new ModeloIndividualEnfrentamientoGruposFaseI2021(4, 7, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(17, new ModeloIndividualEnfrentamientoGruposFaseI2021(2, 5, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(18, new ModeloIndividualEnfrentamientoGruposFaseI2021(6, 4, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(19, new ModeloIndividualEnfrentamientoGruposFaseI2021(5, 1, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(20, new ModeloIndividualEnfrentamientoGruposFaseI2021(7, 3, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(21, new ModeloIndividualEnfrentamientoGruposFaseI2021(6, 2, TipoSorteo2021.LOCAL));
	}

}
