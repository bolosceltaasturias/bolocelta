package com.bolocelta.entities.sorteos.modelosGrupos;

public class ModeloParejasEnfrentamientoFinalConsolacion2021 {

	private Integer faseAnterior;
	private String grupoProcedenciaPareja1;
	private String grupoProcedenciaPareja2;
	private Integer posicionProcedenciaPareja1;
	private Integer posicionProcedenciaPareja2;
	private Integer pareja1;
	private Integer pareja2;
	private String sorteo;
	private String cruceCF;
	private String idCruce;

	public ModeloParejasEnfrentamientoFinalConsolacion2021(String idCruce, Integer faseAnterior, String grupoProcedenciaPareja1,
			String grupoProcedenciaPareja2, Integer posicionProcedenciaPareja1, Integer posicionProcedenciaPareja2,
			Integer pareja1, Integer pareja2, String sorteo, String cruceCF) {
		this.idCruce = idCruce;
		this.faseAnterior = faseAnterior;
		this.grupoProcedenciaPareja1 = grupoProcedenciaPareja1;
		this.grupoProcedenciaPareja2 = grupoProcedenciaPareja2;
		this.posicionProcedenciaPareja1 = posicionProcedenciaPareja1;
		this.posicionProcedenciaPareja2 = posicionProcedenciaPareja2;
		this.pareja1 = pareja1;
		this.pareja2 = pareja2;
		this.sorteo = sorteo;
		this.cruceCF = cruceCF;
	}

	public Integer getPareja1() {
		return pareja1;
	}

	public void setPareja1(Integer pareja1) {
		this.pareja1 = pareja1;
	}

	public Integer getPareja2() {
		return pareja2;
	}

	public void setPareja2(Integer pareja2) {
		this.pareja2 = pareja2;
	}

	public String getSorteo() {
		return sorteo;
	}

	public void setSorteo(String sorteo) {
		this.sorteo = sorteo;
	}

	public Integer getFaseAnterior() {
		return faseAnterior;
	}

	public void setFaseAnterior(Integer faseAnterior) {
		this.faseAnterior = faseAnterior;
	}

	public String getGrupoProcedenciaPareja1() {
		return grupoProcedenciaPareja1;
	}

	public void setGrupoProcedenciaPareja1(String grupoProcedenciaPareja1) {
		this.grupoProcedenciaPareja1 = grupoProcedenciaPareja1;
	}

	public String getGrupoProcedenciaPareja2() {
		return grupoProcedenciaPareja2;
	}

	public void setGrupoProcedenciaPareja2(String grupoProcedenciaPareja2) {
		this.grupoProcedenciaPareja2 = grupoProcedenciaPareja2;
	}

	public Integer getPosicionProcedenciaPareja1() {
		return posicionProcedenciaPareja1;
	}

	public void setPosicionProcedenciaPareja1(Integer posicionProcedenciaPareja1) {
		this.posicionProcedenciaPareja1 = posicionProcedenciaPareja1;
	}

	public Integer getPosicionProcedenciaPareja2() {
		return posicionProcedenciaPareja2;
	}

	public void setPosicionProcedenciaPareja2(Integer posicionProcedenciaPareja2) {
		this.posicionProcedenciaPareja2 = posicionProcedenciaPareja2;
	}

	public String getCruceCF() {
		return cruceCF;
	}

	public void setCruceCF(String cruceCF) {
		this.cruceCF = cruceCF;
	}

	public String getIdCruce() {
		return idCruce;
	}

	public void setIdCruce(String idCruce) {
		this.idCruce = idCruce;
	}

}
