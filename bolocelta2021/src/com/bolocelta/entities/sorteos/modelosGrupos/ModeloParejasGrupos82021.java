package com.bolocelta.entities.sorteos.modelosGrupos;

import java.util.LinkedHashMap;

import com.bolocelta.bbdd.constants.TipoSorteo2021;

public class ModeloParejasGrupos82021 {

	private String nombre = "Grupos 8";
	private LinkedHashMap<Integer, ModeloParejasEnfrentamientoGruposFaseI2021> enfrentamientosGrupo = new LinkedHashMap<>();

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedHashMap<Integer, ModeloParejasEnfrentamientoGruposFaseI2021> getEnfrentamientosGrupo() {
		return enfrentamientosGrupo;
	}

	public void setEnfrentamientosGrupo(LinkedHashMap<Integer, ModeloParejasEnfrentamientoGruposFaseI2021> enfrentamientosGrupo) {
		this.enfrentamientosGrupo = enfrentamientosGrupo;
	}
	
	public ModeloParejasGrupos82021() {
		this.enfrentamientosGrupo.put(1, new ModeloParejasEnfrentamientoGruposFaseI2021(1, 2, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(2, new ModeloParejasEnfrentamientoGruposFaseI2021(3, 4, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(3, new ModeloParejasEnfrentamientoGruposFaseI2021(5, 6, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(4, new ModeloParejasEnfrentamientoGruposFaseI2021(7, 8, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(5, new ModeloParejasEnfrentamientoGruposFaseI2021(4, 1, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(6, new ModeloParejasEnfrentamientoGruposFaseI2021(2, 3, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(7, new ModeloParejasEnfrentamientoGruposFaseI2021(6, 7, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(8, new ModeloParejasEnfrentamientoGruposFaseI2021(8, 5, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(9, new ModeloParejasEnfrentamientoGruposFaseI2021(1, 6, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(10, new ModeloParejasEnfrentamientoGruposFaseI2021(7, 4, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(11, new ModeloParejasEnfrentamientoGruposFaseI2021(5, 2, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(12, new ModeloParejasEnfrentamientoGruposFaseI2021(3, 8, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(13, new ModeloParejasEnfrentamientoGruposFaseI2021(2, 7, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(14, new ModeloParejasEnfrentamientoGruposFaseI2021(4, 5, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(15, new ModeloParejasEnfrentamientoGruposFaseI2021(6, 3, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(16, new ModeloParejasEnfrentamientoGruposFaseI2021(8, 1, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(17, new ModeloParejasEnfrentamientoGruposFaseI2021(5, 7, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(18, new ModeloParejasEnfrentamientoGruposFaseI2021(2, 4, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(19, new ModeloParejasEnfrentamientoGruposFaseI2021(6, 8, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(20, new ModeloParejasEnfrentamientoGruposFaseI2021(1, 3, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(21, new ModeloParejasEnfrentamientoGruposFaseI2021(4, 6, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(22, new ModeloParejasEnfrentamientoGruposFaseI2021(8, 2, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(23, new ModeloParejasEnfrentamientoGruposFaseI2021(3, 5, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(24, new ModeloParejasEnfrentamientoGruposFaseI2021(7, 1, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(25, new ModeloParejasEnfrentamientoGruposFaseI2021(2, 6, TipoSorteo2021.MONEDA));
		this.enfrentamientosGrupo.put(26, new ModeloParejasEnfrentamientoGruposFaseI2021(4, 8, TipoSorteo2021.MONEDA));
		this.enfrentamientosGrupo.put(27, new ModeloParejasEnfrentamientoGruposFaseI2021(1, 5, TipoSorteo2021.MONEDA));
		this.enfrentamientosGrupo.put(28, new ModeloParejasEnfrentamientoGruposFaseI2021(3, 7, TipoSorteo2021.MONEDA));
	}

}
