package com.bolocelta.entities.sorteos.modelosGrupos;

import java.util.LinkedHashMap;

import com.bolocelta.bbdd.constants.TipoSorteo2021;

public class ModeloParejasGrupos42021 {

	private String nombre = "Grupo 4";
	private LinkedHashMap<Integer, ModeloParejasEnfrentamientoGruposFaseI2021> enfrentamientosGrupo = new LinkedHashMap<>();

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedHashMap<Integer, ModeloParejasEnfrentamientoGruposFaseI2021> getEnfrentamientosGrupo() {
		return enfrentamientosGrupo;
	}

	public void setEnfrentamientosGrupo(LinkedHashMap<Integer, ModeloParejasEnfrentamientoGruposFaseI2021> enfrentamientosGrupo) {
		this.enfrentamientosGrupo = enfrentamientosGrupo;
	}
	
	public ModeloParejasGrupos42021() {
		this.enfrentamientosGrupo.put(1, new ModeloParejasEnfrentamientoGruposFaseI2021(1, 2, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(2, new ModeloParejasEnfrentamientoGruposFaseI2021(3, 4, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(3, new ModeloParejasEnfrentamientoGruposFaseI2021(4, 1, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(4, new ModeloParejasEnfrentamientoGruposFaseI2021(2, 3, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(5, new ModeloParejasEnfrentamientoGruposFaseI2021(1, 3, TipoSorteo2021.MONEDA));
		this.enfrentamientosGrupo.put(6, new ModeloParejasEnfrentamientoGruposFaseI2021(2, 4, TipoSorteo2021.MONEDA));
	}

}
