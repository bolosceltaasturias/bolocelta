package com.bolocelta.entities.sorteos.modelosGrupos;

import java.util.LinkedHashMap;

import com.bolocelta.bbdd.constants.TipoSorteo2021;

public class ModeloIndividualGrupos52021 {

	private String nombre = "Grupo 5";
	private LinkedHashMap<Integer, ModeloIndividualEnfrentamientoGruposFaseI2021> enfrentamientosGrupo = new LinkedHashMap<>();

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedHashMap<Integer, ModeloIndividualEnfrentamientoGruposFaseI2021> getEnfrentamientosGrupo() {
		return enfrentamientosGrupo;
	}

	public void setEnfrentamientosGrupo(LinkedHashMap<Integer, ModeloIndividualEnfrentamientoGruposFaseI2021> enfrentamientosGrupo) {
		this.enfrentamientosGrupo = enfrentamientosGrupo;
	}
	
	public ModeloIndividualGrupos52021() {
		this.enfrentamientosGrupo.put(1, new ModeloIndividualEnfrentamientoGruposFaseI2021(1, 2, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(2, new ModeloIndividualEnfrentamientoGruposFaseI2021(3, 4, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(3, new ModeloIndividualEnfrentamientoGruposFaseI2021(5, 1, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(4, new ModeloIndividualEnfrentamientoGruposFaseI2021(2, 3, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(5, new ModeloIndividualEnfrentamientoGruposFaseI2021(4, 5, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(6, new ModeloIndividualEnfrentamientoGruposFaseI2021(1, 3, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(7, new ModeloIndividualEnfrentamientoGruposFaseI2021(2, 4, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(8, new ModeloIndividualEnfrentamientoGruposFaseI2021(3, 5, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(9, new ModeloIndividualEnfrentamientoGruposFaseI2021(4, 1, TipoSorteo2021.LOCAL));
		this.enfrentamientosGrupo.put(10, new ModeloIndividualEnfrentamientoGruposFaseI2021(5, 2, TipoSorteo2021.LOCAL));
	}

}
