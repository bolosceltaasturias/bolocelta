package com.bolocelta.entities.sorteos.modelosGrupos;

import java.util.LinkedHashMap;

import com.bolocelta.bbdd.constants.CrucesCampeonatos2021;
import com.bolocelta.bbdd.constants.GruposLetra2021;
import com.bolocelta.bbdd.constants.TipoSorteo2021;

public class ModeloIndividualCuartosFinal4OFClasifican32021 {

	private String nombre = "Cuartos de Final";
	private LinkedHashMap<Integer, ModeloIndividualEnfrentamientoCuartosFinal2021> enfrentamientosCuartosFinal = new LinkedHashMap<>();

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedHashMap<Integer, ModeloIndividualEnfrentamientoCuartosFinal2021> getEnfrentamientosCuartosFinal() {
		return enfrentamientosCuartosFinal;
	}

	public void setEnfrentamientosCuartosFinal(LinkedHashMap<Integer, ModeloIndividualEnfrentamientoCuartosFinal2021> enfrentamientosCuartosFinal) {
		this.enfrentamientosCuartosFinal = enfrentamientosCuartosFinal;
	}
	
	public ModeloIndividualCuartosFinal4OFClasifican32021() {
		this.enfrentamientosCuartosFinal.put(1, new ModeloIndividualEnfrentamientoCuartosFinal2021(CrucesCampeonatos2021.CFA, 1, GruposLetra2021.FASE_I_G_A, CrucesCampeonatos2021.OFA, 1, 0, 0, 0, TipoSorteo2021.MONEDA, CrucesCampeonatos2021.SF1));
		this.enfrentamientosCuartosFinal.put(2, new ModeloIndividualEnfrentamientoCuartosFinal2021(CrucesCampeonatos2021.CFB, 1, GruposLetra2021.FASE_I_G_B, CrucesCampeonatos2021.OFB, 1, 0, 0, 0, TipoSorteo2021.MONEDA, CrucesCampeonatos2021.SF1));
		this.enfrentamientosCuartosFinal.put(3, new ModeloIndividualEnfrentamientoCuartosFinal2021(CrucesCampeonatos2021.CFC, 1, GruposLetra2021.FASE_I_G_C, CrucesCampeonatos2021.OFC, 1, 0, 0, 0, TipoSorteo2021.MONEDA, CrucesCampeonatos2021.SF2));
		this.enfrentamientosCuartosFinal.put(4, new ModeloIndividualEnfrentamientoCuartosFinal2021(CrucesCampeonatos2021.CFD, 1, GruposLetra2021.FASE_I_G_D, CrucesCampeonatos2021.OFD, 1, 0, 0, 0, TipoSorteo2021.MONEDA, CrucesCampeonatos2021.SF2));
	}

}
