package com.bolocelta.entities.sorteos.individual;

import com.bolocelta.entities.Jugadores2021;

public class ClasificacionFaseI2021 {

	private Integer id;
	private Integer categoriaId;
	private String grupo;
	private Integer jugadorId;
	private Jugadores2021 jugador;
	private Integer pj;
	private Integer pg;
	private Integer pe;
	private Integer pp;
	private Integer pf;
	private Integer pc;
	private Integer pt;
	private Long rowNum;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCategoriaId() {
		return categoriaId;
	}

	public void setCategoriaId(Integer categoriaId) {
		this.categoriaId = categoriaId;
	}

	public String getGrupo() {
		return grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

	public Integer getJugadorId() {
		return jugadorId;
	}

	public void setJugadorId(Integer jugadorId) {
		this.jugadorId = jugadorId;
	}

	public Integer getPj() {
		return pj;
	}

	public void setPj(Integer pj) {
		this.pj = pj;
	}

	public Integer getPg() {
		return pg;
	}

	public void setPg(Integer pg) {
		this.pg = pg;
	}

	public Integer getPe() {
		return pe;
	}

	public void setPe(Integer pe) {
		this.pe = pe;
	}

	public Integer getPp() {
		return pp;
	}

	public void setPp(Integer pp) {
		this.pp = pp;
	}

	public Integer getPf() {
		return pf;
	}

	public void setPf(Integer pf) {
		this.pf = pf;
	}

	public Integer getPc() {
		return pc;
	}

	public void setPc(Integer pc) {
		this.pc = pc;
	}

	public Integer getPt() {
		return pt;
	}

	public void setPt(Integer pt) {
		this.pt = pt;
	}

	public Long getRowNum() {
		return rowNum;
	}

	public void setRowNum(Long rowNum) {
		this.rowNum = rowNum;
	}
	
	public String getInsertRow(){
		return (this.id + ";" +
					this.categoriaId + ";" +
					this.grupo + ";" +
					this.jugadorId + ";" +
					this.pj + ";" +
					this.pg + ";" +
					this.pe + ";" +
					this.pp + ";" +
					this.pf + ";" +
					this.pc + ";" +
					this.pt
					);
	}

	public Jugadores2021 getJugador() {
		return jugador;
	}

	public void setJugador(Jugadores2021 jugador) {
		this.jugador = jugador;
	}
	
	public String getKey(){
		return this.id + "-" + this.categoriaId + "-" + this.grupo;
	}

}
