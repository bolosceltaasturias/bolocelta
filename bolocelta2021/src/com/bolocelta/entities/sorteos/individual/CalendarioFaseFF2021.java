package com.bolocelta.entities.sorteos.individual;

import com.bolocelta.bbdd.constants.Activo2021;
import com.bolocelta.entities.Boleras2021;
import com.bolocelta.entities.Jugadores2021;

public class CalendarioFaseFF2021 {

	private Integer id;
	private Integer categoriaId;
	private String idCruce;
	private Integer numeroEnfrentamiento;
	private Jugadores2021 jugador1;
	private Integer jugador1Id;
	private Integer juegosJugador1P1;
	private Integer juegosJugador1P2;
	private Integer juegosJugador1P3;
	private Integer juegosJugador2P1;
	private Integer juegosJugador2P2;
	private Integer juegosJugador2P3;
	private Integer jugador2Id;
	private Jugadores2021 jugador2;
	private Integer orden;
	private String cruceCF;
	private String activo;
	private Long rowNum;
	private String inicia;
	private Integer faseAnterior;
	private String grupoProcedenciaJugador1;
	private String grupoProcedenciaJugador2;
	private Integer posicionProcedenciaJugador1;
	private Integer posicionProcedenciaJugador2;
	private Integer boleraId;
	private Boleras2021 bolera;
	private String requipo1;
	private String requipo2;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCategoriaId() {
		return categoriaId;
	}

	public void setCategoriaId(Integer categoriaId) {
		this.categoriaId = categoriaId;
	}

	public Integer getNumeroEnfrentamiento() {
		return numeroEnfrentamiento;
	}

	public void setNumeroEnfrentamiento(Integer numeroEnfrentamiento) {
		this.numeroEnfrentamiento = numeroEnfrentamiento;
	}

	public Integer getJugador1Id() {
		return jugador1Id;
	}

	public void setJugador1Id(Integer jugador1Id) {
		this.jugador1Id = jugador1Id;
	}

	public Integer getJuegosJugador1P1() {
		return juegosJugador1P1;
	}

	public void setJuegosJugador1P1(Integer juegosJugador1P1) {
		this.juegosJugador1P1 = juegosJugador1P1;
	}

	public Integer getJuegosJugador1P2() {
		return juegosJugador1P2;
	}

	public void setJuegosJugador1P2(Integer juegosJugador1P2) {
		this.juegosJugador1P2 = juegosJugador1P2;
	}

	public Integer getJuegosJugador1P3() {
		return juegosJugador1P3;
	}

	public void setJuegosJugador1P3(Integer juegosJugador1P3) {
		this.juegosJugador1P3 = juegosJugador1P3;
	}

	public Integer getJuegosJugador2P1() {
		return juegosJugador2P1;
	}

	public void setJuegosJugador2P1(Integer juegosJugador2P1) {
		this.juegosJugador2P1 = juegosJugador2P1;
	}

	public Integer getJuegosJugador2P2() {
		return juegosJugador2P2;
	}

	public void setJuegosJugador2P2(Integer juegosJugador2P2) {
		this.juegosJugador2P2 = juegosJugador2P2;
	}

	public Integer getJuegosJugador2P3() {
		return juegosJugador2P3;
	}

	public void setJuegosJugador2P3(Integer juegosJugador2P3) {
		this.juegosJugador2P3 = juegosJugador2P3;
	}

	public Integer getJugador2Id() {
		return jugador2Id;
	}

	public void setJugador2Id(Integer jugador2Id) {
		this.jugador2Id = jugador2Id;
	}

	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}

	public String getCruceCF() {
		return cruceCF;
	}

	public void setCruceCF(String cruceCF) {
		this.cruceCF = cruceCF;
	}

	public String getActivo() {
		return activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	public Long getRowNum() {
		return rowNum;
	}

	public void setRowNum(Long rowNum) {
		this.rowNum = rowNum;
	}

	public Integer getFaseAnterior() {
		return faseAnterior;
	}

	public void setFaseAnterior(Integer faseAnterior) {
		this.faseAnterior = faseAnterior;
	}

	public String getGrupoProcedenciaJugador1() {
		return grupoProcedenciaJugador1;
	}

	public void setGrupoProcedenciaJugador1(String grupoProcedenciaJugador1) {
		this.grupoProcedenciaJugador1 = grupoProcedenciaJugador1;
	}

	public String getGrupoProcedenciaJugador2() {
		return grupoProcedenciaJugador2;
	}

	public void setGrupoProcedenciaJugador2(String grupoProcedenciaJugador2) {
		this.grupoProcedenciaJugador2 = grupoProcedenciaJugador2;
	}

	public Integer getPosicionProcedenciaJugador1() {
		return posicionProcedenciaJugador1;
	}

	public void setPosicionProcedenciaJugador1(Integer posicionProcedenciaJugador1) {
		this.posicionProcedenciaJugador1 = posicionProcedenciaJugador1;
	}

	public Integer getPosicionProcedenciaJugador2() {
		return posicionProcedenciaJugador2;
	}

	public void setPosicionProcedenciaJugador2(Integer posicionProcedenciaJugador2) {
		this.posicionProcedenciaJugador2 = posicionProcedenciaJugador2;
	}

	public String getInicia() {
		return inicia;
	}

	public void setInicia(String inicia) {
		this.inicia = inicia;
	}

	public Integer getBoleraId() {
		return boleraId;
	}

	public void setBoleraId(Integer boleraId) {
		this.boleraId = boleraId;
	}

	public String getIdCruce() {
		return idCruce;
	}

	public void setIdCruce(String idCruce) {
		this.idCruce = idCruce;
	}
	
	public String getInsertRow(){
		return (this.id + ";" +
					this.categoriaId + ";" +
					this.idCruce + ";" +
					this.numeroEnfrentamiento + ";" +
					this.jugador1Id + ";" +
					this.juegosJugador1P1 + ";" +
					this.juegosJugador1P2 + ";" +
					this.juegosJugador1P3 + ";" +
					this.juegosJugador2P1 + ";" +
					this.juegosJugador2P2 + ";" +
					this.juegosJugador2P3 + ";" +
					this.jugador2Id + ";" +
					this.orden + ";" +
					this.cruceCF + ";" +
					this.activo + ";" +
					this.inicia + ";" +
					this.faseAnterior + ";" +
					this.grupoProcedenciaJugador1 + ";" +
					this.grupoProcedenciaJugador2 + ";" +
					this.posicionProcedenciaJugador1 + ";" +
					this.posicionProcedenciaJugador2 + ";" +
					this.boleraId
					);
	}

	public Jugadores2021 getJugador1() {
		return jugador1;
	}

	public void setJugador1(Jugadores2021 jugador1) {
		this.jugador1 = jugador1;
	}

	public Jugadores2021 getJugador2() {
		return jugador2;
	}

	public void setJugador2(Jugadores2021 jugador2) {
		this.jugador2 = jugador2;
	}

	public Boleras2021 getBolera() {
		return bolera;
	}

	public void setBolera(Boleras2021 bolera) {
		this.bolera = bolera;
	}

	public String getRequipo1() {
		return requipo1;
	}

	public void setRequipo1(String requipo1) {
		this.requipo1 = requipo1;
	}

	public String getRequipo2() {
		return requipo2;
	}

	public void setRequipo2(String requipo2) {
		this.requipo2 = requipo2;
	}
	
	public String getOrigenJugador1(){
		return "SF: " + this.grupoProcedenciaJugador1 + (this.posicionProcedenciaJugador1 > 0 ? this.posicionProcedenciaJugador1 : "");
	}

	public String getOrigenJugador2(){
		return "SF: " + this.grupoProcedenciaJugador2 + (this.posicionProcedenciaJugador2 > 0 ? this.posicionProcedenciaJugador2 : "");
	}
	
	public String getPartida1(){
		return this.juegosJugador1P1.toString() + " - " + this.juegosJugador2P1.toString();
	}
	
	public String getPartida2(){
		return this.juegosJugador1P2.toString() + " - " + this.juegosJugador2P2.toString();
	}
	
	public String getPartida3(){
		return this.juegosJugador1P3.toString() + " - " + this.juegosJugador2P3.toString();
	}
	
	public boolean isModificable(){
		if(this.activo != null && this.activo.equals(Activo2021.SI)){
			return true;
		}
		return false;
	}
	
	public boolean isJugadoresEnFase(){
		if(this.jugador1Id > 0 && this.jugador2Id > 0){
			return true;
		}
		return false;
	}
	
	public Integer isGanaJugador(){
		//Calcular victoria, empate y derrota
    	Integer resultadoPartida1 =  getJuegosJugador1P1() - getJuegosJugador2P1();
    	Integer resultadoPartida2 =  getJuegosJugador1P2() - getJuegosJugador2P2();
    	Integer resultadoPartida3 =  getJuegosJugador1P3() - getJuegosJugador2P3();
    	
    	//Sin Jugar
    	if(resultadoPartida1 == 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
    		return 0;
    	}else
    	//Ganador 1 solo una partida
    	if(resultadoPartida1 > 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
    		return 1;
    	}else 
    	//Ganador 2 solo una partida
    	if(resultadoPartida1 < 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
    		return 2;
    	}else 
    	//Ganador 1 dos partidas
    	if( (resultadoPartida1 > 0 && resultadoPartida2 > 0) ||
			(resultadoPartida1 > 0 && resultadoPartida3 > 0) ||
			(resultadoPartida2 > 0 && resultadoPartida3 > 0)){
    		return 1;
    	}else
    	//Ganador 2 dos partidas
    	if( (resultadoPartida1 < 0 && resultadoPartida2 < 0) ||
			(resultadoPartida1 < 0 && resultadoPartida3 < 0) ||
			(resultadoPartida2 < 0 && resultadoPartida3 < 0)){
    		return 2;
    	}else{
    		return 0;
    	}
	}
	
	public String getPartidasJugador1(){
		return this.juegosJugador1P1.toString() + " - " + this.juegosJugador1P2.toString() + " - " + this.juegosJugador1P3.toString();
	}
	
	public String getPartidasJugador2(){
		return this.juegosJugador2P1.toString() + " - " + this.juegosJugador2P2.toString() + " - " + this.juegosJugador2P3.toString();
	}
	
	public boolean isPartida1Jugada(){
		if(this.juegosJugador1P1 > 0 || this.juegosJugador2P1 > 0){
			return true;
		}
		return false;
	}
	
	public boolean isPartida2Jugada(){
		if(this.juegosJugador1P2 > 0 || this.juegosJugador2P2 > 0){
			return true;
		}
		return false;
	}
	
	public boolean isPartida3Jugada(){
		if(this.juegosJugador1P3 > 0 || this.juegosJugador2P3 > 0){
			return true;
		}
		return false;
	}
	
	public Integer getPartidasJugadas(){
		Integer partidasJugadas = 0;
		if(isPartida1Jugada()) partidasJugadas++;
		if(isPartida2Jugada()) partidasJugadas++;
		if(isPartida3Jugada()) partidasJugadas++;
		return partidasJugadas;
	}
	
	public boolean isResultadoPartidas1(){
		if(isPartida1Jugada()){
			if(isPartida1FOtoFFGanaJugador() > 0){
				return true;
			}
		}
		return false;
	}
	
	public boolean isResultadoPartidas2(){
		if(isPartida1Jugada() && isPartida2Jugada() && !isPartida3Jugada()){
			if(
				(isPartida1FOtoFFGanaJugador() > 0 && isPartida2FOtoFFGanaJugador() > 0) &&
				(isPartida1FOtoFFGanaJugador() == isPartida2FOtoFFGanaJugador())
			){
				return true;
			}
		}else if(isPartida1Jugada() && isPartida2Jugada() && isPartida3Jugada()){
			if(
					(isPartida1FOtoFFGanaJugador() > 0 && isPartida2FOtoFFGanaJugador() > 0 && isPartida3FOtoFFGanaJugador() > 0) &&
					(isPartida1FOtoFFGanaJugador() == isPartida2FOtoFFGanaJugador() || 
					 isPartida1FOtoFFGanaJugador() == isPartida3FOtoFFGanaJugador() || 
					 isPartida2FOtoFFGanaJugador() == isPartida3FOtoFFGanaJugador())
			){
				return true;
			}
		}
		return false;
	}
	
	public Integer isPartida1FOtoFFGanaJugador(){
		//Calcular victoria, empate y derrota
    	Integer resultadoPartida1 =  getJuegosJugador1P1() - getJuegosJugador2P1();
    	
    	//Sin Jugar
    	if(resultadoPartida1 == 0){
    		return 0;
    	}else
    	//Ganador 1 solo una partida
    	if(resultadoPartida1 > 0){
    		return 1;
    	}else 
    	//Ganador 2 solo una partida
    	if(resultadoPartida1 < 0){
    		return 2;
    	}
    	return 0;
	}
	
	public Integer isPartida2FOtoFFGanaJugador(){
		//Calcular victoria, empate y derrota
    	Integer resultadoPartida2 =  getJuegosJugador1P2() - getJuegosJugador2P2();
    	
    	//Sin Jugar
    	if(resultadoPartida2 == 0){
    		return 0;
    	}else
    	//Ganador 1 solo una partida
    	if(resultadoPartida2 > 0){
    		return 1;
    	}else 
    	//Ganador 2 solo una partida
    	if(resultadoPartida2 < 0){
    		return 2;
    	}
    	return 0;
	}
	
	public Integer isPartida3FOtoFFGanaJugador(){
		//Calcular victoria, empate y derrota
    	Integer resultadoPartida3 =  getJuegosJugador1P3() - getJuegosJugador2P3();
    	
    	//Sin Jugar
    	if(resultadoPartida3 == 0){
    		return 0;
    	}else
    	//Ganador 1 solo una partida
    	if(resultadoPartida3 > 0){
    		return 1;
    	}else 
    	//Ganador 2 solo una partida
    	if(resultadoPartida3 < 0){
    		return 2;
    	}
    	return 0;
	}

}
