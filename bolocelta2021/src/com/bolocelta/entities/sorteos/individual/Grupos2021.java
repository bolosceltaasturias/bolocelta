package com.bolocelta.entities.sorteos.individual;

import java.util.LinkedHashMap;
import java.util.Map.Entry;

import com.bolocelta.entities.CampeonatoEquiposCalendario2021;
import com.bolocelta.entities.ParticipantesIndividual2021;

public class Grupos2021 {

	private String id;
	private Integer categoriaId;
	private Integer boleraId;
	private String fecha;
	private String hora;
	private Integer jugadores;
	private LinkedHashMap<Integer, ParticipantesIndividual2021> jugadoresGrupoList = new LinkedHashMap();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getCategoriaId() {
		return categoriaId;
	}

	public void setCategoriaId(Integer categoriaId) {
		this.categoriaId = categoriaId;
	}

	public Integer getBoleraId() {
		return boleraId;
	}

	public void setBoleraId(Integer boleraId) {
		this.boleraId = boleraId;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public Integer getJugadores() {
		return jugadores;
	}

	public void setJugadores(Integer jugadores) {
		this.jugadores = jugadores;
	}

	public LinkedHashMap<Integer, ParticipantesIndividual2021> getJugadoresGrupoList() {
		return jugadoresGrupoList;
	}

	public void setJugadoresGrupoList(LinkedHashMap<Integer, ParticipantesIndividual2021> jugadoresGrupoList) {
		this.jugadoresGrupoList = jugadoresGrupoList;
	}
	
	public boolean isExisteJugadorMismoEquipo(Integer idEquipo, LinkedHashMap<Integer, Integer> equipoMasJugadoresQueGrupos, Integer numeroGruposFase){
		
		Integer contJugadoresEquiposGrupo = 0;
		for (Entry<Integer, ParticipantesIndividual2021> entry : this.jugadoresGrupoList.entrySet()) {
			Object value = entry.getValue();
			ParticipantesIndividual2021 pi = (ParticipantesIndividual2021) value;
			if(pi.getJugador().getEquipoId().equals(idEquipo)){
				contJugadoresEquiposGrupo++;
			}
		}
		
		if(contJugadoresEquiposGrupo > 0){
			if(contJugadoresEquiposGrupo == 1){
				
				//Si hay un jugador de un equipo en el grupo, 
				//pero el numero de jugadores del equipo es mayor del numero de grupos, 
				//permitir asignar hasta 2 jugadores en el mismo grupo
				
				if(equipoMasJugadoresQueGrupos.get(idEquipo) != null){
					Integer numeroJugadores = equipoMasJugadoresQueGrupos.get(idEquipo);
					if(numeroJugadores > numeroGruposFase){
						if((numeroJugadores - numeroGruposFase) > 1){
							return false;
						}
					}
				}
				
			}
			return true;
		}
		return false;
	}
	
	public ParticipantesIndividual2021 searchJugadorGrupo(Integer numeroJugadorGrupo, String grupo){
		for (Entry<Integer, ParticipantesIndividual2021> entry : this.jugadoresGrupoList.entrySet()) {
			Object value = entry.getValue();
			ParticipantesIndividual2021 pi = (ParticipantesIndividual2021) value;
			if(pi.getNumeroJugadorGrupo().equals(numeroJugadorGrupo) && this.id.equals(grupo)){
				return pi;
			}
		}
		return null;
	}

}
