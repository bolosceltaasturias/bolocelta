package com.bolocelta.entities.sorteos.individual;

import com.bolocelta.bbdd.constants.Activo2021;
import com.bolocelta.entities.Boleras2021;
import com.bolocelta.entities.Jugadores2021;

public class CalendarioFaseI2021 {

	private Integer id;
	private Integer categoriaId;
	private String grupo;
	private Integer jugador1Id;
	private Jugadores2021 jugador1;
	private Integer juegosJugador1;
	private Integer juegosJugador2;
	private Integer jugador2Id;
	private Jugadores2021 jugador2;
	private Integer orden;
	private String activo;
	private String inicia;
	private Long rowNum;
	private Integer boleraId;
	private Boleras2021 bolera;
	private String requipo1;
	private String requipo2;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCategoriaId() {
		return categoriaId;
	}

	public void setCategoriaId(Integer categoriaId) {
		this.categoriaId = categoriaId;
	}

	public String getGrupo() {
		return grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

	public Integer getJugador1Id() {
		return jugador1Id;
	}

	public void setJugador1Id(Integer jugador1Id) {
		this.jugador1Id = jugador1Id;
	}

	public Integer getJuegosJugador1() {
		return juegosJugador1;
	}

	public void setJuegosJugador1(Integer juegosJugador1) {
		this.juegosJugador1 = juegosJugador1;
	}

	public Integer getJuegosJugador2() {
		return juegosJugador2;
	}

	public void setJuegosJugador2(Integer juegosJugador2) {
		this.juegosJugador2 = juegosJugador2;
	}

	public Integer getJugador2Id() {
		return jugador2Id;
	}

	public void setJugador2Id(Integer jugador2Id) {
		this.jugador2Id = jugador2Id;
	}

	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}

	public String getActivo() {
		return activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	public Long getRowNum() {
		return rowNum;
	}

	public void setRowNum(Long rowNum) {
		this.rowNum = rowNum;
	}

	public String getInicia() {
		return inicia;
	}

	public void setInicia(String inicia) {
		this.inicia = inicia;
	}
	
	public String getInsertRow(){
		return (this.id + ";" +
					this.categoriaId + ";" +
					this.grupo + ";" +
					this.jugador1Id + ";" +
					this.juegosJugador1 + ";" +
					this.juegosJugador2 + ";" +
					this.jugador2Id + ";" +
					this.orden + ";" +
					this.activo + ";" +
					this.inicia + ";" +
					this.boleraId
					);
	}

	public Jugadores2021 getJugador1() {
		return jugador1;
	}

	public void setJugador1(Jugadores2021 jugador1) {
		this.jugador1 = jugador1;
	}

	public Jugadores2021 getJugador2() {
		return jugador2;
	}

	public void setJugador2(Jugadores2021 jugador2) {
		this.jugador2 = jugador2;
	}

	public Integer getBoleraId() {
		return boleraId;
	}

	public void setBoleraId(Integer boleraId) {
		this.boleraId = boleraId;
	}

	public Boleras2021 getBolera() {
		return bolera;
	}

	public void setBolera(Boleras2021 bolera) {
		this.bolera = bolera;
	}

	public String getRequipo1() {
		return requipo1;
	}

	public void setRequipo1(String requipo1) {
		this.requipo1 = requipo1;
	}

	public String getRequipo2() {
		return requipo2;
	}

	public void setRequipo2(String requipo2) {
		this.requipo2 = requipo2;
	}
	
	public String getKey(){
		return this.id + "-" + this.categoriaId + "-" + this.grupo;
	}
	
	public boolean isModificable(){
		if(this.activo != null && this.activo.equals(Activo2021.SI)){
			return true;
		}
		return false;
	}
	
	public boolean isGanaJugador1(){
		if(this.juegosJugador1 > this.juegosJugador2){
			return true;
		}
		return false;
	}
	
	public boolean isEmpate(){
		if(this.juegosJugador1 == this.juegosJugador2){
			return true;
		}
		return false;
	}
	
	public boolean isGanaJugador2(){
		if(this.juegosJugador1 < this.juegosJugador2){
			return true;
		}
		return false;
	}
	
}
