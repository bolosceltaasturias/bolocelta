package com.bolocelta.entities.sorteos.liga;

import java.util.HashMap;

import com.bolocelta.entities.Boleras2021;
import com.bolocelta.entities.Equipos2021;

public class SorteoBolerasEquipos2021 {

	private Boleras2021 bolera;
	private HashMap<Integer, Equipos2021> equipos = new HashMap<>();

	public Boleras2021 getBolera() {
		return bolera;
	}

	public void setBolera(Boleras2021 bolera) {
		this.bolera = bolera;
	}

	public HashMap<Integer, Equipos2021> getEquipos() {
		return equipos;
	}

	public void setEquipos(HashMap<Integer, Equipos2021> equipos) {
		this.equipos = equipos;
	}
	
	public Integer getNumeroEquipos(){
		return this.equipos.size();
	}

}
