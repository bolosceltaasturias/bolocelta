package com.bolocelta.entities.sorteos.liga;

import java.util.HashMap;

import com.bolocelta.entities.Categorias2021;
import com.bolocelta.entities.Equipos2021;

public class SorteoCategoria2021 {

	private Categorias2021 categoria;
	private HashMap<Integer, Equipos2021> equiposPorCategoria = new HashMap<>();

	public Categorias2021 getCategoria() {
		return categoria;
	}

	public void setCategoria(Categorias2021 categoria) {
		this.categoria = categoria;
	}

	public HashMap<Integer, Equipos2021> getEquiposPorCategoria() {
		return equiposPorCategoria;
	}

	public void setEquiposPorCategoria(HashMap<Integer, Equipos2021> equiposPorCategoria) {
		this.equiposPorCategoria = equiposPorCategoria;
	}

}
