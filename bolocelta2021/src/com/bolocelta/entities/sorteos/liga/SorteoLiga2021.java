package com.bolocelta.entities.sorteos.liga;

import java.util.HashMap;
import java.util.LinkedHashMap;

import com.bolocelta.entities.CampeonatoEquiposCalendario2021;

public class SorteoLiga2021 {
	
	private LinkedHashMap<String, SorteoBolerasCrucesLiga2021> sorteoBolerasCrucesLigaMap = new LinkedHashMap<>();
	private LinkedHashMap<Integer, SorteoTuplaEnfrentamientosLiga2021> sorteoTuplaEnfrentamientosLigaMap = new LinkedHashMap<>();
	
	

	private LinkedHashMap<Integer, SorteoCategoria2021> sorteoCategoriasMap = new LinkedHashMap<>();
	private LinkedHashMap<Integer, SorteoBolerasEquipos2021> sorteoBolerasEquiposMap = new LinkedHashMap<>();
	private HashMap<Integer, SorteoEnfrentamientosCategoria2021> sorteoEnfrentamientosCategoriaMap = new HashMap<>();
	private LinkedHashMap<String, SorteoJornadasCategoria2021> sorteoJornadasCategoriaMap = new LinkedHashMap<>();
	
	//Enfrentamientos jornada asignados
	private LinkedHashMap<String, CampeonatoEquiposCalendario2021> enfrentamientosJornadaAsignados = new LinkedHashMap<>();

	public LinkedHashMap<Integer, SorteoCategoria2021> getSorteoCategoriasMap() {
		return sorteoCategoriasMap;
	}

	public void setSorteoCategoriasMap(LinkedHashMap<Integer, SorteoCategoria2021> sorteoCategoriasMap) {
		this.sorteoCategoriasMap = sorteoCategoriasMap;
	}

	public LinkedHashMap<Integer, SorteoBolerasEquipos2021> getSorteoBolerasEquiposMap() {
		return sorteoBolerasEquiposMap;
	}

	public void setSorteoBolerasEquiposMap(LinkedHashMap<Integer, SorteoBolerasEquipos2021> sorteoBolerasEquiposMap) {
		this.sorteoBolerasEquiposMap = sorteoBolerasEquiposMap;
	}

	public HashMap<Integer, SorteoEnfrentamientosCategoria2021> getSorteoEnfrentamientosCategoriaMap() {
		return sorteoEnfrentamientosCategoriaMap;
	}

	public void setSorteoEnfrentamientosCategoriaMap(
			LinkedHashMap<Integer, SorteoEnfrentamientosCategoria2021> sorteoEnfrentamientosCategoriaMap) {
		this.sorteoEnfrentamientosCategoriaMap = sorteoEnfrentamientosCategoriaMap;
	}

	public LinkedHashMap<String, SorteoJornadasCategoria2021> getSorteoJornadasCategoriaMap() {
		return sorteoJornadasCategoriaMap;
	}

	public void setSorteoJornadasCategoriaMap(LinkedHashMap<String, SorteoJornadasCategoria2021> sorteoJornadasCategoriaMap) {
		this.sorteoJornadasCategoriaMap = sorteoJornadasCategoriaMap;
	}

	public LinkedHashMap<String, CampeonatoEquiposCalendario2021> getEnfrentamientosJornadaAsignados() {
		return enfrentamientosJornadaAsignados;
	}

	public void setEnfrentamientosJornadaAsignados(
			LinkedHashMap<String, CampeonatoEquiposCalendario2021> enfrentamientosJornadaAsignados) {
		this.enfrentamientosJornadaAsignados = enfrentamientosJornadaAsignados;
	}

	public LinkedHashMap<String, SorteoBolerasCrucesLiga2021> getSorteoBolerasCrucesLigaMap() {
		return sorteoBolerasCrucesLigaMap;
	}

	public void setSorteoBolerasCrucesLigaMap(LinkedHashMap<String, SorteoBolerasCrucesLiga2021> sorteoBolerasCrucesLigaMap) {
		this.sorteoBolerasCrucesLigaMap = sorteoBolerasCrucesLigaMap;
	}

	public LinkedHashMap<Integer, SorteoTuplaEnfrentamientosLiga2021> getSorteoTuplaEnfrentamientosLigaMap() {
		return sorteoTuplaEnfrentamientosLigaMap;
	}

	public void setSorteoTuplaEnfrentamientosLigaMap(
			LinkedHashMap<Integer, SorteoTuplaEnfrentamientosLiga2021> sorteoTuplaEnfrentamientosLigaMap) {
		this.sorteoTuplaEnfrentamientosLigaMap = sorteoTuplaEnfrentamientosLigaMap;
	}

}
