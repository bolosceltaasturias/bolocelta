package com.bolocelta.entities.sorteos.liga;

import java.util.HashMap;

import com.bolocelta.entities.CampeonatoEquiposCalendario2021;

public class SorteoEnfrentamientosCategoria2021 {

	private Integer id;
	private HashMap<String, CampeonatoEquiposCalendario2021> campeonatoEquiposCalendario = new HashMap<>();
	private HashMap<String, Integer> campeonatoEquiposCalendarioTuplaBolera = new HashMap<>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public HashMap<String, CampeonatoEquiposCalendario2021> getCampeonatoEquiposCalendario() {
		return campeonatoEquiposCalendario;
	}

	public void setCampeonatoEquiposCalendario(
			HashMap<String, CampeonatoEquiposCalendario2021> campeonatoEquiposCalendario) {
		this.campeonatoEquiposCalendario = campeonatoEquiposCalendario;
	}
	
	public HashMap<String, Integer> getCampeonatoEquiposCalendarioTuplaBolera() {
		return campeonatoEquiposCalendarioTuplaBolera;
	}

	public void setCampeonatoEquiposCalendarioTuplaBolera(
			HashMap<String, Integer> campeonatoEquiposCalendarioTuplaBolera) {
		this.campeonatoEquiposCalendarioTuplaBolera = campeonatoEquiposCalendarioTuplaBolera;
	}

}
