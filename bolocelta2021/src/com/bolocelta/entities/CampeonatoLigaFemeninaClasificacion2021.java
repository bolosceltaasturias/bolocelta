package com.bolocelta.entities;

public class CampeonatoLigaFemeninaClasificacion2021 implements Comparable<CampeonatoLigaFemeninaClasificacion2021>{

	private Integer id;
	private Integer jugadoraId;
	private Integer puntosTirada;
	private Integer puntosSacada;
	private Integer puntos;
	private Jugadores2021 jugadora;
	private Long rowNum;

	public CampeonatoLigaFemeninaClasificacion2021() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getJugadoraId() {
		return jugadoraId;
	}

	public void setJugadoraId(Integer jugadoraId) {
		this.jugadoraId = jugadoraId;
	}

	public Integer getPuntosTirada() {
		return puntosTirada;
	}

	public void setPuntosTirada(Integer puntosTirada) {
		this.puntosTirada = puntosTirada;
	}

	public Integer getPuntosSacada() {
		return puntosSacada;
	}

	public void setPuntosSacada(Integer puntosSacada) {
		this.puntosSacada = puntosSacada;
	}

	public Jugadores2021 getJugadora() {
		return jugadora;
	}

	public void setJugadora(Jugadores2021 jugadora) {
		this.jugadora = jugadora;
	}

	public Integer getPuntos() {
		return puntos;
	}

	public void setPuntos(Integer puntos) {
		this.puntos = puntos;
	}
	
	public Integer getTotalTantos(){
		return this.puntosTirada + this.puntosSacada;
	}


	@Override
	public int compareTo(CampeonatoLigaFemeninaClasificacion2021 o) {
		return (int)(o.getPuntos());
	}
	
	public Long getRowNum() {
		return rowNum;
	}

	public void setRowNum(Long rowNum) {
		this.rowNum = rowNum;
	}

}
