package com.bolocelta.entities;

import com.bolocelta.bbdd.constants.Activo2021;

public class CampeonatoEquiposCalendario2021 {

	private Integer id;
	private Integer jornada;
	private Integer categoriaId;
	private String fecha;
	private String hora;
	private Integer equipo1Id;
	private Integer pgequipo1;
	private Integer equipo2Id;
	private Integer pgequipo2;
	private Integer activo;
	private Categorias2021 categoria;
	private Equipos2021 equipo1;
	private Equipos2021 equipo2;
	private String requipo1;
	private String requipo2;
	private Integer idVuelta;
	private String foto = "sinfoto.png";
	private Long rowNum;

	public CampeonatoEquiposCalendario2021() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getJornada() {
		return jornada;
	}

	public void setJornada(Integer jornada) {
		this.jornada = jornada;
	}

	public Integer getCategoriaId() {
		return categoriaId;
	}

	public void setCategoriaId(Integer categoriaId) {
		this.categoriaId = categoriaId;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public Integer getEquipo1Id() {
		return equipo1Id;
	}

	public void setEquipo1Id(Integer equipo1Id) {
		this.equipo1Id = equipo1Id;
	}

	public Integer getPgequipo1() {
		return pgequipo1;
	}

	public void setPgequipo1(Integer pgequipo1) {
		this.pgequipo1 = pgequipo1;
	}

	public Integer getEquipo2Id() {
		return equipo2Id;
	}

	public void setEquipo2Id(Integer equipo2Id) {
		this.equipo2Id = equipo2Id;
	}

	public Integer getPgequipo2() {
		return pgequipo2;
	}

	public void setPgequipo2(Integer pgequipo2) {
		this.pgequipo2 = pgequipo2;
	}

	public Integer getActivo() {
		return activo;
	}

	public void setActivo(Integer activo) {
		this.activo = activo;
	}

	public Categorias2021 getCategoria() {
		return categoria;
	}

	public void setCategoria(Categorias2021 categoria) {
		this.categoria = categoria;
	}

	public Equipos2021 getEquipo1() {
		return equipo1;
	}

	public void setEquipo1(Equipos2021 equipo1) {
		this.equipo1 = equipo1;
	}

	public Equipos2021 getEquipo2() {
		return equipo2;
	}

	public void setEquipo2(Equipos2021 equipo2) {
		this.equipo2 = equipo2;
	}

	public String getRequipo1() {
		return requipo1;
	}

	public void setRequipo1(String requipo1) {
		this.requipo1 = requipo1;
	}

	public String getRequipo2() {
		return requipo2;
	}

	public void setRequipo2(String requipo2) {
		this.requipo2 = requipo2;
	}

	public Integer getIdVuelta() {
		return idVuelta;
	}

	public void setIdVuelta(Integer idVuelta) {
		this.idVuelta = idVuelta;
	}
	
	public String getFechaText(){
		return this.fecha;
	}
	
	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}
	
	public String getPrintCec(){
		String printCec = "";
		printCec += " *|* ID :" + this.id;
		printCec += " *|* CATEGORIA :" + this.categoria.getNombreCategoria();
		printCec += " *|* CATEGORIA ID :" + this.categoriaId;
		printCec += " *|* JORNADA :" + this.jornada;
		printCec += " *|* FECHA :" + this.fecha;
		printCec += " *|* HORA :" + this.hora;
		printCec += " *|* BOLERA :" + this.equipo1.getBoleraId() + " - " + this.equipo1.getBolera().getBolera();
		printCec += " *|* PG EQUIPO 1 :" + this.pgequipo1;
		printCec += " *|* NOMBRE EQUIPO 1 :" + this.equipo1.getNombre();
		printCec += " *|* PG EQUIPO 2 :" + this.pgequipo2;
		printCec += " *|* NOMBRE EQUIPO 2 :" + this.equipo2.getNombre();
		printCec += " *|* ACTIVO :" + this.activo;
		printCec += " *|* ID IDA :" + this.idVuelta;
		printCec += " *|* FOTO :" + this.foto;
		return printCec;
	}
	
	public String getInsertRow(){
		return (this.id + ";" +
				this.categoriaId + ";" +
				this.jornada + ";" +
				this.fecha + ";" +
				this.hora + ";" +
				this.pgequipo1 + ";" +
				this.equipo1Id + ";" +
				this.pgequipo2 + ";" +
				this.equipo2Id + ";" +
				this.activo + ";" +
				this.idVuelta + ";" +
				this.foto
				);
	}
	
	public boolean isModificable(){
		if(this.activo != null && this.activo == Activo2021.SI_NUMBER){
			return true;
		}
		return false;
	}
	
	public Long getRowNum() {
		return rowNum;
	}

	public void setRowNum(Long rowNum) {
		this.rowNum = rowNum;
	}
	
	public boolean isGanaEquipo1(){
		//Calcular victoria, empate y derrota
    	Integer resultadoPartida =  getPgequipo1() - getPgequipo2();
    	//Sin Jugar
    	if(resultadoPartida == 0){
    		return false;
    	}else
    	//Ganador 1 solo una partida
    	if(resultadoPartida > 0){
    		return true;
    	}else 
    	//Ganador 2 solo una partida
    	if(resultadoPartida < 0){
    		return false;
    	}
    	return false;
	}
	
	public boolean isGanaEquipo2(){
		//Calcular victoria, empate y derrota
    	Integer resultadoPartida =  getPgequipo1() - getPgequipo2();
    	//Sin Jugar
    	if(resultadoPartida == 0){
    		return false;
    	}else
    	//Ganador 1 solo una partida
    	if(resultadoPartida > 0){
    		return false;
    	}else 
    	//Ganador 2 solo una partida
    	if(resultadoPartida < 0){
    		return true;
    	}
    	return false;
	}
	
	public boolean isEmpate(){
		//Calcular victoria, empate y derrota
    	Integer resultadoPartida =  getPgequipo1() - getPgequipo2();
    	//Sin Jugar
    	if(resultadoPartida == 0){
    		return true;
    	}else
    	//Ganador 1 solo una partida
    	if(resultadoPartida > 0){
    		return false;
    	}else 
    	//Ganador 2 solo una partida
    	if(resultadoPartida < 0){
    		return false;
    	}
    	return false;
	}
		

}
