package com.bolocelta.entities;

import java.util.List;
import java.util.Map;

import com.bolocelta.application.enumerations.ActivoEnumeration2021;

public class Equipos2021 {

	private Integer id;
	private Integer categoriaId;
	private String nombre;
	private String logo;
	private Categorias2021 categoria;
	private Integer boleraId;
	private Boleras2021 bolera;
	private List<Jugadores2021> jugadoresList;
	private Map<Integer,Jugadores2021> jugadoresMap;
	private String horarioPreferenteSabadoMaņana;
	private String horarioPreferenteSabadoTarde;
	private String horarioPreferenteDomingoMaņana;
	private String horarioPreferenteDomingoTarde;
	private String email;
	private String liga;
	private Long rowNum;
	private Integer noJugarHorarioOtroEquipo;
	private String cerradoHorariosPreferentes;

	public Equipos2021() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCategoriaId() {
		return categoriaId;
	}

	public void setCategoriaId(Integer categoriaId) {
		this.categoriaId = categoriaId;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public Categorias2021 getCategoria() {
		return categoria;
	}

	public void setCategoria(Categorias2021 categoria) {
		this.categoria = categoria;
	}

	public Integer getBoleraId() {
		return boleraId;
	}

	public void setBoleraId(Integer boleraId) {
		this.boleraId = boleraId;
	}

	public Boleras2021 getBolera() {
		return bolera;
	}

	public void setBolera(Boleras2021 bolera) {
		this.bolera = bolera;
	}

	public List<Jugadores2021> getJugadoresList() {
		return jugadoresList;
	}

	public void setJugadoresList(List<Jugadores2021> jugadoresList) {
		this.jugadoresList = jugadoresList;
	}

	public Map<Integer, Jugadores2021> getJugadoresMap() {
		return jugadoresMap;
	}

	public void setJugadoresMap(Map<Integer, Jugadores2021> jugadoresMap) {
		this.jugadoresMap = jugadoresMap;
	}
	
	public String getHorarioPreferenteSabadoMaņana() {
		return horarioPreferenteSabadoMaņana;
	}

	public void setHorarioPreferenteSabadoMaņana(String horarioPreferenteSabadoMaņana) {
		this.horarioPreferenteSabadoMaņana = horarioPreferenteSabadoMaņana;
	}

	public String getHorarioPreferenteSabadoTarde() {
		return horarioPreferenteSabadoTarde;
	}

	public void setHorarioPreferenteSabadoTarde(String horarioPreferenteSabadoTarde) {
		this.horarioPreferenteSabadoTarde = horarioPreferenteSabadoTarde;
	}

	public String getHorarioPreferenteDomingoMaņana() {
		return horarioPreferenteDomingoMaņana;
	}

	public void setHorarioPreferenteDomingoMaņana(String horarioPreferenteDomingoMaņana) {
		this.horarioPreferenteDomingoMaņana = horarioPreferenteDomingoMaņana;
	}

	public String getHorarioPreferenteDomingoTarde() {
		return horarioPreferenteDomingoTarde;
	}

	public void setHorarioPreferenteDomingoTarde(String horarioPreferenteDomingoTarde) {
		this.horarioPreferenteDomingoTarde = horarioPreferenteDomingoTarde;
	}

	public String getHashCode(){
		return this.id + "_" + this.nombre; 
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getLiga() {
		return liga;
	}

	public void setLiga(String liga) {
		this.liga = liga;
	}

	public Long getRowNum() {
		return rowNum;
	}

	public void setRowNum(Long rowNum) {
		this.rowNum = rowNum;
	}

	public Integer getNoJugarHorarioOtroEquipo() {
		return noJugarHorarioOtroEquipo;
	}

	public void setNoJugarHorarioOtroEquipo(Integer noJugarHorarioOtroEquipo) {
		this.noJugarHorarioOtroEquipo = noJugarHorarioOtroEquipo;
	}
	
	public String getCerradoHorariosPreferentes() {
		return cerradoHorariosPreferentes;
	}

	public void setCerradoHorariosPreferentes(String cerradoHorariosPreferentes) {
		this.cerradoHorariosPreferentes = cerradoHorariosPreferentes;
	}
	
	public boolean isCerradoHorarioPreferente(){
		if(this.cerradoHorariosPreferentes != null && this.cerradoHorariosPreferentes.equalsIgnoreCase((ActivoEnumeration2021.SI))){
			return true;
		}
		return false;
	}

	
}
