package com.bolocelta.entities;

public class Galerias2021 {

	private Integer id;
	private String nombre;
	private String descripcion;

	public Galerias2021() {
		// TODO Auto-generated constructor stub
	}
	
	public Galerias2021(Integer id, String nombre, String descripcion) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.descripcion = descripcion;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
