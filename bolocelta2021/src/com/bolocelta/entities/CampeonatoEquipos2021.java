package com.bolocelta.entities;

import java.util.List;

public class CampeonatoEquipos2021 {

	private List<CampeonatoEquiposClasificacion2021> clasificacion;
	private List<CampeonatoEquiposCalendario2021> calendario;

	public CampeonatoEquipos2021() {
		// TODO Auto-generated constructor stub
	}

	public List<CampeonatoEquiposClasificacion2021> getClasificacion() {
		return clasificacion;
	}

	public void setClasificacion(List<CampeonatoEquiposClasificacion2021> clasificacion) {
		this.clasificacion = clasificacion;
	}

	public List<CampeonatoEquiposCalendario2021> getCalendario() {
		return calendario;
	}

	public void setCalendario(List<CampeonatoEquiposCalendario2021> calendario) {
		this.calendario = calendario;
	}



}
