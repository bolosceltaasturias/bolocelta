package com.bolocelta.entities;

public class Noticias2021 {

	private Integer id;
	private String fecha;
	private String tituloCampeonato;
	private String descripcion;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getTituloCampeonato() {
		return tituloCampeonato;
	}

	public void setTituloCampeonato(String tituloCampeonato) {
		this.tituloCampeonato = tituloCampeonato;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public String getResumenCorto(){
		return this.tituloCampeonato + ": " + this.descripcion;
	}

}
