package com.bolocelta.facade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.inject.Named;

import com.bolocelta.application.session.SessionState;
import com.bolocelta.bbdd.readTables.LeerHistoricoCampeonatosEspana2021;
import com.bolocelta.entities.HistoricoCampeonatosEspana2021;

@Named
@ConversationScoped
@ManagedBean
public class HistoriaCampeonatoEspana2021 implements Serializable {
	
	@Inject
	private SessionState sessionState;
	
	private static final long serialVersionUID = 1L;
	
	private LeerHistoricoCampeonatosEspana2021 leerHistoricoCampeonatosEspana = new LeerHistoricoCampeonatosEspana2021();
	
	private List<HistoricoCampeonatosEspana2021> resultListHistoricoCampeonatoEspana = null;
	private List<HistoricoCampeonatosEspana2021> resultListHistoricoCampeonatoEspanaEquipos = null;
	private List<HistoricoCampeonatosEspana2021> resultListHistoricoCampeonatoEspanaIndividual = null;
	private List<HistoricoCampeonatosEspana2021> resultListHistoricoCampeonatoEspanaParejas = null;
	private List<HistoricoCampeonatosEspana2021> resultListHistoricoCampeonatoEspanaIndividualFemenino = null;
	private List<HistoricoCampeonatosEspana2021> resultListHistoricoCampeonatoEspanaParejasFemenino = null;
	private List<HistoricoCampeonatosEspana2021> resultListHistoricoCampeonatoEspanaMixto = null;
	private List<HistoricoCampeonatosEspana2021> resultListHistoricoCampeonatoEspanaVeteranos = null;
	private List<HistoricoCampeonatosEspana2021> resultListHistoricoCampeonatoEspanaJuveniles = null;

	
	private List<Integer> resultListAnyoFasesTabShow = null;
	
	public List<HistoricoCampeonatosEspana2021> getResultListHistoricoCampeonatoEspanaAll() {
		if(resultListHistoricoCampeonatoEspana == null){
			resultListHistoricoCampeonatoEspana = (List<HistoricoCampeonatosEspana2021>) leerHistoricoCampeonatosEspana.listResult();
		}
		return resultListHistoricoCampeonatoEspana;
	}
	
	public List<HistoricoCampeonatosEspana2021> getResultListHistoricoCampeonatoEspanaEquipos() {
		if(resultListHistoricoCampeonatoEspanaEquipos == null){
			resultListHistoricoCampeonatoEspanaEquipos = (List<HistoricoCampeonatosEspana2021>) leerHistoricoCampeonatosEspana.listResultEquipos();
		}
		return resultListHistoricoCampeonatoEspanaEquipos;
	}

	public List<HistoricoCampeonatosEspana2021> getResultListHistoricoCampeonatoEspanaIndividual() {
		if(resultListHistoricoCampeonatoEspanaIndividual == null){
			resultListHistoricoCampeonatoEspanaIndividual = (List<HistoricoCampeonatosEspana2021>) leerHistoricoCampeonatosEspana.listResultIndividual();
		}
		return resultListHistoricoCampeonatoEspanaIndividual;
	}
	
	public List<HistoricoCampeonatosEspana2021> getResultListHistoricoCampeonatoEspanaParejas() {
		if(resultListHistoricoCampeonatoEspanaParejas == null){
			resultListHistoricoCampeonatoEspanaParejas = (List<HistoricoCampeonatosEspana2021>) leerHistoricoCampeonatosEspana.listResultParejas();
		}
		return resultListHistoricoCampeonatoEspanaParejas;
	}
	
	public List<HistoricoCampeonatosEspana2021> getResultListHistoricoCampeonatoEspanaIndividualFemenino() {
		if(resultListHistoricoCampeonatoEspanaIndividualFemenino == null){
			resultListHistoricoCampeonatoEspanaIndividualFemenino = (List<HistoricoCampeonatosEspana2021>) leerHistoricoCampeonatosEspana.listResultIndividualFemenino();
		}
		return resultListHistoricoCampeonatoEspanaIndividualFemenino;
	}
	
	public List<HistoricoCampeonatosEspana2021> getResultListHistoricoCampeonatoEspanaParejasFemenino() {
		if(resultListHistoricoCampeonatoEspanaParejasFemenino == null){
			resultListHistoricoCampeonatoEspanaParejasFemenino = (List<HistoricoCampeonatosEspana2021>) leerHistoricoCampeonatosEspana.listResultParejasFemenino();
		}
		return resultListHistoricoCampeonatoEspanaParejasFemenino;
	}
	
	public List<HistoricoCampeonatosEspana2021> getResultListHistoricoCampeonatoEspanaMixto() {
		if(resultListHistoricoCampeonatoEspanaMixto == null){
			resultListHistoricoCampeonatoEspanaMixto = (List<HistoricoCampeonatosEspana2021>) leerHistoricoCampeonatosEspana.listResultMixto();
		}
		return resultListHistoricoCampeonatoEspanaMixto;
	}
	
	public List<HistoricoCampeonatosEspana2021> getResultListHistoricoCampeonatoEspanaVeteranos() {
		if(resultListHistoricoCampeonatoEspanaVeteranos == null){
			resultListHistoricoCampeonatoEspanaVeteranos = (List<HistoricoCampeonatosEspana2021>) leerHistoricoCampeonatosEspana.listResultVeteranos();
		}
		return resultListHistoricoCampeonatoEspanaVeteranos;
	}
	
	public List<HistoricoCampeonatosEspana2021> getResultListHistoricoCampeonatoEspanaJuveniles() {
		if(resultListHistoricoCampeonatoEspanaJuveniles == null){
			resultListHistoricoCampeonatoEspanaJuveniles = (List<HistoricoCampeonatosEspana2021>) leerHistoricoCampeonatosEspana.listResultJuveniles();
		}
		return resultListHistoricoCampeonatoEspanaJuveniles;
	}
	
	
	public List<HistoricoCampeonatosEspana2021> getResultListHistoricoCampeonatoEspanaEquiposByAnyo(Integer anyo) {
		List<HistoricoCampeonatosEspana2021> result = new ArrayList<HistoricoCampeonatosEspana2021>();
		for (HistoricoCampeonatosEspana2021 historicoCampeonatosEspana : getResultListHistoricoCampeonatoEspanaEquipos()) {
			if(historicoCampeonatosEspana.getAnyo().equals(anyo)){
				result.add(historicoCampeonatosEspana);
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(result, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				HistoricoCampeonatosEspana2021 cec1 = (HistoricoCampeonatosEspana2021) o1;
				HistoricoCampeonatosEspana2021 cec2 = (HistoricoCampeonatosEspana2021) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPosicion().compareTo(cec2.getPosicion());
					return rpuntos;
			}
		});
		
		return result;
	}
	
	public List<HistoricoCampeonatosEspana2021> getResultListHistoricoCampeonatoEspanaIndividualByAnyo(Integer anyo) {
		List<HistoricoCampeonatosEspana2021> result = new ArrayList<HistoricoCampeonatosEspana2021>();
		for (HistoricoCampeonatosEspana2021 historicoCampeonatosEspana : getResultListHistoricoCampeonatoEspanaIndividual()) {
			if(historicoCampeonatosEspana.getAnyo().equals(anyo)){
				result.add(historicoCampeonatosEspana);
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(result, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				HistoricoCampeonatosEspana2021 cec1 = (HistoricoCampeonatosEspana2021) o1;
				HistoricoCampeonatosEspana2021 cec2 = (HistoricoCampeonatosEspana2021) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPosicion().compareTo(cec2.getPosicion());
					return rpuntos;
			}
		});
		
		return result;
	}
	
	public List<HistoricoCampeonatosEspana2021> getResultListHistoricoCampeonatoEspanaParejasByAnyo(Integer anyo) {
		List<HistoricoCampeonatosEspana2021> result = new ArrayList<HistoricoCampeonatosEspana2021>();
		for (HistoricoCampeonatosEspana2021 historicoCampeonatosEspana : getResultListHistoricoCampeonatoEspanaParejas()) {
			if(historicoCampeonatosEspana.getAnyo().equals(anyo)){
				result.add(historicoCampeonatosEspana);
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(result, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				HistoricoCampeonatosEspana2021 cec1 = (HistoricoCampeonatosEspana2021) o1;
				HistoricoCampeonatosEspana2021 cec2 = (HistoricoCampeonatosEspana2021) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPosicion().compareTo(cec2.getPosicion());
					return rpuntos;
			}
		});
		
		return result;
	}
	

	public List<HistoricoCampeonatosEspana2021> getResultListHistoricoCampeonatoEspanaIndividualFemeninoByAnyo(Integer anyo) {
		List<HistoricoCampeonatosEspana2021> result = new ArrayList<HistoricoCampeonatosEspana2021>();
		for (HistoricoCampeonatosEspana2021 historicoCampeonatosEspana : getResultListHistoricoCampeonatoEspanaIndividualFemenino()) {
			if(historicoCampeonatosEspana.getAnyo().equals(anyo)){
				result.add(historicoCampeonatosEspana);
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(result, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				HistoricoCampeonatosEspana2021 cec1 = (HistoricoCampeonatosEspana2021) o1;
				HistoricoCampeonatosEspana2021 cec2 = (HistoricoCampeonatosEspana2021) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPosicion().compareTo(cec2.getPosicion());
					return rpuntos;
			}
		});
		
		return result;
	}
	
	public List<HistoricoCampeonatosEspana2021> getResultListHistoricoCampeonatoEspanaParejasFemeninoByAnyo(Integer anyo) {
		List<HistoricoCampeonatosEspana2021> result = new ArrayList<HistoricoCampeonatosEspana2021>();
		for (HistoricoCampeonatosEspana2021 historicoCampeonatosEspana : getResultListHistoricoCampeonatoEspanaParejasFemenino()) {
			if(historicoCampeonatosEspana.getAnyo().equals(anyo)){
				result.add(historicoCampeonatosEspana);
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(result, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				HistoricoCampeonatosEspana2021 cec1 = (HistoricoCampeonatosEspana2021) o1;
				HistoricoCampeonatosEspana2021 cec2 = (HistoricoCampeonatosEspana2021) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPosicion().compareTo(cec2.getPosicion());
					return rpuntos;
			}
		});
		
		return result;
	}
	
	public List<HistoricoCampeonatosEspana2021> getResultListHistoricoCampeonatoEspanaMixtoByAnyo(Integer anyo) {
		List<HistoricoCampeonatosEspana2021> result = new ArrayList<HistoricoCampeonatosEspana2021>();
		for (HistoricoCampeonatosEspana2021 historicoCampeonatosEspana : getResultListHistoricoCampeonatoEspanaMixto()) {
			if(historicoCampeonatosEspana.getAnyo().equals(anyo)){
				result.add(historicoCampeonatosEspana);
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(result, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				HistoricoCampeonatosEspana2021 cec1 = (HistoricoCampeonatosEspana2021) o1;
				HistoricoCampeonatosEspana2021 cec2 = (HistoricoCampeonatosEspana2021) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPosicion().compareTo(cec2.getPosicion());
					return rpuntos;
			}
		});
		
		return result;
	}
	
	public List<HistoricoCampeonatosEspana2021> getResultListHistoricoCampeonatoEspanaVeteranosByAnyo(Integer anyo) {
		List<HistoricoCampeonatosEspana2021> result = new ArrayList<HistoricoCampeonatosEspana2021>();
		for (HistoricoCampeonatosEspana2021 historicoCampeonatosEspana : getResultListHistoricoCampeonatoEspanaVeteranos()) {
			if(historicoCampeonatosEspana.getAnyo().equals(anyo)){
				result.add(historicoCampeonatosEspana);
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(result, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				HistoricoCampeonatosEspana2021 cec1 = (HistoricoCampeonatosEspana2021) o1;
				HistoricoCampeonatosEspana2021 cec2 = (HistoricoCampeonatosEspana2021) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPosicion().compareTo(cec2.getPosicion());
					return rpuntos;
			}
		});
		
		return result;
	}
	
	public List<HistoricoCampeonatosEspana2021> getResultListHistoricoCampeonatoEspanaJuvenilesByAnyo(Integer anyo) {
		List<HistoricoCampeonatosEspana2021> result = new ArrayList<HistoricoCampeonatosEspana2021>();
		for (HistoricoCampeonatosEspana2021 historicoCampeonatosEspana : getResultListHistoricoCampeonatoEspanaJuveniles()) {
			if(historicoCampeonatosEspana.getAnyo().equals(anyo)){
				result.add(historicoCampeonatosEspana);
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(result, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				HistoricoCampeonatosEspana2021 cec1 = (HistoricoCampeonatosEspana2021) o1;
				HistoricoCampeonatosEspana2021 cec2 = (HistoricoCampeonatosEspana2021) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPosicion().compareTo(cec2.getPosicion());
					return rpuntos;
			}
		});
		
		return result;
	}
	

	
	public boolean isDataCampeonato() {
		if(getResultListAnyoTabShow().size() > 0){
			return true;
		}
		return false;
	}
	
	public List<Integer> getResultListAnyoTabShow() {
		if(resultListAnyoFasesTabShow == null){
			resultListAnyoFasesTabShow = new ArrayList<>();
			
			for (HistoricoCampeonatosEspana2021 historicoCampeonatosEspana : getResultListHistoricoCampeonatoEspanaAll()) {
				if(!resultListAnyoFasesTabShow.contains(historicoCampeonatosEspana.getAnyo())){
					resultListAnyoFasesTabShow.add(historicoCampeonatosEspana.getAnyo());
				}
			}
			
		}
		
		//Ordenar la clasificacion
		Collections.sort(resultListAnyoFasesTabShow, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				Integer cec1 = (Integer) o1;
				Integer cec2 = (Integer) o2;
				//1. Ordenar por anyo descendente 
				int rpuntos = cec1.compareTo(cec2);
					return rpuntos;
			}
		});
		
		Collections.reverse(resultListAnyoFasesTabShow);
		
		return resultListAnyoFasesTabShow;
	}

	public Object selected() {
		// TODO Auto-generated method stub
		return null;
	}

	public Object read(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	public String insert(Object insert) {
		// TODO Auto-generated method stub
		return null;
	}

	public String update(Object update) {
		// TODO Auto-generated method stub
		return null;
	}

	public String delete(Object delete) {
		// TODO Auto-generated method stub
		return null;
	}
		

}
