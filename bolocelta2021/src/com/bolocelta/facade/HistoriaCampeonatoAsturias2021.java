package com.bolocelta.facade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.inject.Named;

import com.bolocelta.application.session.SessionState;
import com.bolocelta.bbdd.readTables.LeerHistoricoCampeonatosAsturias2021;
import com.bolocelta.entities.HistoricoCampeonatosAsturias2021;

@Named
@ConversationScoped
@ManagedBean
public class HistoriaCampeonatoAsturias2021 implements Serializable {
	
	@Inject
	private SessionState sessionState;
	
	private static final long serialVersionUID = 1L;
	
	private LeerHistoricoCampeonatosAsturias2021 leerHistoricoCampeonatosAsturias = new LeerHistoricoCampeonatosAsturias2021();
	
	private List<HistoricoCampeonatosAsturias2021> resultListHistoricoCampeonatoAsturias = null;
	private List<HistoricoCampeonatosAsturias2021> resultListHistoricoCampeonatoAsturiasEquipos = null;
	private List<HistoricoCampeonatosAsturias2021> resultListHistoricoCampeonatoAsturiasIndividual = null;
	private List<HistoricoCampeonatosAsturias2021> resultListHistoricoCampeonatoAsturiasParejas = null;
	private List<HistoricoCampeonatosAsturias2021> resultListHistoricoCampeonatoAsturiasIndividualFemenino = null;
	private List<HistoricoCampeonatosAsturias2021> resultListHistoricoCampeonatoAsturiasParejasFemenino = null;
	private List<HistoricoCampeonatosAsturias2021> resultListHistoricoCampeonatoAsturiasMixto = null;
	private List<HistoricoCampeonatosAsturias2021> resultListHistoricoCampeonatoAsturiasVeteranos = null;
	private List<HistoricoCampeonatosAsturias2021> resultListHistoricoCampeonatoAsturiasJuveniles = null;

	
	private List<Integer> resultListAnyoFasesTabShow = null;
	
	public List<HistoricoCampeonatosAsturias2021> getResultListHistoricoCampeonatoAsturiasAll() {
		if(resultListHistoricoCampeonatoAsturias == null){
			resultListHistoricoCampeonatoAsturias = (List<HistoricoCampeonatosAsturias2021>) leerHistoricoCampeonatosAsturias.listResult();
		}
		return resultListHistoricoCampeonatoAsturias;
	}
	
	public List<HistoricoCampeonatosAsturias2021> getResultListHistoricoCampeonatoAsturiasEquipos() {
		if(resultListHistoricoCampeonatoAsturiasEquipos == null){
			resultListHistoricoCampeonatoAsturiasEquipos = (List<HistoricoCampeonatosAsturias2021>) leerHistoricoCampeonatosAsturias.listResultEquipos();
		}
		return resultListHistoricoCampeonatoAsturiasEquipos;
	}

	public List<HistoricoCampeonatosAsturias2021> getResultListHistoricoCampeonatoAsturiasIndividual() {
		if(resultListHistoricoCampeonatoAsturiasIndividual == null){
			resultListHistoricoCampeonatoAsturiasIndividual = (List<HistoricoCampeonatosAsturias2021>) leerHistoricoCampeonatosAsturias.listResultIndividual();
		}
		return resultListHistoricoCampeonatoAsturiasIndividual;
	}
	
	public List<HistoricoCampeonatosAsturias2021> getResultListHistoricoCampeonatoAsturiasParejas() {
		if(resultListHistoricoCampeonatoAsturiasParejas == null){
			resultListHistoricoCampeonatoAsturiasParejas = (List<HistoricoCampeonatosAsturias2021>) leerHistoricoCampeonatosAsturias.listResultParejas();
		}
		return resultListHistoricoCampeonatoAsturiasParejas;
	}
	
	public List<HistoricoCampeonatosAsturias2021> getResultListHistoricoCampeonatoAsturiasIndividualFemenino() {
		if(resultListHistoricoCampeonatoAsturiasIndividualFemenino == null){
			resultListHistoricoCampeonatoAsturiasIndividualFemenino = (List<HistoricoCampeonatosAsturias2021>) leerHistoricoCampeonatosAsturias.listResultIndividualFemenino();
		}
		return resultListHistoricoCampeonatoAsturiasIndividualFemenino;
	}
	
	public List<HistoricoCampeonatosAsturias2021> getResultListHistoricoCampeonatoAsturiasParejasFemenino() {
		if(resultListHistoricoCampeonatoAsturiasParejasFemenino == null){
			resultListHistoricoCampeonatoAsturiasParejasFemenino = (List<HistoricoCampeonatosAsturias2021>) leerHistoricoCampeonatosAsturias.listResultParejasFemenino();
		}
		return resultListHistoricoCampeonatoAsturiasParejasFemenino;
	}
	
	public List<HistoricoCampeonatosAsturias2021> getResultListHistoricoCampeonatoAsturiasMixto() {
		if(resultListHistoricoCampeonatoAsturiasMixto == null){
			resultListHistoricoCampeonatoAsturiasMixto = (List<HistoricoCampeonatosAsturias2021>) leerHistoricoCampeonatosAsturias.listResultMixto();
		}
		return resultListHistoricoCampeonatoAsturiasMixto;
	}
	
	public List<HistoricoCampeonatosAsturias2021> getResultListHistoricoCampeonatoAsturiasVeteranos() {
		if(resultListHistoricoCampeonatoAsturiasVeteranos == null){
			resultListHistoricoCampeonatoAsturiasVeteranos = (List<HistoricoCampeonatosAsturias2021>) leerHistoricoCampeonatosAsturias.listResultVeteranos();
		}
		return resultListHistoricoCampeonatoAsturiasVeteranos;
	}
	
	public List<HistoricoCampeonatosAsturias2021> getResultListHistoricoCampeonatoAsturiasJuveniles() {
		if(resultListHistoricoCampeonatoAsturiasJuveniles == null){
			resultListHistoricoCampeonatoAsturiasJuveniles = (List<HistoricoCampeonatosAsturias2021>) leerHistoricoCampeonatosAsturias.listResultJuveniles();
		}
		return resultListHistoricoCampeonatoAsturiasJuveniles;
	}
	
	
	public List<HistoricoCampeonatosAsturias2021> getResultListHistoricoCampeonatoAsturiasEquiposByAnyo(Integer anyo) {
		List<HistoricoCampeonatosAsturias2021> result = new ArrayList<HistoricoCampeonatosAsturias2021>();
		for (HistoricoCampeonatosAsturias2021 historicoCampeonatosAsturias : getResultListHistoricoCampeonatoAsturiasEquipos()) {
			if(historicoCampeonatosAsturias.getAnyo().equals(anyo)){
				result.add(historicoCampeonatosAsturias);
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(result, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				HistoricoCampeonatosAsturias2021 cec1 = (HistoricoCampeonatosAsturias2021) o1;
				HistoricoCampeonatosAsturias2021 cec2 = (HistoricoCampeonatosAsturias2021) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPosicion().compareTo(cec2.getPosicion());
					return rpuntos;
			}
		});
		
		return result;
	}
	
	public List<HistoricoCampeonatosAsturias2021> getResultListHistoricoCampeonatoAsturiasIndividualByAnyo(Integer anyo) {
		List<HistoricoCampeonatosAsturias2021> result = new ArrayList<HistoricoCampeonatosAsturias2021>();
		for (HistoricoCampeonatosAsturias2021 historicoCampeonatosAsturias : getResultListHistoricoCampeonatoAsturiasIndividual()) {
			if(historicoCampeonatosAsturias.getAnyo().equals(anyo)){
				result.add(historicoCampeonatosAsturias);
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(result, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				HistoricoCampeonatosAsturias2021 cec1 = (HistoricoCampeonatosAsturias2021) o1;
				HistoricoCampeonatosAsturias2021 cec2 = (HistoricoCampeonatosAsturias2021) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPosicion().compareTo(cec2.getPosicion());
					return rpuntos;
			}
		});
		
		return result;
	}
	
	public List<HistoricoCampeonatosAsturias2021> getResultListHistoricoCampeonatoAsturiasParejasByAnyo(Integer anyo) {
		List<HistoricoCampeonatosAsturias2021> result = new ArrayList<HistoricoCampeonatosAsturias2021>();
		for (HistoricoCampeonatosAsturias2021 historicoCampeonatosAsturias : getResultListHistoricoCampeonatoAsturiasParejas()) {
			if(historicoCampeonatosAsturias.getAnyo().equals(anyo)){
				result.add(historicoCampeonatosAsturias);
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(result, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				HistoricoCampeonatosAsturias2021 cec1 = (HistoricoCampeonatosAsturias2021) o1;
				HistoricoCampeonatosAsturias2021 cec2 = (HistoricoCampeonatosAsturias2021) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPosicion().compareTo(cec2.getPosicion());
					return rpuntos;
			}
		});
		
		return result;
	}
	

	public List<HistoricoCampeonatosAsturias2021> getResultListHistoricoCampeonatoAsturiasIndividualFemeninoByAnyo(Integer anyo) {
		List<HistoricoCampeonatosAsturias2021> result = new ArrayList<HistoricoCampeonatosAsturias2021>();
		for (HistoricoCampeonatosAsturias2021 historicoCampeonatosAsturias : getResultListHistoricoCampeonatoAsturiasIndividualFemenino()) {
			if(historicoCampeonatosAsturias.getAnyo().equals(anyo)){
				result.add(historicoCampeonatosAsturias);
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(result, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				HistoricoCampeonatosAsturias2021 cec1 = (HistoricoCampeonatosAsturias2021) o1;
				HistoricoCampeonatosAsturias2021 cec2 = (HistoricoCampeonatosAsturias2021) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPosicion().compareTo(cec2.getPosicion());
					return rpuntos;
			}
		});
		
		return result;
	}
	
	public List<HistoricoCampeonatosAsturias2021> getResultListHistoricoCampeonatoAsturiasParejasFemeninoByAnyo(Integer anyo) {
		List<HistoricoCampeonatosAsturias2021> result = new ArrayList<HistoricoCampeonatosAsturias2021>();
		for (HistoricoCampeonatosAsturias2021 historicoCampeonatosAsturias : getResultListHistoricoCampeonatoAsturiasParejasFemenino()) {
			if(historicoCampeonatosAsturias.getAnyo().equals(anyo)){
				result.add(historicoCampeonatosAsturias);
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(result, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				HistoricoCampeonatosAsturias2021 cec1 = (HistoricoCampeonatosAsturias2021) o1;
				HistoricoCampeonatosAsturias2021 cec2 = (HistoricoCampeonatosAsturias2021) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPosicion().compareTo(cec2.getPosicion());
					return rpuntos;
			}
		});
		
		return result;
	}
	
	public List<HistoricoCampeonatosAsturias2021> getResultListHistoricoCampeonatoAsturiasMixtoByAnyo(Integer anyo) {
		List<HistoricoCampeonatosAsturias2021> result = new ArrayList<HistoricoCampeonatosAsturias2021>();
		for (HistoricoCampeonatosAsturias2021 historicoCampeonatosAsturias : getResultListHistoricoCampeonatoAsturiasMixto()) {
			if(historicoCampeonatosAsturias.getAnyo().equals(anyo)){
				result.add(historicoCampeonatosAsturias);
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(result, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				HistoricoCampeonatosAsturias2021 cec1 = (HistoricoCampeonatosAsturias2021) o1;
				HistoricoCampeonatosAsturias2021 cec2 = (HistoricoCampeonatosAsturias2021) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPosicion().compareTo(cec2.getPosicion());
					return rpuntos;
			}
		});
		
		return result;
	}
	
	public List<HistoricoCampeonatosAsturias2021> getResultListHistoricoCampeonatoAsturiasVeteranosByAnyo(Integer anyo) {
		List<HistoricoCampeonatosAsturias2021> result = new ArrayList<HistoricoCampeonatosAsturias2021>();
		for (HistoricoCampeonatosAsturias2021 historicoCampeonatosAsturias : getResultListHistoricoCampeonatoAsturiasVeteranos()) {
			if(historicoCampeonatosAsturias.getAnyo().equals(anyo)){
				result.add(historicoCampeonatosAsturias);
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(result, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				HistoricoCampeonatosAsturias2021 cec1 = (HistoricoCampeonatosAsturias2021) o1;
				HistoricoCampeonatosAsturias2021 cec2 = (HistoricoCampeonatosAsturias2021) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPosicion().compareTo(cec2.getPosicion());
					return rpuntos;
			}
		});
		
		return result;
	}
	
	public List<HistoricoCampeonatosAsturias2021> getResultListHistoricoCampeonatoAsturiasJuvenilesByAnyo(Integer anyo) {
		List<HistoricoCampeonatosAsturias2021> result = new ArrayList<HistoricoCampeonatosAsturias2021>();
		for (HistoricoCampeonatosAsturias2021 historicoCampeonatosAsturias : getResultListHistoricoCampeonatoAsturiasJuveniles()) {
			if(historicoCampeonatosAsturias.getAnyo().equals(anyo)){
				result.add(historicoCampeonatosAsturias);
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(result, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				HistoricoCampeonatosAsturias2021 cec1 = (HistoricoCampeonatosAsturias2021) o1;
				HistoricoCampeonatosAsturias2021 cec2 = (HistoricoCampeonatosAsturias2021) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPosicion().compareTo(cec2.getPosicion());
					return rpuntos;
			}
		});
		
		return result;
	}
	

	
	public boolean isDataCampeonato() {
		if(getResultListAnyoTabShow().size() > 0){
			return true;
		}
		return false;
	}
	
	public List<Integer> getResultListAnyoTabShow() {
		if(resultListAnyoFasesTabShow == null){
			resultListAnyoFasesTabShow = new ArrayList<>();
			
			for (HistoricoCampeonatosAsturias2021 historicoCampeonatosAsturias : getResultListHistoricoCampeonatoAsturiasAll()) {
				if(!resultListAnyoFasesTabShow.contains(historicoCampeonatosAsturias.getAnyo())){
					resultListAnyoFasesTabShow.add(historicoCampeonatosAsturias.getAnyo());
				}
			}
			
		}
		
		//Ordenar la clasificacion
		Collections.sort(resultListAnyoFasesTabShow, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				Integer cec1 = (Integer) o1;
				Integer cec2 = (Integer) o2;
				//1. Ordenar por anyo descendente 
				int rpuntos = cec1.compareTo(cec2);
					return rpuntos;
			}
		});
		
		Collections.reverse(resultListAnyoFasesTabShow);
		
		return resultListAnyoFasesTabShow;
	}

	public Object selected() {
		// TODO Auto-generated method stub
		return null;
	}

	public Object read(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	public String insert(Object insert) {
		// TODO Auto-generated method stub
		return null;
	}

	public String update(Object update) {
		// TODO Auto-generated method stub
		return null;
	}

	public String delete(Object delete) {
		// TODO Auto-generated method stub
		return null;
	}
		

}
