package com.bolocelta.facade.usuarios;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;

import com.bolocelta.bbdd.readTables.usuarios.LeerRoles2021;
import com.bolocelta.entities.usuarios.Roles2021;

@Named
@ConversationScoped
@ManagedBean
public class RolesFacade2021 implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private LeerRoles2021 leerRoles = new LeerRoles2021();
	
	private List<Roles2021> resultList = null;

	public List<Roles2021> getResultList() {
		if(resultList == null){
			resultList = (List<Roles2021>) leerRoles.listResult();
		}
		return resultList;
	}

	public Object selected() {
		// TODO Auto-generated method stub
		return null;
	}

	public Object read(Integer id) {
		return leerRoles.read(id);
	}

	public String insert(Object insert) {
		// TODO Auto-generated method stub
		return null;
	}

	public String update(Object update) {
		// TODO Auto-generated method stub
		return null;
	}

	public String delete(Object delete) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
	

}
