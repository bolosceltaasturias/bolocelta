package com.bolocelta.facade.usuarios;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;

import com.bolocelta.bbdd.readTables.usuarios.LeerPermisos2021;
import com.bolocelta.entities.usuarios.Permisos2021;

@Named
@ConversationScoped
@ManagedBean
public class PermisosFacade2021 implements Serializable {

	private static final long serialVersionUID = 1L;

	private LeerPermisos2021 leerPermisos = new LeerPermisos2021();

	private List<Permisos2021> resultList = null;

	public List<Permisos2021> getResultList() {
		if (resultList == null) {
			resultList = (List<Permisos2021>) leerPermisos.listResult();
		}
		return resultList;
	}
	
	public List<Permisos2021> getResultListByRol(Integer rolId) {
		if (resultList == null) {
			resultList = (List<Permisos2021>) leerPermisos.listResultByRol(rolId);
		}
		return resultList;
	}

	public Object selected() {
		// TODO Auto-generated method stub
		return null;
	}

	public Object read(Integer id) {
		return leerPermisos.read(id);
	}

	public String insert(Object insert) {
		// TODO Auto-generated method stub
		return null;
	}

	public String update(Object update) {
		// TODO Auto-generated method stub
		return null;
	}

	public String delete(Object delete) {
		// TODO Auto-generated method stub
		return null;
	}

}
