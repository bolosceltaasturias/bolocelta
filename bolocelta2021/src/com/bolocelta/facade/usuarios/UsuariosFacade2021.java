package com.bolocelta.facade.usuarios;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;

import com.bolocelta.bbdd.readTables.usuarios.LeerUsuarios2021;
import com.bolocelta.entities.usuarios.Usuarios2021;

@Named
@ConversationScoped
@ManagedBean
public class UsuariosFacade2021 implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private LeerUsuarios2021 leerUsuarios = new LeerUsuarios2021();
	
	private List<Usuarios2021> resultList = null;

	public List<Usuarios2021> getResultList() {
		if(resultList == null){
			resultList = (List<Usuarios2021>) leerUsuarios.listResult();
		}
		return resultList;
	}

	public Object selected() {
		// TODO Auto-generated method stub
		return null;
	}

	public Object read(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Usuarios2021 read(String user, String pass) {
		return leerUsuarios.read(user, pass);
	}

	public String insert(Object insert) {
		// TODO Auto-generated method stub
		return null;
	}

	public String update(Object update) {
		// TODO Auto-generated method stub
		return null;
	}

	public String delete(Object delete) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
	

}
