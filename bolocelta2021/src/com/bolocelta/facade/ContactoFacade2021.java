package com.bolocelta.facade;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.inject.Named;

@Named
@ManagedBean
public class ContactoFacade2021 implements Serializable {

	private static final long serialVersionUID = 1L;

	public String titleOrganizacion = "Organizacion";
	public String titleSanciones = "Sanciones";
	public String titleCompeticiones = "Competiciones";

	public String emailText = "Email:  ";

	public String emailOrganizacion = "organizacion.asociacion.bc@gmail.com";
	public String emailSanciones = "sanciones.asociacion.bc@gmail.com";
	public String emailCompeticiones = "competiciones.asociacion.bc@gmail.com";
	
	public String detalleOrganizacion = "Contacto y gestión con todo aquello que tenga que ver con la organización de la Asociación de Bolo Celta.";
	public String detalleSanciones = "Contacto y gestión todos los tramites pertenecientes a Sanciones.";
	public String detalleCompeticiones = "Contacto y gestión para todas las competiciones de la modalidad.";

	public String getTitleOrganizacion() {
		return titleOrganizacion;
	}

	public void setTitleOrganizacion(String titleOrganizacion) {
		this.titleOrganizacion = titleOrganizacion;
	}

	public String getTitleSanciones() {
		return titleSanciones;
	}

	public void setTitleSanciones(String titleSanciones) {
		this.titleSanciones = titleSanciones;
	}

	public String getTitleCompeticiones() {
		return titleCompeticiones;
	}

	public void setTitleCompeticiones(String titleCompeticiones) {
		this.titleCompeticiones = titleCompeticiones;
	}

	public String getEmailText() {
		return emailText;
	}

	public void setEmailText(String emailText) {
		this.emailText = emailText;
	}

	public String getEmailOrganizacion() {
		return emailOrganizacion;
	}

	public void setEmailOrganizacion(String emailOrganizacion) {
		this.emailOrganizacion = emailOrganizacion;
	}

	public String getEmailSanciones() {
		return emailSanciones;
	}

	public void setEmailSanciones(String emailSanciones) {
		this.emailSanciones = emailSanciones;
	}

	public String getEmailCompeticiones() {
		return emailCompeticiones;
	}

	public void setEmailCompeticiones(String emailCompeticiones) {
		this.emailCompeticiones = emailCompeticiones;
	}

	public String getDetalleOrganizacion() {
		return detalleOrganizacion;
	}

	public void setDetalleOrganizacion(String detalleOrganizacion) {
		this.detalleOrganizacion = detalleOrganizacion;
	}

	public String getDetalleSanciones() {
		return detalleSanciones;
	}

	public void setDetalleSanciones(String detalleSanciones) {
		this.detalleSanciones = detalleSanciones;
	}

	public String getDetalleCompeticiones() {
		return detalleCompeticiones;
	}

	public void setDetalleCompeticiones(String detalleCompeticiones) {
		this.detalleCompeticiones = detalleCompeticiones;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
