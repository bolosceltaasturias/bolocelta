package com.bolocelta.facade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.inject.Named;

import com.bolocelta.application.enumerations.CategoriasEnumeration2021;
import com.bolocelta.application.session.SessionState;
import com.bolocelta.bbdd.constants.Activo2021;
import com.bolocelta.bbdd.constants.Categorias2021;
import com.bolocelta.bbdd.readTables.LeerEquipos2021;
import com.bolocelta.bbdd.readTables.LeerPremiosMVP2021;
import com.bolocelta.bbdd.readTables.LeerVotacionesMVP2021;
import com.bolocelta.entities.Equipos2021;
import com.bolocelta.entities.PremiosMVP2021;
import com.bolocelta.entities.VotacionesMVP2021;

@Named
@ConversationScoped
@ManagedBean
public class PremiosMVPFacade2021 implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private SessionState sessionState;
	
	private LeerPremiosMVP2021 leerPremiosMVP2021 = new LeerPremiosMVP2021();
	private LeerVotacionesMVP2021 leerVotacionesMVP2021 = new LeerVotacionesMVP2021();
	
	private LeerEquipos2021 leerEquipos2021 = new LeerEquipos2021();
	
	//private CrearCampeonatoLigaInfantil2021 campeonatoLigaInfantil = new CrearCampeonatoLigaInfantil2021();
	
	private List<PremiosMVP2021> resultListPremiosPrimera = null;
	private List<PremiosMVP2021> resultListPremiosSegunda = null;
	private List<PremiosMVP2021> resultListPremiosTercera = null;
	private List<PremiosMVP2021> resultListPremiosJuvenil = null;
	private List<PremiosMVP2021> resultListPremiosVeterano = null;
	private List<PremiosMVP2021> resultListPremiosFemenino = null;
	
	
	private List<VotacionesMVP2021> resultListVotacionesPrimera = null;
	private List<VotacionesMVP2021> resultListVotacionesSegunda = null;
	private List<VotacionesMVP2021> resultListVotacionesTercera = null;
	private List<VotacionesMVP2021> resultListVotacionesJuvenil = null;
	private List<VotacionesMVP2021> resultListVotacionesVeterano = null;
	private List<VotacionesMVP2021> resultListVotacionesFemenino = null;
	
	private List<Equipos2021> resultListEquiposVotaciones = null;

	public List<PremiosMVP2021> getResultListPremiosPrimera() {
		if(resultListPremiosPrimera == null){
			resultListPremiosPrimera = (List<PremiosMVP2021>) leerPremiosMVP2021.listResultPremios(Categorias2021.PRIMERA);
		}
		
		//Ordenar la clasificacion
		Collections.sort(resultListPremiosPrimera, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				PremiosMVP2021 cec1 = (PremiosMVP2021) o1;
				PremiosMVP2021 cec2 = (PremiosMVP2021) o2;
				
				int rpuntos = cec1.getTotalPuntos().compareTo(cec2.getTotalPuntos());
				if (rpuntos == 0) {
					String nomb1 = cec1.getJugador().getApodo();
					String nomb2 = cec2.getJugador().getApodo();
					int rdifnomb = nomb2.compareTo(nomb1);
					return rdifnomb;
				}
				return rpuntos;
			}
		});
		
		Collections.reverse(resultListPremiosPrimera);
		
		return resultListPremiosPrimera;
	}
	
	public List<PremiosMVP2021> getResultListPremiosSegunda() {
		if(resultListPremiosSegunda == null){
			resultListPremiosSegunda = (List<PremiosMVP2021>) leerPremiosMVP2021.listResultPremios(Categorias2021.SEGUNDA);
		}
		
		//Ordenar la clasificacion
		Collections.sort(resultListPremiosSegunda, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				PremiosMVP2021 cec1 = (PremiosMVP2021) o1;
				PremiosMVP2021 cec2 = (PremiosMVP2021) o2;
				
				int rpuntos = cec1.getTotalPuntos().compareTo(cec2.getTotalPuntos());
				if (rpuntos == 0) {
					String nomb1 = cec1.getJugador().getApodo();
					String nomb2 = cec2.getJugador().getApodo();
					int rdifnomb = nomb2.compareTo(nomb1);
					return rdifnomb;
				}
				return rpuntos;
			}
		});
		
		Collections.reverse(resultListPremiosSegunda);
		
		return resultListPremiosSegunda;
	}
	
	public List<PremiosMVP2021> getResultListPremiosTercera() {
		if(resultListPremiosTercera == null){
			resultListPremiosTercera = (List<PremiosMVP2021>) leerPremiosMVP2021.listResultPremios(Categorias2021.TERCERA);
		}
		
		//Ordenar la clasificacion
		Collections.sort(resultListPremiosTercera, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				PremiosMVP2021 cec1 = (PremiosMVP2021) o1;
				PremiosMVP2021 cec2 = (PremiosMVP2021) o2;
				
				int rpuntos = cec1.getTotalPuntos().compareTo(cec2.getTotalPuntos());
				if (rpuntos == 0) {
					String nomb1 = cec1.getJugador().getApodo();
					String nomb2 = cec2.getJugador().getApodo();
					int rdifnomb = nomb2.compareTo(nomb1);
					return rdifnomb;
				}
				return rpuntos;
			}
		});
		
		Collections.reverse(resultListPremiosTercera);
		
		return resultListPremiosTercera;
	}
	
	public List<PremiosMVP2021> getResultListPremiosFemenino() {
		if(resultListPremiosFemenino == null){
			resultListPremiosFemenino = (List<PremiosMVP2021>) leerPremiosMVP2021.listResultPremios(Categorias2021.FEMENINO);
		}
		
		//Ordenar la clasificacion
		Collections.sort(resultListPremiosFemenino, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				PremiosMVP2021 cec1 = (PremiosMVP2021) o1;
				PremiosMVP2021 cec2 = (PremiosMVP2021) o2;
				
				int rpuntos = cec1.getTotalPuntos().compareTo(cec2.getTotalPuntos());
				if (rpuntos == 0) {
					String nomb1 = cec1.getJugador().getApodo();
					String nomb2 = cec2.getJugador().getApodo();
					int rdifnomb = nomb2.compareTo(nomb1);
					return rdifnomb;
				}
				return rpuntos;
			}
		});

		Collections.reverse(resultListPremiosFemenino);
		
		return resultListPremiosFemenino;
	}
	
	public List<PremiosMVP2021> getResultListPremiosJuvenil() {
		if(resultListPremiosJuvenil == null){
			resultListPremiosJuvenil = new ArrayList<PremiosMVP2021>();
			for (PremiosMVP2021 premiosMVP2021 : getResultListPremiosPrimera()) {
				if(premiosMVP2021.getEsJuvenil().equalsIgnoreCase(Activo2021.SI)){
					resultListPremiosJuvenil.add(premiosMVP2021);
				}
			}
			for (PremiosMVP2021 premiosMVP2021 : getResultListPremiosSegunda()) {
				if(premiosMVP2021.getEsJuvenil().equalsIgnoreCase(Activo2021.SI)){
					resultListPremiosJuvenil.add(premiosMVP2021);
				}
			}
			for (PremiosMVP2021 premiosMVP2021 : getResultListPremiosTercera()) {
				if(premiosMVP2021.getEsJuvenil().equalsIgnoreCase(Activo2021.SI)){
					resultListPremiosJuvenil.add(premiosMVP2021);
				}
			}
		}
		
		//Ordenar la clasificacion
		Collections.sort(resultListPremiosJuvenil, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				PremiosMVP2021 cec1 = (PremiosMVP2021) o1;
				PremiosMVP2021 cec2 = (PremiosMVP2021) o2;
				
				int rpuntos = cec1.getTotalPuntos().compareTo(cec2.getTotalPuntos());
				if (rpuntos == 0) {
					String nomb1 = cec1.getJugador().getApodo();
					String nomb2 = cec2.getJugador().getApodo();
					int rdifnomb = nomb2.compareTo(nomb1);
					return rdifnomb;
				}
				return rpuntos;
			}
		});
		
		Collections.reverse(resultListPremiosJuvenil);
		
		return resultListPremiosJuvenil;
	}
	
	public List<PremiosMVP2021> getResultListPremiosVeterano() {
		if(resultListPremiosVeterano == null){
			resultListPremiosVeterano = new ArrayList<PremiosMVP2021>();
			for (PremiosMVP2021 premiosMVP2021 : getResultListPremiosPrimera()) {
				if(premiosMVP2021.getEsVeterano().equalsIgnoreCase(Activo2021.SI)){
					resultListPremiosVeterano.add(premiosMVP2021);
				}
			}
			for (PremiosMVP2021 premiosMVP2021 : getResultListPremiosSegunda()) {
				if(premiosMVP2021.getEsVeterano().equalsIgnoreCase(Activo2021.SI)){
					resultListPremiosVeterano.add(premiosMVP2021);
				}
			}
			for (PremiosMVP2021 premiosMVP2021 : getResultListPremiosTercera()) {
				if(premiosMVP2021.getEsVeterano().equalsIgnoreCase(Activo2021.SI)){
					resultListPremiosVeterano.add(premiosMVP2021);
				}
			}
		}
		
		//Ordenar la clasificacion
		Collections.sort(resultListPremiosVeterano, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				PremiosMVP2021 cec1 = (PremiosMVP2021) o1;
				PremiosMVP2021 cec2 = (PremiosMVP2021) o2;
				
				int rpuntos = cec1.getTotalPuntos().compareTo(cec2.getTotalPuntos());
				if (rpuntos == 0) {
					String nomb1 = cec1.getJugador().getApodo();
					String nomb2 = cec2.getJugador().getApodo();
					int rdifnomb = nomb2.compareTo(nomb1);
					return rdifnomb;
				}
				return rpuntos;
			}
		});
		
		Collections.reverse(resultListPremiosVeterano);
		
		return resultListPremiosVeterano;
	}

	public Object selected() {
		// TODO Auto-generated method stub
		return null;
	}

	public Object read(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	public String insert(Object insert) {
		// TODO Auto-generated method stub
		return null;
	}

	public String update(Object update) {
		// TODO Auto-generated method stub
		return null;
	}

	public String delete(Object delete) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Integer getRowsPaginatorClasificacion(){
		return 10;
	}
	
	public List<Equipos2021> getResultListEquiposVotaciones() {
		if(resultListEquiposVotaciones == null){
			resultListEquiposVotaciones = (List<Equipos2021>) leerEquipos2021.listResultVotaciones();
		}
			
		return resultListEquiposVotaciones;
	}
	
	public List<VotacionesMVP2021> getResultListVotacionesPrimera() {
		if(resultListVotacionesPrimera == null){
			resultListVotacionesPrimera = (List<VotacionesMVP2021>) leerVotacionesMVP2021.listResultVotaciones(Categorias2021.PRIMERA);
		}
		
		//Ordenar la clasificacion
		Collections.sort(resultListVotacionesPrimera, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				VotacionesMVP2021 cec1 = (VotacionesMVP2021) o1;
				VotacionesMVP2021 cec2 = (VotacionesMVP2021) o2;
				
				int rpuntos = cec1.getTotalPuntos().compareTo(cec2.getTotalPuntos());
				if (rpuntos == 0) {
					String nomb1 = cec1.getJugador().getApodo();
					String nomb2 = cec2.getJugador().getApodo();
					int rdifnomb = nomb2.compareTo(nomb1);
					return rdifnomb;
				}
				return rpuntos;
			}
		});
		
		Collections.reverse(resultListVotacionesPrimera);
		
		return resultListVotacionesPrimera;
	}
	
	public List<VotacionesMVP2021> getResultListVotacionesSegunda() {
		if(resultListVotacionesSegunda == null){
			resultListVotacionesSegunda = (List<VotacionesMVP2021>) leerVotacionesMVP2021.listResultVotaciones(Categorias2021.SEGUNDA);
		}
		
		//Ordenar la clasificacion
		Collections.sort(resultListVotacionesSegunda, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				VotacionesMVP2021 cec1 = (VotacionesMVP2021) o1;
				VotacionesMVP2021 cec2 = (VotacionesMVP2021) o2;
				
				int rpuntos = cec1.getTotalPuntos().compareTo(cec2.getTotalPuntos());
				if (rpuntos == 0) {
					String nomb1 = cec1.getJugador().getApodo();
					String nomb2 = cec2.getJugador().getApodo();
					int rdifnomb = nomb2.compareTo(nomb1);
					return rdifnomb;
				}
				return rpuntos;
			}
		});
		
		Collections.reverse(resultListVotacionesSegunda);
		
		return resultListVotacionesSegunda;
	}
	
	public List<VotacionesMVP2021> getResultListVotacionesTercera() {
		if(resultListVotacionesTercera == null){
			resultListVotacionesTercera = (List<VotacionesMVP2021>) leerVotacionesMVP2021.listResultVotaciones(Categorias2021.TERCERA);
		}
		
		//Ordenar la clasificacion
		Collections.sort(resultListVotacionesTercera, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				VotacionesMVP2021 cec1 = (VotacionesMVP2021) o1;
				VotacionesMVP2021 cec2 = (VotacionesMVP2021) o2;
				
				int rpuntos = cec1.getTotalPuntos().compareTo(cec2.getTotalPuntos());
				if (rpuntos == 0) {
					String nomb1 = cec1.getJugador().getApodo();
					String nomb2 = cec2.getJugador().getApodo();
					int rdifnomb = nomb2.compareTo(nomb1);
					return rdifnomb;
				}
				return rpuntos;
			}
		});
		
		Collections.reverse(resultListVotacionesTercera);
		
		return resultListVotacionesTercera;
	}
	
	public List<VotacionesMVP2021> getResultListVotacionesVeterano() {
		if(resultListVotacionesVeterano == null){
			resultListVotacionesVeterano = (List<VotacionesMVP2021>) leerVotacionesMVP2021.listResultVotaciones(Categorias2021.VETERANOS);
		}
		
		//Ordenar la clasificacion
		Collections.sort(resultListVotacionesVeterano, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				VotacionesMVP2021 cec1 = (VotacionesMVP2021) o1;
				VotacionesMVP2021 cec2 = (VotacionesMVP2021) o2;
				
				int rpuntos = cec1.getTotalPuntos().compareTo(cec2.getTotalPuntos());
				if (rpuntos == 0) {
					String nomb1 = cec1.getJugador().getApodo();
					String nomb2 = cec2.getJugador().getApodo();
					int rdifnomb = nomb2.compareTo(nomb1);
					return rdifnomb;
				}
				return rpuntos;
			}
		});
		
		Collections.reverse(resultListVotacionesVeterano);
		
		return resultListVotacionesVeterano;
	}
	
	public List<VotacionesMVP2021> getResultListVotacionesJuvenil() {
		if(resultListVotacionesJuvenil == null){
			resultListVotacionesJuvenil = (List<VotacionesMVP2021>) leerVotacionesMVP2021.listResultVotaciones(Categorias2021.JUVENILES);
		}
		
		//Ordenar la clasificacion
		Collections.sort(resultListVotacionesJuvenil, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				VotacionesMVP2021 cec1 = (VotacionesMVP2021) o1;
				VotacionesMVP2021 cec2 = (VotacionesMVP2021) o2;
				
				int rpuntos = cec1.getTotalPuntos().compareTo(cec2.getTotalPuntos());
				if (rpuntos == 0) {
					String nomb1 = cec1.getJugador().getApodo();
					String nomb2 = cec2.getJugador().getApodo();
					int rdifnomb = nomb2.compareTo(nomb1);
					return rdifnomb;
				}
				return rpuntos;
			}
		});
		
		Collections.reverse(resultListVotacionesJuvenil);
		
		return resultListVotacionesJuvenil;
	}
	
	public List<VotacionesMVP2021> getResultListVotacionesFemenino() {
		if(resultListVotacionesFemenino == null){
			resultListVotacionesFemenino = (List<VotacionesMVP2021>) leerVotacionesMVP2021.listResultVotaciones(Categorias2021.FEMENINO);
		}
		
		//Ordenar la clasificacion
		Collections.sort(resultListVotacionesFemenino, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				VotacionesMVP2021 cec1 = (VotacionesMVP2021) o1;
				VotacionesMVP2021 cec2 = (VotacionesMVP2021) o2;
				
				int rpuntos = cec1.getTotalPuntos().compareTo(cec2.getTotalPuntos());
				if (rpuntos == 0) {
					String nomb1 = cec1.getJugador().getApodo();
					String nomb2 = cec2.getJugador().getApodo();
					int rdifnomb = nomb2.compareTo(nomb1);
					return rdifnomb;
				}
				return rpuntos;
			}
		});
		
		Collections.reverse(resultListVotacionesFemenino);
		
		return resultListVotacionesFemenino;
	}
	
	public Integer buscarValorVotacionEquipo(Integer categoria, Integer equipo, Integer idJugador){
		if(categoria.equals(Categorias2021.FEMENINO)){
			for (VotacionesMVP2021 votacionesMVP2021 : getResultListVotacionesFemenino()) {
				if(votacionesMVP2021.getIdJugador().equals(idJugador)){
					return votacionesMVP2021.getEquipoPuntos().get(equipo);
				}
			}
		}
		if(categoria.equals(Categorias2021.JUVENILES)){
			for (VotacionesMVP2021 votacionesMVP2021 : getResultListVotacionesJuvenil()) {
				if(votacionesMVP2021.getIdJugador().equals(idJugador)){
					return votacionesMVP2021.getEquipoPuntos().get(equipo);
				}
			}
		}
		if(categoria.equals(Categorias2021.VETERANOS)){
			for (VotacionesMVP2021 votacionesMVP2021 : getResultListVotacionesVeterano()) {
				if(votacionesMVP2021.getIdJugador().equals(idJugador)){
					return votacionesMVP2021.getEquipoPuntos().get(equipo);
				}
			}
		}
		if(categoria.equals(Categorias2021.TERCERA)){
			for (VotacionesMVP2021 votacionesMVP2021 : getResultListVotacionesTercera()) {
				if(votacionesMVP2021.getIdJugador().equals(idJugador)){
					return votacionesMVP2021.getEquipoPuntos().get(equipo);
				}
			}
		}
		if(categoria.equals(Categorias2021.SEGUNDA)){
			for (VotacionesMVP2021 votacionesMVP2021 : getResultListVotacionesSegunda()) {
				if(votacionesMVP2021.getIdJugador().equals(idJugador)){
					return votacionesMVP2021.getEquipoPuntos().get(equipo);
				}
			}
		}
		if(categoria.equals(Categorias2021.PRIMERA)){
			for (VotacionesMVP2021 votacionesMVP2021 : getResultListVotacionesPrimera()) {
				if(votacionesMVP2021.getIdJugador().equals(idJugador)){
					return votacionesMVP2021.getEquipoPuntos().get(equipo);
				}
			}
		}
		return 0;
	}
	
}
