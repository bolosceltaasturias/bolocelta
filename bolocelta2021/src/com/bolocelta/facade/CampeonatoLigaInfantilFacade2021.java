package com.bolocelta.facade;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.bolocelta.application.enumerations.CategoriasInfantilEnumeration2021;
import com.bolocelta.application.session.SessionState;
import com.bolocelta.bbdd.constants.Activo2021;
import com.bolocelta.bbdd.createTable.CrearCampeonatoLigaInfantil2021;
import com.bolocelta.bbdd.readTables.LeerCampeonatoLigaInfantil2021;
import com.bolocelta.entities.CampeonatoLigaInfantilCalendario2021;

@Named
@ConversationScoped
@ManagedBean
public class CampeonatoLigaInfantilFacade2021 implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private SessionState sessionState;
	
	private LeerCampeonatoLigaInfantil2021 leerCampeonatoLigaInfantil = new LeerCampeonatoLigaInfantil2021();
	
	private CrearCampeonatoLigaInfantil2021 campeonatoLigaInfantil = new CrearCampeonatoLigaInfantil2021();
	
	private List<CampeonatoLigaInfantilCalendario2021> resultListClasificacionMini = null;
	private List<CampeonatoLigaInfantilCalendario2021> resultListClasificacionBenjamin = null;
	private List<CampeonatoLigaInfantilCalendario2021> resultListClasificacionAlevin = null;
	private List<CampeonatoLigaInfantilCalendario2021> resultListClasificacionInfantil = null;
	private List<CampeonatoLigaInfantilCalendario2021> resultListClasificacionCadete = null;
	private List<CampeonatoLigaInfantilCalendario2021> resultListCalendario = null;

	public List<CampeonatoLigaInfantilCalendario2021> getResultListClasificacionMini() {
		if(resultListClasificacionMini == null){
			resultListClasificacionMini = (List<CampeonatoLigaInfantilCalendario2021>) leerCampeonatoLigaInfantil.listResultClasificacion(CategoriasInfantilEnumeration2021.CATEGORIA_MINI);
		}
		return resultListClasificacionMini;
	}
	
	public List<CampeonatoLigaInfantilCalendario2021> getResultListClasificacionBenjamin() {
		if(resultListClasificacionBenjamin == null){
			resultListClasificacionBenjamin = (List<CampeonatoLigaInfantilCalendario2021>) leerCampeonatoLigaInfantil.listResultClasificacion(CategoriasInfantilEnumeration2021.CATEGORIA_BENJAMIN);
		}
		return resultListClasificacionBenjamin;
	}
	
	public List<CampeonatoLigaInfantilCalendario2021> getResultListClasificacionAlevin() {
		if(resultListClasificacionAlevin == null){
			resultListClasificacionAlevin = (List<CampeonatoLigaInfantilCalendario2021>) leerCampeonatoLigaInfantil.listResultClasificacion(CategoriasInfantilEnumeration2021.CATEGORIA_ALEVIN);
		}
		return resultListClasificacionAlevin;
	}
	
	public List<CampeonatoLigaInfantilCalendario2021> getResultListClasificacionInfantil() {
		if(resultListClasificacionInfantil == null){
			resultListClasificacionInfantil = (List<CampeonatoLigaInfantilCalendario2021>) leerCampeonatoLigaInfantil.listResultClasificacion(CategoriasInfantilEnumeration2021.CATEGORIA_INFANTIL);
		}
		return resultListClasificacionInfantil;
	}
	
	public List<CampeonatoLigaInfantilCalendario2021> getResultListClasificacionCadete() {
		if(resultListClasificacionCadete == null){
			resultListClasificacionCadete = (List<CampeonatoLigaInfantilCalendario2021>) leerCampeonatoLigaInfantil.listResultClasificacion(CategoriasInfantilEnumeration2021.CATEGORIA_CADETE);
		}
		return resultListClasificacionCadete;
	}
	
	public List<CampeonatoLigaInfantilCalendario2021> getResultListCalendario() {
		if(resultListCalendario == null){
			resultListCalendario = (List<CampeonatoLigaInfantilCalendario2021>) leerCampeonatoLigaInfantil.listResultCalendario();
		}
		return resultListCalendario;
	}

	public Object selected() {
		// TODO Auto-generated method stub
		return null;
	}

	public Object read(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	public String insert(Object insert) {
		// TODO Auto-generated method stub
		return null;
	}

	public String update(Object update) {
		// TODO Auto-generated method stub
		return null;
	}

	public String delete(Object delete) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Integer getRowsPaginatorClasificacion(){
		return getResultListCalendario().size()/7;
	}
	
	public Integer getRowsPaginatorCalendario(){
		return getResultListCalendario().size()/7;
	}
	
//	public Integer getTotalRowsClasificacion(){
//		return getResultListClasificacion().size();
//	}
	
	public String getPermisoActualizarResultado(){
    	if(sessionState != null && sessionState.getUserEquipo() != null && sessionState.getUserEquipo().getId().equals(0)){
			return "BCMEGEARLI";
		}
    	return "SIN PERMISO";
    }

	public String getPermisoConfirmarResultado() {
		if(sessionState != null && sessionState.getUserEquipo() != null && sessionState.getUserEquipo().getId().equals(0)){
			return "BCMEGEARLI";
		}
    	return "SIN PERMISO";
	}
		
    public void doActualizarResultadoLiga(CampeonatoLigaInfantilCalendario2021 cec){
    	boolean error = false;
		if(cec.isModificable()){
			//Validaciones
			if(cec.getRonda1() < 0 || cec.getRonda2() < 0 || cec.getRonda3() < 0 || cec.getRonda4() < 0 || cec.getPuntos() < 0){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Las rondas han de tener un valor igual o mayor de 0.", null));
			}
			
			//Si todo correcto actualizar resultado
			if(!error){
				campeonatoLigaInfantil.actualizarResultadoCalendarioLiga(cec);
				leerCampeonatoLigaInfantil = new LeerCampeonatoLigaInfantil2021();
				resultListClasificacionMini = (List<CampeonatoLigaInfantilCalendario2021>) leerCampeonatoLigaInfantil.listResultClasificacion(CategoriasInfantilEnumeration2021.CATEGORIA_MINI);
				resultListClasificacionBenjamin = (List<CampeonatoLigaInfantilCalendario2021>) leerCampeonatoLigaInfantil.listResultClasificacion(CategoriasInfantilEnumeration2021.CATEGORIA_BENJAMIN);
				resultListClasificacionAlevin = (List<CampeonatoLigaInfantilCalendario2021>) leerCampeonatoLigaInfantil.listResultClasificacion(CategoriasInfantilEnumeration2021.CATEGORIA_ALEVIN);
				resultListClasificacionInfantil = (List<CampeonatoLigaInfantilCalendario2021>) leerCampeonatoLigaInfantil.listResultClasificacion(CategoriasInfantilEnumeration2021.CATEGORIA_INFANTIL);
				resultListClasificacionCadete = (List<CampeonatoLigaInfantilCalendario2021>) leerCampeonatoLigaInfantil.listResultClasificacion(CategoriasInfantilEnumeration2021.CATEGORIA_CADETE);
				resultListCalendario = (List<CampeonatoLigaInfantilCalendario2021>) leerCampeonatoLigaInfantil.listResultCalendario();
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha actualizado el resultado de la jugadora " + cec.getNombre(), null));
			}
		}
		
	}
	    
		public void doConfirmarResultado(CampeonatoLigaInfantilCalendario2021 cec){
			boolean error = false;
			if(cec.isModificable()){
				//Validaciones
				if(cec.getRonda1() < 0 || cec.getRonda2() < 0 || cec.getRonda3() < 0 || cec.getRonda4() < 0 || cec.getPuntos() < 0){
					error = true;
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Las rondas han de tener un valor igual o mayor de 0.", null));
				}
				
				//Si todo correcto confirmar resultado y actualizar clasificacion
				if(!error){
					cec.setActivo(Activo2021.NO_NUMBER);
					campeonatoLigaInfantil.actualizarResultadoCalendarioLigaConfirmar(cec);
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha confirmado el resultado de la jugador " + cec.getNombre(), null));
					leerCampeonatoLigaInfantil = new LeerCampeonatoLigaInfantil2021();
					resultListClasificacionMini = (List<CampeonatoLigaInfantilCalendario2021>) leerCampeonatoLigaInfantil.listResultClasificacion(CategoriasInfantilEnumeration2021.CATEGORIA_MINI);
					resultListClasificacionBenjamin = (List<CampeonatoLigaInfantilCalendario2021>) leerCampeonatoLigaInfantil.listResultClasificacion(CategoriasInfantilEnumeration2021.CATEGORIA_BENJAMIN);
					resultListClasificacionAlevin = (List<CampeonatoLigaInfantilCalendario2021>) leerCampeonatoLigaInfantil.listResultClasificacion(CategoriasInfantilEnumeration2021.CATEGORIA_ALEVIN);
					resultListClasificacionInfantil = (List<CampeonatoLigaInfantilCalendario2021>) leerCampeonatoLigaInfantil.listResultClasificacion(CategoriasInfantilEnumeration2021.CATEGORIA_INFANTIL);
					resultListClasificacionCadete = (List<CampeonatoLigaInfantilCalendario2021>) leerCampeonatoLigaInfantil.listResultClasificacion(CategoriasInfantilEnumeration2021.CATEGORIA_CADETE);
					resultListCalendario = (List<CampeonatoLigaInfantilCalendario2021>) leerCampeonatoLigaInfantil.listResultCalendario();
				}
			}
			
			
		}		

}
