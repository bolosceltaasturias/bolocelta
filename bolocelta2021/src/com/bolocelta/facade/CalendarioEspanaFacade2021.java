package com.bolocelta.facade;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;

import com.bolocelta.bbdd.readTables.LeerCalendarioEspana2021;
import com.bolocelta.entities.CalendarioEspana2021;

@Named
@ConversationScoped
@ManagedBean
public class CalendarioEspanaFacade2021 implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private LeerCalendarioEspana2021 leerCalendarioEspana = new LeerCalendarioEspana2021();
	
	private List<CalendarioEspana2021> resultList = null;

	public List<CalendarioEspana2021> getResultList() {
		if(resultList == null){
			resultList = (List<CalendarioEspana2021>) leerCalendarioEspana.listResult();
		}
		return resultList;
	}

	public Object selected() {
		// TODO Auto-generated method stub
		return null;
	}

	public Object read(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	public String insert(Object insert) {
		// TODO Auto-generated method stub
		return null;
	}

	public String update(Object update) {
		// TODO Auto-generated method stub
		return null;
	}

	public String delete(Object delete) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Integer getMaxRowsPaginator(){
		return 10;
	}
	
	public boolean isEnabledPaginator(){
		return getResultList() != null ? (getResultList().size() > getMaxRowsPaginator()) : false;
	}	
	
	

}
