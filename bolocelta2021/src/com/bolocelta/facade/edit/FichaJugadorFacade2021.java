package com.bolocelta.facade.edit;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.bolocelta.application.Redirect;
import com.bolocelta.application.session.SessionState;
import com.bolocelta.bbdd.constants.Activo2021;
import com.bolocelta.bbdd.createTable.CrearJugadores2021;
import com.bolocelta.bbdd.readTables.LeerJugadores2021;
import com.bolocelta.entities.Jugadores2021;

@Named
@ManagedBean
@SessionScoped
public class FichaJugadorFacade2021 implements Serializable {

	private static final String GUION = "-";

	private static final long serialVersionUID = 1L;

	@Inject
	private Redirect redirect;

	@Inject
	private SessionState sessionState;
	
	@Inject
	private FichaEquipoFacade2021 fichaEquipoFacade;

	private LeerJugadores2021 leerJugadores = new LeerJugadores2021();
	private CrearJugadores2021 crearJugadores = new CrearJugadores2021();

	private Jugadores2021 jugador = null;
	
	public boolean isNew(){
		if(this.jugador == null || (this.jugador != null && this.jugador.getRowNum() == null)){
			return true;
		}
		return false;
	}

	public void doSelected(Integer id) {
		this.jugador = (Jugadores2021) leerJugadores.read(id);
		redirect.getJugadorDetalle();
	}
	
	public void doCancel() {
		this.jugador = null;
		redirect.getMiEquipo();
	}
	
	public void doInsert() {
		if(crearJugadores.validate(this.jugador)){
			Long newRowNum = leerJugadores.getLastRowSheet();
			this.jugador.setRowNum(newRowNum+1);
			crearJugadores.preparateInsertRow(this.jugador);
			leerJugadores.refreshList();
			fichaEquipoFacade.doSelectedMyTeam();
		}
	}
	
	public void doSave() {
		if(crearJugadores.validate(this.jugador)){
			crearJugadores.preparateUpdateRow(this.jugador);
			fichaEquipoFacade.doSelectedMyTeam();
		}
	}
	
	public void doDelete() {
		this.jugador.setActivo(Activo2021.NO);
		crearJugadores.preparateDeleteLogicRow(this.jugador);
		fichaEquipoFacade.doSelectedMyTeam();
	}
	
	public void doCreate(){
		this.jugador = new Jugadores2021(fichaEquipoFacade.getEquipo().getId(), Activo2021.SI, Activo2021.NO, Activo2021.NO, Activo2021.NO, Activo2021.NO, GUION);
		redirect.getJugadorDetalle();
	}

	public Jugadores2021 getJugador() {
		return jugador;
	}

	public void setJugador(Jugadores2021 jugador) {
		this.jugador = jugador;
	}
	
}
