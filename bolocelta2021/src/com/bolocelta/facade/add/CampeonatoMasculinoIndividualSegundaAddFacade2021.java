package com.bolocelta.facade.add;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.bolocelta.application.MenuView;
import com.bolocelta.application.Redirect;
import com.bolocelta.application.utils.DateUtils2021;
import com.bolocelta.application.utils.NumberUtils2021;
import com.bolocelta.bbdd.constants.Activo2021;
import com.bolocelta.bbdd.constants.EstadosIndividualParejas2021;
import com.bolocelta.bbdd.constants.FasesModelo2021;
import com.bolocelta.bbdd.constants.GruposLetra2021;
import com.bolocelta.bbdd.constants.TipoEnfrentamiento2021;
import com.bolocelta.bbdd.constants.structure.EstructuraCampeonatoMasculinoIndividualSegunda2021;
import com.bolocelta.bbdd.createTable.CrearCampeonatoMasculinoIndividualSegunda2021;
import com.bolocelta.bbdd.readTables.LeerBoleras2021;
import com.bolocelta.bbdd.readTables.LeerCampeonatoMasculinoIndividualSegunda2021;
import com.bolocelta.entities.Boleras2021;
import com.bolocelta.entities.Categorias2021;
import com.bolocelta.entities.Configuracion2021;
import com.bolocelta.entities.Fases2021;
import com.bolocelta.entities.Jugadores2021;
import com.bolocelta.entities.ParticipantesIndividual2021;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseCF2021;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseFC2021;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseFF2021;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseI2021;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseII2021;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseOF2021;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseSF2021;
import com.bolocelta.entities.sorteos.individual.ClasificacionFaseI2021;
import com.bolocelta.entities.sorteos.individual.ClasificacionFaseII2021;
import com.bolocelta.entities.sorteos.individual.Grupos2021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualCuartosFinal2Grupos2021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualCuartosFinal4Grupos2021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualCuartosFinal4OF2021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualCuartosFinal4OFClasifican32021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualEnfrentamientoCuartosFinal2021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualEnfrentamientoFinal2021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualEnfrentamientoFinalConsolacion2021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualEnfrentamientoGruposFaseI2021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualEnfrentamientoOctavosFinal2021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualEnfrentamientoSemiFinal2021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualFinal4SF2021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualFinalConsolacion4SF2021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualGrupos32021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualGrupos42021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualGrupos52021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualGrupos62021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualGrupos72021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualGrupos82021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualOctavosFinal4Grupos2021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualOctavosFinal4GruposClasifican32021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualSemiFinal4CF2021;

@Named
@ManagedBean
@RequestScoped
public class CampeonatoMasculinoIndividualSegundaAddFacade2021 implements Serializable{
	
	private static final int CLASIFICAN_3 = 3;

	@Inject
	private Redirect redirect;
	
	@Inject
	private MenuView menuView;
	
	private static final long serialVersionUID = 1L;
	
	private LeerCampeonatoMasculinoIndividualSegunda2021 leerCampeonatoMasculinoIndividual = new LeerCampeonatoMasculinoIndividualSegunda2021();
	private CrearCampeonatoMasculinoIndividualSegunda2021 crearCampeonatoMasculinoIndividual = new CrearCampeonatoMasculinoIndividualSegunda2021();
	
	private LeerBoleras2021 leerBoleras = new LeerBoleras2021();
	
	private String diaFechaMaxInscripcion = null;
	private String mesFechaMaxInscripcion = null;
	private String anyoFechaMaxInscripcion = null;
	
	private Date fechaMaxInscripcion = null;
	private String estadoCampeonato = null;
	private String bolera = null;
	private String observacionesCampeonato1 = null;
	private String observacionesCampeonato2 = null;
	private String observacionesCampeonato3 = null;
	private String observacionesCampeonato4 = null;
	private String observacionesCampeonato5 = null;
	private String observacionesCampeonato6 = null;
	private String observacionesCampeonato7 = null;
	private String bolerasOcupadasFI = null;
	private String bolerasOcupadasOFCF = null;
	
	private List<Configuracion2021> resultListConfiguracion = null;
	private List<Fases2021> resultListFases = null;
	private List<ParticipantesIndividual2021> resultListParticipantesIndividual = null;
	
	public CampeonatoMasculinoIndividualSegundaAddFacade2021() {
		Date fechaMaxInscripcion = leerCampeonatoMasculinoIndividual.getFechaMaxInscripcion();
		if(fechaMaxInscripcion != null){
			setDiaFechaMaxInscripcion(String.valueOf(fechaMaxInscripcion.getDate()));
			setMesFechaMaxInscripcion(String.valueOf(fechaMaxInscripcion.getMonth()+1));
			setAnyoFechaMaxInscripcion(String.valueOf(fechaMaxInscripcion.getYear()+1900));
		}
		setEstadoCampeonato(leerCampeonatoMasculinoIndividual.getEstadoCampeonato());
		setBolera(leerCampeonatoMasculinoIndividual.getBoleraFinal());
		setObservacionesCampeonato1(leerCampeonatoMasculinoIndividual.getObservacionesCampeonato1());
		setObservacionesCampeonato2(leerCampeonatoMasculinoIndividual.getObservacionesCampeonato2());
		setObservacionesCampeonato3(leerCampeonatoMasculinoIndividual.getObservacionesCampeonato3());
		setObservacionesCampeonato4(leerCampeonatoMasculinoIndividual.getObservacionesCampeonato4());
		setObservacionesCampeonato5(leerCampeonatoMasculinoIndividual.getObservacionesCampeonato5());
		setObservacionesCampeonato6(leerCampeonatoMasculinoIndividual.getObservacionesCampeonato6());
		setObservacionesCampeonato7(leerCampeonatoMasculinoIndividual.getObservacionesCampeonato7());
		setBolerasOcupadasFI(leerCampeonatoMasculinoIndividual.getBolerasOcupadasFI());
		setBolerasOcupadasOFCF(leerCampeonatoMasculinoIndividual.getBolerasOcupadasOFCF());
	}

	public List<Configuracion2021> getResultListConfiguracion() {
		if(resultListConfiguracion == null){
			resultListConfiguracion = (List<Configuracion2021>) leerCampeonatoMasculinoIndividual.listResultConfig();
		}
		return resultListConfiguracion;
	}
	
	public List<Fases2021> getResultListFases() {
		if(resultListFases == null){
			resultListFases = (List<Fases2021>) leerCampeonatoMasculinoIndividual.listResultFases();
		}
		return resultListFases;
	}
	
	public List<ParticipantesIndividual2021> getResultListParticipantes() {
		if(resultListParticipantesIndividual == null){
			resultListParticipantesIndividual = (List<ParticipantesIndividual2021>) leerCampeonatoMasculinoIndividual.listResultParticipantesOrderByEquipo();
		}
		return resultListParticipantesIndividual;
	}
	
	//NUEVO
	public List<ParticipantesIndividual2021> getResultListParticipantesByEquipos(Set<Integer> keySetequipoMasJugadoresQueGruposLibreGrupo) {
		if(resultListParticipantesIndividual == null){
			resultListParticipantesIndividual = (List<ParticipantesIndividual2021>) leerCampeonatoMasculinoIndividual.listResultParticipantesOrderByEquipo();
		}
		List<ParticipantesIndividual2021> resultListParticipantesIndividualMasJugadoresQueGrupos = new ArrayList<>();
		for (ParticipantesIndividual2021 participantesIndividual : resultListParticipantesIndividual) {
			if(keySetequipoMasJugadoresQueGruposLibreGrupo.contains(participantesIndividual.getJugador().getEquipoId())){
				resultListParticipantesIndividualMasJugadoresQueGrupos.add(participantesIndividual);
			}
		}
		return resultListParticipantesIndividualMasJugadoresQueGrupos;
	}
	//F-NUEVO
	
	public Integer getTotalRowsParticipantes(){
		if(resultListParticipantesIndividual == null){
			getResultListParticipantes();
		}
		if(resultListParticipantesIndividual != null){
			return resultListParticipantesIndividual.size();
		}
		return 0;
	}
	
	public Integer getMaxRowsPaginator(){
		return 8;
	}
	
	public boolean isEnabledPaginatorParticipantes(){
		return (getResultListParticipantes() != null && getResultListParticipantes() != null) ? 
					(getResultListParticipantes().size() > getMaxRowsPaginator()) : false;
	}
	
	//Manejo de estados
	public boolean isEstadoCampeonatoSinCrear(){
		return leerCampeonatoMasculinoIndividual.isEstadoCampeonatoSinCrear();
	}
	
	public boolean isAbrirInscripciones(){
		return leerCampeonatoMasculinoIndividual.isAbrirInscripciones();
	}
	
	public boolean isCerrarInscripciones(){
		return leerCampeonatoMasculinoIndividual.isCerrarInscripciones();
	}
	
	public boolean isInscripcionesCerradas(){
		return leerCampeonatoMasculinoIndividual.isInscripcionesCerradas();
	}
	
	public boolean isFasesCerradas(){
		return leerCampeonatoMasculinoIndividual.isFasesCerradas();
	}
	
	public boolean isSorteoRealizado(){
		return leerCampeonatoMasculinoIndividual.isSorteoRealizado();
	}
	
	public boolean isSorteoFinalizado(){
		return leerCampeonatoMasculinoIndividual.isSorteoFinalizado();
	}
	
	//Acciones para los diferentes estados
	
	private boolean validateFecha() {
		if(getAnyoFechaMaxInscripcion() != null && getMesFechaMaxInscripcion() != null && getDiaFechaMaxInscripcion() != null){
			if(NumberUtils2021.isNumber(getAnyoFechaMaxInscripcion()) && NumberUtils2021.isNumber(getMesFechaMaxInscripcion()) && NumberUtils2021.isNumber(getDiaFechaMaxInscripcion())){
				Integer dia = Integer.valueOf(getDiaFechaMaxInscripcion());
				if(dia > 31){
					return false;
				}
				Integer mes = Integer.valueOf(getMesFechaMaxInscripcion());
				if(mes > 12){
					return false;
				}
				Integer anyo = Integer.valueOf(getAnyoFechaMaxInscripcion());
				String fecha = (dia < 10 ? "0" + dia : dia) + "/" + (mes < 10 ? "0" + mes : mes) + "/" + anyo;
				Date fechaConvert = DateUtils2021.convertStringToDate(fecha);
				if(fechaConvert == null){
					return false;
				}else{
					setFechaMaxInscripcion(fechaConvert);
					return true;
				}
			}
		}
		return false;
		
	}
	
	public void doCrearBaseCampeonato(){
		
		boolean error = validateFecha();
		if(error){
			crearCampeonatoMasculinoIndividual.crearBaseCampeonato(getFechaMaxInscripcion(), EstadosIndividualParejas2021.CONFIG_ESTADO_CREAR, getBolera());
			//Inicializar variables de nuevo
			leerCampeonatoMasculinoIndividual = new LeerCampeonatoMasculinoIndividualSegunda2021();
			//Redirigir
			redirect.redirect(menuView.PAG_OM_ADD_CAMPEONATO_MASCULINO_INDIVIDUAL_SEGUNDA);
		}else{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "La Fecha max. inscripci�n no es valida.", null));
		}
	}
	
	public void doAbrirInscripcionesCampeonato(){
		crearCampeonatoMasculinoIndividual.actualizarEstadoConfiguracion(EstadosIndividualParejas2021.CONFIG_ESTADO_ABRIR_INSCRIPCIONES);
		//Inicializar variables de nuevo
		leerCampeonatoMasculinoIndividual = new LeerCampeonatoMasculinoIndividualSegunda2021();
		//Redirigir
		redirect.redirect(menuView.PAG_OM_ADD_CAMPEONATO_MASCULINO_INDIVIDUAL_SEGUNDA);
	}
	
	public void doCerrarInscripcionesCampeonato(){
		crearCampeonatoMasculinoIndividual.actualizarEstadoConfiguracion(EstadosIndividualParejas2021.CONFIG_ESTADO_CERRAR_INSCRIPCIONES);
		//Inicializar variables de nuevo
		leerCampeonatoMasculinoIndividual = new LeerCampeonatoMasculinoIndividualSegunda2021();
		//Redirigir
		redirect.redirect(menuView.PAG_OM_ADD_CAMPEONATO_MASCULINO_INDIVIDUAL_SEGUNDA);
	}
	
	public void doInscribirJugador(Jugadores2021 jugador, Categorias2021 categoria, String usuario, String activo){
		ParticipantesIndividual2021 participantesIndividual = leerCampeonatoMasculinoIndividual.existeJugadorComoParticipante(jugador.getId());
		//Si existe se actualiza, sino se a�ade nuevo
		if(participantesIndividual == null){
			crearCampeonatoMasculinoIndividual.inscribirNewJugador(jugador, categoria, usuario, activo);
		}else{
			crearCampeonatoMasculinoIndividual.inscribirUpdateJugador(participantesIndividual, jugador, categoria, usuario, activo);
		}
		//Inicializar variables de nuevo
		leerCampeonatoMasculinoIndividual = new LeerCampeonatoMasculinoIndividualSegunda2021();
	}
	
	public Date getFechaMaxInscripcion() {
		return fechaMaxInscripcion;
	}

	public void setFechaMaxInscripcion(Date fechaMaxInscripcion) {
		this.fechaMaxInscripcion = fechaMaxInscripcion;
	}
	
	public String getBolera() {
		return bolera;
	}

	public void setBolera(String bolera) {
		this.bolera = bolera;
	}
	
	public String getNombreBolera(){
		if(this.bolera != null && this.bolera.length() > 0){
			return leerBoleras.read(Integer.valueOf(this.bolera)).getBolera();
		}
		return "";
	}
	
	public String getObservacionesCampeonato1() {
		return observacionesCampeonato1;
	}

	public void setObservacionesCampeonato1(String observacionesCampeonato1) {
		this.observacionesCampeonato1 = observacionesCampeonato1;
	}
	
	public String getObservacionesCampeonato2() {
		return observacionesCampeonato2;
	}

	public void setObservacionesCampeonato2(String observacionesCampeonato2) {
		this.observacionesCampeonato2 = observacionesCampeonato2;
	}
	
	public String getObservacionesCampeonato3() {
		return observacionesCampeonato3;
	}

	public void setObservacionesCampeonato3(String observacionesCampeonato3) {
		this.observacionesCampeonato3 = observacionesCampeonato3;
	}
	
	public String getObservacionesCampeonato4() {
		return observacionesCampeonato4;
	}

	public void setObservacionesCampeonato4(String observacionesCampeonato4) {
		this.observacionesCampeonato4 = observacionesCampeonato4;
	}
	
	public String getObservacionesCampeonato5() {
		return observacionesCampeonato5;
	}

	public void setObservacionesCampeonato5(String observacionesCampeonato5) {
		this.observacionesCampeonato5 = observacionesCampeonato5;
	}
	
	public String getObservacionesCampeonato6() {
		return observacionesCampeonato6;
	}

	public void setObservacionesCampeonato6(String observacionesCampeonato6) {
		this.observacionesCampeonato6 = observacionesCampeonato6;
	}
	
	public String getObservacionesCampeonato7() {
		return observacionesCampeonato7;
	}

	public void setObservacionesCampeonato7(String observacionesCampeonato7) {
		this.observacionesCampeonato7 = observacionesCampeonato7;
	}

	public String getEstadoCampeonato() {
		return estadoCampeonato;
	}

	public void setEstadoCampeonato(String estadoCampeonato) {
		this.estadoCampeonato = estadoCampeonato;
	}
	
	public String getDiaFechaMaxInscripcion() {
		return diaFechaMaxInscripcion;
	}

	public void setDiaFechaMaxInscripcion(String diaFechaMaxInscripcion) {
		this.diaFechaMaxInscripcion = diaFechaMaxInscripcion;
	}

	public String getMesFechaMaxInscripcion() {
		return mesFechaMaxInscripcion;
	}

	public void setMesFechaMaxInscripcion(String mesFechaMaxInscripcion) {
		this.mesFechaMaxInscripcion = mesFechaMaxInscripcion;
	}

	public String getAnyoFechaMaxInscripcion() {
		return anyoFechaMaxInscripcion;
	}

	public void setAnyoFechaMaxInscripcion(String anyoFechaMaxInscripcion) {
		this.anyoFechaMaxInscripcion = anyoFechaMaxInscripcion;
	}
	
	public void doCambiarActivarFase(Fases2021 fase){
		if(fase.getActivo().equalsIgnoreCase(Activo2021.SI)){
			fase.setActivo(Activo2021.NO);
		}else if(fase.getActivo().equalsIgnoreCase(Activo2021.NO)){
			fase.setActivo(Activo2021.SI);
		}
		crearCampeonatoMasculinoIndividual.actualizarActivoFase(fase);
		leerCampeonatoMasculinoIndividual = new LeerCampeonatoMasculinoIndividualSegunda2021();
	}
	
	public void doActualizarDatos(Fases2021 fase){
		crearCampeonatoMasculinoIndividual.actualizarFase(fase);
		leerCampeonatoMasculinoIndividual = new LeerCampeonatoMasculinoIndividualSegunda2021();
	}

	public void doCerrarFasesCampeonato(){
		//Inicializar variables de nuevo
		leerCampeonatoMasculinoIndividual = new LeerCampeonatoMasculinoIndividualSegunda2021();
		for(Fases2021 fase : getResultListFases()){
			if(fase.getActivo().equals(Activo2021.SI)){
				if(fase.getNumero().equals(FasesModelo2021.FASE_I)){
					crearCampeonatoMasculinoIndividual.crearClasificacionFaseI();
					crearCampeonatoMasculinoIndividual.crearCalendarioFaseI();
				}else if(fase.getNumero().equals(FasesModelo2021.FASE_II)){
					crearCampeonatoMasculinoIndividual.crearCalendarioFaseII();
					crearCampeonatoMasculinoIndividual.crearClasificacionFaseII();
				}else if(fase.getNumero().equals(FasesModelo2021.FASE_OF)){
					crearCampeonatoMasculinoIndividual.crearCalendarioFaseOF();
				}else if(fase.getNumero().equals(FasesModelo2021.FASE_CF)){
					crearCampeonatoMasculinoIndividual.crearCalendarioFaseCF();
				}else if(fase.getNumero().equals(FasesModelo2021.FASE_SF)){
					crearCampeonatoMasculinoIndividual.crearCalendarioFaseSF();
				}else if(fase.getNumero().equals(FasesModelo2021.FASE_FC)){
					crearCampeonatoMasculinoIndividual.crearCalendarioFaseFC();
				}else if(fase.getNumero().equals(FasesModelo2021.FASE_FF)){
					crearCampeonatoMasculinoIndividual.crearCalendarioFaseFF();
				}
			}
		}
		crearCampeonatoMasculinoIndividual.actualizarEstadoConfiguracion(EstadosIndividualParejas2021.CONFIG_ESTADO_CERRAR_FASES);
		//Inicializar variables de nuevo
		leerCampeonatoMasculinoIndividual = new LeerCampeonatoMasculinoIndividualSegunda2021();
		//Redirigir
		redirect.redirect(menuView.PAG_OM_ADD_CAMPEONATO_MASCULINO_INDIVIDUAL_SEGUNDA);
	}
	
	public void doRealizarSorteoCampeonato(){
		
		boolean sorteoRealizado = false;
		
		try {
		
			//Asignar las observaciones del campeonato
			EstructuraCampeonatoMasculinoIndividualSegunda2021 ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda2021();
			crearCampeonatoMasculinoIndividual.updateRow(ecmi.getEstructuraObservacionesCampeonato1(), ecmi.getEstructuraObservacionesCampeonato1().getFila(), observacionesCampeonato1);
			crearCampeonatoMasculinoIndividual.updateRow(ecmi.getEstructuraObservacionesCampeonato2(), ecmi.getEstructuraObservacionesCampeonato2().getFila(), observacionesCampeonato2);
			crearCampeonatoMasculinoIndividual.updateRow(ecmi.getEstructuraObservacionesCampeonato3(), ecmi.getEstructuraObservacionesCampeonato3().getFila(), observacionesCampeonato3);
			crearCampeonatoMasculinoIndividual.updateRow(ecmi.getEstructuraObservacionesCampeonato4(), ecmi.getEstructuraObservacionesCampeonato4().getFila(), observacionesCampeonato4);
			crearCampeonatoMasculinoIndividual.updateRow(ecmi.getEstructuraObservacionesCampeonato5(), ecmi.getEstructuraObservacionesCampeonato5().getFila(), observacionesCampeonato5);
			crearCampeonatoMasculinoIndividual.updateRow(ecmi.getEstructuraObservacionesCampeonato6(), ecmi.getEstructuraObservacionesCampeonato6().getFila(), observacionesCampeonato6);
			crearCampeonatoMasculinoIndividual.updateRow(ecmi.getEstructuraObservacionesCampeonato7(), ecmi.getEstructuraObservacionesCampeonato7().getFila(), observacionesCampeonato7);
			crearCampeonatoMasculinoIndividual.updateRow(ecmi.getEstructuraBolerasOcupadasFI(), ecmi.getEstructuraBolerasOcupadasFI().getFila(), bolerasOcupadasFI);
			crearCampeonatoMasculinoIndividual.updateRow(ecmi.getEstructuraBolerasOcupadasOFCF(), ecmi.getEstructuraBolerasOcupadasOFCF().getFila(), bolerasOcupadasOFCF);
			
			
			
			//Inicializar variables de nuevo
			leerCampeonatoMasculinoIndividual = new LeerCampeonatoMasculinoIndividualSegunda2021();
			//Crear los mapas para el sorteo
			//FaseI
			LinkedHashMap<Integer, CalendarioFaseI2021> calendarioFaseIMap = new LinkedHashMap<>();
			LinkedHashMap<Integer, ClasificacionFaseI2021> clasificacionFaseIMap = new LinkedHashMap<>();
			//FaseII
			LinkedHashMap<Integer, CalendarioFaseII2021> calendarioFaseIIMap = new LinkedHashMap<>();
			LinkedHashMap<Integer, ClasificacionFaseII2021> clasificacionFaseIIMap = new LinkedHashMap<>();
			//FaseOF
			LinkedHashMap<Integer, CalendarioFaseOF2021> calendarioFaseOFMap = new LinkedHashMap<>();
			//FaseCF
			LinkedHashMap<Integer, CalendarioFaseCF2021> calendarioFaseCFMap = new LinkedHashMap<>();
			//FaseSF
			LinkedHashMap<Integer, CalendarioFaseSF2021> calendarioFaseSFMap = new LinkedHashMap<>();
			//FaseFC
			LinkedHashMap<Integer, CalendarioFaseFC2021> calendarioFaseFCMap = new LinkedHashMap<>();
			//FaseFF
			LinkedHashMap<Integer, CalendarioFaseFF2021> calendarioFaseFFMap = new LinkedHashMap<>();
			
			//1. Obtener las boleras disponibles, en principio solo las federadas.
			LinkedHashMap<Integer, String> bolerasGrupoAsignadasMap = new LinkedHashMap<>();
			LeerBoleras2021 leerBoleras = new LeerBoleras2021();
			List<Boleras2021> resultListBoleras = (List<Boleras2021>) leerBoleras.listResultFederadas();
			
			//Fase 1
			Integer numerofaseAnterior = 1;
			Integer numerofaseSiguiente = 1;
			
			//Bolera de la final
			Boleras2021 boleraFinal = null;
			for (Configuracion2021 configuracion : getResultListConfiguracion()) {
				if(configuracion.getNombre().equals("Bolera Final")){
					boleraFinal = leerBoleras.read(Integer.valueOf(configuracion.getValor()));
				}
			}
			
			//Bolera de la final
			LinkedHashMap<Integer, Boleras2021> bolerasOcupadasFIMap =  new LinkedHashMap<>();
			for (Configuracion2021 configuracion : getResultListConfiguracion()) {
				if(configuracion.getNombre().equals("Boleras Ocupadas FI")){
					String bolerasOcupadas = configuracion.getValor();
					String[] bolerasOcupadasList = bolerasOcupadas.split("-");
					for (String bolera : bolerasOcupadasList) {
						Integer idBolera = Integer.valueOf(bolera);
						if(idBolera != null && idBolera > 0){
							Boleras2021 boleraObject = leerBoleras.read(idBolera);
							if(boleraObject != null){
								bolerasOcupadasFIMap.put(idBolera, boleraObject);
							}
						}
					}
				}
			}
			
			//Bolera de la final
			LinkedHashMap<Integer, Boleras2021> bolerasOcupadasOFCFMap =  new LinkedHashMap<>();
			for (Configuracion2021 configuracion : getResultListConfiguracion()) {
				if(configuracion.getNombre().equals("Boleras Ocupadas OFCF")){
					String bolerasOcupadas = configuracion.getValor();
					String[] bolerasOcupadasList = bolerasOcupadas.split("-");
					for (String bolera : bolerasOcupadasList) {
						Integer idBolera = Integer.valueOf(bolera);
						if(idBolera != null && idBolera > 0){
							Boleras2021 boleraObject = leerBoleras.read(idBolera);
							if(boleraObject != null){
								bolerasOcupadasOFCFMap.put(idBolera, boleraObject);
							}
						}
					}
				}
			}
			
			boolean isFaseGruposClasifican3 = false;
			
			//2. Definir la faseI
			for(Fases2021 fase : getResultListFases()){
				if(fase.getActivo().equals(Activo2021.SI)){
					if(fase.getNumero().equals(FasesModelo2021.FASE_I)){
						if(numerofaseSiguiente.equals(fase.getNumero())){
							
							if(fase.getClasifican().equals(CLASIFICAN_3)){
								isFaseGruposClasifican3 = true;
							}
						
							//Objetos
							//Grupos Fase I
							LinkedHashMap<String, Grupos2021> gruposFaseIMap = new LinkedHashMap<>();
							//Jugadores Asignados
							LinkedHashMap<Integer, ParticipantesIndividual2021> jugadoresAsignados = new LinkedHashMap<>();
							
							//2.1. Obtener el numero de grupos de la fase
							Integer numeroGruposFase = fase.getNumeroEnfrentamientos();
							Integer numeroGruposFaseAsignados = 0;
							
							//2.2. Obtener el numero de participantes para saber el numero de jugadores por grupo y los asignados al grupo para
							Integer numeroParticipantes = getResultListParticipantes().size();
							Integer jugadoresPorGrupoAsignados = 0;
							
							//NUEVO
							//Equipos mismos o mas jugadores que grupos (idEquipo, numeroJugadores)
							LinkedHashMap<Integer, Integer> equipoMasJugadoresQueGrupos = new LinkedHashMap<>();
							for (ParticipantesIndividual2021 participante : getResultListParticipantes()) {
								if(equipoMasJugadoresQueGrupos.containsKey(participante.getJugador().getEquipoId())){
									Integer jugadoresEquipoValue = equipoMasJugadoresQueGrupos.get(participante.getJugador().getEquipoId());
									equipoMasJugadoresQueGrupos.put(participante.getJugador().getEquipoId(), jugadoresEquipoValue + 1 );
								}else{
									equipoMasJugadoresQueGrupos.put(participante.getJugador().getEquipoId(), 1);
								}
							}
							//Lista de equipos con mas jugadores que grupos
							LinkedHashMap<Integer, Integer> equipoMasJugadoresQueGruposLibre = (LinkedHashMap<Integer, Integer>) equipoMasJugadoresQueGrupos.clone();
							//Limpiar equipos que tengan menos jugadores que grupos
							Set<Integer> keySet = equipoMasJugadoresQueGrupos.keySet();
							for (Integer idEquipoKey : keySet) {
								Integer jugadoresEquipoValue = equipoMasJugadoresQueGrupos.get(idEquipoKey);
								if(jugadoresEquipoValue < numeroGruposFase){
									equipoMasJugadoresQueGruposLibre.remove(idEquipoKey);
								}
							}
							//F-NUEVO
							
							//2.3. Crear los grupos a participar
							Integer gruposCreados = 1;
							while(gruposCreados <= numeroGruposFase){
								Grupos2021 grupos = new Grupos2021();
								grupos.setId(GruposLetra2021.getGruposLetraFaseI(gruposCreados));
								grupos.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2021.SEGUNDA);
								grupos.setFecha(fase.getFecha());
								grupos.setHora(fase.getHora());
								//Asignar bolera aleatoriamente
								Boleras2021 boleraAsignar = null;
								boolean boleraAsignada = false;
								
								//Quito las boleras ocupadas
								List<Boleras2021> resultListBolerasLibres = new ArrayList<>();
								for (Boleras2021 boleras : resultListBoleras) {
									if(!bolerasOcupadasFIMap.containsKey(boleras.getId())){
										resultListBolerasLibres.add(boleras);
									}
								}
								
								
								while(!boleraAsignada){
									Random rand = new Random();
									boleraAsignar = resultListBolerasLibres.get(rand.nextInt(resultListBolerasLibres.size()));
									//if(!boleraAsignar.getId().equals(boleraFinal.getId())){
										if(!bolerasOcupadasFIMap.containsKey(boleraAsignar.getId())){
											if(!bolerasGrupoAsignadasMap.containsKey(boleraAsignar.getId())){
												boleraAsignada = true;
											}
										}
									//}
								}
								grupos.setBoleraId(boleraAsignar.getId());
								
								
								//Obtener el numero de jugadores del grupo
								BigDecimal jugadoresPendientesAsignar = new BigDecimal(numeroParticipantes - jugadoresPorGrupoAsignados);
								BigDecimal gruposPendientesAsignar = new BigDecimal(numeroGruposFase - numeroGruposFaseAsignados);
								BigDecimal jugadoresPorGrupoCalculo = jugadoresPendientesAsignar.divide(gruposPendientesAsignar, RoundingMode.UP);
								grupos.setJugadores(jugadoresPorGrupoCalculo.intValue());
								
								//Agregar los jugadores para cada grupo a sorteo
								//A cada grupo sortear los jugadores, se obtienen por equipos de mayor numero de jugadores a menor
								//Limitaciones: 1-  Se han de repartir los jugadores de equipo por grupo, 
								//					si hay mas jugadores del equipo por grupo se asigna a alguno de los grupos
								Integer jugadoresAsignadosGrupo = 0;
								
								//NUEVO
								//Lista de equipos con mas jugadores que grupos para cada grupo
								LinkedHashMap<Integer, Integer> equipoMasJugadoresQueGruposLibreGrupo = (LinkedHashMap<Integer, Integer>) equipoMasJugadoresQueGruposLibre.clone();
								//F-NUEVO
								
								Integer reintentosCancelarSorteo = 0;
								
								Integer reintentosEquipoRepetido = 0;
								while(jugadoresAsignadosGrupo < jugadoresPorGrupoCalculo.intValue()){
									
									
									Random rand = new Random();
									
									
									//NUEVO
									ParticipantesIndividual2021 jugadorSorteo = null;
									
									//Primero buscar jugadores pertenecientes a los equipos que tienen los mismos o mas jugadores que grupos
									Set<Integer> keySetequipoMasJugadoresQueGruposLibreGrupo = equipoMasJugadoresQueGruposLibreGrupo.keySet();
									if(keySetequipoMasJugadoresQueGruposLibreGrupo.size() > 0){
										List<ParticipantesIndividual2021> list = getResultListParticipantesByEquipos(keySetequipoMasJugadoresQueGruposLibreGrupo);
										boolean jugadorCorrecto = false;
										while(!jugadorCorrecto){

											reintentosCancelarSorteo++;
											
											if(reintentosCancelarSorteo > 250000){
												throw new Exception();
											}
											
											jugadorSorteo = list.get(rand.nextInt(list.size()));
											if(!jugadoresAsignados.containsKey(jugadorSorteo.getIdJugador())){
												equipoMasJugadoresQueGruposLibreGrupo.remove(jugadorSorteo.getJugador().getEquipoId());
												jugadorCorrecto = true;
											}
										}
									}else{
										boolean jugadorCorrecto = false;
										while(!jugadorCorrecto){

											reintentosCancelarSorteo++;
											
											if(reintentosCancelarSorteo > 250000){
												throw new Exception();
											}
											
											jugadorSorteo = getResultListParticipantes().get(rand.nextInt(getResultListParticipantes().size()));
											if(!jugadoresAsignados.containsKey(jugadorSorteo.getIdJugador())){
												if(equipoMasJugadoresQueGruposLibreGrupo.size() > 0){
													equipoMasJugadoresQueGruposLibreGrupo.remove(jugadorSorteo.getJugador().getEquipoId());
												}
												jugadorCorrecto = true;
											}
										}
										
									}
									
									//F-NUEVO
									
									if(!grupos.getJugadoresGrupoList().containsKey(jugadorSorteo.getIdJugador())){
										if(!jugadoresAsignados.containsKey(jugadorSorteo.getIdJugador())){
											if(!grupos.isExisteJugadorMismoEquipo(jugadorSorteo.getJugador().getEquipoId(), equipoMasJugadoresQueGrupos, numeroGruposFase)){
												jugadorSorteo.setNumeroJugadorGrupo(jugadoresAsignadosGrupo + 1);
												grupos.getJugadoresGrupoList().put(jugadorSorteo.getIdJugador(), jugadorSorteo);
												jugadoresAsignadosGrupo++;
												jugadoresAsignados.put(jugadorSorteo.getIdJugador(), jugadorSorteo);
											}else{
												if(reintentosEquipoRepetido >50){
													jugadorSorteo.setNumeroJugadorGrupo(jugadoresAsignadosGrupo + 1);
													grupos.getJugadoresGrupoList().put(jugadorSorteo.getIdJugador(), jugadorSorteo);
													jugadoresAsignadosGrupo++;
													reintentosEquipoRepetido = 0;
													jugadoresAsignados.put(jugadorSorteo.getIdJugador(), jugadorSorteo);
												}else{
													reintentosEquipoRepetido++;
												}
											}
										}
									}
								}
								
								//Agregar a los mapas
								gruposFaseIMap.put(grupos.getId(), grupos);
								bolerasGrupoAsignadasMap.put(boleraAsignar.getId(), grupos.getId());
								
								//Actualizar variables
								gruposCreados++;
								numeroGruposFaseAsignados++;
								jugadoresPorGrupoAsignados += jugadoresPorGrupoCalculo.intValue();
								
								
							}
							
							//A�adir los datos a el calendario y clasificacion de la fase I
							List<CalendarioFaseI2021> calendarioFaseIList = new ArrayList<CalendarioFaseI2021>();
							List<ClasificacionFaseI2021> clasificacionFaseIList = new ArrayList<ClasificacionFaseI2021>();
							//Se recorren los grupos obtenidos
							//Se a�aden a la clasificacion
							for (Entry<String, Grupos2021> entry : gruposFaseIMap.entrySet()) {
							    Object value = entry.getValue();
							    Grupos2021 grupo = (Grupos2021) value;
							    for (Entry<Integer, ParticipantesIndividual2021> entryPi : grupo.getJugadoresGrupoList().entrySet()) {
							    	Object valuePi = entryPi.getValue();
							    	ParticipantesIndividual2021 participantesIndividual = (ParticipantesIndividual2021) valuePi;
							    	
							    	//Agrego a clasificacion el jugador para el grupo
							    	ClasificacionFaseI2021 cf1 = new ClasificacionFaseI2021();
							    	cf1.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2021.SEGUNDA);
							    	cf1.setGrupo(grupo.getId());
							    	cf1.setId(participantesIndividual.getNumeroJugadorGrupo());
							    	cf1.setJugadorId(participantesIndividual.getIdJugador());
							    	cf1.setPj(0);
							    	cf1.setPg(0);
							    	cf1.setPe(0);
							    	cf1.setPp(0);
							    	cf1.setPf(0);
							    	cf1.setPc(0);
							    	cf1.setPt(0);
							    	clasificacionFaseIList.add(cf1);
							    }
							}
							//Se a�aden al calendario
							for (Entry<String, Grupos2021> entry : gruposFaseIMap.entrySet()) {
							    Object value = entry.getValue();
							    Grupos2021 grupo = (Grupos2021) value;
							    //Recorro los enfrentamientos para el modelo del grupo
							    if(grupo.getJugadores().equals(8)){
							    	calendarioFaseIList = prepareCalendarioFaseIModeloGrupos8(calendarioFaseIList, grupo);
							    }else if(grupo.getJugadores().equals(7)){
							    	calendarioFaseIList = prepareCalendarioFaseIModeloGrupos7(calendarioFaseIList, grupo);
							    }else if(grupo.getJugadores().equals(6)){
							    	calendarioFaseIList = prepareCalendarioFaseIModeloGrupos6(calendarioFaseIList, grupo);
							    }else if(grupo.getJugadores().equals(5)){
							    	calendarioFaseIList = prepareCalendarioFaseIModeloGrupos5(calendarioFaseIList, grupo);
							    }else if(grupo.getJugadores().equals(4)){
							    	calendarioFaseIList = prepareCalendarioFaseIModeloGrupos4(calendarioFaseIList, grupo);
							    }else if(grupo.getJugadores().equals(3)){
							    	calendarioFaseIList = prepareCalendarioFaseIModeloGrupos3(calendarioFaseIList, grupo);
							    }
							    
							}
							
							//Insertar clasificacion y calendario fase I
							//Clasificacion
							for (ClasificacionFaseI2021 clasificacionFaseI : clasificacionFaseIList) {
								crearCampeonatoMasculinoIndividual.insertarRowClasificacionFaseI(clasificacionFaseI.getInsertRow());
							}
							//Calendario
							for (CalendarioFaseI2021 calendarioFaseI : calendarioFaseIList) {
								crearCampeonatoMasculinoIndividual.insertarRowCalendarioFaseI(calendarioFaseI.getInsertRow());
							}
							
							numerofaseSiguiente = fase.getFaseSiguiente();
						}
						
					}
				}
			}
			
			
			//3. Definir la faseII
			for(Fases2021 fase : getResultListFases()){
				if(fase.getActivo().equals(Activo2021.SI)){
					if(fase.getNumero().equals(FasesModelo2021.FASE_II)){
						if(numerofaseSiguiente.equals(fase.getNumero())){
							
						}
					}
				}
			}
			
			//4. Definir la faseOF
			
			Boleras2021 boleraFaseAnterior1 = null;
			Boleras2021 boleraFaseAnterior2 = null;
			
			for(Fases2021 fase : getResultListFases()){
				if(fase.getActivo().equals(Activo2021.SI)){
					if(fase.getNumero().equals(FasesModelo2021.FASE_OF)){
						if(numerofaseSiguiente.equals(fase.getNumero())){
							//Recuperar la fase anterior, para averiguar si era grupos escoger el modelo
							Fases2021 faseAnterior = searchFase(numerofaseAnterior);
							//Asignar boleras aleatoriamente
							Boleras2021 boleraAsignar1 = null;
							Boleras2021 boleraAsignar2 = null;
							boolean boleraAsignada1 = false;
							boolean boleraAsignada2 = false;
							LinkedHashMap<Integer, String> bolerasGrupoAsignadasCFMap = new LinkedHashMap<>();
							while(!boleraAsignada1){
								Random rand = new Random();
								boleraAsignar1 = resultListBoleras.get(rand.nextInt(resultListBoleras.size()));
								if(!boleraAsignar1.getId().equals(boleraFinal.getId())){
									if(!bolerasOcupadasOFCFMap.containsKey(boleraAsignar1.getId())){
										if(!bolerasGrupoAsignadasCFMap.containsKey(boleraAsignar1.getId())){
											boleraAsignada1 = true;
											boleraFaseAnterior1 = boleraAsignar1;
										}
									}
								}
							}
							while(!boleraAsignada2){
								Random rand = new Random();
								boleraAsignar2 = resultListBoleras.get(rand.nextInt(resultListBoleras.size()));
								if(!boleraAsignar2.getId().equals(boleraFinal.getId())){
									if(!bolerasOcupadasOFCFMap.containsKey(boleraAsignar2.getId())){
										if(!bolerasGrupoAsignadasCFMap.containsKey(boleraAsignar2.getId())){
											boleraAsignada2 = true;
											boleraFaseAnterior2 = boleraAsignar2;
										}
									}
								}
							}
							//Si en la fase previa eran 4 grupos
							if(faseAnterior.getTipoEnfrentamiento().equals(TipoEnfrentamiento2021.GRUPOS)){
								if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra2021.FASE_I_II_G_4)){
									if(isFaseGruposClasifican3){
										List<CalendarioFaseOF2021> calendarioFaseOFList = new ArrayList<CalendarioFaseOF2021>();
										calendarioFaseOFList = prepareCalendarioFaseOFModeloGrupos4Clasifican3(calendarioFaseOFList, faseAnterior, boleraAsignar1, boleraAsignar2);
										//Calendario
										for (CalendarioFaseOF2021 calendarioFaseOF : calendarioFaseOFList) {
											crearCampeonatoMasculinoIndividual.insertarRowCalendarioFaseOF(calendarioFaseOF.getInsertRow());
										}
										numerofaseSiguiente = fase.getFaseSiguiente();
										numerofaseAnterior = fase.getNumero();
									}else{
										List<CalendarioFaseOF2021> calendarioFaseOFList = new ArrayList<CalendarioFaseOF2021>();
										calendarioFaseOFList = prepareCalendarioFaseOFModeloGrupos4(calendarioFaseOFList, faseAnterior, boleraAsignar1, boleraAsignar2);
										//Calendario
										for (CalendarioFaseOF2021 calendarioFaseOF : calendarioFaseOFList) {
											crearCampeonatoMasculinoIndividual.insertarRowCalendarioFaseOF(calendarioFaseOF.getInsertRow());
										}
										numerofaseSiguiente = fase.getFaseSiguiente();
										numerofaseAnterior = fase.getNumero();
									}
								}
							}
						}
					}
				}
			}
			
			//5. Definir la faseCF
			for(Fases2021 fase : getResultListFases()){
				if(fase.getActivo().equals(Activo2021.SI)){
					if(fase.getNumero().equals(FasesModelo2021.FASE_CF)){
						if(numerofaseSiguiente.equals(fase.getNumero())){
							//Recuperar la fase anterior, para averiguar si era grupos escoger el modelo
							Fases2021 faseAnterior = searchFase(numerofaseAnterior);
							//Si en la fase previa eran 4 grupos
							if(faseAnterior.getTipoEnfrentamiento().equals(TipoEnfrentamiento2021.GRUPOS)){
								if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra2021.FASE_I_II_G_4)){
									
									//Asignar boleras aleatoriamente
									Boleras2021 boleraAsignar1 = null;
									Boleras2021 boleraAsignar2 = null;
									boolean boleraAsignada1 = false;
									boolean boleraAsignada2 = false;
									LinkedHashMap<Integer, String> bolerasGrupoAsignadasCFMap = new LinkedHashMap<>();
									while(!boleraAsignada1){
										Random rand = new Random();
										boleraAsignar1 = resultListBoleras.get(rand.nextInt(resultListBoleras.size()));
										if(!boleraAsignar1.getId().equals(boleraFinal.getId())){
											if(!bolerasOcupadasOFCFMap.containsKey(boleraAsignar1.getId())){
												if(!bolerasGrupoAsignadasCFMap.containsKey(boleraAsignar1.getId())){
													boleraAsignada1 = true;
												}
											}
										}
									}
									while(!boleraAsignada2){
										Random rand = new Random();
										boleraAsignar2 = resultListBoleras.get(rand.nextInt(resultListBoleras.size()));
										if(!boleraAsignar2.getId().equals(boleraFinal.getId())){
											if(!bolerasOcupadasOFCFMap.containsKey(boleraAsignar2.getId())){
												if(!bolerasGrupoAsignadasCFMap.containsKey(boleraAsignar2.getId())){
													boleraAsignada2 = true;
												}
											}
										}
									}
									
									List<CalendarioFaseCF2021> calendarioFaseCFList = new ArrayList<CalendarioFaseCF2021>();
									calendarioFaseCFList = prepareCalendarioFaseCFModeloGrupos4(calendarioFaseCFList, faseAnterior, boleraAsignar1, boleraAsignar2);
									//Calendario
									for (CalendarioFaseCF2021 calendarioFaseCF : calendarioFaseCFList) {
										crearCampeonatoMasculinoIndividual.insertarRowCalendarioFaseCF(calendarioFaseCF.getInsertRow());
									}
									numerofaseSiguiente = fase.getFaseSiguiente();
									numerofaseAnterior = fase.getNumero();
								}else if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra2021.FASE_I_II_G_2)){
									
									//Asignar boleras aleatoriamente
									Boleras2021 boleraAsignar1 = null;
									Boleras2021 boleraAsignar2 = null;
									boolean boleraAsignada1 = false;
									boolean boleraAsignada2 = false;
									LinkedHashMap<Integer, String> bolerasGrupoAsignadasCFMap = new LinkedHashMap<>();
									while(!boleraAsignada1){
										Random rand = new Random();
										boleraAsignar1 = resultListBoleras.get(rand.nextInt(resultListBoleras.size()));
										if(!boleraAsignar1.getId().equals(boleraFinal.getId())){
											if(!bolerasOcupadasOFCFMap.containsKey(boleraAsignar1.getId())){
												if(!bolerasGrupoAsignadasCFMap.containsKey(boleraAsignar1.getId())){
													boleraAsignada1 = true;
												}
											}
										}
									}
									while(!boleraAsignada2){
										Random rand = new Random();
										boleraAsignar2 = resultListBoleras.get(rand.nextInt(resultListBoleras.size()));
										if(!boleraAsignar2.getId().equals(boleraFinal.getId())){
											if(!bolerasOcupadasOFCFMap.containsKey(boleraAsignar2.getId())){
												if(!bolerasGrupoAsignadasCFMap.containsKey(boleraAsignar2.getId())){
													boleraAsignada2 = true;
												}
											}
										}
									}
									
									List<CalendarioFaseCF2021> calendarioFaseCFList = new ArrayList<CalendarioFaseCF2021>();
									calendarioFaseCFList = prepareCalendarioFaseCFModeloGrupos2(calendarioFaseCFList, faseAnterior, boleraAsignar1, boleraAsignar2);
									//Calendario
									for (CalendarioFaseCF2021 calendarioFaseCF : calendarioFaseCFList) {
										crearCampeonatoMasculinoIndividual.insertarRowCalendarioFaseCF(calendarioFaseCF.getInsertRow());
									}
									numerofaseSiguiente = fase.getFaseSiguiente();
									numerofaseAnterior = fase.getNumero();
								}
							}
							//Si en la fase previa eran Octavos de final
							else if(fase.getTipoEnfrentamiento().equals(TipoEnfrentamiento2021.DIRECTO)){
								if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra2021.FASE_OF)){
									
									
									if(isFaseGruposClasifican3){
										
										List<CalendarioFaseCF2021> calendarioFaseCFList = new ArrayList<CalendarioFaseCF2021>();
										calendarioFaseCFList = prepareCalendarioFaseCFModeloOFClasifican3(calendarioFaseCFList, faseAnterior, boleraFaseAnterior1, boleraFaseAnterior2);
										//Calendario
										for (CalendarioFaseCF2021 calendarioFaseCF : calendarioFaseCFList) {
											crearCampeonatoMasculinoIndividual.insertarRowCalendarioFaseCF(calendarioFaseCF.getInsertRow());
										}
										numerofaseSiguiente = fase.getFaseSiguiente();
										numerofaseAnterior = fase.getNumero();
										
									}else{
									
										List<CalendarioFaseCF2021> calendarioFaseCFList = new ArrayList<CalendarioFaseCF2021>();
										calendarioFaseCFList = prepareCalendarioFaseCFModeloOF(calendarioFaseCFList, faseAnterior, boleraFaseAnterior1, boleraFaseAnterior2);
										//Calendario
										for (CalendarioFaseCF2021 calendarioFaseCF : calendarioFaseCFList) {
											crearCampeonatoMasculinoIndividual.insertarRowCalendarioFaseCF(calendarioFaseCF.getInsertRow());
										}
										numerofaseSiguiente = fase.getFaseSiguiente();
										numerofaseAnterior = fase.getNumero();
										
									}
								}
							}
						}
					}
				}
			}
			//6. Definir la faseSF
			
			for(Fases2021 fase : getResultListFases()){
				if(fase.getActivo().equals(Activo2021.SI)){
					if(fase.getNumero().equals(FasesModelo2021.FASE_SF)){
						if(numerofaseSiguiente.equals(fase.getNumero())){
							//Recuperar la fase anterior, para averiguar si era grupos escoger el modelo
							Fases2021 faseAnterior = searchFase(numerofaseAnterior);
							//Si en la fase previa eran Cuartos de final
							if(fase.getTipoEnfrentamiento().equals(TipoEnfrentamiento2021.DIRECTO)){
								if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra2021.FASE_CF)){
									List<CalendarioFaseSF2021> calendarioFaseSFList = new ArrayList<CalendarioFaseSF2021>();
									calendarioFaseSFList = prepareCalendarioFaseSFModeloCF(calendarioFaseSFList, faseAnterior, boleraFinal);
									//Calendario
									for (CalendarioFaseSF2021 calendarioFaseSF : calendarioFaseSFList) {
										crearCampeonatoMasculinoIndividual.insertarRowCalendarioFaseSF(calendarioFaseSF.getInsertRow());
									}
									numerofaseSiguiente = fase.getFaseSiguiente();
									numerofaseAnterior = fase.getNumero();
								}
							}
						}
					}
				}
			}
			
			//7. Definir la faseFC
			
			for(Fases2021 fase : getResultListFases()){
				if(fase.getActivo().equals(Activo2021.SI)){
					if(fase.getNumero().equals(FasesModelo2021.FASE_FC)){
						//if(numerofaseSiguiente.equals(fase.getNumero())){
							//Recuperar la fase anterior, para averiguar si era grupos escoger el modelo
							Fases2021 faseAnterior = searchFase(numerofaseAnterior);
							//Si en la fase previa eran Semifinal
							if(fase.getTipoEnfrentamiento().equals(TipoEnfrentamiento2021.DIRECTO)){
								if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra2021.FASE_SF)){
									List<CalendarioFaseFC2021> calendarioFaseFCList = new ArrayList<CalendarioFaseFC2021>();
									calendarioFaseFCList = prepareCalendarioFaseFCModeloSF(calendarioFaseFCList, faseAnterior, boleraFinal);
									//Calendario
									for (CalendarioFaseFC2021 calendarioFaseFC : calendarioFaseFCList) {
										crearCampeonatoMasculinoIndividual.insertarRowCalendarioFaseFC(calendarioFaseFC.getInsertRow());
									}
									//numerofaseSiguiente = fase.getFaseSiguiente();
								}
							}
						//}
					}
				}
			}
			
			//8. Definir la faseFF
			
			for(Fases2021 fase : getResultListFases()){
				if(fase.getActivo().equals(Activo2021.SI)){
					if(fase.getNumero().equals(FasesModelo2021.FASE_FF)){
						if(numerofaseSiguiente.equals(fase.getNumero())){
							//Recuperar la fase anterior, para averiguar si era grupos escoger el modelo
							Fases2021 faseAnterior = searchFase(numerofaseAnterior);
							//Si en la fase previa eran Semifinal
							if(fase.getTipoEnfrentamiento().equals(TipoEnfrentamiento2021.DIRECTO)){
								if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra2021.FASE_SF)){
									List<CalendarioFaseFF2021> calendarioFaseFFList = new ArrayList<CalendarioFaseFF2021>();
									calendarioFaseFFList = prepareCalendarioFaseFFModeloSF(calendarioFaseFFList, faseAnterior, boleraFinal);
									//Calendario
									for (CalendarioFaseFF2021 calendarioFaseFF : calendarioFaseFFList) {
										crearCampeonatoMasculinoIndividual.insertarRowCalendarioFaseFF(calendarioFaseFF.getInsertRow());
										sorteoRealizado = true;
									}
									numerofaseSiguiente = fase.getFaseSiguiente();
								}
							}
						}
					}
				}
			}
		
			if(!sorteoRealizado){
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El Sorteo no se ha realizado.", null));
			}else{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "El Sorteo se ha realizado correctamente.", null));
				crearCampeonatoMasculinoIndividual.actualizarEstadoConfiguracion(EstadosIndividualParejas2021.CONFIG_ESTADO_SORTEADO_CAMPEONATO);
			}
			
			
		}catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El Sorteo no se ha realizado.", null));
			doBorrarSorteo();
		}
		
	}

	private List<CalendarioFaseI2021> prepareCalendarioFaseIModeloGrupos8(List<CalendarioFaseI2021> calendarioFaseIList, Grupos2021 grupo) {
		ModeloIndividualGrupos82021 modeloGrupos8 = new ModeloIndividualGrupos82021();
		Integer id = 1;
		for (Entry<Integer, ModeloIndividualEnfrentamientoGruposFaseI2021> entryPi : modeloGrupos8.getEnfrentamientosGrupo().entrySet()) {
			Integer key = entryPi.getKey();
			Object valuePi = entryPi.getValue();
			ModeloIndividualEnfrentamientoGruposFaseI2021 enfrentamiento = (ModeloIndividualEnfrentamientoGruposFaseI2021) valuePi;
			//Agregar calendario
			CalendarioFaseI2021 calendarioFaseI = new CalendarioFaseI2021();
			calendarioFaseI.setActivo(Activo2021.SI);
			calendarioFaseI.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2021.SEGUNDA);
			calendarioFaseI.setGrupo(grupo.getId());
			calendarioFaseI.setId(id);
			calendarioFaseI.setJuegosJugador1(0);
			ParticipantesIndividual2021 pi1 = grupo.searchJugadorGrupo(enfrentamiento.getJugador1(), grupo.getId());
			calendarioFaseI.setJugador1Id(pi1.getIdJugador());
			calendarioFaseI.setJuegosJugador2(0);
			ParticipantesIndividual2021 pi2 = grupo.searchJugadorGrupo(enfrentamiento.getJugador2(), grupo.getId());
			calendarioFaseI.setJugador2Id(pi2.getIdJugador());
			calendarioFaseI.setOrden(key);
			calendarioFaseI.setInicia(enfrentamiento.getSorteo());
			calendarioFaseI.setBoleraId(grupo.getBoleraId());
			id++;
			calendarioFaseIList.add(calendarioFaseI);
		}
		return calendarioFaseIList;
	}
	
	private List<CalendarioFaseI2021> prepareCalendarioFaseIModeloGrupos7(List<CalendarioFaseI2021> calendarioFaseIList, Grupos2021 grupo) {
		ModeloIndividualGrupos72021 modeloGrupos = new ModeloIndividualGrupos72021();
		Integer id = 1;
		for (Entry<Integer, ModeloIndividualEnfrentamientoGruposFaseI2021> entryPi : modeloGrupos.getEnfrentamientosGrupo().entrySet()) {
			Integer key = entryPi.getKey();
			Object valuePi = entryPi.getValue();
			ModeloIndividualEnfrentamientoGruposFaseI2021 enfrentamiento = (ModeloIndividualEnfrentamientoGruposFaseI2021) valuePi;
			//Agregar calendario
			CalendarioFaseI2021 calendarioFaseI = new CalendarioFaseI2021();
			calendarioFaseI.setActivo(Activo2021.SI);
			calendarioFaseI.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2021.SEGUNDA);
			calendarioFaseI.setGrupo(grupo.getId());
			calendarioFaseI.setId(id);
			calendarioFaseI.setJuegosJugador1(0);
			ParticipantesIndividual2021 pi1 = grupo.searchJugadorGrupo(enfrentamiento.getJugador1(), grupo.getId());
			calendarioFaseI.setJugador1Id(pi1.getIdJugador());
			calendarioFaseI.setJuegosJugador2(0);
			ParticipantesIndividual2021 pi2 = grupo.searchJugadorGrupo(enfrentamiento.getJugador2(), grupo.getId());
			calendarioFaseI.setJugador2Id(pi2.getIdJugador());
			calendarioFaseI.setOrden(key);
			calendarioFaseI.setInicia(enfrentamiento.getSorteo());
			calendarioFaseI.setBoleraId(grupo.getBoleraId());
			id++;
			calendarioFaseIList.add(calendarioFaseI);
		}
		return calendarioFaseIList;
	}
	
	private List<CalendarioFaseI2021> prepareCalendarioFaseIModeloGrupos6(List<CalendarioFaseI2021> calendarioFaseIList, Grupos2021 grupo) {
		ModeloIndividualGrupos62021 modeloGrupos = new ModeloIndividualGrupos62021();
		Integer id = 1;
		for (Entry<Integer, ModeloIndividualEnfrentamientoGruposFaseI2021> entryPi : modeloGrupos.getEnfrentamientosGrupo().entrySet()) {
			Integer key = entryPi.getKey();
			Object valuePi = entryPi.getValue();
			ModeloIndividualEnfrentamientoGruposFaseI2021 enfrentamiento = (ModeloIndividualEnfrentamientoGruposFaseI2021) valuePi;
			//Agregar calendario
			CalendarioFaseI2021 calendarioFaseI = new CalendarioFaseI2021();
			calendarioFaseI.setActivo(Activo2021.SI);
			calendarioFaseI.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2021.SEGUNDA);
			calendarioFaseI.setGrupo(grupo.getId());
			calendarioFaseI.setId(id);
			calendarioFaseI.setJuegosJugador1(0);
			ParticipantesIndividual2021 pi1 = grupo.searchJugadorGrupo(enfrentamiento.getJugador1(), grupo.getId());
			calendarioFaseI.setJugador1Id(pi1.getIdJugador());
			calendarioFaseI.setJuegosJugador2(0);
			ParticipantesIndividual2021 pi2 = grupo.searchJugadorGrupo(enfrentamiento.getJugador2(), grupo.getId());
			calendarioFaseI.setJugador2Id(pi2.getIdJugador());
			calendarioFaseI.setOrden(key);
			calendarioFaseI.setInicia(enfrentamiento.getSorteo());
			calendarioFaseI.setBoleraId(grupo.getBoleraId());
			id++;
			calendarioFaseIList.add(calendarioFaseI);
		}
		return calendarioFaseIList;
	}
	
	private List<CalendarioFaseI2021> prepareCalendarioFaseIModeloGrupos5(List<CalendarioFaseI2021> calendarioFaseIList, Grupos2021 grupo) {
		ModeloIndividualGrupos52021 modeloGrupos = new ModeloIndividualGrupos52021();
		Integer id = 1;
		for (Entry<Integer, ModeloIndividualEnfrentamientoGruposFaseI2021> entryPi : modeloGrupos.getEnfrentamientosGrupo().entrySet()) {
			Integer key = entryPi.getKey();
			Object valuePi = entryPi.getValue();
			ModeloIndividualEnfrentamientoGruposFaseI2021 enfrentamiento = (ModeloIndividualEnfrentamientoGruposFaseI2021) valuePi;
			//Agregar calendario
			CalendarioFaseI2021 calendarioFaseI = new CalendarioFaseI2021();
			calendarioFaseI.setActivo(Activo2021.SI);
			calendarioFaseI.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2021.SEGUNDA);
			calendarioFaseI.setGrupo(grupo.getId());
			calendarioFaseI.setId(id);
			calendarioFaseI.setJuegosJugador1(0);
			ParticipantesIndividual2021 pi1 = grupo.searchJugadorGrupo(enfrentamiento.getJugador1(), grupo.getId());
			calendarioFaseI.setJugador1Id(pi1.getIdJugador());
			calendarioFaseI.setJuegosJugador2(0);
			ParticipantesIndividual2021 pi2 = grupo.searchJugadorGrupo(enfrentamiento.getJugador2(), grupo.getId());
			calendarioFaseI.setJugador2Id(pi2.getIdJugador());
			calendarioFaseI.setOrden(key);
			calendarioFaseI.setInicia(enfrentamiento.getSorteo());
			calendarioFaseI.setBoleraId(grupo.getBoleraId());
			id++;
			calendarioFaseIList.add(calendarioFaseI);
		}
		return calendarioFaseIList;
	}
	
	private List<CalendarioFaseI2021> prepareCalendarioFaseIModeloGrupos4(List<CalendarioFaseI2021> calendarioFaseIList, Grupos2021 grupo) {
		ModeloIndividualGrupos42021 modeloGrupos = new ModeloIndividualGrupos42021();
		Integer id = 1;
		for (Entry<Integer, ModeloIndividualEnfrentamientoGruposFaseI2021> entryPi : modeloGrupos.getEnfrentamientosGrupo().entrySet()) {
			Integer key = entryPi.getKey();
			Object valuePi = entryPi.getValue();
			ModeloIndividualEnfrentamientoGruposFaseI2021 enfrentamiento = (ModeloIndividualEnfrentamientoGruposFaseI2021) valuePi;
			//Agregar calendario
			CalendarioFaseI2021 calendarioFaseI = new CalendarioFaseI2021();
			calendarioFaseI.setActivo(Activo2021.SI);
			calendarioFaseI.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2021.SEGUNDA);
			calendarioFaseI.setGrupo(grupo.getId());
			calendarioFaseI.setId(id);
			calendarioFaseI.setJuegosJugador1(0);
			ParticipantesIndividual2021 pi1 = grupo.searchJugadorGrupo(enfrentamiento.getJugador1(), grupo.getId());
			calendarioFaseI.setJugador1Id(pi1.getIdJugador());
			calendarioFaseI.setJuegosJugador2(0);
			ParticipantesIndividual2021 pi2 = grupo.searchJugadorGrupo(enfrentamiento.getJugador2(), grupo.getId());
			calendarioFaseI.setJugador2Id(pi2.getIdJugador());
			calendarioFaseI.setOrden(key);
			calendarioFaseI.setInicia(enfrentamiento.getSorteo());
			calendarioFaseI.setBoleraId(grupo.getBoleraId());
			id++;
			calendarioFaseIList.add(calendarioFaseI);
		}
		return calendarioFaseIList;
	}
	
	private List<CalendarioFaseI2021> prepareCalendarioFaseIModeloGrupos3(List<CalendarioFaseI2021> calendarioFaseIList, Grupos2021 grupo) {
		ModeloIndividualGrupos32021 modeloGrupos = new ModeloIndividualGrupos32021();
		Integer id = 1;
		for (Entry<Integer, ModeloIndividualEnfrentamientoGruposFaseI2021> entryPi : modeloGrupos.getEnfrentamientosGrupo().entrySet()) {
			Integer key = entryPi.getKey();
			Object valuePi = entryPi.getValue();
			ModeloIndividualEnfrentamientoGruposFaseI2021 enfrentamiento = (ModeloIndividualEnfrentamientoGruposFaseI2021) valuePi;
			//Agregar calendario
			CalendarioFaseI2021 calendarioFaseI = new CalendarioFaseI2021();
			calendarioFaseI.setActivo(Activo2021.SI);
			calendarioFaseI.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2021.TERCERA);
			calendarioFaseI.setGrupo(grupo.getId());
			calendarioFaseI.setId(id);
			calendarioFaseI.setJuegosJugador1(0);
			ParticipantesIndividual2021 pi1 = grupo.searchJugadorGrupo(enfrentamiento.getJugador1(), grupo.getId());
			calendarioFaseI.setJugador1Id(pi1.getIdJugador());
			calendarioFaseI.setJuegosJugador2(0);
			ParticipantesIndividual2021 pi2 = grupo.searchJugadorGrupo(enfrentamiento.getJugador2(), grupo.getId());
			calendarioFaseI.setJugador2Id(pi2.getIdJugador());
			calendarioFaseI.setOrden(key);
			calendarioFaseI.setInicia(enfrentamiento.getSorteo());
			calendarioFaseI.setBoleraId(grupo.getBoleraId());
			id++;
			calendarioFaseIList.add(calendarioFaseI);
		}
		return calendarioFaseIList;
	}

	public Fases2021 searchFase(Integer numeroFase){
		for(Fases2021 fase : getResultListFases()){
			if(fase.getNumero().equals(numeroFase)){
				return fase;
			}
		}
		return null;
	}
	
	private List<CalendarioFaseOF2021> prepareCalendarioFaseOFModeloGrupos4(List<CalendarioFaseOF2021> calendarioFaseOFList, Fases2021 faseAnterior, Boleras2021 boleraAsignar1, Boleras2021 boleraAsignar2) {
		ModeloIndividualOctavosFinal4Grupos2021 modeloOctavosFinal4Grupos = new ModeloIndividualOctavosFinal4Grupos2021();
		Integer id = 1;

		for (Entry<Integer, ModeloIndividualEnfrentamientoOctavosFinal2021> entry : modeloOctavosFinal4Grupos.getEnfrentamientosOctavosFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloIndividualEnfrentamientoOctavosFinal2021 enfrentamiento = (ModeloIndividualEnfrentamientoOctavosFinal2021) value;
			//Agregar calendario
			CalendarioFaseOF2021 calendarioFaseOF = new CalendarioFaseOF2021();
			calendarioFaseOF.setId(id);
			calendarioFaseOF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2021.SEGUNDA);
			calendarioFaseOF.setNumeroEnfrentamiento(id);
			calendarioFaseOF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseOF.setJugador1Id(0);
			calendarioFaseOF.setJuegosJugador1P1(0);
			calendarioFaseOF.setJuegosJugador1P2(0);
			calendarioFaseOF.setJuegosJugador1P3(0);
			calendarioFaseOF.setJuegosJugador2P1(0);
			calendarioFaseOF.setJuegosJugador2P2(0);
			calendarioFaseOF.setJuegosJugador2P3(0);
			calendarioFaseOF.setJugador2Id(0);
			calendarioFaseOF.setOrden(key);
			calendarioFaseOF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseOF.setActivo(Activo2021.SI);
			calendarioFaseOF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseOF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseOF.setGrupoProcedenciaJugador1(enfrentamiento.getGrupoProcedenciaJugador1());
			calendarioFaseOF.setPosicionProcedenciaJugador1(enfrentamiento.getPosicionProcedenciaJugador1());
			calendarioFaseOF.setGrupoProcedenciaJugador2(enfrentamiento.getGrupoProcedenciaJugador2());
			calendarioFaseOF.setPosicionProcedenciaJugador2(enfrentamiento.getPosicionProcedenciaJugador2());
			
			//Asignar la bolera por parte del cuadro
			if(id > 0 && id <5 ){
				calendarioFaseOF.setBoleraId(boleraAsignar1.getId());
			}else if(id > 4 && id <9 ){
				calendarioFaseOF.setBoleraId(boleraAsignar2.getId());
			}
			id++;
			calendarioFaseOFList.add(calendarioFaseOF);
		}
		return calendarioFaseOFList;
	}
	
	private List<CalendarioFaseOF2021> prepareCalendarioFaseOFModeloGrupos4Clasifican3(List<CalendarioFaseOF2021> calendarioFaseOFList, Fases2021 faseAnterior, Boleras2021 boleraAsignar1, Boleras2021 boleraAsignar2) {
		ModeloIndividualOctavosFinal4GruposClasifican32021 modeloOctavosFinal4Grupos = new ModeloIndividualOctavosFinal4GruposClasifican32021();
		Integer id = 1;

		for (Entry<Integer, ModeloIndividualEnfrentamientoOctavosFinal2021> entry : modeloOctavosFinal4Grupos.getEnfrentamientosOctavosFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloIndividualEnfrentamientoOctavosFinal2021 enfrentamiento = (ModeloIndividualEnfrentamientoOctavosFinal2021) value;
			//Agregar calendario
			CalendarioFaseOF2021 calendarioFaseOF = new CalendarioFaseOF2021();
			calendarioFaseOF.setId(id);
			calendarioFaseOF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2021.SEGUNDA);
			calendarioFaseOF.setNumeroEnfrentamiento(id);
			calendarioFaseOF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseOF.setJugador1Id(0);
			calendarioFaseOF.setJuegosJugador1P1(0);
			calendarioFaseOF.setJuegosJugador1P2(0);
			calendarioFaseOF.setJuegosJugador1P3(0);
			calendarioFaseOF.setJuegosJugador2P1(0);
			calendarioFaseOF.setJuegosJugador2P2(0);
			calendarioFaseOF.setJuegosJugador2P3(0);
			calendarioFaseOF.setJugador2Id(0);
			calendarioFaseOF.setOrden(key);
			calendarioFaseOF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseOF.setActivo(Activo2021.SI);
			calendarioFaseOF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseOF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseOF.setGrupoProcedenciaJugador1(enfrentamiento.getGrupoProcedenciaJugador1());
			calendarioFaseOF.setPosicionProcedenciaJugador1(enfrentamiento.getPosicionProcedenciaJugador1());
			calendarioFaseOF.setGrupoProcedenciaJugador2(enfrentamiento.getGrupoProcedenciaJugador2());
			calendarioFaseOF.setPosicionProcedenciaJugador2(enfrentamiento.getPosicionProcedenciaJugador2());
			
			//Asignar la bolera por parte del cuadro
			if(id > 0 && id <3 ){
				calendarioFaseOF.setBoleraId(boleraAsignar1.getId());
			}else if(id > 2 && id <5 ){
				calendarioFaseOF.setBoleraId(boleraAsignar2.getId());
			}
			id++;
			calendarioFaseOFList.add(calendarioFaseOF);
		}
		return calendarioFaseOFList;
	}
	
	private List<CalendarioFaseCF2021> prepareCalendarioFaseCFModeloGrupos2(List<CalendarioFaseCF2021> calendarioFaseCFList, Fases2021 faseAnterior, Boleras2021 boleraAsignar1, Boleras2021 boleraAsignar2) {
		ModeloIndividualCuartosFinal2Grupos2021 modeloCuartosFinal2Grupos = new ModeloIndividualCuartosFinal2Grupos2021();
		Integer id = 1;
		
		for (Entry<Integer, ModeloIndividualEnfrentamientoCuartosFinal2021> entry : modeloCuartosFinal2Grupos.getEnfrentamientosCuartosFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloIndividualEnfrentamientoCuartosFinal2021 enfrentamiento = (ModeloIndividualEnfrentamientoCuartosFinal2021) value;
			//Agregar calendario
			CalendarioFaseCF2021 calendarioFaseCF = new CalendarioFaseCF2021();
			calendarioFaseCF.setId(id);
			calendarioFaseCF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2021.SEGUNDA);
			calendarioFaseCF.setNumeroEnfrentamiento(id);
			calendarioFaseCF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseCF.setJugador1Id(0);
			calendarioFaseCF.setJuegosJugador1P1(0);
			calendarioFaseCF.setJuegosJugador1P2(0);
			calendarioFaseCF.setJuegosJugador1P3(0);
			calendarioFaseCF.setJuegosJugador2P1(0);
			calendarioFaseCF.setJuegosJugador2P2(0);
			calendarioFaseCF.setJuegosJugador2P3(0);
			calendarioFaseCF.setJugador2Id(0);
			calendarioFaseCF.setOrden(key);
			calendarioFaseCF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseCF.setActivo(Activo2021.SI);
			calendarioFaseCF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseCF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseCF.setGrupoProcedenciaJugador1(enfrentamiento.getGrupoProcedenciaJugador1());
			calendarioFaseCF.setPosicionProcedenciaJugador1(enfrentamiento.getPosicionProcedenciaJugador1());
			calendarioFaseCF.setGrupoProcedenciaJugador2(enfrentamiento.getGrupoProcedenciaJugador2());
			calendarioFaseCF.setPosicionProcedenciaJugador2(enfrentamiento.getPosicionProcedenciaJugador2());
			
			//Asignar la bolera por parte del cuadro
			if(id > 0 && id <5 ){
				calendarioFaseCF.setBoleraId(boleraAsignar1.getId());
			}else if(id > 4 && id <9 ){
				calendarioFaseCF.setBoleraId(boleraAsignar2.getId());
			}
			id++;
			calendarioFaseCFList.add(calendarioFaseCF);
		}
		return calendarioFaseCFList;
	}
	
	private List<CalendarioFaseCF2021> prepareCalendarioFaseCFModeloGrupos4(List<CalendarioFaseCF2021> calendarioFaseCFList, Fases2021 faseAnterior, Boleras2021 boleraAsignar1, Boleras2021 boleraAsignar2) {
		ModeloIndividualCuartosFinal4Grupos2021 modeloCuartosFinal4Grupos = new ModeloIndividualCuartosFinal4Grupos2021();
		Integer id = 1;
		
		for (Entry<Integer, ModeloIndividualEnfrentamientoCuartosFinal2021> entry : modeloCuartosFinal4Grupos.getEnfrentamientosCuartosFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloIndividualEnfrentamientoCuartosFinal2021 enfrentamiento = (ModeloIndividualEnfrentamientoCuartosFinal2021) value;
			//Agregar calendario
			CalendarioFaseCF2021 calendarioFaseCF = new CalendarioFaseCF2021();
			calendarioFaseCF.setId(id);
			calendarioFaseCF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2021.SEGUNDA);
			calendarioFaseCF.setNumeroEnfrentamiento(id);
			calendarioFaseCF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseCF.setJugador1Id(0);
			calendarioFaseCF.setJuegosJugador1P1(0);
			calendarioFaseCF.setJuegosJugador1P2(0);
			calendarioFaseCF.setJuegosJugador1P3(0);
			calendarioFaseCF.setJuegosJugador2P1(0);
			calendarioFaseCF.setJuegosJugador2P2(0);
			calendarioFaseCF.setJuegosJugador2P3(0);
			calendarioFaseCF.setJugador2Id(0);
			calendarioFaseCF.setOrden(key);
			calendarioFaseCF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseCF.setActivo(Activo2021.SI);
			calendarioFaseCF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseCF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseCF.setGrupoProcedenciaJugador1(enfrentamiento.getGrupoProcedenciaJugador1());
			calendarioFaseCF.setPosicionProcedenciaJugador1(enfrentamiento.getPosicionProcedenciaJugador1());
			calendarioFaseCF.setGrupoProcedenciaJugador2(enfrentamiento.getGrupoProcedenciaJugador2());
			calendarioFaseCF.setPosicionProcedenciaJugador2(enfrentamiento.getPosicionProcedenciaJugador2());
			
			//Asignar la bolera por parte del cuadro
			if(id > 0 && id <5 ){
				calendarioFaseCF.setBoleraId(boleraAsignar1.getId());
			}else if(id > 4 && id <9 ){
				calendarioFaseCF.setBoleraId(boleraAsignar2.getId());
			}
			id++;
			calendarioFaseCFList.add(calendarioFaseCF);
		}
		return calendarioFaseCFList;
	}
	
	private List<CalendarioFaseCF2021> prepareCalendarioFaseCFModeloOF(List<CalendarioFaseCF2021> calendarioFaseCFList, Fases2021 faseAnterior, Boleras2021 boleraAsignar1, Boleras2021 boleraAsignar2) {
		ModeloIndividualCuartosFinal4OF2021 modeloCuartosFinal4OF = new ModeloIndividualCuartosFinal4OF2021();
		Integer id = 1;
		
		for (Entry<Integer, ModeloIndividualEnfrentamientoCuartosFinal2021> entry : modeloCuartosFinal4OF.getEnfrentamientosCuartosFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloIndividualEnfrentamientoCuartosFinal2021 enfrentamiento = (ModeloIndividualEnfrentamientoCuartosFinal2021) value;
			//Agregar calendario
			CalendarioFaseCF2021 calendarioFaseCF = new CalendarioFaseCF2021();
			calendarioFaseCF.setId(id);
			calendarioFaseCF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2021.SEGUNDA);
			calendarioFaseCF.setNumeroEnfrentamiento(id);
			calendarioFaseCF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseCF.setJugador1Id(0);
			calendarioFaseCF.setJuegosJugador1P1(0);
			calendarioFaseCF.setJuegosJugador1P2(0);
			calendarioFaseCF.setJuegosJugador1P3(0);
			calendarioFaseCF.setJuegosJugador2P1(0);
			calendarioFaseCF.setJuegosJugador2P2(0);
			calendarioFaseCF.setJuegosJugador2P3(0);
			calendarioFaseCF.setJugador2Id(0);
			calendarioFaseCF.setOrden(key);
			calendarioFaseCF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseCF.setActivo(Activo2021.SI);
			calendarioFaseCF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseCF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseCF.setGrupoProcedenciaJugador1(enfrentamiento.getGrupoProcedenciaJugador1());
			calendarioFaseCF.setPosicionProcedenciaJugador1(enfrentamiento.getPosicionProcedenciaJugador1());
			calendarioFaseCF.setGrupoProcedenciaJugador2(enfrentamiento.getGrupoProcedenciaJugador2());
			calendarioFaseCF.setPosicionProcedenciaJugador2(enfrentamiento.getPosicionProcedenciaJugador2());
			
			//Asignar la bolera por parte del cuadro
			if(id > 0 && id <3 ){
				calendarioFaseCF.setBoleraId(boleraAsignar1.getId());
			}else if(id > 2 && id <5 ){
				calendarioFaseCF.setBoleraId(boleraAsignar2.getId());
			}
			id++;
			calendarioFaseCFList.add(calendarioFaseCF);
		}
		return calendarioFaseCFList;
	}
	
	private List<CalendarioFaseCF2021> prepareCalendarioFaseCFModeloOFClasifican3(List<CalendarioFaseCF2021> calendarioFaseCFList, Fases2021 faseAnterior, Boleras2021 boleraAsignar1, Boleras2021 boleraAsignar2) {
		ModeloIndividualCuartosFinal4OFClasifican32021 modeloCuartosFinal4OF = new ModeloIndividualCuartosFinal4OFClasifican32021();
		Integer id = 1;
		
		for (Entry<Integer, ModeloIndividualEnfrentamientoCuartosFinal2021> entry : modeloCuartosFinal4OF.getEnfrentamientosCuartosFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloIndividualEnfrentamientoCuartosFinal2021 enfrentamiento = (ModeloIndividualEnfrentamientoCuartosFinal2021) value;
			//Agregar calendario
			CalendarioFaseCF2021 calendarioFaseCF = new CalendarioFaseCF2021();
			calendarioFaseCF.setId(id);
			calendarioFaseCF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2021.SEGUNDA);
			calendarioFaseCF.setNumeroEnfrentamiento(id);
			calendarioFaseCF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseCF.setJugador1Id(0);
			calendarioFaseCF.setJuegosJugador1P1(0);
			calendarioFaseCF.setJuegosJugador1P2(0);
			calendarioFaseCF.setJuegosJugador1P3(0);
			calendarioFaseCF.setJuegosJugador2P1(0);
			calendarioFaseCF.setJuegosJugador2P2(0);
			calendarioFaseCF.setJuegosJugador2P3(0);
			calendarioFaseCF.setJugador2Id(0);
			calendarioFaseCF.setOrden(key);
			calendarioFaseCF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseCF.setActivo(Activo2021.SI);
			calendarioFaseCF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseCF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseCF.setGrupoProcedenciaJugador1(enfrentamiento.getGrupoProcedenciaJugador1());
			calendarioFaseCF.setPosicionProcedenciaJugador1(enfrentamiento.getPosicionProcedenciaJugador1());
			calendarioFaseCF.setGrupoProcedenciaJugador2(enfrentamiento.getGrupoProcedenciaJugador2());
			calendarioFaseCF.setPosicionProcedenciaJugador2(enfrentamiento.getPosicionProcedenciaJugador2());
			
			//Asignar la bolera por parte del cuadro
			if(id > 0 && id <3 ){
				calendarioFaseCF.setBoleraId(boleraAsignar1.getId());
			}else if(id > 2 && id <5 ){
				calendarioFaseCF.setBoleraId(boleraAsignar2.getId());
			}
			id++;
			calendarioFaseCFList.add(calendarioFaseCF);
		}
		return calendarioFaseCFList;
	}
	
	private List<CalendarioFaseSF2021> prepareCalendarioFaseSFModeloCF(List<CalendarioFaseSF2021> calendarioFaseSFList, Fases2021 faseAnterior, Boleras2021 boleraFinal) {
		ModeloIndividualSemiFinal4CF2021 modeloSemiFinal4CF = new ModeloIndividualSemiFinal4CF2021();
		Integer id = 1;
		
		for (Entry<Integer, ModeloIndividualEnfrentamientoSemiFinal2021> entry : modeloSemiFinal4CF.getEnfrentamientosSemiFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloIndividualEnfrentamientoSemiFinal2021 enfrentamiento = (ModeloIndividualEnfrentamientoSemiFinal2021) value;
			//Agregar calendario
			CalendarioFaseSF2021 calendarioFaseSF = new CalendarioFaseSF2021();
			calendarioFaseSF.setId(id);
			calendarioFaseSF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2021.SEGUNDA);
			calendarioFaseSF.setNumeroEnfrentamiento(id);
			calendarioFaseSF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseSF.setJugador1Id(0);
			calendarioFaseSF.setJuegosJugador1P1(0);
			calendarioFaseSF.setJuegosJugador1P2(0);
			calendarioFaseSF.setJuegosJugador1P3(0);
			calendarioFaseSF.setJuegosJugador2P1(0);
			calendarioFaseSF.setJuegosJugador2P2(0);
			calendarioFaseSF.setJuegosJugador2P3(0);
			calendarioFaseSF.setJugador2Id(0);
			calendarioFaseSF.setOrden(key);
			calendarioFaseSF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseSF.setActivo(Activo2021.SI);
			calendarioFaseSF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseSF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseSF.setGrupoProcedenciaJugador1(enfrentamiento.getGrupoProcedenciaJugador1());
			calendarioFaseSF.setPosicionProcedenciaJugador1(enfrentamiento.getPosicionProcedenciaJugador1());
			calendarioFaseSF.setGrupoProcedenciaJugador2(enfrentamiento.getGrupoProcedenciaJugador2());
			calendarioFaseSF.setPosicionProcedenciaJugador2(enfrentamiento.getPosicionProcedenciaJugador2());
			
			//Asignar la bolera por parte del cuadro
			calendarioFaseSF.setBoleraId(boleraFinal.getId());
			id++;
			calendarioFaseSFList.add(calendarioFaseSF);
		}
		return calendarioFaseSFList;
	}
	
	private List<CalendarioFaseFC2021> prepareCalendarioFaseFCModeloSF(List<CalendarioFaseFC2021> calendarioFaseFCList, Fases2021 faseAnterior, Boleras2021 boleraFinal) {
		ModeloIndividualFinalConsolacion4SF2021 modeloFinalConsolacion4SF = new ModeloIndividualFinalConsolacion4SF2021();
		Integer id = 1;
		
		for (Entry<Integer, ModeloIndividualEnfrentamientoFinalConsolacion2021> entry : modeloFinalConsolacion4SF.getEnfrentamientosFinalConsolacion().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloIndividualEnfrentamientoFinalConsolacion2021 enfrentamiento = (ModeloIndividualEnfrentamientoFinalConsolacion2021) value;
			//Agregar calendario
			CalendarioFaseFC2021 calendarioFaseFC = new CalendarioFaseFC2021();
			calendarioFaseFC.setId(id);
			calendarioFaseFC.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2021.SEGUNDA);
			calendarioFaseFC.setNumeroEnfrentamiento(id);
			calendarioFaseFC.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseFC.setJugador1Id(0);
			calendarioFaseFC.setJuegosJugador1P1(0);
			calendarioFaseFC.setJuegosJugador1P2(0);
			calendarioFaseFC.setJuegosJugador1P3(0);
			calendarioFaseFC.setJuegosJugador2P1(0);
			calendarioFaseFC.setJuegosJugador2P2(0);
			calendarioFaseFC.setJuegosJugador2P3(0);
			calendarioFaseFC.setJugador2Id(0);
			calendarioFaseFC.setOrden(key);
			calendarioFaseFC.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseFC.setActivo(Activo2021.SI);
			calendarioFaseFC.setInicia(enfrentamiento.getSorteo());
			calendarioFaseFC.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseFC.setGrupoProcedenciaJugador1(enfrentamiento.getGrupoProcedenciaJugador1());
			calendarioFaseFC.setPosicionProcedenciaJugador1(enfrentamiento.getPosicionProcedenciaJugador1());
			calendarioFaseFC.setGrupoProcedenciaJugador2(enfrentamiento.getGrupoProcedenciaJugador2());
			calendarioFaseFC.setPosicionProcedenciaJugador2(enfrentamiento.getPosicionProcedenciaJugador2());
			
			//Asignar la bolera por parte del cuadro
			calendarioFaseFC.setBoleraId(boleraFinal.getId());
			id++;
			calendarioFaseFCList.add(calendarioFaseFC);
		}
		return calendarioFaseFCList;
	}
	
	private List<CalendarioFaseFF2021> prepareCalendarioFaseFFModeloSF(List<CalendarioFaseFF2021> calendarioFaseFFList, Fases2021 faseAnterior, Boleras2021 boleraFinal) {
		ModeloIndividualFinal4SF2021 modeloFinal4SF = new ModeloIndividualFinal4SF2021();
		Integer id = 1;
		
		for (Entry<Integer, ModeloIndividualEnfrentamientoFinal2021> entry : modeloFinal4SF.getEnfrentamientosFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloIndividualEnfrentamientoFinal2021 enfrentamiento = (ModeloIndividualEnfrentamientoFinal2021) value;
			//Agregar calendario
			CalendarioFaseFF2021 calendarioFaseFF = new CalendarioFaseFF2021();
			calendarioFaseFF.setId(id);
			calendarioFaseFF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2021.SEGUNDA);
			calendarioFaseFF.setNumeroEnfrentamiento(id);
			calendarioFaseFF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseFF.setJugador1Id(0);
			calendarioFaseFF.setJuegosJugador1P1(0);
			calendarioFaseFF.setJuegosJugador1P2(0);
			calendarioFaseFF.setJuegosJugador1P3(0);
			calendarioFaseFF.setJuegosJugador2P1(0);
			calendarioFaseFF.setJuegosJugador2P2(0);
			calendarioFaseFF.setJuegosJugador2P3(0);
			calendarioFaseFF.setJugador2Id(0);
			calendarioFaseFF.setOrden(key);
			calendarioFaseFF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseFF.setActivo(Activo2021.SI);
			calendarioFaseFF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseFF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseFF.setGrupoProcedenciaJugador1(enfrentamiento.getGrupoProcedenciaJugador1());
			calendarioFaseFF.setPosicionProcedenciaJugador1(enfrentamiento.getPosicionProcedenciaJugador1());
			calendarioFaseFF.setGrupoProcedenciaJugador2(enfrentamiento.getGrupoProcedenciaJugador2());
			calendarioFaseFF.setPosicionProcedenciaJugador2(enfrentamiento.getPosicionProcedenciaJugador2());
			
			//Asignar la bolera por parte del cuadro
			calendarioFaseFF.setBoleraId(boleraFinal.getId());
			id++;
			calendarioFaseFFList.add(calendarioFaseFF);
		}
		return calendarioFaseFFList;
	}

	public String getBolerasOcupadasFI() {
		return bolerasOcupadasFI;
	}

	public void setBolerasOcupadasFI(String bolerasOcupadasFI) {
		this.bolerasOcupadasFI = bolerasOcupadasFI;
	}

	public String getBolerasOcupadasOFCF() {
		return bolerasOcupadasOFCF;
	}

	public void setBolerasOcupadasOFCF(String bolerasOcupadasOFCF) {
		this.bolerasOcupadasOFCF = bolerasOcupadasOFCF;
	}
	
	
	public void doBorrarSorteo(){
		crearCampeonatoMasculinoIndividual.borrarSorteo();
		//Inicializar variables de nuevo
		leerCampeonatoMasculinoIndividual = new LeerCampeonatoMasculinoIndividualSegunda2021();
		//Redirigir
		redirect.redirect(menuView.PAG_OM_ADD_CAMPEONATO_MASCULINO_INDIVIDUAL_SEGUNDA);
	}
	
	public void doConfirmarSorteoCampeonato(){
		crearCampeonatoMasculinoIndividual.actualizarEstadoConfiguracion(EstadosIndividualParejas2021.CONFIG_ESTADO_FINALIZADO);
		//Inicializar variables de nuevo
		leerCampeonatoMasculinoIndividual = new LeerCampeonatoMasculinoIndividualSegunda2021();
		//Redirigir
		redirect.redirect(menuView.PAG_OM_ADD_CAMPEONATO_MASCULINO_INDIVIDUAL_SEGUNDA);		
	}	

}
