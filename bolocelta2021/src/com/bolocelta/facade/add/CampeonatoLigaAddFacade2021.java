package com.bolocelta.facade.add;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.bolocelta.application.MenuView;
import com.bolocelta.application.Redirect;
import com.bolocelta.bbdd.createTable.CrearCampeonatoLiga2021;

@Named
@ManagedBean
@RequestScoped
public class CampeonatoLigaAddFacade2021 implements Serializable{
	
	@Inject
	private Redirect redirect;
	
	@Inject
	private MenuView menuView;
	
	private static final long serialVersionUID = 1L;
	
	private CrearCampeonatoLiga2021 crearCampeonatoLiga = new CrearCampeonatoLiga2021();
	
	public CampeonatoLigaAddFacade2021() {
	}

	public Integer getMaxRowsPaginator(){
		return 8;
	}
	
	public void doRealizarSorteoCampeonato(){
		
		crearCampeonatoLiga.realizarSorteo();

	}
	
	public void doValidarCalendario(){
		
		crearCampeonatoLiga.doValidarCalendario();

	}
	
	
	public void doBorrarSorteo(){
		
		crearCampeonatoLiga.borrarSorteo();
		
		String message = "El sorteo se ha borrado.";
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, message, null));
		
	}
	

}
