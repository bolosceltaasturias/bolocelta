package com.bolocelta.facade.add;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.bolocelta.application.MenuView;
import com.bolocelta.application.Redirect;
import com.bolocelta.application.enumerations.CategoriasEnumeration2021;
import com.bolocelta.application.utils.DateUtils2021;
import com.bolocelta.application.utils.NumberUtils2021;
import com.bolocelta.bbdd.constants.Activo2021;
import com.bolocelta.bbdd.constants.EstadosIndividualParejas2021;
import com.bolocelta.bbdd.constants.FasesModelo2021;
import com.bolocelta.bbdd.constants.GruposLetra2021;
import com.bolocelta.bbdd.constants.TipoEnfrentamiento2021;
import com.bolocelta.bbdd.constants.structure.EstructuraCampeonatoParejasMixto2021;
import com.bolocelta.bbdd.createTable.CrearCampeonatoParejasMixto2021;
import com.bolocelta.bbdd.readTables.LeerBoleras2021;
import com.bolocelta.bbdd.readTables.LeerCampeonatoParejasMixto2021;
import com.bolocelta.entities.Boleras2021;
import com.bolocelta.entities.Categorias2021;
import com.bolocelta.entities.Configuracion2021;
import com.bolocelta.entities.Fases2021;
import com.bolocelta.entities.Jugadores2021;
import com.bolocelta.entities.Parejas2021;
import com.bolocelta.entities.ParticipantesParejas2021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasCuartosFinal2Grupos2021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasCuartosFinal4Grupos2021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasCuartosFinal4OF2021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasEnfrentamientoCuartosFinal2021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasEnfrentamientoFinal2021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasEnfrentamientoFinalConsolacion2021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasEnfrentamientoGruposFaseI2021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasEnfrentamientoOctavosFinal2021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasEnfrentamientoSemiFinal2021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasFinal4SF2021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasFinalConsolacion4SF2021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasGrupos32021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasGrupos42021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasGrupos52021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasGrupos62021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasGrupos72021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasGrupos82021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasOctavosFinal4Grupos2021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasOctavosFinal8Grupos2021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasSemiFinal4CF2021;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasSemifinal2Grupos2021;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseCF2021;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseFC2021;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseFF2021;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseI2021;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseII2021;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseOF2021;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseSF2021;
import com.bolocelta.entities.sorteos.parejas.ClasificacionFaseI2021;
import com.bolocelta.entities.sorteos.parejas.ClasificacionFaseII2021;
import com.bolocelta.entities.sorteos.parejas.Grupos2021;
import com.bolocelta.facade.edit.FichaEquipoFacade2021;

@Named
@ManagedBean
@RequestScoped
public class CampeonatoParejasMixtoAddFacade2021 implements Serializable{
	
	private static final int PAREJA_EQUIPOS_MIXTO = 0;

	@Inject
	private Redirect redirect;
	
	@Inject
	private MenuView menuView;
	
	@Inject
	private FichaEquipoFacade2021 fichaEquipoFacade;
	
	private static final long serialVersionUID = 1L;
	
	private LeerCampeonatoParejasMixto2021 leerCampeonatoParejasMixto2021 = new LeerCampeonatoParejasMixto2021();
	private CrearCampeonatoParejasMixto2021 crearCampeonatoParejasMixto = new CrearCampeonatoParejasMixto2021();
	
	private LeerBoleras2021 leerBoleras = new LeerBoleras2021();
	
	private String diaFechaMaxInscripcion = null;
	private String mesFechaMaxInscripcion = null;
	private String anyoFechaMaxInscripcion = null;
	
	private Date fechaMaxInscripcion = null;
	private String estadoCampeonato = null;
	private String bolera = null;
	private String observacionesCampeonato1 = null;
	private String observacionesCampeonato2 = null;
	private String observacionesCampeonato3 = null;
	private String observacionesCampeonato4 = null;
	private String observacionesCampeonato5 = null;
	private String observacionesCampeonato6 = null;
	private String observacionesCampeonato7 = null;
	private String bolerasOcupadasFI = null;
	private String bolerasOcupadasOFCF = null;
	
	private List<Configuracion2021> resultListConfiguracion = null;
	private List<Fases2021> resultListFases = null;
	private List<ParticipantesParejas2021> resultListParticipantes = null;
	private List<ParticipantesParejas2021> resultListParticipantesByEquipo = null;
	
	public CampeonatoParejasMixtoAddFacade2021() {
		Date fechaMaxInscripcion = leerCampeonatoParejasMixto2021.getFechaMaxInscripcion();
		if(fechaMaxInscripcion != null){
			setDiaFechaMaxInscripcion(String.valueOf(fechaMaxInscripcion.getDate()));
			setMesFechaMaxInscripcion(String.valueOf(fechaMaxInscripcion.getMonth()+1));
			setAnyoFechaMaxInscripcion(String.valueOf(fechaMaxInscripcion.getYear()+1900));
		}
		setEstadoCampeonato(leerCampeonatoParejasMixto2021.getEstadoCampeonato());
		setBolera(leerCampeonatoParejasMixto2021.getBoleraFinal());
		setObservacionesCampeonato1(leerCampeonatoParejasMixto2021.getObservacionesCampeonato1());
		setObservacionesCampeonato2(leerCampeonatoParejasMixto2021.getObservacionesCampeonato2());
		setObservacionesCampeonato3(leerCampeonatoParejasMixto2021.getObservacionesCampeonato3());
		setObservacionesCampeonato4(leerCampeonatoParejasMixto2021.getObservacionesCampeonato4());
		setObservacionesCampeonato5(leerCampeonatoParejasMixto2021.getObservacionesCampeonato5());
		setObservacionesCampeonato6(leerCampeonatoParejasMixto2021.getObservacionesCampeonato6());
		setObservacionesCampeonato7(leerCampeonatoParejasMixto2021.getObservacionesCampeonato7());
		setBolerasOcupadasFI(leerCampeonatoParejasMixto2021.getBolerasOcupadasFI());
		setBolerasOcupadasOFCF(leerCampeonatoParejasMixto2021.getBolerasOcupadasOFCF());
	}

	public List<Configuracion2021> getResultListConfiguracion() {
		if(resultListConfiguracion == null){
			resultListConfiguracion = (List<Configuracion2021>) leerCampeonatoParejasMixto2021.listResultConfig();
		}
		return resultListConfiguracion;
	}
	
	public List<Fases2021> getResultListFases() {
		if(resultListFases == null){
			resultListFases = (List<Fases2021>) leerCampeonatoParejasMixto2021.listResultFases();
		}
		return resultListFases;
	}
	
	public List<ParticipantesParejas2021> getResultListParticipantes() {
		if(resultListParticipantes == null){
			resultListParticipantes = (List<ParticipantesParejas2021>) leerCampeonatoParejasMixto2021.listResultParticipantesOrderByEquipo();
		}
		return resultListParticipantes;
	}
	
	public List<ParticipantesParejas2021> getResultListParticipantesByEquipo(Integer idEquipo) {
		if(resultListParticipantesByEquipo == null){
			
			resultListParticipantesByEquipo = new ArrayList<>();

			if(resultListParticipantes == null){
				resultListParticipantes = (List<ParticipantesParejas2021>) leerCampeonatoParejasMixto2021.listResultParticipantesOrderByEquipo();
			}
			
			for (ParticipantesParejas2021 participantesParejas : resultListParticipantes) {
				if(participantesParejas != null && participantesParejas.getPareja() != null && participantesParejas.getPareja().getIdJugador1() != null && participantesParejas.getPareja().getIdJugador2() != null){
					if(participantesParejas.getPareja().getJugador1().getEquipoId().equals(idEquipo) || participantesParejas.getPareja().getJugador2().getEquipoId().equals(idEquipo)){
						resultListParticipantesByEquipo.add(participantesParejas);
					}
				}
			}
		}
		
		return resultListParticipantesByEquipo;
	}
	
	//NUEVO
	public List<ParticipantesParejas2021> getResultListParticipantesByEquipos(Set<Integer> keySetequipoMasParejasQueGruposLibreGrupo) {
		if(resultListParticipantes == null){
			resultListParticipantes = (List<ParticipantesParejas2021>) leerCampeonatoParejasMixto2021.listResultParticipantesOrderByEquipo();
		}
		List<ParticipantesParejas2021> resultListParticipantesParejasMasEquiposQueGrupos = new ArrayList<>();
		for (ParticipantesParejas2021 participantesParejas : resultListParticipantes) {
			if(keySetequipoMasParejasQueGruposLibreGrupo.contains(participantesParejas.getPareja().getJugador1().getEquipoId()) && keySetequipoMasParejasQueGruposLibreGrupo.contains(participantesParejas.getPareja().getJugador2().getEquipoId())){
				resultListParticipantesParejasMasEquiposQueGrupos.add(participantesParejas);
			}
		}
		return resultListParticipantesParejasMasEquiposQueGrupos;
	}
	//F-NUEVO
	
	public Integer getTotalRowsParticipantes(){
		if(resultListParticipantes == null){
			getResultListParticipantes();
		}
		if(resultListParticipantes != null){
			return resultListParticipantes.size();
		}
		return 0;
	}
	
	public Integer getMaxRowsPaginator(){
		return 8;
	}
	
	public boolean isEnabledPaginatorParticipantes(){
		return (getResultListParticipantes() != null && getResultListParticipantes() != null) ? 
					(getResultListParticipantes().size() > getMaxRowsPaginator()) : false;
	}
	
	//Manejo de estados
	public boolean isEstadoCampeonatoSinCrear(){
		return leerCampeonatoParejasMixto2021.isEstadoCampeonatoSinCrear();
	}
	
	public boolean isAbrirInscripciones(){
		return leerCampeonatoParejasMixto2021.isAbrirInscripciones();
	}
	
	public boolean isCerrarInscripciones(){
		return leerCampeonatoParejasMixto2021.isCerrarInscripciones();
	}
	
	public boolean isInscripcionesCerradas(){
		return leerCampeonatoParejasMixto2021.isInscripcionesCerradas();
	}
	
	public boolean isFasesCerradas(){
		return leerCampeonatoParejasMixto2021.isFasesCerradas();
	}
	
	public boolean isSorteoRealizado(){
		return leerCampeonatoParejasMixto2021.isSorteoRealizado();
	}
	
	public boolean isSorteoFinalizado(){
		return leerCampeonatoParejasMixto2021.isSorteoFinalizado();
	}
	
	//Acciones para los diferentes estados
	
	private boolean validateFecha() {
		if(getAnyoFechaMaxInscripcion() != null && getMesFechaMaxInscripcion() != null && getDiaFechaMaxInscripcion() != null){
			if(NumberUtils2021.isNumber(getAnyoFechaMaxInscripcion()) && NumberUtils2021.isNumber(getMesFechaMaxInscripcion()) && NumberUtils2021.isNumber(getDiaFechaMaxInscripcion())){
				Integer dia = Integer.valueOf(getDiaFechaMaxInscripcion());
				if(dia > 31){
					return false;
				}
				Integer mes = Integer.valueOf(getMesFechaMaxInscripcion());
				if(mes > 12){
					return false;
				}
				Integer anyo = Integer.valueOf(getAnyoFechaMaxInscripcion());
				String fecha = (dia < 10 ? "0" + dia : dia) + "/" + (mes < 10 ? "0" + mes : mes) + "/" + anyo;
				Date fechaConvert = DateUtils2021.convertStringToDate(fecha);
				if(fechaConvert == null){
					return false;
				}else{
					setFechaMaxInscripcion(fechaConvert);
					return true;
				}
			}
		}
		return false;
		
	}
	
	public void doCrearBaseCampeonato(){
		
		boolean error = validateFecha();
		if(error){
			crearCampeonatoParejasMixto.crearBaseCampeonato(getFechaMaxInscripcion(), EstadosIndividualParejas2021.CONFIG_ESTADO_CREAR, getBolera());
			//Inicializar variables de nuevo
			leerCampeonatoParejasMixto2021 = new LeerCampeonatoParejasMixto2021();
			//Redirigir
			redirect.redirect(menuView.PAG_OM_ADD_CAMPEONATO_MIXTO_PAREJAS);
		}else{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "La Fecha max. inscripci�n no es valida.", null));
		}
	}
	
	public void doAbrirInscripcionesCampeonato(){
		crearCampeonatoParejasMixto.actualizarEstadoConfiguracion(EstadosIndividualParejas2021.CONFIG_ESTADO_ABRIR_INSCRIPCIONES);
		//Inicializar variables de nuevo
		leerCampeonatoParejasMixto2021 = new LeerCampeonatoParejasMixto2021();
		//Redirigir
		redirect.redirect(menuView.PAG_OM_ADD_CAMPEONATO_MIXTO_PAREJAS);
	}
	
	public void doCerrarInscripcionesCampeonato(){
		crearCampeonatoParejasMixto.actualizarEstadoConfiguracion(EstadosIndividualParejas2021.CONFIG_ESTADO_CERRAR_INSCRIPCIONES);
		//Inicializar variables de nuevo
		leerCampeonatoParejasMixto2021 = new LeerCampeonatoParejasMixto2021();
		//Redirigir
		redirect.redirect(menuView.PAG_OM_ADD_CAMPEONATO_MIXTO_PAREJAS);
	}
	
	public void doInscribirPareja(Jugadores2021 jugador1, Jugadores2021 jugador2, Categorias2021 categoria, String usuario, String activo){
		
		Parejas2021 parejaNew = new Parejas2021();
		parejaNew.setIdJugador1(jugador1.getId());
		parejaNew.setIdJugador2(jugador2.getId());
		parejaNew.setJugador1(jugador1);
		parejaNew.setJugador2(jugador2);
		
		crearCampeonatoParejasMixto.inscribirNewPareja(parejaNew, categoria, usuario, activo);
		//Inicializar variables de nuevo
		fichaEquipoFacade.refrescarParticipantesParejas();
		this.resultListParticipantesByEquipo = null;
		this.resultListParticipantes = null;
		leerCampeonatoParejasMixto2021 = new LeerCampeonatoParejasMixto2021();
		getResultListParticipantesByEquipo(fichaEquipoFacade.getEquipo().getId());
		if(categoria.getId() == CategoriasEnumeration2021.CATEGORIA_MIXTO){
			redirect.getInscripcionParejasMixto();
		}else{
			redirect.getInscripcionParejas();
		}

	}
	
	public void doDesinscribirPareja(ParticipantesParejas2021 pareja,String usuario, String activo){
		crearCampeonatoParejasMixto.desinscribirPareja(pareja, usuario, activo);
		//Inicializar variables de nuevo
		fichaEquipoFacade.refrescarParticipantesParejas();
		this.resultListParticipantesByEquipo = null;
		this.resultListParticipantes = null;
		leerCampeonatoParejasMixto2021 = new LeerCampeonatoParejasMixto2021();
		getResultListParticipantesByEquipo(fichaEquipoFacade.getEquipo().getId());
		if(pareja.getCategoria() == CategoriasEnumeration2021.CATEGORIA_MIXTO){
			redirect.getInscripcionParejasMixto();
		}else{
			redirect.getInscripcionParejas();
		}
	}
	
	public Date getFechaMaxInscripcion() {
		return fechaMaxInscripcion;
	}

	public void setFechaMaxInscripcion(Date fechaMaxInscripcion) {
		this.fechaMaxInscripcion = fechaMaxInscripcion;
	}
	
	public String getBolera() {
		return bolera;
	}

	public void setBolera(String bolera) {
		this.bolera = bolera;
	}
	
	public String getNombreBolera(){
		if(this.bolera != null && this.bolera.length() > 0){
			return leerBoleras.read(Integer.valueOf(this.bolera)).getBolera();
		}
		return "";
	}
	
	public String getObservacionesCampeonato1() {
		return observacionesCampeonato1;
	}

	public void setObservacionesCampeonato1(String observacionesCampeonato1) {
		this.observacionesCampeonato1 = observacionesCampeonato1;
	}
	
	public String getObservacionesCampeonato2() {
		return observacionesCampeonato2;
	}

	public void setObservacionesCampeonato2(String observacionesCampeonato2) {
		this.observacionesCampeonato2 = observacionesCampeonato2;
	}
	
	public String getObservacionesCampeonato3() {
		return observacionesCampeonato3;
	}

	public void setObservacionesCampeonato3(String observacionesCampeonato3) {
		this.observacionesCampeonato3 = observacionesCampeonato3;
	}
	
	public String getObservacionesCampeonato4() {
		return observacionesCampeonato4;
	}

	public void setObservacionesCampeonato4(String observacionesCampeonato4) {
		this.observacionesCampeonato4 = observacionesCampeonato4;
	}
	
	public String getObservacionesCampeonato5() {
		return observacionesCampeonato5;
	}

	public void setObservacionesCampeonato5(String observacionesCampeonato5) {
		this.observacionesCampeonato5 = observacionesCampeonato5;
	}
	
	public String getObservacionesCampeonato6() {
		return observacionesCampeonato6;
	}

	public void setObservacionesCampeonato6(String observacionesCampeonato6) {
		this.observacionesCampeonato6 = observacionesCampeonato6;
	}
	
	public String getObservacionesCampeonato7() {
		return observacionesCampeonato7;
	}

	public void setObservacionesCampeonato7(String observacionesCampeonato7) {
		this.observacionesCampeonato7 = observacionesCampeonato7;
	}

	public String getEstadoCampeonato() {
		return estadoCampeonato;
	}

	public void setEstadoCampeonato(String estadoCampeonato) {
		this.estadoCampeonato = estadoCampeonato;
	}
	
	public String getDiaFechaMaxInscripcion() {
		return diaFechaMaxInscripcion;
	}

	public void setDiaFechaMaxInscripcion(String diaFechaMaxInscripcion) {
		this.diaFechaMaxInscripcion = diaFechaMaxInscripcion;
	}

	public String getMesFechaMaxInscripcion() {
		return mesFechaMaxInscripcion;
	}

	public void setMesFechaMaxInscripcion(String mesFechaMaxInscripcion) {
		this.mesFechaMaxInscripcion = mesFechaMaxInscripcion;
	}

	public String getAnyoFechaMaxInscripcion() {
		return anyoFechaMaxInscripcion;
	}

	public void setAnyoFechaMaxInscripcion(String anyoFechaMaxInscripcion) {
		this.anyoFechaMaxInscripcion = anyoFechaMaxInscripcion;
	}
	
	public void doCambiarActivarFase(Fases2021 fase){
		if(fase.getActivo().equalsIgnoreCase(Activo2021.SI)){
			fase.setActivo(Activo2021.NO);
		}else if(fase.getActivo().equalsIgnoreCase(Activo2021.NO)){
			fase.setActivo(Activo2021.SI);
		}
		crearCampeonatoParejasMixto.actualizarActivoFase(fase);
		leerCampeonatoParejasMixto2021 = new LeerCampeonatoParejasMixto2021();
	}
	
	public void doActualizarDatos(Fases2021 fase){
		crearCampeonatoParejasMixto.actualizarFase(fase);
		leerCampeonatoParejasMixto2021 = new LeerCampeonatoParejasMixto2021();
	}

	public void doCerrarFasesCampeonato(){
		//Inicializar variables de nuevo
		leerCampeonatoParejasMixto2021 = new LeerCampeonatoParejasMixto2021();
		for(Fases2021 fase : getResultListFases()){
			if(fase.getActivo().equals(Activo2021.SI)){
				if(fase.getNumero().equals(FasesModelo2021.FASE_I)){
					crearCampeonatoParejasMixto.crearClasificacionFaseI();
					crearCampeonatoParejasMixto.crearCalendarioFaseI();
				}else if(fase.getNumero().equals(FasesModelo2021.FASE_II)){
					crearCampeonatoParejasMixto.crearCalendarioFaseII();
					crearCampeonatoParejasMixto.crearClasificacionFaseII();
				}else if(fase.getNumero().equals(FasesModelo2021.FASE_OF)){
					crearCampeonatoParejasMixto.crearCalendarioFaseOF();
				}else if(fase.getNumero().equals(FasesModelo2021.FASE_CF)){
					crearCampeonatoParejasMixto.crearCalendarioFaseCF();
				}else if(fase.getNumero().equals(FasesModelo2021.FASE_SF)){
					crearCampeonatoParejasMixto.crearCalendarioFaseSF();
				}else if(fase.getNumero().equals(FasesModelo2021.FASE_FC)){
					crearCampeonatoParejasMixto.crearCalendarioFaseFC();
				}else if(fase.getNumero().equals(FasesModelo2021.FASE_FF)){
					crearCampeonatoParejasMixto.crearCalendarioFaseFF();
				}
			}
		}
		crearCampeonatoParejasMixto.actualizarEstadoConfiguracion(EstadosIndividualParejas2021.CONFIG_ESTADO_CERRAR_FASES);
		//Inicializar variables de nuevo
		leerCampeonatoParejasMixto2021 = new LeerCampeonatoParejasMixto2021();
		//Redirigir
		redirect.redirect(menuView.PAG_OM_ADD_CAMPEONATO_MIXTO_PAREJAS);
	}
	
	public void doRealizarSorteoCampeonato(){
		
		boolean sorteoRealizado = false;
		
		try {
		
			//Asignar las observaciones del campeonato
			EstructuraCampeonatoParejasMixto2021 ecmi = new EstructuraCampeonatoParejasMixto2021();
			crearCampeonatoParejasMixto.updateRow(ecmi.getEstructuraObservacionesCampeonato1(), ecmi.getEstructuraObservacionesCampeonato1().getFila(), observacionesCampeonato1);
			crearCampeonatoParejasMixto.updateRow(ecmi.getEstructuraObservacionesCampeonato2(), ecmi.getEstructuraObservacionesCampeonato2().getFila(), observacionesCampeonato2);
			crearCampeonatoParejasMixto.updateRow(ecmi.getEstructuraObservacionesCampeonato3(), ecmi.getEstructuraObservacionesCampeonato3().getFila(), observacionesCampeonato3);
			crearCampeonatoParejasMixto.updateRow(ecmi.getEstructuraObservacionesCampeonato4(), ecmi.getEstructuraObservacionesCampeonato4().getFila(), observacionesCampeonato4);
			crearCampeonatoParejasMixto.updateRow(ecmi.getEstructuraObservacionesCampeonato5(), ecmi.getEstructuraObservacionesCampeonato5().getFila(), observacionesCampeonato5);
			crearCampeonatoParejasMixto.updateRow(ecmi.getEstructuraObservacionesCampeonato6(), ecmi.getEstructuraObservacionesCampeonato6().getFila(), observacionesCampeonato6);
			crearCampeonatoParejasMixto.updateRow(ecmi.getEstructuraObservacionesCampeonato7(), ecmi.getEstructuraObservacionesCampeonato7().getFila(), observacionesCampeonato7);
			crearCampeonatoParejasMixto.updateRow(ecmi.getEstructuraBolerasOcupadasFI(), ecmi.getEstructuraBolerasOcupadasFI().getFila(), bolerasOcupadasFI);
			crearCampeonatoParejasMixto.updateRow(ecmi.getEstructuraBolerasOcupadasOFCF(), ecmi.getEstructuraBolerasOcupadasOFCF().getFila(), bolerasOcupadasOFCF);
			
			
			
			//Inicializar variables de nuevo
			leerCampeonatoParejasMixto2021 = new LeerCampeonatoParejasMixto2021();
			//Crear los mapas para el sorteo
			//FaseI
			LinkedHashMap<Integer, CalendarioFaseI2021> calendarioFaseIMap = new LinkedHashMap<>();
			LinkedHashMap<Integer, ClasificacionFaseI2021> clasificacionFaseIMap = new LinkedHashMap<>();
			//FaseII
			LinkedHashMap<Integer, CalendarioFaseII2021> calendarioFaseIIMap = new LinkedHashMap<>();
			LinkedHashMap<Integer, ClasificacionFaseII2021> clasificacionFaseIIMap = new LinkedHashMap<>();
			//FaseOF
			LinkedHashMap<Integer, CalendarioFaseOF2021> calendarioFaseOFMap = new LinkedHashMap<>();
			//FaseCF
			LinkedHashMap<Integer, CalendarioFaseCF2021> calendarioFaseCFMap = new LinkedHashMap<>();
			//FaseSF
			LinkedHashMap<Integer, CalendarioFaseSF2021> calendarioFaseSFMap = new LinkedHashMap<>();
			//FaseFC
			LinkedHashMap<Integer, CalendarioFaseFC2021> calendarioFaseFCMap = new LinkedHashMap<>();
			//FaseFF
			LinkedHashMap<Integer, CalendarioFaseFF2021> calendarioFaseFFMap = new LinkedHashMap<>();
			
			//1. Obtener las boleras disponibles, en principio solo las federadas.
			LinkedHashMap<Integer, String> bolerasGrupoAsignadasMap = new LinkedHashMap<>();
			LeerBoleras2021 leerBoleras = new LeerBoleras2021();
			List<Boleras2021> resultListBoleras = (List<Boleras2021>) leerBoleras.listResultFederadas();
			
			//Fase primera
			Integer numerofaseAnterior = 1;
			Integer numerofaseSiguiente = 1;
			
			//Bolera de la final
			Boleras2021 boleraFinal = null;
			for (Configuracion2021 configuracion : getResultListConfiguracion()) {
				if(configuracion.getNombre().equals("Bolera Final")){
					boleraFinal = leerBoleras.read(Integer.valueOf(configuracion.getValor()));
				}
			}
			
			//Bolera Ocupadas FI
			LinkedHashMap<Integer, Boleras2021> bolerasOcupadasFIMap =  new LinkedHashMap<>();
			for (Configuracion2021 configuracion : getResultListConfiguracion()) {
				if(configuracion.getNombre().equals("Boleras Ocupadas FI")){
					String bolerasOcupadas = configuracion.getValor();
					String[] bolerasOcupadasList = bolerasOcupadas.split("-");
					for (String bolera : bolerasOcupadasList) {
						Integer idBolera = Integer.valueOf(bolera);
						if(idBolera != null && idBolera > 0){
							Boleras2021 boleraObject = leerBoleras.read(idBolera);
							if(boleraObject != null){
								bolerasOcupadasFIMap.put(idBolera, boleraObject);
							}
						}
					}
				}
			}
			
			//Boleras Ocupadas OFCF
			LinkedHashMap<Integer, Boleras2021> bolerasOcupadasOFCFMap =  new LinkedHashMap<>();
			for (Configuracion2021 configuracion : getResultListConfiguracion()) {
				if(configuracion.getNombre().equals("Boleras Ocupadas OFCF")){
					String bolerasOcupadas = configuracion.getValor();
					String[] bolerasOcupadasList = bolerasOcupadas.split("-");
					for (String bolera : bolerasOcupadasList) {
						Integer idBolera = Integer.valueOf(bolera);
						if(idBolera != null && idBolera > 0){
							Boleras2021 boleraObject = leerBoleras.read(idBolera);
							if(boleraObject != null){
								bolerasOcupadasOFCFMap.put(idBolera, boleraObject);
							}
						}
					}
				}
			}
			
			//2. Definir la faseI
			for(Fases2021 fase : getResultListFases()){
				if(fase.getActivo().equals(Activo2021.SI)){
					if(fase.getNumero().equals(FasesModelo2021.FASE_I)){
						if(numerofaseSiguiente.equals(fase.getNumero())){
						
							//Objetos
							//Grupos Fase I
							LinkedHashMap<String, Grupos2021> gruposFaseIMap = new LinkedHashMap<>();
							//Parejas Asignados
							LinkedHashMap<Integer, ParticipantesParejas2021> parejasAsignados = new LinkedHashMap<>();
							
							//2.1. Obtener el numero de grupos de la fase
							Integer numeroGruposFase = fase.getNumeroEnfrentamientos();
							Integer numeroGruposFaseAsignados = 0;
							
							//2.2. Obtener el numero de participantes para saber el numero de parejas por grupo y los asignados al grupo para
							Integer numeroParticipantes = getResultListParticipantes().size();
							Integer parejasPorGrupoAsignados = 0;
							
							
							//NUEVO
							//Equipos mismos o mas parejas que grupos (idEquipo, numeroParejas)
							LinkedHashMap<Integer, Integer> equipoMasParejasQueGrupos = new LinkedHashMap<>();
							for (ParticipantesParejas2021 participante : getResultListParticipantes()) {
								
								if(participante.getPareja().getJugador1().getEquipoId().equals(participante.getPareja().getJugador2().getEquipoId())){
									if(equipoMasParejasQueGrupos.containsKey(participante.getPareja().getJugador1().getEquipoId())){
										Integer parejasEquipoValue = equipoMasParejasQueGrupos.get(participante.getPareja().getJugador1().getEquipoId());
										equipoMasParejasQueGrupos.put(participante.getPareja().getJugador1().getEquipoId(), parejasEquipoValue + 1 );
									}else{
										equipoMasParejasQueGrupos.put(participante.getPareja().getJugador1().getEquipoId(), 1);
									}
								}else{
									if(!equipoMasParejasQueGrupos.containsKey(PAREJA_EQUIPOS_MIXTO)){
										equipoMasParejasQueGrupos.put(PAREJA_EQUIPOS_MIXTO, 1);
									}else{
										Integer parejasEquipoValue = equipoMasParejasQueGrupos.get(PAREJA_EQUIPOS_MIXTO);
										equipoMasParejasQueGrupos.put(PAREJA_EQUIPOS_MIXTO, parejasEquipoValue + 1 );
									}
								}
								
							}
							//Lista de equipos con mas parejas que grupos
							LinkedHashMap<Integer, Integer> equipoMasParejasQueGruposLibre = (LinkedHashMap<Integer, Integer>) equipoMasParejasQueGrupos.clone();
							//Limpiar equipos que tengan menos parejas que grupos
							Set<Integer> keySet = equipoMasParejasQueGrupos.keySet();
							for (Integer idEquipoKey : keySet) {
								Integer parejaEquipoValue = equipoMasParejasQueGrupos.get(idEquipoKey);
								if(parejaEquipoValue < numeroGruposFase){
									equipoMasParejasQueGruposLibre.remove(idEquipoKey);
								}
							}
							//F-NUEVO
							
							
							
							//2.3. Crear los grupos a participar
							Integer gruposCreados = 1;
							while(gruposCreados <= numeroGruposFase){
								Grupos2021 grupos = new Grupos2021();
								grupos.setId(GruposLetra2021.getGruposLetraFaseI(gruposCreados));
								grupos.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2021.MIXTO);
								grupos.setFecha(fase.getFecha());
								grupos.setHora(fase.getHora());
								//Asignar bolera aleatoriamente
								Boleras2021 boleraAsignar = null;
								boolean boleraAsignada = false;
								
								//Quito las boleras ocupadas
								List<Boleras2021> resultListBolerasLibres = new ArrayList<>();
								for (Boleras2021 boleras : resultListBoleras) {
									if(!bolerasOcupadasFIMap.containsKey(boleras.getId())){
										resultListBolerasLibres.add(boleras);
									}
								}
								
								
								
								while(!boleraAsignada){
									Random rand = new Random();
									boleraAsignar = resultListBolerasLibres.get(rand.nextInt(resultListBolerasLibres.size()));
									//if(!boleraAsignar.getId().equals(boleraFinal.getId())){
										if(!bolerasOcupadasFIMap.containsKey(boleraAsignar.getId())){
											if(!bolerasGrupoAsignadasMap.containsKey(boleraAsignar.getId())){
												boleraAsignada = true;
											}
										}
									//}
								}
								grupos.setBoleraId(boleraAsignar.getId());
								
								
								//Obtener el numero de parejas del grupo
								BigDecimal parejasPendientesAsignar = new BigDecimal((numeroParticipantes) - parejasPorGrupoAsignados);
								BigDecimal gruposPendientesAsignar = new BigDecimal(numeroGruposFase - numeroGruposFaseAsignados);
								BigDecimal parejasPorGrupoCalculo = parejasPendientesAsignar.divide(gruposPendientesAsignar, RoundingMode.UP);
								grupos.setParejas(parejasPorGrupoCalculo.intValue());
								
								//Agregar los parejas para cada grupo a sorteo
								//A cada grupo sortear los parejas, se obtienen por equipos de mayor numero de parejas a menor
								//Limitaciones: 1-  Se han de repartir los parejas de equipo por grupo, 
								//					si hay mas parejas del equipo por grupo se asigna a alguno de los grupos
								Integer parejasAsignadosGrupo = 0;
								
								//NUEVO
								//Lista de equipos con mas parejas que grupos para cada grupo
								LinkedHashMap<Integer, Integer> equipoMasParejasQueGruposLibreGrupo = (LinkedHashMap<Integer, Integer>) equipoMasParejasQueGruposLibre.clone();
								//F-NUEVO
								
								Integer reintentosEquipoRepetido = 0;
								while(parejasAsignadosGrupo < parejasPorGrupoCalculo.intValue()){
									
									Random rand = new Random();
									
									
									//NUEVO
									ParticipantesParejas2021 parejasSorteo = null;
									
									//Primero buscar parejas pertenecientes a los equipos que tienen los mismos o mas parejas que grupos
									Set<Integer> keySetequipoMasParejasQueGruposLibreGrupo = equipoMasParejasQueGruposLibreGrupo.keySet();
									if(keySetequipoMasParejasQueGruposLibreGrupo.size() > 0){
										List<ParticipantesParejas2021> list = getResultListParticipantesByEquipos(keySetequipoMasParejasQueGruposLibreGrupo);
										boolean parejaCorrecto = false;
										
										if(list.size() == 0){
											while(!parejaCorrecto){
												parejasSorteo = getResultListParticipantes().get(rand.nextInt(getResultListParticipantes().size()));
												if(!parejasAsignados.containsKey(parejasSorteo.getPareja().getId())){
													if(equipoMasParejasQueGruposLibreGrupo.size() > 0){
														equipoMasParejasQueGruposLibreGrupo.remove(parejasSorteo.getPareja().getJugador1().getEquipoId());
													}
													parejaCorrecto = true;
												}
											}
										}else{
										
											while(!parejaCorrecto){
												parejasSorteo = list.get(rand.nextInt(list.size()));
												if(!parejasAsignados.containsKey(parejasSorteo.getPareja().getId())){
													equipoMasParejasQueGruposLibreGrupo.remove(parejasSorteo.getPareja().getJugador1().getEquipoId());
													parejaCorrecto = true;
												}
											}
											
										}
										
									}else{
										boolean parejaCorrecto = false;
										while(!parejaCorrecto){
											parejasSorteo = getResultListParticipantes().get(rand.nextInt(getResultListParticipantes().size()));
											if(!parejasAsignados.containsKey(parejasSorteo.getPareja().getId())){
												if(equipoMasParejasQueGruposLibreGrupo.size() > 0){
													equipoMasParejasQueGruposLibreGrupo.remove(parejasSorteo.getPareja().getJugador1().getEquipoId());
												}
												parejaCorrecto = true;
											}
										}
										
									}
									
									//F-NUEVO
									
									
									
//									if(!grupos.getParejasGrupoList().containsKey(parejasSorteo.getPareja().getId())){
										if(!parejasAsignados.containsKey(parejasSorteo.getPareja().getId())){
											if( !grupos.isExisteParejaMismoEquipo(parejasSorteo.getPareja().getJugador1().getEquipoId(), equipoMasParejasQueGrupos, numeroGruposFase)){
												parejasSorteo.setNumeroParejaGrupo(parejasAsignadosGrupo + 1);
												grupos.getParejasGrupoList().put(parejasSorteo.getPareja().getId(), parejasSorteo);
												parejasAsignadosGrupo++;
												parejasAsignados.put(parejasSorteo.getPareja().getId(), parejasSorteo);
											}else{
												if(reintentosEquipoRepetido >50){
													parejasSorteo.setNumeroParejaGrupo(parejasAsignadosGrupo + 1);
													grupos.getParejasGrupoList().put(parejasSorteo.getPareja().getId(), parejasSorteo);
													parejasAsignadosGrupo++;
													reintentosEquipoRepetido = 0;
													parejasAsignados.put(parejasSorteo.getPareja().getId(), parejasSorteo);
												}else{
													reintentosEquipoRepetido++;
												}
											}
										}
//									}
								}
								
								//Agregar a los mapas
								gruposFaseIMap.put(grupos.getId(), grupos);
								bolerasGrupoAsignadasMap.put(boleraAsignar.getId(), grupos.getId());
								
								//Actualizar variables
								gruposCreados++;
								numeroGruposFaseAsignados++;
								parejasPorGrupoAsignados += parejasPorGrupoCalculo.intValue();
								
								
							}
							
							//A�adir los datos a el calendario y clasificacion de la fase I
							List<CalendarioFaseI2021> calendarioFaseIList = new ArrayList<CalendarioFaseI2021>();
							List<ClasificacionFaseI2021> clasificacionFaseIList = new ArrayList<ClasificacionFaseI2021>();
							//Se recorren los grupos obtenidos
							//Se a�aden a la clasificacion
							for (Entry<String, Grupos2021> entry : gruposFaseIMap.entrySet()) {
							    Object value = entry.getValue();
							    Grupos2021 grupo = (Grupos2021) value;
							    for (Entry<Integer, ParticipantesParejas2021> entryPi : grupo.getParejasGrupoList().entrySet()) {
							    	Object valuePi = entryPi.getValue();
							    	ParticipantesParejas2021 participantesParejas = (ParticipantesParejas2021) valuePi;
							    	
							    	//Agrego a clasificacion el pareja para el grupo
							    	ClasificacionFaseI2021 cf1 = new ClasificacionFaseI2021();
							    	cf1.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2021.MIXTO);
							    	cf1.setGrupo(grupo.getId());
							    	cf1.setId(participantesParejas.getNumeroParejaGrupo());
							    	cf1.setParejaId(participantesParejas.getPareja().getId());
							    	cf1.setPj(0);
							    	cf1.setPg(0);
							    	cf1.setPe(0);
							    	cf1.setPp(0);
							    	cf1.setPf(0);
							    	cf1.setPc(0);
							    	cf1.setPt(0);
							    	clasificacionFaseIList.add(cf1);
							    }
							}
							//Se a�aden al calendario
							for (Entry<String, Grupos2021> entry : gruposFaseIMap.entrySet()) {
							    Object value = entry.getValue();
							    Grupos2021 grupo = (Grupos2021) value;
							    //Recorro los enfrentamientos para el modelo del grupo
							    if(grupo.getParejas().equals(8)){
							    	calendarioFaseIList = prepareCalendarioFaseIModeloGrupos8(calendarioFaseIList, grupo);
							    }else if(grupo.getParejas().equals(7)){
							    	calendarioFaseIList = prepareCalendarioFaseIModeloGrupos7(calendarioFaseIList, grupo);
							    }else if(grupo.getParejas().equals(6)){
							    	calendarioFaseIList = prepareCalendarioFaseIModeloGrupos6(calendarioFaseIList, grupo);
							    }else if(grupo.getParejas().equals(5)){
							    	calendarioFaseIList = prepareCalendarioFaseIModeloGrupos5(calendarioFaseIList, grupo);
							    }else if(grupo.getParejas().equals(4)){
							    	calendarioFaseIList = prepareCalendarioFaseIModeloGrupos4(calendarioFaseIList, grupo);
							    }else if(grupo.getParejas().equals(3)){
							    	calendarioFaseIList = prepareCalendarioFaseIModeloGrupos3(calendarioFaseIList, grupo);
							    }else if(grupo.getParejas().equals(2)){
							    	calendarioFaseIList = prepareCalendarioFaseIModeloGrupos3(calendarioFaseIList, grupo);
							    }
							    
							}
							
							//Insertar clasificacion y calendario fase I
							//Clasificacion
							for (ClasificacionFaseI2021 clasificacionFaseI : clasificacionFaseIList) {
								crearCampeonatoParejasMixto.insertarRowClasificacionFaseI(clasificacionFaseI.getInsertRow());
							}
							//Calendario
							for (CalendarioFaseI2021 calendarioFaseI : calendarioFaseIList) {
								crearCampeonatoParejasMixto.insertarRowCalendarioFaseI(calendarioFaseI.getInsertRow());
							}
							
							numerofaseSiguiente = fase.getFaseSiguiente();
						}
						
					}
				}
			}
			
			
			//3. Definir la faseII
			for(Fases2021 fase : getResultListFases()){
				if(fase.getActivo().equals(Activo2021.SI)){
					if(fase.getNumero().equals(FasesModelo2021.FASE_II)){
						if(numerofaseSiguiente.equals(fase.getNumero())){
							
						}
					}
				}
			}
			
			//4. Definir la faseOF
			
			Boleras2021 boleraFaseAnterior1 = null;
			Boleras2021 boleraFaseAnterior2 = null;
			
			for(Fases2021 fase : getResultListFases()){
				if(fase.getActivo().equals(Activo2021.SI)){
					if(fase.getNumero().equals(FasesModelo2021.FASE_OF)){
						if(numerofaseSiguiente.equals(fase.getNumero())){
							//Recuperar la fase anterior, para averiguar si era grupos escoger el modelo
							Fases2021 faseAnterior = searchFase(numerofaseAnterior);
							//Asignar boleras aleatoriamente
							Boleras2021 boleraAsignar1 = null;
							Boleras2021 boleraAsignar2 = null;
							boolean boleraAsignada1 = false;
							boolean boleraAsignada2 = false;
							LinkedHashMap<Integer, String> bolerasGrupoAsignadasCFMap = new LinkedHashMap<>();
							while(!boleraAsignada1){
								Random rand = new Random();
								boleraAsignar1 = resultListBoleras.get(rand.nextInt(resultListBoleras.size()));
								if(!boleraAsignar1.getId().equals(boleraFinal.getId())){
									if(!bolerasOcupadasOFCFMap.containsKey(boleraAsignar1.getId())){
										if(!bolerasGrupoAsignadasCFMap.containsKey(boleraAsignar1.getId())){
											boleraAsignada1 = true;
											boleraFaseAnterior1 = boleraAsignar1;
										}
									}
								}
							}
							while(!boleraAsignada2){
								Random rand = new Random();
								boleraAsignar2 = resultListBoleras.get(rand.nextInt(resultListBoleras.size()));
								if(!boleraAsignar2.getId().equals(boleraFinal.getId())){
									if(!bolerasOcupadasOFCFMap.containsKey(boleraAsignar2.getId())){
										if(!bolerasGrupoAsignadasCFMap.containsKey(boleraAsignar2.getId())){
											boleraAsignada2 = true;
											boleraFaseAnterior2 = boleraAsignar2;
										}
									}
								}
							}
							//Si en la fase previa eran 4 grupos
							if(faseAnterior.getTipoEnfrentamiento().equals(TipoEnfrentamiento2021.GRUPOS)){
								if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra2021.FASE_I_II_G_4)){
									List<CalendarioFaseOF2021> calendarioFaseOFList = new ArrayList<CalendarioFaseOF2021>();
									calendarioFaseOFList = prepareCalendarioFaseOFModeloGrupos4(calendarioFaseOFList, faseAnterior, boleraAsignar1, boleraAsignar2);
									//Calendario
									for (CalendarioFaseOF2021 calendarioFaseOF : calendarioFaseOFList) {
										crearCampeonatoParejasMixto.insertarRowCalendarioFaseOF(calendarioFaseOF.getInsertRow());
									}
									numerofaseSiguiente = fase.getFaseSiguiente();
									numerofaseAnterior = fase.getNumero();
								}
								//Si en la fase previa eran 8 grupos
								else if(faseAnterior.getTipoEnfrentamiento().equals(TipoEnfrentamiento2021.GRUPOS)){
									if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra2021.FASE_I_II_G_8)){
										List<CalendarioFaseOF2021> calendarioFaseOFList = new ArrayList<CalendarioFaseOF2021>();
										calendarioFaseOFList = prepareCalendarioFaseOFModeloGrupos8(calendarioFaseOFList, faseAnterior, boleraAsignar1, boleraAsignar2);
										//Calendario
										for (CalendarioFaseOF2021 calendarioFaseOF : calendarioFaseOFList) {
											crearCampeonatoParejasMixto.insertarRowCalendarioFaseOF(calendarioFaseOF.getInsertRow());
										}
										numerofaseSiguiente = fase.getFaseSiguiente();
										numerofaseAnterior = fase.getNumero();
									}
								}
							}
							
						}
					}
				}
			}
			
			//5. Definir la faseCF
			for(Fases2021 fase : getResultListFases()){
				if(fase.getActivo().equals(Activo2021.SI)){
					if(fase.getNumero().equals(FasesModelo2021.FASE_CF)){
						if(numerofaseSiguiente.equals(fase.getNumero())){
							//Recuperar la fase anterior, para averiguar si era grupos escoger el modelo
							Fases2021 faseAnterior = searchFase(numerofaseAnterior);
							//Si en la fase previa eran 4 grupos
							if(faseAnterior.getTipoEnfrentamiento().equals(TipoEnfrentamiento2021.GRUPOS)){
								if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra2021.FASE_I_II_G_4)){
									
									//Asignar boleras aleatoriamente
									Boleras2021 boleraAsignar1 = null;
									Boleras2021 boleraAsignar2 = null;
									boolean boleraAsignada1 = false;
									boolean boleraAsignada2 = false;
									LinkedHashMap<Integer, String> bolerasGrupoAsignadasCFMap = new LinkedHashMap<>();
									while(!boleraAsignada1){
										Random rand = new Random();
										boleraAsignar1 = resultListBoleras.get(rand.nextInt(resultListBoleras.size()));
										if(!boleraAsignar1.getId().equals(boleraFinal.getId())){
											if(!bolerasOcupadasOFCFMap.containsKey(boleraAsignar1.getId())){
												if(!bolerasGrupoAsignadasCFMap.containsKey(boleraAsignar1.getId())){
													boleraAsignada1 = true;
												}
											}
										}
									}
									while(!boleraAsignada2){
										Random rand = new Random();
										boleraAsignar2 = resultListBoleras.get(rand.nextInt(resultListBoleras.size()));
										if(!boleraAsignar2.getId().equals(boleraFinal.getId())){
											if(!bolerasOcupadasOFCFMap.containsKey(boleraAsignar2.getId())){
												if(!bolerasGrupoAsignadasCFMap.containsKey(boleraAsignar2.getId())){
													boleraAsignada2 = true;
												}
											}
										}
									}
									
									List<CalendarioFaseCF2021> calendarioFaseCFList = new ArrayList<CalendarioFaseCF2021>();
									calendarioFaseCFList = prepareCalendarioFaseCFModeloGrupos4(calendarioFaseCFList, faseAnterior, boleraAsignar1, boleraAsignar2);
									//Calendario
									for (CalendarioFaseCF2021 calendarioFaseCF : calendarioFaseCFList) {
										crearCampeonatoParejasMixto.insertarRowCalendarioFaseCF(calendarioFaseCF.getInsertRow());
									}
									numerofaseSiguiente = fase.getFaseSiguiente();
									numerofaseAnterior = fase.getNumero();
								}else if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra2021.FASE_I_II_G_2)){
									
									//Asignar boleras aleatoriamente
									Boleras2021 boleraAsignar1 = null;
									Boleras2021 boleraAsignar2 = null;
									boolean boleraAsignada1 = false;
									boolean boleraAsignada2 = false;
									LinkedHashMap<Integer, String> bolerasGrupoAsignadasCFMap = new LinkedHashMap<>();
									while(!boleraAsignada1){
										Random rand = new Random();
										boleraAsignar1 = resultListBoleras.get(rand.nextInt(resultListBoleras.size()));
										if(!boleraAsignar1.getId().equals(boleraFinal.getId())){
											if(!bolerasOcupadasOFCFMap.containsKey(boleraAsignar1.getId())){
												if(!bolerasGrupoAsignadasCFMap.containsKey(boleraAsignar1.getId())){
													boleraAsignada1 = true;
												}
											}
										}
									}
									while(!boleraAsignada2){
										Random rand = new Random();
										boleraAsignar2 = resultListBoleras.get(rand.nextInt(resultListBoleras.size()));
										if(!boleraAsignar2.getId().equals(boleraFinal.getId())){
											if(!bolerasOcupadasOFCFMap.containsKey(boleraAsignar2.getId())){
												if(!bolerasGrupoAsignadasCFMap.containsKey(boleraAsignar2.getId())){
													boleraAsignada2 = true;
												}
											}
										}
									}
									
									List<CalendarioFaseCF2021> calendarioFaseCFList = new ArrayList<CalendarioFaseCF2021>();
									calendarioFaseCFList = prepareCalendarioFaseCFModeloGrupos2(calendarioFaseCFList, faseAnterior, boleraAsignar1, boleraAsignar2);
									//Calendario
									for (CalendarioFaseCF2021 calendarioFaseCF : calendarioFaseCFList) {
										crearCampeonatoParejasMixto.insertarRowCalendarioFaseCF(calendarioFaseCF.getInsertRow());
									}
									numerofaseSiguiente = fase.getFaseSiguiente();
									numerofaseAnterior = fase.getNumero();
								}
							}
							//Si en la fase previa eran Octavos de final
							else if(fase.getTipoEnfrentamiento().equals(TipoEnfrentamiento2021.DIRECTO)){
								if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra2021.FASE_OF)){
									List<CalendarioFaseCF2021> calendarioFaseCFList = new ArrayList<CalendarioFaseCF2021>();
									calendarioFaseCFList = prepareCalendarioFaseCFModeloOF(calendarioFaseCFList, faseAnterior, boleraFaseAnterior1, boleraFaseAnterior2);
									//Calendario
									for (CalendarioFaseCF2021 calendarioFaseCF : calendarioFaseCFList) {
										crearCampeonatoParejasMixto.insertarRowCalendarioFaseCF(calendarioFaseCF.getInsertRow());
									}
									numerofaseSiguiente = fase.getFaseSiguiente();
									numerofaseAnterior = fase.getNumero();
								}
							}
						}
					}
				}
			}
			//6. Definir la faseSF
			
			for(Fases2021 fase : getResultListFases()){
				if(fase.getActivo().equals(Activo2021.SI)){
					if(fase.getNumero().equals(FasesModelo2021.FASE_SF)){
						if(numerofaseSiguiente.equals(fase.getNumero())){
							//Recuperar la fase anterior, para averiguar si era grupos escoger el modelo
							Fases2021 faseAnterior = searchFase(numerofaseAnterior);
							
							
							
							//Si en la fase previa eran grupos
							if(faseAnterior.getTipoEnfrentamiento().equals(TipoEnfrentamiento2021.GRUPOS)){
								if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra2021.FASE_I_II_G_2)){
									
									List<CalendarioFaseSF2021> calendarioFaseSFList = new ArrayList<CalendarioFaseSF2021>();
									calendarioFaseSFList = prepareCalendarioFaseSFModeloGrupos2(calendarioFaseSFList, faseAnterior, boleraFinal);
									//Calendario
									for (CalendarioFaseSF2021 calendarioFaseSF : calendarioFaseSFList) {
										crearCampeonatoParejasMixto.insertarRowCalendarioFaseSF(calendarioFaseSF.getInsertRow());
									}
									numerofaseSiguiente = fase.getFaseSiguiente();
									numerofaseAnterior = fase.getNumero();
								}
							}
							//Si en la fase previa eran Cuartos de final
							else if(fase.getTipoEnfrentamiento().equals(TipoEnfrentamiento2021.DIRECTO)){
								if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra2021.FASE_CF)){
									List<CalendarioFaseSF2021> calendarioFaseSFList = new ArrayList<CalendarioFaseSF2021>();
									calendarioFaseSFList = prepareCalendarioFaseSFModeloCF(calendarioFaseSFList, faseAnterior, boleraFinal);
									//Calendario
									for (CalendarioFaseSF2021 calendarioFaseSF : calendarioFaseSFList) {
										crearCampeonatoParejasMixto.insertarRowCalendarioFaseSF(calendarioFaseSF.getInsertRow());
									}
									numerofaseSiguiente = fase.getFaseSiguiente();
									numerofaseAnterior = fase.getNumero();
								}
							}
						}
					}
				}
			}
			
			//7. Definir la faseFC
			
			for(Fases2021 fase : getResultListFases()){
				if(fase.getActivo().equals(Activo2021.SI)){
					if(fase.getNumero().equals(FasesModelo2021.FASE_FC)){
						//if(numerofaseSiguiente.equals(fase.getNumero())){
							//Recuperar la fase anterior, para averiguar si era grupos escoger el modelo
							Fases2021 faseAnterior = searchFase(numerofaseAnterior);
							//Si en la fase previa eran Semifinal
							if(fase.getTipoEnfrentamiento().equals(TipoEnfrentamiento2021.DIRECTO)){
								if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra2021.FASE_SF)){
									List<CalendarioFaseFC2021> calendarioFaseFCList = new ArrayList<CalendarioFaseFC2021>();
									calendarioFaseFCList = prepareCalendarioFaseFCModeloSF(calendarioFaseFCList, faseAnterior, boleraFinal);
									//Calendario
									for (CalendarioFaseFC2021 calendarioFaseFC : calendarioFaseFCList) {
										crearCampeonatoParejasMixto.insertarRowCalendarioFaseFC(calendarioFaseFC.getInsertRow());
									}
									//numerofaseSiguiente = fase.getFaseSiguiente();
								}
							}
						//}
					}
				}
			}
			
			//8. Definir la faseFF
			
			for(Fases2021 fase : getResultListFases()){
				if(fase.getActivo().equals(Activo2021.SI)){
					if(fase.getNumero().equals(FasesModelo2021.FASE_FF)){
						if(numerofaseSiguiente.equals(fase.getNumero())){
							//Recuperar la fase anterior, para averiguar si era grupos escoger el modelo
							Fases2021 faseAnterior = searchFase(numerofaseAnterior);
							//Si en la fase previa eran Semifinal
							if(fase.getTipoEnfrentamiento().equals(TipoEnfrentamiento2021.DIRECTO)){
								if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra2021.FASE_SF)){
									List<CalendarioFaseFF2021> calendarioFaseFFList = new ArrayList<CalendarioFaseFF2021>();
									calendarioFaseFFList = prepareCalendarioFaseFFModeloSF(calendarioFaseFFList, faseAnterior, boleraFinal);
									//Calendario
									for (CalendarioFaseFF2021 calendarioFaseFF : calendarioFaseFFList) {
										crearCampeonatoParejasMixto.insertarRowCalendarioFaseFF(calendarioFaseFF.getInsertRow());
										sorteoRealizado = true;
									}
									numerofaseSiguiente = fase.getFaseSiguiente();
								}
							}
						}
					}
				}
			}
		
			if(!sorteoRealizado){
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El Sorteo no se ha realizado.", null));
			}else{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "El Sorteo se ha realizado correctamente.", null));
				crearCampeonatoParejasMixto.actualizarEstadoConfiguracion(EstadosIndividualParejas2021.CONFIG_ESTADO_SORTEADO_CAMPEONATO);
				//Inicializar variables de nuevo
				leerCampeonatoParejasMixto2021 = new LeerCampeonatoParejasMixto2021();
				//Redirigir
				//redirect.redirect(menuView.PAG_OM_ADD_CAMPEONATO_MIXTO_PAREJAS);
				
			}
			
			
		}catch (Exception e) {
			e.printStackTrace();
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El Sorteo no se ha realizado.", null));
		}
		
	}

	private List<CalendarioFaseI2021> prepareCalendarioFaseIModeloGrupos8(List<CalendarioFaseI2021> calendarioFaseIList, Grupos2021 grupo) {
		ModeloParejasGrupos82021 modeloGrupos8 = new ModeloParejasGrupos82021();
		Integer id = 1;
		for (Entry<Integer, ModeloParejasEnfrentamientoGruposFaseI2021> entryPi : modeloGrupos8.getEnfrentamientosGrupo().entrySet()) {
			Integer key = entryPi.getKey();
			Object valuePi = entryPi.getValue();
			ModeloParejasEnfrentamientoGruposFaseI2021 enfrentamiento = (ModeloParejasEnfrentamientoGruposFaseI2021) valuePi;
			//Agregar calendario
			CalendarioFaseI2021 calendarioFaseI = new CalendarioFaseI2021();
			calendarioFaseI.setActivo(Activo2021.SI);
			calendarioFaseI.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2021.MIXTO);
			calendarioFaseI.setGrupo(grupo.getId());
			calendarioFaseI.setId(id);
			calendarioFaseI.setJuegosPareja1(0);
			ParticipantesParejas2021 pi1 = grupo.searchParejaGrupo(enfrentamiento.getPareja1(), grupo.getId());
			calendarioFaseI.setPareja1Id(pi1.getPareja().getId());
			calendarioFaseI.setJuegosPareja2(0);
			ParticipantesParejas2021 pi2 = grupo.searchParejaGrupo(enfrentamiento.getPareja2(), grupo.getId());
			calendarioFaseI.setPareja2Id(pi2.getPareja().getId());
			calendarioFaseI.setOrden(key);
			calendarioFaseI.setInicia(enfrentamiento.getSorteo());
			calendarioFaseI.setBoleraId(grupo.getBoleraId());
			id++;
			calendarioFaseIList.add(calendarioFaseI);
		}
		return calendarioFaseIList;
	}
	
	private List<CalendarioFaseI2021> prepareCalendarioFaseIModeloGrupos7(List<CalendarioFaseI2021> calendarioFaseIList, Grupos2021 grupo) {
		ModeloParejasGrupos72021 modeloGrupos = new ModeloParejasGrupos72021();
		Integer id = 1;
		for (Entry<Integer, ModeloParejasEnfrentamientoGruposFaseI2021> entryPi : modeloGrupos.getEnfrentamientosGrupo().entrySet()) {
			Integer key = entryPi.getKey();
			Object valuePi = entryPi.getValue();
			ModeloParejasEnfrentamientoGruposFaseI2021 enfrentamiento = (ModeloParejasEnfrentamientoGruposFaseI2021) valuePi;
			//Agregar calendario
			CalendarioFaseI2021 calendarioFaseI = new CalendarioFaseI2021();
			calendarioFaseI.setActivo(Activo2021.SI);
			calendarioFaseI.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2021.MIXTO);
			calendarioFaseI.setGrupo(grupo.getId());
			calendarioFaseI.setId(id);
			calendarioFaseI.setJuegosPareja1(0);
			ParticipantesParejas2021 pi1 = grupo.searchParejaGrupo(enfrentamiento.getPareja1(), grupo.getId());
			calendarioFaseI.setPareja1Id(pi1.getPareja().getId());
			calendarioFaseI.setJuegosPareja2(0);
			ParticipantesParejas2021 pi2 = grupo.searchParejaGrupo(enfrentamiento.getPareja2(), grupo.getId());
			calendarioFaseI.setPareja2Id(pi2.getPareja().getId());
			calendarioFaseI.setOrden(key);
			calendarioFaseI.setInicia(enfrentamiento.getSorteo());
			calendarioFaseI.setBoleraId(grupo.getBoleraId());
			id++;
			calendarioFaseIList.add(calendarioFaseI);
		}
		return calendarioFaseIList;
	}
	
	private List<CalendarioFaseI2021> prepareCalendarioFaseIModeloGrupos6(List<CalendarioFaseI2021> calendarioFaseIList, Grupos2021 grupo) {
		ModeloParejasGrupos62021 modeloGrupos = new ModeloParejasGrupos62021();
		Integer id = 1;
		for (Entry<Integer, ModeloParejasEnfrentamientoGruposFaseI2021> entryPi : modeloGrupos.getEnfrentamientosGrupo().entrySet()) {
			Integer key = entryPi.getKey();
			Object valuePi = entryPi.getValue();
			ModeloParejasEnfrentamientoGruposFaseI2021 enfrentamiento = (ModeloParejasEnfrentamientoGruposFaseI2021) valuePi;
			//Agregar calendario
			CalendarioFaseI2021 calendarioFaseI = new CalendarioFaseI2021();
			calendarioFaseI.setActivo(Activo2021.SI);
			calendarioFaseI.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2021.MIXTO);
			calendarioFaseI.setGrupo(grupo.getId());
			calendarioFaseI.setId(id);
			calendarioFaseI.setJuegosPareja1(0);
			ParticipantesParejas2021 pi1 = grupo.searchParejaGrupo(enfrentamiento.getPareja1(), grupo.getId());
			calendarioFaseI.setPareja1Id(pi1.getPareja().getId());
			calendarioFaseI.setJuegosPareja2(0);
			ParticipantesParejas2021 pi2 = grupo.searchParejaGrupo(enfrentamiento.getPareja2(), grupo.getId());
			calendarioFaseI.setPareja2Id(pi2.getPareja().getId());
			calendarioFaseI.setOrden(key);
			calendarioFaseI.setInicia(enfrentamiento.getSorteo());
			calendarioFaseI.setBoleraId(grupo.getBoleraId());
			id++;
			calendarioFaseIList.add(calendarioFaseI);
		}
		return calendarioFaseIList;
	}
	
	private List<CalendarioFaseI2021> prepareCalendarioFaseIModeloGrupos5(List<CalendarioFaseI2021> calendarioFaseIList, Grupos2021 grupo) {
		ModeloParejasGrupos52021 modeloGrupos = new ModeloParejasGrupos52021();
		Integer id = 1;
		for (Entry<Integer, ModeloParejasEnfrentamientoGruposFaseI2021> entryPi : modeloGrupos.getEnfrentamientosGrupo().entrySet()) {
			Integer key = entryPi.getKey();
			Object valuePi = entryPi.getValue();
			ModeloParejasEnfrentamientoGruposFaseI2021 enfrentamiento = (ModeloParejasEnfrentamientoGruposFaseI2021) valuePi;
			//Agregar calendario
			CalendarioFaseI2021 calendarioFaseI = new CalendarioFaseI2021();
			calendarioFaseI.setActivo(Activo2021.SI);
			calendarioFaseI.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2021.MIXTO);
			calendarioFaseI.setGrupo(grupo.getId());
			calendarioFaseI.setId(id);
			calendarioFaseI.setJuegosPareja1(0);
			ParticipantesParejas2021 pi1 = grupo.searchParejaGrupo(enfrentamiento.getPareja1(), grupo.getId());
			calendarioFaseI.setPareja1Id(pi1.getPareja().getId());
			calendarioFaseI.setJuegosPareja2(0);
			ParticipantesParejas2021 pi2 = grupo.searchParejaGrupo(enfrentamiento.getPareja2(), grupo.getId());
			calendarioFaseI.setPareja2Id(pi2.getPareja().getId());
			calendarioFaseI.setOrden(key);
			calendarioFaseI.setInicia(enfrentamiento.getSorteo());
			calendarioFaseI.setBoleraId(grupo.getBoleraId());
			id++;
			calendarioFaseIList.add(calendarioFaseI);
		}
		return calendarioFaseIList;
	}
	
	private List<CalendarioFaseI2021> prepareCalendarioFaseIModeloGrupos4(List<CalendarioFaseI2021> calendarioFaseIList, Grupos2021 grupo) {
		ModeloParejasGrupos42021 modeloGrupos = new ModeloParejasGrupos42021();
		Integer id = 1;
		for (Entry<Integer, ModeloParejasEnfrentamientoGruposFaseI2021> entryPi : modeloGrupos.getEnfrentamientosGrupo().entrySet()) {
			Integer key = entryPi.getKey();
			Object valuePi = entryPi.getValue();
			ModeloParejasEnfrentamientoGruposFaseI2021 enfrentamiento = (ModeloParejasEnfrentamientoGruposFaseI2021) valuePi;
			//Agregar calendario
			CalendarioFaseI2021 calendarioFaseI = new CalendarioFaseI2021();
			calendarioFaseI.setActivo(Activo2021.SI);
			calendarioFaseI.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2021.MIXTO);
			calendarioFaseI.setGrupo(grupo.getId());
			calendarioFaseI.setId(id);
			calendarioFaseI.setJuegosPareja1(0);
			ParticipantesParejas2021 pi1 = grupo.searchParejaGrupo(enfrentamiento.getPareja1(), grupo.getId());
			calendarioFaseI.setPareja1Id(pi1.getPareja().getId());
			calendarioFaseI.setJuegosPareja2(0);
			ParticipantesParejas2021 pi2 = grupo.searchParejaGrupo(enfrentamiento.getPareja2(), grupo.getId());
			calendarioFaseI.setPareja2Id(pi2.getPareja().getId());
			calendarioFaseI.setOrden(key);
			calendarioFaseI.setInicia(enfrentamiento.getSorteo());
			calendarioFaseI.setBoleraId(grupo.getBoleraId());
			id++;
			calendarioFaseIList.add(calendarioFaseI);
		}
		return calendarioFaseIList;
	}
	
	private List<CalendarioFaseI2021> prepareCalendarioFaseIModeloGrupos3(List<CalendarioFaseI2021> calendarioFaseIList, Grupos2021 grupo) {
		ModeloParejasGrupos32021 modeloGrupos = new ModeloParejasGrupos32021();
		Integer id = 1;
		for (Entry<Integer, ModeloParejasEnfrentamientoGruposFaseI2021> entryPi : modeloGrupos.getEnfrentamientosGrupo().entrySet()) {
			Integer key = entryPi.getKey();
			Object valuePi = entryPi.getValue();
			ModeloParejasEnfrentamientoGruposFaseI2021 enfrentamiento = (ModeloParejasEnfrentamientoGruposFaseI2021) valuePi;
			//Agregar calendario
			CalendarioFaseI2021 calendarioFaseI = new CalendarioFaseI2021();
			calendarioFaseI.setActivo(Activo2021.SI);
			calendarioFaseI.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2021.MIXTO);
			calendarioFaseI.setGrupo(grupo.getId());
			calendarioFaseI.setId(id);
			calendarioFaseI.setJuegosPareja1(0);
			ParticipantesParejas2021 pi1 = grupo.searchParejaGrupo(enfrentamiento.getPareja1(), grupo.getId());
			calendarioFaseI.setPareja1Id(pi1.getPareja().getId());
			calendarioFaseI.setJuegosPareja2(0);
			ParticipantesParejas2021 pi2 = grupo.searchParejaGrupo(enfrentamiento.getPareja2(), grupo.getId());
			calendarioFaseI.setPareja2Id(pi2.getPareja().getId());
			calendarioFaseI.setOrden(key);
			calendarioFaseI.setInicia(enfrentamiento.getSorteo());
			calendarioFaseI.setBoleraId(grupo.getBoleraId());
			id++;
			calendarioFaseIList.add(calendarioFaseI);
		}
		return calendarioFaseIList;
	}

	public Fases2021 searchFase(Integer numeroFase){
		for(Fases2021 fase : getResultListFases()){
			if(fase.getNumero().equals(numeroFase)){
				return fase;
			}
		}
		return null;
	}
	
	private List<CalendarioFaseOF2021> prepareCalendarioFaseOFModeloGrupos4(List<CalendarioFaseOF2021> calendarioFaseOFList, Fases2021 faseAnterior, Boleras2021 boleraAsignar1, Boleras2021 boleraAsignar2) {
		ModeloParejasOctavosFinal4Grupos2021 modeloOctavosFinal4Grupos = new ModeloParejasOctavosFinal4Grupos2021();
		Integer id = 1;

		for (Entry<Integer, ModeloParejasEnfrentamientoOctavosFinal2021> entry : modeloOctavosFinal4Grupos.getEnfrentamientosOctavosFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloParejasEnfrentamientoOctavosFinal2021 enfrentamiento = (ModeloParejasEnfrentamientoOctavosFinal2021) value;
			//Agregar calendario
			CalendarioFaseOF2021 calendarioFaseOF = new CalendarioFaseOF2021();
			calendarioFaseOF.setId(id);
			calendarioFaseOF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2021.MIXTO);
			calendarioFaseOF.setNumeroEnfrentamiento(id);
			calendarioFaseOF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseOF.setPareja1Id(0);
			calendarioFaseOF.setJuegosPareja1P1(0);
			calendarioFaseOF.setJuegosPareja1P2(0);
			calendarioFaseOF.setJuegosPareja1P3(0);
			calendarioFaseOF.setJuegosPareja2P1(0);
			calendarioFaseOF.setJuegosPareja2P2(0);
			calendarioFaseOF.setJuegosPareja2P3(0);
			calendarioFaseOF.setPareja2Id(0);
			calendarioFaseOF.setOrden(key);
			calendarioFaseOF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseOF.setActivo(Activo2021.SI);
			calendarioFaseOF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseOF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseOF.setGrupoProcedenciaPareja1(enfrentamiento.getGrupoProcedenciaPareja1());
			calendarioFaseOF.setPosicionProcedenciaPareja1(enfrentamiento.getPosicionProcedenciaPareja1());
			calendarioFaseOF.setGrupoProcedenciaPareja2(enfrentamiento.getGrupoProcedenciaPareja2());
			calendarioFaseOF.setPosicionProcedenciaPareja2(enfrentamiento.getPosicionProcedenciaPareja2());
			
			//Asignar la bolera por parte del cuadro
			if(id > 0 && id <5 ){
				calendarioFaseOF.setBoleraId(boleraAsignar1.getId());
			}else if(id > 4 && id <9 ){
				calendarioFaseOF.setBoleraId(boleraAsignar2.getId());
			}
			id++;
			calendarioFaseOFList.add(calendarioFaseOF);
		}
		return calendarioFaseOFList;
	}
	
	private List<CalendarioFaseOF2021> prepareCalendarioFaseOFModeloGrupos8(List<CalendarioFaseOF2021> calendarioFaseOFList, Fases2021 faseAnterior, Boleras2021 boleraAsignar1, Boleras2021 boleraAsignar2) {
		ModeloParejasOctavosFinal8Grupos2021 modeloOctavosFinal8Grupos = new ModeloParejasOctavosFinal8Grupos2021();
		Integer id = 1;

		for (Entry<Integer, ModeloParejasEnfrentamientoOctavosFinal2021> entry : modeloOctavosFinal8Grupos.getEnfrentamientosOctavosFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloParejasEnfrentamientoOctavosFinal2021 enfrentamiento = (ModeloParejasEnfrentamientoOctavosFinal2021) value;
			//Agregar calendario
			CalendarioFaseOF2021 calendarioFaseOF = new CalendarioFaseOF2021();
			calendarioFaseOF.setId(id);
			calendarioFaseOF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2021.MIXTO);
			calendarioFaseOF.setNumeroEnfrentamiento(id);
			calendarioFaseOF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseOF.setPareja1Id(0);
			calendarioFaseOF.setJuegosPareja1P1(0);
			calendarioFaseOF.setJuegosPareja1P2(0);
			calendarioFaseOF.setJuegosPareja1P3(0);
			calendarioFaseOF.setJuegosPareja2P1(0);
			calendarioFaseOF.setJuegosPareja2P2(0);
			calendarioFaseOF.setJuegosPareja2P3(0);
			calendarioFaseOF.setPareja2Id(0);
			calendarioFaseOF.setOrden(key);
			calendarioFaseOF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseOF.setActivo(Activo2021.SI);
			calendarioFaseOF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseOF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseOF.setGrupoProcedenciaPareja1(enfrentamiento.getGrupoProcedenciaPareja1());
			calendarioFaseOF.setPosicionProcedenciaPareja1(enfrentamiento.getPosicionProcedenciaPareja1());
			calendarioFaseOF.setGrupoProcedenciaPareja2(enfrentamiento.getGrupoProcedenciaPareja2());
			calendarioFaseOF.setPosicionProcedenciaPareja2(enfrentamiento.getPosicionProcedenciaPareja2());
			
			//Asignar la bolera por parte del cuadro
			if(id > 0 && id <5 ){
				calendarioFaseOF.setBoleraId(boleraAsignar1.getId());
			}else if(id > 4 && id <9 ){
				calendarioFaseOF.setBoleraId(boleraAsignar2.getId());
			}
			id++;
			calendarioFaseOFList.add(calendarioFaseOF);
		}
		return calendarioFaseOFList;
	}
	
	private List<CalendarioFaseCF2021> prepareCalendarioFaseCFModeloGrupos4(List<CalendarioFaseCF2021> calendarioFaseCFList, Fases2021 faseAnterior, Boleras2021 boleraAsignar1, Boleras2021 boleraAsignar2) {
		ModeloParejasCuartosFinal4Grupos2021 modeloCuartosFinal4Grupos = new ModeloParejasCuartosFinal4Grupos2021();
		Integer id = 1;
		
		for (Entry<Integer, ModeloParejasEnfrentamientoCuartosFinal2021> entry : modeloCuartosFinal4Grupos.getEnfrentamientosCuartosFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloParejasEnfrentamientoCuartosFinal2021 enfrentamiento = (ModeloParejasEnfrentamientoCuartosFinal2021) value;
			//Agregar calendario
			CalendarioFaseCF2021 calendarioFaseCF = new CalendarioFaseCF2021();
			calendarioFaseCF.setId(id);
			calendarioFaseCF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2021.MIXTO);
			calendarioFaseCF.setNumeroEnfrentamiento(id);
			calendarioFaseCF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseCF.setPareja1Id(0);
			calendarioFaseCF.setJuegosPareja1P1(0);
			calendarioFaseCF.setJuegosPareja1P2(0);
			calendarioFaseCF.setJuegosPareja1P3(0);
			calendarioFaseCF.setJuegosPareja2P1(0);
			calendarioFaseCF.setJuegosPareja2P2(0);
			calendarioFaseCF.setJuegosPareja2P3(0);
			calendarioFaseCF.setPareja2Id(0);
			calendarioFaseCF.setOrden(key);
			calendarioFaseCF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseCF.setActivo(Activo2021.SI);
			calendarioFaseCF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseCF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseCF.setGrupoProcedenciaPareja1(enfrentamiento.getGrupoProcedenciaPareja1());
			calendarioFaseCF.setPosicionProcedenciaPareja1(enfrentamiento.getPosicionProcedenciaPareja1());
			calendarioFaseCF.setGrupoProcedenciaPareja2(enfrentamiento.getGrupoProcedenciaPareja2());
			calendarioFaseCF.setPosicionProcedenciaPareja2(enfrentamiento.getPosicionProcedenciaPareja2());
			
			//Asignar la bolera por parte del cuadro
			if(id > 0 && id <5 ){
				calendarioFaseCF.setBoleraId(boleraAsignar1.getId());
			}else if(id > 4 && id <9 ){
				calendarioFaseCF.setBoleraId(boleraAsignar2.getId());
			}
			id++;
			calendarioFaseCFList.add(calendarioFaseCF);
		}
		return calendarioFaseCFList;
	}
	
	private List<CalendarioFaseCF2021> prepareCalendarioFaseCFModeloGrupos2(List<CalendarioFaseCF2021> calendarioFaseCFList, Fases2021 faseAnterior, Boleras2021 boleraAsignar1, Boleras2021 boleraAsignar2) {
		ModeloParejasCuartosFinal2Grupos2021 modeloCuartosFinal2Grupos = new ModeloParejasCuartosFinal2Grupos2021();
		Integer id = 1;
		
		for (Entry<Integer, ModeloParejasEnfrentamientoCuartosFinal2021> entry : modeloCuartosFinal2Grupos.getEnfrentamientosCuartosFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloParejasEnfrentamientoCuartosFinal2021 enfrentamiento = (ModeloParejasEnfrentamientoCuartosFinal2021) value;
			//Agregar calendario
			CalendarioFaseCF2021 calendarioFaseCF = new CalendarioFaseCF2021();
			calendarioFaseCF.setId(id);
			calendarioFaseCF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2021.MIXTO);
			calendarioFaseCF.setNumeroEnfrentamiento(id);
			calendarioFaseCF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseCF.setPareja1Id(0);
			calendarioFaseCF.setJuegosPareja1P1(0);
			calendarioFaseCF.setJuegosPareja1P2(0);
			calendarioFaseCF.setJuegosPareja1P3(0);
			calendarioFaseCF.setJuegosPareja2P1(0);
			calendarioFaseCF.setJuegosPareja2P2(0);
			calendarioFaseCF.setJuegosPareja2P3(0);
			calendarioFaseCF.setPareja2Id(0);
			calendarioFaseCF.setOrden(key);
			calendarioFaseCF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseCF.setActivo(Activo2021.SI);
			calendarioFaseCF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseCF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseCF.setGrupoProcedenciaPareja1(enfrentamiento.getGrupoProcedenciaPareja1());
			calendarioFaseCF.setPosicionProcedenciaPareja1(enfrentamiento.getPosicionProcedenciaPareja1());
			calendarioFaseCF.setGrupoProcedenciaPareja2(enfrentamiento.getGrupoProcedenciaPareja2());
			calendarioFaseCF.setPosicionProcedenciaPareja2(enfrentamiento.getPosicionProcedenciaPareja2());
			
			//Asignar la bolera por parte del cuadro
			if(id > 0 && id <5 ){
				calendarioFaseCF.setBoleraId(boleraAsignar1.getId());
			}else if(id > 4 && id <9 ){
				calendarioFaseCF.setBoleraId(boleraAsignar2.getId());
			}
			id++;
			calendarioFaseCFList.add(calendarioFaseCF);
		}
		return calendarioFaseCFList;
	}
	
	private List<CalendarioFaseCF2021> prepareCalendarioFaseCFModeloOF(List<CalendarioFaseCF2021> calendarioFaseCFList, Fases2021 faseAnterior, Boleras2021 boleraAsignar1, Boleras2021 boleraAsignar2) {
		ModeloParejasCuartosFinal4OF2021 modeloCuartosFinal4OF = new ModeloParejasCuartosFinal4OF2021();
		Integer id = 1;
		
		for (Entry<Integer, ModeloParejasEnfrentamientoCuartosFinal2021> entry : modeloCuartosFinal4OF.getEnfrentamientosCuartosFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloParejasEnfrentamientoCuartosFinal2021 enfrentamiento = (ModeloParejasEnfrentamientoCuartosFinal2021) value;
			//Agregar calendario
			CalendarioFaseCF2021 calendarioFaseCF = new CalendarioFaseCF2021();
			calendarioFaseCF.setId(id);
			calendarioFaseCF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2021.MIXTO);
			calendarioFaseCF.setNumeroEnfrentamiento(id);
			calendarioFaseCF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseCF.setPareja1Id(0);
			calendarioFaseCF.setJuegosPareja1P1(0);
			calendarioFaseCF.setJuegosPareja1P2(0);
			calendarioFaseCF.setJuegosPareja1P3(0);
			calendarioFaseCF.setJuegosPareja2P1(0);
			calendarioFaseCF.setJuegosPareja2P2(0);
			calendarioFaseCF.setJuegosPareja2P3(0);
			calendarioFaseCF.setPareja2Id(0);
			calendarioFaseCF.setOrden(key);
			calendarioFaseCF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseCF.setActivo(Activo2021.SI);
			calendarioFaseCF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseCF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseCF.setGrupoProcedenciaPareja1(enfrentamiento.getGrupoProcedenciaPareja1());
			calendarioFaseCF.setPosicionProcedenciaPareja1(enfrentamiento.getPosicionProcedenciaPareja1());
			calendarioFaseCF.setGrupoProcedenciaPareja2(enfrentamiento.getGrupoProcedenciaPareja2());
			calendarioFaseCF.setPosicionProcedenciaPareja2(enfrentamiento.getPosicionProcedenciaPareja2());
			
			//Asignar la bolera por parte del cuadro
			if(id > 0 && id <3 ){
				calendarioFaseCF.setBoleraId(boleraAsignar1.getId());
			}else if(id > 2 && id <5 ){
				calendarioFaseCF.setBoleraId(boleraAsignar2.getId());
			}
			id++;
			calendarioFaseCFList.add(calendarioFaseCF);
		}
		return calendarioFaseCFList;
	}

	private List<CalendarioFaseSF2021> prepareCalendarioFaseSFModeloGrupos2(List<CalendarioFaseSF2021> calendarioFaseSFList, Fases2021 faseAnterior, Boleras2021 boleraFinal) {
		ModeloParejasSemifinal2Grupos2021 modeloSemiFinal2Grupos = new ModeloParejasSemifinal2Grupos2021();
		Integer id = 1;
		
		for (Entry<Integer, ModeloParejasEnfrentamientoSemiFinal2021> entry : modeloSemiFinal2Grupos.getEnfrentamientosSemiFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloParejasEnfrentamientoSemiFinal2021 enfrentamiento = (ModeloParejasEnfrentamientoSemiFinal2021) value;
			//Agregar calendario
			CalendarioFaseSF2021 calendarioFaseSF = new CalendarioFaseSF2021();
			calendarioFaseSF.setId(id);
			calendarioFaseSF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2021.MIXTO);
			calendarioFaseSF.setNumeroEnfrentamiento(id);
			calendarioFaseSF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseSF.setPareja1Id(0);
			calendarioFaseSF.setJuegosPareja1P1(0);
			calendarioFaseSF.setJuegosPareja1P2(0);
			calendarioFaseSF.setJuegosPareja1P3(0);
			calendarioFaseSF.setJuegosPareja2P1(0);
			calendarioFaseSF.setJuegosPareja2P2(0);
			calendarioFaseSF.setJuegosPareja2P3(0);
			calendarioFaseSF.setPareja2Id(0);
			calendarioFaseSF.setOrden(key);
			calendarioFaseSF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseSF.setActivo(Activo2021.SI);
			calendarioFaseSF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseSF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseSF.setGrupoProcedenciaPareja1(enfrentamiento.getGrupoProcedenciaPareja1());
			calendarioFaseSF.setPosicionProcedenciaPareja1(enfrentamiento.getPosicionProcedenciaPareja1());
			calendarioFaseSF.setGrupoProcedenciaPareja2(enfrentamiento.getGrupoProcedenciaPareja2());
			calendarioFaseSF.setPosicionProcedenciaPareja2(enfrentamiento.getPosicionProcedenciaPareja2());
			
			//Asignar la bolera por parte del cuadro
			calendarioFaseSF.setBoleraId(boleraFinal.getId());
			id++;
			calendarioFaseSFList.add(calendarioFaseSF);
		}
		return calendarioFaseSFList;
	}
	
	
	private List<CalendarioFaseSF2021> prepareCalendarioFaseSFModeloCF(List<CalendarioFaseSF2021> calendarioFaseSFList, Fases2021 faseAnterior, Boleras2021 boleraFinal) {
		ModeloParejasSemiFinal4CF2021 modeloSemiFinal4CF = new ModeloParejasSemiFinal4CF2021();
		Integer id = 1;
		
		for (Entry<Integer, ModeloParejasEnfrentamientoSemiFinal2021> entry : modeloSemiFinal4CF.getEnfrentamientosSemiFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloParejasEnfrentamientoSemiFinal2021 enfrentamiento = (ModeloParejasEnfrentamientoSemiFinal2021) value;
			//Agregar calendario
			CalendarioFaseSF2021 calendarioFaseSF = new CalendarioFaseSF2021();
			calendarioFaseSF.setId(id);
			calendarioFaseSF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2021.MIXTO);
			calendarioFaseSF.setNumeroEnfrentamiento(id);
			calendarioFaseSF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseSF.setPareja1Id(0);
			calendarioFaseSF.setJuegosPareja1P1(0);
			calendarioFaseSF.setJuegosPareja1P2(0);
			calendarioFaseSF.setJuegosPareja1P3(0);
			calendarioFaseSF.setJuegosPareja2P1(0);
			calendarioFaseSF.setJuegosPareja2P2(0);
			calendarioFaseSF.setJuegosPareja2P3(0);
			calendarioFaseSF.setPareja2Id(0);
			calendarioFaseSF.setOrden(key);
			calendarioFaseSF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseSF.setActivo(Activo2021.SI);
			calendarioFaseSF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseSF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseSF.setGrupoProcedenciaPareja1(enfrentamiento.getGrupoProcedenciaPareja1());
			calendarioFaseSF.setPosicionProcedenciaPareja1(enfrentamiento.getPosicionProcedenciaPareja1());
			calendarioFaseSF.setGrupoProcedenciaPareja2(enfrentamiento.getGrupoProcedenciaPareja2());
			calendarioFaseSF.setPosicionProcedenciaPareja2(enfrentamiento.getPosicionProcedenciaPareja2());
			
			//Asignar la bolera por parte del cuadro
			calendarioFaseSF.setBoleraId(boleraFinal.getId());
			id++;
			calendarioFaseSFList.add(calendarioFaseSF);
		}
		return calendarioFaseSFList;
	}
	
	private List<CalendarioFaseFC2021> prepareCalendarioFaseFCModeloSF(List<CalendarioFaseFC2021> calendarioFaseFCList, Fases2021 faseAnterior, Boleras2021 boleraFinal) {
		ModeloParejasFinalConsolacion4SF2021 modeloFinalConsolacion4SF = new ModeloParejasFinalConsolacion4SF2021();
		Integer id = 1;
		
		for (Entry<Integer, ModeloParejasEnfrentamientoFinalConsolacion2021> entry : modeloFinalConsolacion4SF.getEnfrentamientosFinalConsolacion().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloParejasEnfrentamientoFinalConsolacion2021 enfrentamiento = (ModeloParejasEnfrentamientoFinalConsolacion2021) value;
			//Agregar calendario
			CalendarioFaseFC2021 calendarioFaseFC = new CalendarioFaseFC2021();
			calendarioFaseFC.setId(id);
			calendarioFaseFC.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2021.MIXTO);
			calendarioFaseFC.setNumeroEnfrentamiento(id);
			calendarioFaseFC.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseFC.setPareja1Id(0);
			calendarioFaseFC.setJuegosPareja1P1(0);
			calendarioFaseFC.setJuegosPareja1P2(0);
			calendarioFaseFC.setJuegosPareja1P3(0);
			calendarioFaseFC.setJuegosPareja2P1(0);
			calendarioFaseFC.setJuegosPareja2P2(0);
			calendarioFaseFC.setJuegosPareja2P3(0);
			calendarioFaseFC.setPareja2Id(0);
			calendarioFaseFC.setOrden(key);
			calendarioFaseFC.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseFC.setActivo(Activo2021.SI);
			calendarioFaseFC.setInicia(enfrentamiento.getSorteo());
			calendarioFaseFC.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseFC.setGrupoProcedenciaPareja1(enfrentamiento.getGrupoProcedenciaPareja1());
			calendarioFaseFC.setPosicionProcedenciaPareja1(enfrentamiento.getPosicionProcedenciaPareja1());
			calendarioFaseFC.setGrupoProcedenciaPareja2(enfrentamiento.getGrupoProcedenciaPareja2());
			calendarioFaseFC.setPosicionProcedenciaPareja2(enfrentamiento.getPosicionProcedenciaPareja2());
			
			//Asignar la bolera por parte del cuadro
			calendarioFaseFC.setBoleraId(boleraFinal.getId());
			id++;
			calendarioFaseFCList.add(calendarioFaseFC);
		}
		return calendarioFaseFCList;
	}
	
	private List<CalendarioFaseFF2021> prepareCalendarioFaseFFModeloSF(List<CalendarioFaseFF2021> calendarioFaseFFList, Fases2021 faseAnterior, Boleras2021 boleraFinal) {
		ModeloParejasFinal4SF2021 modeloFinal4SF = new ModeloParejasFinal4SF2021();
		Integer id = 1;
		
		for (Entry<Integer, ModeloParejasEnfrentamientoFinal2021> entry : modeloFinal4SF.getEnfrentamientosFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloParejasEnfrentamientoFinal2021 enfrentamiento = (ModeloParejasEnfrentamientoFinal2021) value;
			//Agregar calendario
			CalendarioFaseFF2021 calendarioFaseFF = new CalendarioFaseFF2021();
			calendarioFaseFF.setId(id);
			calendarioFaseFF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2021.MIXTO);
			calendarioFaseFF.setNumeroEnfrentamiento(id);
			calendarioFaseFF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseFF.setPareja1Id(0);
			calendarioFaseFF.setJuegosPareja1P1(0);
			calendarioFaseFF.setJuegosPareja1P2(0);
			calendarioFaseFF.setJuegosPareja1P3(0);
			calendarioFaseFF.setJuegosPareja2P1(0);
			calendarioFaseFF.setJuegosPareja2P2(0);
			calendarioFaseFF.setJuegosPareja2P3(0);
			calendarioFaseFF.setPareja2Id(0);
			calendarioFaseFF.setOrden(key);
			calendarioFaseFF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseFF.setActivo(Activo2021.SI);
			calendarioFaseFF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseFF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseFF.setGrupoProcedenciaPareja1(enfrentamiento.getGrupoProcedenciaPareja1());
			calendarioFaseFF.setPosicionProcedenciaPareja1(enfrentamiento.getPosicionProcedenciaPareja1());
			calendarioFaseFF.setGrupoProcedenciaPareja2(enfrentamiento.getGrupoProcedenciaPareja2());
			calendarioFaseFF.setPosicionProcedenciaPareja2(enfrentamiento.getPosicionProcedenciaPareja2());
			
			//Asignar la bolera por parte del cuadro
			calendarioFaseFF.setBoleraId(boleraFinal.getId());
			id++;
			calendarioFaseFFList.add(calendarioFaseFF);
		}
		return calendarioFaseFFList;
	}

	public String getBolerasOcupadasFI() {
		return bolerasOcupadasFI;
	}

	public void setBolerasOcupadasFI(String bolerasOcupadasFI) {
		this.bolerasOcupadasFI = bolerasOcupadasFI;
	}

	public String getBolerasOcupadasOFCF() {
		return bolerasOcupadasOFCF;
	}

	public void setBolerasOcupadasOFCF(String bolerasOcupadasOFCF) {
		this.bolerasOcupadasOFCF = bolerasOcupadasOFCF;
	}
	
	public void doBorrarSorteo(){
		crearCampeonatoParejasMixto.borrarSorteo();
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "El Sorteo se ha borrado correctamente. Se puede volver a sortear.", null));
		//Inicializar variables de nuevo
		leerCampeonatoParejasMixto2021 = new LeerCampeonatoParejasMixto2021();
		//Redirigir
		//redirect.redirect(menuView.PAG_OM_ADD_CAMPEONATO_MIXTO_PAREJAS);
	}
	
	public void doConfirmarSorteoCampeonato(){
		crearCampeonatoParejasMixto.actualizarEstadoConfiguracion(EstadosIndividualParejas2021.CONFIG_ESTADO_FINALIZADO);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "El Sorteo se ha confirmado correctamente.", null));
		//Inicializar variables de nuevo
		leerCampeonatoParejasMixto2021 = new LeerCampeonatoParejasMixto2021();
		//Redirigir
		//redirect.redirect(menuView.PAG_OM_ADD_CAMPEONATO_MIXTO_PAREJAS);		
	}
	
	

}
