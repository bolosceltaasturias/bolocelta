package com.bolocelta.facade;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.bolocelta.application.session.SessionState;
import com.bolocelta.bbdd.constants.Activo2021;
import com.bolocelta.bbdd.createTable.CrearCampeonatoLigaFemenina2021;
import com.bolocelta.bbdd.readTables.LeerCampeonatoLigaFemenina2021;
import com.bolocelta.entities.CampeonatoLigaFemeninaCalendario2021;
import com.bolocelta.entities.CampeonatoLigaFemeninaClasificacion2021;

@Named
@ConversationScoped
@ManagedBean
public class CampeonatoLigaFemeninaFacade2021 implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private SessionState sessionState;
	
	private LeerCampeonatoLigaFemenina2021 leerCampeonatoLigaFemenina = new LeerCampeonatoLigaFemenina2021();
	
	private CrearCampeonatoLigaFemenina2021 campeonatoLigaFemenina = new CrearCampeonatoLigaFemenina2021();
	
	private List<CampeonatoLigaFemeninaClasificacion2021> resultListClasificacion = null;
	private List<CampeonatoLigaFemeninaCalendario2021> resultListCalendario = null;

	public List<CampeonatoLigaFemeninaClasificacion2021> getResultListClasificacion() {
		if(resultListClasificacion == null){
			resultListClasificacion = (List<CampeonatoLigaFemeninaClasificacion2021>) leerCampeonatoLigaFemenina.listResultClasificacion();
		}
		return resultListClasificacion;
	}
	
	public List<CampeonatoLigaFemeninaCalendario2021> getResultListCalendario() {
		if(resultListCalendario == null){
			resultListCalendario = (List<CampeonatoLigaFemeninaCalendario2021>) leerCampeonatoLigaFemenina.listResultCalendario();
		}
		return resultListCalendario;
	}

	public Object selected() {
		// TODO Auto-generated method stub
		return null;
	}

	public Object read(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	public String insert(Object insert) {
		// TODO Auto-generated method stub
		return null;
	}

	public String update(Object update) {
		// TODO Auto-generated method stub
		return null;
	}

	public String delete(Object delete) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Integer getRowsPaginatorClasificacion(){
		return getResultListClasificacion().size();
	}
	
	public Integer getRowsPaginatorCalendario(){
		return getResultListCalendario().size()/8;
	}
	
	public Integer getTotalRowsClasificacion(){
		return getResultListClasificacion().size();
	}
	
	   public String getPermisoActualizarResultado(Integer idEquipo){
	    	if(sessionState != null && sessionState.getUserEquipo() != null && sessionState.getUserEquipo().getId() != null){
	    		if(sessionState.getUserEquipo().getId().equals(idEquipo)){
	    			return "BCMEGEARLF";
	    		}
	    	}
	    	return "SIN PERMISO";
	    }
	    
	    public String getPermisoConfirmarResultado(Integer idEquipo){
	    	if(sessionState != null && sessionState.getUserEquipo() != null && sessionState.getUserEquipo().getId() != null){
	    		if(sessionState.getUserEquipo().getId().equals(idEquipo)){
	    			return "BCMEGEARCLF";
	    		}
	    	}
	    	return "SIN PERMISO";
	    }
		
	    public void doActualizarResultadoLiga(CampeonatoLigaFemeninaCalendario2021 cec){
	    	boolean error = false;
			if(cec.isModificable()){
				//Validaciones
				if(cec.getTirada() < 0 || cec.getSacada() < 0 || cec.getPuntos() < 0){
					error = true;
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "La tirada, sacada y puntos han de tenera valor mayor de 0.", null));
				}
				
				//Si todo correcto actualizar resultado
				if(!error){
					campeonatoLigaFemenina.actualizarResultadoCalendarioLiga(cec);
					leerCampeonatoLigaFemenina = new LeerCampeonatoLigaFemenina2021();
					resultListClasificacion = (List<CampeonatoLigaFemeninaClasificacion2021>) leerCampeonatoLigaFemenina.listResultClasificacion();
					resultListCalendario = (List<CampeonatoLigaFemeninaCalendario2021>) leerCampeonatoLigaFemenina.listResultCalendario();
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha actualizado el resultado de la jugadora " + cec.getJugadora().getNombre(), null));
				}
			}
			
		}
	    
		public void doConfirmarResultado(CampeonatoLigaFemeninaCalendario2021 cec){
			boolean error = false;
			if(cec.isModificable()){
				//Validaciones
				if(cec.getTirada() < 0 || cec.getSacada() < 0 || cec.getPuntos() < 0){
					error = true;
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "La tirada, sacada y puntos han de tenera valor mayor de 0.", null));
				}
				
				//Si todo correcto confirmar resultado y actualizar clasificacion
				if(!error){
					CampeonatoLigaFemeninaClasificacion2021 cecCla = recoveryClasificacionEquipo(cec.getJugadoraId());
					campeonatoLigaFemenina.actualizarClasificacion(cecCla, cec);
					cec.setActivo(Activo2021.NO_NUMBER);
					campeonatoLigaFemenina.actualizarResultadoCalendarioLigaConfirmar(cec);
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha confirmado el resultado de la jugadora " + cec.getJugadora().getNombre(), null));
					leerCampeonatoLigaFemenina = new LeerCampeonatoLigaFemenina2021();
					resultListClasificacion = (List<CampeonatoLigaFemeninaClasificacion2021>) leerCampeonatoLigaFemenina.listResultClasificacion();
				}
			}
			
			
		}
		
		public CampeonatoLigaFemeninaClasificacion2021 recoveryClasificacionEquipo(Integer idJugadora){
			for (CampeonatoLigaFemeninaClasificacion2021 cec : getResultListClasificacion()) {
				if(cec.getJugadoraId().equals(idJugadora)){
					return cec;
				}
			}
			return null;
		}	
		

}
