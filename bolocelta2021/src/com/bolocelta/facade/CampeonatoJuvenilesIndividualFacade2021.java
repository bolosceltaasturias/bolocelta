package com.bolocelta.facade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.bolocelta.application.session.SessionState;
import com.bolocelta.bbdd.constants.Activo2021;
import com.bolocelta.bbdd.constants.FasesModelo2021;
import com.bolocelta.bbdd.constants.FasesTabShow2021;
import com.bolocelta.bbdd.createTable.CrearCampeonatoIndividualJuveniles2021;
import com.bolocelta.bbdd.readTables.LeerCampeonatoJuvenilesIndividual2021;
import com.bolocelta.entities.CampeonatoJuvenilesIndividualClasificacion2021;
import com.bolocelta.entities.Jugadores2021;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseCF2021;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseFF2021;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseSF2021;

@Named
@ConversationScoped
@ManagedBean
public class CampeonatoJuvenilesIndividualFacade2021 implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private SessionState sessionState;
	
	private LeerCampeonatoJuvenilesIndividual2021 leerCampeonato = new LeerCampeonatoJuvenilesIndividual2021();
	
	private CrearCampeonatoIndividualJuveniles2021 crearCampeonatoIndividualJuveniles2021 = new CrearCampeonatoIndividualJuveniles2021();
	
	private List<CampeonatoJuvenilesIndividualClasificacion2021> resultListClasificacion = null;
	private List<CalendarioFaseCF2021> resultListCalendarioFaseCF = null;
	private List<CalendarioFaseSF2021> resultListCalendarioFaseSF = null;
	private List<CalendarioFaseFF2021> resultListCalendarioFaseFF = null;

	public List<CampeonatoJuvenilesIndividualClasificacion2021> getResultListClasificacion() {
		if(resultListClasificacion == null){
			resultListClasificacion = (List<CampeonatoJuvenilesIndividualClasificacion2021>) leerCampeonato.listResultClasificacion();
		}
		return resultListClasificacion;
	}
	
	public List<CalendarioFaseCF2021> getResultListCalendarioFaseCF() {
		if(resultListCalendarioFaseCF == null){
			resultListCalendarioFaseCF = (List<CalendarioFaseCF2021>) leerCampeonato.listResultCalendarioFaseCF();
		}
		return resultListCalendarioFaseCF;
	}
	
	public List<CalendarioFaseSF2021> getResultListCalendarioFaseSF() {
		if(resultListCalendarioFaseSF == null){
			resultListCalendarioFaseSF = (List<CalendarioFaseSF2021>) leerCampeonato.listResultCalendarioFaseSF();
		}
		return resultListCalendarioFaseSF;
	}
	
	public List<CalendarioFaseFF2021> getResultListCalendarioFaseFF() {
		if(resultListCalendarioFaseFF == null){
			resultListCalendarioFaseFF = (List<CalendarioFaseFF2021>) leerCampeonato.listResultCalendarioFaseFF();
		}
		return resultListCalendarioFaseFF;
	}

	public Object selected() {
		// TODO Auto-generated method stub
		return null;
	}

	public Object read(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	public String insert(Object insert) {
		// TODO Auto-generated method stub
		return null;
	}

	public String update(Object update) {
		// TODO Auto-generated method stub
		return null;
	}

	public String delete(Object delete) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public List<CalendarioFaseSF2021> getResultListCalendarioByCruceDirectoSF() {
		
		List<CalendarioFaseSF2021> resultListCalendarioByCruceDirecto = new ArrayList<>();
		if(resultListCalendarioFaseSF == null){
			resultListCalendarioFaseSF = (List<CalendarioFaseSF2021>) obtenerCalendarioByCruceDirectoSF();
			return resultListCalendarioFaseSF;
		}
		return resultListCalendarioByCruceDirecto;
	}
	
	public List<CalendarioFaseFF2021> getResultListCalendarioByCruceDirectoFF() {
		
		List<CalendarioFaseFF2021> resultListCalendarioByCruceDirecto = new ArrayList<>();
		if(resultListCalendarioFaseFF == null){
			resultListCalendarioFaseFF = (List<CalendarioFaseFF2021>) obtenerCalendarioByCruceDirectoFF();
			return resultListCalendarioFaseFF;
		}
		return resultListCalendarioByCruceDirecto;
	}
	
	public List<CalendarioFaseSF2021> obtenerCalendarioByCruceDirectoSF(){
		if(resultListCalendarioFaseSF == null){
			resultListCalendarioFaseSF = (List<CalendarioFaseSF2021>) getResultListCalendarioFaseSF();
		}
		//Ordenar la calendario
		Collections.sort(resultListCalendarioFaseSF, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				CalendarioFaseSF2021 cec1 = (CalendarioFaseSF2021) o1;
				CalendarioFaseSF2021 cec2 = (CalendarioFaseSF2021) o2;
				
				int rpuntos = cec1.getOrden().compareTo(cec2.getOrden());
				return rpuntos;
			}
		});
		
		Collections.reverseOrder();
		return resultListCalendarioFaseSF;
	}
	
	public List<CalendarioFaseFF2021> obtenerCalendarioByCruceDirectoFF(){
		if(resultListCalendarioFaseFF == null){
			resultListCalendarioFaseFF = (List<CalendarioFaseFF2021>) getResultListCalendarioFaseFF();
		}
		//Ordenar la calendario
		Collections.sort(resultListCalendarioFaseFF, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				CalendarioFaseFF2021 cec1 = (CalendarioFaseFF2021) o1;
				CalendarioFaseFF2021 cec2 = (CalendarioFaseFF2021) o2;
				
				int rpuntos = cec1.getOrden().compareTo(cec2.getOrden());
				return rpuntos;
			}
		});
		
		Collections.reverseOrder();
		return resultListCalendarioFaseFF;
	}
	
	public boolean isDataCampeonato() {
		if(getResultListFasesTabShow().size() > 0){
			return true;
		}
		return false;
	}
	
	private List<FasesTabShow2021> resultListFasesTabShow = null;
	
	public List<FasesTabShow2021> getResultListFasesTabShow() {
		if(resultListFasesTabShow == null){
			resultListFasesTabShow = new ArrayList<>();
			if(obtenerCalendarioByCruceDirectoSF() != null && obtenerCalendarioByCruceDirectoSF().size() > 0){
				resultListFasesTabShow.add(FasesTabShow2021.searchFaseTabShow(FasesModelo2021.FASE_SF, null));
			}
			if(obtenerCalendarioByCruceDirectoFF() != null && obtenerCalendarioByCruceDirectoFF().size() > 0){
				resultListFasesTabShow.add(FasesTabShow2021.searchFaseTabShow(FasesModelo2021.FASE_FF, null));
				//A�adir grafico
				resultListFasesTabShow.add(FasesTabShow2021.FASE_GRAPHIC);
			}
		}
		return resultListFasesTabShow;
	}
	
	public Integer getRowsPaginatorClasificacion(){
		return getResultListClasificacion().size();
	}
	
	public Integer getTotalRowsClasificacion(){
		return getResultListClasificacion().size();
	}
	
	   public String getPermisoActualizarResultado(){
	    	if(sessionState != null && sessionState.getUserEquipo() != null && sessionState.getUserEquipo().getId().equals(0)){
    			return "BCMEGEARIJ";
    		}
	    	return "SIN PERMISO";
	    }
		
	    public void doActualizarClasificacion(CampeonatoJuvenilesIndividualClasificacion2021 cec){
	    	boolean error = false;
			if(cec.getActivo() == Activo2021.SI_NUMBER){
				//Validaciones
				
				//Si todo correcto actualizar resultado
				if(!error){
					crearCampeonatoIndividualJuveniles2021.actualizarClasificacion(cec);
					leerCampeonato = new LeerCampeonatoJuvenilesIndividual2021();
					resultListClasificacion = (List<CampeonatoJuvenilesIndividualClasificacion2021>) leerCampeonato.listResultClasificacion();
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha actualizado el resultado del jugador " + cec.getJugador().getNombre(), null));
				}
			}
			
		}
	    
		public void doConfirmarResultado(CampeonatoJuvenilesIndividualClasificacion2021 cec){
			boolean error = false;
			if(cec.getActivo() == Activo2021.SI_NUMBER){
				//Validaciones
				
				//Si todo correcto confirmar resultado y actualizar clasificacion
				if(!error){
					crearCampeonatoIndividualJuveniles2021.actualizarClasificacionConfirmar(cec);
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha confirmado el resultado de la jugador " + cec.getJugador().getNombre(), null));
					leerCampeonato = new LeerCampeonatoJuvenilesIndividual2021();
					resultListClasificacion = (List<CampeonatoJuvenilesIndividualClasificacion2021>) leerCampeonato.listResultClasificacion();
					doClasificarToCuartosFinal();
					resultListClasificacion = (List<CampeonatoJuvenilesIndividualClasificacion2021>) leerCampeonato.listResultClasificacion();
					resultListCalendarioFaseCF = (List<CalendarioFaseCF2021>) leerCampeonato.listResultCalendarioFaseCF();
					resultListCalendarioFaseSF = (List<CalendarioFaseSF2021>) leerCampeonato.listResultCalendarioFaseSF();
					resultListCalendarioFaseFF = (List<CalendarioFaseFF2021>) leerCampeonato.listResultCalendarioFaseFF();
				}
			}
			
			
		}		
		
	    public void doActualizarResultadosEnfrentamientoDirectoCF(CalendarioFaseCF2021 calendarioFaseCF){
        	boolean error = false;
        	if(calendarioFaseCF.isModificable() && calendarioFaseCF.isJugadoresEnFase()){
    			
    			Integer juegosJugador1P1 = calendarioFaseCF.getJuegosJugador1P1();
    			Integer juegosJugador1P2 = calendarioFaseCF.getJuegosJugador1P2();
    			Integer juegosJugador1P3 = calendarioFaseCF.getJuegosJugador1P3();
    			Integer juegosJugador2P1 = calendarioFaseCF.getJuegosJugador2P1();
    			Integer juegosJugador2P2 = calendarioFaseCF.getJuegosJugador2P2();
    			Integer juegosJugador2P3 = calendarioFaseCF.getJuegosJugador2P3();
    			
    			//Si todo correcto actualizar resultado
    			if(!error){
    				crearCampeonatoIndividualJuveniles2021.actualizarResultadosEnfrentamientoDirectoCF(calendarioFaseCF);
    				doClasificarToSemifinal();
    	    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha actualizado el resultado de Cuartos de Final.", null));
    	    		leerCampeonato = new LeerCampeonatoJuvenilesIndividual2021();
    			}
    		}else if(!calendarioFaseCF.isJugadoresEnFase()){
    			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede modificar el resultado, no hay jugadores clasificados para la fase.", null));
    		}
	    		
    	}
		
		public void doActualizarResultadosEnfrentamientoDirectoSF(CalendarioFaseSF2021 calendarioFaseSF){
        	boolean error = false;
        	if(calendarioFaseSF.isModificable() && calendarioFaseSF.isJugadoresEnFase()){
    			
    			Integer juegosJugador1P1 = calendarioFaseSF.getJuegosJugador1P1();
    			Integer juegosJugador1P2 = calendarioFaseSF.getJuegosJugador1P2();
    			Integer juegosJugador1P3 = calendarioFaseSF.getJuegosJugador1P3();
    			Integer juegosJugador2P1 = calendarioFaseSF.getJuegosJugador2P1();
    			Integer juegosJugador2P2 = calendarioFaseSF.getJuegosJugador2P2();
    			Integer juegosJugador2P3 = calendarioFaseSF.getJuegosJugador2P3();
    			
    			//Si todo correcto actualizar resultado
    			if(!error){
    				crearCampeonatoIndividualJuveniles2021.actualizarResultadosEnfrentamientoDirectoSF(calendarioFaseSF);
    				doClasificarToFinal();
    	    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha actualizado el resultado de Semifinales.", null));
    	    		leerCampeonato = new LeerCampeonatoJuvenilesIndividual2021();
    			}
    		}else if(!calendarioFaseSF.isJugadoresEnFase()){
    			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede modificar el resultado, no hay jugadores clasificados para la fase.", null));
    		}
	    		
    	}
	    
	    public void doActualizarResultadosEnfrentamientoDirectoFF(CalendarioFaseFF2021 calendarioFaseFF){
        	boolean error = false;
        	if(calendarioFaseFF.isModificable() && calendarioFaseFF.isJugadoresEnFase()){
    			
    			Integer juegosJugador1P1 = calendarioFaseFF.getJuegosJugador1P1();
    			Integer juegosJugador1P2 = calendarioFaseFF.getJuegosJugador1P2();
    			Integer juegosJugador1P3 = calendarioFaseFF.getJuegosJugador1P3();
    			Integer juegosJugador2P1 = calendarioFaseFF.getJuegosJugador2P1();
    			Integer juegosJugador2P2 = calendarioFaseFF.getJuegosJugador2P2();
    			Integer juegosJugador2P3 = calendarioFaseFF.getJuegosJugador2P3();
    			
    			//Si todo correcto actualizar resultado
    			if(!error){
    				crearCampeonatoIndividualJuveniles2021.actualizarResultadosEnfrentamientoDirectoFF(calendarioFaseFF);
    	    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha actualizado el resultado de Final.", null));
    	    		leerCampeonato = new LeerCampeonatoJuvenilesIndividual2021();
    			}
    		}else if(!calendarioFaseFF.isJugadoresEnFase()){
    			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede modificar el resultado, no hay jugadores clasificados para la fase.", null));
    		}

    	}
	    
	    public void doClasificarToCuartosFinal(){
	    	
	    	Jugadores2021 primero = extraerClasificado(1);
	    	Jugadores2021 segundo = extraerClasificado(2);
	    	Jugadores2021 tercero = extraerClasificado(3);
	    	Jugadores2021 cuarto = extraerClasificado(4);
	    	Jugadores2021 quinto = extraerClasificado(5);
	    	Jugadores2021 sexto = extraerClasificado(6);
	    	Jugadores2021 septimo = extraerClasificado(7);
	    	Jugadores2021 octavo = extraerClasificado(8);
	    	
	    	//Recuperar los semifinales
	    	List<CalendarioFaseCF2021> cuartosFinalCalendarioList = getResultListCalendarioFaseCF();
	    	//Buscar en la fase de semifinal la posicion de los clasificados del grupo y asignarlos 
	    	for (CalendarioFaseCF2021 calendarioFaseCF : cuartosFinalCalendarioList) {
				if(calendarioFaseCF.getPosicionProcedenciaJugador1() == 1){
					calendarioFaseCF.setJugador1Id(primero.getId());
					calendarioFaseCF.setJugador1(primero);
					calendarioFaseCF.setJugador2Id(cuarto.getId());
					calendarioFaseCF.setJugador2(cuarto);
					crearCampeonatoIndividualJuveniles2021.actualizarJugadorCuartosFinal(calendarioFaseCF, true, false);
				}else if(calendarioFaseCF.getPosicionProcedenciaJugador1() == 2){
					calendarioFaseCF.setJugador1Id(segundo.getId());
					calendarioFaseCF.setJugador1(segundo);
					calendarioFaseCF.setJugador2Id(tercero.getId());
					calendarioFaseCF.setJugador2(tercero);
					crearCampeonatoIndividualJuveniles2021.actualizarJugadorCuartosFinal(calendarioFaseCF, true, false);
				}else if(calendarioFaseCF.getPosicionProcedenciaJugador1() == 3){
					calendarioFaseCF.setJugador1Id(quinto.getId());
					calendarioFaseCF.setJugador1(quinto);
					calendarioFaseCF.setJugador2Id(sexto.getId());
					calendarioFaseCF.setJugador2(sexto);
					crearCampeonatoIndividualJuveniles2021.actualizarJugadorCuartosFinal(calendarioFaseCF, true, false);
				}else if(calendarioFaseCF.getPosicionProcedenciaJugador1() == 4){
					calendarioFaseCF.setJugador1Id(septimo.getId());
					calendarioFaseCF.setJugador1(septimo);
					calendarioFaseCF.setJugador2Id(octavo.getId());
					calendarioFaseCF.setJugador2(octavo);
					crearCampeonatoIndividualJuveniles2021.actualizarJugadorCuartosFinal(calendarioFaseCF, true, false);
				}
    		}
	    	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Clasificados a Cuartos de Final jugadores de la clasificacion.", null));
		}

	    public void doClasificarToSemifinal(){
	    	
	    	//Recuperar la clasificacion de las cuartos de final 
	    	List<CalendarioFaseCF2021> cuartosFinalCalendarioList = getResultListCalendarioFaseCF();
	    	//Recuperar los semifinales
	    	List<CalendarioFaseSF2021> semifinalesCalendarioList = getResultListCalendarioFaseSF();
	    	//Buscar en la fase de semifinal la posicion de los clasificados del grupo y asignarlos 
	    	for (CalendarioFaseSF2021 calendarioFaseSF : semifinalesCalendarioList) {
	    		for (CalendarioFaseCF2021 calendarioFaseCF : cuartosFinalCalendarioList) {
					if(calendarioFaseSF.getGrupoProcedenciaJugador1().equalsIgnoreCase(calendarioFaseCF.getIdCruce())){
						if(calendarioFaseCF.isGanaJugador() == 1){
							calendarioFaseSF.setJugador1Id(calendarioFaseCF.getJugador1Id());
							calendarioFaseSF.setJugador1(calendarioFaseCF.getJugador1());
							crearCampeonatoIndividualJuveniles2021.actualizarJugadorSemiFinal(calendarioFaseSF, true, false);
						}else if(calendarioFaseCF.isGanaJugador() == 2){
							calendarioFaseSF.setJugador1Id(calendarioFaseCF.getJugador2Id());
							calendarioFaseSF.setJugador1(calendarioFaseCF.getJugador2());
							crearCampeonatoIndividualJuveniles2021.actualizarJugadorSemiFinal(calendarioFaseSF, true, false);
						}
					}else  if(calendarioFaseSF.getGrupoProcedenciaJugador2().equalsIgnoreCase(calendarioFaseCF.getIdCruce())){
						if(calendarioFaseCF.isGanaJugador() == 1){
							calendarioFaseSF.setJugador2Id(calendarioFaseCF.getJugador1Id());
							calendarioFaseSF.setJugador2(calendarioFaseCF.getJugador1());
							crearCampeonatoIndividualJuveniles2021.actualizarJugadorSemiFinal(calendarioFaseSF, false, true);
						}else if(calendarioFaseCF.isGanaJugador() == 2){
							calendarioFaseSF.setJugador2Id(calendarioFaseCF.getJugador2Id());
							calendarioFaseSF.setJugador2(calendarioFaseCF.getJugador2());
							crearCampeonatoIndividualJuveniles2021.actualizarJugadorSemiFinal(calendarioFaseSF, false, true);
						}
					}
	    		}
			}
	    	
	    	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Clasificados a Semifinales jugadores de los Cuartos de final.", null));
		}
	    
	    public Jugadores2021 extraerClasificado(Integer posicion){
	    	return getResultListClasificacion().get(posicion-1).getJugador();
	    }
	    	
	    
	    public void doClasificarToFinal(){
	    	
	    	//Recuperar la clasificacion de las semifinales
	    	List<CalendarioFaseSF2021> semifinalCalendarioList = getResultListCalendarioFaseSF();
	    	//Recuperar los semifinales
	    	List<CalendarioFaseFF2021> finalCalendarioList = getResultListCalendarioFaseFF();
	    	//Buscar en la fase de semifinal la posicion de los clasificados del grupo y asignarlos 
	    	for (CalendarioFaseFF2021 calendarioFaseFF : finalCalendarioList) {
	    		for (CalendarioFaseSF2021 calendarioFaseSF : semifinalCalendarioList) {
					if(calendarioFaseFF.getGrupoProcedenciaJugador1().equalsIgnoreCase(calendarioFaseSF.getIdCruce())){
						if(calendarioFaseSF.isGanaJugador() == 1){
							calendarioFaseFF.setJugador1Id(calendarioFaseSF.getJugador1Id());
							calendarioFaseFF.setJugador1(calendarioFaseSF.getJugador1());
							crearCampeonatoIndividualJuveniles2021.actualizarJugadorFinal(calendarioFaseFF, true, false);
						}else if(calendarioFaseSF.isGanaJugador() == 2){
							calendarioFaseFF.setJugador1Id(calendarioFaseSF.getJugador2Id());
							calendarioFaseFF.setJugador1(calendarioFaseSF.getJugador2());
							crearCampeonatoIndividualJuveniles2021.actualizarJugadorFinal(calendarioFaseFF, true, false);
						}
					}else  if(calendarioFaseFF.getGrupoProcedenciaJugador2().equalsIgnoreCase(calendarioFaseSF.getIdCruce())){
						if(calendarioFaseSF.isGanaJugador() == 1){
							calendarioFaseFF.setJugador2Id(calendarioFaseSF.getJugador1Id());
							calendarioFaseFF.setJugador2(calendarioFaseSF.getJugador1());
							crearCampeonatoIndividualJuveniles2021.actualizarJugadorFinal(calendarioFaseFF, false, true);
						}else if(calendarioFaseSF.isGanaJugador() == 2){
							calendarioFaseFF.setJugador2Id(calendarioFaseSF.getJugador2Id());
							calendarioFaseFF.setJugador2(calendarioFaseSF.getJugador2());
							crearCampeonatoIndividualJuveniles2021.actualizarJugadorFinal(calendarioFaseFF, false, true);
						}
					}
	    		}
			}
	    	
	    	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Clasificados a Final jugadores de las Semifinales.", null));
	    }

}
