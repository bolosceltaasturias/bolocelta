package com.bolocelta.facade;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.inject.Named;

import com.bolocelta.application.utils.Downloader2021;
import com.bolocelta.bbdd.constants.Ubicaciones2021;
import com.bolocelta.bbdd.readTables.LeerDocumentos2021;
import com.bolocelta.entities.Documentos2021;

@Named
@ConversationScoped
@ManagedBean
public class DocumentosFacade2021 implements Serializable {
	
	@Inject
	private Downloader2021 downloader;
	
	private static final long serialVersionUID = 1L;
	
	private LeerDocumentos2021 leerDocumentos = new LeerDocumentos2021();
	
	private List<Documentos2021> resultList = null;

	public List<Documentos2021> getResultList() {
		if(resultList == null){
			resultList = (List<Documentos2021>) leerDocumentos.listResult();
		}
		return resultList;
	}

	public Object selected() {
		// TODO Auto-generated method stub
		return null;
	}

	public Object read(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	public String insert(Object insert) {
		// TODO Auto-generated method stub
		return null;
	}

	public String update(Object update) {
		// TODO Auto-generated method stub
		return null;
	}

	public String delete(Object delete) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Integer getMaxRowsPaginator(){
		return 10;
	}
	
	public boolean isEnabledPaginator(){
		return getResultList() != null ? (getResultList().size() > getMaxRowsPaginator()) : false;
	}
	
	public void doDownloadFile(Documentos2021 documento){
		File file = searchFile(documento.getNombre());
		try {
			downloader.downloadResource(documento.getNombre(), "application/pdf", file);
		} catch (FileNotFoundException e) {
			System.out.println("Error descargando fichero " + e.getMessage());
		}
	}
	
	public File searchFile(String nameFile) {
		File folder = new File(Ubicaciones2021.UBICACION_BBDD_DOCUMENTOS);

		File[] files = folder.listFiles();

		for (File file : files) {
			if (file.getName().equalsIgnoreCase(nameFile)) {
				return file;
			}
		}
		return null;
	}
	
	

}
