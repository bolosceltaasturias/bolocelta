package com.bolocelta.bbdd.constants;

public class TipoEnfrentamiento2022 {
	
	public static final String GRUPOS = "GRUPOS";
	public static final String DIRECTO = "DIRECTO";
	public static final String BOLADAS = "BOLADAS";

}
