package com.bolocelta.bbdd.constants.structure;

import java.util.ArrayList;
import java.util.List;

import com.bolocelta.application.enumerations.EstadosFasesEnumeration2022;
import com.bolocelta.application.enumerations.FasesEnumeration2022;
import com.bolocelta.bbdd.constants.Activo2022;
import com.bolocelta.bbdd.constants.NombresTablas2022;
import com.bolocelta.bbdd.constants.TipoEnfrentamiento2022;

public class EstructuraCampeonatoIndividualVeteranos2022 extends NombresTablas2022{
	
	private List<Estructura2022> listClasificacion = new ArrayList<Estructura2022>();
	
	
	//Filas
	private final Long F1 = new Long(0);
	private final Long F2 = new Long(1);
	private final Long F3 = new Long(2);
	private final Long F4 = new Long(3);
	private final Long F5 = new Long(4);
	private final Long F6 = new Long(5);
	private final Long F7 = new Long(6);
	private final Long F8 = new Long(7);
	private final Long F9 = new Long(8);
	private final Long F10 = new Long(9);
	private final Long F11 = new Long(10);
	private final Long F12 = new Long(11);
	private final Long F13 = new Long(12);
	
	//Columnas
	private final Integer C1 = 1;
	private final Integer C2 = 2;
	private final Integer C3 = 3;
	private final Integer C4 = 4;
	private final Integer C5 = 5;
	private final Integer C6 = 6;
	private final Integer C7 = 7;
	private final Integer C8 = 8;
	private final Integer C9 = 9;
	private final Integer C10 = 10;
	private final Integer C11 = 11;
	private final Integer C12 = 12;
	private final Integer C13 = 13;
	private final Integer C14 = 14;
	private final Integer C15 = 15;
	private final Integer C16 = 16;
	private final Integer C17 = 17;
	private final Integer C18 = 18;
	private final Integer C19 = 19;
	private final Integer C20 = 20;
	private final Integer C21 = 21;
	private final Integer C22 = 22;
	private final Integer C23 = 23;
	private final Integer C24 = 24;
	private final Integer C25 = 25;
	private final Integer C26 = 26;
	private final Integer C27 = 27;
	private final Integer C28 = 28;
	private final Integer C29 = 29;
	private final Integer C30 = 30;
	private final Integer C31 = 31;
	private final Integer C32 = 32;
	private final Integer C33 = 33;
	private final Integer C34 = 34;
	private final Integer C35 = 35;
	private final Integer C36 = 36;
	private final Integer C37 = 37;
	private final Integer C38 = 38;
	private final Integer C39 = 39;
	private final Integer C40 = 40;
	private final Integer C41 = 41;
	private final Integer C42 = 42;
	private final Integer C43 = 43;
	private final Integer C44 = 44;
	private final Integer C45 = 45;
	private final Integer C46 = 46;
	
	//Cabeceras Configuracion
	private final String COL_CONFIGURACION_ID = "ID";
	private final String COL_CONFIGURACION_NOMBRE = "NOMBRE";
	private final String COL_CONFIGURACION_VALOR = "VALOR";
	
	//Cabeceras Fases
	public final String COL_FASES_ID = "ID";
	public final String COL_FASES_NUMERO_FASE = "NUMERO FASE";
	public final String COL_FASES_NOMBRE = "NOMBRE";
	public final String COL_FASES_TIPO_ENFRENTAMIENTO = "TIPO ENFRENTAMIENTO";
	public final String COL_FASES_NUMERO_ENFRENTAMIENTOS_GRUPOS = "NUMERO ENFRENTAMIENTOS-GRUPOS";
	public final String COL_FASES_NUMERO_PARTIDAS = "NUMERO PARTIDAS";
	public final String COL_FASES_NUMERO_JUEGOS = "NUMERO JUEGOS";
	public final String COL_FASES_CLASIFICAN = "CLASIFICAN";
	public final String COL_FASES_FECHA = "FECHA";
	public final String COL_FASES_HORA = "HORA";
	public final String COL_FASES_ESTADO = "ESTADO";
	public final String COL_FASES_ACTIVO = "ACTIVO";
	public final String COL_FASES_FASE_SIGUIENTE = "FASE SIGUIENTE";
	
	//Cabeceras Participantes
	public static final String COL_PARTICIPANTES_ID = "ID";
	public static final String COL_PARTICIPANTES_ID_JUGADOR = "ID JUGADOR";
	public static final String COL_PARTICIPANTES_FECHA_INSCRIPCION = "FECHA INSCRIPCION";
	public static final String COL_PARTICIPANTES_USUARIO = "USUARIO";
	public static final String COL_PARTICIPANTES_ACTIVO = "ACTIVO";

	//Cabeceras Clasificacion
	public final String COL_CLASIFICACION_ID = "ID";
	public final String COL_CLASIFICACION_FECHA = "FECHA";
	public final String COL_CLASIFICACION_HORA = "HORA";
	public final String COL_CLASIFICACION_BOLERA = "BOLERA";
	public final String COL_CLASIFICACION_ID_JUGADOR = "ID_JUGADOR";
	public final String COL_CLASIFICACION_PUNTOS_TIRADA_1  = "T1";
	public final String COL_CLASIFICACION_PUNTOS_SACADA_1  = "S1";
	public final String COL_CLASIFICACION_PUNTOS_TIRADA_2  = "T2";
	public final String COL_CLASIFICACION_PUNTOS_SACADA_2  = "S2";
	public final String COL_CLASIFICACION_PUNTOS_TIRADA_3  = "T3";
	public final String COL_CLASIFICACION_PUNTOS_SACADA_3  = "S3";
	public final String COL_CLASIFICACION_PUNTOS_TIRADA_4  = "T4";
	public final String COL_CLASIFICACION_PUNTOS_SACADA_4  = "S4";
	public final String COL_CLASIFICACION_PUNTOS_TIRADA_5  = "T5";
	public final String COL_CLASIFICACION_PUNTOS_SACADA_5  = "S5";
	public final String COL_CLASIFICACION_PUNTOS_TIRADA_6  = "T6";
	public final String COL_CLASIFICACION_PUNTOS_SACADA_6  = "S6";
	public final String COL_CLASIFICACION_PUNTOS_TIRADA_7  = "T7";
	public final String COL_CLASIFICACION_PUNTOS_SACADA_7  = "S7";
	public final String COL_CLASIFICACION_PUNTOS_TIRADA_8  = "T8";
	public final String COL_CLASIFICACION_PUNTOS_SACADA_8  = "S8";
	public final String COL_CLASIFICACION_PUNTOS_TIRADA_9  = "T9";
	public final String COL_CLASIFICACION_PUNTOS_SACADA_9  = "S9";
	public final String COL_CLASIFICACION_PUNTOS_TIRADA_10 = "T10";
	public final String COL_CLASIFICACION_PUNTOS_SACADA_10 = "S10";
	public final String COL_CLASIFICACION_PUNTOS_TIRADA_11 = "T11";
	public final String COL_CLASIFICACION_PUNTOS_SACADA_11 = "S11";
	public final String COL_CLASIFICACION_PUNTOS_TIRADA_12 = "T12";
	public final String COL_CLASIFICACION_PUNTOS_SACADA_12 = "S12";
	public final String COL_CLASIFICACION_PUNTOS_TIRADA_13 = "T13";
	public final String COL_CLASIFICACION_PUNTOS_SACADA_13 = "S13";
	public final String COL_CLASIFICACION_PUNTOS_TIRADA_14 = "T14";
	public final String COL_CLASIFICACION_PUNTOS_SACADA_14 = "S14";
	public final String COL_CLASIFICACION_PUNTOS_TIRADA_15 = "T15";
	public final String COL_CLASIFICACION_PUNTOS_SACADA_15 = "S15";
	public final String COL_CLASIFICACION_PUNTOS_TIRADA_16 = "T16";
	public final String COL_CLASIFICACION_PUNTOS_SACADA_16 = "S16";
	public final String COL_CLASIFICACION_PUNTOS_TIRADA_17 = "T17";
	public final String COL_CLASIFICACION_PUNTOS_SACADA_17 = "S17";
	public final String COL_CLASIFICACION_PUNTOS_TIRADA_18 = "T18";
	public final String COL_CLASIFICACION_PUNTOS_SACADA_18 = "S18";
	public final String COL_CLASIFICACION_PUNTOS_TIRADA_19 = "T19";
	public final String COL_CLASIFICACION_PUNTOS_SACADA_19 = "S19";
	public final String COL_CLASIFICACION_PUNTOS_TIRADA_20 = "T20";
	public final String COL_CLASIFICACION_PUNTOS_SACADA_20 = "S20";
	public final String COL_CLASIFICACION_ACTIVO = "ACTIVO";
	
	//Propiedades
	private final String prop_fecha_maxima_inscripcion = "Fecha Maxima Inscripcion";
	private final String prop_estado = "Estado";
	private final String prop_bolera_final = "Bolera Final";
	private final String prop_observaciones_campeonato_1= "Observaciones Campeonato 1";
	private final String prop_observaciones_campeonato_2= "Observaciones Campeonato 2";
	private final String prop_observaciones_campeonato_3= "Observaciones Campeonato 3";
	private final String prop_observaciones_campeonato_4= "Observaciones Campeonato 4";
	private final String prop_observaciones_campeonato_5= "Observaciones Campeonato 5";
	private final String prop_observaciones_campeonato_6= "Observaciones Campeonato 6";
	private final String prop_observaciones_campeonato_7= "Observaciones Campeonato 7";
	private final String prop_boleras_ocupadas_fi= "Boleras Ocupadas FI";
	private final String prop_boleras_ocupadas_ofcf= "Boleras Ocupadas OFCF";
	
	public EstructuraCampeonatoIndividualVeteranos2022() {
	}
	
	public EstructuraCampeonatoIndividualVeteranos2022(String excelClasificacion) {
		dataClasificacion(excelClasificacion);
	}
	
	private void dataClasificacion(String excelClasificacion) {
		//Cabecera - Clasificacion
		listClasificacion.add(new Estructura2022(this.F1, this.C1, COL_CLASIFICACION_ID, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C2, COL_CLASIFICACION_FECHA, excelClasificacion, this.TD_TEXTO));
		listClasificacion.add(new Estructura2022(this.F1, this.C3, COL_CLASIFICACION_HORA, excelClasificacion, this.TD_TEXTO));
		listClasificacion.add(new Estructura2022(this.F1, this.C4, COL_CLASIFICACION_BOLERA, excelClasificacion, this.TD_TEXTO));
		listClasificacion.add(new Estructura2022(this.F1, this.C5, COL_CLASIFICACION_ID_JUGADOR, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C6, COL_CLASIFICACION_PUNTOS_TIRADA_1, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C7, COL_CLASIFICACION_PUNTOS_SACADA_1, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C8, COL_CLASIFICACION_PUNTOS_TIRADA_2, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C9, COL_CLASIFICACION_PUNTOS_SACADA_2, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C10, COL_CLASIFICACION_PUNTOS_TIRADA_3, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C11, COL_CLASIFICACION_PUNTOS_SACADA_3, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C12, COL_CLASIFICACION_PUNTOS_TIRADA_4, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C13, COL_CLASIFICACION_PUNTOS_SACADA_4, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C14, COL_CLASIFICACION_PUNTOS_TIRADA_5, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C15, COL_CLASIFICACION_PUNTOS_SACADA_5, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C16, COL_CLASIFICACION_PUNTOS_TIRADA_6, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C17, COL_CLASIFICACION_PUNTOS_SACADA_6, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C18, COL_CLASIFICACION_PUNTOS_TIRADA_7, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C19, COL_CLASIFICACION_PUNTOS_SACADA_7, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C20, COL_CLASIFICACION_PUNTOS_TIRADA_8, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C21, COL_CLASIFICACION_PUNTOS_SACADA_8, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C22, COL_CLASIFICACION_PUNTOS_TIRADA_9, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C23, COL_CLASIFICACION_PUNTOS_SACADA_9, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C24, COL_CLASIFICACION_PUNTOS_TIRADA_10, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C25, COL_CLASIFICACION_PUNTOS_SACADA_10, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C26, COL_CLASIFICACION_PUNTOS_TIRADA_11, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C27, COL_CLASIFICACION_PUNTOS_SACADA_11, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C28, COL_CLASIFICACION_PUNTOS_TIRADA_12, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C29, COL_CLASIFICACION_PUNTOS_SACADA_12, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C30, COL_CLASIFICACION_PUNTOS_TIRADA_13, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C31, COL_CLASIFICACION_PUNTOS_SACADA_13, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C32, COL_CLASIFICACION_PUNTOS_TIRADA_14, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C33, COL_CLASIFICACION_PUNTOS_SACADA_14, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C34, COL_CLASIFICACION_PUNTOS_TIRADA_15, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C35, COL_CLASIFICACION_PUNTOS_SACADA_15, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C36, COL_CLASIFICACION_PUNTOS_TIRADA_16, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C37, COL_CLASIFICACION_PUNTOS_SACADA_16, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C38, COL_CLASIFICACION_PUNTOS_TIRADA_17, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C39, COL_CLASIFICACION_PUNTOS_SACADA_17, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C40, COL_CLASIFICACION_PUNTOS_TIRADA_18, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C41, COL_CLASIFICACION_PUNTOS_SACADA_18, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C42, COL_CLASIFICACION_PUNTOS_TIRADA_19, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C43, COL_CLASIFICACION_PUNTOS_SACADA_19, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C44, COL_CLASIFICACION_PUNTOS_TIRADA_20, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C45, COL_CLASIFICACION_PUNTOS_SACADA_20, excelClasificacion, this.TD_ENTERO));
		listClasificacion.add(new Estructura2022(this.F1, this.C46, COL_CLASIFICACION_ACTIVO, excelClasificacion, this.TD_ENTERO));		
		
	}
	
	public List<Estructura2022> getClasificacionList(){
		return this.listClasificacion;
	}
	
	
	public String getInsertCabeceraClasificacionRow(){
		return (this.COL_CLASIFICACION_ID + ";" +
				this.COL_CLASIFICACION_FECHA + ";" +
				this.COL_CLASIFICACION_HORA + ";" +
				this.COL_CLASIFICACION_BOLERA + ";" +
				this.COL_CLASIFICACION_ID_JUGADOR + ";" +
				this.COL_CLASIFICACION_PUNTOS_TIRADA_1+ ";" +  
				this.COL_CLASIFICACION_PUNTOS_SACADA_1+ ";" +  
				this.COL_CLASIFICACION_PUNTOS_TIRADA_2+ ";" +  
				this.COL_CLASIFICACION_PUNTOS_SACADA_2+ ";" +  
				this.COL_CLASIFICACION_PUNTOS_TIRADA_3+ ";" +  
				this.COL_CLASIFICACION_PUNTOS_SACADA_3+ ";" +  
				this.COL_CLASIFICACION_PUNTOS_TIRADA_4+ ";" +  
				this.COL_CLASIFICACION_PUNTOS_SACADA_4+ ";" +  
				this.COL_CLASIFICACION_PUNTOS_TIRADA_5+ ";" +  
				this.COL_CLASIFICACION_PUNTOS_SACADA_5+ ";" +  
				this.COL_CLASIFICACION_PUNTOS_TIRADA_6+ ";" +  
				this.COL_CLASIFICACION_PUNTOS_SACADA_6+ ";" +  
				this.COL_CLASIFICACION_PUNTOS_TIRADA_7+ ";" +  
				this.COL_CLASIFICACION_PUNTOS_SACADA_7+ ";" +  
				this.COL_CLASIFICACION_PUNTOS_TIRADA_8+ ";" +  
				this.COL_CLASIFICACION_PUNTOS_SACADA_8+ ";" +  
				this.COL_CLASIFICACION_PUNTOS_TIRADA_9+ ";" +  
				this.COL_CLASIFICACION_PUNTOS_SACADA_9+ ";" +  
				this.COL_CLASIFICACION_PUNTOS_TIRADA_10+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_SACADA_10+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_TIRADA_11+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_SACADA_11+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_TIRADA_12+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_SACADA_12+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_TIRADA_13+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_SACADA_13+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_TIRADA_14+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_SACADA_14+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_TIRADA_15+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_SACADA_15+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_TIRADA_16+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_SACADA_16+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_TIRADA_17+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_SACADA_17+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_TIRADA_18+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_SACADA_18+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_TIRADA_19+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_SACADA_19+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_TIRADA_20+ ";" + 
				this.COL_CLASIFICACION_PUNTOS_SACADA_20+ ";" + 
				this.COL_CLASIFICACION_ACTIVO
				);
	}
	
	
	//Cabeceras Fases SF - Calendario
	public final String COL_CALENDARIO_FASE_SF_ID = "ID";
	public final String COL_CALENDARIO_FASE_SF_CATEGORIA = "CATEGORIA";
	public final String COL_CALENDARIO_FASE_SF_IDCRUCE = "ID CRUCE";
	public final String COL_CALENDARIO_FASE_SF_NUM_ENFRENTAMIENTO = "NUM_ENFRENTAMIENTO";
	public final String COL_CALENDARIO_FASE_SF_JUGADOR_1 = "JUGADOR 1";
	public final String COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_1_P1 = "JUG. JUGADOR 1 P1";
	public final String COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_1_P2 = "JUG. JUGADOR 1 P2";
	public final String COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_1_P3 = "JUG. JUGADOR 1 P3";
	public final String COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_2_P1 = "JUG. JUGADOR 2 P1";
	public final String COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_2_P2 = "JUG. JUGADOR 2 P2";
	public final String COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_2_P3 = "JUG. JUGADOR 2 P3";
	public final String COL_CALENDARIO_FASE_SF_JUGADOR_2 = "JUGADOR 2";
	public final String COL_CALENDARIO_FASE_SF_ORDEN = "ORDEN";
	public final String COL_CALENDARIO_FASE_SF_CRUCE_CF = "CRUCE_CF";
	public final String COL_CALENDARIO_FASE_SF_ACTIVO = "ACTIVO";
	public final String COL_CALENDARIO_FASE_SF_INICIA = "INICIA";
	public final String COL_CALENDARIO_FASE_SF_FASE_ANTERIOR = "FASE ANTERIOR";
	public final String COL_CALENDARIO_FASE_SF_GRUPO_PROCEDENCIA_JUGADOR_1 = "GR PROC JUG 1";
	public final String COL_CALENDARIO_FASE_SF_GRUPO_PROCEDENCIA_JUGADOR_2 = "GR PROC JUG 2";
	public final String COL_CALENDARIO_FASE_SF_POSICION_PROCEDENCIA_JUGADOR_1 = "POS PROC JUG 1";
	public final String COL_CALENDARIO_FASE_SF_POSICION_PROCEDENCIA_JUGADOR_2 = "POS PROC JUG 2";
	public final String COL_CALENDARIO_FASE_SF_BOLERA = "BOLERA";
	
	
	public String getInsertCabeceraCalendarioFaseSFRow(){
		return (this.COL_CALENDARIO_FASE_SF_ID + ";" +
				this.COL_CALENDARIO_FASE_SF_CATEGORIA + ";" +
				this.COL_CALENDARIO_FASE_SF_IDCRUCE + ";" +
				this.COL_CALENDARIO_FASE_SF_NUM_ENFRENTAMIENTO + ";" +
				this.COL_CALENDARIO_FASE_SF_JUGADOR_1 + ";" +
				this.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_1_P1 + ";" +
				this.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_1_P2 + ";" +
				this.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_1_P3 + ";" +
				this.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_2_P1 + ";" +
				this.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_2_P2 + ";" +
				this.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_2_P3 + ";" +
				this.COL_CALENDARIO_FASE_SF_JUGADOR_2 + ";" +
				this.COL_CALENDARIO_FASE_SF_ORDEN + ";" +
				this.COL_CALENDARIO_FASE_SF_CRUCE_CF + ";" +
				this.COL_CALENDARIO_FASE_SF_ACTIVO + ";" +
				this.COL_CALENDARIO_FASE_SF_INICIA + ";" +
				this.COL_CALENDARIO_FASE_SF_FASE_ANTERIOR + ";" +
				this.COL_CALENDARIO_FASE_SF_GRUPO_PROCEDENCIA_JUGADOR_1 + ";" +
				this.COL_CALENDARIO_FASE_SF_GRUPO_PROCEDENCIA_JUGADOR_2 + ";" +
				this.COL_CALENDARIO_FASE_SF_POSICION_PROCEDENCIA_JUGADOR_1 + ";" +
				this.COL_CALENDARIO_FASE_SF_POSICION_PROCEDENCIA_JUGADOR_2 + ";" +
				this.COL_CALENDARIO_FASE_SF_BOLERA
				);
	}
	
	//Cabeceras Fases FF - Calendario
	public final String COL_CALENDARIO_FASE_FF_ID = "ID";
	public final String COL_CALENDARIO_FASE_FF_CATEGORIA = "CATEGORIA";
	public final String COL_CALENDARIO_FASE_FF_IDCRUCE = "ID CRUCE";
	public final String COL_CALENDARIO_FASE_FF_NUM_ENFRENTAMIENTO = "NUM_ENFRENTAMIENTO";
	public final String COL_CALENDARIO_FASE_FF_JUGADOR_1 = "JUGADOR 1";
	public final String COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_1_P1 = "JUG. JUGADOR 1 P1";
	public final String COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_1_P2 = "JUG. JUGADOR 1 P2";
	public final String COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_1_P3 = "JUG. JUGADOR 1 P3";
	public final String COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_2_P1 = "JUG. JUGADOR 2 P1";
	public final String COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_2_P2 = "JUG. JUGADOR 2 P2";
	public final String COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_2_P3 = "JUG. JUGADOR 2 P3";
	public final String COL_CALENDARIO_FASE_FF_JUGADOR_2 = "JUGADOR 2";
	public final String COL_CALENDARIO_FASE_FF_ORDEN = "ORDEN";
	public final String COL_CALENDARIO_FASE_FF_CRUCE_CF = "CRUCE_CF";
	public final String COL_CALENDARIO_FASE_FF_ACTIVO = "ACTIVO";
	public final String COL_CALENDARIO_FASE_FF_INICIA = "INICIA";
	public final String COL_CALENDARIO_FASE_FF_FASE_ANTERIOR = "FASE ANTERIOR";
	public final String COL_CALENDARIO_FASE_FF_GRUPO_PROCEDENCIA_JUGADOR_1 = "GR PROC JUG 1";
	public final String COL_CALENDARIO_FASE_FF_GRUPO_PROCEDENCIA_JUGADOR_2 = "GR PROC JUG 2";
	public final String COL_CALENDARIO_FASE_FF_POSICION_PROCEDENCIA_JUGADOR_1 = "POS PROC JUG 1";
	public final String COL_CALENDARIO_FASE_FF_POSICION_PROCEDENCIA_JUGADOR_2 = "POS PROC JUG 2";
	public final String COL_CALENDARIO_FASE_FF_BOLERA = "BOLERA";
	
	public String getInsertCabeceraCalendarioFaseFFRow(){
		return (this.COL_CALENDARIO_FASE_FF_ID + ";" +
				this.COL_CALENDARIO_FASE_FF_CATEGORIA + ";" +
				this.COL_CALENDARIO_FASE_FF_IDCRUCE + ";" +
				this.COL_CALENDARIO_FASE_FF_NUM_ENFRENTAMIENTO + ";" +
				this.COL_CALENDARIO_FASE_FF_JUGADOR_1 + ";" +
				this.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_1_P1 + ";" +
				this.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_1_P2 + ";" +
				this.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_1_P3 + ";" +
				this.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_2_P1 + ";" +
				this.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_2_P2 + ";" +
				this.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_2_P3 + ";" +
				this.COL_CALENDARIO_FASE_FF_JUGADOR_2 + ";" +
				this.COL_CALENDARIO_FASE_FF_ORDEN + ";" +
				this.COL_CALENDARIO_FASE_FF_CRUCE_CF + ";" +
				this.COL_CALENDARIO_FASE_FF_ACTIVO + ";" +
				this.COL_CALENDARIO_FASE_FF_INICIA + ";" +
				this.COL_CALENDARIO_FASE_FF_FASE_ANTERIOR + ";" +
				this.COL_CALENDARIO_FASE_FF_GRUPO_PROCEDENCIA_JUGADOR_1 + ";" +
				this.COL_CALENDARIO_FASE_FF_GRUPO_PROCEDENCIA_JUGADOR_2 + ";" +
				this.COL_CALENDARIO_FASE_FF_POSICION_PROCEDENCIA_JUGADOR_1 + ";" +
				this.COL_CALENDARIO_FASE_FF_POSICION_PROCEDENCIA_JUGADOR_2 + ";" +
				this.COL_CALENDARIO_FASE_FF_BOLERA
				);
	}
	
	public List<Estructura2022> getEstructuraCalendarioSemifinal(){
		List<Estructura2022> list = new ArrayList<>();
		
		list.add(new Estructura2022(this.F1, this.C1 , COL_CALENDARIO_FASE_SF_ID, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C2 , COL_CALENDARIO_FASE_SF_CATEGORIA, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C3 , COL_CALENDARIO_FASE_SF_IDCRUCE, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C4 , COL_CALENDARIO_FASE_SF_NUM_ENFRENTAMIENTO, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C5 , COL_CALENDARIO_FASE_SF_JUGADOR_1, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C6 , COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_1_P1, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C7 , COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_1_P2, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C8 , COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_1_P3, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C9 , COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_2_P1, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C10 , COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_2_P2, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C11 , COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_2_P3, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C12 , COL_CALENDARIO_FASE_SF_JUGADOR_2, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C13 , COL_CALENDARIO_FASE_SF_ORDEN, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C14 , COL_CALENDARIO_FASE_SF_CRUCE_CF, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C15 , COL_CALENDARIO_FASE_SF_ACTIVO, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C16 , COL_CALENDARIO_FASE_SF_INICIA, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C17 , COL_CALENDARIO_FASE_SF_FASE_ANTERIOR, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C18 , COL_CALENDARIO_FASE_SF_GRUPO_PROCEDENCIA_JUGADOR_1, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C19 , COL_CALENDARIO_FASE_SF_GRUPO_PROCEDENCIA_JUGADOR_2, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C20 , COL_CALENDARIO_FASE_SF_POSICION_PROCEDENCIA_JUGADOR_1, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C21 , COL_CALENDARIO_FASE_SF_POSICION_PROCEDENCIA_JUGADOR_2, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_SF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C22 , COL_CALENDARIO_FASE_SF_BOLERA, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_SF, this.TD_TEXTO));
		
		return list; 
	}
	
	
	public List<Estructura2022> getEstructuraCalendarioFinal(){
		List<Estructura2022> list = new ArrayList<>();
		
		list.add(new Estructura2022(this.F1, this.C1 , COL_CALENDARIO_FASE_FF_ID, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C2 , COL_CALENDARIO_FASE_FF_CATEGORIA, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C3 , COL_CALENDARIO_FASE_FF_IDCRUCE, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C4 , COL_CALENDARIO_FASE_FF_NUM_ENFRENTAMIENTO, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C5 , COL_CALENDARIO_FASE_FF_JUGADOR_1, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C6 , COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_1_P1, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C7 , COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_1_P2, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C8 , COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_1_P3, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C9 , COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_2_P1, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C10 , COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_2_P2, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C11 , COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_2_P3, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C12 , COL_CALENDARIO_FASE_FF_JUGADOR_2, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C13 , COL_CALENDARIO_FASE_FF_ORDEN, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C14 , COL_CALENDARIO_FASE_FF_CRUCE_CF, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C15 , COL_CALENDARIO_FASE_FF_ACTIVO, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C16 , COL_CALENDARIO_FASE_FF_INICIA, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C17 , COL_CALENDARIO_FASE_FF_FASE_ANTERIOR, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C18 , COL_CALENDARIO_FASE_FF_GRUPO_PROCEDENCIA_JUGADOR_1, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C19 , COL_CALENDARIO_FASE_FF_GRUPO_PROCEDENCIA_JUGADOR_2, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C20 , COL_CALENDARIO_FASE_FF_POSICION_PROCEDENCIA_JUGADOR_1, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C21 , COL_CALENDARIO_FASE_FF_POSICION_PROCEDENCIA_JUGADOR_2, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_FF, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C22 , COL_CALENDARIO_FASE_FF_BOLERA, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_FF, this.TD_TEXTO));		
		return list; 
	}
	
	public List<String> getInsertCabeceraConfiguracionRow(){
		List<String> dataConfiguracion = new ArrayList<>();
		String cabecera = this.COL_CONFIGURACION_ID + ";" + this.COL_CONFIGURACION_NOMBRE + ";" + this.COL_CONFIGURACION_VALOR;
		dataConfiguracion.add(cabecera);
		String fechamaxima = this.C1 + ";" + this.prop_fecha_maxima_inscripcion + ";" + " ";
		dataConfiguracion.add(fechamaxima);
		String estado = this.C2 + ";" + this.prop_estado + ";" + " ";
		dataConfiguracion.add(estado);
		String boleraFinal = this.C3 + ";" + this.prop_bolera_final + ";" + " ";
		dataConfiguracion.add(boleraFinal);
		String observacionesCampeonato1 = this.C4 + ";" + this.prop_observaciones_campeonato_1 + ";" + " ";
		dataConfiguracion.add(observacionesCampeonato1);
		String observacionesCampeonato2 = this.C5 + ";" + this.prop_observaciones_campeonato_2 + ";" + " ";
		dataConfiguracion.add(observacionesCampeonato2);
		String observacionesCampeonato3 = this.C6 + ";" + this.prop_observaciones_campeonato_3 + ";" + " ";
		dataConfiguracion.add(observacionesCampeonato3);
		String observacionesCampeonato4 = this.C7 + ";" + this.prop_observaciones_campeonato_4 + ";" + " ";
		dataConfiguracion.add(observacionesCampeonato4);
		String observacionesCampeonato5 = this.C8 + ";" + this.prop_observaciones_campeonato_5 + ";" + " ";
		dataConfiguracion.add(observacionesCampeonato5);
		String observacionesCampeonato6 = this.C9 + ";" + this.prop_observaciones_campeonato_6 + ";" + " ";
		dataConfiguracion.add(observacionesCampeonato6);
		String observacionesCampeonato7 = this.C10 + ";" + this.prop_observaciones_campeonato_7 + ";" + " ";
		dataConfiguracion.add(observacionesCampeonato7);
		String bolerasOcupadasfi = this.C11 + ";" + this.prop_boleras_ocupadas_fi + ";" + " ";
		dataConfiguracion.add(bolerasOcupadasfi);
		String bolerasOcupadasofcf = this.C12 + ";" + this.prop_boleras_ocupadas_ofcf + ";" + " ";
		dataConfiguracion.add(bolerasOcupadasofcf);
		return dataConfiguracion;
	}
	
	public String getInsertCabeceraFasesRow(){
		return (this.COL_FASES_ID + ";" +
				this.COL_FASES_NUMERO_FASE + ";" +
				this.COL_FASES_NOMBRE + ";" +
				this.COL_FASES_TIPO_ENFRENTAMIENTO + ";" +
				this.COL_FASES_NUMERO_ENFRENTAMIENTOS_GRUPOS + ";" +
				this.COL_FASES_NUMERO_PARTIDAS + ";" +
				this.COL_FASES_NUMERO_JUEGOS + ";" +
				this.COL_FASES_CLASIFICAN + ";" +
				this.COL_FASES_FECHA + ";" +
				this.COL_FASES_HORA + ";" +
				this.COL_FASES_ESTADO + ";" +
				this.COL_FASES_ACTIVO + ";" +
				this.COL_FASES_FASE_SIGUIENTE
				);
	}
	
	public String getInsertCabeceraParticipantesRow(){
		return (this.COL_PARTICIPANTES_ID + ";" +
				this.COL_PARTICIPANTES_ID_JUGADOR + ";" +
				this.COL_PARTICIPANTES_FECHA_INSCRIPCION + ";" +
				this.COL_PARTICIPANTES_USUARIO + ";" +
				this.COL_PARTICIPANTES_ACTIVO 
				);
	}
	
	public Estructura2022 getEstructuraFechaMaximaInscripcion(){
		return new Estructura2022(this.F2, this.C3 , null, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CONFIG, this.TD_FECHA); 
	}
	
	public Estructura2022 getEstructuraEstadoCampeonato(){
		return new Estructura2022(this.F3, this.C3 , null, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CONFIG, this.TD_TEXTO); 
	}
	
	public Estructura2022 getEstructuraBoleraFinal(){
		return new Estructura2022(this.F4, this.C3 , null, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CONFIG, this.TD_TEXTO); 
	}

	public List<String> getEstructuraTodasFases(){
		//Crear fases
		List<String> listFases = new ArrayList<>();
		String fase = "";
		//Fase I
		fase = agregarFase(0, 0, FasesEnumeration2022.FASE_CLA, TipoEnfrentamiento2022.BOLADAS, 0, 0, 0, 0, "", "", 1, EstadosFasesEnumeration2022.PENDIENTE, Activo2022.NO);
		listFases.add(fase);
		//Fase I
		fase = agregarFase(1, 1, FasesEnumeration2022.FASE_I, TipoEnfrentamiento2022.GRUPOS, 0, 0, 0, 0, "", "", 2, EstadosFasesEnumeration2022.PENDIENTE, Activo2022.NO);
		listFases.add(fase);
		//Fase II
		fase = agregarFase(2, 2, FasesEnumeration2022.FASE_II, TipoEnfrentamiento2022.GRUPOS, 0, 0, 0, 0, "", "",  3, EstadosFasesEnumeration2022.PENDIENTE, Activo2022.NO);
		listFases.add(fase);
		//Fase Octavos de final
		fase = agregarFase(3, 3, FasesEnumeration2022.FASE_OF, TipoEnfrentamiento2022.DIRECTO, 0, 0, 0, 0, "", "",  4, EstadosFasesEnumeration2022.PENDIENTE, Activo2022.NO);
		listFases.add(fase);
		//Fase Cuartos de final
		fase = agregarFase(4, 4, FasesEnumeration2022.FASE_CF, TipoEnfrentamiento2022.DIRECTO, 0, 0, 0, 0, "", "",  5, EstadosFasesEnumeration2022.PENDIENTE, Activo2022.NO);
		listFases.add(fase);
		//Fase Semifinales
		fase = agregarFase(5, 5, FasesEnumeration2022.FASE_SF, TipoEnfrentamiento2022.DIRECTO, 0, 0, 0, 0, "", "",  6, EstadosFasesEnumeration2022.PENDIENTE, Activo2022.NO);
		listFases.add(fase);
		//Fase Final consolacion
		fase = agregarFase(6, 6, FasesEnumeration2022.FASE_FC, TipoEnfrentamiento2022.DIRECTO, 0, 0, 0, 0, "", "",  7, EstadosFasesEnumeration2022.PENDIENTE, Activo2022.NO);
		listFases.add(fase);
		//Fase Final
		fase = agregarFase(7, 7, FasesEnumeration2022.FASE_FF, TipoEnfrentamiento2022.DIRECTO, 0, 0, 0, 0, "", "",  8, EstadosFasesEnumeration2022.PENDIENTE, Activo2022.NO);
		listFases.add(fase);
		
		return listFases; 
	}
	
	private String agregarFase(Integer id, Integer numeroFase, String fase, String tipoEnfrentamiento, 
			Integer numeroEnfrentamientos, Integer numeroPartidas, Integer numeroJuegos, Integer clasifican
			, String fecha, String hora, Integer faseSiguiente, String estado, String activo) {
		return id + ";" +numeroFase + ";" + fase + ";" + tipoEnfrentamiento + ";" + numeroEnfrentamientos + ";" + 
			numeroPartidas + ";" + numeroJuegos + ";" + clasifican + ";" + fecha + ";" + hora + ";" + estado + ";" + activo + ";" + faseSiguiente  + ";" ;
	}
	
	public List<Estructura2022> getEstructuraInscribirParticipantes(){
		List<Estructura2022> list = new ArrayList<>();
		list.add(new Estructura2022(this.F1, this.C1 , COL_PARTICIPANTES_ID, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_PARTICIPANTES, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C2 , COL_PARTICIPANTES_ID_JUGADOR, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_PARTICIPANTES, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C3 , COL_PARTICIPANTES_FECHA_INSCRIPCION, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_PARTICIPANTES, this.TD_FECHA));
		list.add(new Estructura2022(this.F1, this.C4 , COL_PARTICIPANTES_USUARIO, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_PARTICIPANTES, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C5 , COL_PARTICIPANTES_ACTIVO, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_PARTICIPANTES, this.TD_TEXTO));
		return list; 
	}
	
	public List<Estructura2022> getEstructuraFases(){
		List<Estructura2022> list = new ArrayList<>();
		
		list.add(new Estructura2022(this.F1, this.C1 , COL_FASES_ID, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_FASES, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C2 , COL_FASES_NUMERO_FASE, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_FASES, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C3 , COL_FASES_NOMBRE, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_FASES, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C4 , COL_FASES_TIPO_ENFRENTAMIENTO, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_FASES, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C5 , COL_FASES_NUMERO_ENFRENTAMIENTOS_GRUPOS, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_FASES, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C6 , COL_FASES_NUMERO_PARTIDAS, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_FASES, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C7 , COL_FASES_NUMERO_JUEGOS, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_FASES, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C8 , COL_FASES_CLASIFICAN, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_FASES, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C9 , COL_FASES_FECHA, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_FASES, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C10 , COL_FASES_HORA, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_FASES, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C11 , COL_FASES_ESTADO, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_FASES, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C12 , COL_FASES_ACTIVO, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_FASES, this.TD_TEXTO));
		list.add(new Estructura2022(this.F1, this.C13 , COL_FASES_FASE_SIGUIENTE, this.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_FASES, this.TD_TEXTO));
		
		return list; 
	}
	
	//Cabeceras Fases I - Clasificacion
	public final String COL_CLASIFICACION_FASE_I_ID = "ID";
	public final String COL_CLASIFICACION_FASE_I_CATEGORIA = "CATEGORIA";
	public final String COL_CLASIFICACION_FASE_I_GRUPO = "GRUPO";
	public final String COL_CLASIFICACION_FASE_I_JUGADOR = "JUGADOR";
	public final String COL_CLASIFICACION_FASE_I_PJ = "PJ";
	public final String COL_CLASIFICACION_FASE_I_PG = "PG";
	public final String COL_CLASIFICACION_FASE_I_PE = "PE";
	public final String COL_CLASIFICACION_FASE_I_PP = "PP";
	public final String COL_CLASIFICACION_FASE_I_PF = "PF";
	public final String COL_CLASIFICACION_FASE_I_PC = "PC";
	public final String COL_CLASIFICACION_FASE_I_PT = "PT";
	
	public String getInsertCabeceraClasificacionFaseIRow(){
		return (this.COL_CLASIFICACION_FASE_I_ID + ";" +
				this.COL_CLASIFICACION_FASE_I_CATEGORIA + ";" +
				this.COL_CLASIFICACION_FASE_I_GRUPO + ";" +
				this.COL_CLASIFICACION_FASE_I_JUGADOR + ";" +
				this.COL_CLASIFICACION_FASE_I_PJ + ";" +
				this.COL_CLASIFICACION_FASE_I_PG + ";" +
				this.COL_CLASIFICACION_FASE_I_PE + ";" +
				this.COL_CLASIFICACION_FASE_I_PP + ";" +
				this.COL_CLASIFICACION_FASE_I_PF + ";" +
				this.COL_CLASIFICACION_FASE_I_PC + ";" +
				this.COL_CLASIFICACION_FASE_I_PT
				);
	}
	
	//Cabeceras Fases I - Calendario
	public final String COL_CALENDARIO_FASE_I_ID = "ID";
	public final String COL_CALENDARIO_FASE_I_CATEGORIA = "CATEGORIA";
	public final String COL_CALENDARIO_FASE_I_GRUPO = "GRUPO";
	public final String COL_CALENDARIO_FASE_I_JUGADOR_1 = "JUGADOR 1";
	public final String COL_CALENDARIO_FASE_I_JUEGOS_JUGADOR_1 = "JUG. JUGADOR 1";
	public final String COL_CALENDARIO_FASE_I_JUEGOS_JUGADOR_2 = "JUG. JUGADOR 2";
	public final String COL_CALENDARIO_FASE_I_JUGADOR_2 = "JUGADOR 2";
	public final String COL_CALENDARIO_FASE_I_ORDEN = "ORDEN";
	public final String COL_CALENDARIO_FASE_I_ACTIVO = "ACTIVO";
	public final String COL_CALENDARIO_FASE_I_INICIA = "INICIA";
	public final String COL_CALENDARIO_FASE_I_BOLERA = "BOLERA";
	
	public String getInsertCabeceraCalendarioFaseIRow(){
		return (this.COL_CALENDARIO_FASE_I_ID + ";" +
				this.COL_CALENDARIO_FASE_I_CATEGORIA + ";" +
				this.COL_CALENDARIO_FASE_I_GRUPO + ";" +
				this.COL_CALENDARIO_FASE_I_JUGADOR_1 + ";" +
				this.COL_CALENDARIO_FASE_I_JUEGOS_JUGADOR_1 + ";" +
				this.COL_CALENDARIO_FASE_I_JUEGOS_JUGADOR_2 + ";" +
				this.COL_CALENDARIO_FASE_I_JUGADOR_2 + ";" +
				this.COL_CALENDARIO_FASE_I_ORDEN + ";" +
				this.COL_CALENDARIO_FASE_I_ACTIVO + ";" +
				this.COL_CALENDARIO_FASE_I_INICIA + ";" +
				this.COL_CALENDARIO_FASE_I_BOLERA
				);
	}
	
	//Cabeceras Fases II - Clasificacion
	public final String COL_CLASIFICACION_FASE_II_ID = "ID";
	public final String COL_CLASIFICACION_FASE_II_CATEGORIA = "CATEGORIA";
	public final String COL_CLASIFICACION_FASE_II_GRUPO = "GRUPO";
	public final String COL_CLASIFICACION_FASE_II_JUGADOR = "JUGADOR";
	public final String COL_CLASIFICACION_FASE_II_PJ = "PJ";
	public final String COL_CLASIFICACION_FASE_II_PG = "PG";
	public final String COL_CLASIFICACION_FASE_II_PE = "PE";
	public final String COL_CLASIFICACION_FASE_II_PP = "PP";
	public final String COL_CLASIFICACION_FASE_II_PF = "PF";
	public final String COL_CLASIFICACION_FASE_II_PC = "PC";
	public final String COL_CLASIFICACION_FASE_II_PT = "PT";
	
	public String getInsertCabeceraClasificacionFaseIIRow(){
		return (this.COL_CLASIFICACION_FASE_II_ID + ";" +
				this.COL_CLASIFICACION_FASE_II_CATEGORIA + ";" +
				this.COL_CLASIFICACION_FASE_II_GRUPO + ";" +
				this.COL_CLASIFICACION_FASE_II_JUGADOR + ";" +
				this.COL_CLASIFICACION_FASE_II_PJ + ";" +
				this.COL_CLASIFICACION_FASE_II_PG + ";" +
				this.COL_CLASIFICACION_FASE_II_PE + ";" +
				this.COL_CLASIFICACION_FASE_II_PP + ";" +
				this.COL_CLASIFICACION_FASE_II_PF + ";" +
				this.COL_CLASIFICACION_FASE_II_PC + ";" +
				this.COL_CLASIFICACION_FASE_II_PT
				);
	}
	
	//Cabeceras Fases II - Calendario
	public final String COL_CALENDARIO_FASE_II_ID = "ID";
	public final String COL_CALENDARIO_FASE_II_CATEGORIA = "CATEGORIA";
	public final String COL_CALENDARIO_FASE_II_GRUPO = "GRUPO";
	public final String COL_CALENDARIO_FASE_II_JUGADOR_1 = "JUGADOR 1";
	public final String COL_CALENDARIO_FASE_II_JUEGOS_JUGADOR_1 = "JUG. JUGADOR 1";
	public final String COL_CALENDARIO_FASE_II_JUEGOS_JUGADOR_2 = "JUG. JUGADOR 2";
	public final String COL_CALENDARIO_FASE_II_JUGADOR_2 = "JUGADOR 2";
	public final String COL_CALENDARIO_FASE_II_ORDEN = "ORDEN";
	public final String COL_CALENDARIO_FASE_II_ACTIVO = "ACTIVO";
	public final String COL_CALENDARIO_FASE_II_INICIA = "INICIA";
	public final String COL_CALENDARIO_FASE_II_BOLERA = "BOLERA";
	
	public String getInsertCabeceraCalendarioFaseIIRow(){
		return (this.COL_CALENDARIO_FASE_II_ID + ";" +
				this.COL_CALENDARIO_FASE_II_CATEGORIA + ";" +
				this.COL_CALENDARIO_FASE_II_GRUPO + ";" +
				this.COL_CALENDARIO_FASE_II_JUGADOR_1 + ";" +
				this.COL_CALENDARIO_FASE_II_JUEGOS_JUGADOR_1 + ";" +
				this.COL_CALENDARIO_FASE_II_JUEGOS_JUGADOR_1 + ";" +
				this.COL_CALENDARIO_FASE_II_JUEGOS_JUGADOR_2 + ";" +
				this.COL_CALENDARIO_FASE_II_JUGADOR_2 + ";" +
				this.COL_CALENDARIO_FASE_II_ORDEN + ";" +
				this.COL_CALENDARIO_FASE_II_ACTIVO + ";" +
				this.COL_CALENDARIO_FASE_II_INICIA + ";" +
				this.COL_CALENDARIO_FASE_II_BOLERA 
				);
	}
	
	//Cabeceras Fases OF - Calendario
	public final String COL_CALENDARIO_FASE_OF_ID = "ID";
	public final String COL_CALENDARIO_FASE_OF_CATEGORIA = "CATEGORIA";
	public final String COL_CALENDARIO_FASE_OF_IDCRUCE = "ID CRUCE";
	public final String COL_CALENDARIO_FASE_OF_NUM_ENFRENTAMIENTO = "NUM_ENFRENTAMIENTO";
	public final String COL_CALENDARIO_FASE_OF_JUGADOR_1 = "JUGADOR 1";
	public final String COL_CALENDARIO_FASE_OF_JUEGOS_JUGADOR_1_P1 = "JUG. JUGADOR 1 P1";
	public final String COL_CALENDARIO_FASE_OF_JUEGOS_JUGADOR_1_P2 = "JUG. JUGADOR 1 P2";
	public final String COL_CALENDARIO_FASE_OF_JUEGOS_JUGADOR_1_P3 = "JUG. JUGADOR 1 P3";
	public final String COL_CALENDARIO_FASE_OF_JUEGOS_JUGADOR_2_P1 = "JUG. JUGADOR 2 P1";
	public final String COL_CALENDARIO_FASE_OF_JUEGOS_JUGADOR_2_P2 = "JUG. JUGADOR 2 P2";
	public final String COL_CALENDARIO_FASE_OF_JUEGOS_JUGADOR_2_P3 = "JUG. JUGADOR 2 P3";
	public final String COL_CALENDARIO_FASE_OF_JUGADOR_2 = "JUGADOR 2";
	public final String COL_CALENDARIO_FASE_OF_ORDEN = "ORDEN";
	public final String COL_CALENDARIO_FASE_OF_CRUCE_CF = "CRUCE_CF";
	public final String COL_CALENDARIO_FASE_OF_ACTIVO = "ACTIVO";
	public final String COL_CALENDARIO_FASE_OF_INICIA = "INICIA";
	public final String COL_CALENDARIO_FASE_OF_FASE_ANTERIOR = "FASE ANTERIOR";
	public final String COL_CALENDARIO_FASE_OF_GRUPO_PROCEDENCIA_JUGADOR_1 = "GR PROC JUG 1";
	public final String COL_CALENDARIO_FASE_OF_GRUPO_PROCEDENCIA_JUGADOR_2 = "GR PROC JUG 2";
	public final String COL_CALENDARIO_FASE_OF_POSICION_PROCEDENCIA_JUGADOR_1 = "POS PROC JUG 1";
	public final String COL_CALENDARIO_FASE_OF_POSICION_PROCEDENCIA_JUGADOR_2 = "POS PROC JUG 2";
	public final String COL_CALENDARIO_FASE_OF_BOLERA = "BOLERA";
	
	
	public String getInsertCabeceraCalendarioFaseOFRow(){
		return (this.COL_CALENDARIO_FASE_OF_ID + ";" +
				this.COL_CALENDARIO_FASE_OF_CATEGORIA + ";" +
				this.COL_CALENDARIO_FASE_OF_IDCRUCE + ";" +
				this.COL_CALENDARIO_FASE_OF_NUM_ENFRENTAMIENTO + ";" +
				this.COL_CALENDARIO_FASE_OF_JUGADOR_1 + ";" +
				this.COL_CALENDARIO_FASE_OF_JUEGOS_JUGADOR_1_P1 + ";" +
				this.COL_CALENDARIO_FASE_OF_JUEGOS_JUGADOR_1_P2 + ";" +
				this.COL_CALENDARIO_FASE_OF_JUEGOS_JUGADOR_1_P3 + ";" +
				this.COL_CALENDARIO_FASE_OF_JUEGOS_JUGADOR_2_P1 + ";" +
				this.COL_CALENDARIO_FASE_OF_JUEGOS_JUGADOR_2_P2 + ";" +
				this.COL_CALENDARIO_FASE_OF_JUEGOS_JUGADOR_2_P3 + ";" +
				this.COL_CALENDARIO_FASE_OF_JUGADOR_2 + ";" +
				this.COL_CALENDARIO_FASE_OF_ORDEN + ";" +
				this.COL_CALENDARIO_FASE_OF_CRUCE_CF + ";" +
				this.COL_CALENDARIO_FASE_OF_ACTIVO + ";" +
				this.COL_CALENDARIO_FASE_OF_INICIA + ";" +
				this.COL_CALENDARIO_FASE_OF_FASE_ANTERIOR + ";" +
				this.COL_CALENDARIO_FASE_OF_GRUPO_PROCEDENCIA_JUGADOR_1 + ";" +
				this.COL_CALENDARIO_FASE_OF_GRUPO_PROCEDENCIA_JUGADOR_2 + ";" +
				this.COL_CALENDARIO_FASE_OF_POSICION_PROCEDENCIA_JUGADOR_1 + ";" +
				this.COL_CALENDARIO_FASE_OF_POSICION_PROCEDENCIA_JUGADOR_2 + ";" +
				this.COL_CALENDARIO_FASE_OF_BOLERA
				);
	}
	
	//Cabeceras Fases CF - Calendario
	public final String COL_CALENDARIO_FASE_CF_ID = "ID";
	public final String COL_CALENDARIO_FASE_CF_CATEGORIA = "CATEGORIA";
	public final String COL_CALENDARIO_FASE_CF_IDCRUCE = "ID CRUCE";
	public final String COL_CALENDARIO_FASE_CF_NUM_ENFRENTAMIENTO = "NUM_ENFRENTAMIENTO";
	public final String COL_CALENDARIO_FASE_CF_JUGADOR_1 = "JUGADOR 1";
	public final String COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_1_P1 = "JUG. JUGADOR 1 P1";
	public final String COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_1_P2 = "JUG. JUGADOR 1 P2";
	public final String COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_1_P3 = "JUG. JUGADOR 1 P3";
	public final String COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_2_P1 = "JUG. JUGADOR 2 P1";
	public final String COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_2_P2 = "JUG. JUGADOR 2 P2";
	public final String COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_2_P3 = "JUG. JUGADOR 2 P3";
	public final String COL_CALENDARIO_FASE_CF_JUGADOR_2 = "JUGADOR 2";
	public final String COL_CALENDARIO_FASE_CF_ORDEN = "ORDEN";
	public final String COL_CALENDARIO_FASE_CF_CRUCE_CF = "CRUCE_CF";
	public final String COL_CALENDARIO_FASE_CF_ACTIVO = "ACTIVO";
	public final String COL_CALENDARIO_FASE_CF_INICIA = "INICIA";
	public final String COL_CALENDARIO_FASE_CF_FASE_ANTERIOR = "FASE ANTERIOR";
	public final String COL_CALENDARIO_FASE_CF_GRUPO_PROCEDENCIA_JUGADOR_1 = "GR PROC JUG 1";
	public final String COL_CALENDARIO_FASE_CF_GRUPO_PROCEDENCIA_JUGADOR_2 = "GR PROC JUG 2";
	public final String COL_CALENDARIO_FASE_CF_POSICION_PROCEDENCIA_JUGADOR_1 = "POS PROC JUG 1";
	public final String COL_CALENDARIO_FASE_CF_POSICION_PROCEDENCIA_JUGADOR_2 = "POS PROC JUG 2";
	public final String COL_CALENDARIO_FASE_CF_BOLERA = "BOLERA";
	
	
	public String getInsertCabeceraCalendarioFaseCFRow(){
		return (this.COL_CALENDARIO_FASE_CF_ID + ";" +
				this.COL_CALENDARIO_FASE_CF_CATEGORIA + ";" +
				this.COL_CALENDARIO_FASE_CF_IDCRUCE + ";" +
				this.COL_CALENDARIO_FASE_CF_NUM_ENFRENTAMIENTO + ";" +
				this.COL_CALENDARIO_FASE_CF_JUGADOR_1 + ";" +
				this.COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_1_P1 + ";" +
				this.COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_1_P2 + ";" +
				this.COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_1_P3 + ";" +
				this.COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_2_P1 + ";" +
				this.COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_2_P2 + ";" +
				this.COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_2_P3 + ";" +
				this.COL_CALENDARIO_FASE_CF_JUGADOR_2 + ";" +
				this.COL_CALENDARIO_FASE_CF_ORDEN + ";" +
				this.COL_CALENDARIO_FASE_CF_CRUCE_CF + ";" +
				this.COL_CALENDARIO_FASE_CF_ACTIVO + ";" +
				this.COL_CALENDARIO_FASE_CF_INICIA + ";" +
				this.COL_CALENDARIO_FASE_CF_FASE_ANTERIOR + ";" +
				this.COL_CALENDARIO_FASE_CF_GRUPO_PROCEDENCIA_JUGADOR_1 + ";" +
				this.COL_CALENDARIO_FASE_CF_GRUPO_PROCEDENCIA_JUGADOR_2 + ";" +
				this.COL_CALENDARIO_FASE_CF_POSICION_PROCEDENCIA_JUGADOR_1 + ";" +
				this.COL_CALENDARIO_FASE_CF_POSICION_PROCEDENCIA_JUGADOR_2 + ";" +
				this.COL_CALENDARIO_FASE_CF_BOLERA
				);
	}
	
	//Cabeceras Fases FC - Calendario
	public final String COL_CALENDARIO_FASE_FC_ID = "ID";
	public final String COL_CALENDARIO_FASE_FC_CATEGORIA = "CATEGORIA";
	public final String COL_CALENDARIO_FASE_FC_IDCRUCE = "ID CRUCE";
	public final String COL_CALENDARIO_FASE_FC_NUM_ENFRENTAMIENTO = "NUM_ENFRENTAMIENTO";
	public final String COL_CALENDARIO_FASE_FC_JUGADOR_1 = "JUGADOR 1";
	public final String COL_CALENDARIO_FASE_FC_JUEGOS_JUGADOR_1_P1 = "JUG. JUGADOR 1 P1";
	public final String COL_CALENDARIO_FASE_FC_JUEGOS_JUGADOR_1_P2 = "JUG. JUGADOR 1 P2";
	public final String COL_CALENDARIO_FASE_FC_JUEGOS_JUGADOR_1_P3 = "JUG. JUGADOR 1 P3";
	public final String COL_CALENDARIO_FASE_FC_JUEGOS_JUGADOR_2_P1 = "JUG. JUGADOR 2 P1";
	public final String COL_CALENDARIO_FASE_FC_JUEGOS_JUGADOR_2_P2 = "JUG. JUGADOR 2 P2";
	public final String COL_CALENDARIO_FASE_FC_JUEGOS_JUGADOR_2_P3 = "JUG. JUGADOR 2 P3";
	public final String COL_CALENDARIO_FASE_FC_JUGADOR_2 = "JUGADOR 2";
	public final String COL_CALENDARIO_FASE_FC_ORDEN = "ORDEN";
	public final String COL_CALENDARIO_FASE_FC_CRUCE_CF = "CRUCE_CF";
	public final String COL_CALENDARIO_FASE_FC_ACTIVO = "ACTIVO";
	public final String COL_CALENDARIO_FASE_FC_INICIA = "INICIA";
	public final String COL_CALENDARIO_FASE_FC_FASE_ANTERIOR = "FASE ANTERIOR";
	public final String COL_CALENDARIO_FASE_FC_GRUPO_PROCEDENCIA_JUGADOR_1 = "GR PROC JUG 1";
	public final String COL_CALENDARIO_FASE_FC_GRUPO_PROCEDENCIA_JUGADOR_2 = "GR PROC JUG 2";
	public final String COL_CALENDARIO_FASE_FC_POSICION_PROCEDENCIA_JUGADOR_1 = "POS PROC JUG 1";
	public final String COL_CALENDARIO_FASE_FC_POSICION_PROCEDENCIA_JUGADOR_2 = "POS PROC JUG 2";
	public final String COL_CALENDARIO_FASE_FC_BOLERA = "BOLERA";
	
	public String getInsertCabeceraCalendarioFaseFCRow(){
		return (this.COL_CALENDARIO_FASE_FC_ID + ";" +
				this.COL_CALENDARIO_FASE_FC_CATEGORIA + ";" +
				this.COL_CALENDARIO_FASE_FC_IDCRUCE + ";" +
				this.COL_CALENDARIO_FASE_FC_NUM_ENFRENTAMIENTO + ";" +
				this.COL_CALENDARIO_FASE_FC_JUGADOR_1 + ";" +
				this.COL_CALENDARIO_FASE_FC_JUEGOS_JUGADOR_1_P1 + ";" +
				this.COL_CALENDARIO_FASE_FC_JUEGOS_JUGADOR_1_P2 + ";" +
				this.COL_CALENDARIO_FASE_FC_JUEGOS_JUGADOR_1_P3 + ";" +
				this.COL_CALENDARIO_FASE_FC_JUEGOS_JUGADOR_2_P1 + ";" +
				this.COL_CALENDARIO_FASE_FC_JUEGOS_JUGADOR_2_P2 + ";" +
				this.COL_CALENDARIO_FASE_FC_JUEGOS_JUGADOR_2_P3 + ";" +
				this.COL_CALENDARIO_FASE_FC_JUGADOR_2 + ";" +
				this.COL_CALENDARIO_FASE_FC_ORDEN + ";" +
				this.COL_CALENDARIO_FASE_FC_CRUCE_CF + ";" +
				this.COL_CALENDARIO_FASE_FC_ACTIVO + ";" +
				this.COL_CALENDARIO_FASE_FC_INICIA + ";" +
				this.COL_CALENDARIO_FASE_FC_FASE_ANTERIOR + ";" +
				this.COL_CALENDARIO_FASE_FC_GRUPO_PROCEDENCIA_JUGADOR_1 + ";" +
				this.COL_CALENDARIO_FASE_FC_GRUPO_PROCEDENCIA_JUGADOR_2 + ";" +
				this.COL_CALENDARIO_FASE_FC_POSICION_PROCEDENCIA_JUGADOR_1 + ";" +
				this.COL_CALENDARIO_FASE_FC_POSICION_PROCEDENCIA_JUGADOR_2 + ";" +
				this.COL_CALENDARIO_FASE_FC_BOLERA
				);
	}
	
}
