package com.bolocelta.bbdd.constants;

public class EstadosIndividualParejas2022 {
	
	public static final String CONFIG_ESTADO_CREAR = "CR";
	public static final String CONFIG_ESTADO_ABRIR_INSCRIPCIONES = "AI";
	public static final String CONFIG_ESTADO_CERRAR_INSCRIPCIONES = "CI";
	public static final String CONFIG_ESTADO_CERRAR_FASES = "CF";
	public static final String CONFIG_ESTADO_SORTEADO_CAMPEONATO = "SC";
	public static final String CONFIG_ESTADO_FINALIZADO = "FN";

}
