package com.bolocelta.bbdd.constants;

public class CrucesCampeonatos2022 {
	
	//Boladas
	public static String CB1 = "CB1";
	public static String CB2 = "CB2";
	public static String CB3 = "CB3";
	public static String CB4 = "CB4";
	public static String CB5 = "CB5";
	public static String CB6 = "CB6";
	public static String CB7 = "CB7";
	public static String CB8 = "CB8";
	
	//Octavos de final
	public static String OFA = "OFA";
	public static String OFB = "OFB";
	public static String OFC = "OFC";
	public static String OFD = "OFD";
	public static String OFE = "OFE";
	public static String OFF = "OFF";
	public static String OFG = "OFG";
	public static String OFH = "OFH";
	//Cuartos de final
	public static String CFA = "CFA";
	public static String CFB = "CFB";
	public static String CFC = "CFC";
	public static String CFD = "CFD";
	//Semifinales
	public static String SF1 = "SF1";
	public static String SF2 = "SF2";
	//Final consolacion
	public static String FC1 = "FC1";
	//Final
	public static String FF1 = "FF1";
	
}
