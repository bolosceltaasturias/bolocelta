package com.bolocelta.bbdd.createTable;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.bolocelta.bbdd.constants.Activo2022;
import com.bolocelta.bbdd.constants.EstadosIndividualParejas2022;
import com.bolocelta.bbdd.constants.NombresTablas2022;
import com.bolocelta.bbdd.constants.Ubicaciones2022;
import com.bolocelta.bbdd.constants.structure.Estructura2022;
import com.bolocelta.bbdd.constants.structure.EstructuraCampeonatoMasculinoIndividualSegunda2022;
import com.bolocelta.bbdd.readTables.LeerCampeonatoMasculinoIndividualSegunda2022;
import com.bolocelta.entities.Categorias2022;
import com.bolocelta.entities.Fases2022;
import com.bolocelta.entities.Jugadores2022;
import com.bolocelta.entities.ParticipantesIndividual2022;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseCF2022;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseFC2022;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseFF2022;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseI2022;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseOF2022;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseSF2022;
import com.bolocelta.entities.sorteos.individual.ClasificacionFaseI2022;

public class CrearCampeonatoMasculinoIndividualSegunda2022 extends ACrearModificar2022 {
	
	LeerCampeonatoMasculinoIndividualSegunda2022 leerCampeonatoMasculinoIndividualSegunda = new LeerCampeonatoMasculinoIndividualSegunda2022();

	private void crearExcel() {

	}
	
	private void crearHojaConfiguracion(Date fechaMaximaInscripcion, String estado, String bolera){
		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CONFIG,
				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
	}
	
	private void crearHojaFases(){
		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_FASES,
				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
	}
	
	private void crearHojaParticipantes(){
		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_PARTICIPANTES,
				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
	}
	
	private void crearHojaGrupos(){
		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_GRUPOS,
				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
	}
	
	private void crearHojaClasificacionFaseI(){
		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CLA_FASE_I,
				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
	}
	
	private void crearHojaCalendarioFaseI(){
		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_I,
				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
	}
	
	private void crearHojaClasificacionFaseII(){
		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CLA_FASE_II,
				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
	}
	
	private void crearHojaCalendarioFaseII(){
		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_II,
				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
	}
	
	private void crearHojaCalendarioFaseOF(){
		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_OF,
				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
	}
	
	private void crearHojaCalendarioFaseCF(){
		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_CF,
				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
	}
	
	private void crearHojaCalendarioFaseSF(){
		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_SF,
				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
	}
	
	private void crearHojaCalendarioFaseFC(){
		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_FC,
				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
	}
	
	private void crearHojaCalendarioFaseFF(){
		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_FF,
				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
	}
	
	private void insertRow(String excel, String newRowData) {
		insertDataCsv(excel, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA, newRowData);
	}
	
	public void updateRow(Estructura2022 update, Long fila, Object newValue) {
		updateDataCsv(update.getExcel(), Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA, fila, update.getColumna(), newValue, update.getTipoDato());
	}
	
	
	//Creacion de la base del campeonato
	public void crearBaseCampeonato(Date fechaMaximaInscripcion, String estado, String bolera){
		//Crear las hojas excel
		crearExcel();
		crearHojaConfiguracion(fechaMaximaInscripcion, estado, bolera);
		crearHojaFases();
		crearHojaParticipantes();
		
		
		//Crear la estructura de las hojas (cabeceras y registros por defecto)
		EstructuraCampeonatoMasculinoIndividualSegunda2022 ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda2022();
		//Crear las estructuras de las hojas
		crearEstructuraHojas(ecmi);
		//Rellenar datos de configuración
		rellenarDatosConfiguracion(ecmi, fechaMaximaInscripcion, estado, bolera);
		//Rellenar todas las fases
		rellenarDatosFases(ecmi);
	}
	
	public void crearEstructuraHojas(EstructuraCampeonatoMasculinoIndividualSegunda2022 ecmi) {
		//Configuracion
		for (String row : ecmi.getInsertCabeceraConfiguracionRow()) {
			insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CONFIG, row);
		}
		//Fases
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_FASES, ecmi.getInsertCabeceraFasesRow());
		//Participantes
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_PARTICIPANTES, ecmi.getInsertCabeceraParticipantesRow());
	}

	private void rellenarDatosConfiguracion(EstructuraCampeonatoMasculinoIndividualSegunda2022 ecmi, Date fechaMaximaInscripcion, String estado, 
			String bolera) {
		//Actualizar la fecha
	    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy"); // date pattern
	    String fechaMaxInscripcion = simpleDateFormat.format(fechaMaximaInscripcion);
		updateRow(ecmi.getEstructuraFechaMaximaInscripcion(), ecmi.getEstructuraFechaMaximaInscripcion().getFila(), fechaMaxInscripcion);
		//Actualizar el estado
		updateRow(ecmi.getEstructuraEstadoCampeonato(), ecmi.getEstructuraEstadoCampeonato().getFila(), estado);
		updateRow(ecmi.getEstructuraBoleraFinal(), ecmi.getEstructuraBoleraFinal().getFila(), bolera);
	}
	
	public void actualizarEstadoConfiguracion(String estado) {
		EstructuraCampeonatoMasculinoIndividualSegunda2022 ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda2022();
		//Actualizar el estado
		updateRow(ecmi.getEstructuraEstadoCampeonato(), ecmi.getEstructuraEstadoCampeonato().getFila(), estado);
	}
	
	private void rellenarDatosFases(EstructuraCampeonatoMasculinoIndividualSegunda2022 ecmi) {
		for(String fase : ecmi.getEstructuraTodasFases()){
			insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_FASES, fase);
		}
	}
	
	public void inscribirNewJugador(Jugadores2022 jugador, Categorias2022 categoria, String usuario, String activo) {
		EstructuraCampeonatoMasculinoIndividualSegunda2022 ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda2022();
    	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss"); // date pattern
    	String fecha = simpleDateFormat.format(new Date());
    	
    	Long newRowNum = leerCampeonatoMasculinoIndividualSegunda.lastRowSheet(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_PARTICIPANTES, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
    	newRowNum++;
    	
	    String insert = newRowNum.toString() + ";" + jugador.getId().toString() + ";" + fecha + ";" + usuario + ";" + activo;
	    insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_PARTICIPANTES, insert);
	}
	
	public void inscribirUpdateJugador(ParticipantesIndividual2022 participantesIndividual, Jugadores2022 jugador, Categorias2022 categoria, String usuario, String activo) {
		EstructuraCampeonatoMasculinoIndividualSegunda2022 ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda2022();
		
		Long fila = participantesIndividual.getRowNum();
		
		for (Estructura2022 eip : ecmi.getEstructuraInscribirParticipantes()) {
			
			// Cabeceras
			if (eip.getValor().equals(ecmi.COL_PARTICIPANTES_ID)) {
				updateRow(eip, fila, fila.toString());
			} else if (eip.getValor().equals(ecmi.COL_PARTICIPANTES_ID_JUGADOR)) {
				updateRow(eip, fila, jugador.getId().toString());
			} else if (eip.getValor().equals(ecmi.COL_PARTICIPANTES_USUARIO)) {
				updateRow(eip, fila, usuario);
			} else if (eip.getValor().equals(ecmi.COL_PARTICIPANTES_FECHA_INSCRIPCION)) {
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss"); // date pattern
			    String fecha = simpleDateFormat.format(new Date());
				updateRow(eip, fila, fecha);
			} else if (eip.getValor().equals(ecmi.COL_PARTICIPANTES_ACTIVO)) {
				updateRow(eip, fila, activo);
			}

		}
	}
	
	
	public void actualizarActivoFase(Fases2022 fase) {
		EstructuraCampeonatoMasculinoIndividualSegunda2022 ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda2022();
		Long fila = fase.getRowNum();
		for (Estructura2022 ef : ecmi.getEstructuraFases()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_FASES_ACTIVO)) {
				updateRow(ef, fila, fase.getActivo());
			}
		}
	}
	
	public void actualizarFase(Fases2022 fase) {
		EstructuraCampeonatoMasculinoIndividualSegunda2022 ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda2022();
		Long fila = fase.getRowNum();
		for (Estructura2022 ef : ecmi.getEstructuraFases()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_FASES_NUMERO_ENFRENTAMIENTOS_GRUPOS)) {
				updateRow(ef, fila, fase.getNumeroEnfrentamientos());
			}else if (ef.getValor().equals(ecmi.COL_FASES_NUMERO_PARTIDAS)) {
				updateRow(ef, fila, fase.getNumeroPartidas());
			}else if (ef.getValor().equals(ecmi.COL_FASES_NUMERO_JUEGOS)) {
				updateRow(ef, fila, fase.getNumeroJuegos());
			}else if (ef.getValor().equals(ecmi.COL_FASES_CLASIFICAN)) {
				updateRow(ef, fila, fase.getClasifican());
			}else if (ef.getValor().equals(ecmi.COL_FASES_FECHA)) {
				updateRow(ef, fila, fase.getFecha());
			}else if (ef.getValor().equals(ecmi.COL_FASES_HORA)) {
				updateRow(ef, fila, fase.getHora());
			}else if (ef.getValor().equals(ecmi.COL_FASES_FASE_SIGUIENTE)) {
				updateRow(ef, fila, fase.getFaseSiguiente());
			}
			
		}		
	}
	
	public void crearClasificacionFaseI(){
		EstructuraCampeonatoMasculinoIndividualSegunda2022 ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda2022();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CLA_FASE_I, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CLA_FASE_I, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		}else{
			crearHojaClasificacionFaseI();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CLA_FASE_I, ecmi.getInsertCabeceraClasificacionFaseIRow());
	}
	
	public void insertarRowClasificacionFaseI(String insert){
		EstructuraCampeonatoMasculinoIndividualSegunda2022 ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda2022();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CLA_FASE_I, insert);
	}
	
	public void crearCalendarioFaseI(){
		EstructuraCampeonatoMasculinoIndividualSegunda2022 ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda2022();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_I, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_I, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		}else{
			crearHojaCalendarioFaseI();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_I, ecmi.getInsertCabeceraCalendarioFaseIRow());
	}
	
	public void insertarRowCalendarioFaseI(String insert){
		EstructuraCampeonatoMasculinoIndividualSegunda2022 ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda2022();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_I, insert);
	}
	
	public void actualizarResultadoCalendarioFaseI(CalendarioFaseI2022 calendarioFaseI, ClasificacionFaseI2022 clasificacionFaseIJugador1, 
			ClasificacionFaseI2022 clasificacionFaseIJugador2) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoIndividualSegunda2022 ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda2022();
		Long fila = calendarioFaseI.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
		
		if(calendarioFaseI.isModificable()){
			for (Estructura2022 ef : ecmi.getEstructuraCalendarioFaseI()) {
				// Cabeceras
				if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_I_JUEGOS_JUGADOR_1)) {
					updateRow(ef, fila, calendarioFaseI.getJuegosJugador1());
				}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_I_JUEGOS_JUGADOR_2)) {
					updateRow(ef, fila, calendarioFaseI.getJuegosJugador2());
				}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_I_ACTIVO)) {
					calendarioFaseI.setActivo(Activo2022.NO);
					updateRow(ef, fila, calendarioFaseI.getActivo());
				}
			}
			fila = clasificacionFaseIJugador1.getRowNum();
			//Actualizar resultado en clasificacion Jugador 1
			for (Estructura2022 ef : ecmi.getEstructuraClasificacionFaseI()) {
				// Cabeceras
				if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PJ)) {
					clasificacionFaseIJugador1.setPj(clasificacionFaseIJugador1.getPj()+1);
					updateRow(ef, fila, clasificacionFaseIJugador1.getPj());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PG)) {
					if(calendarioFaseI.isGanaJugador1()){
						clasificacionFaseIJugador1.setPg(clasificacionFaseIJugador1.getPg()+1);
					}
					updateRow(ef, fila, clasificacionFaseIJugador1.getPg());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PE)) {
					if(calendarioFaseI.isEmpate()){
						clasificacionFaseIJugador1.setPe(clasificacionFaseIJugador1.getPe()+1);
					}
					updateRow(ef, fila, clasificacionFaseIJugador1.getPe());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PP)) {
					if(calendarioFaseI.isGanaJugador2()){
						clasificacionFaseIJugador1.setPp(clasificacionFaseIJugador1.getPp()+1);
					}
					updateRow(ef, fila, clasificacionFaseIJugador1.getPp());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PF)) {
					clasificacionFaseIJugador1.setPf(clasificacionFaseIJugador1.getPf()+calendarioFaseI.getJuegosJugador1());
					updateRow(ef, fila, clasificacionFaseIJugador1.getPf());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PC)) {
					clasificacionFaseIJugador1.setPc(clasificacionFaseIJugador1.getPc()+calendarioFaseI.getJuegosJugador2());
					updateRow(ef, fila, clasificacionFaseIJugador1.getPc());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PT)) {
					if(calendarioFaseI.isGanaJugador1()){
						clasificacionFaseIJugador1.setPt(clasificacionFaseIJugador1.getPt()+3);
					}else if(calendarioFaseI.isEmpate()){
						clasificacionFaseIJugador1.setPt(clasificacionFaseIJugador1.getPt()+1);
					}
					updateRow(ef, fila, clasificacionFaseIJugador1.getPt());
				}
			}
			fila = clasificacionFaseIJugador2.getRowNum();
			//Actualizar resultado en clasificacion Jugador 2
			for (Estructura2022 ef : ecmi.getEstructuraClasificacionFaseI()) {
				// Cabeceras
				if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PJ)) {
					clasificacionFaseIJugador2.setPj(clasificacionFaseIJugador2.getPj()+1);
					updateRow(ef, fila, clasificacionFaseIJugador2.getPj());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PG)) {
					if(calendarioFaseI.isGanaJugador2()){
						clasificacionFaseIJugador2.setPg(clasificacionFaseIJugador2.getPg()+1);
					}
					updateRow(ef, fila, clasificacionFaseIJugador2.getPg());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PE)) {
					if(calendarioFaseI.isEmpate()){
						clasificacionFaseIJugador2.setPe(clasificacionFaseIJugador2.getPe()+1);
					}
					updateRow(ef, fila, clasificacionFaseIJugador2.getPe());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PP)) {
					if(calendarioFaseI.isGanaJugador1()){
						clasificacionFaseIJugador2.setPp(clasificacionFaseIJugador2.getPp()+1);
					}
					updateRow(ef, fila, clasificacionFaseIJugador2.getPp());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PF)) {
					clasificacionFaseIJugador2.setPf(clasificacionFaseIJugador2.getPf()+calendarioFaseI.getJuegosJugador2());
					updateRow(ef, fila, clasificacionFaseIJugador2.getPf());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PC)) {
					clasificacionFaseIJugador2.setPc(clasificacionFaseIJugador2.getPc()+calendarioFaseI.getJuegosJugador1());
					updateRow(ef, fila, clasificacionFaseIJugador2.getPc());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PT)) {
					if(calendarioFaseI.isGanaJugador2()){
						clasificacionFaseIJugador2.setPt(clasificacionFaseIJugador2.getPt()+3);
					}else if(calendarioFaseI.isEmpate()){
						clasificacionFaseIJugador2.setPt(clasificacionFaseIJugador2.getPt()+1);
					}
					updateRow(ef, fila, clasificacionFaseIJugador2.getPt());
				}
			}
		}
		
		
	}
	
	public void crearCalendarioFaseII(){
		EstructuraCampeonatoMasculinoIndividualSegunda2022 ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda2022();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_II, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_II, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		}else{
			crearHojaCalendarioFaseII();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_II, ecmi.getInsertCabeceraCalendarioFaseIIRow());
	}
	
	public void crearClasificacionFaseII(){
		EstructuraCampeonatoMasculinoIndividualSegunda2022 ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda2022();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CLA_FASE_II, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CLA_FASE_II, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		}else{
			crearHojaClasificacionFaseII();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CLA_FASE_II, ecmi.getInsertCabeceraClasificacionFaseIIRow());
	}
	
	public void crearCalendarioFaseOF(){
		EstructuraCampeonatoMasculinoIndividualSegunda2022 ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda2022();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_OF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_OF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		}else{
			crearHojaCalendarioFaseOF();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_OF, ecmi.getInsertCabeceraCalendarioFaseOFRow());
	}
	
	public void insertarRowCalendarioFaseOF(String insert){
		EstructuraCampeonatoMasculinoIndividualSegunda2022 ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda2022();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_OF, insert);
	}
	
	public void actualizarJugadorOctavosFinal(CalendarioFaseOF2022 calendarioFaseOF, boolean jugador1, boolean jugador2) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoIndividualSegunda2022 ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda2022();
		Long fila = calendarioFaseOF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2022 ef : ecmi.getEstructuraCalendarioOctavosFinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_JUGADOR_1) && jugador1) {
				updateRow(ef, fila, calendarioFaseOF.getJugador1Id());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_JUGADOR_2) && jugador2) {
				updateRow(ef, fila, calendarioFaseOF.getJugador2Id());
			}
		}
		
		
	}
	
	public void actualizarJugadorCuartosFinal(CalendarioFaseCF2022 calendarioFaseCF, boolean jugador1, boolean jugador2) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoIndividualSegunda2022 ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda2022();
		Long fila = calendarioFaseCF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2022 ef : ecmi.getEstructuraCalendarioCuartosFinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_JUGADOR_1) && jugador1) {
				updateRow(ef, fila, calendarioFaseCF.getJugador1Id());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_JUGADOR_2) && jugador2) {
				updateRow(ef, fila, calendarioFaseCF.getJugador2Id());
			}
		}
		
		
	}
	
	public void actualizarJugadorSemiFinal(CalendarioFaseSF2022 calendarioFaseSF, boolean jugador1, boolean jugador2) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoIndividualSegunda2022 ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda2022();
		Long fila = calendarioFaseSF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2022 ef : ecmi.getEstructuraCalendarioSemifinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUGADOR_1) && jugador1) {
				updateRow(ef, fila, calendarioFaseSF.getJugador1Id());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUGADOR_2) && jugador2) {
				updateRow(ef, fila, calendarioFaseSF.getJugador2Id());
			}
		}
		
		
	}
	
	public void actualizarJugadorFinalConsolacion(CalendarioFaseFC2022 calendarioFaseFC, boolean jugador1, boolean jugador2) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoIndividualSegunda2022 ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda2022();
		Long fila = calendarioFaseFC.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2022 ef : ecmi.getEstructuraCalendarioFinalConsolacion()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_JUGADOR_1) && jugador1) {
				updateRow(ef, fila, calendarioFaseFC.getJugador1Id());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_JUGADOR_2) && jugador2) {
				updateRow(ef, fila, calendarioFaseFC.getJugador2Id());
			}
		}
		
		
	}
	
	public void actualizarJugadorFinal(CalendarioFaseFF2022 calendarioFaseFF, boolean jugador1, boolean jugador2) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoIndividualSegunda2022 ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda2022();
		Long fila = calendarioFaseFF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2022 ef : ecmi.getEstructuraCalendarioFinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUGADOR_1) && jugador1) {
				updateRow(ef, fila, calendarioFaseFF.getJugador1Id());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUGADOR_2) && jugador2) {
				updateRow(ef, fila, calendarioFaseFF.getJugador2Id());
			}
		}
		
		
	}
	
	public void actualizarResultadosEnfrentamientoDirectoOF(CalendarioFaseOF2022 calendarioFaseOF) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoIndividualSegunda2022 ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda2022();
		Long fila = calendarioFaseOF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2022 ef : ecmi.getEstructuraCalendarioOctavosFinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_JUEGOS_JUGADOR_1_P1)) {
				updateRow(ef, fila, calendarioFaseOF.getJuegosJugador1P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_JUEGOS_JUGADOR_2_P1)) {
				updateRow(ef, fila, calendarioFaseOF.getJuegosJugador2P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_JUEGOS_JUGADOR_1_P2)) {
				updateRow(ef, fila, calendarioFaseOF.getJuegosJugador1P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_JUEGOS_JUGADOR_2_P2)) {
				updateRow(ef, fila, calendarioFaseOF.getJuegosJugador2P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_JUEGOS_JUGADOR_1_P3)) {
				updateRow(ef, fila, calendarioFaseOF.getJuegosJugador1P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_JUEGOS_JUGADOR_2_P3)) {
				updateRow(ef, fila, calendarioFaseOF.getJuegosJugador2P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_ACTIVO)) {
				calendarioFaseOF.setActivo(Activo2022.NO);
				updateRow(ef, fila, calendarioFaseOF.getActivo());
			}
		}
		
		
	}
	
	public void crearCalendarioFaseCF(){
		EstructuraCampeonatoMasculinoIndividualSegunda2022 ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda2022();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_CF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_CF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		}else{
			crearHojaCalendarioFaseCF();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_CF, ecmi.getInsertCabeceraCalendarioFaseCFRow());
	}
	
	public void insertarRowCalendarioFaseCF(String insert){
		EstructuraCampeonatoMasculinoIndividualSegunda2022 ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda2022();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_CF, insert);
	}
	
	public void actualizarResultadosEnfrentamientoDirectoCF(CalendarioFaseCF2022 calendarioFaseCF) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoIndividualSegunda2022 ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda2022();
		Long fila = calendarioFaseCF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2022 ef : ecmi.getEstructuraCalendarioCuartosFinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_1_P1)) {
				updateRow(ef, fila, calendarioFaseCF.getJuegosJugador1P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_2_P1)) {
				updateRow(ef, fila, calendarioFaseCF.getJuegosJugador2P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_1_P2)) {
				updateRow(ef, fila, calendarioFaseCF.getJuegosJugador1P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_2_P2)) {
				updateRow(ef, fila, calendarioFaseCF.getJuegosJugador2P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_1_P3)) {
				updateRow(ef, fila, calendarioFaseCF.getJuegosJugador1P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_JUEGOS_JUGADOR_2_P3)) {
				updateRow(ef, fila, calendarioFaseCF.getJuegosJugador2P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_ACTIVO)) {
				calendarioFaseCF.setActivo(Activo2022.NO);
				updateRow(ef, fila, calendarioFaseCF.getActivo());
			}
		}
		
		
	}
	
	public void crearCalendarioFaseSF(){
		EstructuraCampeonatoMasculinoIndividualSegunda2022 ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda2022();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_SF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_SF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		}else{
			crearHojaCalendarioFaseSF();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_SF, ecmi.getInsertCabeceraCalendarioFaseSFRow());
	}
	
	public void insertarRowCalendarioFaseSF(String insert){
		EstructuraCampeonatoMasculinoIndividualSegunda2022 ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda2022();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_SF, insert);
	}
	
	public void actualizarResultadosEnfrentamientoDirectoSF(CalendarioFaseSF2022 calendarioFaseSF) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoIndividualSegunda2022 ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda2022();
		Long fila = calendarioFaseSF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2022 ef : ecmi.getEstructuraCalendarioSemifinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_1_P1)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosJugador1P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_2_P1)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosJugador2P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_1_P2)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosJugador1P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_2_P2)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosJugador2P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_1_P3)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosJugador1P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_2_P3)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosJugador2P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_ACTIVO)) {
				calendarioFaseSF.setActivo(Activo2022.NO);
				updateRow(ef, fila, calendarioFaseSF.getActivo());
			}
		}
		
		
	}
	
	public void crearCalendarioFaseFC(){
		EstructuraCampeonatoMasculinoIndividualSegunda2022 ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda2022();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_FC, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_FC, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		}else{
			crearHojaCalendarioFaseFC();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_FC, ecmi.getInsertCabeceraCalendarioFaseFCRow());
	}
	
	public void insertarRowCalendarioFaseFC(String insert){
		EstructuraCampeonatoMasculinoIndividualSegunda2022 ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda2022();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_FC, insert);
	}
	
	public void actualizarResultadosEnfrentamientoDirectoFC(CalendarioFaseFC2022 calendarioFaseFC) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoIndividualSegunda2022 ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda2022();
		Long fila = calendarioFaseFC.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2022 ef : ecmi.getEstructuraCalendarioFinalConsolacion()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_JUEGOS_JUGADOR_1_P1)) {
				updateRow(ef, fila, calendarioFaseFC.getJuegosJugador1P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_JUEGOS_JUGADOR_2_P1)) {
				updateRow(ef, fila, calendarioFaseFC.getJuegosJugador2P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_JUEGOS_JUGADOR_1_P2)) {
				updateRow(ef, fila, calendarioFaseFC.getJuegosJugador1P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_JUEGOS_JUGADOR_2_P2)) {
				updateRow(ef, fila, calendarioFaseFC.getJuegosJugador2P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_JUEGOS_JUGADOR_1_P3)) {
				updateRow(ef, fila, calendarioFaseFC.getJuegosJugador1P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_JUEGOS_JUGADOR_2_P3)) {
				updateRow(ef, fila, calendarioFaseFC.getJuegosJugador2P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_ACTIVO)) {
				calendarioFaseFC.setActivo(Activo2022.NO);
				updateRow(ef, fila, calendarioFaseFC.getActivo());
			}
		}
		
		
	}
	
	public void crearCalendarioFaseFF(){
		EstructuraCampeonatoMasculinoIndividualSegunda2022 ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda2022();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_FF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_FF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		}else{
			crearHojaCalendarioFaseFF();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_FF, ecmi.getInsertCabeceraCalendarioFaseFFRow());
	}
	
	public void insertarRowCalendarioFaseFF(String insert){
		EstructuraCampeonatoMasculinoIndividualSegunda2022 ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda2022();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_FF, insert);
	}
	
	public void actualizarResultadosEnfrentamientoDirectoFF(CalendarioFaseFF2022 calendarioFaseFF) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoIndividualSegunda2022 ecmi = new EstructuraCampeonatoMasculinoIndividualSegunda2022();
		Long fila = calendarioFaseFF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2022 ef : ecmi.getEstructuraCalendarioFinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_1_P1)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosJugador1P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_2_P1)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosJugador2P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_1_P2)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosJugador1P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_2_P2)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosJugador2P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_1_P3)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosJugador1P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_2_P3)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosJugador2P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_ACTIVO)) {
				calendarioFaseFF.setActivo(Activo2022.NO);
				updateRow(ef, fila, calendarioFaseFF.getActivo());
			}
		}
		
		
	}
	
	public void borrarSorteo(){
		deleteFile(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_I, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		deleteFile(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CLA_FASE_I, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		deleteFile(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_II, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		deleteFile(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CLA_FASE_II, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		deleteFile(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_OF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		deleteFile(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_CF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		deleteFile(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_SF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		deleteFile(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_FC, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		deleteFile(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_SEGUNDA_CAL_FASE_FF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_SEGUNDA);
		actualizarEstadoConfiguracion(EstadosIndividualParejas2022.CONFIG_ESTADO_CERRAR_INSCRIPCIONES);
	}
	
	

}
