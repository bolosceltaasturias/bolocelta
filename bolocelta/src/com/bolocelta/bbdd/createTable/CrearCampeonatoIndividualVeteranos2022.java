package com.bolocelta.bbdd.createTable;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.bolocelta.bbdd.constants.Activo2022;
import com.bolocelta.bbdd.constants.EstadosIndividualParejas2022;
import com.bolocelta.bbdd.constants.NombresTablas2022;
import com.bolocelta.bbdd.constants.Ubicaciones2022;
import com.bolocelta.bbdd.constants.structure.Estructura2022;
import com.bolocelta.bbdd.constants.structure.EstructuraCampeonatoIndividualVeteranos2022;
import com.bolocelta.bbdd.readTables.LeerCampeonatoVeteranosIndividual2022;
import com.bolocelta.entities.CampeonatoVeteranosIndividualClasificacion2022;
import com.bolocelta.entities.Categorias2022;
import com.bolocelta.entities.Fases2022;
import com.bolocelta.entities.Jugadores2022;
import com.bolocelta.entities.ParticipantesIndividual2022;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseFF2022;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseSF2022;

public class CrearCampeonatoIndividualVeteranos2022 extends ACrearModificar2022 {

	LeerCampeonatoVeteranosIndividual2022 leerCampeonatoVeteranosIndividual = new LeerCampeonatoVeteranosIndividual2022();
	
	public void actualizarClasificacionConfirmar(CampeonatoVeteranosIndividualClasificacion2022 cecCla) {
		
		Long filaCla = cecCla.getRowNum();
				
		if(cecCla.getActivo() == 1){
		
			//Actualizar clasificacion de equipo 1
			EstructuraCampeonatoIndividualVeteranos2022 ecl1 = new EstructuraCampeonatoIndividualVeteranos2022(
					NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CLASIFICACION);
			cecCla.setActivo(Activo2022.NO_NUMBER);
			for (Estructura2022 ef : ecl1.getClasificacionList()) {
				if (ef.getValor().equals(ecl1.COL_CLASIFICACION_ACTIVO)) {
					updateRowClasificacion(ef, filaCla, cecCla.getActivo(), cecCla);
				}
			}
		}
	}

	public void actualizarClasificacion(CampeonatoVeteranosIndividualClasificacion2022 cecCla) {
		
		Long filaCla = cecCla.getRowNum();
				
		if(cecCla.getActivo() == 1){
		
			//Actualizar clasificacion de equipo 1
			EstructuraCampeonatoIndividualVeteranos2022 ecl1 = new EstructuraCampeonatoIndividualVeteranos2022(
					NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CLASIFICACION);
			for (Estructura2022 ef : ecl1.getClasificacionList()) {
				if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA_1)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada1(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA_1)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada1(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA_2)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada2(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA_2)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada2(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA_3)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada3(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA_3)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada3(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA_4)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada4(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA_4)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada4(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA_5)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada5(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA_5)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada5(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA_6)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada6(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA_6)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada6(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA_7)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada7(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA_7)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada7(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA_8)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada8(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA_8)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada8(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA_9)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada9(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA_9)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada9(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA_10)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada10(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA_10)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada10(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA_11)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada11(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA_11)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada11(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA_12)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada12(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA_12)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada12(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA_13)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada13(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA_13)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada13(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA_14)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada14(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA_14)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada14(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA_15)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada15(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA_15)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada15(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA_16)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada16(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA_16)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada16(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA_17)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada17(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA_17)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada17(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA_18)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada18(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA_18)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada18(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA_19)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada19(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA_19)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada19(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA_20)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada20(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA_20)) {
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada20(), cecCla);
				}
			}
		}
	}
	
	public void updateRowClasificacion(Estructura2022 update, Long fila, Object newValue, CampeonatoVeteranosIndividualClasificacion2022 cec) {
		updateDataCsv(update.getExcel(), Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS, fila, update.getColumna(), newValue, update.getTipoDato());
	}
	
	
	public void actualizarJugadorSemiFinal(CalendarioFaseSF2022 calendarioFaseSF, boolean jugador1, boolean jugador2) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoIndividualVeteranos2022 ecmi = new EstructuraCampeonatoIndividualVeteranos2022();
		Long fila = calendarioFaseSF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2022 ef : ecmi.getEstructuraCalendarioSemifinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUGADOR_1)) {
				updateRow(ef, fila, calendarioFaseSF.getJugador1Id());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUGADOR_2)) {
				updateRow(ef, fila, calendarioFaseSF.getJugador2Id());
			}
		}
		
		
	}
		
	public void actualizarJugadorFinal(CalendarioFaseFF2022 calendarioFaseFF, boolean jugador1, boolean jugador2) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoIndividualVeteranos2022 ecmi = new EstructuraCampeonatoIndividualVeteranos2022();
		Long fila = calendarioFaseFF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2022 ef : ecmi.getEstructuraCalendarioFinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUGADOR_1) && jugador1) {
				updateRow(ef, fila, calendarioFaseFF.getJugador1Id());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUGADOR_2) && jugador2) {
				updateRow(ef, fila, calendarioFaseFF.getJugador2Id());
			}
		}
		
		
	}

	public void updateRow(Estructura2022 update, Long fila, Object newValue) {
		updateDataCsv(update.getExcel(), Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS, fila, update.getColumna(), newValue, update.getTipoDato());
	}
	
	
	public void actualizarResultadosEnfrentamientoDirectoSF(CalendarioFaseSF2022 calendarioFaseSF) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoIndividualVeteranos2022 ecmi = new EstructuraCampeonatoIndividualVeteranos2022();
		Long fila = calendarioFaseSF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2022 ef : ecmi.getEstructuraCalendarioSemifinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_1_P1)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosJugador1P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_2_P1)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosJugador2P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_1_P2)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosJugador1P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_2_P2)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosJugador2P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_1_P3)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosJugador1P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_JUGADOR_2_P3)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosJugador2P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_ACTIVO)) {
				calendarioFaseSF.setActivo(Activo2022.NO);
				updateRow(ef, fila, calendarioFaseSF.getActivo());
			}
		}
		
		
	}
	
	public void actualizarResultadosEnfrentamientoDirectoFF(CalendarioFaseFF2022 calendarioFaseFF) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoIndividualVeteranos2022 ecmi = new EstructuraCampeonatoIndividualVeteranos2022();
		Long fila = calendarioFaseFF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2022 ef : ecmi.getEstructuraCalendarioFinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_1_P1)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosJugador1P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_2_P1)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosJugador2P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_1_P2)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosJugador1P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_2_P2)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosJugador2P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_1_P3)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosJugador1P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_JUGADOR_2_P3)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosJugador2P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_ACTIVO)) {
				calendarioFaseFF.setActivo(Activo2022.NO);
				updateRow(ef, fila, calendarioFaseFF.getActivo());
			}
		}
		
		
	}
	
	//Creacion de la base del campeonato
	public void crearBaseCampeonato(Date fechaMaximaInscripcion, String estado, String bolera){
		//Crear las hojas excel
		crearExcel();
		crearHojaConfiguracion(fechaMaximaInscripcion, estado, bolera);
		crearHojaFases();
		crearHojaParticipantes();
		
		
		//Crear la estructura de las hojas (cabeceras y registros por defecto)
		EstructuraCampeonatoIndividualVeteranos2022 ecmi = new EstructuraCampeonatoIndividualVeteranos2022();
		//Crear las estructuras de las hojas
		crearEstructuraHojas(ecmi);
		//Rellenar datos de configuración
		rellenarDatosConfiguracion(ecmi, fechaMaximaInscripcion, estado, bolera);
		//Rellenar todas las fases
		rellenarDatosFases(ecmi);
	}
	
	private void crearExcel() {

	}
	
	private void crearHojaConfiguracion(Date fechaMaximaInscripcion, String estado, String bolera){
		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CONFIG,
				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
	}
	
	private void crearHojaFases(){
		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_FASES,
				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
	}
	
	private void crearHojaParticipantes(){
		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_PARTICIPANTES,
				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
	}
	
//	private void crearHojaGrupos(){
//		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_GRUPOS,
//				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
//	}
	
//	private void crearHojaClasificacionFaseI(){
//		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CLA_FASE_I,
//				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
//	}
//	
//	private void crearHojaCalendarioFaseI(){
//		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_I,
//				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
//	}
//	
//	private void crearHojaClasificacionFaseII(){
//		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CLA_FASE_II,
//				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
//	}
//	
//	private void crearHojaCalendarioFaseII(){
//		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_II,
//				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
//	}
	
//	private void crearHojaCalendarioFaseOF(){
//		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_OF,
//				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
//	}
//	
//	private void crearHojaCalendarioFaseCF(){
//		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_CF,
//				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
//	}
	
//	private void crearFaseClasificacion(){
//		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CLASIFICACION,
//			Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
//	}
//	
//	private void crearHojaCalendarioFaseSF(){
//		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_SF,
//				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
//	}
	
//	private void crearHojaCalendarioFaseFC(){
//		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_FC,
//				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
//	}
	
//	private void crearHojaCalendarioFaseFF(){
//		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_FF,
//				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
//	}
	
	private void insertRow(String excel, String newRowData) {
		insertDataCsv(excel, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS, newRowData);
	}
	
	public void crearEstructuraHojas(EstructuraCampeonatoIndividualVeteranos2022 ecmi) {
		//Configuracion
		for (String row : ecmi.getInsertCabeceraConfiguracionRow()) {
			insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CONFIG, row);
		}
		//Fases
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_FASES, ecmi.getInsertCabeceraFasesRow());
		//Participantes
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_PARTICIPANTES, ecmi.getInsertCabeceraParticipantesRow());
	}

	private void rellenarDatosConfiguracion(EstructuraCampeonatoIndividualVeteranos2022 ecmi, Date fechaMaximaInscripcion, String estado, 
			String bolera) {
		//Actualizar la fecha
	    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy"); // date pattern
	    String fechaMaxInscripcion = simpleDateFormat.format(fechaMaximaInscripcion);
		updateRow(ecmi.getEstructuraFechaMaximaInscripcion(), ecmi.getEstructuraFechaMaximaInscripcion().getFila(), fechaMaxInscripcion);
		//Actualizar el estado
		updateRow(ecmi.getEstructuraEstadoCampeonato(), ecmi.getEstructuraEstadoCampeonato().getFila(), estado);
		updateRow(ecmi.getEstructuraBoleraFinal(), ecmi.getEstructuraBoleraFinal().getFila(), bolera);
	}

	private void rellenarDatosFases(EstructuraCampeonatoIndividualVeteranos2022 ecmi) {
		for(String fase : ecmi.getEstructuraTodasFases()){
			insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_FASES, fase);
		}
	}
	
	public void actualizarEstadoConfiguracion(String estado) {
		EstructuraCampeonatoIndividualVeteranos2022 ecmi = new EstructuraCampeonatoIndividualVeteranos2022();
		//Actualizar el estado
		updateRow(ecmi.getEstructuraEstadoCampeonato(), ecmi.getEstructuraEstadoCampeonato().getFila(), estado);
	}
	
	public void inscribirNewJugador(Jugadores2022 jugador, Categorias2022 categoria, String usuario, String activo) {
		EstructuraCampeonatoIndividualVeteranos2022 ecmi = new EstructuraCampeonatoIndividualVeteranos2022();
    	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss"); // date pattern
    	String fecha = simpleDateFormat.format(new Date());
    	
    	Long newRowNum = leerCampeonatoVeteranosIndividual.lastRowSheet(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_PARTICIPANTES, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
    	newRowNum++;
    	
	    String insert = newRowNum.toString() + ";" + jugador.getId().toString() + ";" + fecha + ";" + usuario + ";" + activo;
	    insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_PARTICIPANTES, insert);
	}
	
	public void inscribirUpdateJugador(ParticipantesIndividual2022 participantesIndividual, Jugadores2022 jugador, Categorias2022 categoria, String usuario, String activo) {
		EstructuraCampeonatoIndividualVeteranos2022 ecmi = new EstructuraCampeonatoIndividualVeteranos2022();
		
		Long fila = participantesIndividual.getRowNum();
		
		for (Estructura2022 eip : ecmi.getEstructuraInscribirParticipantes()) {
			
			// Cabeceras
			if (eip.getValor().equals(ecmi.COL_PARTICIPANTES_ID)) {
				updateRow(eip, fila, fila.toString());
			} else if (eip.getValor().equals(ecmi.COL_PARTICIPANTES_ID_JUGADOR)) {
				updateRow(eip, fila, jugador.getId().toString());
			} else if (eip.getValor().equals(ecmi.COL_PARTICIPANTES_USUARIO)) {
				updateRow(eip, fila, usuario);
			} else if (eip.getValor().equals(ecmi.COL_PARTICIPANTES_FECHA_INSCRIPCION)) {
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss"); // date pattern
			    String fecha = simpleDateFormat.format(new Date());
				updateRow(eip, fila, fecha);
			} else if (eip.getValor().equals(ecmi.COL_PARTICIPANTES_ACTIVO)) {
				updateRow(eip, fila, activo);
			}

		}
	}
	
	public void actualizarActivoFase(Fases2022 fase) {
		EstructuraCampeonatoIndividualVeteranos2022 ecmi = new EstructuraCampeonatoIndividualVeteranos2022();
		Long fila = fase.getRowNum();
		for (Estructura2022 ef : ecmi.getEstructuraFases()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_FASES_ACTIVO)) {
				updateRow(ef, fila, fase.getActivo());
			}
		}
	}
	
	public void actualizarFase(Fases2022 fase) {
		EstructuraCampeonatoIndividualVeteranos2022 ecmi = new EstructuraCampeonatoIndividualVeteranos2022();
		Long fila = fase.getRowNum();
		for (Estructura2022 ef : ecmi.getEstructuraFases()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_FASES_NUMERO_ENFRENTAMIENTOS_GRUPOS)) {
				updateRow(ef, fila, fase.getNumeroEnfrentamientos());
			}else if (ef.getValor().equals(ecmi.COL_FASES_NUMERO_PARTIDAS)) {
				updateRow(ef, fila, fase.getNumeroPartidas());
			}else if (ef.getValor().equals(ecmi.COL_FASES_NUMERO_JUEGOS)) {
				updateRow(ef, fila, fase.getNumeroJuegos());
			}else if (ef.getValor().equals(ecmi.COL_FASES_CLASIFICAN)) {
				updateRow(ef, fila, fase.getClasifican());
			}else if (ef.getValor().equals(ecmi.COL_FASES_FECHA)) {
				updateRow(ef, fila, fase.getFecha());
			}else if (ef.getValor().equals(ecmi.COL_FASES_HORA)) {
				updateRow(ef, fila, fase.getHora());
			}else if (ef.getValor().equals(ecmi.COL_FASES_FASE_SIGUIENTE)) {
				updateRow(ef, fila, fase.getFaseSiguiente());
			}
			
		}		
	}
	
	public void crearClasificacionBoladas(){
		EstructuraCampeonatoIndividualVeteranos2022 ecmi = new EstructuraCampeonatoIndividualVeteranos2022();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CLASIFICACION, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CLASIFICACION, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
		}else{
			crearHojaClasificacionBoladas();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CLASIFICACION, ecmi.getInsertCabeceraClasificacionRow());
	}
	
	public void crearClasificacionFaseI(){
		EstructuraCampeonatoIndividualVeteranos2022 ecmi = new EstructuraCampeonatoIndividualVeteranos2022();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CLA_FASE_I, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CLA_FASE_I, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
		}else{
			crearHojaClasificacionFaseI();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CLA_FASE_I, ecmi.getInsertCabeceraClasificacionFaseIRow());
	}

	public void crearCalendarioFaseI(){
		EstructuraCampeonatoIndividualVeteranos2022 ecmi = new EstructuraCampeonatoIndividualVeteranos2022();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_I, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_I, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
		}else{
			crearHojaCalendarioFaseI();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_I, ecmi.getInsertCabeceraCalendarioFaseIRow());
	}
	
	public void crearCalendarioFaseII(){
		EstructuraCampeonatoIndividualVeteranos2022 ecmi = new EstructuraCampeonatoIndividualVeteranos2022();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_II, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_II, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
		}else{
			crearHojaCalendarioFaseII();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_II, ecmi.getInsertCabeceraCalendarioFaseIIRow());
	}
	
	public void crearClasificacionFaseII(){
		EstructuraCampeonatoIndividualVeteranos2022 ecmi = new EstructuraCampeonatoIndividualVeteranos2022();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CLA_FASE_II, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CLA_FASE_II, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
		}else{
			crearHojaClasificacionFaseII();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CLA_FASE_II, ecmi.getInsertCabeceraClasificacionFaseIIRow());
	}
	
	public void crearCalendarioFaseOF(){
		EstructuraCampeonatoIndividualVeteranos2022 ecmi = new EstructuraCampeonatoIndividualVeteranos2022();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_OF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_OF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
		}else{
			crearHojaCalendarioFaseOF();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_OF, ecmi.getInsertCabeceraCalendarioFaseOFRow());
	}
	
	public void crearCalendarioFaseCF(){
		EstructuraCampeonatoIndividualVeteranos2022 ecmi = new EstructuraCampeonatoIndividualVeteranos2022();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_CF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_CF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
		}else{
			crearHojaCalendarioFaseCF();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_CF, ecmi.getInsertCabeceraCalendarioFaseCFRow());
	}
	
	public void crearCalendarioFaseSF(){
		EstructuraCampeonatoIndividualVeteranos2022 ecmi = new EstructuraCampeonatoIndividualVeteranos2022();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_SF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_SF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
		}else{
			crearHojaCalendarioFaseSF();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_SF, ecmi.getInsertCabeceraCalendarioFaseSFRow());
	}
	
	public void crearCalendarioFaseFC(){
		EstructuraCampeonatoIndividualVeteranos2022 ecmi = new EstructuraCampeonatoIndividualVeteranos2022();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_FC, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_FC, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
		}else{
			crearHojaCalendarioFaseFC();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_FC, ecmi.getInsertCabeceraCalendarioFaseFCRow());
	}
	
	public void crearCalendarioFaseFF(){
		EstructuraCampeonatoIndividualVeteranos2022 ecmi = new EstructuraCampeonatoIndividualVeteranos2022();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_FF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_FF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
		}else{
			crearHojaCalendarioFaseFF();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_FF, ecmi.getInsertCabeceraCalendarioFaseFFRow());
	}
	
	private void crearHojaClasificacionBoladas(){
		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CLASIFICACION,
				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
	}
	
	private void crearHojaClasificacionFaseI(){
		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CLA_FASE_I,
				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
	}
	
	private void crearHojaCalendarioFaseI(){
		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_I,
				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
	}
	
	private void crearHojaClasificacionFaseII(){
		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CLA_FASE_II,
				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
	}
	
	private void crearHojaCalendarioFaseII(){
		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_II,
				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
	}
	
	private void crearHojaCalendarioFaseOF(){
		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_OF,
				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
	}
	
	private void crearHojaCalendarioFaseCF(){
		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_CF,
				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
	}
	
	private void crearHojaCalendarioFaseSF(){
		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_SF,
				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
	}
	
	private void crearHojaCalendarioFaseFC(){
		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_FC,
				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
	}
	
	private void crearHojaCalendarioFaseFF(){
		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_FF,
				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
	}
	
	public void insertarRowClasificacionBoladas(String insert){
		EstructuraCampeonatoIndividualVeteranos2022 ecmi = new EstructuraCampeonatoIndividualVeteranos2022();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CLASIFICACION, insert);
	}
	
	public void insertarRowCalendarioFaseI(String insert){
		EstructuraCampeonatoIndividualVeteranos2022 ecmi = new EstructuraCampeonatoIndividualVeteranos2022();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_I, insert);
	}
	
	public void insertarRowClasificacionFaseI(String insert){
		EstructuraCampeonatoIndividualVeteranos2022 ecmi = new EstructuraCampeonatoIndividualVeteranos2022();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CLA_FASE_I, insert);
	}
	
	public void insertarRowCalendarioFaseOF(String insert){
		EstructuraCampeonatoIndividualVeteranos2022 ecmi = new EstructuraCampeonatoIndividualVeteranos2022();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_OF, insert);
	}
	
	public void insertarRowCalendarioFaseCF(String insert){
		EstructuraCampeonatoIndividualVeteranos2022 ecmi = new EstructuraCampeonatoIndividualVeteranos2022();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_CF, insert);
	}
	
	public void insertarRowCalendarioFaseSF(String insert){
		EstructuraCampeonatoIndividualVeteranos2022 ecmi = new EstructuraCampeonatoIndividualVeteranos2022();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_SF, insert);
	}
	
	public void insertarRowCalendarioFaseFC(String insert){
		EstructuraCampeonatoIndividualVeteranos2022 ecmi = new EstructuraCampeonatoIndividualVeteranos2022();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_FC, insert);
	}
	
	public void insertarRowCalendarioFaseFF(String insert){
		EstructuraCampeonatoIndividualVeteranos2022 ecmi = new EstructuraCampeonatoIndividualVeteranos2022();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_FF, insert);
	}
	
	public void borrarSorteo(){
		deleteFile(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CLASIFICACION, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
		deleteFile(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_I, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
		deleteFile(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CLA_FASE_I, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
		deleteFile(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_II, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
		deleteFile(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CLA_FASE_II, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
		deleteFile(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_OF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
		deleteFile(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_CF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
		deleteFile(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_SF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
		deleteFile(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_FC, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
		deleteFile(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS_CAL_FASE_FF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_VETERANOS);
		actualizarEstadoConfiguracion(EstadosIndividualParejas2022.CONFIG_ESTADO_CERRAR_INSCRIPCIONES);
	}	
	
}
