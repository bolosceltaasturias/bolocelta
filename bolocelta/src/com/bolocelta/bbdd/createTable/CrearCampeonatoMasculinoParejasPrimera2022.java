package com.bolocelta.bbdd.createTable;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.bolocelta.bbdd.constants.Activo2022;
import com.bolocelta.bbdd.constants.EstadosIndividualParejas2022;
import com.bolocelta.bbdd.constants.NombresTablas2022;
import com.bolocelta.bbdd.constants.Ubicaciones2022;
import com.bolocelta.bbdd.constants.structure.Estructura2022;
import com.bolocelta.bbdd.constants.structure.EstructuraCampeonatoMasculinoParejasPrimera2022;
import com.bolocelta.bbdd.readTables.LeerCampeonatoMasculinoParejasPrimera2022;
import com.bolocelta.entities.Categorias2022;
import com.bolocelta.entities.Fases2022;
import com.bolocelta.entities.Jugadores2022;
import com.bolocelta.entities.Parejas2022;
import com.bolocelta.entities.ParticipantesParejas2022;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseCF2022;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseFC2022;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseFF2022;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseI2022;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseOF2022;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseSF2022;
import com.bolocelta.entities.sorteos.parejas.ClasificacionFaseI2022;

public class CrearCampeonatoMasculinoParejasPrimera2022 extends ACrearModificar2022 {
	
	LeerCampeonatoMasculinoParejasPrimera2022 leerCampeonatoMasculinoParejasPrimera = new LeerCampeonatoMasculinoParejasPrimera2022();

	private void crearExcel() {

	}
	
	private void crearHojaConfiguracion(Date fechaMaximaInscripcion, String estado, String bolera){
		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CONFIG,
				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA);
	}
	
	private void crearHojaFases(){
		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_FASES,
				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA);
	}
	
	private void crearHojaParticipantes(){
		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_PARTICIPANTES,
				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA);
	}
	
	private void crearHojaGrupos(){
		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_GRUPOS,
				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA);
	}
	
	private void crearHojaClasificacionFaseI(){
		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CLA_FASE_I,
				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA);
	}
	
	private void crearHojaCalendarioFaseI(){
		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CAL_FASE_I,
				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA);
	}
	
	private void crearHojaClasificacionFaseII(){
		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CLA_FASE_II,
				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA);
	}
	
	private void crearHojaCalendarioFaseII(){
		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CAL_FASE_II,
				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA);
	}
	
	private void crearHojaCalendarioFaseOF(){
		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CAL_FASE_OF,
				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA);
	}
	
	private void crearHojaCalendarioFaseCF(){
		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CAL_FASE_CF,
				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA);
	}
	
	private void crearHojaCalendarioFaseSF(){
		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CAL_FASE_SF,
				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA);
	}
	
	private void crearHojaCalendarioFaseFC(){
		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CAL_FASE_FC,
				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA);
	}
	
	private void crearHojaCalendarioFaseFF(){
		createWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CAL_FASE_FF,
				Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA);
	}
	
	private void insertRow(String excel, String newRowData) {
		insertDataCsv(excel, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA, newRowData);
	}
	
	public void updateRow(Estructura2022 update, Long fila, Object newValue) {
		updateDataCsv(update.getExcel(), Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA, fila, update.getColumna(), newValue, update.getTipoDato());
	}
	
	
	//Creacion de la base del campeonato
	public void crearBaseCampeonato(Date fechaMaximaInscripcion, String estado, String bolera){
		//Crear las hojas excel
		crearExcel();
		crearHojaConfiguracion(fechaMaximaInscripcion, estado, bolera);
		crearHojaFases();
		crearHojaParticipantes();
		
		
		//Crear la estructura de las hojas (cabeceras y registros por defecto)
		EstructuraCampeonatoMasculinoParejasPrimera2022 ecmi = new EstructuraCampeonatoMasculinoParejasPrimera2022();
		//Crear las estructuras de las hojas
		crearEstructuraHojas(ecmi);
		//Rellenar datos de configuración
		rellenarDatosConfiguracion(ecmi, fechaMaximaInscripcion, estado, bolera);
		//Rellenar todas las fases
		rellenarDatosFases(ecmi);
	}
	
	public void crearEstructuraHojas(EstructuraCampeonatoMasculinoParejasPrimera2022 ecmi) {
		//Configuracion
		for (String row : ecmi.getInsertCabeceraConfiguracionRow()) {
			insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CONFIG, row);
		}
		//Fases
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_FASES, ecmi.getInsertCabeceraFasesRow());
		//Participantes
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_PARTICIPANTES, ecmi.getInsertCabeceraParticipantesRow());
	}

	private void rellenarDatosConfiguracion(EstructuraCampeonatoMasculinoParejasPrimera2022 ecmi, Date fechaMaximaInscripcion, String estado, 
			String bolera) {
		//Actualizar la fecha
	    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy"); // date pattern
	    String fechaMaxInscripcion = simpleDateFormat.format(fechaMaximaInscripcion);
		updateRow(ecmi.getEstructuraFechaMaximaInscripcion(), ecmi.getEstructuraFechaMaximaInscripcion().getFila(), fechaMaxInscripcion);
		//Actualizar el estado
		updateRow(ecmi.getEstructuraEstadoCampeonato(), ecmi.getEstructuraEstadoCampeonato().getFila(), estado);
		updateRow(ecmi.getEstructuraBoleraFinal(), ecmi.getEstructuraBoleraFinal().getFila(), bolera);
	}
	
	public void actualizarEstadoConfiguracion(String estado) {
		EstructuraCampeonatoMasculinoParejasPrimera2022 ecmi = new EstructuraCampeonatoMasculinoParejasPrimera2022();
		//Actualizar el estado
		updateRow(ecmi.getEstructuraEstadoCampeonato(), ecmi.getEstructuraEstadoCampeonato().getFila(), estado);
	}
	
	private void rellenarDatosFases(EstructuraCampeonatoMasculinoParejasPrimera2022 ecmi) {
		for(String fase : ecmi.getEstructuraTodasFases()){
			insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_FASES, fase);
		}
	}
	
	public void inscribirNewPareja(Parejas2022 pareja, Categorias2022 categoria, String usuario, String activo) {
		EstructuraCampeonatoMasculinoParejasPrimera2022 ecmi = new EstructuraCampeonatoMasculinoParejasPrimera2022();
    	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss"); // date pattern
    	String fecha = simpleDateFormat.format(new Date());
    	
    	Long newRowNum = leerCampeonatoMasculinoParejasPrimera.lastRowSheet(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_PARTICIPANTES, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA);
    	newRowNum++;
    	
	    String insert = newRowNum.toString() + ";" + pareja.getJugador1().getId().toString() + ";" + pareja.getJugador2().getId().toString() + ";" + fecha + ";" + usuario + ";" + activo;
	    insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_PARTICIPANTES, insert);
	}
	
	public void desinscribirPareja(ParticipantesParejas2022 pareja, String usuario, String activo) {
		EstructuraCampeonatoMasculinoParejasPrimera2022 ecmi = new EstructuraCampeonatoMasculinoParejasPrimera2022();
		
		Long fila = pareja.getRowNum();
		
		for (Estructura2022 eip : ecmi.getEstructuraInscribirParticipantes()) {
			
			// Cabeceras
			if (eip.getValor().equals(ecmi.COL_PARTICIPANTES_USUARIO)) {
				updateRow(eip, fila, usuario);
			} else if (eip.getValor().equals(ecmi.COL_PARTICIPANTES_FECHA_INSCRIPCION)) {
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss"); // date pattern
			    String fecha = simpleDateFormat.format(new Date());
				updateRow(eip, fila, fecha);
			} else if (eip.getValor().equals(ecmi.COL_PARTICIPANTES_ACTIVO)) {
				updateRow(eip, fila, activo);
			}

		}
	}
	
	
	public void actualizarActivoFase(Fases2022 fase) {
		EstructuraCampeonatoMasculinoParejasPrimera2022 ecmi = new EstructuraCampeonatoMasculinoParejasPrimera2022();
		Long fila = fase.getRowNum();
		for (Estructura2022 ef : ecmi.getEstructuraFases()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_FASES_ACTIVO)) {
				updateRow(ef, fila, fase.getActivo());
			}
		}
	}
	
	public void actualizarFase(Fases2022 fase) {
		EstructuraCampeonatoMasculinoParejasPrimera2022 ecmi = new EstructuraCampeonatoMasculinoParejasPrimera2022();
		Long fila = fase.getRowNum();
		for (Estructura2022 ef : ecmi.getEstructuraFases()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_FASES_NUMERO_ENFRENTAMIENTOS_GRUPOS)) {
				updateRow(ef, fila, fase.getNumeroEnfrentamientos());
			}else if (ef.getValor().equals(ecmi.COL_FASES_NUMERO_PARTIDAS)) {
				updateRow(ef, fila, fase.getNumeroPartidas());
			}else if (ef.getValor().equals(ecmi.COL_FASES_NUMERO_JUEGOS)) {
				updateRow(ef, fila, fase.getNumeroJuegos());
			}else if (ef.getValor().equals(ecmi.COL_FASES_CLASIFICAN)) {
				updateRow(ef, fila, fase.getClasifican());
			}else if (ef.getValor().equals(ecmi.COL_FASES_FECHA)) {
				updateRow(ef, fila, fase.getFecha());
			}else if (ef.getValor().equals(ecmi.COL_FASES_HORA)) {
				updateRow(ef, fila, fase.getHora());
			}else if (ef.getValor().equals(ecmi.COL_FASES_FASE_SIGUIENTE)) {
				updateRow(ef, fila, fase.getFaseSiguiente());
			}
			
		}		
	}
	
	public void crearClasificacionFaseI(){
		EstructuraCampeonatoMasculinoParejasPrimera2022 ecmi = new EstructuraCampeonatoMasculinoParejasPrimera2022();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CLA_FASE_I, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CLA_FASE_I, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA);
		}else{
			crearHojaClasificacionFaseI();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CLA_FASE_I, ecmi.getInsertCabeceraClasificacionFaseIRow());
	}
	
	public void insertarRowClasificacionFaseI(String insert){
		EstructuraCampeonatoMasculinoParejasPrimera2022 ecmi = new EstructuraCampeonatoMasculinoParejasPrimera2022();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CLA_FASE_I, insert);
	}
	
	public void crearCalendarioFaseI(){
		EstructuraCampeonatoMasculinoParejasPrimera2022 ecmi = new EstructuraCampeonatoMasculinoParejasPrimera2022();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CAL_FASE_I, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CAL_FASE_I, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA);
		}else{
			crearHojaCalendarioFaseI();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CAL_FASE_I, ecmi.getInsertCabeceraCalendarioFaseIRow());
	}
	
	public void insertarRowCalendarioFaseI(String insert){
		EstructuraCampeonatoMasculinoParejasPrimera2022 ecmi = new EstructuraCampeonatoMasculinoParejasPrimera2022();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CAL_FASE_I, insert);
	}
	
	public void actualizarResultadoCalendarioFaseI(CalendarioFaseI2022 calendarioFaseI, ClasificacionFaseI2022 clasificacionFaseIPareja1, 
			ClasificacionFaseI2022 clasificacionFaseIPareja2) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoParejasPrimera2022 ecmi = new EstructuraCampeonatoMasculinoParejasPrimera2022();
		Long fila = calendarioFaseI.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
		
		if(calendarioFaseI.isModificable()){
			for (Estructura2022 ef : ecmi.getEstructuraCalendarioFaseI()) {
				// Cabeceras
				if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_I_JUEGOS_PAREJA_1)) {
					updateRow(ef, fila, calendarioFaseI.getJuegosPareja1());
				}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_I_JUEGOS_PAREJA_2)) {
					updateRow(ef, fila, calendarioFaseI.getJuegosPareja2());
				}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_I_ACTIVO)) {
					calendarioFaseI.setActivo(Activo2022.NO);
					updateRow(ef, fila, calendarioFaseI.getActivo());
				}
			}
			fila = clasificacionFaseIPareja1.getRowNum();
			//Actualizar resultado en clasificacion Pareja 1
			for (Estructura2022 ef : ecmi.getEstructuraClasificacionFaseI()) {
				// Cabeceras
				if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PJ)) {
					clasificacionFaseIPareja1.setPj(clasificacionFaseIPareja1.getPj()+1);
					updateRow(ef, fila, clasificacionFaseIPareja1.getPj());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PG)) {
					if(calendarioFaseI.isGanaPareja1()){
						clasificacionFaseIPareja1.setPg(clasificacionFaseIPareja1.getPg()+1);
					}
					updateRow(ef, fila, clasificacionFaseIPareja1.getPg());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PE)) {
					if(calendarioFaseI.isEmpate()){
						clasificacionFaseIPareja1.setPe(clasificacionFaseIPareja1.getPe()+1);
					}
					updateRow(ef, fila, clasificacionFaseIPareja1.getPe());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PP)) {
					if(calendarioFaseI.isGanaPareja2()){
						clasificacionFaseIPareja1.setPp(clasificacionFaseIPareja1.getPp()+1);
					}
					updateRow(ef, fila, clasificacionFaseIPareja1.getPp());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PF)) {
					clasificacionFaseIPareja1.setPf(clasificacionFaseIPareja1.getPf()+calendarioFaseI.getJuegosPareja1());
					updateRow(ef, fila, clasificacionFaseIPareja1.getPf());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PC)) {
					clasificacionFaseIPareja1.setPc(clasificacionFaseIPareja1.getPc()+calendarioFaseI.getJuegosPareja2());
					updateRow(ef, fila, clasificacionFaseIPareja1.getPc());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PT)) {
					if(calendarioFaseI.isGanaPareja1()){
						clasificacionFaseIPareja1.setPt(clasificacionFaseIPareja1.getPt()+3);
					}else if(calendarioFaseI.isEmpate()){
						clasificacionFaseIPareja1.setPt(clasificacionFaseIPareja1.getPt()+1);
					}
					updateRow(ef, fila, clasificacionFaseIPareja1.getPt());
				}
			}
			fila = clasificacionFaseIPareja2.getRowNum();
			//Actualizar resultado en clasificacion Pareja 2
			for (Estructura2022 ef : ecmi.getEstructuraClasificacionFaseI()) {
				// Cabeceras
				if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PJ)) {
					clasificacionFaseIPareja2.setPj(clasificacionFaseIPareja2.getPj()+1);
					updateRow(ef, fila, clasificacionFaseIPareja2.getPj());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PG)) {
					if(calendarioFaseI.isGanaPareja2()){
						clasificacionFaseIPareja2.setPg(clasificacionFaseIPareja2.getPg()+1);
					}
					updateRow(ef, fila, clasificacionFaseIPareja2.getPg());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PE)) {
					if(calendarioFaseI.isEmpate()){
						clasificacionFaseIPareja2.setPe(clasificacionFaseIPareja2.getPe()+1);
					}
					updateRow(ef, fila, clasificacionFaseIPareja2.getPe());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PP)) {
					if(calendarioFaseI.isGanaPareja1()){
						clasificacionFaseIPareja2.setPp(clasificacionFaseIPareja2.getPp()+1);
					}
					updateRow(ef, fila, clasificacionFaseIPareja2.getPp());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PF)) {
					clasificacionFaseIPareja2.setPf(clasificacionFaseIPareja2.getPf()+calendarioFaseI.getJuegosPareja2());
					updateRow(ef, fila, clasificacionFaseIPareja2.getPf());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PC)) {
					clasificacionFaseIPareja2.setPc(clasificacionFaseIPareja2.getPc()+calendarioFaseI.getJuegosPareja1());
					updateRow(ef, fila, clasificacionFaseIPareja2.getPc());
				}else if (ef.getValor().equals(ecmi.COL_CLASIFICACION_FASE_I_PT)) {
					if(calendarioFaseI.isGanaPareja2()){
						clasificacionFaseIPareja2.setPt(clasificacionFaseIPareja2.getPt()+3);
					}else if(calendarioFaseI.isEmpate()){
						clasificacionFaseIPareja2.setPt(clasificacionFaseIPareja2.getPt()+1);
					}
					updateRow(ef, fila, clasificacionFaseIPareja2.getPt());
				}
			}
		}
		
		
	}
	
	public void crearCalendarioFaseII(){
		EstructuraCampeonatoMasculinoParejasPrimera2022 ecmi = new EstructuraCampeonatoMasculinoParejasPrimera2022();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CAL_FASE_II, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CAL_FASE_II, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA);
		}else{
			crearHojaCalendarioFaseII();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CAL_FASE_II, ecmi.getInsertCabeceraCalendarioFaseIIRow());
	}
	
	public void crearClasificacionFaseII(){
		EstructuraCampeonatoMasculinoParejasPrimera2022 ecmi = new EstructuraCampeonatoMasculinoParejasPrimera2022();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CLA_FASE_II, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CLA_FASE_II, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA);
		}else{
			crearHojaClasificacionFaseII();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CLA_FASE_II, ecmi.getInsertCabeceraClasificacionFaseIIRow());
	}
	
	public void crearCalendarioFaseOF(){
		EstructuraCampeonatoMasculinoParejasPrimera2022 ecmi = new EstructuraCampeonatoMasculinoParejasPrimera2022();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CAL_FASE_OF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CAL_FASE_OF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA);
		}else{
			crearHojaCalendarioFaseOF();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CAL_FASE_OF, ecmi.getInsertCabeceraCalendarioFaseOFRow());
	}
	
	public void insertarRowCalendarioFaseOF(String insert){
		EstructuraCampeonatoMasculinoParejasPrimera2022 ecmi = new EstructuraCampeonatoMasculinoParejasPrimera2022();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CAL_FASE_OF, insert);
	}
	
	public void actualizarParejaOctavosFinal(CalendarioFaseOF2022 calendarioFaseOF, boolean pareja1, boolean pareja2) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoParejasPrimera2022 ecmi = new EstructuraCampeonatoMasculinoParejasPrimera2022();
		Long fila = calendarioFaseOF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2022 ef : ecmi.getEstructuraCalendarioOctavosFinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_PAREJA_1) && pareja1) {
				updateRow(ef, fila, calendarioFaseOF.getPareja1Id());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_PAREJA_2) && pareja2) {
				updateRow(ef, fila, calendarioFaseOF.getPareja2Id());
			}
		}
		
		
	}
	
	public void actualizarParejaCuartosFinal(CalendarioFaseCF2022 calendarioFaseCF, boolean pareja1, boolean pareja2) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoParejasPrimera2022 ecmi = new EstructuraCampeonatoMasculinoParejasPrimera2022();
		Long fila = calendarioFaseCF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2022 ef : ecmi.getEstructuraCalendarioCuartosFinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_PAREJA_1) && pareja1) {
				updateRow(ef, fila, calendarioFaseCF.getPareja1Id());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_PAREJA_2) && pareja2) {
				updateRow(ef, fila, calendarioFaseCF.getPareja2Id());
			}
		}
		
		
	}
	
	public void actualizarParejaSemiFinal(CalendarioFaseSF2022 calendarioFaseSF, boolean pareja1, boolean pareja2) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoParejasPrimera2022 ecmi = new EstructuraCampeonatoMasculinoParejasPrimera2022();
		Long fila = calendarioFaseSF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2022 ef : ecmi.getEstructuraCalendarioSemifinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_PAREJA_1) && pareja1) {
				updateRow(ef, fila, calendarioFaseSF.getPareja1Id());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_PAREJA_2) && pareja2) {
				updateRow(ef, fila, calendarioFaseSF.getPareja2Id());
			}
		}
		
		
	}
	
	public void actualizarParejaFinalConsolacion(CalendarioFaseFC2022 calendarioFaseFC, boolean pareja1, boolean pareja2) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoParejasPrimera2022 ecmi = new EstructuraCampeonatoMasculinoParejasPrimera2022();
		Long fila = calendarioFaseFC.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2022 ef : ecmi.getEstructuraCalendarioFinalConsolacion()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_PAREJA_1) && pareja1) {
				updateRow(ef, fila, calendarioFaseFC.getPareja1Id());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_PAREJA_2) && pareja2) {
				updateRow(ef, fila, calendarioFaseFC.getPareja2Id());
			}
		}
		
		
	}
	
	public void actualizarParejaFinal(CalendarioFaseFF2022 calendarioFaseFF, boolean pareja1, boolean pareja2) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoParejasPrimera2022 ecmi = new EstructuraCampeonatoMasculinoParejasPrimera2022();
		Long fila = calendarioFaseFF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2022 ef : ecmi.getEstructuraCalendarioFinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_PAREJA_1) && pareja1) {
				updateRow(ef, fila, calendarioFaseFF.getPareja1Id());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_PAREJA_2) && pareja2) {
				updateRow(ef, fila, calendarioFaseFF.getPareja2Id());
			}
		}
		
		
	}
	
	public void actualizarResultadosEnfrentamientoDirectoOF(CalendarioFaseOF2022 calendarioFaseOF) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoParejasPrimera2022 ecmi = new EstructuraCampeonatoMasculinoParejasPrimera2022();
		Long fila = calendarioFaseOF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2022 ef : ecmi.getEstructuraCalendarioOctavosFinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_JUEGOS_PAREJA_1_P1)) {
				updateRow(ef, fila, calendarioFaseOF.getJuegosPareja1P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_JUEGOS_PAREJA_2_P1)) {
				updateRow(ef, fila, calendarioFaseOF.getJuegosPareja2P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_JUEGOS_PAREJA_1_P2)) {
				updateRow(ef, fila, calendarioFaseOF.getJuegosPareja1P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_JUEGOS_PAREJA_2_P2)) {
				updateRow(ef, fila, calendarioFaseOF.getJuegosPareja2P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_JUEGOS_PAREJA_1_P3)) {
				updateRow(ef, fila, calendarioFaseOF.getJuegosPareja1P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_JUEGOS_PAREJA_2_P3)) {
				updateRow(ef, fila, calendarioFaseOF.getJuegosPareja2P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_OF_ACTIVO)) {
				calendarioFaseOF.setActivo(Activo2022.NO);
				updateRow(ef, fila, calendarioFaseOF.getActivo());
			}
		}
		
		
	}
	
	public void crearCalendarioFaseCF(){
		EstructuraCampeonatoMasculinoParejasPrimera2022 ecmi = new EstructuraCampeonatoMasculinoParejasPrimera2022();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CAL_FASE_CF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CAL_FASE_CF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA);
		}else{
			crearHojaCalendarioFaseCF();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CAL_FASE_CF, ecmi.getInsertCabeceraCalendarioFaseCFRow());
	}
	
	public void insertarRowCalendarioFaseCF(String insert){
		EstructuraCampeonatoMasculinoParejasPrimera2022 ecmi = new EstructuraCampeonatoMasculinoParejasPrimera2022();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CAL_FASE_CF, insert);
	}
	
	public void actualizarResultadosEnfrentamientoDirectoCF(CalendarioFaseCF2022 calendarioFaseCF) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoParejasPrimera2022 ecmi = new EstructuraCampeonatoMasculinoParejasPrimera2022();
		Long fila = calendarioFaseCF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2022 ef : ecmi.getEstructuraCalendarioCuartosFinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_JUEGOS_PAREJA_1_P1)) {
				updateRow(ef, fila, calendarioFaseCF.getJuegosPareja1P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_JUEGOS_PAREJA_2_P1)) {
				updateRow(ef, fila, calendarioFaseCF.getJuegosPareja2P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_JUEGOS_PAREJA_1_P2)) {
				updateRow(ef, fila, calendarioFaseCF.getJuegosPareja1P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_JUEGOS_PAREJA_2_P2)) {
				updateRow(ef, fila, calendarioFaseCF.getJuegosPareja2P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_JUEGOS_PAREJA_1_P3)) {
				updateRow(ef, fila, calendarioFaseCF.getJuegosPareja1P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_JUEGOS_PAREJA_2_P3)) {
				updateRow(ef, fila, calendarioFaseCF.getJuegosPareja2P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_CF_ACTIVO)) {
				calendarioFaseCF.setActivo(Activo2022.NO);
				updateRow(ef, fila, calendarioFaseCF.getActivo());
			}
		}
		
		
	}
	
	public void crearCalendarioFaseSF(){
		EstructuraCampeonatoMasculinoParejasPrimera2022 ecmi = new EstructuraCampeonatoMasculinoParejasPrimera2022();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CAL_FASE_SF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CAL_FASE_SF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA);
		}else{
			crearHojaCalendarioFaseSF();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CAL_FASE_SF, ecmi.getInsertCabeceraCalendarioFaseSFRow());
	}
	
	public void insertarRowCalendarioFaseSF(String insert){
		EstructuraCampeonatoMasculinoParejasPrimera2022 ecmi = new EstructuraCampeonatoMasculinoParejasPrimera2022();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CAL_FASE_SF, insert);
	}
	
	public void actualizarResultadosEnfrentamientoDirectoSF(CalendarioFaseSF2022 calendarioFaseSF) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoParejasPrimera2022 ecmi = new EstructuraCampeonatoMasculinoParejasPrimera2022();
		Long fila = calendarioFaseSF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2022 ef : ecmi.getEstructuraCalendarioSemifinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_PAREJA_1_P1)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosPareja1P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_PAREJA_2_P1)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosPareja2P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_PAREJA_1_P2)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosPareja1P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_PAREJA_2_P2)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosPareja2P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_PAREJA_1_P3)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosPareja1P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_JUEGOS_PAREJA_2_P3)) {
				updateRow(ef, fila, calendarioFaseSF.getJuegosPareja2P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_SF_ACTIVO)) {
				calendarioFaseSF.setActivo(Activo2022.NO);
				updateRow(ef, fila, calendarioFaseSF.getActivo());
			}
		}
		
		
	}
	
	public void crearCalendarioFaseFC(){
		EstructuraCampeonatoMasculinoParejasPrimera2022 ecmi = new EstructuraCampeonatoMasculinoParejasPrimera2022();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CAL_FASE_FC, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CAL_FASE_FC, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA);
		}else{
			crearHojaCalendarioFaseFC();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CAL_FASE_FC, ecmi.getInsertCabeceraCalendarioFaseFCRow());
	}
	
	public void insertarRowCalendarioFaseFC(String insert){
		EstructuraCampeonatoMasculinoParejasPrimera2022 ecmi = new EstructuraCampeonatoMasculinoParejasPrimera2022();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CAL_FASE_FC, insert);
	}
	
	public void actualizarResultadosEnfrentamientoDirectoFC(CalendarioFaseFC2022 calendarioFaseFC) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoParejasPrimera2022 ecmi = new EstructuraCampeonatoMasculinoParejasPrimera2022();
		Long fila = calendarioFaseFC.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2022 ef : ecmi.getEstructuraCalendarioFinalConsolacion()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_JUEGOS_PAREJA_1_P1)) {
				updateRow(ef, fila, calendarioFaseFC.getJuegosPareja1P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_JUEGOS_PAREJA_2_P1)) {
				updateRow(ef, fila, calendarioFaseFC.getJuegosPareja2P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_JUEGOS_PAREJA_1_P2)) {
				updateRow(ef, fila, calendarioFaseFC.getJuegosPareja1P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_JUEGOS_PAREJA_2_P2)) {
				updateRow(ef, fila, calendarioFaseFC.getJuegosPareja2P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_JUEGOS_PAREJA_1_P3)) {
				updateRow(ef, fila, calendarioFaseFC.getJuegosPareja1P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_JUEGOS_PAREJA_2_P3)) {
				updateRow(ef, fila, calendarioFaseFC.getJuegosPareja2P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FC_ACTIVO)) {
				calendarioFaseFC.setActivo(Activo2022.NO);
				updateRow(ef, fila, calendarioFaseFC.getActivo());
			}
		}
		
		
	}
	
	public void crearCalendarioFaseFF(){
		EstructuraCampeonatoMasculinoParejasPrimera2022 ecmi = new EstructuraCampeonatoMasculinoParejasPrimera2022();
		boolean existFile = existFile(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CAL_FASE_FF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA);
		if(existFile){
			vaciarFile(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CAL_FASE_FF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA);
		}else{
			crearHojaCalendarioFaseFF();
		}
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CAL_FASE_FF, ecmi.getInsertCabeceraCalendarioFaseFFRow());
	}
	
	public void insertarRowCalendarioFaseFF(String insert){
		EstructuraCampeonatoMasculinoParejasPrimera2022 ecmi = new EstructuraCampeonatoMasculinoParejasPrimera2022();
		insertRow(ecmi.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CAL_FASE_FF, insert);
	}
	
	public void actualizarResultadosEnfrentamientoDirectoFF(CalendarioFaseFF2022 calendarioFaseFF) {
		//Actualizar resultado en calendario
		EstructuraCampeonatoMasculinoParejasPrimera2022 ecmi = new EstructuraCampeonatoMasculinoParejasPrimera2022();
		Long fila = calendarioFaseFF.getRowNum();
		
		//Si el calendario no esta activo no se hace nada
	
		for (Estructura2022 ef : ecmi.getEstructuraCalendarioFinal()) {
			// Cabeceras
			if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_PAREJA_1_P1)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosPareja1P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_PAREJA_2_P1)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosPareja2P1());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_PAREJA_1_P2)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosPareja1P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_PAREJA_2_P2)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosPareja2P2());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_PAREJA_1_P3)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosPareja1P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_JUEGOS_PAREJA_2_P3)) {
				updateRow(ef, fila, calendarioFaseFF.getJuegosPareja2P3());
			}else if (ef.getValor().equals(ecmi.COL_CALENDARIO_FASE_FF_ACTIVO)) {
				calendarioFaseFF.setActivo(Activo2022.NO);
				updateRow(ef, fila, calendarioFaseFF.getActivo());
			}
		}
		
		
	}
	
	public void borrarSorteo(){
		deleteFile(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CAL_FASE_I, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA);
		deleteFile(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CLA_FASE_I, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA);
		deleteFile(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CAL_FASE_II, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA);
		deleteFile(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CLA_FASE_II, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA);
		deleteFile(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CAL_FASE_OF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA);
		deleteFile(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CAL_FASE_CF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA);
		deleteFile(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CAL_FASE_SF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA);
		deleteFile(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CAL_FASE_FC, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA);
		deleteFile(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_PAREJAS_MASC_PRIMERA_CAL_FASE_FF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_PRIMERA);
		actualizarEstadoConfiguracion(EstadosIndividualParejas2022.CONFIG_ESTADO_CERRAR_INSCRIPCIONES);
	}
	
	

}
