package com.bolocelta.bbdd.createTable;

import com.bolocelta.bbdd.constants.NombresTablas2022;
import com.bolocelta.bbdd.constants.Ubicaciones2022;
import com.bolocelta.bbdd.constants.structure.Estructura2022;
import com.bolocelta.bbdd.constants.structure.EstructuraCampeonatoLigaFemenina2022;
import com.bolocelta.entities.CampeonatoLigaFemeninaCalendario2022;
import com.bolocelta.entities.CampeonatoLigaFemeninaClasificacion2022;

public class CrearCampeonatoLigaFemenina2022 extends ACrearModificar2022 {

	public void actualizarResultadoCalendarioLiga(CampeonatoLigaFemeninaCalendario2022 cec) {
		Long fila = cec.getRowNum();
		
		EstructuraCampeonatoLigaFemenina2022 eclf = new EstructuraCampeonatoLigaFemenina2022(
				NombresTablas2022.N_CSV_BBDD_CAMPEONATO_CAL_LIGA_FEMENINA,
				NombresTablas2022.N_CSV_BBDD_CAMPEONATO_CLA_LIGA_FEMENINA);
		for (Estructura2022 ef : eclf.getCalendarioList()) {
			if (ef.getValor().equals(eclf.COL_CALENDARIO_TIRADA)) {
				updateRowCalendario(ef, fila, cec.getTirada(), cec);
			}else if (ef.getValor().equals(eclf.COL_CALENDARIO_SACADA)) {
				updateRowCalendario(ef, fila, cec.getSacada(), cec);
			}else if (ef.getValor().equals(eclf.COL_CALENDARIO_PUNTOS)) {
				updateRowCalendario(ef, fila, cec.getPuntos(), cec);
			}
		}
	
		
	}

	public void updateRowCalendario(Estructura2022 update, Long fila, Object newValue, CampeonatoLigaFemeninaCalendario2022 cec) {
		updateDataCsv(update.getExcel(), Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_EQUIPOS, fila, update.getColumna(), newValue, update.getTipoDato());
	}
	
	public void actualizarClasificacion(CampeonatoLigaFemeninaClasificacion2022 cecCla, CampeonatoLigaFemeninaCalendario2022 cecCal) {
		
		Long filaCla = cecCla.getRowNum();
				
		if(cecCal.isModificable()){
		
			//Actualizar clasificacion de equipo 1
			EstructuraCampeonatoLigaFemenina2022 ecl1 = new EstructuraCampeonatoLigaFemenina2022(
					NombresTablas2022.N_CSV_BBDD_CAMPEONATO_CAL_LIGA_FEMENINA,
					NombresTablas2022.N_CSV_BBDD_CAMPEONATO_CLA_LIGA_FEMENINA);
			for (Estructura2022 ef : ecl1.getClasificacionList()) {
				if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_TIRADA)) {
					cecCla.setPuntosTirada(cecCla.getPuntosTirada()+cecCal.getTirada());
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosTirada(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS_SACADA)) {
					cecCla.setPuntosSacada(cecCla.getPuntosSacada()+cecCal.getSacada());
					updateRowClasificacion(ef, filaCla, cecCla.getPuntosSacada(), cecCla);
				}else if (ef.getValor().equals(ecl1.COL_CLASIFICACION_PUNTOS)) {
					cecCla.setPuntos(cecCla.getPuntos()+cecCal.getPuntos());
					updateRowClasificacion(ef, filaCla, cecCla.getPuntos(), cecCla);
				}
			}
		}
	}
	
	public void updateRowClasificacion(Estructura2022 update, Long fila, Object newValue, CampeonatoLigaFemeninaClasificacion2022 cec) {
		updateDataCsv(update.getExcel(), Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_EQUIPOS, fila, update.getColumna(), newValue, update.getTipoDato());
	}
	
	public void actualizarResultadoCalendarioLigaConfirmar(CampeonatoLigaFemeninaCalendario2022 cec) {
		Long fila = cec.getRowNum();
		EstructuraCampeonatoLigaFemenina2022 ecl1 = new EstructuraCampeonatoLigaFemenina2022(
				NombresTablas2022.N_CSV_BBDD_CAMPEONATO_CAL_LIGA_FEMENINA,
				NombresTablas2022.N_CSV_BBDD_CAMPEONATO_CLA_LIGA_FEMENINA);
		for (Estructura2022 ef : ecl1.getCalendarioList()) {
			if (ef.getValor().equals(ecl1.COL_CALENDARIO_ACTIVO)) {
				updateRowCalendario(ef, fila, cec.getActivo(), cec);
			}
		}
	}
	

}
