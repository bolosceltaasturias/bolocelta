package com.bolocelta.bbdd.createTable;

import com.bolocelta.bbdd.constants.NombresTablas2022;
import com.bolocelta.bbdd.constants.Ubicaciones2022;
import com.bolocelta.bbdd.constants.structure.Estructura2022;
import com.bolocelta.bbdd.constants.structure.EstructuraCampeonatoLigaInfantil2022;
import com.bolocelta.entities.CampeonatoLigaInfantilCalendario2022;

public class CrearCampeonatoLigaInfantil2022 extends ACrearModificar2022 {

	public void actualizarResultadoCalendarioLiga(CampeonatoLigaInfantilCalendario2022 cec) {
		Long fila = cec.getRowNum();
		
		EstructuraCampeonatoLigaInfantil2022 eclf = new EstructuraCampeonatoLigaInfantil2022(
				NombresTablas2022.N_CSV_BBDD_CAMPEONATO_CAL_LIGA_INFANTIL);
		for (Estructura2022 ef : eclf.getCalendarioList()) {
			if (ef.getValor().equals(eclf.COL_CALENDARIO_RONDA1)) {
				updateRowCalendario(ef, fila, cec.getRonda1(), cec);
			}else if (ef.getValor().equals(eclf.COL_CALENDARIO_RONDA2)) {
				updateRowCalendario(ef, fila, cec.getRonda2(), cec);
			}else if (ef.getValor().equals(eclf.COL_CALENDARIO_RONDA3)) {
				updateRowCalendario(ef, fila, cec.getRonda3(), cec);
			}else if (ef.getValor().equals(eclf.COL_CALENDARIO_RONDA4)) {
				updateRowCalendario(ef, fila, cec.getRonda4(), cec);
			}else if (ef.getValor().equals(eclf.COL_CALENDARIO_PUNTOS)) {
				updateRowCalendario(ef, fila, cec.getPuntos(), cec);
			}
		}
	
		
	}

	public void updateRowCalendario(Estructura2022 update, Long fila, Object newValue, CampeonatoLigaInfantilCalendario2022 cec) {
		updateDataCsv(update.getExcel(), Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_EQUIPOS, fila, update.getColumna(), newValue, update.getTipoDato());
	}
	
	public void actualizarResultadoCalendarioLigaConfirmar(CampeonatoLigaInfantilCalendario2022 cec) {
		Long fila = cec.getRowNum();
		EstructuraCampeonatoLigaInfantil2022 ecl1 = new EstructuraCampeonatoLigaInfantil2022(
				NombresTablas2022.N_CSV_BBDD_CAMPEONATO_CAL_LIGA_INFANTIL);
		for (Estructura2022 ef : ecl1.getCalendarioList()) {
			if (ef.getValor().equals(ecl1.COL_CALENDARIO_ACTIVO)) {
				updateRowCalendario(ef, fila, cec.getActivo(), cec);
			}
		}
	}
	

}
