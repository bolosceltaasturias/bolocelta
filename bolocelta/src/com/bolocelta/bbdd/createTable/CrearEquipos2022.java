package com.bolocelta.bbdd.createTable;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import com.bolocelta.bbdd.constants.Ubicaciones2022;
import com.bolocelta.bbdd.constants.structure.Estructura2022;
import com.bolocelta.bbdd.constants.structure.EstructuraEquipos2022;
import com.bolocelta.entities.Equipos2022;
import com.bolocelta.entities.Jugadores2022;

public class CrearEquipos2022 extends ACrearModificar2022 {
	
	public void preparateUpdateHorarioPreferente(Equipos2022 equipo, String colHorarioPreferente) {

		EstructuraEquipos2022 eq = new EstructuraEquipos2022();

		Long fila = equipo.getRowNum();

		for (Estructura2022 update : eq.getList()) {
			// Cabeceras
			if (update.getValor().equals(colHorarioPreferente) && update.getValor().equals(eq.COL_HORARIO_PSM)) {
				updateRow(update, fila, equipo.getHorarioPreferenteSabadoMaņana());
			}else if (update.getValor().equals(colHorarioPreferente) && update.getValor().equals(eq.COL_HORARIO_PST)) {
				updateRow(update, fila, equipo.getHorarioPreferenteSabadoTarde());
			}else if (update.getValor().equals(colHorarioPreferente) && update.getValor().equals(eq.COL_HORARIO_PDM)) {
				updateRow(update, fila, equipo.getHorarioPreferenteDomingoMaņana());
			}else if (update.getValor().equals(colHorarioPreferente) && update.getValor().equals(eq.COL_HORARIO_PDT)) {
				updateRow(update, fila, equipo.getHorarioPreferenteDomingoTarde());
			}
		}
	}

	public void preparateUpdateEmail(Equipos2022 equipo) {

		EstructuraEquipos2022 eq = new EstructuraEquipos2022();

		Long fila = equipo.getRowNum();

		for (Estructura2022 update : eq.getList()) {
			// Cabeceras
			if (update.getValor().equals(eq.COL_EMAIL)) {
				updateRow(update, fila, equipo.getEmail());
			}
		}
	}

	private void updateRow(Estructura2022 update, Long fila, Object newValue) {
		updateDataCsv(update.getExcel(), Ubicaciones2022.UBICACION_BBDD_ADMINISTRACION, fila, update.getColumna(), newValue, update.getTipoDato());
	}
	
	public boolean validateEmail(Equipos2022 equipo){
		boolean valid = true;
		if(equipo.getEmail() != null && !equipo.getEmail().isEmpty() && !emailValidator.validate(equipo.getEmail())){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El email no es valido", null));
			valid = false;
		}
		return valid;
	}
	
}
