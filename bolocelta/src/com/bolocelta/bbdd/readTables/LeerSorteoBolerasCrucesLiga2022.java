package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas2022;
import com.bolocelta.bbdd.constants.Ubicaciones2022;
import com.bolocelta.entities.sorteos.liga.SorteoBolerasCrucesLiga2022;
import com.bolocelta.transformer.SorteoBolerasCrucesLigaTransformer2022;

public class LeerSorteoBolerasCrucesLiga2022 extends ALeer2022 {

	private final String nameFile = NombresTablas2022.N_CSV_BBDD_SORTEO_LIGA_CALENDARIO_BOLERAS_CRUCE;
	private final String path = Ubicaciones2022.UBICACION_BBDD_ADMINISTRACION;
	
	HashMap<Integer, SorteoBolerasCrucesLiga2022> result = null;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, SorteoBolerasCrucesLiga2022> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(nameFile, path);
		if(csvParser != null){
			for (CSVRecord row : csvParser) {
		    	SorteoBolerasCrucesLiga2022 sorteoBolerasCrucesLiga = new SorteoBolerasCrucesLiga2022();
	    		sorteoBolerasCrucesLiga = SorteoBolerasCrucesLigaTransformer2022.transformerObjectCsv(sorteoBolerasCrucesLiga, row);
		    	result.put(sorteoBolerasCrucesLiga.getId(), sorteoBolerasCrucesLiga);
		    }
		}
		return result;
	}
	
	public Long getLastRowSheet(){
		return lastRowSheet(nameFile, path);
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public SorteoBolerasCrucesLiga2022 read(Integer id) {
		SorteoBolerasCrucesLiga2022 sorteoBolerasCrucesLiga = null;
		if(sorteoBolerasCrucesLiga == null){
			if(this.result == null){
				this.result = (HashMap<Integer, SorteoBolerasCrucesLiga2022>) readAll();
			}
			sorteoBolerasCrucesLiga = result.get(id);
		}
		return sorteoBolerasCrucesLiga;
	}
	
	public void refreshList(){
		this.result = null;
	}
	
	@SuppressWarnings("unchecked")
	public List<SorteoBolerasCrucesLiga2022> readCategoria(Integer id) {
		List<SorteoBolerasCrucesLiga2022> sorteoBolerasCrucesLigaByCategoria = new ArrayList<>();
		List<SorteoBolerasCrucesLiga2022> allSorteoBolerasCrucesLiga = (List<SorteoBolerasCrucesLiga2022>) listResult();
		for (SorteoBolerasCrucesLiga2022 sbcl : allSorteoBolerasCrucesLiga) {
			if(sbcl.getCategoria() != null && sbcl.getCategoria().equals(id)){
				sorteoBolerasCrucesLigaByCategoria.add(sbcl);
			}
		}
		return sorteoBolerasCrucesLigaByCategoria;
	}

	@Override
	public List<?> listResult() {
		List<SorteoBolerasCrucesLiga2022> listResult = new ArrayList<SorteoBolerasCrucesLiga2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    SorteoBolerasCrucesLiga2022 sorteoBolerasCrucesLiga = (SorteoBolerasCrucesLiga2022) value;
		    if(sorteoBolerasCrucesLiga.getId() != null && sorteoBolerasCrucesLiga.getId() > 0){
		    	listResult.add((SorteoBolerasCrucesLiga2022) sorteoBolerasCrucesLiga);
		    }
		}
		return listResult;
	}

}
