package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas2022;
import com.bolocelta.bbdd.constants.Ubicaciones2022;
import com.bolocelta.entities.CalendarioEspana2022;
import com.bolocelta.entities.CampeonatoEquiposClasificacion2022;
import com.bolocelta.transformer.CalendarioEspanaTransformer2022;

public class LeerCalendarioEspana2022 extends ALeer2022 {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, CalendarioEspana2022> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CALENDARIO_ESP, Ubicaciones2022.UBICACION_BBDD_ADMINISTRACION);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
	    		CalendarioEspana2022 calendarioEspana = new CalendarioEspana2022();
	    		calendarioEspana = CalendarioEspanaTransformer2022.transformerObjectCsv(calendarioEspana, row);
		    	result.put(calendarioEspana.getId(), calendarioEspana);
		    }
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public CalendarioEspana2022 read(Integer id) {
		HashMap<Integer, CalendarioEspana2022> result = (HashMap<Integer, CalendarioEspana2022>) readAll();
		return result.get(id);
	}

	@Override
	public List<?> listResult() {
		List<CalendarioEspana2022> listResult = new ArrayList<CalendarioEspana2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    CalendarioEspana2022 calendarioEspana = (CalendarioEspana2022) value;
		    if(calendarioEspana.getId() > 0){
		    	listResult.add((CalendarioEspana2022) calendarioEspana);
		    }
		}
		
		
		
		//Ordenar el calendario
		Collections.sort(listResult, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				CalendarioEspana2022 cec1 = (CalendarioEspana2022) o1;
				CalendarioEspana2022 cec2 = (CalendarioEspana2022) o2;
				
				int comp1 = cec1.getFechaDesde().compareTo(cec2.getFechaDesde());
				if (comp1 == 0) {
					int comp2 = cec1.getFechaHasta().compareTo(cec2.getFechaHasta());
					if (comp2 == 0) {
						return cec1.getId().compareTo(cec2.getId());
					}
					return comp2;
				}
				return comp1;
			}
		});
		
		
		
		
		return listResult;
	}

}
