package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas2022;
import com.bolocelta.bbdd.constants.Ubicaciones2022;
import com.bolocelta.entities.CampeonatoLigaFemeninaCalendario2022;
import com.bolocelta.entities.CampeonatoLigaInfantilCalendario2023;
import com.bolocelta.entities.CampeonatoLigaInfantilClasificacion2023;
import com.bolocelta.transformer.CampeonatoLigaInfantilTransformer2023;

public class LeerCampeonatoLigaInfantil2023 extends ALeer2022 {

	private LeerJugadores2022 leerJugadores = new LeerJugadores2022();
	private LeerBoleras2022 leerBoleras = new LeerBoleras2022();
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<Integer, ?> readAllCalendario() {
		HashMap<Integer, CampeonatoLigaInfantilCalendario2023> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_CAL_LIGA_INFANTIL_2023, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_EQUIPOS);
		if(csvParser != null){
			for (CSVRecord row : csvParser) {
				CampeonatoLigaInfantilCalendario2023 campeonatoLigaInfantilCalendario = new CampeonatoLigaInfantilCalendario2023();
		    	campeonatoLigaInfantilCalendario.setRowNum(row.getRecordNumber());
		    	campeonatoLigaInfantilCalendario = CampeonatoLigaInfantilTransformer2023.transformerObjectCalendarioCsv(campeonatoLigaInfantilCalendario, row);
		    	result.put(campeonatoLigaInfantilCalendario.getId(), campeonatoLigaInfantilCalendario);
		    }
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public CampeonatoLigaFemeninaCalendario2022 readCalendario(Integer id) {
		HashMap<Integer, CampeonatoLigaFemeninaCalendario2022> result = (HashMap<Integer, CampeonatoLigaFemeninaCalendario2022>) readAllCalendario();
		return result.get(id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object read(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HashMap<Integer, ?> readAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<?> listResult() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public List<?> listResultClasificacion(String categoria) {
		HashMap<String, String> mapJugadores = new HashMap<>();
		for (Entry<Integer, ?> entry : readAllCalendario().entrySet()) {
		    Object value = entry.getValue();
		    CampeonatoLigaInfantilCalendario2023 cec = (CampeonatoLigaInfantilCalendario2023) value;
		    if(categoria.equalsIgnoreCase(cec.getCategoria())){
			    if(!mapJugadores.containsKey(cec.getNombre())){
			    	mapJugadores.put(cec.getNombre(), cec.getNombre());
			    }
		    }
		}
		
		List<CampeonatoLigaInfantilClasificacion2023> listResult = new ArrayList<CampeonatoLigaInfantilClasificacion2023>();
		
		for (Entry<String, ?> entry : mapJugadores.entrySet()) {
		    Object value = entry.getValue();
		    String nombre = (String) value;
			
			
			CampeonatoLigaInfantilClasificacion2023 clic = new CampeonatoLigaInfantilClasificacion2023();
			
			for (Entry<Integer, ?> entry2 : readAllCalendario().entrySet()) {
			    Object valueList = entry2.getValue();
			    CampeonatoLigaInfantilCalendario2023 cec = (CampeonatoLigaInfantilCalendario2023) valueList;
			    if(nombre.equals(cec.getNombre())){
			    	clic.setNombre(cec.getNombre());
			    	clic.setCategoria(cec.getCategoria());
			    	clic.setPuntos(clic.getPuntos() + cec.getPuntos());
			    	clic.setTantos(clic.getTantos() + cec.getTotalTantos());
			    	if(cec.isContarFinal()){
				    	clic.setPuntosFinal(clic.getPuntosFinal() + cec.getPuntos());
				    	clic.setTantosFinal(clic.getTantosFinal() + cec.getTotalTantos());			    		
			    	}
			    }
			}
			
			listResult.add(clic);
		}

		//Ordenar la clasificacion
		Collections.sort(listResult, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				CampeonatoLigaInfantilClasificacion2023 cec1 = (CampeonatoLigaInfantilClasificacion2023) o1;
				CampeonatoLigaInfantilClasificacion2023 cec2 = (CampeonatoLigaInfantilClasificacion2023) o2;
				
				int rpuntos = cec1.getPuntosFinal().compareTo(cec2.getPuntosFinal());
				if (rpuntos == 0) {
					Integer part1 = cec1.getTantosFinal();
					Integer part2 = cec2.getTantosFinal();
					int rdifpartidas = part1.compareTo(part2);
					if (rdifpartidas == 0) {
						String nomb1 = cec1.getNombre();
						String nomb2 = cec2.getNombre();
						int rdifnomb = nomb1.compareTo(nomb2);
						return rdifnomb;
					}
					if(rdifpartidas == -1) return 1;
					if(rdifpartidas == 1) return -1;
					return rdifpartidas;
				}
				return rpuntos;
			}
		});
		
		
		
		return listResult;
	}
	
	public List<?> listResultCalendario() {
		List<CampeonatoLigaInfantilCalendario2023> listResult = new ArrayList<CampeonatoLigaInfantilCalendario2023>();
		for (Entry<Integer, ?> entry : readAllCalendario().entrySet()) {
		    Object value = entry.getValue();
		    CampeonatoLigaInfantilCalendario2023 cec = (CampeonatoLigaInfantilCalendario2023) value;
		    if(cec.getId() > 0){
	    		if(cec.getBoleraId() != null){
	    			cec.setBolera(leerBoleras.read(cec.getBoleraId()));
		    	}
	    		listResult.add((CampeonatoLigaInfantilCalendario2023) cec);
		    }
		}
		
		if(listResult != null && !listResult.isEmpty()){
		
			//Ordenar la clasificacion
			Collections.sort(listResult, new Comparator() {
				@Override
				public int compare(Object o1, Object o2) {
					CampeonatoLigaInfantilCalendario2023 cec1 = (CampeonatoLigaInfantilCalendario2023) o1;
					CampeonatoLigaInfantilCalendario2023 cec2 = (CampeonatoLigaInfantilCalendario2023) o2;
					
					int rjornada = cec1.getJornada().compareTo(cec2.getJornada());
					if (rjornada == 0) {

						String cat1 = cec1.getCategoria();
						String cat2 = cec2.getCategoria();
						
						int rdifcat = cat1.compareTo(cat2);
						if(rdifcat == 0){
							Integer punt1 = cec1.getPuntos();
							Integer punt2 = cec2.getPuntos();
							int puntDif = punt1.compareTo(punt2);
							if (puntDif == 0) {
								Integer tant1 = cec1.getTotalTantos();
								Integer tant2 = cec2.getTotalTantos();
								int tantDif = punt1.compareTo(punt2);
								if (puntDif == 0) {
									String nomb1 = cec1.getNombre();
									String nomb2 = cec2.getNombre();
									int rdifnomb = nomb1.compareTo(nomb2);
									return rdifnomb;
								}
								return tantDif;
							}
							return puntDif;
						}
						return rdifcat;
					}
					return rjornada;
					
				}
			});
		
		}
		
		return listResult;
	}

}
