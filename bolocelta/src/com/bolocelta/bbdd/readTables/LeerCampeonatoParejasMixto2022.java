package com.bolocelta.bbdd.readTables;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.application.enumerations.CategoriasEnumeration2022;
import com.bolocelta.bbdd.constants.Activo2022;
import com.bolocelta.bbdd.constants.EstadosIndividualParejas2022;
import com.bolocelta.bbdd.constants.FasesModelo2022;
import com.bolocelta.bbdd.constants.FasesTabShow2022;
import com.bolocelta.bbdd.constants.NombresTablas2022;
import com.bolocelta.bbdd.constants.Ubicaciones2022;
import com.bolocelta.entities.Configuracion2022;
import com.bolocelta.entities.Fases2022;
import com.bolocelta.entities.Parejas2022;
import com.bolocelta.entities.ParticipantesParejas2022;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseCF2022;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseFC2022;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseFF2022;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseI2022;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseOF2022;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseSF2022;
import com.bolocelta.entities.sorteos.parejas.ClasificacionFaseI2022;
import com.bolocelta.transformer.CalendarioParejasFaseCFTransformer2022;
import com.bolocelta.transformer.CalendarioParejasFaseFCTransformer2022;
import com.bolocelta.transformer.CalendarioParejasFaseFFTransformer2022;
import com.bolocelta.transformer.CalendarioParejasFaseITransformer2022;
import com.bolocelta.transformer.CalendarioParejasFaseOFTransformer2022;
import com.bolocelta.transformer.CalendarioParejasFaseSFTransformer2022;
import com.bolocelta.transformer.ClasificacionParejasFaseITransformer2022;
import com.bolocelta.transformer.ConfiguracionTransformer2022;
import com.bolocelta.transformer.FasesTransformer2022;
import com.bolocelta.transformer.ParticipantesTransformer2022;

public class LeerCampeonatoParejasMixto2022 extends ALeer2022 {
	
	private static final int CONFIG_FECHA_MAX_INSCRIPCION = 1;
	private static final int CONFIG_ESTADO = 2;
	private static final int CONFIG_BOLERA_FINAL = 3;
	private static final int CONFIG_OBSERVACIONES_CAMPEONATO_1 = 4;
	private static final int CONFIG_OBSERVACIONES_CAMPEONATO_2 = 5;
	private static final int CONFIG_OBSERVACIONES_CAMPEONATO_3 = 6;
	private static final int CONFIG_OBSERVACIONES_CAMPEONATO_4 = 7;
	private static final int CONFIG_OBSERVACIONES_CAMPEONATO_5 = 8;
	private static final int CONFIG_OBSERVACIONES_CAMPEONATO_6 = 9;
	private static final int CONFIG_OBSERVACIONES_CAMPEONATO_7 = 10;
	private static final int CONFIG_BOLERAS_OCUPADAS_FI = 11;
	private static final int CONFIG_BOLERAS_OCUPADAS_OFCF = 12;
	
	HashMap<Integer, Configuracion2022> resultConfiguracion = null;
	HashMap<Integer, Fases2022> resultFases = null;
	HashMap<Integer, ParticipantesParejas2022> resultParticipantes = null;
	HashMap<String, String> resultGruposFaseI = null;
	HashMap<String, ClasificacionFaseI2022> resultClasificacionFaseI = null;
	HashMap<String, CalendarioFaseI2022> resultCalendarioFaseI = null;
	HashMap<String, CalendarioFaseOF2022> resultCalendarioFaseOF = null;
	HashMap<String, CalendarioFaseCF2022> resultCalendarioFaseCF = null;
	HashMap<String, CalendarioFaseSF2022> resultCalendarioFaseSF = null;
	HashMap<String, CalendarioFaseFC2022> resultCalendarioFaseFC = null;
	HashMap<String, CalendarioFaseFF2022> resultCalendarioFaseFF = null;
	
	private LeerJugadores2022 leerJugadores = new LeerJugadores2022();
	private LeerBoleras2022 leerBoleras = new LeerBoleras2022();
	
	public LeerCampeonatoParejasMixto2022() {
		readConfig();
		readFases();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<Integer, ?> readConfig() {
		resultConfiguracion = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_PAREJAS_MIXTO_CONFIG, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_MIXTO);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	Configuracion2022 configuracion = new Configuracion2022();
		    	configuracion = ConfiguracionTransformer2022.transformerObjectCsv(configuracion, row);
		    	resultConfiguracion.put(configuracion.getId(), configuracion);
		    }
		}
		return resultConfiguracion;
	}
	
	@SuppressWarnings("unchecked")
	public Configuracion2022 readConfig(Integer id) {
		if(this.resultConfiguracion != null && id != null){
			return this.resultConfiguracion.get(id);
		}
		return null;
	}
	
	public List<?> listResultConfig() {
		List<Configuracion2022> listResult = new ArrayList<Configuracion2022>();
		for (Entry<Integer, ?> entry : readConfig().entrySet()) {
		    Object value = entry.getValue();
		    Configuracion2022 configuracion = (Configuracion2022) value;
		    if(configuracion.getId() > 0){
		    	listResult.add(configuracion);
		    }
		}
		return listResult;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<Integer, ?> readFases() {
		resultFases = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_PAREJAS_MIXTO_FASES, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_MIXTO);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	Fases2022 fases = new Fases2022();
		    	fases.setRowNum(row.getRecordNumber());
		    	fases = FasesTransformer2022.transformerObjectCsv(fases, row);
		    	resultFases.put(fases.getId(), fases);
		    }
		}
		return resultFases;
	}
	
	@SuppressWarnings("unchecked")
	public Fases2022 readFases(Integer id) {
		if(this.resultFases == null){
			resultFases = (HashMap<Integer, Fases2022>) readFases();
		}
		if(this.resultFases != null){
			return this.resultFases.get(id);
		}
		return null;
	}
	
	public List<?> listResultFases() {
		List<Fases2022> listResult = new ArrayList<Fases2022>();
		for (Entry<Integer, ?> entry : readFases().entrySet()) {
		    Object value = entry.getValue();
		    Fases2022 fases = (Fases2022) value;
		    if(fases.getId() > 0){
		    	listResult.add(fases);
		    }
		}
		return listResult;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<Integer, ?> readParticipantes() {
		resultParticipantes = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_PAREJAS_MIXTO_PARTICIPANTES, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_MIXTO);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	ParticipantesParejas2022 participantesParejas = new ParticipantesParejas2022();
		    	participantesParejas = ParticipantesTransformer2022.transformerObjectCsv(participantesParejas, row);
		    	participantesParejas.setRowNum(row.getRecordNumber());
		    	participantesParejas.getPareja().setJugador1(leerJugadores.read(participantesParejas.getPareja().getIdJugador1(), true));
		    	participantesParejas.getPareja().setJugador2(leerJugadores.read(participantesParejas.getPareja().getIdJugador2(), true));
		    	participantesParejas.setCategoria(CategoriasEnumeration2022.CATEGORIA_PRIMERA);
				resultParticipantes.put(participantesParejas.getPareja().getId(), participantesParejas);
		    }
		}
		return resultParticipantes;
	}
	
	@SuppressWarnings("unchecked")
	public ParticipantesParejas2022 readParticipante(Integer id) {
		if(this.resultParticipantes == null){
			resultParticipantes = (HashMap<Integer, ParticipantesParejas2022>) readParticipantes();
		}
		if(this.resultParticipantes != null){
			return this.resultParticipantes.get(id);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public ParticipantesParejas2022 existeParejaComoParticipante(Parejas2022 pareja) {
		List<ParticipantesParejas2022> listResult = (List<ParticipantesParejas2022>) listResultParticipantes();
		for (ParticipantesParejas2022 participantesParejas : listResult) {
			if(participantesParejas.getPareja().getIdJugador1().equals(pareja.getIdJugador1()) && participantesParejas.getPareja().getIdJugador2().equals(pareja.getIdJugador2()) && participantesParejas.getActivo().equals(Activo2022.SI)){
				return participantesParejas;
			}
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public ParticipantesParejas2022 existeJugadoresParejaComoParticipante(Parejas2022 pareja) {
		List<ParticipantesParejas2022> listResult = (List<ParticipantesParejas2022>) listResultParticipantes();
		for (ParticipantesParejas2022 participantesParejas : listResult) {
			if((participantesParejas.getPareja().getIdJugador1().equals(pareja.getIdJugador1()) || participantesParejas.getPareja().getIdJugador2().equals(pareja.getIdJugador2())) && participantesParejas.getActivo().equals(Activo2022.SI)){
				return participantesParejas;
			}
		}
		return null;
	}
	
	public List<?> listResultParticipantes() {
		List<ParticipantesParejas2022> listResult = new ArrayList<ParticipantesParejas2022>();
		for (Entry<Integer, ?> entry : readParticipantes().entrySet()) {
		    Object value = entry.getValue();
		    ParticipantesParejas2022 participantesParejas = (ParticipantesParejas2022) value;
		    if(participantesParejas.getPareja().getId() > 0){
		    	listResult.add(participantesParejas);
		    }
		}
		return listResult;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<String, ?> readClasificacionFaseI() {
		resultClasificacionFaseI = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_PAREJAS_MIXTO_CLA_FASE_I, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_MIXTO);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	ClasificacionFaseI2022 clasificacionFaseI = new ClasificacionFaseI2022();
		    	clasificacionFaseI.setRowNum(row.getRecordNumber());
		    	clasificacionFaseI = ClasificacionParejasFaseITransformer2022.transformerObjectCsv(clasificacionFaseI, row);
		    	ParticipantesParejas2022 participantesParejas = readParticipante(clasificacionFaseI.getParejaId());
		    	clasificacionFaseI.setPareja(participantesParejas.getPareja());
		    	resultClasificacionFaseI.put(clasificacionFaseI.getKey(), clasificacionFaseI);
		    }
		}
		return resultClasificacionFaseI;
	}
	
	@SuppressWarnings("unchecked")
	public ClasificacionFaseI2022 readClasificacionFaseI(Integer id) {
		if(this.resultClasificacionFaseI == null){
			resultClasificacionFaseI = (HashMap<String, ClasificacionFaseI2022>) readClasificacionFaseI();
		}
		if(this.resultClasificacionFaseI != null){
			return this.resultClasificacionFaseI.get(id);
		}
		return null;
	}
	
	public List<?> listResultClasificacionFaseI() {
		List<ClasificacionFaseI2022> listResult = new ArrayList<ClasificacionFaseI2022>();
		for (Entry<String, ?> entry : readClasificacionFaseI().entrySet()) {
		    Object value = entry.getValue();
		    ClasificacionFaseI2022 clasificacionFaseI = (ClasificacionFaseI2022) value;
		    if(clasificacionFaseI.getId() > 0){
		    	clasificacionFaseI.getPareja().setJugador1(leerJugadores.read(clasificacionFaseI.getPareja().getIdJugador1(), true));
		    	clasificacionFaseI.getPareja().setJugador2(leerJugadores.read(clasificacionFaseI.getPareja().getIdJugador2(), true));
		    	listResult.add(clasificacionFaseI);
		    }
		}
		return listResult;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<String, ?> readCalendarioFaseI() {
		resultCalendarioFaseI = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_PAREJAS_MIXTO_CAL_FASE_I, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_MIXTO);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	CalendarioFaseI2022 calendarioFaseI = new CalendarioFaseI2022();
		    	calendarioFaseI.setRowNum(row.getRecordNumber());
		    	calendarioFaseI = CalendarioParejasFaseITransformer2022.transformerObjectCsv(calendarioFaseI, row);
		    	calendarioFaseI.setPareja1(readParticipante(calendarioFaseI.getPareja1Id()).getPareja());
		    	calendarioFaseI.setPareja2(readParticipante(calendarioFaseI.getPareja2Id()).getPareja());
		    	resultCalendarioFaseI.put(calendarioFaseI.getKey(), calendarioFaseI);
		    }
		}
		return resultCalendarioFaseI;
	}
	
	@SuppressWarnings("unchecked")
	public CalendarioFaseI2022 readCalendarioFaseI(Integer id) {
		if(this.resultCalendarioFaseI == null){
			resultCalendarioFaseI = (HashMap<String, CalendarioFaseI2022>) readCalendarioFaseI();
		}
		if(this.resultCalendarioFaseI != null){
			return this.resultCalendarioFaseI.get(id);
		}
		return null;
	}
	
	public List<?> listResultCalendarioFaseI() {
		List<CalendarioFaseI2022> listResult = new ArrayList<CalendarioFaseI2022>();
		for (Entry<String, ?> entry : readCalendarioFaseI().entrySet()) {
		    Object value = entry.getValue();
		    CalendarioFaseI2022 calendarioFaseI = (CalendarioFaseI2022) value;
		    if(calendarioFaseI.getId() > 0){
		    	calendarioFaseI.setBolera(leerBoleras.read(calendarioFaseI.getBoleraId()));
		    	if(calendarioFaseI.getJuegosPareja1() != null && calendarioFaseI.getJuegosPareja2() != null){
		    		if(calendarioFaseI.getJuegosPareja1() == 0 && calendarioFaseI.getJuegosPareja2() == 0){
		    			calendarioFaseI.setRequipo1("/resources/sinjugar.ico");
		    			calendarioFaseI.setRequipo2("/resources/sinjugar.ico");
		    		}else if(calendarioFaseI.getJuegosPareja1() > calendarioFaseI.getJuegosPareja2()){
		    			calendarioFaseI.setRequipo1("/resources/ganador.ico");
		    			calendarioFaseI.setRequipo2("/resources/derrota.ico");
		    		}else if(calendarioFaseI.getJuegosPareja1() < calendarioFaseI.getJuegosPareja2()){
		    			calendarioFaseI.setRequipo1("/resources/derrota.ico");
		    			calendarioFaseI.setRequipo2("/resources/ganador.ico");
		    		}else if(calendarioFaseI.getJuegosPareja1().equals(calendarioFaseI.getJuegosPareja2())){
		    			calendarioFaseI.setRequipo1("/resources/empate.ico");
		    			calendarioFaseI.setRequipo2("/resources/empate.ico");
		    		}
		    		
		    	}else{
		    		calendarioFaseI.setRequipo1("/resources/sinjugar.ico");
		    		calendarioFaseI.setRequipo2("/resources/sinjugar.ico");
		    	}
		    	listResult.add(calendarioFaseI);
		    }
		}
		return listResult;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<String, ?> readCalendarioFaseOF() {
		resultCalendarioFaseOF = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_PAREJAS_MIXTO_CAL_FASE_OF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_MIXTO);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	CalendarioFaseOF2022 calendarioFaseOF = new CalendarioFaseOF2022();
		    	calendarioFaseOF.setRowNum(row.getRecordNumber());
		    	calendarioFaseOF = CalendarioParejasFaseOFTransformer2022.transformerObjectCsv(calendarioFaseOF, row);
		    	if(calendarioFaseOF.getPareja1Id() != null && calendarioFaseOF.getPareja1Id() != 0){
			    	ParticipantesParejas2022 participantesParejas1 = readParticipante(calendarioFaseOF.getPareja1Id());
			    	calendarioFaseOF.setPareja1(participantesParejas1.getPareja());
		    	}
		    	if(calendarioFaseOF.getPareja2Id() != null && calendarioFaseOF.getPareja2Id() != 0){
		    		ParticipantesParejas2022 participantesParejas2 = readParticipante(calendarioFaseOF.getPareja2Id());
		    		calendarioFaseOF.setPareja2(participantesParejas2.getPareja());
		    	}
		    	resultCalendarioFaseOF.put(calendarioFaseOF.getIdCruce(), calendarioFaseOF);
		    }
		}
		return resultCalendarioFaseOF;
	}
	
	@SuppressWarnings("unchecked")
	public CalendarioFaseOF2022 readCalendarioFaseOF(Integer id) {
		if(this.resultCalendarioFaseOF == null){
			resultCalendarioFaseOF = (HashMap<String, CalendarioFaseOF2022>) readCalendarioFaseOF();
		}
		if(this.resultCalendarioFaseOF != null){
			return this.resultCalendarioFaseOF.get(id);
		}
		return null;
	}
	
	public List<?> listResultCalendarioFaseOF() {
		List<CalendarioFaseOF2022> listResult = new ArrayList<CalendarioFaseOF2022>();
		for (Entry<String, ?> entry : readCalendarioFaseOF().entrySet()) {
		    Object value = entry.getValue();
		    CalendarioFaseOF2022 calendarioFaseOF = (CalendarioFaseOF2022) value;
		    if(calendarioFaseOF.getId() > 0){
		    	calendarioFaseOF.setBolera(leerBoleras.read(calendarioFaseOF.getBoleraId()));
		    	//Calcular icono victoria, empate y derrota
		    	Integer resultadoPartida1 =  calendarioFaseOF.getJuegosPareja1P1() - calendarioFaseOF.getJuegosPareja2P1();
		    	Integer resultadoPartida2 =  calendarioFaseOF.getJuegosPareja1P2() - calendarioFaseOF.getJuegosPareja2P2();
		    	Integer resultadoPartida3 =  calendarioFaseOF.getJuegosPareja1P3() - calendarioFaseOF.getJuegosPareja2P3();
		    	
		    	//Sin Jugar
		    	if(resultadoPartida1 == 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
	    			calendarioFaseOF.setRequipo1("/resources/sinjugar.ico");
	    			calendarioFaseOF.setRequipo2("/resources/sinjugar.ico");
		    	}else
		    	//Ganador 1 solo una partida
		    	if(resultadoPartida1 > 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
		    		calendarioFaseOF.setRequipo1("/resources/ganador.ico");
	    			calendarioFaseOF.setRequipo2("/resources/derrota.ico");
		    	}else 
		    	//Ganador 2 solo una partida
		    	if(resultadoPartida1 < 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
		    		calendarioFaseOF.setRequipo1("/resources/derrota.ico");
	    			calendarioFaseOF.setRequipo2("/resources/ganador.ico");
		    	}else 
		    	//Ganador 1 dos partidas
		    	if( (resultadoPartida1 > 0 && resultadoPartida2 > 0) ||
	    			(resultadoPartida1 > 0 && resultadoPartida3 > 0) ||
	    			(resultadoPartida2 > 0 && resultadoPartida3 > 0)){
		    		calendarioFaseOF.setRequipo1("/resources/ganador.ico");
	    			calendarioFaseOF.setRequipo2("/resources/derrota.ico");
		    	}else
		    	//Ganador 2 dos partidas
		    	if( (resultadoPartida1 < 0 && resultadoPartida2 < 0) ||
	    			(resultadoPartida1 < 0 && resultadoPartida3 < 0) ||
	    			(resultadoPartida2 < 0 && resultadoPartida3 < 0)){
		    		calendarioFaseOF.setRequipo1("/resources/derrota.ico");
	    			calendarioFaseOF.setRequipo2("/resources/ganador.ico");
		    	}else{
		    		calendarioFaseOF.setRequipo1("/resources/sinjugar.ico");
		    		calendarioFaseOF.setRequipo2("/resources/sinjugar.ico");
		    	}

		    	listResult.add(calendarioFaseOF);
		    }
		}
		return listResult;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<String, ?> readCalendarioFaseCF() {
		resultCalendarioFaseCF = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_PAREJAS_MIXTO_CAL_FASE_CF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_MIXTO);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	CalendarioFaseCF2022 calendarioFaseCF = new CalendarioFaseCF2022();
		    	calendarioFaseCF.setRowNum(row.getRecordNumber());
		    	calendarioFaseCF = CalendarioParejasFaseCFTransformer2022.transformerObjectCsv(calendarioFaseCF, row);
		    	if(calendarioFaseCF.getPareja1Id() != null && calendarioFaseCF.getPareja1Id() != 0){
			    	ParticipantesParejas2022 participantesParejas1 = readParticipante(calendarioFaseCF.getPareja1Id());
			    	calendarioFaseCF.setPareja1(participantesParejas1.getPareja());
		    	}
		    	if(calendarioFaseCF.getPareja2Id() != null && calendarioFaseCF.getPareja2Id() != 0){
		    		ParticipantesParejas2022 participantesParejas2 = readParticipante(calendarioFaseCF.getPareja2Id());
		    		calendarioFaseCF.setPareja2(participantesParejas2.getPareja());
		    	}
		    	resultCalendarioFaseCF.put(calendarioFaseCF.getIdCruce(), calendarioFaseCF);
		    }
		}
		return resultCalendarioFaseCF;
	}
	
	@SuppressWarnings("unchecked")
	public CalendarioFaseCF2022 readCalendarioFaseCF(Integer id) {
		if(this.resultCalendarioFaseCF == null){
			resultCalendarioFaseCF = (HashMap<String, CalendarioFaseCF2022>) readCalendarioFaseCF();
		}
		if(this.resultCalendarioFaseCF != null){
			return this.resultCalendarioFaseCF.get(id);
		}
		return null;
	}
	
	public List<?> listResultCalendarioFaseCF() {
		List<CalendarioFaseCF2022> listResult = new ArrayList<CalendarioFaseCF2022>();
		for (Entry<String, ?> entry : readCalendarioFaseCF().entrySet()) {
		    Object value = entry.getValue();
		    CalendarioFaseCF2022 calendarioFaseCF = (CalendarioFaseCF2022) value;
		    if(calendarioFaseCF.getId() > 0){
		    	calendarioFaseCF.setBolera(leerBoleras.read(calendarioFaseCF.getBoleraId()));
		    	//Calcular icono victoria, empate y derrota
		    	Integer resultadoPartida1 =  calendarioFaseCF.getJuegosPareja1P1() - calendarioFaseCF.getJuegosPareja2P1();
		    	Integer resultadoPartida2 =  calendarioFaseCF.getJuegosPareja1P2() - calendarioFaseCF.getJuegosPareja2P2();
		    	Integer resultadoPartida3 =  calendarioFaseCF.getJuegosPareja1P3() - calendarioFaseCF.getJuegosPareja2P3();
		    	
		    	//Sin Jugar
		    	if(resultadoPartida1 == 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
	    			calendarioFaseCF.setRequipo1("/resources/sinjugar.ico");
	    			calendarioFaseCF.setRequipo2("/resources/sinjugar.ico");
		    	}else
		    	//Ganador 1 solo una partida
		    	if(resultadoPartida1 > 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
		    		calendarioFaseCF.setRequipo1("/resources/ganador.ico");
	    			calendarioFaseCF.setRequipo2("/resources/derrota.ico");
		    	}else 
		    	//Ganador 2 solo una partida
		    	if(resultadoPartida1 < 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
		    		calendarioFaseCF.setRequipo1("/resources/derrota.ico");
	    			calendarioFaseCF.setRequipo2("/resources/ganador.ico");
		    	}else 
		    	//Ganador 1 dos partidas
		    	if( (resultadoPartida1 > 0 && resultadoPartida2 > 0) ||
	    			(resultadoPartida1 > 0 && resultadoPartida3 > 0) ||
	    			(resultadoPartida2 > 0 && resultadoPartida3 > 0)){
		    		calendarioFaseCF.setRequipo1("/resources/ganador.ico");
	    			calendarioFaseCF.setRequipo2("/resources/derrota.ico");
		    	}else
		    	//Ganador 2 dos partidas
		    	if( (resultadoPartida1 < 0 && resultadoPartida2 < 0) ||
	    			(resultadoPartida1 < 0 && resultadoPartida3 < 0) ||
	    			(resultadoPartida2 < 0 && resultadoPartida3 < 0)){
		    		calendarioFaseCF.setRequipo1("/resources/derrota.ico");
	    			calendarioFaseCF.setRequipo2("/resources/ganador.ico");
		    	}else{
		    		calendarioFaseCF.setRequipo1("/resources/sinjugar.ico");
		    		calendarioFaseCF.setRequipo2("/resources/sinjugar.ico");
		    	}

		    	listResult.add(calendarioFaseCF);
		    }
		}
		return listResult;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<String, ?> readCalendarioFaseSF() {
		resultCalendarioFaseSF = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_PAREJAS_MIXTO_CAL_FASE_SF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_MIXTO);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	CalendarioFaseSF2022 calendarioFaseSF = new CalendarioFaseSF2022();
		    	calendarioFaseSF.setRowNum(row.getRecordNumber());
		    	calendarioFaseSF = CalendarioParejasFaseSFTransformer2022.transformerObjectCsv(calendarioFaseSF, row);
		    	if(calendarioFaseSF.getPareja1Id() != null && calendarioFaseSF.getPareja1Id() != 0){
			    	ParticipantesParejas2022 participantesParejas1 = readParticipante(calendarioFaseSF.getPareja1Id());
			    	calendarioFaseSF.setPareja1(participantesParejas1.getPareja());
		    	}
		    	if(calendarioFaseSF.getPareja2Id() != null && calendarioFaseSF.getPareja2Id() != 0){
		    		ParticipantesParejas2022 participantesParejas2 = readParticipante(calendarioFaseSF.getPareja2Id());
		    		calendarioFaseSF.setPareja2(participantesParejas2.getPareja());
		    	}
		    	resultCalendarioFaseSF.put(calendarioFaseSF.getIdCruce(), calendarioFaseSF);
		    }
		}
		return resultCalendarioFaseSF;
	}
	
	@SuppressWarnings("unchecked")
	public CalendarioFaseSF2022 readCalendarioFaseSF(Integer id) {
		if(this.resultCalendarioFaseSF == null){
			resultCalendarioFaseSF = (HashMap<String, CalendarioFaseSF2022>) readCalendarioFaseSF();
		}
		if(this.resultCalendarioFaseSF != null){
			return this.resultCalendarioFaseSF.get(id);
		}
		return null;
	}
	
	public List<?> listResultCalendarioFaseSF() {
		List<CalendarioFaseSF2022> listResult = new ArrayList<CalendarioFaseSF2022>();
		for (Entry<String, ?> entry : readCalendarioFaseSF().entrySet()) {
		    Object value = entry.getValue();
		    CalendarioFaseSF2022 calendarioFaseSF = (CalendarioFaseSF2022) value;
		    if(calendarioFaseSF.getId() > 0){
		    	calendarioFaseSF.setBolera(leerBoleras.read(calendarioFaseSF.getBoleraId()));
		    	//Calcular icono victoria, empate y derrota
		    	Integer resultadoPartida1 =  calendarioFaseSF.getJuegosPareja1P1() - calendarioFaseSF.getJuegosPareja2P1();
		    	Integer resultadoPartida2 =  calendarioFaseSF.getJuegosPareja1P2() - calendarioFaseSF.getJuegosPareja2P2();
		    	Integer resultadoPartida3 =  calendarioFaseSF.getJuegosPareja1P3() - calendarioFaseSF.getJuegosPareja2P3();
		    	
		    	//Sin Jugar
		    	if(resultadoPartida1 == 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
	    			calendarioFaseSF.setRequipo1("/resources/sinjugar.ico");
	    			calendarioFaseSF.setRequipo2("/resources/sinjugar.ico");
		    	}else
		    	//Ganador 1 solo una partida
		    	if(resultadoPartida1 > 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
		    		calendarioFaseSF.setRequipo1("/resources/ganador.ico");
	    			calendarioFaseSF.setRequipo2("/resources/derrota.ico");
		    	}else 
		    	//Ganador 2 solo una partida
		    	if(resultadoPartida1 < 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
		    		calendarioFaseSF.setRequipo1("/resources/derrota.ico");
	    			calendarioFaseSF.setRequipo2("/resources/ganador.ico");
		    	}else 
		    	//Ganador 1 dos partidas
		    	if( (resultadoPartida1 > 0 && resultadoPartida2 > 0) ||
	    			(resultadoPartida1 > 0 && resultadoPartida3 > 0) ||
	    			(resultadoPartida2 > 0 && resultadoPartida3 > 0)){
		    		calendarioFaseSF.setRequipo1("/resources/ganador.ico");
	    			calendarioFaseSF.setRequipo2("/resources/derrota.ico");
		    	}else
		    	//Ganador 2 dos partidas
		    	if( (resultadoPartida1 < 0 && resultadoPartida2 < 0) ||
	    			(resultadoPartida1 < 0 && resultadoPartida3 < 0) ||
	    			(resultadoPartida2 < 0 && resultadoPartida3 < 0)){
		    		calendarioFaseSF.setRequipo1("/resources/derrota.ico");
	    			calendarioFaseSF.setRequipo2("/resources/ganador.ico");
		    	}else{
		    		calendarioFaseSF.setRequipo1("/resources/sinjugar.ico");
		    		calendarioFaseSF.setRequipo2("/resources/sinjugar.ico");
		    	}

		    	listResult.add(calendarioFaseSF);
		    }
		}
		return listResult;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<String, ?> readCalendarioFaseFC() {
		resultCalendarioFaseFC = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_PAREJAS_MIXTO_CAL_FASE_FC, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_MIXTO);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	CalendarioFaseFC2022 calendarioFaseFC = new CalendarioFaseFC2022();
		    	calendarioFaseFC.setRowNum(row.getRecordNumber());
		    	calendarioFaseFC = CalendarioParejasFaseFCTransformer2022.transformerObjectCsv(calendarioFaseFC, row);
		    	if(calendarioFaseFC.getPareja1Id() != null && calendarioFaseFC.getPareja1Id() != 0){
			    	ParticipantesParejas2022 participantesParejas1 = readParticipante(calendarioFaseFC.getPareja1Id());
			    	calendarioFaseFC.setPareja1(participantesParejas1.getPareja());
		    	}
		    	if(calendarioFaseFC.getPareja2Id() != null && calendarioFaseFC.getPareja2Id() != 0){
		    		ParticipantesParejas2022 participantesParejas2 = readParticipante(calendarioFaseFC.getPareja2Id());
		    		calendarioFaseFC.setPareja2(participantesParejas2.getPareja());
		    	}
		    	resultCalendarioFaseFC.put(calendarioFaseFC.getIdCruce(), calendarioFaseFC);
		    }
		}
		return resultCalendarioFaseFC;
	}
	
	@SuppressWarnings("unchecked")
	public CalendarioFaseFC2022 readCalendarioFaseFC(Integer id) {
		if(this.resultCalendarioFaseFC == null){
			resultCalendarioFaseFC = (HashMap<String, CalendarioFaseFC2022>) readCalendarioFaseFC();
		}
		if(this.resultCalendarioFaseFC != null){
			return this.resultCalendarioFaseFC.get(id);
		}
		return null;
	}
	
	public List<?> listResultCalendarioFaseFC() {
		List<CalendarioFaseFC2022> listResult = new ArrayList<CalendarioFaseFC2022>();
		for (Entry<String, ?> entry : readCalendarioFaseFC().entrySet()) {
		    Object value = entry.getValue();
		    CalendarioFaseFC2022 calendarioFaseFC = (CalendarioFaseFC2022) value;
		    if(calendarioFaseFC.getId() > 0){
		    	calendarioFaseFC.setBolera(leerBoleras.read(calendarioFaseFC.getBoleraId()));
		    	//Calcular icono victoria, empate y derrota
		    	Integer resultadoPartida1 =  calendarioFaseFC.getJuegosPareja1P1() - calendarioFaseFC.getJuegosPareja2P1();
		    	Integer resultadoPartida2 =  calendarioFaseFC.getJuegosPareja1P2() - calendarioFaseFC.getJuegosPareja2P2();
		    	Integer resultadoPartida3 =  calendarioFaseFC.getJuegosPareja1P3() - calendarioFaseFC.getJuegosPareja2P3();
		    	
		    	//Sin Jugar
		    	if(resultadoPartida1 == 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
	    			calendarioFaseFC.setRequipo1("/resources/sinjugar.ico");
	    			calendarioFaseFC.setRequipo2("/resources/sinjugar.ico");
		    	}else
		    	//Ganador 1 solo una partida
		    	if(resultadoPartida1 > 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
		    		calendarioFaseFC.setRequipo1("/resources/ganador.ico");
	    			calendarioFaseFC.setRequipo2("/resources/derrota.ico");
		    	}else 
		    	//Ganador 2 solo una partida
		    	if(resultadoPartida1 < 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
		    		calendarioFaseFC.setRequipo1("/resources/derrota.ico");
	    			calendarioFaseFC.setRequipo2("/resources/ganador.ico");
		    	}else 
		    	//Ganador 1 dos partidas
		    	if( (resultadoPartida1 > 0 && resultadoPartida2 > 0) ||
	    			(resultadoPartida1 > 0 && resultadoPartida3 > 0) ||
	    			(resultadoPartida2 > 0 && resultadoPartida3 > 0)){
		    		calendarioFaseFC.setRequipo1("/resources/ganador.ico");
	    			calendarioFaseFC.setRequipo2("/resources/derrota.ico");
		    	}else
		    	//Ganador 2 dos partidas
		    	if( (resultadoPartida1 < 0 && resultadoPartida2 < 0) ||
	    			(resultadoPartida1 < 0 && resultadoPartida3 < 0) ||
	    			(resultadoPartida2 < 0 && resultadoPartida3 < 0)){
		    		calendarioFaseFC.setRequipo1("/resources/derrota.ico");
	    			calendarioFaseFC.setRequipo2("/resources/ganador.ico");
		    	}else{
		    		calendarioFaseFC.setRequipo1("/resources/sinjugar.ico");
		    		calendarioFaseFC.setRequipo2("/resources/sinjugar.ico");
		    	}

		    	listResult.add(calendarioFaseFC);
		    }
		}
		return listResult;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<String, ?> readCalendarioFaseFF() {
		resultCalendarioFaseFF = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_PAREJAS_MIXTO_CAL_FASE_FF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_PAREJAS_MIXTO);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	CalendarioFaseFF2022 calendarioFaseFF = new CalendarioFaseFF2022();
		    	calendarioFaseFF.setRowNum(row.getRecordNumber());
		    	calendarioFaseFF = CalendarioParejasFaseFFTransformer2022.transformerObjectCsv(calendarioFaseFF, row);
		    	if(calendarioFaseFF.getPareja1Id() != null && calendarioFaseFF.getPareja1Id() != 0){
			    	ParticipantesParejas2022 participantesParejas1 = readParticipante(calendarioFaseFF.getPareja1Id());
			    	calendarioFaseFF.setPareja1(participantesParejas1.getPareja());
		    	}
		    	if(calendarioFaseFF.getPareja2Id() != null && calendarioFaseFF.getPareja2Id() != 0){
		    		ParticipantesParejas2022 participantesParejas2 = readParticipante(calendarioFaseFF.getPareja2Id());
		    		calendarioFaseFF.setPareja2(participantesParejas2.getPareja());
		    	}
		    	resultCalendarioFaseFF.put(calendarioFaseFF.getIdCruce(), calendarioFaseFF);
		    }
		}
		return resultCalendarioFaseFF;
	}
	
	@SuppressWarnings("unchecked")
	public CalendarioFaseFF2022 readCalendarioFaseFF(Integer id) {
		if(this.resultCalendarioFaseFF == null){
			resultCalendarioFaseFF = (HashMap<String, CalendarioFaseFF2022>) readCalendarioFaseFF();
		}
		if(this.resultCalendarioFaseFF != null){
			return this.resultCalendarioFaseFF.get(id);
		}
		return null;
	}
	
	public List<?> listResultCalendarioFaseFF() {
		List<CalendarioFaseFF2022> listResult = new ArrayList<CalendarioFaseFF2022>();
		for (Entry<String, ?> entry : readCalendarioFaseFF().entrySet()) {
		    Object value = entry.getValue();
		    CalendarioFaseFF2022 calendarioFaseFF = (CalendarioFaseFF2022) value;
		    if(calendarioFaseFF.getId() > 0){
		    	calendarioFaseFF.setBolera(leerBoleras.read(calendarioFaseFF.getBoleraId()));
		    	//Calcular icono victoria, empate y derrota
		    	Integer resultadoPartida1 =  calendarioFaseFF.getJuegosPareja1P1() - calendarioFaseFF.getJuegosPareja2P1();
		    	Integer resultadoPartida2 =  calendarioFaseFF.getJuegosPareja1P2() - calendarioFaseFF.getJuegosPareja2P2();
		    	Integer resultadoPartida3 =  calendarioFaseFF.getJuegosPareja1P3() - calendarioFaseFF.getJuegosPareja2P3();
		    	
		    	//Sin Jugar
		    	if(resultadoPartida1 == 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
	    			calendarioFaseFF.setRequipo1("/resources/sinjugar.ico");
	    			calendarioFaseFF.setRequipo2("/resources/sinjugar.ico");
		    	}else
		    	//Ganador 1 solo una partida
		    	if(resultadoPartida1 > 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
		    		calendarioFaseFF.setRequipo1("/resources/ganador.ico");
	    			calendarioFaseFF.setRequipo2("/resources/derrota.ico");
		    	}else 
		    	//Ganador 2 solo una partida
		    	if(resultadoPartida1 < 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
		    		calendarioFaseFF.setRequipo1("/resources/derrota.ico");
	    			calendarioFaseFF.setRequipo2("/resources/ganador.ico");
		    	}else 
		    	//Ganador 1 dos partidas
		    	if( (resultadoPartida1 > 0 && resultadoPartida2 > 0) ||
	    			(resultadoPartida1 > 0 && resultadoPartida3 > 0) ||
	    			(resultadoPartida2 > 0 && resultadoPartida3 > 0)){
		    		calendarioFaseFF.setRequipo1("/resources/ganador.ico");
	    			calendarioFaseFF.setRequipo2("/resources/derrota.ico");
		    	}else
		    	//Ganador 2 dos partidas
		    	if( (resultadoPartida1 < 0 && resultadoPartida2 < 0) ||
	    			(resultadoPartida1 < 0 && resultadoPartida3 < 0) ||
	    			(resultadoPartida2 < 0 && resultadoPartida3 < 0)){
		    		calendarioFaseFF.setRequipo1("/resources/derrota.ico");
	    			calendarioFaseFF.setRequipo2("/resources/ganador.ico");
		    	}else{
		    		calendarioFaseFF.setRequipo1("/resources/sinjugar.ico");
		    		calendarioFaseFF.setRequipo2("/resources/sinjugar.ico");
		    	}

		    	listResult.add(calendarioFaseFF);
		    }
		}
		return listResult;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<String, ?> readGruposFaseI() {
		resultGruposFaseI = new HashMap();
		List<ClasificacionFaseI2022> listResult = (List<ClasificacionFaseI2022>) listResultClasificacionFaseI();
		for (ClasificacionFaseI2022 clasificacionFaseI : listResult) {
			if(!resultGruposFaseI.containsKey(clasificacionFaseI.getGrupo())){
				resultGruposFaseI.put(clasificacionFaseI.getGrupo(), clasificacionFaseI.getGrupo());
			}
		}
		return resultGruposFaseI;
	}
	
	@SuppressWarnings("unchecked")
	public String readGrupoFaseI(String id) {
		if(this.resultGruposFaseI == null){
			resultGruposFaseI = (HashMap<String, String>) readGruposFaseI();
		}
		if(this.resultGruposFaseI != null){
			return this.resultGruposFaseI.get(id);
		}
		return null;
	}
	
	public List<FasesTabShow2022> listResultGruposFaseI() {
		List<FasesTabShow2022> listResult = new ArrayList<FasesTabShow2022>();
		for (Entry<String, ?> entry : readGruposFaseI().entrySet()) {
		    Object value = entry.getValue();
		    String grupo = (String) value;
		    if(grupo != null){
		    	listResult.add(FasesTabShow2022.searchFaseTabShow(FasesModelo2022.FASE_I, grupo));
		    }
		}
		return listResult;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public Object read(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HashMap<Integer, ?> readAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<?> listResult() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public boolean isEstadoCampeonatoSinCrear(){
		Configuracion2022 configuracionFechaMaxInscripcion = readConfig(CONFIG_FECHA_MAX_INSCRIPCION);
		if(configuracionFechaMaxInscripcion == null){
			return true;
		}
		return false;
	}
	
	public boolean isAbrirInscripciones(){
		Configuracion2022 configuracionFechaMaxInscripcion = readConfig(CONFIG_FECHA_MAX_INSCRIPCION);
		if(configuracionFechaMaxInscripcion != null){
			Configuracion2022 configuracionEstado = readConfig(CONFIG_ESTADO);
			if(configuracionEstado != null && configuracionEstado.getValor() != null && configuracionEstado.getValor().equals(EstadosIndividualParejas2022.CONFIG_ESTADO_CREAR)){
				return true;
			}
		}
		return false;
	}
	
	public boolean isCerrarInscripciones(){
		Configuracion2022 configuracionFechaMaxInscripcion = readConfig(CONFIG_FECHA_MAX_INSCRIPCION);
		if(configuracionFechaMaxInscripcion != null){
			Configuracion2022 configuracionEstado = readConfig(CONFIG_ESTADO);
			if(configuracionEstado != null && configuracionEstado.getValor() != null && configuracionEstado.getValor().equals(EstadosIndividualParejas2022.CONFIG_ESTADO_ABRIR_INSCRIPCIONES)){
				return true;
			}
		}
		return false;
	}
	
	public boolean isInscripcionesCerradas(){
		Configuracion2022 configuracionFechaMaxInscripcion = readConfig(CONFIG_FECHA_MAX_INSCRIPCION);
		if(configuracionFechaMaxInscripcion != null){
			Configuracion2022 configuracionEstado = readConfig(CONFIG_ESTADO);
			if(configuracionEstado != null && configuracionEstado.getValor() != null && configuracionEstado.getValor().equals(EstadosIndividualParejas2022.CONFIG_ESTADO_CERRAR_INSCRIPCIONES)){
				return true;
			}
		}
		return false;
	}
	
	public boolean isFasesCerradas(){
		Configuracion2022 configuracionFechaMaxInscripcion = readConfig(CONFIG_FECHA_MAX_INSCRIPCION);
		if(configuracionFechaMaxInscripcion != null){
			Configuracion2022 configuracionEstado = readConfig(CONFIG_ESTADO);
			if(configuracionEstado != null && configuracionEstado.getValor() != null && configuracionEstado.getValor().equals(EstadosIndividualParejas2022.CONFIG_ESTADO_CERRAR_FASES)){
				return true;
			}
		}
		return false;
	}
	
	public boolean isSorteoRealizado(){
		Configuracion2022 configuracionFechaMaxInscripcion = readConfig(CONFIG_FECHA_MAX_INSCRIPCION);
		if(configuracionFechaMaxInscripcion != null){
			Configuracion2022 configuracionEstado = readConfig(CONFIG_ESTADO);
			if(configuracionEstado != null && configuracionEstado.getValor() != null && configuracionEstado.getValor().equals(EstadosIndividualParejas2022.CONFIG_ESTADO_SORTEADO_CAMPEONATO)){
				return true;
			}
		}
		return false;
	}
	
	public boolean isSorteoFinalizado(){
		Configuracion2022 configuracionFechaMaxInscripcion = readConfig(CONFIG_FECHA_MAX_INSCRIPCION);
		if(configuracionFechaMaxInscripcion != null){
			Configuracion2022 configuracionEstado = readConfig(CONFIG_ESTADO);
			if(configuracionEstado != null && configuracionEstado.getValor() != null && configuracionEstado.getValor().equals(EstadosIndividualParejas2022.CONFIG_ESTADO_FINALIZADO)){
				return true;
			}
		}
		return false;
	}
	
	public String getEstadoCampeonato(){
		Configuracion2022 configuracionEstado = readConfig(CONFIG_ESTADO);
		if(configuracionEstado != null && configuracionEstado.getValor().equals(EstadosIndividualParejas2022.CONFIG_ESTADO_CREAR)){
			return "Creado";
		}else if(configuracionEstado != null && configuracionEstado.getValor().equals(EstadosIndividualParejas2022.CONFIG_ESTADO_ABRIR_INSCRIPCIONES)){
			return "Inscripciones abiertas";
		}else if(configuracionEstado != null && configuracionEstado.getValor().equals(EstadosIndividualParejas2022.CONFIG_ESTADO_CERRAR_INSCRIPCIONES)){
			return "Inscripciones cerradas";
		}else if(configuracionEstado != null && configuracionEstado.getValor().equals(EstadosIndividualParejas2022.CONFIG_ESTADO_CERRAR_FASES)){
			return "Fases cerradas";
		}else if(configuracionEstado != null && configuracionEstado.getValor().equals(EstadosIndividualParejas2022.CONFIG_ESTADO_SORTEADO_CAMPEONATO)){
			return "Campeonato sorteado";
		}else if(configuracionEstado != null && configuracionEstado.getValor().equals(EstadosIndividualParejas2022.CONFIG_ESTADO_FINALIZADO)){
			return "Finalizado";
		}
		return "Sin crear";
	}
	
	public String getBoleraFinal(){
		Configuracion2022 configuracionEstado = readConfig(CONFIG_BOLERA_FINAL);
		if(configuracionEstado != null){
			return configuracionEstado.getValor();
		}
		return "";
	}
	
	public String getObservacionesCampeonato1(){
		Configuracion2022 configuracionEstado = readConfig(CONFIG_OBSERVACIONES_CAMPEONATO_1);
		if(configuracionEstado != null){
			return configuracionEstado.getValor();
		}
		return "";
	}

	public String getObservacionesCampeonato2(){
		Configuracion2022 configuracionEstado = readConfig(CONFIG_OBSERVACIONES_CAMPEONATO_2);
		if(configuracionEstado != null){
			return configuracionEstado.getValor();
		}
		return "";
	}
	
	public String getObservacionesCampeonato3(){
		Configuracion2022 configuracionEstado = readConfig(CONFIG_OBSERVACIONES_CAMPEONATO_3);
		if(configuracionEstado != null){
			return configuracionEstado.getValor();
		}
		return "";
	}
	
	public String getObservacionesCampeonato4(){
		Configuracion2022 configuracionEstado = readConfig(CONFIG_OBSERVACIONES_CAMPEONATO_4);
		if(configuracionEstado != null){
			return configuracionEstado.getValor();
		}
		return "";
	}
	
	public String getObservacionesCampeonato5(){
		Configuracion2022 configuracionEstado = readConfig(CONFIG_OBSERVACIONES_CAMPEONATO_5);
		if(configuracionEstado != null){
			return configuracionEstado.getValor();
		}
		return "";
	}
	
	public String getObservacionesCampeonato6(){
		Configuracion2022 configuracionEstado = readConfig(CONFIG_OBSERVACIONES_CAMPEONATO_6);
		if(configuracionEstado != null){
			return configuracionEstado.getValor();
		}
		return "";
	}
	
	public String getObservacionesCampeonato7(){
		Configuracion2022 configuracionEstado = readConfig(CONFIG_OBSERVACIONES_CAMPEONATO_7);
		if(configuracionEstado != null){
			return configuracionEstado.getValor();
		}
		return "";
	}
	
	public String getBolerasOcupadasFI(){
		Configuracion2022 configuracionEstado = readConfig(CONFIG_BOLERAS_OCUPADAS_FI);
		if(configuracionEstado != null){
			return configuracionEstado.getValor();
		}
		return "";
	}
	
	public String getBolerasOcupadasOFCF(){
		Configuracion2022 configuracionEstado = readConfig(CONFIG_BOLERAS_OCUPADAS_OFCF);
		if(configuracionEstado != null){
			return configuracionEstado.getValor();
		}
		return "";
	}
	
	
	public Date getFechaMaxInscripcion(){
		Configuracion2022 configuracionFechaMaxInscripcion = readConfig(CONFIG_FECHA_MAX_INSCRIPCION);
		if(configuracionFechaMaxInscripcion != null && configuracionFechaMaxInscripcion.getValor() != null){
		    String date = configuracionFechaMaxInscripcion.getValor();
		    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy"); // date pattern
		    try {
				return simpleDateFormat.parse(date);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
			} 
		}
		return null;
	}
	
	public List<?> listResultParticipantesOrderByEquipo() {
		List<ParticipantesParejas2022> listResult = new ArrayList<ParticipantesParejas2022>();
		for (Entry<Integer, ?> entry : readParticipantes().entrySet()) {
		    Object value = entry.getValue();
		    ParticipantesParejas2022 participantesParejas = (ParticipantesParejas2022) value;
		    if(participantesParejas.getPareja().getId() > 0 && participantesParejas.getActivo().equals(Activo2022.SI)){
		    	listResult.add(participantesParejas);
		    }
		}
		
		//Ordenar la clasificacion
		Collections.sort(listResult, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				ParticipantesParejas2022 pi1 = (ParticipantesParejas2022) o1;
				ParticipantesParejas2022 pi2 = (ParticipantesParejas2022) o2;
				
				int requipo = pi1.getPareja().getJugador1().getEquipoId().compareTo(pi2.getPareja().getJugador1().getEquipoId());
				return requipo;
			}
		});
		
		return listResult;
	}
	


}
