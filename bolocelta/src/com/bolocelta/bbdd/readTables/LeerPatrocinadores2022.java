package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.inject.Named;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas2022;
import com.bolocelta.bbdd.constants.Ubicaciones2022;
import com.bolocelta.entities.Patrocinadores2022;
import com.bolocelta.transformer.PatrocinadoresTransformer2022;

@Named
public class LeerPatrocinadores2022 extends ALeer2022 {
	
	HashMap<Integer, Patrocinadores2022> result = null;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, Patrocinadores2022> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_PATROCINADORES, Ubicaciones2022.UBICACION_BBDD_ADMINISTRACION);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	Patrocinadores2022 patrocinador = new Patrocinadores2022();
		    	patrocinador = PatrocinadoresTransformer2022.transformerObjectCsv(patrocinador, row);
		    	result.put(patrocinador.getId(), patrocinador);
		    }
		}
	    return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Patrocinadores2022 read(Integer id) {
		if(this.result == null){
			result = (HashMap<Integer, Patrocinadores2022>) readAll();
		}
		return this.result.get(id);
	}
	
	@Override
	public List<?> listResult() {
		List<Patrocinadores2022> listResult = new ArrayList<Patrocinadores2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    Patrocinadores2022 patrocinador = (Patrocinadores2022) value;
		    if(patrocinador.getId() > 0){
		    	listResult.add(patrocinador);
		    }
		}
		return listResult;
	}

}
