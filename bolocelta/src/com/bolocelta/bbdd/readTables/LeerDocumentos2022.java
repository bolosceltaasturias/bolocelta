package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.inject.Named;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas2022;
import com.bolocelta.bbdd.constants.Ubicaciones2022;
import com.bolocelta.entities.Documentos2022;
import com.bolocelta.transformer.DocumentosTransformer2022;

@Named
public class LeerDocumentos2022 extends ALeer2022 {
	
	HashMap<Integer, Documentos2022> result = null;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, Documentos2022> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_DOCUMENTOS, Ubicaciones2022.UBICACION_BBDD_DOCUMENTOS);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	Documentos2022 documentos = new Documentos2022();
		    	documentos = DocumentosTransformer2022.transformerObjectCsv(documentos, row);
		    	result.put(documentos.getId(), documentos);
		    }
		}
	    return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Documentos2022 read(Integer id) {
		if(this.result == null){
			result = (HashMap<Integer, Documentos2022>) readAll();
		}
		return this.result.get(id);
	}
	
	@Override
	public List<?> listResult() {
		List<Documentos2022> listResult = new ArrayList<Documentos2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    Documentos2022 documentos = (Documentos2022) value;
		    if(documentos.getId() > 0){
		    	listResult.add(documentos);
		    }
		}
		return listResult;
	}

}
