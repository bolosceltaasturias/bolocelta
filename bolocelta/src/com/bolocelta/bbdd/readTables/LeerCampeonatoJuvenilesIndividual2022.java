package com.bolocelta.bbdd.readTables;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.Activo2022;
import com.bolocelta.bbdd.constants.EstadosIndividualParejas2022;
import com.bolocelta.bbdd.constants.NombresTablas2022;
import com.bolocelta.bbdd.constants.Ubicaciones2022;
import com.bolocelta.entities.CampeonatoJuvenilesIndividualClasificacion2022;
import com.bolocelta.entities.Configuracion2022;
import com.bolocelta.entities.Fases2022;
import com.bolocelta.entities.ParticipantesIndividual2022;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseCF2022;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseFF2022;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseSF2022;
import com.bolocelta.transformer.CalendarioIndividualFaseCFTransformer2022;
import com.bolocelta.transformer.CalendarioIndividualFaseFFTransformer2022;
import com.bolocelta.transformer.CalendarioIndividualFaseSFTransformer2022;
import com.bolocelta.transformer.CampeonatoJuvenilesIndividualTransformer2022;
import com.bolocelta.transformer.ConfiguracionTransformer2022;
import com.bolocelta.transformer.FasesTransformer2022;
import com.bolocelta.transformer.ParticipantesTransformer2022;

public class LeerCampeonatoJuvenilesIndividual2022 extends ALeer2022 {

	private static final int CONFIG_FECHA_MAX_INSCRIPCION = 1;
	private static final int CONFIG_ESTADO = 2;
	private static final int CONFIG_BOLERA_FINAL = 3;
	private static final int CONFIG_OBSERVACIONES_CAMPEONATO_1 = 4;
	private static final int CONFIG_OBSERVACIONES_CAMPEONATO_2 = 5;
	private static final int CONFIG_OBSERVACIONES_CAMPEONATO_3 = 6;
	private static final int CONFIG_OBSERVACIONES_CAMPEONATO_4 = 7;
	private static final int CONFIG_OBSERVACIONES_CAMPEONATO_5 = 8;
	private static final int CONFIG_OBSERVACIONES_CAMPEONATO_6 = 9;
	private static final int CONFIG_OBSERVACIONES_CAMPEONATO_7 = 10;
	private static final int CONFIG_BOLERAS_OCUPADAS_FI = 11;
	private static final int CONFIG_BOLERAS_OCUPADAS_OFCF = 12;
	
	private LeerJugadores2022 leerJugadores = new LeerJugadores2022();
	private LeerBoleras2022 leerBoleras = new LeerBoleras2022();
	
	HashMap<Integer, Fases2022> resultFases = null;
	HashMap<Integer, ParticipantesIndividual2022> resultParticipantes = null;
	HashMap<Integer, Configuracion2022> resultConfiguracion = null;
	HashMap<String, CalendarioFaseCF2022> resultCalendarioFaseCF = null;
	HashMap<String, CalendarioFaseSF2022> resultCalendarioFaseSF = null;
	HashMap<String, CalendarioFaseFF2022> resultCalendarioFaseFF = null;
	
	public LeerCampeonatoJuvenilesIndividual2022() {
		readConfig();
		readFases();
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<Integer, ?> readAllClasificacion() {
		HashMap<Integer, CampeonatoJuvenilesIndividualClasificacion2022> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CLASIFICACION, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES);
		if(csvParser != null){
			for (CSVRecord row : csvParser) {
				CampeonatoJuvenilesIndividualClasificacion2022 campeonatoJuvenilesIndividualClasificacion = new CampeonatoJuvenilesIndividualClasificacion2022();
				campeonatoJuvenilesIndividualClasificacion.setRowNum(row.getRecordNumber());
				campeonatoJuvenilesIndividualClasificacion = CampeonatoJuvenilesIndividualTransformer2022.transformerObjectClasificacionCsv(campeonatoJuvenilesIndividualClasificacion, row);
				campeonatoJuvenilesIndividualClasificacion.setJugador(leerJugadores.read(campeonatoJuvenilesIndividualClasificacion.getJugadorId(), true));
		    	result.put(campeonatoJuvenilesIndividualClasificacion.getId(), campeonatoJuvenilesIndividualClasificacion);
		    }
		}
		return result;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<String, CalendarioFaseCF2022> readCalendarioFaseCF() {
		resultCalendarioFaseCF = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_CF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	CalendarioFaseCF2022 calendarioFaseCF = new CalendarioFaseCF2022();
		    	calendarioFaseCF.setRowNum(row.getRecordNumber());
		    	calendarioFaseCF = CalendarioIndividualFaseCFTransformer2022.transformerObjectCsv(calendarioFaseCF, row);
		    	resultCalendarioFaseCF.put(calendarioFaseCF.getIdCruce(), calendarioFaseCF);
		    }
		}
		return resultCalendarioFaseCF;
	}
	
	@SuppressWarnings("unchecked")
	public CalendarioFaseCF2022 readCalendarioFaseCF(Integer id) {
		if(this.resultCalendarioFaseCF == null){
			resultCalendarioFaseCF = (HashMap<String, CalendarioFaseCF2022>) readCalendarioFaseCF();
		}
		if(this.resultCalendarioFaseCF != null){
			return this.resultCalendarioFaseCF.get(id);
		}
		return null;
	}
	
	public List<CalendarioFaseCF2022> listResultCalendarioFaseCF() {
		List<CalendarioFaseCF2022> listResult = new ArrayList<CalendarioFaseCF2022>();
		for (Entry<String, CalendarioFaseCF2022> entry : readCalendarioFaseCF().entrySet()) {
		    Object value = entry.getValue();
		    CalendarioFaseCF2022 calendarioFaseCF = (CalendarioFaseCF2022) value;
		    if(calendarioFaseCF.getId() > 0){
		    	calendarioFaseCF.setJugador1(leerJugadores.read(calendarioFaseCF.getJugador1Id(), true));
		    	calendarioFaseCF.setJugador2(leerJugadores.read(calendarioFaseCF.getJugador2Id(), true));
		    	calendarioFaseCF.setBolera(leerBoleras.read(calendarioFaseCF.getBoleraId()));
		    	//Calcular icono victoria, empate y derrota
		    	Integer resultadoPartida1 =  calendarioFaseCF.getJuegosJugador1P1() - calendarioFaseCF.getJuegosJugador2P1();
		    	Integer resultadoPartida2 =  calendarioFaseCF.getJuegosJugador1P2() - calendarioFaseCF.getJuegosJugador2P2();
		    	Integer resultadoPartida3 =  calendarioFaseCF.getJuegosJugador1P3() - calendarioFaseCF.getJuegosJugador2P3();
		    	
		    	//Sin Jugar
		    	if(resultadoPartida1 == 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
	    			calendarioFaseCF.setRequipo1("/resources/sinjugar.ico");
	    			calendarioFaseCF.setRequipo2("/resources/sinjugar.ico");
		    	}else
		    	//Ganador 1 solo una partida
		    	if(resultadoPartida1 > 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
		    		calendarioFaseCF.setRequipo1("/resources/ganador.ico");
	    			calendarioFaseCF.setRequipo2("/resources/derrota.ico");
		    	}else 
		    	//Ganador 2 solo una partida
		    	if(resultadoPartida1 < 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
		    		calendarioFaseCF.setRequipo1("/resources/derrota.ico");
	    			calendarioFaseCF.setRequipo2("/resources/ganador.ico");
		    	}else 
		    	//Ganador 1 dos partidas
		    	if( (resultadoPartida1 > 0 && resultadoPartida2 > 0) ||
	    			(resultadoPartida1 > 0 && resultadoPartida3 > 0) ||
	    			(resultadoPartida2 > 0 && resultadoPartida3 > 0)){
		    		calendarioFaseCF.setRequipo1("/resources/ganador.ico");
	    			calendarioFaseCF.setRequipo2("/resources/derrota.ico");
		    	}else
		    	//Ganador 2 dos partidas
		    	if( (resultadoPartida1 < 0 && resultadoPartida2 < 0) ||
	    			(resultadoPartida1 < 0 && resultadoPartida3 < 0) ||
	    			(resultadoPartida2 < 0 && resultadoPartida3 < 0)){
		    		calendarioFaseCF.setRequipo1("/resources/derrota.ico");
	    			calendarioFaseCF.setRequipo2("/resources/ganador.ico");
		    	}else{
		    		calendarioFaseCF.setRequipo1("/resources/sinjugar.ico");
		    		calendarioFaseCF.setRequipo2("/resources/sinjugar.ico");
		    	}

		    	listResult.add(calendarioFaseCF);
		    }
		}
		return listResult;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<String, CalendarioFaseSF2022> readCalendarioFaseSF() {
		resultCalendarioFaseSF = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_SF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	CalendarioFaseSF2022 calendarioFaseSF = new CalendarioFaseSF2022();
		    	calendarioFaseSF.setRowNum(row.getRecordNumber());
		    	calendarioFaseSF = CalendarioIndividualFaseSFTransformer2022.transformerObjectCsv(calendarioFaseSF, row);
		    	resultCalendarioFaseSF.put(calendarioFaseSF.getIdCruce(), calendarioFaseSF);
		    }
		}
		return resultCalendarioFaseSF;
	}
	
	@SuppressWarnings("unchecked")
	public CalendarioFaseSF2022 readCalendarioFaseSF(Integer id) {
		if(this.resultCalendarioFaseSF == null){
			resultCalendarioFaseSF = (HashMap<String, CalendarioFaseSF2022>) readCalendarioFaseSF();
		}
		if(this.resultCalendarioFaseSF != null){
			return this.resultCalendarioFaseSF.get(id);
		}
		return null;
	}
	
	public List<CalendarioFaseSF2022> listResultCalendarioFaseSF() {
		List<CalendarioFaseSF2022> listResult = new ArrayList<CalendarioFaseSF2022>();
		for (Entry<String, CalendarioFaseSF2022> entry : readCalendarioFaseSF().entrySet()) {
		    Object value = entry.getValue();
		    CalendarioFaseSF2022 calendarioFaseSF = (CalendarioFaseSF2022) value;
		    if(calendarioFaseSF.getId() > 0){
		    	calendarioFaseSF.setJugador1(leerJugadores.read(calendarioFaseSF.getJugador1Id(), true));
		    	calendarioFaseSF.setJugador2(leerJugadores.read(calendarioFaseSF.getJugador2Id(), true));
		    	calendarioFaseSF.setBolera(leerBoleras.read(calendarioFaseSF.getBoleraId()));
		    	//Calcular icono victoria, empate y derrota
		    	Integer resultadoPartida1 =  calendarioFaseSF.getJuegosJugador1P1() - calendarioFaseSF.getJuegosJugador2P1();
		    	Integer resultadoPartida2 =  calendarioFaseSF.getJuegosJugador1P2() - calendarioFaseSF.getJuegosJugador2P2();
		    	Integer resultadoPartida3 =  calendarioFaseSF.getJuegosJugador1P3() - calendarioFaseSF.getJuegosJugador2P3();
		    	
		    	//Sin Jugar
		    	if(resultadoPartida1 == 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
	    			calendarioFaseSF.setRequipo1("/resources/sinjugar.ico");
	    			calendarioFaseSF.setRequipo2("/resources/sinjugar.ico");
		    	}else
		    	//Ganador 1 solo una partida
		    	if(resultadoPartida1 > 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
		    		calendarioFaseSF.setRequipo1("/resources/ganador.ico");
	    			calendarioFaseSF.setRequipo2("/resources/derrota.ico");
		    	}else 
		    	//Ganador 2 solo una partida
		    	if(resultadoPartida1 < 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
		    		calendarioFaseSF.setRequipo1("/resources/derrota.ico");
	    			calendarioFaseSF.setRequipo2("/resources/ganador.ico");
		    	}else 
		    	//Ganador 1 dos partidas
		    	if( (resultadoPartida1 > 0 && resultadoPartida2 > 0) ||
	    			(resultadoPartida1 > 0 && resultadoPartida3 > 0) ||
	    			(resultadoPartida2 > 0 && resultadoPartida3 > 0)){
		    		calendarioFaseSF.setRequipo1("/resources/ganador.ico");
	    			calendarioFaseSF.setRequipo2("/resources/derrota.ico");
		    	}else
		    	//Ganador 2 dos partidas
		    	if( (resultadoPartida1 < 0 && resultadoPartida2 < 0) ||
	    			(resultadoPartida1 < 0 && resultadoPartida3 < 0) ||
	    			(resultadoPartida2 < 0 && resultadoPartida3 < 0)){
		    		calendarioFaseSF.setRequipo1("/resources/derrota.ico");
	    			calendarioFaseSF.setRequipo2("/resources/ganador.ico");
		    	}else{
		    		calendarioFaseSF.setRequipo1("/resources/sinjugar.ico");
		    		calendarioFaseSF.setRequipo2("/resources/sinjugar.ico");
		    	}

		    	listResult.add(calendarioFaseSF);
		    }
		}
		return listResult;
	}
	
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<String, CalendarioFaseFF2022> readCalendarioFaseFF() {
		resultCalendarioFaseFF = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CAL_FASE_FF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	CalendarioFaseFF2022 calendarioFaseFF = new CalendarioFaseFF2022();
		    	calendarioFaseFF.setRowNum(row.getRecordNumber());
		    	calendarioFaseFF = CalendarioIndividualFaseFFTransformer2022.transformerObjectCsv(calendarioFaseFF, row);
		    	resultCalendarioFaseFF.put(calendarioFaseFF.getIdCruce(), calendarioFaseFF);
		    }
		}
		return resultCalendarioFaseFF;
	}
	
	@SuppressWarnings("unchecked")
	public CalendarioFaseFF2022 readCalendarioFaseFF(Integer id) {
		if(this.resultCalendarioFaseFF == null){
			resultCalendarioFaseFF = (HashMap<String, CalendarioFaseFF2022>) readCalendarioFaseFF();
		}
		if(this.resultCalendarioFaseFF != null){
			return this.resultCalendarioFaseFF.get(id);
		}
		return null;
	}
	
	public List<CalendarioFaseFF2022> listResultCalendarioFaseFF() {
		List<CalendarioFaseFF2022> listResult = new ArrayList<CalendarioFaseFF2022>();
		for (Entry<String, CalendarioFaseFF2022> entry : readCalendarioFaseFF().entrySet()) {
		    Object value = entry.getValue();
		    CalendarioFaseFF2022 calendarioFaseFF = (CalendarioFaseFF2022) value;
		    if(calendarioFaseFF.getId() > 0){
		    	calendarioFaseFF.setJugador1(leerJugadores.read(calendarioFaseFF.getJugador1Id(), true));
		    	calendarioFaseFF.setJugador2(leerJugadores.read(calendarioFaseFF.getJugador2Id(), true));
		    	calendarioFaseFF.setBolera(leerBoleras.read(calendarioFaseFF.getBoleraId()));
		    	//Calcular icono victoria, empate y derrota
		    	Integer resultadoPartida1 =  calendarioFaseFF.getJuegosJugador1P1() - calendarioFaseFF.getJuegosJugador2P1();
		    	Integer resultadoPartida2 =  calendarioFaseFF.getJuegosJugador1P2() - calendarioFaseFF.getJuegosJugador2P2();
		    	Integer resultadoPartida3 =  calendarioFaseFF.getJuegosJugador1P3() - calendarioFaseFF.getJuegosJugador2P3();
		    	
		    	//Sin Jugar
		    	if(resultadoPartida1 == 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
	    			calendarioFaseFF.setRequipo1("/resources/sinjugar.ico");
	    			calendarioFaseFF.setRequipo2("/resources/sinjugar.ico");
		    	}else
		    	//Ganador 1 solo una partida
		    	if(resultadoPartida1 > 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
		    		calendarioFaseFF.setRequipo1("/resources/ganador.ico");
	    			calendarioFaseFF.setRequipo2("/resources/derrota.ico");
		    	}else 
		    	//Ganador 2 solo una partida
		    	if(resultadoPartida1 < 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
		    		calendarioFaseFF.setRequipo1("/resources/derrota.ico");
	    			calendarioFaseFF.setRequipo2("/resources/ganador.ico");
		    	}else 
		    	//Ganador 1 dos partidas
		    	if( (resultadoPartida1 > 0 && resultadoPartida2 > 0) ||
	    			(resultadoPartida1 > 0 && resultadoPartida3 > 0) ||
	    			(resultadoPartida2 > 0 && resultadoPartida3 > 0)){
		    		calendarioFaseFF.setRequipo1("/resources/ganador.ico");
	    			calendarioFaseFF.setRequipo2("/resources/derrota.ico");
		    	}else
		    	//Ganador 2 dos partidas
		    	if( (resultadoPartida1 < 0 && resultadoPartida2 < 0) ||
	    			(resultadoPartida1 < 0 && resultadoPartida3 < 0) ||
	    			(resultadoPartida2 < 0 && resultadoPartida3 < 0)){
		    		calendarioFaseFF.setRequipo1("/resources/derrota.ico");
	    			calendarioFaseFF.setRequipo2("/resources/ganador.ico");
		    	}else{
		    		calendarioFaseFF.setRequipo1("/resources/sinjugar.ico");
		    		calendarioFaseFF.setRequipo2("/resources/sinjugar.ico");
		    	}

		    	listResult.add(calendarioFaseFF);
		    }
		}
		return listResult;
	}
	
	
	
	
	@SuppressWarnings("unchecked")
	public CampeonatoJuvenilesIndividualClasificacion2022 readClasificacion(Integer id) {
		HashMap<Integer, CampeonatoJuvenilesIndividualClasificacion2022> result = (HashMap<Integer, CampeonatoJuvenilesIndividualClasificacion2022>) readAllClasificacion();
		return result.get(id);
	}
	
	@SuppressWarnings("unchecked")
	public CalendarioFaseSF2022 readCalendarioSemifinales(Integer id) {
		HashMap<String, CalendarioFaseSF2022> result = (HashMap<String, CalendarioFaseSF2022>) readCalendarioFaseSF();
		return result.get(id);
	}
	
	@SuppressWarnings("unchecked")
	public CalendarioFaseFF2022 readCalendarioFinal(Integer id) {
		HashMap<String, CalendarioFaseFF2022> result = (HashMap<String, CalendarioFaseFF2022>) readCalendarioFaseFF();
		return result.get(id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object read(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HashMap<Integer, ?> readAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<?> listResult() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public List<?> listResultClasificacion() {
		List<CampeonatoJuvenilesIndividualClasificacion2022> listResult = new ArrayList<CampeonatoJuvenilesIndividualClasificacion2022>();
		for (Entry<Integer, ?> entry : readAllClasificacion().entrySet()) {
		    Object value = entry.getValue();
		    CampeonatoJuvenilesIndividualClasificacion2022 cec = (CampeonatoJuvenilesIndividualClasificacion2022) value;
		    if(cec.getId() > 0){
		    	if(cec.getJugadorId() != null){
		    		cec.setJugador(leerJugadores.read(cec.getJugadorId()));
		    	}
		    	listResult.add((CampeonatoJuvenilesIndividualClasificacion2022) cec);
		    }
		}
		
		//Ordenar la clasificacion
		Collections.sort(listResult, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				CampeonatoJuvenilesIndividualClasificacion2022 cec1 = (CampeonatoJuvenilesIndividualClasificacion2022) o1;
				CampeonatoJuvenilesIndividualClasificacion2022 cec2 = (CampeonatoJuvenilesIndividualClasificacion2022) o2;
				
				Integer part1 = cec1.getTotalPuntosFinal();
				Integer part2 = cec2.getTotalPuntosFinal();
				int rdifpartidas = part1.compareTo(part2);
				if(rdifpartidas == -1) return 1;
				if(rdifpartidas == 1) return -1;
				return rdifpartidas;
				
			}
		});
		
		
		
		return listResult;
	}
	
	public Date getFechaMaxInscripcion(){
		Configuracion2022 configuracionFechaMaxInscripcion = readConfig(CONFIG_FECHA_MAX_INSCRIPCION);
		if(configuracionFechaMaxInscripcion != null && configuracionFechaMaxInscripcion.getValor() != null){
		    String date = configuracionFechaMaxInscripcion.getValor();
		    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy"); // date pattern
		    try {
				return simpleDateFormat.parse(date);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
			} 
		}
		return null;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<Integer, ?> readConfig() {
		resultConfiguracion = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_CONFIG, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	Configuracion2022 configuracion = new Configuracion2022();
		    	configuracion = ConfiguracionTransformer2022.transformerObjectCsv(configuracion, row);
		    	resultConfiguracion.put(configuracion.getId(), configuracion);
		    }
		}
		return resultConfiguracion;
	}
	
	@SuppressWarnings("unchecked")
	public Configuracion2022 readConfig(Integer id) {
		if(this.resultConfiguracion != null && id != null){
			return this.resultConfiguracion.get(id);
		}
		return null;
	}
	
	public String getEstadoCampeonato(){
		Configuracion2022 configuracionEstado = readConfig(CONFIG_ESTADO);
		if(configuracionEstado != null && configuracionEstado.getValor().equals(EstadosIndividualParejas2022.CONFIG_ESTADO_CREAR)){
			return "Creado";
		}else if(configuracionEstado != null && configuracionEstado.getValor().equals(EstadosIndividualParejas2022.CONFIG_ESTADO_ABRIR_INSCRIPCIONES)){
			return "Inscripciones abiertas";
		}else if(configuracionEstado != null && configuracionEstado.getValor().equals(EstadosIndividualParejas2022.CONFIG_ESTADO_CERRAR_INSCRIPCIONES)){
			return "Inscripciones cerradas";
		}else if(configuracionEstado != null && configuracionEstado.getValor().equals(EstadosIndividualParejas2022.CONFIG_ESTADO_CERRAR_FASES)){
			return "Fases cerradas";
		}else if(configuracionEstado != null && configuracionEstado.getValor().equals(EstadosIndividualParejas2022.CONFIG_ESTADO_SORTEADO_CAMPEONATO)){
			return "Campeonato sorteado";
		}else if(configuracionEstado != null && configuracionEstado.getValor().equals(EstadosIndividualParejas2022.CONFIG_ESTADO_FINALIZADO)){
			return "Finalizado";
		}
		return "Sin crear";
	}
	
	public String getBoleraFinal(){
		Configuracion2022 configuracionEstado = readConfig(CONFIG_BOLERA_FINAL);
		if(configuracionEstado != null){
			return configuracionEstado.getValor();
		}
		return "";
	}
	
	public String getObservacionesCampeonato1(){
		Configuracion2022 configuracionEstado = readConfig(CONFIG_OBSERVACIONES_CAMPEONATO_1);
		if(configuracionEstado != null){
			return configuracionEstado.getValor();
		}
		return "";
	}

	public String getObservacionesCampeonato2(){
		Configuracion2022 configuracionEstado = readConfig(CONFIG_OBSERVACIONES_CAMPEONATO_2);
		if(configuracionEstado != null){
			return configuracionEstado.getValor();
		}
		return "";
	}
	
	public String getObservacionesCampeonato3(){
		Configuracion2022 configuracionEstado = readConfig(CONFIG_OBSERVACIONES_CAMPEONATO_3);
		if(configuracionEstado != null){
			return configuracionEstado.getValor();
		}
		return "";
	}
	
	public String getObservacionesCampeonato4(){
		Configuracion2022 configuracionEstado = readConfig(CONFIG_OBSERVACIONES_CAMPEONATO_4);
		if(configuracionEstado != null){
			return configuracionEstado.getValor();
		}
		return "";
	}
	
	public String getObservacionesCampeonato5(){
		Configuracion2022 configuracionEstado = readConfig(CONFIG_OBSERVACIONES_CAMPEONATO_5);
		if(configuracionEstado != null){
			return configuracionEstado.getValor();
		}
		return "";
	}
	
	public String getObservacionesCampeonato6(){
		Configuracion2022 configuracionEstado = readConfig(CONFIG_OBSERVACIONES_CAMPEONATO_6);
		if(configuracionEstado != null){
			return configuracionEstado.getValor();
		}
		return "";
	}
	
	public String getObservacionesCampeonato7(){
		Configuracion2022 configuracionEstado = readConfig(CONFIG_OBSERVACIONES_CAMPEONATO_7);
		if(configuracionEstado != null){
			return configuracionEstado.getValor();
		}
		return "";
	}
	
	public String getBolerasOcupadasFI(){
		Configuracion2022 configuracionEstado = readConfig(CONFIG_BOLERAS_OCUPADAS_FI);
		if(configuracionEstado != null){
			return configuracionEstado.getValor();
		}
		return "";
	}
	
	public String getBolerasOcupadasOFCF(){
		Configuracion2022 configuracionEstado = readConfig(CONFIG_BOLERAS_OCUPADAS_OFCF);
		if(configuracionEstado != null){
			return configuracionEstado.getValor();
		}
		return "";
	}
	
	public List<?> listResultConfig() {
		List<Configuracion2022> listResult = new ArrayList<Configuracion2022>();
		for (Entry<Integer, ?> entry : readConfig().entrySet()) {
		    Object value = entry.getValue();
		    Configuracion2022 configuracion = (Configuracion2022) value;
		    if(configuracion.getId() > 0){
		    	listResult.add(configuracion);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultParticipantesOrderByEquipo() {
		List<ParticipantesIndividual2022> listResult = new ArrayList<ParticipantesIndividual2022>();
		for (Entry<Integer, ?> entry : readParticipantes().entrySet()) {
		    Object value = entry.getValue();
		    ParticipantesIndividual2022 participantesIndividual = (ParticipantesIndividual2022) value;
		    if(participantesIndividual.getId() > 0){
		    	if(participantesIndividual.getActivo().equalsIgnoreCase(Activo2022.SI)){
			    	if(participantesIndividual.getIdJugador() != null){
				    	participantesIndividual.setJugador(leerJugadores.read(participantesIndividual.getIdJugador()));
				    }
			    	listResult.add(participantesIndividual);
		    	}
		    	
		    }
		}
		
		//Ordenar la clasificacion
		Collections.sort(listResult, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				ParticipantesIndividual2022 pi1 = (ParticipantesIndividual2022) o1;
				ParticipantesIndividual2022 pi2 = (ParticipantesIndividual2022) o2;
				
				int requipo = pi1.getJugador().getEquipoId().compareTo(pi2.getJugador().getEquipoId());
				return requipo;
			}
		});
		
		return listResult;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<Integer, ?> readParticipantes() {
		resultParticipantes = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_PARTICIPANTES, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	ParticipantesIndividual2022 participantesIndividual = new ParticipantesIndividual2022();
		    	participantesIndividual = ParticipantesTransformer2022.transformerObjectCsv(participantesIndividual, row);
		    	participantesIndividual.setRowNum(row.getRecordNumber());
				//if(participantesIndividual.getActivo().equalsIgnoreCase(Activo.SI)){
					resultParticipantes.put(participantesIndividual.getId(), participantesIndividual);
//					System.out.println();
				//}
		    }
		}
		return resultParticipantes;
	}
	
	@SuppressWarnings("unchecked")
	public ParticipantesIndividual2022 readParticipante(Integer id) {
		if(this.resultParticipantes == null){
			resultParticipantes = (HashMap<Integer, ParticipantesIndividual2022>) readParticipantes();
		}
		if(this.resultParticipantes != null){
			return this.resultParticipantes.get(id);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public ParticipantesIndividual2022 existeJugadorComoParticipante(Integer idJugador) {
		List<ParticipantesIndividual2022> listResult = (List<ParticipantesIndividual2022>) listResultParticipantes();
		for (ParticipantesIndividual2022 participantesIndividual : listResult) {
			if(participantesIndividual.getIdJugador().equals(idJugador)){
				return participantesIndividual;
			}
		}
		return null;
	}
	
	public List<?> listResultParticipantes() {
		List<ParticipantesIndividual2022> listResult = new ArrayList<ParticipantesIndividual2022>();
		for (Entry<Integer, ?> entry : readParticipantes().entrySet()) {
		    Object value = entry.getValue();
		    ParticipantesIndividual2022 participantesIndividual = (ParticipantesIndividual2022) value;
		    if(participantesIndividual.getId() > 0){
		    	if(participantesIndividual.getIdJugador() != null){
			    	participantesIndividual.setJugador(leerJugadores.read(participantesIndividual.getIdJugador()));
			    }		    	
		    	listResult.add(participantesIndividual);
		    }
		}
		return listResult;
	}
	
	public boolean isEstadoCampeonatoSinCrear(){
		Configuracion2022 configuracionFechaMaxInscripcion = readConfig(CONFIG_FECHA_MAX_INSCRIPCION);
		if(configuracionFechaMaxInscripcion == null){
			return true;
		}
		return false;
	}
	
	public boolean isAbrirInscripciones(){
		Configuracion2022 configuracionFechaMaxInscripcion = readConfig(CONFIG_FECHA_MAX_INSCRIPCION);
		if(configuracionFechaMaxInscripcion != null){
			Configuracion2022 configuracionEstado = readConfig(CONFIG_ESTADO);
			if(configuracionEstado != null && configuracionEstado.getValor() != null && configuracionEstado.getValor().equals(EstadosIndividualParejas2022.CONFIG_ESTADO_CREAR)){
				return true;
			}
		}
		return false;
	}
	
	public boolean isCerrarInscripciones(){
		Configuracion2022 configuracionFechaMaxInscripcion = readConfig(CONFIG_FECHA_MAX_INSCRIPCION);
		if(configuracionFechaMaxInscripcion != null){
			Configuracion2022 configuracionEstado = readConfig(CONFIG_ESTADO);
			if(configuracionEstado != null && configuracionEstado.getValor() != null && configuracionEstado.getValor().equals(EstadosIndividualParejas2022.CONFIG_ESTADO_ABRIR_INSCRIPCIONES)){
				return true;
			}
		}
		return false;
	}
	
	public boolean isInscripcionesCerradas(){
		Configuracion2022 configuracionFechaMaxInscripcion = readConfig(CONFIG_FECHA_MAX_INSCRIPCION);
		if(configuracionFechaMaxInscripcion != null){
			Configuracion2022 configuracionEstado = readConfig(CONFIG_ESTADO);
			if(configuracionEstado != null && configuracionEstado.getValor() != null && configuracionEstado.getValor().equals(EstadosIndividualParejas2022.CONFIG_ESTADO_CERRAR_INSCRIPCIONES)){
				return true;
			}
		}
		return false;
	}
	
	public boolean isFasesCerradas(){
		Configuracion2022 configuracionFechaMaxInscripcion = readConfig(CONFIG_FECHA_MAX_INSCRIPCION);
		if(configuracionFechaMaxInscripcion != null){
			Configuracion2022 configuracionEstado = readConfig(CONFIG_ESTADO);
			if(configuracionEstado != null && configuracionEstado.getValor() != null && configuracionEstado.getValor().equals(EstadosIndividualParejas2022.CONFIG_ESTADO_CERRAR_FASES)){
				return true;
			}
		}
		return false;
	}
	
	public boolean isSorteoRealizado(){
		Configuracion2022 configuracionFechaMaxInscripcion = readConfig(CONFIG_FECHA_MAX_INSCRIPCION);
		if(configuracionFechaMaxInscripcion != null){
			Configuracion2022 configuracionEstado = readConfig(CONFIG_ESTADO);
			if(configuracionEstado != null && configuracionEstado.getValor() != null && configuracionEstado.getValor().equals(EstadosIndividualParejas2022.CONFIG_ESTADO_SORTEADO_CAMPEONATO)){
				return true;
			}
		}
		return false;
	}
	
	public boolean isSorteoFinalizado(){
		Configuracion2022 configuracionFechaMaxInscripcion = readConfig(CONFIG_FECHA_MAX_INSCRIPCION);
		if(configuracionFechaMaxInscripcion != null){
			Configuracion2022 configuracionEstado = readConfig(CONFIG_ESTADO);
			if(configuracionEstado != null && configuracionEstado.getValor() != null && configuracionEstado.getValor().equals(EstadosIndividualParejas2022.CONFIG_ESTADO_FINALIZADO)){
				return true;
			}
		}
		return false;
	}
	
	public List<?> listResultFases() {
		List<Fases2022> listResult = new ArrayList<Fases2022>();
		for (Entry<Integer, ?> entry : readFases().entrySet()) {
		    Object value = entry.getValue();
		    Fases2022 fases = (Fases2022) value;
		    if(fases.getId() > -1){
		    	listResult.add(fases);
		    }
		}
		return listResult;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<Integer, ?> readFases() {
		resultFases = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES_FASES, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_JUVENILES);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	Fases2022 fases = new Fases2022();
		    	fases.setRowNum(row.getRecordNumber());
		    	fases = FasesTransformer2022.transformerObjectCsv(fases, row);
		    	resultFases.put(fases.getId(), fases);
		    }
		}
		return resultFases;
	}

}
