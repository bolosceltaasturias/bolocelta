package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas2022;
import com.bolocelta.bbdd.constants.Ubicaciones2022;
import com.bolocelta.entities.Modalidades2022;
import com.bolocelta.transformer.ModalidadesTransformer2022;

public class LeerModalidades2022 extends ALeer2022 {
	
	HashMap<Integer, Modalidades2022> result = null;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, Modalidades2022> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_MODALIDADES, Ubicaciones2022.UBICACION_BBDD_ADMINISTRACION);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
	    		Modalidades2022 modalidad = new Modalidades2022();
	    		modalidad = ModalidadesTransformer2022.transformerObjectCsv(modalidad, row);
		    	result.put(modalidad.getId(), modalidad);
		    }
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Modalidades2022 read(Integer id) {
		if(this.result == null){
			this.result = (HashMap<Integer, Modalidades2022>) readAll();
		}
		return result.get(id);
	}

	@Override
	public List<?> listResult() {
		List<Modalidades2022> listResult = new ArrayList<Modalidades2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    Modalidades2022 modalidad = (Modalidades2022) value;
		    if(modalidad.getId() > 0){
		    	listResult.add((Modalidades2022) modalidad);
		    }
		}
		return listResult;
	}

}
