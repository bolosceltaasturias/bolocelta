package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.inject.Named;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.bolocelta.bbdd.constants.Activo2022;
import com.bolocelta.bbdd.constants.NombresTablas2022;
import com.bolocelta.bbdd.constants.Ubicaciones2022;
import com.bolocelta.entities.Boleras2022;
import com.bolocelta.transformer.BolerasTransformer2022;

@Named
public class LeerBoleras2022 extends ALeer2022 {
	
	HashMap<Integer, Boleras2022> result = null;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, Boleras2022> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_BOLERAS, Ubicaciones2022.UBICACION_BBDD_ADMINISTRACION);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	Boleras2022 bolera = new Boleras2022();
		    	bolera = BolerasTransformer2022.transformerObjectCsv(bolera, row);
		    	result.put(bolera.getId(), bolera);
		    }
		}
	    return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Boleras2022 read(Integer id) {
		if(this.result == null){
			result = (HashMap<Integer, Boleras2022>) readAll();
		}
		return this.result.get(id);
	}
	
	@Override
	public List<?> listResult() {
		List<Boleras2022> listResult = new ArrayList<Boleras2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    Boleras2022 bolera = (Boleras2022) value;
		    if(bolera.getId() > 0){
		    	listResult.add(bolera);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultFederadas() {
		List<Boleras2022> listResult = new ArrayList<Boleras2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    Boleras2022 bolera = (Boleras2022) value;
		    if(bolera.getId() > 0){
		    	if(bolera.getFederada().equalsIgnoreCase(Activo2022.SI)){
		    		listResult.add(bolera);
		    	}
		    }
		}
		return listResult;
	}

}
