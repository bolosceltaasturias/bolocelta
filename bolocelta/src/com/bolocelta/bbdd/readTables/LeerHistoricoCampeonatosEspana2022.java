package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.inject.Named;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.application.enumerations.ModalidadJuegoEnumeration2022;
import com.bolocelta.bbdd.constants.NombresTablas2022;
import com.bolocelta.bbdd.constants.Ubicaciones2022;
import com.bolocelta.entities.HistoricoCampeonatosEspana2022;
import com.bolocelta.transformer.HistoricoCampeonatoEspanaTransformer2022;

@Named
public class LeerHistoricoCampeonatosEspana2022 extends ALeer2022 {

	HashMap<Integer, HistoricoCampeonatosEspana2022> result = null;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, HistoricoCampeonatosEspana2022> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_HISTORICO_ESPANA, Ubicaciones2022.UBICACION_BBDD_HISTORICO);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	HistoricoCampeonatosEspana2022 historicoCE = new HistoricoCampeonatosEspana2022();
	    		historicoCE = HistoricoCampeonatoEspanaTransformer2022.transformerObjectCsv(historicoCE, row);
		    	result.put(historicoCE.getId(), historicoCE);
		    }
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public HistoricoCampeonatosEspana2022 read(Integer id) {
		if(this.result == null){
			this.result = (HashMap<Integer, HistoricoCampeonatosEspana2022>) readAll();
		}
		return result.get(id);
	}

	@Override
	public List<?> listResult() {
		List<HistoricoCampeonatosEspana2022> listResult = new ArrayList<HistoricoCampeonatosEspana2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    listResult.add((HistoricoCampeonatosEspana2022) value);
		}
		return listResult;
	}
	
	public List<?> listResultEquipos() {
		List<HistoricoCampeonatosEspana2022> listResult = new ArrayList<HistoricoCampeonatosEspana2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    HistoricoCampeonatosEspana2022 historicoCE = (HistoricoCampeonatosEspana2022) value;
		    if(historicoCE.getTipo() == ModalidadJuegoEnumeration2022.MODALIDAD_EQUIPOS){
		    	listResult.add((HistoricoCampeonatosEspana2022) value);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultIndividual() {
		List<HistoricoCampeonatosEspana2022> listResult = new ArrayList<HistoricoCampeonatosEspana2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    HistoricoCampeonatosEspana2022 historicoCE = (HistoricoCampeonatosEspana2022) value;
		    if(historicoCE.getTipo() == ModalidadJuegoEnumeration2022.MODALIDAD_INDIVIDUAL){
		    	listResult.add((HistoricoCampeonatosEspana2022) value);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultParejas() {
		List<HistoricoCampeonatosEspana2022> listResult = new ArrayList<HistoricoCampeonatosEspana2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    HistoricoCampeonatosEspana2022 historicoCE = (HistoricoCampeonatosEspana2022) value;
		    if(historicoCE.getTipo() == ModalidadJuegoEnumeration2022.MODALIDAD_PAREJAS){
		    	listResult.add((HistoricoCampeonatosEspana2022) value);
		    }
		}
		return listResult;
	}
	

	public List<?> listResultIndividualFemenino() {
		List<HistoricoCampeonatosEspana2022> listResult = new ArrayList<HistoricoCampeonatosEspana2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    HistoricoCampeonatosEspana2022 historicoCA = (HistoricoCampeonatosEspana2022) value;
		    if(historicoCA.getTipo() == ModalidadJuegoEnumeration2022.MODALIDAD_INDIVIDUAL_FEMENINO){
		    	listResult.add((HistoricoCampeonatosEspana2022) value);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultParejasFemenino() {
		List<HistoricoCampeonatosEspana2022> listResult = new ArrayList<HistoricoCampeonatosEspana2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    HistoricoCampeonatosEspana2022 historicoCA = (HistoricoCampeonatosEspana2022) value;
		    if(historicoCA.getTipo() == ModalidadJuegoEnumeration2022.MODALIDAD_PAREJAS_FEMENINO){
		    	listResult.add((HistoricoCampeonatosEspana2022) value);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultMixto() {
		List<HistoricoCampeonatosEspana2022> listResult = new ArrayList<HistoricoCampeonatosEspana2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    HistoricoCampeonatosEspana2022 historicoCA = (HistoricoCampeonatosEspana2022) value;
		    if(historicoCA.getTipo() == ModalidadJuegoEnumeration2022.MODALIDAD_MIXTO){
		    	listResult.add((HistoricoCampeonatosEspana2022) value);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultVeteranos() {
		List<HistoricoCampeonatosEspana2022> listResult = new ArrayList<HistoricoCampeonatosEspana2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    HistoricoCampeonatosEspana2022 historicoCA = (HistoricoCampeonatosEspana2022) value;
		    if(historicoCA.getTipo() == ModalidadJuegoEnumeration2022.MODALIDAD_VETERANOS){
		    	listResult.add((HistoricoCampeonatosEspana2022) value);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultJuveniles() {
		List<HistoricoCampeonatosEspana2022> listResult = new ArrayList<HistoricoCampeonatosEspana2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    HistoricoCampeonatosEspana2022 historicoCA = (HistoricoCampeonatosEspana2022) value;
		    if(historicoCA.getTipo() == ModalidadJuegoEnumeration2022.MODALIDAD_JUVENILES){
		    	listResult.add((HistoricoCampeonatosEspana2022) value);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultParejasJuveniles() {
		List<HistoricoCampeonatosEspana2022> listResult = new ArrayList<HistoricoCampeonatosEspana2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    HistoricoCampeonatosEspana2022 historicoCA = (HistoricoCampeonatosEspana2022) value;
		    if(historicoCA.getTipo() == ModalidadJuegoEnumeration2022.MODALIDAD_PAREJAS_JUVENILES){
		    	listResult.add((HistoricoCampeonatosEspana2022) value);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultCadetes() {
		List<HistoricoCampeonatosEspana2022> listResult = new ArrayList<HistoricoCampeonatosEspana2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    HistoricoCampeonatosEspana2022 historicoCA = (HistoricoCampeonatosEspana2022) value;
		    if(historicoCA.getTipo() == ModalidadJuegoEnumeration2022.MODALIDAD_CADETES){
		    	listResult.add((HistoricoCampeonatosEspana2022) value);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultParejasCadetes() {
		List<HistoricoCampeonatosEspana2022> listResult = new ArrayList<HistoricoCampeonatosEspana2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    HistoricoCampeonatosEspana2022 historicoCA = (HistoricoCampeonatosEspana2022) value;
		    if(historicoCA.getTipo() == ModalidadJuegoEnumeration2022.MODALIDAD_PAREJAS_CADETES){
		    	listResult.add((HistoricoCampeonatosEspana2022) value);
		    }
		}
		return listResult;
	}

}
