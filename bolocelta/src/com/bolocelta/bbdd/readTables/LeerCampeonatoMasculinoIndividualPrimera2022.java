package com.bolocelta.bbdd.readTables;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.Activo2022;
import com.bolocelta.bbdd.constants.EstadosIndividualParejas2022;
import com.bolocelta.bbdd.constants.FasesModelo2022;
import com.bolocelta.bbdd.constants.FasesTabShow2022;
import com.bolocelta.bbdd.constants.NombresTablas2022;
import com.bolocelta.bbdd.constants.Ubicaciones2022;
import com.bolocelta.entities.Configuracion2022;
import com.bolocelta.entities.Fases2022;
import com.bolocelta.entities.ParticipantesIndividual2022;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseCF2022;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseFC2022;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseFF2022;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseI2022;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseOF2022;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseSF2022;
import com.bolocelta.entities.sorteos.individual.ClasificacionFaseI2022;
import com.bolocelta.transformer.CalendarioIndividualFaseCFTransformer2022;
import com.bolocelta.transformer.CalendarioIndividualFaseFCTransformer2022;
import com.bolocelta.transformer.CalendarioIndividualFaseFFTransformer2022;
import com.bolocelta.transformer.CalendarioIndividualFaseITransformer2022;
import com.bolocelta.transformer.CalendarioIndividualFaseOFTransformer2022;
import com.bolocelta.transformer.CalendarioIndividualFaseSFTransformer2022;
import com.bolocelta.transformer.ClasificacionIndividualFaseITransformer2022;
import com.bolocelta.transformer.ConfiguracionTransformer2022;
import com.bolocelta.transformer.FasesTransformer2022;
import com.bolocelta.transformer.ParticipantesTransformer2022;

public class LeerCampeonatoMasculinoIndividualPrimera2022 extends ALeer2022 {
	
	private static final int CONFIG_FECHA_MAX_INSCRIPCION = 1;
	private static final int CONFIG_ESTADO = 2;
	private static final int CONFIG_BOLERA_FINAL = 3;
	private static final int CONFIG_OBSERVACIONES_CAMPEONATO_1 = 4;
	private static final int CONFIG_OBSERVACIONES_CAMPEONATO_2 = 5;
	private static final int CONFIG_OBSERVACIONES_CAMPEONATO_3 = 6;
	private static final int CONFIG_OBSERVACIONES_CAMPEONATO_4 = 7;
	private static final int CONFIG_OBSERVACIONES_CAMPEONATO_5 = 8;
	private static final int CONFIG_OBSERVACIONES_CAMPEONATO_6 = 9;
	private static final int CONFIG_OBSERVACIONES_CAMPEONATO_7 = 10;
	private static final int CONFIG_BOLERAS_OCUPADAS_FI = 11;
	private static final int CONFIG_BOLERAS_OCUPADAS_OFCF = 12;
	
	HashMap<Integer, Configuracion2022> resultConfiguracion = null;
	HashMap<Integer, Fases2022> resultFases = null;
	HashMap<Integer, ParticipantesIndividual2022> resultParticipantes = null;
	HashMap<String, String> resultGruposFaseI = null;
	HashMap<String, ClasificacionFaseI2022> resultClasificacionFaseI = null;
	HashMap<String, CalendarioFaseI2022> resultCalendarioFaseI = null;
	HashMap<String, CalendarioFaseOF2022> resultCalendarioFaseOF = null;
	HashMap<String, CalendarioFaseCF2022> resultCalendarioFaseCF = null;
	HashMap<String, CalendarioFaseSF2022> resultCalendarioFaseSF = null;
	HashMap<String, CalendarioFaseFC2022> resultCalendarioFaseFC = null;
	HashMap<String, CalendarioFaseFF2022> resultCalendarioFaseFF = null;
	
	private LeerJugadores2022 leerJugadores = new LeerJugadores2022();
	private LeerBoleras2022 leerBoleras = new LeerBoleras2022();
	
	public LeerCampeonatoMasculinoIndividualPrimera2022() {
		readConfig();
		readFases();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<Integer, ?> readConfig() {
		resultConfiguracion = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_PRIMERA_CONFIG, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_PRIMERA);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	Configuracion2022 configuracion = new Configuracion2022();
		    	configuracion = ConfiguracionTransformer2022.transformerObjectCsv(configuracion, row);
		    	resultConfiguracion.put(configuracion.getId(), configuracion);
		    }
		}
		return resultConfiguracion;
	}
	
	@SuppressWarnings("unchecked")
	public Configuracion2022 readConfig(Integer id) {
		if(this.resultConfiguracion != null && id != null){
			return this.resultConfiguracion.get(id);
		}
		return null;
	}
	
	public List<?> listResultConfig() {
		List<Configuracion2022> listResult = new ArrayList<Configuracion2022>();
		for (Entry<Integer, ?> entry : readConfig().entrySet()) {
		    Object value = entry.getValue();
		    Configuracion2022 configuracion = (Configuracion2022) value;
		    if(configuracion.getId() > 0){
		    	listResult.add(configuracion);
		    }
		}
		return listResult;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<Integer, ?> readFases() {
		resultFases = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_PRIMERA_FASES, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_PRIMERA);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	Fases2022 fases = new Fases2022();
		    	fases.setRowNum(row.getRecordNumber());
		    	fases = FasesTransformer2022.transformerObjectCsv(fases, row);
		    	resultFases.put(fases.getId(), fases);
		    }
		}
		return resultFases;
	}
	
	@SuppressWarnings("unchecked")
	public Fases2022 readFases(Integer id) {
		if(this.resultFases == null){
			resultFases = (HashMap<Integer, Fases2022>) readFases();
		}
		if(this.resultFases != null){
			return this.resultFases.get(id);
		}
		return null;
	}
	
	public List<?> listResultFases() {
		List<Fases2022> listResult = new ArrayList<Fases2022>();
		for (Entry<Integer, ?> entry : readFases().entrySet()) {
		    Object value = entry.getValue();
		    Fases2022 fases = (Fases2022) value;
		    if(fases.getId() > 0){
		    	listResult.add(fases);
		    }
		}
		return listResult;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<Integer, ?> readParticipantes() {
		resultParticipantes = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_PRIMERA_PARTICIPANTES, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_PRIMERA);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	ParticipantesIndividual2022 participantesIndividual = new ParticipantesIndividual2022();
		    	participantesIndividual = ParticipantesTransformer2022.transformerObjectCsv(participantesIndividual, row);
		    	participantesIndividual.setRowNum(row.getRecordNumber());
				//if(participantesIndividual.getActivo().equalsIgnoreCase(Activo.SI)){
					resultParticipantes.put(participantesIndividual.getId(), participantesIndividual);
//					System.out.println();
				//}
		    }
		}
		return resultParticipantes;
	}
	
	@SuppressWarnings("unchecked")
	public ParticipantesIndividual2022 readParticipante(Integer id) {
		if(this.resultParticipantes == null){
			resultParticipantes = (HashMap<Integer, ParticipantesIndividual2022>) readParticipantes();
		}
		if(this.resultParticipantes != null){
			return this.resultParticipantes.get(id);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public ParticipantesIndividual2022 existeJugadorComoParticipante(Integer idJugador) {
		List<ParticipantesIndividual2022> listResult = (List<ParticipantesIndividual2022>) listResultParticipantes();
		for (ParticipantesIndividual2022 participantesIndividual : listResult) {
			if(participantesIndividual.getIdJugador().equals(idJugador)){
				return participantesIndividual;
			}
		}
		return null;
	}
	
	public List<?> listResultParticipantes() {
		List<ParticipantesIndividual2022> listResult = new ArrayList<ParticipantesIndividual2022>();
		for (Entry<Integer, ?> entry : readParticipantes().entrySet()) {
		    Object value = entry.getValue();
		    ParticipantesIndividual2022 participantesIndividual = (ParticipantesIndividual2022) value;
		    if(participantesIndividual.getId() > 0){
		    	if(participantesIndividual.getIdJugador() != null){
			    	participantesIndividual.setJugador(leerJugadores.read(participantesIndividual.getIdJugador()));
			    }		    	
		    	listResult.add(participantesIndividual);
		    }
		}
		return listResult;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<String, ?> readClasificacionFaseI() {
		resultClasificacionFaseI = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_PRIMERA_CLA_FASE_I, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_PRIMERA);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	ClasificacionFaseI2022 clasificacionFaseI = new ClasificacionFaseI2022();
		    	clasificacionFaseI.setRowNum(row.getRecordNumber());
		    	clasificacionFaseI = ClasificacionIndividualFaseITransformer2022.transformerObjectCsv(clasificacionFaseI, row);
		    	resultClasificacionFaseI.put(clasificacionFaseI.getKey(), clasificacionFaseI);
		    }
		}
		return resultClasificacionFaseI;
	}
	
	@SuppressWarnings("unchecked")
	public ClasificacionFaseI2022 readClasificacionFaseI(Integer id) {
		if(this.resultClasificacionFaseI == null){
			resultClasificacionFaseI = (HashMap<String, ClasificacionFaseI2022>) readClasificacionFaseI();
		}
		if(this.resultClasificacionFaseI != null){
			return this.resultClasificacionFaseI.get(id);
		}
		return null;
	}
	
	public List<?> listResultClasificacionFaseI() {
		List<ClasificacionFaseI2022> listResult = new ArrayList<ClasificacionFaseI2022>();
		for (Entry<String, ?> entry : readClasificacionFaseI().entrySet()) {
		    Object value = entry.getValue();
		    ClasificacionFaseI2022 clasificacionFaseI = (ClasificacionFaseI2022) value;
		    if(clasificacionFaseI.getId() > 0){
		    	clasificacionFaseI.setJugador(leerJugadores.read(clasificacionFaseI.getJugadorId(), true));
		    	listResult.add(clasificacionFaseI);
		    }
		}
		return listResult;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<String, ?> readCalendarioFaseI() {
		resultCalendarioFaseI = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_PRIMERA_CAL_FASE_I, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_PRIMERA);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	CalendarioFaseI2022 calendarioFaseI = new CalendarioFaseI2022();
		    	calendarioFaseI.setRowNum(row.getRecordNumber());
		    	calendarioFaseI = CalendarioIndividualFaseITransformer2022.transformerObjectCsv(calendarioFaseI, row);
		    	resultCalendarioFaseI.put(calendarioFaseI.getKey(), calendarioFaseI);
		    }
		}
		return resultCalendarioFaseI;
	}
	
	@SuppressWarnings("unchecked")
	public CalendarioFaseI2022 readCalendarioFaseI(Integer id) {
		if(this.resultCalendarioFaseI == null){
			resultCalendarioFaseI = (HashMap<String, CalendarioFaseI2022>) readCalendarioFaseI();
		}
		if(this.resultCalendarioFaseI != null){
			return this.resultCalendarioFaseI.get(id);
		}
		return null;
	}
	
	public List<?> listResultCalendarioFaseI() {
		List<CalendarioFaseI2022> listResult = new ArrayList<CalendarioFaseI2022>();
		for (Entry<String, ?> entry : readCalendarioFaseI().entrySet()) {
		    Object value = entry.getValue();
		    CalendarioFaseI2022 calendarioFaseI = (CalendarioFaseI2022) value;
		    if(calendarioFaseI.getId() > 0){
		    	calendarioFaseI.setJugador1(leerJugadores.read(calendarioFaseI.getJugador1Id(), true));
		    	calendarioFaseI.setJugador2(leerJugadores.read(calendarioFaseI.getJugador2Id(), true));
		    	calendarioFaseI.setBolera(leerBoleras.read(calendarioFaseI.getBoleraId()));
		    	if(calendarioFaseI.getJuegosJugador1() != null && calendarioFaseI.getJuegosJugador2() != null){
		    		if(calendarioFaseI.getJuegosJugador1() == 0 && calendarioFaseI.getJuegosJugador2() == 0){
		    			calendarioFaseI.setRequipo1("/resources/sinjugar.ico");
		    			calendarioFaseI.setRequipo2("/resources/sinjugar.ico");
		    		}else if(calendarioFaseI.getJuegosJugador1() > calendarioFaseI.getJuegosJugador2()){
		    			calendarioFaseI.setRequipo1("/resources/ganador.ico");
		    			calendarioFaseI.setRequipo2("/resources/derrota.ico");
		    		}else if(calendarioFaseI.getJuegosJugador1() < calendarioFaseI.getJuegosJugador2()){
		    			calendarioFaseI.setRequipo1("/resources/derrota.ico");
		    			calendarioFaseI.setRequipo2("/resources/ganador.ico");
		    		}else if(calendarioFaseI.getJuegosJugador1().equals(calendarioFaseI.getJuegosJugador2())){
		    			calendarioFaseI.setRequipo1("/resources/empate.ico");
		    			calendarioFaseI.setRequipo2("/resources/empate.ico");
		    		}
		    		
		    	}else{
		    		calendarioFaseI.setRequipo1("/resources/sinjugar.ico");
		    		calendarioFaseI.setRequipo2("/resources/sinjugar.ico");
		    	}
		    	listResult.add(calendarioFaseI);
		    }
		}
		return listResult;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<String, ?> readCalendarioFaseOF() {
		resultCalendarioFaseOF = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_PRIMERA_CAL_FASE_OF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_PRIMERA);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	CalendarioFaseOF2022 calendarioFaseOF = new CalendarioFaseOF2022();
		    	calendarioFaseOF.setRowNum(row.getRecordNumber());
		    	calendarioFaseOF = CalendarioIndividualFaseOFTransformer2022.transformerObjectCsv(calendarioFaseOF, row);
		    	resultCalendarioFaseOF.put(calendarioFaseOF.getIdCruce(), calendarioFaseOF);
		    }
		}
		return resultCalendarioFaseOF;
	}
	
	@SuppressWarnings("unchecked")
	public CalendarioFaseOF2022 readCalendarioFaseOF(Integer id) {
		if(this.resultCalendarioFaseOF == null){
			resultCalendarioFaseOF = (HashMap<String, CalendarioFaseOF2022>) readCalendarioFaseOF();
		}
		if(this.resultCalendarioFaseOF != null){
			return this.resultCalendarioFaseOF.get(id);
		}
		return null;
	}
	
	public List<?> listResultCalendarioFaseOF() {
		List<CalendarioFaseOF2022> listResult = new ArrayList<CalendarioFaseOF2022>();
		for (Entry<String, ?> entry : readCalendarioFaseOF().entrySet()) {
		    Object value = entry.getValue();
		    CalendarioFaseOF2022 calendarioFaseOF = (CalendarioFaseOF2022) value;
		    if(calendarioFaseOF.getId() > 0){
		    	calendarioFaseOF.setJugador1(leerJugadores.read(calendarioFaseOF.getJugador1Id(), true));
		    	calendarioFaseOF.setJugador2(leerJugadores.read(calendarioFaseOF.getJugador2Id(), true));
		    	calendarioFaseOF.setBolera(leerBoleras.read(calendarioFaseOF.getBoleraId()));
		    	//Calcular icono victoria, empate y derrota
		    	Integer resultadoPartida1 =  calendarioFaseOF.getJuegosJugador1P1() - calendarioFaseOF.getJuegosJugador2P1();
		    	Integer resultadoPartida2 =  calendarioFaseOF.getJuegosJugador1P2() - calendarioFaseOF.getJuegosJugador2P2();
		    	Integer resultadoPartida3 =  calendarioFaseOF.getJuegosJugador1P3() - calendarioFaseOF.getJuegosJugador2P3();
		    	
		    	//Sin Jugar
		    	if(resultadoPartida1 == 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
	    			calendarioFaseOF.setRequipo1("/resources/sinjugar.ico");
	    			calendarioFaseOF.setRequipo2("/resources/sinjugar.ico");
		    	}else
		    	//Ganador 1 solo una partida
		    	if(resultadoPartida1 > 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
		    		calendarioFaseOF.setRequipo1("/resources/ganador.ico");
	    			calendarioFaseOF.setRequipo2("/resources/derrota.ico");
		    	}else 
		    	//Ganador 2 solo una partida
		    	if(resultadoPartida1 < 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
		    		calendarioFaseOF.setRequipo1("/resources/derrota.ico");
	    			calendarioFaseOF.setRequipo2("/resources/ganador.ico");
		    	}else 
		    	//Ganador 1 dos partidas
		    	if( (resultadoPartida1 > 0 && resultadoPartida2 > 0) ||
	    			(resultadoPartida1 > 0 && resultadoPartida3 > 0) ||
	    			(resultadoPartida2 > 0 && resultadoPartida3 > 0)){
		    		calendarioFaseOF.setRequipo1("/resources/ganador.ico");
	    			calendarioFaseOF.setRequipo2("/resources/derrota.ico");
		    	}else
		    	//Ganador 2 dos partidas
		    	if( (resultadoPartida1 < 0 && resultadoPartida2 < 0) ||
	    			(resultadoPartida1 < 0 && resultadoPartida3 < 0) ||
	    			(resultadoPartida2 < 0 && resultadoPartida3 < 0)){
		    		calendarioFaseOF.setRequipo1("/resources/derrota.ico");
	    			calendarioFaseOF.setRequipo2("/resources/ganador.ico");
		    	}else{
		    		calendarioFaseOF.setRequipo1("/resources/sinjugar.ico");
		    		calendarioFaseOF.setRequipo2("/resources/sinjugar.ico");
		    	}

		    	listResult.add(calendarioFaseOF);
		    }
		}
		return listResult;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<String, ?> readCalendarioFaseCF() {
		resultCalendarioFaseCF = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_PRIMERA_CAL_FASE_CF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_PRIMERA);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	CalendarioFaseCF2022 calendarioFaseCF = new CalendarioFaseCF2022();
		    	calendarioFaseCF.setRowNum(row.getRecordNumber());
		    	calendarioFaseCF = CalendarioIndividualFaseCFTransformer2022.transformerObjectCsv(calendarioFaseCF, row);
		    	resultCalendarioFaseCF.put(calendarioFaseCF.getIdCruce(), calendarioFaseCF);
		    }
		}
		return resultCalendarioFaseCF;
	}
	
	@SuppressWarnings("unchecked")
	public CalendarioFaseCF2022 readCalendarioFaseCF(Integer id) {
		if(this.resultCalendarioFaseCF == null){
			resultCalendarioFaseCF = (HashMap<String, CalendarioFaseCF2022>) readCalendarioFaseCF();
		}
		if(this.resultCalendarioFaseCF != null){
			return this.resultCalendarioFaseCF.get(id);
		}
		return null;
	}
	
	public List<?> listResultCalendarioFaseCF() {
		List<CalendarioFaseCF2022> listResult = new ArrayList<CalendarioFaseCF2022>();
		for (Entry<String, ?> entry : readCalendarioFaseCF().entrySet()) {
		    Object value = entry.getValue();
		    CalendarioFaseCF2022 calendarioFaseCF = (CalendarioFaseCF2022) value;
		    if(calendarioFaseCF.getId() > 0){
		    	calendarioFaseCF.setJugador1(leerJugadores.read(calendarioFaseCF.getJugador1Id(), true));
		    	calendarioFaseCF.setJugador2(leerJugadores.read(calendarioFaseCF.getJugador2Id(), true));
		    	calendarioFaseCF.setBolera(leerBoleras.read(calendarioFaseCF.getBoleraId()));
		    	//Calcular icono victoria, empate y derrota
		    	Integer resultadoPartida1 =  calendarioFaseCF.getJuegosJugador1P1() - calendarioFaseCF.getJuegosJugador2P1();
		    	Integer resultadoPartida2 =  calendarioFaseCF.getJuegosJugador1P2() - calendarioFaseCF.getJuegosJugador2P2();
		    	Integer resultadoPartida3 =  calendarioFaseCF.getJuegosJugador1P3() - calendarioFaseCF.getJuegosJugador2P3();
		    	
		    	//Sin Jugar
		    	if(resultadoPartida1 == 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
	    			calendarioFaseCF.setRequipo1("/resources/sinjugar.ico");
	    			calendarioFaseCF.setRequipo2("/resources/sinjugar.ico");
		    	}else
		    	//Ganador 1 solo una partida
		    	if(resultadoPartida1 > 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
		    		calendarioFaseCF.setRequipo1("/resources/ganador.ico");
	    			calendarioFaseCF.setRequipo2("/resources/derrota.ico");
		    	}else 
		    	//Ganador 2 solo una partida
		    	if(resultadoPartida1 < 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
		    		calendarioFaseCF.setRequipo1("/resources/derrota.ico");
	    			calendarioFaseCF.setRequipo2("/resources/ganador.ico");
		    	}else 
		    	//Ganador 1 dos partidas
		    	if( (resultadoPartida1 > 0 && resultadoPartida2 > 0) ||
	    			(resultadoPartida1 > 0 && resultadoPartida3 > 0) ||
	    			(resultadoPartida2 > 0 && resultadoPartida3 > 0)){
		    		calendarioFaseCF.setRequipo1("/resources/ganador.ico");
	    			calendarioFaseCF.setRequipo2("/resources/derrota.ico");
		    	}else
		    	//Ganador 2 dos partidas
		    	if( (resultadoPartida1 < 0 && resultadoPartida2 < 0) ||
	    			(resultadoPartida1 < 0 && resultadoPartida3 < 0) ||
	    			(resultadoPartida2 < 0 && resultadoPartida3 < 0)){
		    		calendarioFaseCF.setRequipo1("/resources/derrota.ico");
	    			calendarioFaseCF.setRequipo2("/resources/ganador.ico");
		    	}else{
		    		calendarioFaseCF.setRequipo1("/resources/sinjugar.ico");
		    		calendarioFaseCF.setRequipo2("/resources/sinjugar.ico");
		    	}

		    	listResult.add(calendarioFaseCF);
		    }
		}
		return listResult;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<String, ?> readCalendarioFaseSF() {
		resultCalendarioFaseSF = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_PRIMERA_CAL_FASE_SF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_PRIMERA);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	CalendarioFaseSF2022 calendarioFaseSF = new CalendarioFaseSF2022();
		    	calendarioFaseSF.setRowNum(row.getRecordNumber());
		    	calendarioFaseSF = CalendarioIndividualFaseSFTransformer2022.transformerObjectCsv(calendarioFaseSF, row);
		    	resultCalendarioFaseSF.put(calendarioFaseSF.getIdCruce(), calendarioFaseSF);
		    }
		}
		return resultCalendarioFaseSF;
	}
	
	@SuppressWarnings("unchecked")
	public CalendarioFaseSF2022 readCalendarioFaseSF(Integer id) {
		if(this.resultCalendarioFaseSF == null){
			resultCalendarioFaseSF = (HashMap<String, CalendarioFaseSF2022>) readCalendarioFaseSF();
		}
		if(this.resultCalendarioFaseSF != null){
			return this.resultCalendarioFaseSF.get(id);
		}
		return null;
	}
	
	public List<?> listResultCalendarioFaseSF() {
		List<CalendarioFaseSF2022> listResult = new ArrayList<CalendarioFaseSF2022>();
		for (Entry<String, ?> entry : readCalendarioFaseSF().entrySet()) {
		    Object value = entry.getValue();
		    CalendarioFaseSF2022 calendarioFaseSF = (CalendarioFaseSF2022) value;
		    if(calendarioFaseSF.getId() > 0){
		    	calendarioFaseSF.setJugador1(leerJugadores.read(calendarioFaseSF.getJugador1Id(), true));
		    	calendarioFaseSF.setJugador2(leerJugadores.read(calendarioFaseSF.getJugador2Id(), true));
		    	calendarioFaseSF.setBolera(leerBoleras.read(calendarioFaseSF.getBoleraId()));
		    	//Calcular icono victoria, empate y derrota
		    	Integer resultadoPartida1 =  calendarioFaseSF.getJuegosJugador1P1() - calendarioFaseSF.getJuegosJugador2P1();
		    	Integer resultadoPartida2 =  calendarioFaseSF.getJuegosJugador1P2() - calendarioFaseSF.getJuegosJugador2P2();
		    	Integer resultadoPartida3 =  calendarioFaseSF.getJuegosJugador1P3() - calendarioFaseSF.getJuegosJugador2P3();
		    	
		    	//Sin Jugar
		    	if(resultadoPartida1 == 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
	    			calendarioFaseSF.setRequipo1("/resources/sinjugar.ico");
	    			calendarioFaseSF.setRequipo2("/resources/sinjugar.ico");
		    	}else
		    	//Ganador 1 solo una partida
		    	if(resultadoPartida1 > 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
		    		calendarioFaseSF.setRequipo1("/resources/ganador.ico");
	    			calendarioFaseSF.setRequipo2("/resources/derrota.ico");
		    	}else 
		    	//Ganador 2 solo una partida
		    	if(resultadoPartida1 < 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
		    		calendarioFaseSF.setRequipo1("/resources/derrota.ico");
	    			calendarioFaseSF.setRequipo2("/resources/ganador.ico");
		    	}else 
		    	//Ganador 1 dos partidas
		    	if( (resultadoPartida1 > 0 && resultadoPartida2 > 0) ||
	    			(resultadoPartida1 > 0 && resultadoPartida3 > 0) ||
	    			(resultadoPartida2 > 0 && resultadoPartida3 > 0)){
		    		calendarioFaseSF.setRequipo1("/resources/ganador.ico");
	    			calendarioFaseSF.setRequipo2("/resources/derrota.ico");
		    	}else
		    	//Ganador 2 dos partidas
		    	if( (resultadoPartida1 < 0 && resultadoPartida2 < 0) ||
	    			(resultadoPartida1 < 0 && resultadoPartida3 < 0) ||
	    			(resultadoPartida2 < 0 && resultadoPartida3 < 0)){
		    		calendarioFaseSF.setRequipo1("/resources/derrota.ico");
	    			calendarioFaseSF.setRequipo2("/resources/ganador.ico");
		    	}else{
		    		calendarioFaseSF.setRequipo1("/resources/sinjugar.ico");
		    		calendarioFaseSF.setRequipo2("/resources/sinjugar.ico");
		    	}

		    	listResult.add(calendarioFaseSF);
		    }
		}
		return listResult;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<String, ?> readCalendarioFaseFC() {
		resultCalendarioFaseFC = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_PRIMERA_CAL_FASE_FC, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_PRIMERA);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	CalendarioFaseFC2022 calendarioFaseFC = new CalendarioFaseFC2022();
		    	calendarioFaseFC.setRowNum(row.getRecordNumber());
		    	calendarioFaseFC = CalendarioIndividualFaseFCTransformer2022.transformerObjectCsv(calendarioFaseFC, row);
		    	resultCalendarioFaseFC.put(calendarioFaseFC.getIdCruce(), calendarioFaseFC);
		    }
		}
		return resultCalendarioFaseFC;
	}
	
	@SuppressWarnings("unchecked")
	public CalendarioFaseFC2022 readCalendarioFaseFC(Integer id) {
		if(this.resultCalendarioFaseFC == null){
			resultCalendarioFaseFC = (HashMap<String, CalendarioFaseFC2022>) readCalendarioFaseFC();
		}
		if(this.resultCalendarioFaseFC != null){
			return this.resultCalendarioFaseFC.get(id);
		}
		return null;
	}
	
	public List<?> listResultCalendarioFaseFC() {
		List<CalendarioFaseFC2022> listResult = new ArrayList<CalendarioFaseFC2022>();
		for (Entry<String, ?> entry : readCalendarioFaseFC().entrySet()) {
		    Object value = entry.getValue();
		    CalendarioFaseFC2022 calendarioFaseFC = (CalendarioFaseFC2022) value;
		    if(calendarioFaseFC.getId() > 0){
		    	calendarioFaseFC.setJugador1(leerJugadores.read(calendarioFaseFC.getJugador1Id(), true));
		    	calendarioFaseFC.setJugador2(leerJugadores.read(calendarioFaseFC.getJugador2Id(), true));
		    	calendarioFaseFC.setBolera(leerBoleras.read(calendarioFaseFC.getBoleraId()));
		    	//Calcular icono victoria, empate y derrota
		    	Integer resultadoPartida1 =  calendarioFaseFC.getJuegosJugador1P1() - calendarioFaseFC.getJuegosJugador2P1();
		    	Integer resultadoPartida2 =  calendarioFaseFC.getJuegosJugador1P2() - calendarioFaseFC.getJuegosJugador2P2();
		    	Integer resultadoPartida3 =  calendarioFaseFC.getJuegosJugador1P3() - calendarioFaseFC.getJuegosJugador2P3();
		    	
		    	//Sin Jugar
		    	if(resultadoPartida1 == 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
	    			calendarioFaseFC.setRequipo1("/resources/sinjugar.ico");
	    			calendarioFaseFC.setRequipo2("/resources/sinjugar.ico");
		    	}else
		    	//Ganador 1 solo una partida
		    	if(resultadoPartida1 > 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
		    		calendarioFaseFC.setRequipo1("/resources/ganador.ico");
	    			calendarioFaseFC.setRequipo2("/resources/derrota.ico");
		    	}else 
		    	//Ganador 2 solo una partida
		    	if(resultadoPartida1 < 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
		    		calendarioFaseFC.setRequipo1("/resources/derrota.ico");
	    			calendarioFaseFC.setRequipo2("/resources/ganador.ico");
		    	}else 
		    	//Ganador 1 dos partidas
		    	if( (resultadoPartida1 > 0 && resultadoPartida2 > 0) ||
	    			(resultadoPartida1 > 0 && resultadoPartida3 > 0) ||
	    			(resultadoPartida2 > 0 && resultadoPartida3 > 0)){
		    		calendarioFaseFC.setRequipo1("/resources/ganador.ico");
	    			calendarioFaseFC.setRequipo2("/resources/derrota.ico");
		    	}else
		    	//Ganador 2 dos partidas
		    	if( (resultadoPartida1 < 0 && resultadoPartida2 < 0) ||
	    			(resultadoPartida1 < 0 && resultadoPartida3 < 0) ||
	    			(resultadoPartida2 < 0 && resultadoPartida3 < 0)){
		    		calendarioFaseFC.setRequipo1("/resources/derrota.ico");
	    			calendarioFaseFC.setRequipo2("/resources/ganador.ico");
		    	}else{
		    		calendarioFaseFC.setRequipo1("/resources/sinjugar.ico");
		    		calendarioFaseFC.setRequipo2("/resources/sinjugar.ico");
		    	}

		    	listResult.add(calendarioFaseFC);
		    }
		}
		return listResult;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<String, ?> readCalendarioFaseFF() {
		resultCalendarioFaseFF = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_INDIVIDUAL_MASC_PRIMERA_CAL_FASE_FF, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_INDIVIDUAL_PRIMERA);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	CalendarioFaseFF2022 calendarioFaseFF = new CalendarioFaseFF2022();
		    	calendarioFaseFF.setRowNum(row.getRecordNumber());
		    	calendarioFaseFF = CalendarioIndividualFaseFFTransformer2022.transformerObjectCsv(calendarioFaseFF, row);
		    	resultCalendarioFaseFF.put(calendarioFaseFF.getIdCruce(), calendarioFaseFF);
		    }
		}
		return resultCalendarioFaseFF;
	}
	
	@SuppressWarnings("unchecked")
	public CalendarioFaseFF2022 readCalendarioFaseFF(Integer id) {
		if(this.resultCalendarioFaseFF == null){
			resultCalendarioFaseFF = (HashMap<String, CalendarioFaseFF2022>) readCalendarioFaseFF();
		}
		if(this.resultCalendarioFaseFF != null){
			return this.resultCalendarioFaseFF.get(id);
		}
		return null;
	}
	
	public List<?> listResultCalendarioFaseFF() {
		List<CalendarioFaseFF2022> listResult = new ArrayList<CalendarioFaseFF2022>();
		for (Entry<String, ?> entry : readCalendarioFaseFF().entrySet()) {
		    Object value = entry.getValue();
		    CalendarioFaseFF2022 calendarioFaseFF = (CalendarioFaseFF2022) value;
		    if(calendarioFaseFF.getId() > 0){
		    	calendarioFaseFF.setJugador1(leerJugadores.read(calendarioFaseFF.getJugador1Id(), true));
		    	calendarioFaseFF.setJugador2(leerJugadores.read(calendarioFaseFF.getJugador2Id(), true));
		    	calendarioFaseFF.setBolera(leerBoleras.read(calendarioFaseFF.getBoleraId()));
		    	//Calcular icono victoria, empate y derrota
		    	Integer resultadoPartida1 =  calendarioFaseFF.getJuegosJugador1P1() - calendarioFaseFF.getJuegosJugador2P1();
		    	Integer resultadoPartida2 =  calendarioFaseFF.getJuegosJugador1P2() - calendarioFaseFF.getJuegosJugador2P2();
		    	Integer resultadoPartida3 =  calendarioFaseFF.getJuegosJugador1P3() - calendarioFaseFF.getJuegosJugador2P3();
		    	
		    	//Sin Jugar
		    	if(resultadoPartida1 == 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
	    			calendarioFaseFF.setRequipo1("/resources/sinjugar.ico");
	    			calendarioFaseFF.setRequipo2("/resources/sinjugar.ico");
		    	}else
		    	//Ganador 1 solo una partida
		    	if(resultadoPartida1 > 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
		    		calendarioFaseFF.setRequipo1("/resources/ganador.ico");
	    			calendarioFaseFF.setRequipo2("/resources/derrota.ico");
		    	}else 
		    	//Ganador 2 solo una partida
		    	if(resultadoPartida1 < 0 && resultadoPartida2 == 0 && resultadoPartida3 == 0){
		    		calendarioFaseFF.setRequipo1("/resources/derrota.ico");
	    			calendarioFaseFF.setRequipo2("/resources/ganador.ico");
		    	}else 
		    	//Ganador 1 dos partidas
		    	if( (resultadoPartida1 > 0 && resultadoPartida2 > 0) ||
	    			(resultadoPartida1 > 0 && resultadoPartida3 > 0) ||
	    			(resultadoPartida2 > 0 && resultadoPartida3 > 0)){
		    		calendarioFaseFF.setRequipo1("/resources/ganador.ico");
	    			calendarioFaseFF.setRequipo2("/resources/derrota.ico");
		    	}else
		    	//Ganador 2 dos partidas
		    	if( (resultadoPartida1 < 0 && resultadoPartida2 < 0) ||
	    			(resultadoPartida1 < 0 && resultadoPartida3 < 0) ||
	    			(resultadoPartida2 < 0 && resultadoPartida3 < 0)){
		    		calendarioFaseFF.setRequipo1("/resources/derrota.ico");
	    			calendarioFaseFF.setRequipo2("/resources/ganador.ico");
		    	}else{
		    		calendarioFaseFF.setRequipo1("/resources/sinjugar.ico");
		    		calendarioFaseFF.setRequipo2("/resources/sinjugar.ico");
		    	}

		    	listResult.add(calendarioFaseFF);
		    }
		}
		return listResult;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<String, ?> readGruposFaseI() {
		resultGruposFaseI = new HashMap();
		List<ClasificacionFaseI2022> listResult = (List<ClasificacionFaseI2022>) listResultClasificacionFaseI();
		for (ClasificacionFaseI2022 clasificacionFaseI : listResult) {
			if(!resultGruposFaseI.containsKey(clasificacionFaseI.getGrupo())){
				resultGruposFaseI.put(clasificacionFaseI.getGrupo(), clasificacionFaseI.getGrupo());
			}
		}
		return resultGruposFaseI;
	}
	
	@SuppressWarnings("unchecked")
	public String readGrupoFaseI(String id) {
		if(this.resultGruposFaseI == null){
			resultGruposFaseI = (HashMap<String, String>) readGruposFaseI();
		}
		if(this.resultGruposFaseI != null){
			return this.resultGruposFaseI.get(id);
		}
		return null;
	}
	
	public List<FasesTabShow2022> listResultGruposFaseI() {
		List<FasesTabShow2022> listResult = new ArrayList<FasesTabShow2022>();
		for (Entry<String, ?> entry : readGruposFaseI().entrySet()) {
		    Object value = entry.getValue();
		    String grupo = (String) value;
		    if(grupo != null){
		    	listResult.add(FasesTabShow2022.searchFaseTabShow(FasesModelo2022.FASE_I, grupo));
		    }
		}
		return listResult;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public Object read(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HashMap<Integer, ?> readAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<?> listResult() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public boolean isEstadoCampeonatoSinCrear(){
		Configuracion2022 configuracionFechaMaxInscripcion = readConfig(CONFIG_FECHA_MAX_INSCRIPCION);
		if(configuracionFechaMaxInscripcion == null){
			return true;
		}
		return false;
	}
	
	public boolean isAbrirInscripciones(){
		Configuracion2022 configuracionFechaMaxInscripcion = readConfig(CONFIG_FECHA_MAX_INSCRIPCION);
		if(configuracionFechaMaxInscripcion != null){
			Configuracion2022 configuracionEstado = readConfig(CONFIG_ESTADO);
			if(configuracionEstado != null && configuracionEstado.getValor() != null && configuracionEstado.getValor().equals(EstadosIndividualParejas2022.CONFIG_ESTADO_CREAR)){
				return true;
			}
		}
		return false;
	}
	
	public boolean isCerrarInscripciones(){
		Configuracion2022 configuracionFechaMaxInscripcion = readConfig(CONFIG_FECHA_MAX_INSCRIPCION);
		if(configuracionFechaMaxInscripcion != null){
			Configuracion2022 configuracionEstado = readConfig(CONFIG_ESTADO);
			if(configuracionEstado != null && configuracionEstado.getValor() != null && configuracionEstado.getValor().equals(EstadosIndividualParejas2022.CONFIG_ESTADO_ABRIR_INSCRIPCIONES)){
				return true;
			}
		}
		return false;
	}
	
	public boolean isInscripcionesCerradas(){
		Configuracion2022 configuracionFechaMaxInscripcion = readConfig(CONFIG_FECHA_MAX_INSCRIPCION);
		if(configuracionFechaMaxInscripcion != null){
			Configuracion2022 configuracionEstado = readConfig(CONFIG_ESTADO);
			if(configuracionEstado != null && configuracionEstado.getValor() != null && configuracionEstado.getValor().equals(EstadosIndividualParejas2022.CONFIG_ESTADO_CERRAR_INSCRIPCIONES)){
				return true;
			}
		}
		return false;
	}
	
	public boolean isFasesCerradas(){
		Configuracion2022 configuracionFechaMaxInscripcion = readConfig(CONFIG_FECHA_MAX_INSCRIPCION);
		if(configuracionFechaMaxInscripcion != null){
			Configuracion2022 configuracionEstado = readConfig(CONFIG_ESTADO);
			if(configuracionEstado != null && configuracionEstado.getValor() != null && configuracionEstado.getValor().equals(EstadosIndividualParejas2022.CONFIG_ESTADO_CERRAR_FASES)){
				return true;
			}
		}
		return false;
	}
	
	public boolean isSorteoRealizado(){
		Configuracion2022 configuracionFechaMaxInscripcion = readConfig(CONFIG_FECHA_MAX_INSCRIPCION);
		if(configuracionFechaMaxInscripcion != null){
			Configuracion2022 configuracionEstado = readConfig(CONFIG_ESTADO);
			if(configuracionEstado != null && configuracionEstado.getValor() != null && configuracionEstado.getValor().equals(EstadosIndividualParejas2022.CONFIG_ESTADO_SORTEADO_CAMPEONATO)){
				return true;
			}
		}
		return false;
	}
	
	public boolean isSorteoFinalizado(){
		Configuracion2022 configuracionFechaMaxInscripcion = readConfig(CONFIG_FECHA_MAX_INSCRIPCION);
		if(configuracionFechaMaxInscripcion != null){
			Configuracion2022 configuracionEstado = readConfig(CONFIG_ESTADO);
			if(configuracionEstado != null && configuracionEstado.getValor() != null && configuracionEstado.getValor().equals(EstadosIndividualParejas2022.CONFIG_ESTADO_FINALIZADO)){
				return true;
			}
		}
		return false;
	}
	
	public String getEstadoCampeonato(){
		Configuracion2022 configuracionEstado = readConfig(CONFIG_ESTADO);
		if(configuracionEstado != null && configuracionEstado.getValor().equals(EstadosIndividualParejas2022.CONFIG_ESTADO_CREAR)){
			return "Creado";
		}else if(configuracionEstado != null && configuracionEstado.getValor().equals(EstadosIndividualParejas2022.CONFIG_ESTADO_ABRIR_INSCRIPCIONES)){
			return "Inscripciones abiertas";
		}else if(configuracionEstado != null && configuracionEstado.getValor().equals(EstadosIndividualParejas2022.CONFIG_ESTADO_CERRAR_INSCRIPCIONES)){
			return "Inscripciones cerradas";
		}else if(configuracionEstado != null && configuracionEstado.getValor().equals(EstadosIndividualParejas2022.CONFIG_ESTADO_CERRAR_FASES)){
			return "Fases cerradas";
		}else if(configuracionEstado != null && configuracionEstado.getValor().equals(EstadosIndividualParejas2022.CONFIG_ESTADO_SORTEADO_CAMPEONATO)){
			return "Campeonato sorteado";
		}else if(configuracionEstado != null && configuracionEstado.getValor().equals(EstadosIndividualParejas2022.CONFIG_ESTADO_FINALIZADO)){
			return "Finalizado";
		}
		return "Sin crear";
	}
	
	public String getBoleraFinal(){
		Configuracion2022 configuracionEstado = readConfig(CONFIG_BOLERA_FINAL);
		if(configuracionEstado != null){
			return configuracionEstado.getValor();
		}
		return "";
	}
	
	public String getObservacionesCampeonato1(){
		Configuracion2022 configuracionEstado = readConfig(CONFIG_OBSERVACIONES_CAMPEONATO_1);
		if(configuracionEstado != null){
			return configuracionEstado.getValor();
		}
		return "";
	}

	public String getObservacionesCampeonato2(){
		Configuracion2022 configuracionEstado = readConfig(CONFIG_OBSERVACIONES_CAMPEONATO_2);
		if(configuracionEstado != null){
			return configuracionEstado.getValor();
		}
		return "";
	}
	
	public String getObservacionesCampeonato3(){
		Configuracion2022 configuracionEstado = readConfig(CONFIG_OBSERVACIONES_CAMPEONATO_3);
		if(configuracionEstado != null){
			return configuracionEstado.getValor();
		}
		return "";
	}
	
	public String getObservacionesCampeonato4(){
		Configuracion2022 configuracionEstado = readConfig(CONFIG_OBSERVACIONES_CAMPEONATO_4);
		if(configuracionEstado != null){
			return configuracionEstado.getValor();
		}
		return "";
	}
	
	public String getObservacionesCampeonato5(){
		Configuracion2022 configuracionEstado = readConfig(CONFIG_OBSERVACIONES_CAMPEONATO_5);
		if(configuracionEstado != null){
			return configuracionEstado.getValor();
		}
		return "";
	}
	
	public String getObservacionesCampeonato6(){
		Configuracion2022 configuracionEstado = readConfig(CONFIG_OBSERVACIONES_CAMPEONATO_6);
		if(configuracionEstado != null){
			return configuracionEstado.getValor();
		}
		return "";
	}
	
	public String getObservacionesCampeonato7(){
		Configuracion2022 configuracionEstado = readConfig(CONFIG_OBSERVACIONES_CAMPEONATO_7);
		if(configuracionEstado != null){
			return configuracionEstado.getValor();
		}
		return "";
	}
	
	public String getBolerasOcupadasFI(){
		Configuracion2022 configuracionEstado = readConfig(CONFIG_BOLERAS_OCUPADAS_FI);
		if(configuracionEstado != null){
			return configuracionEstado.getValor();
		}
		return "";
	}
	
	public String getBolerasOcupadasOFCF(){
		Configuracion2022 configuracionEstado = readConfig(CONFIG_BOLERAS_OCUPADAS_OFCF);
		if(configuracionEstado != null){
			return configuracionEstado.getValor();
		}
		return "";
	}
	
	
	public Date getFechaMaxInscripcion(){
		Configuracion2022 configuracionFechaMaxInscripcion = readConfig(CONFIG_FECHA_MAX_INSCRIPCION);
		if(configuracionFechaMaxInscripcion != null && configuracionFechaMaxInscripcion.getValor() != null){
		    String date = configuracionFechaMaxInscripcion.getValor();
		    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy"); // date pattern
		    try {
				return simpleDateFormat.parse(date);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
			} 
		}
		return null;
	}
	
	public List<?> listResultParticipantesOrderByEquipo() {
		List<ParticipantesIndividual2022> listResult = new ArrayList<ParticipantesIndividual2022>();
		for (Entry<Integer, ?> entry : readParticipantes().entrySet()) {
		    Object value = entry.getValue();
		    ParticipantesIndividual2022 participantesIndividual = (ParticipantesIndividual2022) value;
		    if(participantesIndividual.getId() > 0){
		    	if(participantesIndividual.getActivo().equalsIgnoreCase(Activo2022.SI)){
			    	if(participantesIndividual.getIdJugador() != null){
				    	participantesIndividual.setJugador(leerJugadores.read(participantesIndividual.getIdJugador(), true));
				    }
			    	listResult.add(participantesIndividual);
		    	}
		    	
		    }
		}
		
		//Ordenar la clasificacion
		Collections.sort(listResult, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				ParticipantesIndividual2022 pi1 = (ParticipantesIndividual2022) o1;
				ParticipantesIndividual2022 pi2 = (ParticipantesIndividual2022) o2;
				
				int requipo = pi1.getJugador().getEquipoId().compareTo(pi2.getJugador().getEquipoId());
				return requipo;
			}
		});
		
		return listResult;
	}
	


}
