package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas2022;
import com.bolocelta.bbdd.constants.Ubicaciones2022;
import com.bolocelta.entities.CampeonatoEquiposCalendario2022;
import com.bolocelta.entities.CampeonatoEquiposClasificacion2022;
import com.bolocelta.transformer.CampeonatoEquiposTransformer2022;

public class LeerCampeonatoEquiposPrimera2022 extends ALeer2022 {
	
	private LeerEquipos2022 leerEquipos = new LeerEquipos2022();
	private LeerCategorias2022 leerCategorias = new LeerCategorias2022();
	private LeerBoleras2022 leerBoleras = new LeerBoleras2022();

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<Integer, ?> readAllClasificacion() {
		HashMap<Integer, CampeonatoEquiposClasificacion2022> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CLA_PRIMERA, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_EQUIPOS);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	CampeonatoEquiposClasificacion2022 campeonatoEquiposClasificacion = new CampeonatoEquiposClasificacion2022();
		    	campeonatoEquiposClasificacion.setRowNum(row.getRecordNumber());
		    	campeonatoEquiposClasificacion = CampeonatoEquiposTransformer2022.transformerObjectClasificacionCsv(campeonatoEquiposClasificacion, row);
		    	result.put(campeonatoEquiposClasificacion.getId(), campeonatoEquiposClasificacion);
		    }
		}
		return result;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<Integer, ?> readAllCalendario() {
		HashMap<Integer, CampeonatoEquiposCalendario2022> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CAMPEONATO_EQUIPOS_CAL_PRIMERA, Ubicaciones2022.UBICACION_BBDD_CAMPEONATO_EQUIPOS);
		if(csvParser != null){
			for (CSVRecord row : csvParser) {
		    	CampeonatoEquiposCalendario2022 campeonatoEquiposCalendario = new CampeonatoEquiposCalendario2022();
		    	campeonatoEquiposCalendario.setRowNum(row.getRecordNumber());
		    	campeonatoEquiposCalendario = CampeonatoEquiposTransformer2022.transformerObjectCalendarioCsv(campeonatoEquiposCalendario, row);
		    	result.put(campeonatoEquiposCalendario.getId(), campeonatoEquiposCalendario);
		    }
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public CampeonatoEquiposClasificacion2022 readClasificacion(Integer id) {
		HashMap<Integer, CampeonatoEquiposClasificacion2022> result = (HashMap<Integer, CampeonatoEquiposClasificacion2022>) readAllClasificacion();
		return result.get(id);
	}
	
	@SuppressWarnings("unchecked")
	public CampeonatoEquiposCalendario2022 readCalendario(Integer id) {
		HashMap<Integer, CampeonatoEquiposCalendario2022> result = (HashMap<Integer, CampeonatoEquiposCalendario2022>) readAllCalendario();
		return result.get(id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object read(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HashMap<Integer, ?> readAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<?> listResult() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public List<?> listResultClasificacion() {
		List<CampeonatoEquiposClasificacion2022> listResult = new ArrayList<CampeonatoEquiposClasificacion2022>();
		for (Entry<Integer, ?> entry : readAllClasificacion().entrySet()) {
		    Object value = entry.getValue();
		    CampeonatoEquiposClasificacion2022 cec = (CampeonatoEquiposClasificacion2022) value;
		    if(cec.getId() > 0){
		    	if(cec.getEquipoId() != null){
		    		cec.setEquipo(leerEquipos.read(cec.getEquipoId()));
		    	}
		    	if(cec.getCategoriaId() != null){
		    		cec.setCategoria(leerCategorias.read(cec.getCategoriaId()));
		    	}
		    	listResult.add((CampeonatoEquiposClasificacion2022) cec);
		    }
		}
		
		//Ordenar la clasificacion
		Collections.sort(listResult, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				CampeonatoEquiposClasificacion2022 cec1 = (CampeonatoEquiposClasificacion2022) o1;
				CampeonatoEquiposClasificacion2022 cec2 = (CampeonatoEquiposClasificacion2022) o2;
				
				int rpuntos = cec1.getPuntos().compareTo(cec2.getPuntos());
				if (rpuntos == 0) {
					Integer part1 = cec1.getPartidasFavor() - cec1.getPartidasContra();
					Integer part2 = cec2.getPartidasFavor() - cec2.getPartidasContra();
					int rdifpartidas = part1.compareTo(part2);
					if (rdifpartidas == 0) {
						return cec1.getPartidasFavor().compareTo(cec2.getPartidasFavor());
					}
					return rdifpartidas;
				}
				return rpuntos;
			}
		});
		
		Collections.reverse(listResult);
		
		return listResult;
	}
	
	public List<?> listResultCalendario() {
		List<CampeonatoEquiposCalendario2022> listResult = new ArrayList<CampeonatoEquiposCalendario2022>();
		for (Entry<Integer, ?> entry : readAllCalendario().entrySet()) {
		    Object value = entry.getValue();
		    CampeonatoEquiposCalendario2022 cec = (CampeonatoEquiposCalendario2022) value;
		    if(cec.getId() > 0){
		    	if(cec.getEquipo1Id() != null){
		    		cec.setEquipo1(leerEquipos.read(cec.getEquipo1Id()));
		    		if(cec.getEquipo1().getBoleraId() != null){
		    			cec.getEquipo1().setBolera(leerBoleras.read(cec.getEquipo1().getBoleraId()));
			    	}
		    	}
		    	if(cec.getEquipo2Id() != null){
		    		cec.setEquipo2(leerEquipos.read(cec.getEquipo2Id()));
		    		if(cec.getEquipo2().getBoleraId() != null){
		    			cec.getEquipo2().setBolera(leerBoleras.read(cec.getEquipo2().getBoleraId()));
			    	}
		    	}
		    	if(cec.getCategoriaId() != null){
		    		cec.setCategoria(leerCategorias.read(cec.getCategoriaId()));
		    	}
		    	if(cec.getPgequipo1() != null && cec.getPgequipo2() != null){
		    		if(cec.getPgequipo1() == 0 && cec.getPgequipo2() == 0){
		    			cec.setRequipo1("/resources/sinjugar.ico");
		    			cec.setRequipo2("/resources/sinjugar.ico");
		    		}else if(cec.getPgequipo1() > cec.getPgequipo2()){
		    			cec.setRequipo1("/resources/ganador.ico");
		    			cec.setRequipo2("/resources/derrota.ico");
		    		}else if(cec.getPgequipo1() < cec.getPgequipo2()){
		    			cec.setRequipo1("/resources/derrota.ico");
		    			cec.setRequipo2("/resources/ganador.ico");
		    		}else if(cec.getPgequipo1().equals(cec.getPgequipo2())){
		    			cec.setRequipo1("/resources/empate.ico");
		    			cec.setRequipo2("/resources/empate.ico");
		    		}
		    		
		    	}else{
		    		cec.setRequipo1("/resources/sinjugar.ico");
	    			cec.setRequipo2("/resources/sinjugar.ico");
		    	}
		    	listResult.add((CampeonatoEquiposCalendario2022) cec);
		    }
		}
		
		//Ordenar la clasificacion
		Collections.sort(listResult, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				CampeonatoEquiposCalendario2022 cec1 = (CampeonatoEquiposCalendario2022) o1;
				CampeonatoEquiposCalendario2022 cec2 = (CampeonatoEquiposCalendario2022) o2;
				
				int rjornada = cec1.getJornada().compareTo(cec2.getJornada());
				return rjornada;
			}
		});
		
		return listResult;
	}
	
//	//Generacion de los PDF para descarga el calendario
//	
//	public void generaPDF(){
//		
//		
//	
//		
//		try {
//			// Se crea el documento
//			Document documento = new Document();
//			// Se crea el OutputStream para el fichero donde queremos dejar el pdf.
//			FileOutputStream ficheroPdf = new FileOutputStream("fichero.pdf");
//			
//			// Se asocia el documento al OutputStream y se indica que el espaciado entre
//			// lineas sera de 20. Esta llamada debe hacerse antes de abrir el documento
//			PdfWriter.getInstance(documento,ficheroPdf).setInitialLeading(20);
//			
//			// A�adir parrafos
//			documento.add(new Paragraph("Esto es el primer p�rrafo, normalito"));
//
//			documento.add(new Paragraph("Este es el segundo y tiene una fuente rara",
//							FontFactory.getFont("arial",   // fuente
//							22,                            // tama�o
//							Font.ITALIC,                   // estilo
//							BaseColor.CYAN)));             // color
//			
//			// A�adir imagenes
////			try
////			{
////				Image foto = Image.getInstance("pingu.png");
////				foto.scaleToFit(100, 100);
////				foto.setAlignment(Chunk.ALIGN_MIDDLE);
////				documento.add(foto);
////			}
////			catch ( Exception e )
////			{
////				e.printStackTrace();
////			}
//			
//			// A�adir tabla
//			PdfPTable tabla = new PdfPTable(3);
//			for (int i = 0; i < 15; i++)
//			{
//				tabla.addCell("celda " + i);
//			}
//			documento.add(tabla);
//		
//			// Se abre el documento.
//			documento.open();
//			
//			documento.close();
//			
//			//Descargar
//			
//		} catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//	}

}
