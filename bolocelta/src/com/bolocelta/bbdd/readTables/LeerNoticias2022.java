package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.inject.Named;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas2022;
import com.bolocelta.bbdd.constants.Ubicaciones2022;
import com.bolocelta.entities.Noticias2022;
import com.bolocelta.transformer.NoticiasTransformer2022;

@Named
public class LeerNoticias2022 extends ALeer2022 {
	
	HashMap<Integer, Noticias2022> result = null;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, Noticias2022> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_NOTICIAS, Ubicaciones2022.UBICACION_BBDD_NOTICIAS);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	Noticias2022 noticias = new Noticias2022();
		    	noticias = NoticiasTransformer2022.transformerObjectCsv(noticias, row);
		    	result.put(noticias.getId(), noticias);
		    }
		}
	    return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Noticias2022 read(Integer id) {
		if(this.result == null){
			result = (HashMap<Integer, Noticias2022>) readAll();
		}
		return this.result.get(id);
	}
	
	@Override
	public List<?> listResult() {
		List<Noticias2022> listResult = new ArrayList<Noticias2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    Noticias2022 noticias = (Noticias2022) value;
		    if(noticias.getId() > 0){
		    	listResult.add(noticias);
		    }
		}
		return listResult;
	}

}
