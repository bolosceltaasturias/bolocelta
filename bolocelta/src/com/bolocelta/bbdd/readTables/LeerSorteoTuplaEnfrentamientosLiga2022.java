package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas2022;
import com.bolocelta.bbdd.constants.Ubicaciones2022;
import com.bolocelta.entities.sorteos.liga.SorteoTuplaEnfrentamientosLiga2022;
import com.bolocelta.transformer.SorteoTuplaEnfrentamientosLigaTransformer2022;

public class LeerSorteoTuplaEnfrentamientosLiga2022 extends ALeer2022 {

	private final String nameFile = NombresTablas2022.N_CSV_BBDD_SORTEO_LIGA_CALENDARIO_CRUCES;
	private final String path = Ubicaciones2022.UBICACION_BBDD_ADMINISTRACION;
	
	HashMap<Integer, SorteoTuplaEnfrentamientosLiga2022> result = null;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, SorteoTuplaEnfrentamientosLiga2022> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(nameFile, path);
		if(csvParser != null){    
			for (CSVRecord row : csvParser) {
		    	SorteoTuplaEnfrentamientosLiga2022 sorteoTuplaEnfrentamientosLiga = new SorteoTuplaEnfrentamientosLiga2022();
	    		sorteoTuplaEnfrentamientosLiga = SorteoTuplaEnfrentamientosLigaTransformer2022.transformerObjectCsv(sorteoTuplaEnfrentamientosLiga, row);
		    	result.put(sorteoTuplaEnfrentamientosLiga.getId(), sorteoTuplaEnfrentamientosLiga);
		    }
		}
		return result;
	}
	
	public Long getLastRowSheet(){
		return lastRowSheet(nameFile, path);
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public SorteoTuplaEnfrentamientosLiga2022 read(Integer id) {
		SorteoTuplaEnfrentamientosLiga2022 sorteoTuplaEnfrentamientosLiga = null;
		if(sorteoTuplaEnfrentamientosLiga == null){
			if(this.result == null){
				this.result = (HashMap<Integer, SorteoTuplaEnfrentamientosLiga2022>) readAll();
			}
			sorteoTuplaEnfrentamientosLiga = result.get(id);
		}
		return sorteoTuplaEnfrentamientosLiga;
	}
	
	public void refreshList(){
		this.result = null;
	}
	
	@SuppressWarnings("unchecked")
	public List<SorteoTuplaEnfrentamientosLiga2022> readJornadaIda(Integer id) {
		List<SorteoTuplaEnfrentamientosLiga2022> sorteoTuplaEnfrentamientosLigaByCategoria = new ArrayList<>();
		List<SorteoTuplaEnfrentamientosLiga2022> allSorteoTuplaEnfrentamientosLiga = (List<SorteoTuplaEnfrentamientosLiga2022>) listResult();
		for (SorteoTuplaEnfrentamientosLiga2022 stel : allSorteoTuplaEnfrentamientosLiga) {
			if(stel.getJornadaIda() != null && stel.getJornadaIda().equals(id)){
				sorteoTuplaEnfrentamientosLigaByCategoria.add(stel);
			}
		}
		return sorteoTuplaEnfrentamientosLigaByCategoria;
	}

	@Override
	public List<?> listResult() {
		List<SorteoTuplaEnfrentamientosLiga2022> listResult = new ArrayList<SorteoTuplaEnfrentamientosLiga2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    SorteoTuplaEnfrentamientosLiga2022 sorteoTuplaEnfrentamientosLiga = (SorteoTuplaEnfrentamientosLiga2022) value;
		    if(sorteoTuplaEnfrentamientosLiga.getId() != null && sorteoTuplaEnfrentamientosLiga.getId() > 0){
		    	listResult.add((SorteoTuplaEnfrentamientosLiga2022) sorteoTuplaEnfrentamientosLiga);
		    }
		}
		return listResult;
	}

}
