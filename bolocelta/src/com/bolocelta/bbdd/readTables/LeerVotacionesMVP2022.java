package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas2022;
import com.bolocelta.bbdd.constants.Ubicaciones2022;
import com.bolocelta.entities.PremiosMVP2022;
import com.bolocelta.entities.VotacionesMVP2022;
import com.bolocelta.transformer.VotacionesMVPTransformer2022;

public class LeerVotacionesMVP2022 extends ALeer2022 {

	private LeerJugadores2022 leerJugadores = new LeerJugadores2022();
	private LeerEquipos2022 leerEquipos = new LeerEquipos2022();
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<Integer, ?> readAllVotaciones() {
		HashMap<Integer, VotacionesMVP2022> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_VOTACIONES_MVP, Ubicaciones2022.UBICACION_BBDD_PREMIOS);
		if(csvParser != null){
			for (CSVRecord row : csvParser) {
				VotacionesMVP2022 votacionesMVP = new VotacionesMVP2022();
		    	votacionesMVP.setRowNum(row.getRecordNumber());
		    	votacionesMVP = VotacionesMVPTransformer2022.transformerObjectCalendarioCsv(votacionesMVP, row);
		    	result.put(votacionesMVP.getId(), votacionesMVP);
		    }
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public VotacionesMVP2022 readPremio(Integer id) {
		HashMap<Integer, VotacionesMVP2022> result = (HashMap<Integer, VotacionesMVP2022>) readAllVotaciones();
		return result.get(id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object read(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HashMap<Integer, ?> readAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<?> listResult() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public List<?> listResultVotaciones(Integer categoria) {
		
		List<VotacionesMVP2022> listResult = new ArrayList<VotacionesMVP2022>();
		
		for (Entry<Integer, ?> entry : readAllVotaciones().entrySet()) {
		    Object value = entry.getValue();
		    VotacionesMVP2022 cec = (VotacionesMVP2022) value;
		    if(categoria == cec.getCategoria()){
		    	cec.setJugador(leerJugadores.read(cec.getIdJugador(), true));
		    	listResult.add(cec);
		    }
		}

		return listResult;
	}

}
