package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.inject.Named;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.application.enumerations.ModalidadJuegoEnumeration2022;
import com.bolocelta.bbdd.constants.NombresTablas2022;
import com.bolocelta.bbdd.constants.Ubicaciones2022;
import com.bolocelta.entities.HistoricoCampeonatosAsturias2022;
import com.bolocelta.transformer.HistoricoCampeonatoAsturiasTransformer2022;

@Named
public class LeerHistoricoCampeonatosAsturias2022 extends ALeer2022 {

	HashMap<Integer, HistoricoCampeonatosAsturias2022> result = null;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, HistoricoCampeonatosAsturias2022> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_HISTORICO_ASTURIAS, Ubicaciones2022.UBICACION_BBDD_HISTORICO);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	HistoricoCampeonatosAsturias2022 historicoCA = new HistoricoCampeonatosAsturias2022();
	    		historicoCA = HistoricoCampeonatoAsturiasTransformer2022.transformerObjectCsv(historicoCA, row);
		    	result.put(historicoCA.getId(), historicoCA);
		    }
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public HistoricoCampeonatosAsturias2022 read(Integer id) {
		if(this.result == null){
			this.result = (HashMap<Integer, HistoricoCampeonatosAsturias2022>) readAll();
		}
		return result.get(id);
	}

	@Override
	public List<?> listResult() {
		List<HistoricoCampeonatosAsturias2022> listResult = new ArrayList<HistoricoCampeonatosAsturias2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    listResult.add((HistoricoCampeonatosAsturias2022) value);
		}
		return listResult;
	}
	
	public List<?> listResultEquipos() {
		List<HistoricoCampeonatosAsturias2022> listResult = new ArrayList<HistoricoCampeonatosAsturias2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    HistoricoCampeonatosAsturias2022 historicoCA = (HistoricoCampeonatosAsturias2022) value;
		    if(historicoCA.getTipo() == ModalidadJuegoEnumeration2022.MODALIDAD_EQUIPOS){
		    	listResult.add((HistoricoCampeonatosAsturias2022) value);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultIndividual() {
		List<HistoricoCampeonatosAsturias2022> listResult = new ArrayList<HistoricoCampeonatosAsturias2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    HistoricoCampeonatosAsturias2022 historicoCA = (HistoricoCampeonatosAsturias2022) value;
		    if(historicoCA.getTipo() == ModalidadJuegoEnumeration2022.MODALIDAD_INDIVIDUAL){
		    	listResult.add((HistoricoCampeonatosAsturias2022) value);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultParejas() {
		List<HistoricoCampeonatosAsturias2022> listResult = new ArrayList<HistoricoCampeonatosAsturias2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    HistoricoCampeonatosAsturias2022 historicoCA = (HistoricoCampeonatosAsturias2022) value;
		    if(historicoCA.getTipo() == ModalidadJuegoEnumeration2022.MODALIDAD_PAREJAS){
		    	listResult.add((HistoricoCampeonatosAsturias2022) value);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultIndividualFemenino() {
		List<HistoricoCampeonatosAsturias2022> listResult = new ArrayList<HistoricoCampeonatosAsturias2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    HistoricoCampeonatosAsturias2022 historicoCA = (HistoricoCampeonatosAsturias2022) value;
		    if(historicoCA.getTipo() == ModalidadJuegoEnumeration2022.MODALIDAD_INDIVIDUAL_FEMENINO){
		    	listResult.add((HistoricoCampeonatosAsturias2022) value);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultParejasFemenino() {
		List<HistoricoCampeonatosAsturias2022> listResult = new ArrayList<HistoricoCampeonatosAsturias2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    HistoricoCampeonatosAsturias2022 historicoCA = (HistoricoCampeonatosAsturias2022) value;
		    if(historicoCA.getTipo() == ModalidadJuegoEnumeration2022.MODALIDAD_PAREJAS_FEMENINO){
		    	listResult.add((HistoricoCampeonatosAsturias2022) value);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultMixto() {
		List<HistoricoCampeonatosAsturias2022> listResult = new ArrayList<HistoricoCampeonatosAsturias2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    HistoricoCampeonatosAsturias2022 historicoCA = (HistoricoCampeonatosAsturias2022) value;
		    if(historicoCA.getTipo() == ModalidadJuegoEnumeration2022.MODALIDAD_MIXTO){
		    	listResult.add((HistoricoCampeonatosAsturias2022) value);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultVeteranos() {
		List<HistoricoCampeonatosAsturias2022> listResult = new ArrayList<HistoricoCampeonatosAsturias2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    HistoricoCampeonatosAsturias2022 historicoCA = (HistoricoCampeonatosAsturias2022) value;
		    if(historicoCA.getTipo() == ModalidadJuegoEnumeration2022.MODALIDAD_VETERANOS){
		    	listResult.add((HistoricoCampeonatosAsturias2022) value);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultJuveniles() {
		List<HistoricoCampeonatosAsturias2022> listResult = new ArrayList<HistoricoCampeonatosAsturias2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    HistoricoCampeonatosAsturias2022 historicoCA = (HistoricoCampeonatosAsturias2022) value;
		    if(historicoCA.getTipo() == ModalidadJuegoEnumeration2022.MODALIDAD_JUVENILES){
		    	listResult.add((HistoricoCampeonatosAsturias2022) value);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultParejasJuveniles() {
		List<HistoricoCampeonatosAsturias2022> listResult = new ArrayList<HistoricoCampeonatosAsturias2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    HistoricoCampeonatosAsturias2022 historicoCA = (HistoricoCampeonatosAsturias2022) value;
		    if(historicoCA.getTipo() == ModalidadJuegoEnumeration2022.MODALIDAD_PAREJAS_JUVENILES){
		    	listResult.add((HistoricoCampeonatosAsturias2022) value);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultParejasCadetes() {
		List<HistoricoCampeonatosAsturias2022> listResult = new ArrayList<HistoricoCampeonatosAsturias2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    HistoricoCampeonatosAsturias2022 historicoCA = (HistoricoCampeonatosAsturias2022) value;
		    if(historicoCA.getTipo() == ModalidadJuegoEnumeration2022.MODALIDAD_PAREJAS_CADETES){
		    	listResult.add((HistoricoCampeonatosAsturias2022) value);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultCadetes() {
		List<HistoricoCampeonatosAsturias2022> listResult = new ArrayList<HistoricoCampeonatosAsturias2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    HistoricoCampeonatosAsturias2022 historicoCA = (HistoricoCampeonatosAsturias2022) value;
		    if(historicoCA.getTipo() == ModalidadJuegoEnumeration2022.MODALIDAD_CADETES){
		    	listResult.add((HistoricoCampeonatosAsturias2022) value);
		    }
		}
		return listResult;
	}


}
