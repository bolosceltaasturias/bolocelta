package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.Modalidad2022;
import com.bolocelta.bbdd.constants.NombresTablas2022;
import com.bolocelta.bbdd.constants.Ubicaciones2022;
import com.bolocelta.entities.CalendarioAsturias2022;
import com.bolocelta.entities.CalendarioEspana2022;
import com.bolocelta.transformer.CalendarioAsturiasTransformer2022;

public class LeerCalendarioAsturias2022 extends ALeer2022 {

	private LeerCategorias2022 leerCategorias = new LeerCategorias2022();
	private LeerModalidades2022 leerModalidades = new LeerModalidades2022();
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, CalendarioAsturias2022> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CALENDARIO_AST, Ubicaciones2022.UBICACION_BBDD_ADMINISTRACION);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
	    		CalendarioAsturias2022 calendarioAsturias = new CalendarioAsturias2022();
	    		calendarioAsturias = CalendarioAsturiasTransformer2022.transformerObjectCsv(calendarioAsturias, row);
		    	result.put(calendarioAsturias.getId(), calendarioAsturias);
		    }
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public CalendarioAsturias2022 read(Integer id) {
		HashMap<Integer, CalendarioAsturias2022> result = (HashMap<Integer, CalendarioAsturias2022>) readAll();
		return result.get(id);
	}

	@Override
	public List<?> listResult() {
		List<CalendarioAsturias2022> listResult = new ArrayList<CalendarioAsturias2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    CalendarioAsturias2022 calendarioAsturias = (CalendarioAsturias2022) value;
		    if(calendarioAsturias.getId() > 0){
		    	if(calendarioAsturias.getCategoriaId() != null){
		    		calendarioAsturias.setCategoria(leerCategorias.read(calendarioAsturias.getCategoriaId()));
		    	}
		    	if(calendarioAsturias.getModalidadId() != null){
		    		calendarioAsturias.setModalidad(leerModalidades.read(calendarioAsturias.getModalidadId()));
		    	}
		    	listResult.add((CalendarioAsturias2022) calendarioAsturias);
		    }
		}
		
		
		//Ordenar el calendario
		Collections.sort(listResult, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				CalendarioAsturias2022 cec1 = (CalendarioAsturias2022) o1;
				CalendarioAsturias2022 cec2 = (CalendarioAsturias2022) o2;
				
				int comp1 = cec1.getFechaDesde().compareTo(cec2.getFechaDesde());
				if (comp1 == 0) {
					int comp2 = cec1.getFechaHasta().compareTo(cec2.getFechaHasta());
					if (comp2 == 0) {
						return cec1.getId().compareTo(cec2.getId());
					}
					return comp2;
				}
				return comp1;
			}
		});
		
		
		return listResult;
	}
	
	public List<?> listResultLiga() {
		List<CalendarioAsturias2022> listResult = new ArrayList<CalendarioAsturias2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    CalendarioAsturias2022 calendarioAsturias = (CalendarioAsturias2022) value;
		    if(calendarioAsturias.getId() > 0){
		    	if(calendarioAsturias.getModalidadId() == Modalidad2022.EQUIPOS_LIGA){
		    		if(calendarioAsturias.getCategoriaId() != null){
			    		calendarioAsturias.setCategoria(leerCategorias.read(calendarioAsturias.getCategoriaId()));
			    	}
			    	if(calendarioAsturias.getModalidadId() != null){
			    		calendarioAsturias.setModalidad(leerModalidades.read(calendarioAsturias.getModalidadId()));
			    	}
			    	listResult.add((CalendarioAsturias2022) calendarioAsturias);
		    	}
		    }
		}
		return listResult;
	}

}
