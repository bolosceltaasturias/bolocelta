package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas2022;
import com.bolocelta.bbdd.constants.Ubicaciones2022;
import com.bolocelta.entities.CampeonatoEquiposClasificacion2022;
import com.bolocelta.entities.Jugadores2022;
import com.bolocelta.transformer.JugadoresTransformer2022;

public class LeerJugadores2022 extends ALeer2022 {

	private final String nameFile = NombresTablas2022.N_CSV_BBDD_JUGADORES;
	private final String path = Ubicaciones2022.UBICACION_BBDD_ADMINISTRACION;
	
	HashMap<Integer, Jugadores2022> result = null;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, Jugadores2022> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(nameFile, path);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
	    		Jugadores2022 jugador = new Jugadores2022();
	    		jugador.setRowNum(row.getRecordNumber());
	    		jugador = JugadoresTransformer2022.transformerObjectCsv(jugador, row);
		    	result.put(jugador.getId(), jugador);
		    }
		}
		return result;
	}
	
	public Long getLastRowSheet(){
		return lastRowSheet(nameFile, path);
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public Jugadores2022 read(Integer id) {
		Jugadores2022 jugador = null;
		if(jugador == null){
			if(this.result == null){
				this.result = (HashMap<Integer, Jugadores2022>) readAll();
			}
			jugador = result.get(id);
			if(jugador != null && jugador.getId() != null && jugador.getId() > 0){
//		    	if(jugador.getEquipoId() != null){
//		    		if(leerEquipos != null){
//		    			jugador.setEquipo(leerEquipos.read(jugador.getEquipoId()));
//		    		}
//		    	}
		    }
		}
		return jugador;
	}
	
	@SuppressWarnings("unchecked")
	public Jugadores2022 read(Integer id, boolean readTeam) {
		Jugadores2022 jugador = null;
		if(jugador == null){
			if(this.result == null){
				this.result = (HashMap<Integer, Jugadores2022>) readAll();
			}
			jugador = result.get(id);
			if(jugador != null && jugador.getId() != null && jugador.getId() > 0){
				if(readTeam){
			    	if(jugador.getEquipoId() != null){
			    		LeerEquipos2022 leerEquipos = new LeerEquipos2022();
			    		if(leerEquipos != null){
			    			jugador.setEquipo(leerEquipos.read(jugador.getEquipoId()));
			    		}
			    	}
				}
		    }
		}
		return jugador;
	}
	
	public void refreshList(){
		this.result = null;
	}
	
	@SuppressWarnings("unchecked")
	public List<Jugadores2022> readEquipo(Integer id) {
		List<Jugadores2022> jugadoresEquipo = new ArrayList<>();
		List<Jugadores2022> allJugadores = (List<Jugadores2022>) listResult();
		for (Jugadores2022 jugador : allJugadores) {
			if(jugador.getEquipoId() != null && jugador.getEquipoId().equals(id)){
				LeerEquipos2022 leerEquipos = new LeerEquipos2022();
	    		if(leerEquipos != null){
	    			jugador.setEquipo(leerEquipos.read(jugador.getEquipoId()));
	    		}
	    		jugadoresEquipo.add(jugador);
			}
		}
		return jugadoresEquipo;
	}

	@Override
	public List<?> listResult() {
		List<Jugadores2022> listResult = new ArrayList<Jugadores2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    Jugadores2022 jugador = (Jugadores2022) value;
		    if(jugador.getId() != null && jugador.getId() > 0){
//		    	if(jugador.getEquipoId() != null){
//		    		if(leerEquipos != null){
//		    			jugador.setEquipo(leerEquipos.read(jugador.getEquipoId()));
//		    		}
//		    	}
		    	listResult.add((Jugadores2022) jugador);
		    }
		}
		return listResult;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Jugadores2022> readCategoria(Integer id) {
		List<Jugadores2022> jugadoresCategoria = new ArrayList<>();
		List<Jugadores2022> allJugadores = (List<Jugadores2022>) listResult();
		for (Jugadores2022 jugador : allJugadores) {
			LeerEquipos2022 leerEquipos = new LeerEquipos2022();
    		if(leerEquipos != null){
    			jugador.setEquipo(leerEquipos.read(jugador.getEquipoId()));
    		}
    		if(jugador.getEquipo().getCategoriaId().equals(id) && jugador.isMasculino()){
    			jugadoresCategoria.add(jugador);
    		}
		}
		
		
		
		
		
		//Ordenar la clasificacion
		Collections.sort(jugadoresCategoria, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				Jugadores2022 jug1 = (Jugadores2022) o1;
				Jugadores2022 jug2 = (Jugadores2022) o2;
				
				int total = jug1.getTotalAcabones().compareTo(jug2.getTotalAcabones());
				if (total == 0) {
					int promedio = jug1.getPromedioAcabones().compareTo(jug2.getPromedioAcabones());
					if (promedio == 0) {
						int nombre = jug2.getNombreEquipo().compareTo(jug1.getNombreEquipo());
						return nombre;
					}else{
						return promedio;
					}
				}
				return total;
			}
		});
		
		Collections.reverse(jugadoresCategoria);
		
		
		
		return jugadoresCategoria;
	}
}
