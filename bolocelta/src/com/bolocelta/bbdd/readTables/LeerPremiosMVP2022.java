package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas2022;
import com.bolocelta.bbdd.constants.Ubicaciones2022;
import com.bolocelta.entities.CampeonatoLigaFemeninaCalendario2022;
import com.bolocelta.entities.CampeonatoLigaInfantilCalendario2022;
import com.bolocelta.entities.CampeonatoLigaInfantilClasificacion2022;
import com.bolocelta.entities.Categorias2022;
import com.bolocelta.entities.PremiosMVP2022;
import com.bolocelta.transformer.PremiosMVPTransformer2022;

public class LeerPremiosMVP2022 extends ALeer2022 {

	private LeerJugadores2022 leerJugadores = new LeerJugadores2022();
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<Integer, ?> readAllPremios() {
		HashMap<Integer, PremiosMVP2022> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_PREMIOS_MVP, Ubicaciones2022.UBICACION_BBDD_PREMIOS);
		if(csvParser != null){
			for (CSVRecord row : csvParser) {
				PremiosMVP2022 premiosMVP = new PremiosMVP2022();
		    	premiosMVP.setRowNum(row.getRecordNumber());
		    	premiosMVP = PremiosMVPTransformer2022.transformerObjectCalendarioCsv(premiosMVP, row);
		    	result.put(premiosMVP.getId(), premiosMVP);
		    }
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public PremiosMVP2022 readPremio(Integer id) {
		HashMap<Integer, PremiosMVP2022> result = (HashMap<Integer, PremiosMVP2022>) readAllPremios();
		return result.get(id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object read(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HashMap<Integer, ?> readAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<?> listResult() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public List<?> listResultPremios(Integer categoria) {
		
		List<PremiosMVP2022> listResult = new ArrayList<PremiosMVP2022>();
		
		for (Entry<Integer, ?> entry : readAllPremios().entrySet()) {
		    Object value = entry.getValue();
		    PremiosMVP2022 cec = (PremiosMVP2022) value;
		    if(categoria == cec.getCategoria()){
		    	cec.setJugador(leerJugadores.read(cec.getIdJugador(), true));
		    	listResult.add(cec);
		    }
		}

		return listResult;
	}

}
