package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.Activo2022;
import com.bolocelta.bbdd.constants.Categorias2022;
import com.bolocelta.bbdd.constants.NombresTablas2022;
import com.bolocelta.bbdd.constants.Ubicaciones2022;
import com.bolocelta.entities.Equipos2022;
import com.bolocelta.entities.Jugadores2022;
import com.bolocelta.transformer.EquiposTransformer2022;

public class LeerEquipos2022 extends ALeer2022 {
	
	private LeerCategorias2022 leerCategorias = new LeerCategorias2022();
	private LeerBoleras2022 leerBoleras = new LeerBoleras2022();
	private LeerJugadores2022 leerJugadores = new LeerJugadores2022();
	
	HashMap<Integer, Equipos2022> result = null;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, Equipos2022> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_EQUIPOS, Ubicaciones2022.UBICACION_BBDD_ADMINISTRACION);
		if(csvParser != null){    
			for (CSVRecord row : csvParser) {
	    		Equipos2022 equipo = new Equipos2022();
	    		equipo.setRowNum(row.getRecordNumber());
	    		equipo = EquiposTransformer2022.transformerObjectCsv(equipo, row);
		    	result.put(equipo.getId(), equipo);
		    }
		}
	    return result;
	}
	
	public Object read(Integer id, boolean readAditional){
		Equipos2022 equipo = null;
		if(equipo == null){
			if(this.result == null){
				this.result = (HashMap<Integer, Equipos2022>) readAll();
			}
			equipo = result.get(id);
			if(equipo != null && equipo.getId() > 0){
		    	if(equipo.getCategoriaId() != null){
		    		equipo.setCategoria(leerCategorias.read(equipo.getCategoriaId()));
		    	}
		    	if(equipo.getBoleraId() != null){
		    		equipo.setBolera(leerBoleras.read(equipo.getBoleraId()));
		    	}
		    	if(readAditional){
		    		equipo.setJugadoresList(leerJugadores.readEquipo(id));
		    		equipo.setJugadoresMap(new HashMap<>());
		    		for (Jugadores2022 jugador : equipo.getJugadoresList()) {
						equipo.getJugadoresMap().put(jugador.getId(), jugador);
					}
		    	}
		    	
		    }
		}
		return equipo;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Equipos2022 read(Integer id) {
		
		boolean readAditional = false;
		
		Equipos2022 equipo = null;
		if(equipo == null){
			if(this.result == null){
				this.result = (HashMap<Integer, Equipos2022>) readAll();
			}
			equipo = result.get(id);
			if(equipo.getId() > 0){
		    	if(equipo.getCategoriaId() != null){
		    		equipo.setCategoria(leerCategorias.read(equipo.getCategoriaId()));
		    	}
		    	if(equipo.getBoleraId() != null){
		    		equipo.setBolera(leerBoleras.read(equipo.getBoleraId()));
		    	}
		    	if(readAditional){
		    		equipo.setJugadoresList(leerJugadores.readEquipo(id));
		    	}
		    	
		    }
		}
		return equipo;
	}

	@Override
	public List<?> listResult() {
		List<Equipos2022> listResult = new ArrayList<Equipos2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    Equipos2022 equipo = (Equipos2022) value;
		    if(equipo.getId() > 0){
		    	if(equipo.getCategoriaId() != null){
		    		equipo.setCategoria(leerCategorias.read(equipo.getCategoriaId()));
		    	}
		    	if(equipo.getBoleraId() != null){
		    		equipo.setBolera(leerBoleras.read(equipo.getBoleraId()));
		    	}
		    	listResult.add((Equipos2022) equipo);
		    }
		}
		
		//Ordenar la clasificacion
		Collections.sort(listResult, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				Equipos2022 pi1 = (Equipos2022) o1;
				Equipos2022 pi2 = (Equipos2022) o2;
				
				int requipo = pi1.getCategoriaId().compareTo(pi2.getCategoriaId());
				if(requipo == 0){
					requipo = pi1.getNombre().compareTo(pi2.getNombre());
				}
				return requipo;
			}
		});
		
		return listResult;
	}
	
	public List<?> listResultVotaciones() {
		List<Equipos2022> listResult = new ArrayList<Equipos2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    Equipos2022 equipo = (Equipos2022) value;
		    System.out.println("EQUIPO: " + equipo.getNombre());
		    if(equipo.getId() > 0){
		    	if(equipo.getCategoriaId() != null){
		    		equipo.setCategoria(leerCategorias.read(equipo.getCategoriaId()));
		    	}
		    	if(equipo.getBoleraId() != null){
		    		equipo.setBolera(leerBoleras.read(equipo.getBoleraId()));
		    	}
		    	if(equipo.getCategoriaId() == Categorias2022.PRIMERA || equipo.getCategoriaId() == Categorias2022.SEGUNDA || equipo.getCategoriaId() == Categorias2022.TERCERA){
		    		if(equipo.getBoleraId() != 0){
		    			listResult.add((Equipos2022) equipo);
		    		}
		    	}
		    }
		}
		
		//Ordenar la clasificacion
		Collections.sort(listResult, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				Equipos2022 pi1 = (Equipos2022) o1;
				Equipos2022 pi2 = (Equipos2022) o2;
				
				int requipo = pi1.getCategoriaId().compareTo(pi2.getCategoriaId());
				if(requipo == 0){
					requipo = pi1.getNombre().compareTo(pi2.getNombre());
				}
				return requipo;
			}
		});
		
		return listResult;
	}
	
	public List<?> listResultLiga() {
		List<Equipos2022> listResult = new ArrayList<Equipos2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    Equipos2022 equipo = (Equipos2022) value;
		    if(equipo.getId() > 0){
		    	if(equipo.getLiga().equalsIgnoreCase(Activo2022.SI)){
			    	if(equipo.getCategoriaId() != null){
			    		equipo.setCategoria(leerCategorias.read(equipo.getCategoriaId()));
			    	}
			    	if(equipo.getBoleraId() != null){
			    		equipo.setBolera(leerBoleras.read(equipo.getBoleraId()));
			    	}
			    	listResult.add((Equipos2022) equipo);
			    }
		    }
		}
		return listResult;
	}

}
