package com.bolocelta.bbdd.readTables;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.inject.Named;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.application.enumerations.ActivoEnumeration2022;
import com.bolocelta.bbdd.constants.NombresTablas2022;
import com.bolocelta.bbdd.constants.Ubicaciones2022;
import com.bolocelta.entities.Categorias2022;
import com.bolocelta.transformer.CategoriasTransformer2022;

@Named
public class LeerCategorias2022 extends ALeer2022 {

	HashMap<Integer, Categorias2022> result = null;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, Categorias2022> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_CATEGORIAS, Ubicaciones2022.UBICACION_BBDD_ADMINISTRACION);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
	    		Categorias2022 categoria = new Categorias2022();
	    		categoria = CategoriasTransformer2022.transformerObjectCsv(categoria, row);
		    	result.put(categoria.getId(), categoria);
		    }
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Categorias2022 read(Integer id) {
		if(this.result == null){
			this.result = (HashMap<Integer, Categorias2022>) readAll();
		}
		return result.get(id);
	}

	@Override
	public List<?> listResult() {
		List<Categorias2022> listResult = new ArrayList<Categorias2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    listResult.add((Categorias2022) value);
		}
		return listResult;
	}
	
	public List<?> listResultIndividual() {
		List<Categorias2022> listResult = new ArrayList<Categorias2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    Categorias2022 categoria = (Categorias2022) value;
		    if(categoria.getIndividual().equalsIgnoreCase(ActivoEnumeration2022.SI)){
		    	listResult.add((Categorias2022) value);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultParejas() {
		List<Categorias2022> listResult = new ArrayList<Categorias2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    Categorias2022 categoria = (Categorias2022) value;
		    if(categoria.getParejas().equalsIgnoreCase(ActivoEnumeration2022.SI)){
		    	listResult.add((Categorias2022) value);
		    }
		}
		return listResult;
	}
	
	public List<?> listResultLiga() {
		List<Categorias2022> listResult = new ArrayList<Categorias2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    Categorias2022 categoria = (Categorias2022) value;
//		    System.out.println(categoria.getId());
		    if(categoria.getLiga().equalsIgnoreCase(ActivoEnumeration2022.SI)){
		    	listResult.add((Categorias2022) value);
		    }
		}
		return listResult;
	}

}
