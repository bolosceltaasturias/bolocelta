package com.bolocelta.bbdd.readTables.usuarios;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.inject.Named;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas2022;
import com.bolocelta.bbdd.constants.Ubicaciones2022;
import com.bolocelta.bbdd.readTables.ALeer2022;
import com.bolocelta.entities.usuarios.Usuarios2022;
import com.bolocelta.transformer.usuarios.UsuariosTransformer2022;

@Named
public class LeerUsuarios2022 extends ALeer2022 {

	private LeerRoles2022 leerRoles = new LeerRoles2022();

	HashMap<Integer, Usuarios2022> result = null;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, Usuarios2022> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_USUARIOS, Ubicaciones2022.UBICACION_BBDD_USUARIOS);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	Usuarios2022 usuario = new Usuarios2022();
		    	usuario = UsuariosTransformer2022.transformerObjectCsv(usuario, row);
		    	result.put(usuario.getId(), usuario);
		    }
		}
	    return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Usuarios2022 read(Integer id) {
		if (this.result == null) {
			result = (HashMap<Integer, Usuarios2022>) readAll();
		}
		return this.result.get(id);
	}

	@SuppressWarnings("unchecked")
	public Usuarios2022 read(String user, String pass) {
		if (user != null && pass != null) {
			for (Object object : listResult()) {
				Usuarios2022 usuario = (Usuarios2022) object;
				if (usuario.getUser().equals(user) && usuario.getPass().equals(pass)) {
					return usuario;
				}
			}
		}
		return null;
	}

	@Override
	public List<?> listResult() {
		List<Usuarios2022> listResult = new ArrayList<Usuarios2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
			Object value = entry.getValue();
			Usuarios2022 usuario = (Usuarios2022) value;
			if (usuario.getId() > 0) {
				if (usuario.getRolId() != null) {
					usuario.setRol(leerRoles.read(usuario.getRolId()));
				}
				listResult.add(usuario);
			}
		}
		return listResult;
	}

}
