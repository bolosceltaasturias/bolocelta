package com.bolocelta.bbdd.readTables.usuarios;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.inject.Named;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas2022;
import com.bolocelta.bbdd.constants.Ubicaciones2022;
import com.bolocelta.bbdd.readTables.ALeer2022;
import com.bolocelta.entities.usuarios.Permisos2022;
import com.bolocelta.transformer.usuarios.PermisosTransformer2022;

@Named
public class LeerPermisos2022 extends ALeer2022 {

	private LeerRoles2022 leerRoles = new LeerRoles2022();

	HashMap<Integer, Permisos2022> result = null;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, Permisos2022> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_PERMISOS, Ubicaciones2022.UBICACION_BBDD_USUARIOS);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	Permisos2022 permiso = new Permisos2022();
		    	permiso = PermisosTransformer2022.transformerObjectCsv(permiso, row);
		    	result.put(permiso.getId(), permiso);
		    }
		}
	    return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Permisos2022 read(Integer id) {
		if (this.result == null) {
			result = (HashMap<Integer, Permisos2022>) readAll();
		}
		return this.result.get(id);
	}

	@Override
	public List<?> listResult() {
		List<Permisos2022> listResult = new ArrayList<Permisos2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
			Object value = entry.getValue();
			Permisos2022 permiso = (Permisos2022) value;
			if (permiso.getId() > 0) {
				if (permiso.getRolId() != null) {
					permiso.setRol(leerRoles.read(permiso.getRolId()));
				}
				listResult.add(permiso);
			}
		}
		return listResult;
	}
	
	public List<?> listResultByRol(Integer rolId) {
		List<Permisos2022> listResult = new ArrayList<Permisos2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
			Object value = entry.getValue();
			Permisos2022 permiso = (Permisos2022) value;
			if(permiso != null && permiso.getId() != null){
				if (permiso.getId() > 0) {
					if(permiso.getRolId()== rolId){
						if (permiso.getRolId() != null) {
							permiso.setRol(leerRoles.read(permiso.getRolId()));
						}
						listResult.add(permiso);
					}
				}
			}
		}
		return listResult;
	}

}
