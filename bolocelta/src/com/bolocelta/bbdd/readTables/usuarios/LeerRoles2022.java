package com.bolocelta.bbdd.readTables.usuarios;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.inject.Named;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.bolocelta.bbdd.constants.NombresTablas2022;
import com.bolocelta.bbdd.constants.Ubicaciones2022;
import com.bolocelta.bbdd.readTables.ALeer2022;
import com.bolocelta.entities.usuarios.Roles2022;
import com.bolocelta.transformer.usuarios.RolesTransformer2022;

@Named
public class LeerRoles2022 extends ALeer2022 {
	
	HashMap<Integer, Roles2022> result = null;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HashMap<Integer, ?> readAll() {
		HashMap<Integer, Roles2022> result = new HashMap();
		CSVParser csvParser = readWorkBookCsv(NombresTablas2022.N_CSV_BBDD_ROLES, Ubicaciones2022.UBICACION_BBDD_USUARIOS);
		if(csvParser != null){
		    for (CSVRecord row : csvParser) {
		    	Roles2022 rol = new Roles2022();
		    	rol = RolesTransformer2022.transformerObjectCsv(rol, row);
		    	result.put(rol.getId(), rol);
		    }
		}
	    return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Roles2022 read(Integer id) {
		if(this.result == null){
			result = (HashMap<Integer, Roles2022>) readAll();
		}
		return this.result.get(id);
	}
	
	@Override
	public List<?> listResult() {
		List<Roles2022> listResult = new ArrayList<Roles2022>();
		for (Entry<Integer, ?> entry : readAll().entrySet()) {
		    Object value = entry.getValue();
		    Roles2022 rol = (Roles2022) value;
		    if(rol.getId() > 0){
		    	listResult.add(rol);
		    }
		}
		return listResult;
	}

}
