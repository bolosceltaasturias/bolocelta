package com.bolocelta.transformer;

import org.apache.commons.csv.CSVRecord;

import com.bolocelta.entities.sorteos.parejas.ClasificacionFaseI2022;

public class ClasificacionParejasFaseITransformer2022 {

	
	public static ClasificacionFaseI2022 transformerObjectCsv(ClasificacionFaseI2022 clasificacionFaseI, CSVRecord row) {
		
        // Accessing Values by Column Index
        String col1 = row.get(0);
        String col2 = row.get(1);
        String col3 = row.get(2);
        String col4 = row.get(3);
        String col5 = row.get(4);
        String col6 = row.get(5);
        String col7 = row.get(6);
        String col8 = row.get(7);
        String col9 = row.get(8);
        String col10 = row.get(9);
        String col11 = row.get(10);
        
		if (col1 != null && !col1.isEmpty()) {
			try {
				Double id = Double.valueOf(col1);
				clasificacionFaseI.setId(id.intValue());
				//System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				clasificacionFaseI.setId(null);
			}
		}
		if (col2 != null && !col2.isEmpty()) {
			try {
				Double id = Double.valueOf(col2);
				clasificacionFaseI.setCategoriaId(id.intValue());
				//System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				clasificacionFaseI.setCategoriaId(null);
			}
		} 
		if (col3 != null && !col3.isEmpty()) {
			clasificacionFaseI.setGrupo(col3);
			//System.out.print(col3 + " | ");
		}
		if (col4 != null && !col4.isEmpty()) {
			try {
				Double id = Double.valueOf(col4);
				clasificacionFaseI.setParejaId(id.intValue());
				//System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				clasificacionFaseI.setId(null);
			}
		}
		if (col5 != null && !col5.isEmpty()) {
			try {
				Double id = Double.valueOf(col5);
				clasificacionFaseI.setPj(id.intValue());
				//System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				clasificacionFaseI.setPj(null);
			}
		} 
		if (col6 != null && !col6.isEmpty()) {
			try {
				Double id = Double.valueOf(col6);
				clasificacionFaseI.setPg(id.intValue());
				//System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				clasificacionFaseI.setPg(null);
			}
		} 
		if (col7 != null && !col7.isEmpty()) {
			try {
				Double id = Double.valueOf(col7);
				clasificacionFaseI.setPe(id.intValue());
				//System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				clasificacionFaseI.setPe(null);
			}
		} 
		if (col8 != null && !col8.isEmpty()) {
			try {
				Double id = Double.valueOf(col8);
				clasificacionFaseI.setPp(id.intValue());
				//System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				clasificacionFaseI.setPp(null);
			}
		}
		if (col9 != null && !col9.isEmpty()) {
			try {
				Double id = Double.valueOf(col9);
				clasificacionFaseI.setPf(id.intValue());
				//System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				clasificacionFaseI.setPf(null);
			}
		}
		if (col10 != null && !col10.isEmpty()) {
			try {
				Double id = Double.valueOf(col10);
				clasificacionFaseI.setPc(id.intValue());
				//System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				clasificacionFaseI.setPc(null);
			}
		}
		if (col11 != null && !col11.isEmpty()) {
			try {
				Double id = Double.valueOf(col11);
				clasificacionFaseI.setPt(id.intValue());
				//System.out.print(id.intValue() + " | ");
			}catch (Exception e) {
				clasificacionFaseI.setPt(null);
			}
		}
		return clasificacionFaseI;
	}

}
