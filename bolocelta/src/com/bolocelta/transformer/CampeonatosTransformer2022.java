package com.bolocelta.transformer;

import org.apache.poi.ss.usermodel.Cell;

import com.bolocelta.entities.Campeonatos2022;

public class CampeonatosTransformer2022 {

	public static Campeonatos2022 transformerObject(Cell cell) {

		Campeonatos2022 campeonatos = new Campeonatos2022();
		if (cell.getColumnIndex() == 0) {
			Double id = cell.getNumericCellValue();
			campeonatos.setId(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} else if (cell.getColumnIndex() == 1) {
			Double idModalidad = cell.getNumericCellValue();
			campeonatos.setModalidad(idModalidad.intValue());
			//System.out.print(idModalidad.intValue() + " | ");
		} else if (cell.getColumnIndex() == 2) {
			Double idCategoria = cell.getNumericCellValue();
			campeonatos.setCategoria(idCategoria.intValue());
			//System.out.print(idCategoria.intValue() + " | ");
		} else if (cell.getColumnIndex() == 3) {
			campeonatos.setDetalle(cell.getStringCellValue());
			//System.out.print(cell.getStringCellValue() + " | ");
		} else if (cell.getColumnIndex() == 3) {
			campeonatos.setFecha(cell.getStringCellValue());
			//System.out.print(cell.getStringCellValue() + " | ");
		}

		return campeonatos;
	}

}
