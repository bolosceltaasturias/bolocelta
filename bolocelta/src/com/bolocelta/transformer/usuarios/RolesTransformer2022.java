package com.bolocelta.transformer.usuarios;

import org.apache.commons.csv.CSVRecord;
import org.apache.poi.ss.usermodel.Cell;

import com.bolocelta.entities.usuarios.Roles2022;

public class RolesTransformer2022 {

	public static Roles2022 transformerObject(Roles2022 rol, Cell cell) {

		if (cell.getColumnIndex() == 0) {
			Double id = cell.getNumericCellValue();
			rol.setId(id.intValue());
//			System.out.print(id.intValue() + " | ");
		} else if (cell.getColumnIndex() == 1) {
			rol.setRol(cell.getStringCellValue());
//			System.out.print(cell.getStringCellValue() + " | ");
		} else if (cell.getColumnIndex() == 2) {
			rol.setAdmin(cell.getStringCellValue());
//			System.out.print(cell.getStringCellValue() + " | ");
		}

		return rol;
	}
	
	public static Roles2022 transformerObjectCsv(Roles2022 rol, CSVRecord row) {
		
        // Accessing Values by Column Index
        String col1 = row.get(0);
        String col2 = row.get(1);
        String col3 = row.get(2);

		if (col1 != null && !col1.isEmpty()) {
			Double id = Double.valueOf(col1);
			rol.setId(id.intValue());
//			System.out.print(id.intValue() + " | ");
		} 
		if (col2 != null && !col2.isEmpty()) {
			rol.setRol(col2);
//			System.out.print(col2 + " | ");
		} 
		if (col3 != null && !col3.isEmpty()) {
			rol.setAdmin(col3);
//			System.out.print(col3 + " | ");
		} 

		return rol;
	}

}
