package com.bolocelta.transformer;

import org.apache.commons.csv.CSVRecord;

import com.bolocelta.application.enumerations.ActivoEnumeration2022;
import com.bolocelta.entities.CampeonatoLigaInfantilCalendario2023;

public class CampeonatoLigaInfantilTransformer2023 {

	public static CampeonatoLigaInfantilCalendario2023 transformerObjectCalendarioCsv(CampeonatoLigaInfantilCalendario2023 cec, CSVRecord row) {
		
        // Accessing Values by Column Index
        String col1 = row.get(0);
        String col2 = row.get(1);
        String col3 = row.get(2);
        String col4 = row.get(3);
        String col5 = row.get(4);
        String col6 = row.get(5);
        String col7 = row.get(6);
        String col8 = row.get(7);
        String col9 = row.get(8);
        String col10 = row.get(9);
        String col11 = row.get(10);
        String col12 = row.get(11);
        String col13 = row.get(12);
        String col14 = row.get(13);

		if (col1 != null && !col1.isEmpty()) {
			Double id = Double.valueOf(col1);
			cec.setId(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col2 != null && !col2.isEmpty()) {
			Double id = Double.valueOf(col2);
			cec.setJornada(id.intValue());
			//System.out.print(id.intValue() + " | ");
		}
		if (col3 != null && !col3.isEmpty()) {
			cec.setFecha(col3);
			//System.out.print(col3 + " | ");
		} 
		if (col4 != null && !col4.isEmpty()) {
			cec.setHora(col4);
			//System.out.print(col4 + " | ");
		} 
		if (col5 != null && !col5.isEmpty()) {
			Double id = Double.valueOf(col5);
			cec.setRonda1(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col6 != null && !col6.isEmpty()) {
			Double id = Double.valueOf(col6);
			cec.setRonda2(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col7 != null && !col7.isEmpty()) {
			Double id = Double.valueOf(col7);
			cec.setRonda3(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col8 != null && !col8.isEmpty()) {
			Double id = Double.valueOf(col8);
			cec.setRonda4(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col9 != null && !col9.isEmpty()) {
			Double id = Double.valueOf(col9);
			cec.setPuntos(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col10 != null && !col10.isEmpty()) {
			Double id = Double.valueOf(col10);
			cec.setActivo(id.intValue());
			//System.out.print(id.intValue() + " | ");
		}
		if (col11 != null && !col11.isEmpty()) {
			Double id = Double.valueOf(col11);
			cec.setBoleraId(id.intValue());
			//System.out.print(id.intValue() + " | ");
		}
		if (col12 != null && !col12.isEmpty()) {
			cec.setNombre(col12);
			//System.out.print(id.intValue() + " | ");
		}
		if (col13 != null && !col13.isEmpty()) {
			cec.setCategoria(col13);
			//System.out.print(id.intValue() + " | ");
		}
		if (col14 != null && !col14.isEmpty()) {
			if(col14.equalsIgnoreCase(ActivoEnumeration2022.SI)){
				cec.setContarFinal(true);
			}else{
				cec.setContarFinal(false);
			}
			//System.out.print(id.intValue() + " | ");
		}
		return cec;
	}

}
