package com.bolocelta.transformer;

import org.apache.commons.csv.CSVRecord;

import com.bolocelta.entities.CampeonatoCadetesIndividualClasificacion2022;

public class CampeonatoCadetesIndividualTransformer2022 {

	public static CampeonatoCadetesIndividualClasificacion2022 transformerObjectClasificacionCsv(CampeonatoCadetesIndividualClasificacion2022 cec, CSVRecord row) {
		
        // Accessing Values by Column Index
        String col1 = row.get(0);
        String col2 = row.get(1);
        String col3 = row.get(2);
        String col4 = row.get(3);
        String col5 = row.get(4);
        String col6 = row.get(5);
        String col7 = row.get(6);
        String col8 = row.get(7);
        String col9 = row.get(8);
        String col10 = row.get(9);
        String col11 = row.get(10);
        String col12 = row.get(11);
        String col13 = row.get(12);
        String col14 = row.get(13);
        String col15 = row.get(14);
        String col16 = row.get(15);
        String col17 = row.get(16);
        String col18 = row.get(17);
        String col19 = row.get(18);
        String col20 = row.get(19);
        String col21 = row.get(20);
        String col22 = row.get(21);
        String col23 = row.get(22);
        String col24 = row.get(23);
        String col25 = row.get(24);
        String col26 = row.get(25);
        String col27 = row.get(26);
        String col28 = row.get(27);
        String col29 = row.get(28);
        String col30 = row.get(29);
        String col31 = row.get(30);
        String col32 = row.get(31);
        String col33 = row.get(32);
        String col34 = row.get(33);
        String col35 = row.get(34);
        String col36 = row.get(35);
        String col37 = row.get(36);
        String col38 = row.get(37);
        String col39 = row.get(38);
        String col40 = row.get(39);
        String col41 = row.get(40);
        String col42 = row.get(41);
        String col43 = row.get(42);
        String col44 = row.get(43);
        String col45 = row.get(44);
        String col46 = row.get(45);
        

		if (col1 != null && !col1.isEmpty()) {
			Double id = Double.valueOf(col1);
			cec.setId(id.intValue());
		} 
		if (col2 != null && !col2.isEmpty()) {
			cec.setFecha(col2);
		}
		if (col3 != null && !col3.isEmpty()) {
			cec.setHora(col3);
		}
		if (col4 != null && !col4.isEmpty()) {
			Double id = Double.valueOf(col4);
			cec.setBoleraId(id.intValue());
		}
		if (col5 != null && !col5.isEmpty()) {
			Double id = Double.valueOf(col5);
			cec.setJugadorId(id.intValue());
		}
		if (col6 != null && !col6.isEmpty()) {
			Double id = Double.valueOf(col6);
			cec.setPuntosTirada1(id.intValue());
		}
		if (col7 != null && !col7.isEmpty()) {
			Double id = Double.valueOf(col7);
			cec.setPuntosSacada1(id.intValue());
		}
		if (col8 != null && !col8.isEmpty()) {
			Double id = Double.valueOf(col8);
			cec.setPuntosTirada2(id.intValue());
		}
		if (col9 != null && !col9.isEmpty()) {
			Double id = Double.valueOf(col9);
			cec.setPuntosSacada2(id.intValue());
		}
		if (col10 != null && !col10.isEmpty()) {
			Double id = Double.valueOf(col10);
			cec.setPuntosTirada3(id.intValue());
		}
		if (col11 != null && !col11.isEmpty()) {
			Double id = Double.valueOf(col11);
			cec.setPuntosSacada3(id.intValue());
		}
		if (col12 != null && !col12.isEmpty()) {
			Double id = Double.valueOf(col12);
			cec.setPuntosTirada4(id.intValue());
		}
		if (col13 != null && !col13.isEmpty()) {
			Double id = Double.valueOf(col13);
			cec.setPuntosSacada4(id.intValue());
		}
		if (col14 != null && !col14.isEmpty()) {
			Double id = Double.valueOf(col14);
			cec.setPuntosTirada5(id.intValue());
		}
		if (col15 != null && !col15.isEmpty()) {
			Double id = Double.valueOf(col15);
			cec.setPuntosSacada5(id.intValue());
		}
		if (col16 != null && !col16.isEmpty()) {
			Double id = Double.valueOf(col16);
			cec.setPuntosTirada6(id.intValue());
		}
		if (col17 != null && !col17.isEmpty()) {
			Double id = Double.valueOf(col17);
			cec.setPuntosSacada6(id.intValue());
		}
		if (col18 != null && !col18.isEmpty()) {
			Double id = Double.valueOf(col18);
			cec.setPuntosTirada7(id.intValue());
		}
		if (col19 != null && !col19.isEmpty()) {
			Double id = Double.valueOf(col19);
			cec.setPuntosSacada7(id.intValue());
		}
		if (col20 != null && !col20.isEmpty()) {
			Double id = Double.valueOf(col20);
			cec.setPuntosTirada8(id.intValue());
		}
		if (col21 != null && !col21.isEmpty()) {
			Double id = Double.valueOf(col21);
			cec.setPuntosSacada8(id.intValue());
		}
		if (col22 != null && !col22.isEmpty()) {
			Double id = Double.valueOf(col22);
			cec.setPuntosTirada9(id.intValue());
		}
		if (col23 != null && !col23.isEmpty()) {
			Double id = Double.valueOf(col23);
			cec.setPuntosSacada9(id.intValue());
		}
		if (col24 != null && !col24.isEmpty()) {
			Double id = Double.valueOf(col24);
			cec.setPuntosTirada10(id.intValue());
		}
		if (col25 != null && !col25.isEmpty()) {
			Double id = Double.valueOf(col25);
			cec.setPuntosSacada10(id.intValue());
		}
		if (col26 != null && !col26.isEmpty()) {
			Double id = Double.valueOf(col26);
			cec.setPuntosTirada11(id.intValue());
		}
		if (col27 != null && !col27.isEmpty()) {
			Double id = Double.valueOf(col27);
			cec.setPuntosSacada11(id.intValue());
		}
		if (col28 != null && !col28.isEmpty()) {
			Double id = Double.valueOf(col28);
			cec.setPuntosTirada12(id.intValue());
		}
		if (col29 != null && !col29.isEmpty()) {
			Double id = Double.valueOf(col29);
			cec.setPuntosSacada12(id.intValue());
		}
		if (col30 != null && !col30.isEmpty()) {
			Double id = Double.valueOf(col30);
			cec.setPuntosTirada13(id.intValue());
		}
		if (col31 != null && !col31.isEmpty()) {
			Double id = Double.valueOf(col31);
			cec.setPuntosSacada13(id.intValue());
		}
		if (col32 != null && !col32.isEmpty()) {
			Double id = Double.valueOf(col32);
			cec.setPuntosTirada14(id.intValue());
		}
		if (col33 != null && !col33.isEmpty()) {
			Double id = Double.valueOf(col33);
			cec.setPuntosSacada14(id.intValue());
		}
		if (col34 != null && !col34.isEmpty()) {
			Double id = Double.valueOf(col34);
			cec.setPuntosTirada15(id.intValue());
		}
		if (col35 != null && !col35.isEmpty()) {
			Double id = Double.valueOf(col35);
			cec.setPuntosSacada15(id.intValue());
		}
		if (col36 != null && !col36.isEmpty()) {
			Double id = Double.valueOf(col36);
			cec.setPuntosTirada16(id.intValue());
		}
		if (col37 != null && !col37.isEmpty()) {
			Double id = Double.valueOf(col37);
			cec.setPuntosSacada16(id.intValue());
		}
		if (col38 != null && !col38.isEmpty()) {
			Double id = Double.valueOf(col38);
			cec.setPuntosTirada17(id.intValue());
		}
		if (col39 != null && !col39.isEmpty()) {
			Double id = Double.valueOf(col39);
			cec.setPuntosSacada17(id.intValue());
		}
		if (col40 != null && !col40.isEmpty()) {
			Double id = Double.valueOf(col40);
			cec.setPuntosTirada18(id.intValue());
		}
		if (col41 != null && !col41.isEmpty()) {
			Double id = Double.valueOf(col41);
			cec.setPuntosSacada18(id.intValue());
		}
		if (col42 != null && !col42.isEmpty()) {
			Double id = Double.valueOf(col42);
			cec.setPuntosTirada19(id.intValue());
		}
		if (col43 != null && !col43.isEmpty()) {
			Double id = Double.valueOf(col43);
			cec.setPuntosSacada19(id.intValue());
		}
		if (col44 != null && !col44.isEmpty()) {
			Double id = Double.valueOf(col44);
			cec.setPuntosTirada20(id.intValue());
		}
		if (col45 != null && !col45.isEmpty()) {
			Double id = Double.valueOf(col45);
			cec.setPuntosSacada20(id.intValue());
		}
		if (col46 != null && !col46.isEmpty()) {
			Double id = Double.valueOf(col46);
			cec.setActivo(id.intValue());
		}
		
		return cec;
	}
	


}
