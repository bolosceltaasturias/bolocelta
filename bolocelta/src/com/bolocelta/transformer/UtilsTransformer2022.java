package com.bolocelta.transformer;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;

import com.bolocelta.bbdd.constants.NombresTablas2022;

public class UtilsTransformer2022 {
	
	public static String getNativeValueType(Cell cell){
		final int type = cell.getCellType();
		if (type == Cell.CELL_TYPE_BLANK) {
			return null;
		} else if (type == Cell.CELL_TYPE_STRING) {
			return NombresTablas2022.TD_TEXTO;
		} else if (type == Cell.CELL_TYPE_NUMERIC) {
			if (DateUtil.isCellDateFormatted(cell)) {
				return NombresTablas2022.TD_FECHA;
			} else {
				return NombresTablas2022.TD_DECIMAL;
			}
		} else if (type == Cell.CELL_TYPE_FORMULA) {
			if (cell.getCachedFormulaResultType() == Cell.CELL_TYPE_NUMERIC) {
				return NombresTablas2022.TD_DECIMAL;
			} else {
				return NombresTablas2022.TD_TEXTO;
			}
		} else {
			return NombresTablas2022.TD_TEXTO;
		}
	}

}
