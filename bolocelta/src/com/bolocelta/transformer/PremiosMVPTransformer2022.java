package com.bolocelta.transformer;

import org.apache.commons.csv.CSVRecord;

import com.bolocelta.entities.PremiosMVP2022;

public class PremiosMVPTransformer2022 {

	public static PremiosMVP2022 transformerObjectCalendarioCsv(PremiosMVP2022 cec, CSVRecord row) {
		
        // Accessing Values by Column Index
        String col1 = row.get(0);
        String col2 = row.get(1);
        String col3 = row.get(2);
        String col4 = row.get(3);
        String col5 = row.get(4);
        String col6 = row.get(5);
        String col7 = row.get(6);
        String col8 = row.get(7);
        String col9 = row.get(8);
        String col10 = row.get(9);
        String col11 = row.get(10);
        String col12 = row.get(11);
        String col13 = row.get(12);
        String col14 = row.get(13);
        String col15 = row.get(14);
        String col16 = row.get(15);
        String col17 = row.get(16);
        String col18 = row.get(17);
        String col19 = row.get(18);
        String col20 = row.get(19);

		if (col1 != null && !col1.isEmpty()) {
			Double id = Double.valueOf(col1);
			cec.setId(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col2 != null && !col2.isEmpty()) {
			Double id = Double.valueOf(col2);
			cec.setCategoria(id.intValue());
			//System.out.print(id.intValue() + " | ");
		}
		if (col3 != null && !col3.isEmpty()) {
			Double id = Double.valueOf(col3);
			cec.setIdJugador(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col4 != null && !col4.isEmpty()) {
			cec.setEsVeterano(col4);
			//System.out.print(col4 + " | ");
		} 
		if (col5 != null && !col5.isEmpty()) {
			cec.setEsJuvenil(col5);
			//System.out.print(id.intValue() + " | ");
		} 
		if (col6 != null && !col6.isEmpty()) {
			Double id = Double.valueOf(col6);
			cec.setPia(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col7 != null && !col7.isEmpty()) {
			Double id = Double.valueOf(col7);
			cec.setPpa(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col8 != null && !col8.isEmpty()) {
			Double id = Double.valueOf(col8);
			cec.setPea(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col9 != null && !col9.isEmpty()) {
			Double id = Double.valueOf(col9);
			cec.setPma(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col10 != null && !col10.isEmpty()) {
			Double id = Double.valueOf(col10);
			cec.setPaa(id.intValue());
			//System.out.print(id.intValue() + " | ");
		}
		if (col11 != null && !col11.isEmpty()) {
			Double id = Double.valueOf(col11);
			cec.setPva(id.intValue());
			//System.out.print(id.intValue() + " | ");
		}
		if (col12 != null && !col12.isEmpty()) {
			Double id = Double.valueOf(col12);
			cec.setPja(id.intValue());
			//System.out.print(id.intValue() + " | ");
		}
		if (col13 != null && !col13.isEmpty()) {
			Double id = Double.valueOf(col13);
			cec.setPlf(id.intValue());
			//System.out.print(id.intValue() + " | ");
		}
		if (col14 != null && !col14.isEmpty()) {
			Double id = Double.valueOf(col14);
			cec.setPie(id.intValue());
			//System.out.print(id.intValue() + " | ");
		}
		if (col15 != null && !col15.isEmpty()) {
			Double id = Double.valueOf(col15);
			cec.setPpe(id.intValue());
			//System.out.print(id.intValue() + " | ");
		}
		if (col16 != null && !col16.isEmpty()) {
			Double id = Double.valueOf(col16);
			cec.setPee(id.intValue());
			//System.out.print(id.intValue() + " | ");
		}
		if (col17 != null && !col17.isEmpty()) {
			Double id = Double.valueOf(col17);
			cec.setPme(id.intValue());
			//System.out.print(id.intValue() + " | ");
		}
		if (col18 != null && !col18.isEmpty()) {
			Double id = Double.valueOf(col18);
			cec.setPve(id.intValue());
			//System.out.print(id.intValue() + " | ");
		}
		if (col19 != null && !col19.isEmpty()) {
			Double id = Double.valueOf(col19);
			cec.setPje(id.intValue());
			//System.out.print(id.intValue() + " | ");
		}
		if (col20 != null && !col20.isEmpty()) {
			cec.setEsCadete(col20);
			//System.out.print(id.intValue() + " | ");
		}
		return cec;
	}

}
