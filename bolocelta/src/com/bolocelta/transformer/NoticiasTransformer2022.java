package com.bolocelta.transformer;

import org.apache.commons.csv.CSVRecord;

import com.bolocelta.entities.Noticias2022;

public class NoticiasTransformer2022 {

	public static Noticias2022 transformerObjectCsv(Noticias2022 noticias, CSVRecord row) {

		// Accessing Values by Column Index
		String col1 = row.get(0);
		String col2 = row.get(1);
		String col3 = row.get(2);
		String col4 = row.get(3);

		if (col1 != null && !col1.isEmpty()) {
			Double id = Double.valueOf(col1);
			noticias.setId(id.intValue());
		}
		if (col2 != null && !col2.isEmpty()) {
			noticias.setFecha(col2);
		}
		if (col3 != null && !col3.isEmpty()) {
			noticias.setTituloCampeonato(col3);
		}
		if (col4 != null && !col4.isEmpty()) {
			noticias.setDescripcion(col4);
		}
		return noticias;
	}

}
