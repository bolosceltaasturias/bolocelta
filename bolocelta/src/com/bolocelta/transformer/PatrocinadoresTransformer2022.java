package com.bolocelta.transformer;

import org.apache.commons.csv.CSVRecord;

import com.bolocelta.entities.Patrocinadores2022;

public class PatrocinadoresTransformer2022 {

	public static Patrocinadores2022 transformerObjectCsv(Patrocinadores2022 patrocinador, CSVRecord row) {
		
        // Accessing Values by Column Index
        String col1 = row.get(0);
        String col2 = row.get(1);
        String col3 = row.get(2);
        String col4 = row.get(3);
        String col5 = row.get(4);
        String col6 = row.get(5);
        

		if (col1 != null && !col1.isEmpty()) {
			Double id = Double.valueOf(col1);
			patrocinador.setId(id.intValue());
//			System.out.print(id.intValue() + " | ");
		} 
		if (col2 != null && !col2.isEmpty()) {
			patrocinador.setLogo("/template_1.0/images/anuncios/" + col2);
//			System.out.print(col2 + " | ");
		} 
		if (col3 != null && !col3.isEmpty()) {
			patrocinador.setPatrocinador(col3);
//			System.out.print(col3 + " | ");
		} 
		if (col4 != null && !col4.isEmpty()) {
			patrocinador.setTelefono(col4);
//			System.out.print(col4 + " | ");
		}
		if (col5 != null && !col5.isEmpty()) {
			patrocinador.setWeb(col5);
//			System.out.print(col5 + " | ");
		}
		if (col6 != null && !col6.isEmpty()) {
			patrocinador.setDescripcion(col6);
//			System.out.print(col6 + " | ");
		}

		return patrocinador;
	}

}
