package com.bolocelta.transformer;

import org.apache.commons.csv.CSVRecord;

import com.bolocelta.entities.sorteos.liga.SorteoBolerasCrucesLiga2022;

public class SorteoBolerasCrucesLigaTransformer2022 {

	public static SorteoBolerasCrucesLiga2022 transformerObjectCsv(SorteoBolerasCrucesLiga2022 sorteoBolerasCrucesLiga, CSVRecord row) {
		
        // Accessing Values by Column Index
        String col1 = row.get(0);
        String col2 = row.get(1);
        String col3 = row.get(2);
        String col4 = row.get(3);
        String col5 = row.get(4);
        String col6 = row.get(5);
        String col7 = row.get(6);
        String col8 = row.get(7);
        String col9 = row.get(8);
        String col10 = row.get(9);
        String col11 = row.get(10);
        String col12 = row.get(11);

		if (col1 != null && !col1.isEmpty()) {
			Double id = Double.valueOf(col1);
			sorteoBolerasCrucesLiga.setId(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col2 != null && !col2.isEmpty()) {
			Double id = Double.valueOf(col2);
			sorteoBolerasCrucesLiga.setCategoria(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col3 != null && !col3.isEmpty()) {
			Double id = Double.valueOf(col3);
			sorteoBolerasCrucesLiga.setNumeroCruce(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col4 != null && !col4.isEmpty()) {
			Double id = Double.valueOf(col4);
			sorteoBolerasCrucesLiga.setIdBoleraPreferente(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col5 != null && !col5.isEmpty()) {
			Double id = Double.valueOf(col5);
			sorteoBolerasCrucesLiga.setIdBolera1(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col6 != null && !col6.isEmpty()) {
			Double id = Double.valueOf(col6);
			sorteoBolerasCrucesLiga.setIdBolera2(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col7 != null && !col7.isEmpty()) {
			Double id = Double.valueOf(col7);
			sorteoBolerasCrucesLiga.setIdBolera3(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col8 != null && !col8.isEmpty()) {
			Double id = Double.valueOf(col8);
			sorteoBolerasCrucesLiga.setIdBolera4(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col9 != null && !col9.isEmpty()) {
			Double id = Double.valueOf(col9);
			sorteoBolerasCrucesLiga.setIdBolera5(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col10 != null && !col10.isEmpty()) {
			Double id = Double.valueOf(col10);
			sorteoBolerasCrucesLiga.setIdBolera6(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col11 != null && !col11.isEmpty()) {
			Double id = Double.valueOf(col11);
			sorteoBolerasCrucesLiga.setIdBolera7(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 
		if (col12 != null && !col12.isEmpty()) {
			Double id = Double.valueOf(col12);
			sorteoBolerasCrucesLiga.setIdBolera8(id.intValue());
			//System.out.print(id.intValue() + " | ");
		} 

		return sorteoBolerasCrucesLiga;
	}

}
