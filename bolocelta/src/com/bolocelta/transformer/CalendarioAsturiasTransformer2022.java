package com.bolocelta.transformer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.csv.CSVRecord;
import org.apache.poi.ss.usermodel.Cell;

import com.bolocelta.entities.CalendarioAsturias2022;

public class CalendarioAsturiasTransformer2022 {

	public static CalendarioAsturias2022 transformerObject(CalendarioAsturias2022 calendarioAsturias, Cell cell) {

		if (cell.getColumnIndex() == 0) {
			Double id = cell.getNumericCellValue();
			calendarioAsturias.setId(id.intValue());
//			System.out.print(id.intValue() + " | ");
		} else if (cell.getColumnIndex() == 1) {
			calendarioAsturias.setFechaDesde(cell.getDateCellValue());
//			System.out.print(cell.getDateCellValue() + " | ");
		} else if (cell.getColumnIndex() == 2) {
			calendarioAsturias.setFechaHasta(cell.getDateCellValue());
//			System.out.print(cell.getDateCellValue() + " | ");
		} else if (cell.getColumnIndex() == 3) {
			Double id = cell.getNumericCellValue();
			calendarioAsturias.setModalidadId(id.intValue());
//			System.out.print(id.intValue() + " | ");
		} else if (cell.getColumnIndex() == 4) {
			Double id = cell.getNumericCellValue();
			calendarioAsturias.setCategoriaId(id.intValue());
//			System.out.print(id.intValue() + " | ");
		} else if (cell.getColumnIndex() == 5) {
			Double id = cell.getNumericCellValue();
			if(id != null){
				calendarioAsturias.setJornada(id.intValue());
			}
//			System.out.print(id.intValue() + " | ");
		} else if (cell.getColumnIndex() == 6) {
			calendarioAsturias.setNombreCampeonato(cell.getStringCellValue());
//			System.out.print(cell.getStringCellValue() + " | ");
		}

		return calendarioAsturias;
	}
	
	public static CalendarioAsturias2022 transformerObjectCsv(CalendarioAsturias2022 calendarioAsturias, CSVRecord row) {
		
        // Accessing Values by Column Index
        String col1 = row.get(0);
        String col2 = row.get(1);
        String col3 = row.get(2);
        String col4 = row.get(3);
        String col5 = row.get(4);
        String col6 = row.get(5);
        String col7 = row.get(6);
        String col8 = row.get(7);

		if (col1 != null && !col1.isEmpty()) {
			Double id = Double.valueOf(col1);
			calendarioAsturias.setId(id.intValue());
//			System.out.print(id.intValue() + " | ");
		} 
		if (col2 != null && !col2.isEmpty()) {
			Date date;
			try {
				date = new SimpleDateFormat("dd/MM/yyyy").parse(col2);
			} catch (ParseException e) {
				date = null;
			}
			calendarioAsturias.setFechaDesde(date);
//			System.out.print(col2 + " | ");
		} 
		if (col3 != null && !col3.isEmpty()) {
			Date date;
			try {
				date = new SimpleDateFormat("dd/MM/yyyy").parse(col3);
			} catch (ParseException e) {
				date = null;
			}
			calendarioAsturias.setFechaHasta(date);
//			System.out.print(col3 + " | ");
		} 
		if (col4 != null && !col4.isEmpty()) {
			Double id = Double.valueOf(col4);
			calendarioAsturias.setModalidadId(id.intValue());
//			System.out.print(id.intValue() + " | ");
		}
		if (col5 != null && !col5.isEmpty()) {
			Double id = Double.valueOf(col5);
			calendarioAsturias.setCategoriaId(id.intValue());
//			System.out.print(id.intValue() + " | ");
		}
		if (col6 != null && !col6.isEmpty()) {
			Double id = Double.valueOf(col6);
			if(id != null){
				calendarioAsturias.setJornada(id.intValue());
			}
//			System.out.print(id.intValue() + " | ");
		}
		if (col7 != null && !col7.isEmpty()) {
			Double id = Double.valueOf(col7);
			if(id != null){
				calendarioAsturias.setJornadaVuelta(id.intValue());
			}
//			System.out.print(id.intValue() + " | ");
		}
		if (col8 != null && !col8.isEmpty()) {
			calendarioAsturias.setNombreCampeonato(col8);
//			System.out.print(col8 + " | ");
		}
		return calendarioAsturias;
	}

}
