package com.bolocelta.validator;

import java.util.regex.Pattern;

import javax.faces.validator.ValidatorException;

public class TelefonoValidator2022 {
	 
    private Pattern pattern;
  
    private static final String NUMBER_PATTERN = "\\d+";
  
    public TelefonoValidator2022() {
        pattern = Pattern.compile(NUMBER_PATTERN);
    }
 
    public boolean validate(Object value) throws ValidatorException {
        if(!pattern.matcher(value.toString()).matches()) {
            return false;
        }
        
        return true;
    }
     
}
