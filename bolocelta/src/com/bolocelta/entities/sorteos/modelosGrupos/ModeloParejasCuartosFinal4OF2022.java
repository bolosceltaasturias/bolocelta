package com.bolocelta.entities.sorteos.modelosGrupos;

import java.util.LinkedHashMap;

import com.bolocelta.bbdd.constants.CrucesCampeonatos2022;
import com.bolocelta.bbdd.constants.TipoSorteo2022;

public class ModeloParejasCuartosFinal4OF2022 {

	private String nombre = "Cuartos de Final";
	private LinkedHashMap<Integer, ModeloParejasEnfrentamientoCuartosFinal2022> enfrentamientosCuartosFinal = new LinkedHashMap<>();

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedHashMap<Integer, ModeloParejasEnfrentamientoCuartosFinal2022> getEnfrentamientosCuartosFinal() {
		return enfrentamientosCuartosFinal;
	}

	public void setEnfrentamientosCuartosFinal(LinkedHashMap<Integer, ModeloParejasEnfrentamientoCuartosFinal2022> enfrentamientosCuartosFinal) {
		this.enfrentamientosCuartosFinal = enfrentamientosCuartosFinal;
	}
	
	public ModeloParejasCuartosFinal4OF2022() {
		this.enfrentamientosCuartosFinal.put(1, new ModeloParejasEnfrentamientoCuartosFinal2022(CrucesCampeonatos2022.CFA, 1, CrucesCampeonatos2022.OFA, CrucesCampeonatos2022.OFB, 0, 0, 0, 0, TipoSorteo2022.MONEDA, CrucesCampeonatos2022.SF1));
		this.enfrentamientosCuartosFinal.put(2, new ModeloParejasEnfrentamientoCuartosFinal2022(CrucesCampeonatos2022.CFB, 1, CrucesCampeonatos2022.OFC, CrucesCampeonatos2022.OFD, 0, 0, 0, 0, TipoSorteo2022.MONEDA, CrucesCampeonatos2022.SF1));
		this.enfrentamientosCuartosFinal.put(3, new ModeloParejasEnfrentamientoCuartosFinal2022(CrucesCampeonatos2022.CFC, 1, CrucesCampeonatos2022.OFE, CrucesCampeonatos2022.OFF, 0, 0, 0, 0, TipoSorteo2022.MONEDA, CrucesCampeonatos2022.SF2));
		this.enfrentamientosCuartosFinal.put(4, new ModeloParejasEnfrentamientoCuartosFinal2022(CrucesCampeonatos2022.CFD, 1, CrucesCampeonatos2022.OFG, CrucesCampeonatos2022.OFH, 0, 0, 0, 0, TipoSorteo2022.MONEDA, CrucesCampeonatos2022.SF2));
	}

}
