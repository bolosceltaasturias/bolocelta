package com.bolocelta.entities.sorteos.modelosGrupos;

import java.util.LinkedHashMap;

import com.bolocelta.bbdd.constants.TipoSorteo2022;

public class ModeloIndividualGrupos32022 {

	private String nombre = "Grupo 3";
	private LinkedHashMap<Integer, ModeloIndividualEnfrentamientoGruposFaseI2022> enfrentamientosGrupo = new LinkedHashMap<>();

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedHashMap<Integer, ModeloIndividualEnfrentamientoGruposFaseI2022> getEnfrentamientosGrupo() {
		return enfrentamientosGrupo;
	}

	public void setEnfrentamientosGrupo(LinkedHashMap<Integer, ModeloIndividualEnfrentamientoGruposFaseI2022> enfrentamientosGrupo) {
		this.enfrentamientosGrupo = enfrentamientosGrupo;
	}
	
	public ModeloIndividualGrupos32022() {
		this.enfrentamientosGrupo.put(1, new ModeloIndividualEnfrentamientoGruposFaseI2022(1, 2, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(2, new ModeloIndividualEnfrentamientoGruposFaseI2022(3, 1, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(3, new ModeloIndividualEnfrentamientoGruposFaseI2022(2, 3, TipoSorteo2022.LOCAL));
	}

}
