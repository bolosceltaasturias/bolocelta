package com.bolocelta.entities.sorteos.modelosGrupos;

import java.util.LinkedHashMap;

import com.bolocelta.bbdd.constants.TipoSorteo2022;

public class ModeloParejasGrupos32022 {

	private String nombre = "Grupo 3";
	private LinkedHashMap<Integer, ModeloParejasEnfrentamientoGruposFaseI2022> enfrentamientosGrupo = new LinkedHashMap<>();

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedHashMap<Integer, ModeloParejasEnfrentamientoGruposFaseI2022> getEnfrentamientosGrupo() {
		return enfrentamientosGrupo;
	}

	public void setEnfrentamientosGrupo(LinkedHashMap<Integer, ModeloParejasEnfrentamientoGruposFaseI2022> enfrentamientosGrupo) {
		this.enfrentamientosGrupo = enfrentamientosGrupo;
	}
	
	public ModeloParejasGrupos32022() {
		this.enfrentamientosGrupo.put(1, new ModeloParejasEnfrentamientoGruposFaseI2022(1, 2, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(2, new ModeloParejasEnfrentamientoGruposFaseI2022(3, 1, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(3, new ModeloParejasEnfrentamientoGruposFaseI2022(2, 3, TipoSorteo2022.LOCAL));
	}

}
