package com.bolocelta.entities.sorteos.modelosGrupos;

import java.util.LinkedHashMap;

import com.bolocelta.bbdd.constants.CrucesCampeonatos2022;
import com.bolocelta.bbdd.constants.GruposLetra2022;
import com.bolocelta.bbdd.constants.TipoSorteo2022;

public class ModeloIndividualOctavosFinal8Grupos2022 {

	private String nombre = "Octavos de Final";
	private LinkedHashMap<Integer, ModeloIndividualEnfrentamientoOctavosFinal2022> enfrentamientosOctavosFinal = new LinkedHashMap<>();

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedHashMap<Integer, ModeloIndividualEnfrentamientoOctavosFinal2022> getEnfrentamientosOctavosFinal() {
		return enfrentamientosOctavosFinal;
	}

	public void setEnfrentamientosOctavosFinal(LinkedHashMap<Integer, ModeloIndividualEnfrentamientoOctavosFinal2022> enfrentamientosOctavosFinal) {
		this.enfrentamientosOctavosFinal = enfrentamientosOctavosFinal;
	}
	
	public ModeloIndividualOctavosFinal8Grupos2022() {
		this.enfrentamientosOctavosFinal.put(1, new ModeloIndividualEnfrentamientoOctavosFinal2022(CrucesCampeonatos2022.OFA, 1, GruposLetra2022.FASE_I_G_A, GruposLetra2022.FASE_I_G_E, 1, 2, 0, 0, TipoSorteo2022.MONEDA, CrucesCampeonatos2022.CFA));
		this.enfrentamientosOctavosFinal.put(2, new ModeloIndividualEnfrentamientoOctavosFinal2022(CrucesCampeonatos2022.OFB, 1, GruposLetra2022.FASE_I_G_B, GruposLetra2022.FASE_I_G_F, 1, 2, 0, 0, TipoSorteo2022.MONEDA, CrucesCampeonatos2022.CFA));
		this.enfrentamientosOctavosFinal.put(3, new ModeloIndividualEnfrentamientoOctavosFinal2022(CrucesCampeonatos2022.OFC, 1, GruposLetra2022.FASE_I_G_C, GruposLetra2022.FASE_I_G_G, 1, 2, 0, 0, TipoSorteo2022.MONEDA, CrucesCampeonatos2022.CFB));
		this.enfrentamientosOctavosFinal.put(4, new ModeloIndividualEnfrentamientoOctavosFinal2022(CrucesCampeonatos2022.OFD, 1, GruposLetra2022.FASE_I_G_D, GruposLetra2022.FASE_I_G_H, 1, 2, 0, 0, TipoSorteo2022.MONEDA, CrucesCampeonatos2022.CFB));
		this.enfrentamientosOctavosFinal.put(5, new ModeloIndividualEnfrentamientoOctavosFinal2022(CrucesCampeonatos2022.OFE, 1, GruposLetra2022.FASE_I_G_E, GruposLetra2022.FASE_I_G_A, 1, 2, 0, 0, TipoSorteo2022.MONEDA, CrucesCampeonatos2022.CFC));
		this.enfrentamientosOctavosFinal.put(6, new ModeloIndividualEnfrentamientoOctavosFinal2022(CrucesCampeonatos2022.OFF, 1, GruposLetra2022.FASE_I_G_F, GruposLetra2022.FASE_I_G_B, 1, 2, 0, 0, TipoSorteo2022.MONEDA, CrucesCampeonatos2022.CFC));
		this.enfrentamientosOctavosFinal.put(7, new ModeloIndividualEnfrentamientoOctavosFinal2022(CrucesCampeonatos2022.OFG, 1, GruposLetra2022.FASE_I_G_G, GruposLetra2022.FASE_I_G_C, 1, 2, 0, 0, TipoSorteo2022.MONEDA, CrucesCampeonatos2022.CFD));
		this.enfrentamientosOctavosFinal.put(8, new ModeloIndividualEnfrentamientoOctavosFinal2022(CrucesCampeonatos2022.OFH, 1, GruposLetra2022.FASE_I_G_H, GruposLetra2022.FASE_I_G_D, 1, 2, 0, 0, TipoSorteo2022.MONEDA, CrucesCampeonatos2022.CFD));
	}

}
