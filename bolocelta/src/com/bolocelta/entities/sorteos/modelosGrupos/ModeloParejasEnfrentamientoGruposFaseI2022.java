package com.bolocelta.entities.sorteos.modelosGrupos;

public class ModeloParejasEnfrentamientoGruposFaseI2022 {

	public Integer pareja1;
	public Integer pareja2;
	public String sorteo;

	public ModeloParejasEnfrentamientoGruposFaseI2022(Integer pareja1, Integer pareja2, String sorteo) {
		this.pareja1 = pareja1;
		this.pareja2 = pareja2;
		this.sorteo = sorteo;
	}

	public Integer getPareja1() {
		return pareja1;
	}

	public void setPareja1(Integer pareja1) {
		this.pareja1 = pareja1;
	}

	public Integer getPareja2() {
		return pareja2;
	}

	public void setPareja2(Integer pareja2) {
		this.pareja2 = pareja2;
	}

	public String getSorteo() {
		return sorteo;
	}

	public void setSorteo(String sorteo) {
		this.sorteo = sorteo;
	}

}
