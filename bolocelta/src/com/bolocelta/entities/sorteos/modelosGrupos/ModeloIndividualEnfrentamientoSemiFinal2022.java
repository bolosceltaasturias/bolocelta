package com.bolocelta.entities.sorteos.modelosGrupos;

public class ModeloIndividualEnfrentamientoSemiFinal2022 {

	private Integer faseAnterior;
	private String grupoProcedenciaJugador1;
	private String grupoProcedenciaJugador2;
	private Integer posicionProcedenciaJugador1;
	private Integer posicionProcedenciaJugador2;
	private Integer jugador1;
	private Integer jugador2;
	private String sorteo;
	private String cruceCF;
	private String idCruce;

	public ModeloIndividualEnfrentamientoSemiFinal2022(String idCruce, Integer faseAnterior, String grupoProcedenciaJugador1,
			String grupoProcedenciaJugador2, Integer posicionProcedenciaJugador1, Integer posicionProcedenciaJugador2,
			Integer jugador1, Integer jugador2, String sorteo, String cruceCF) {
		this.idCruce = idCruce;
		this.faseAnterior = faseAnterior;
		this.grupoProcedenciaJugador1 = grupoProcedenciaJugador1;
		this.grupoProcedenciaJugador2 = grupoProcedenciaJugador2;
		this.posicionProcedenciaJugador1 = posicionProcedenciaJugador1;
		this.posicionProcedenciaJugador2 = posicionProcedenciaJugador2;
		this.jugador1 = jugador1;
		this.jugador2 = jugador2;
		this.sorteo = sorteo;
		this.cruceCF = cruceCF;
	}

	public Integer getJugador1() {
		return jugador1;
	}

	public void setJugador1(Integer jugador1) {
		this.jugador1 = jugador1;
	}

	public Integer getJugador2() {
		return jugador2;
	}

	public void setJugador2(Integer jugador2) {
		this.jugador2 = jugador2;
	}

	public String getSorteo() {
		return sorteo;
	}

	public void setSorteo(String sorteo) {
		this.sorteo = sorteo;
	}

	public Integer getFaseAnterior() {
		return faseAnterior;
	}

	public void setFaseAnterior(Integer faseAnterior) {
		this.faseAnterior = faseAnterior;
	}

	public String getGrupoProcedenciaJugador1() {
		return grupoProcedenciaJugador1;
	}

	public void setGrupoProcedenciaJugador1(String grupoProcedenciaJugador1) {
		this.grupoProcedenciaJugador1 = grupoProcedenciaJugador1;
	}

	public String getGrupoProcedenciaJugador2() {
		return grupoProcedenciaJugador2;
	}

	public void setGrupoProcedenciaJugador2(String grupoProcedenciaJugador2) {
		this.grupoProcedenciaJugador2 = grupoProcedenciaJugador2;
	}

	public Integer getPosicionProcedenciaJugador1() {
		return posicionProcedenciaJugador1;
	}

	public void setPosicionProcedenciaJugador1(Integer posicionProcedenciaJugador1) {
		this.posicionProcedenciaJugador1 = posicionProcedenciaJugador1;
	}

	public Integer getPosicionProcedenciaJugador2() {
		return posicionProcedenciaJugador2;
	}

	public void setPosicionProcedenciaJugador2(Integer posicionProcedenciaJugador2) {
		this.posicionProcedenciaJugador2 = posicionProcedenciaJugador2;
	}

	public String getCruceCF() {
		return cruceCF;
	}

	public void setCruceCF(String cruceCF) {
		this.cruceCF = cruceCF;
	}

	public String getIdCruce() {
		return idCruce;
	}

	public void setIdCruce(String idCruce) {
		this.idCruce = idCruce;
	}

}
