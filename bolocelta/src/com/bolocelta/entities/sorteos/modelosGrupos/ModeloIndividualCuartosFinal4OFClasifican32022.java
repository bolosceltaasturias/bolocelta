package com.bolocelta.entities.sorteos.modelosGrupos;

import java.util.LinkedHashMap;

import com.bolocelta.bbdd.constants.CrucesCampeonatos2022;
import com.bolocelta.bbdd.constants.GruposLetra2022;
import com.bolocelta.bbdd.constants.TipoSorteo2022;

public class ModeloIndividualCuartosFinal4OFClasifican32022 {

	private String nombre = "Cuartos de Final";
	private LinkedHashMap<Integer, ModeloIndividualEnfrentamientoCuartosFinal2022> enfrentamientosCuartosFinal = new LinkedHashMap<>();

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedHashMap<Integer, ModeloIndividualEnfrentamientoCuartosFinal2022> getEnfrentamientosCuartosFinal() {
		return enfrentamientosCuartosFinal;
	}

	public void setEnfrentamientosCuartosFinal(LinkedHashMap<Integer, ModeloIndividualEnfrentamientoCuartosFinal2022> enfrentamientosCuartosFinal) {
		this.enfrentamientosCuartosFinal = enfrentamientosCuartosFinal;
	}
	
	public ModeloIndividualCuartosFinal4OFClasifican32022() {
		this.enfrentamientosCuartosFinal.put(1, new ModeloIndividualEnfrentamientoCuartosFinal2022(CrucesCampeonatos2022.CFA, 1, GruposLetra2022.FASE_I_G_A, CrucesCampeonatos2022.OFA, 1, 0, 0, 0, TipoSorteo2022.MONEDA, CrucesCampeonatos2022.SF1));
		this.enfrentamientosCuartosFinal.put(2, new ModeloIndividualEnfrentamientoCuartosFinal2022(CrucesCampeonatos2022.CFB, 1, GruposLetra2022.FASE_I_G_B, CrucesCampeonatos2022.OFB, 1, 0, 0, 0, TipoSorteo2022.MONEDA, CrucesCampeonatos2022.SF1));
		this.enfrentamientosCuartosFinal.put(3, new ModeloIndividualEnfrentamientoCuartosFinal2022(CrucesCampeonatos2022.CFC, 1, GruposLetra2022.FASE_I_G_C, CrucesCampeonatos2022.OFC, 1, 0, 0, 0, TipoSorteo2022.MONEDA, CrucesCampeonatos2022.SF2));
		this.enfrentamientosCuartosFinal.put(4, new ModeloIndividualEnfrentamientoCuartosFinal2022(CrucesCampeonatos2022.CFD, 1, GruposLetra2022.FASE_I_G_D, CrucesCampeonatos2022.OFD, 1, 0, 0, 0, TipoSorteo2022.MONEDA, CrucesCampeonatos2022.SF2));
	}

}
