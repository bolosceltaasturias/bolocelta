package com.bolocelta.entities.sorteos.modelosGrupos;

import java.util.LinkedHashMap;

import com.bolocelta.bbdd.constants.TipoSorteo2022;

public class ModeloParejasGrupos62022 {

	private String nombre = "Grupo 6";
	private LinkedHashMap<Integer, ModeloParejasEnfrentamientoGruposFaseI2022> enfrentamientosGrupo = new LinkedHashMap<>();

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedHashMap<Integer, ModeloParejasEnfrentamientoGruposFaseI2022> getEnfrentamientosGrupo() {
		return enfrentamientosGrupo;
	}

	public void setEnfrentamientosGrupo(LinkedHashMap<Integer, ModeloParejasEnfrentamientoGruposFaseI2022> enfrentamientosGrupo) {
		this.enfrentamientosGrupo = enfrentamientosGrupo;
	}
	
	public ModeloParejasGrupos62022() {
		this.enfrentamientosGrupo.put(1, new ModeloParejasEnfrentamientoGruposFaseI2022(1, 2, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(2, new ModeloParejasEnfrentamientoGruposFaseI2022(3, 4, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(3, new ModeloParejasEnfrentamientoGruposFaseI2022(6, 5, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(4, new ModeloParejasEnfrentamientoGruposFaseI2022(4, 1, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(5, new ModeloParejasEnfrentamientoGruposFaseI2022(5, 3, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(6, new ModeloParejasEnfrentamientoGruposFaseI2022(2, 6, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(7, new ModeloParejasEnfrentamientoGruposFaseI2022(1, 3, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(8, new ModeloParejasEnfrentamientoGruposFaseI2022(4, 6, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(9, new ModeloParejasEnfrentamientoGruposFaseI2022(2, 5, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(10, new ModeloParejasEnfrentamientoGruposFaseI2022(6, 1, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(11, new ModeloParejasEnfrentamientoGruposFaseI2022(3, 2, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(12, new ModeloParejasEnfrentamientoGruposFaseI2022(5, 4, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(13, new ModeloParejasEnfrentamientoGruposFaseI2022(3, 6, TipoSorteo2022.MONEDA));
		this.enfrentamientosGrupo.put(14, new ModeloParejasEnfrentamientoGruposFaseI2022(1, 5, TipoSorteo2022.MONEDA));
		this.enfrentamientosGrupo.put(15, new ModeloParejasEnfrentamientoGruposFaseI2022(2, 4, TipoSorteo2022.MONEDA));
	}

}
