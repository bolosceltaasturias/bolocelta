package com.bolocelta.entities.sorteos.modelosGrupos;

import java.util.LinkedHashMap;

import com.bolocelta.bbdd.constants.CrucesCampeonatos2022;
import com.bolocelta.bbdd.constants.TipoSorteo2022;

public class ModeloIndividualFinalConsolacion4SF2022 {

	private String nombre = "Final Consolacion";
	private LinkedHashMap<Integer, ModeloIndividualEnfrentamientoFinalConsolacion2022> enfrentamientosFinalConsolacion = new LinkedHashMap<>();

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedHashMap<Integer, ModeloIndividualEnfrentamientoFinalConsolacion2022> getEnfrentamientosFinalConsolacion() {
		return enfrentamientosFinalConsolacion;
	}

	public void setEnfrentamientosFinalConsolacion(LinkedHashMap<Integer, ModeloIndividualEnfrentamientoFinalConsolacion2022> enfrentamientosFinalConsolacion) {
		this.enfrentamientosFinalConsolacion = enfrentamientosFinalConsolacion;
	}
	
	public ModeloIndividualFinalConsolacion4SF2022() {
		this.enfrentamientosFinalConsolacion.put(1, new ModeloIndividualEnfrentamientoFinalConsolacion2022(CrucesCampeonatos2022.FC1, 1, CrucesCampeonatos2022.SF1, CrucesCampeonatos2022.SF2, 0, 0, 0, 0, TipoSorteo2022.MONEDA, null));
	}

}
