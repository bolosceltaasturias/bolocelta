package com.bolocelta.entities.sorteos.modelosGrupos;

import java.util.LinkedHashMap;

import com.bolocelta.bbdd.constants.CrucesCampeonatos2022;
import com.bolocelta.bbdd.constants.TipoSorteo2022;

public class ModeloIndividualCuartosFinal4Boladas2022 {

	private String nombre = "Cuartos de Final";
	private LinkedHashMap<Integer, ModeloIndividualEnfrentamientoCuartosFinal2022> enfrentamientosCuartosFinal = new LinkedHashMap<>();

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedHashMap<Integer, ModeloIndividualEnfrentamientoCuartosFinal2022> getEnfrentamientosCuartosFinal() {
		return enfrentamientosCuartosFinal;
	}

	public void setEnfrentamientosCuartosFinal(LinkedHashMap<Integer, ModeloIndividualEnfrentamientoCuartosFinal2022> enfrentamientosCuartosFinal) {
		this.enfrentamientosCuartosFinal = enfrentamientosCuartosFinal;
	}
	
	public ModeloIndividualCuartosFinal4Boladas2022() {
		this.enfrentamientosCuartosFinal.put(1, new ModeloIndividualEnfrentamientoCuartosFinal2022(CrucesCampeonatos2022.CFA, 1, CrucesCampeonatos2022.CB1, CrucesCampeonatos2022.CB8, 1, 8, 0, 0, TipoSorteo2022.LOCAL, CrucesCampeonatos2022.SF1));
		this.enfrentamientosCuartosFinal.put(2, new ModeloIndividualEnfrentamientoCuartosFinal2022(CrucesCampeonatos2022.CFB, 1, CrucesCampeonatos2022.CB2, CrucesCampeonatos2022.CB7, 2, 7, 0, 0, TipoSorteo2022.LOCAL, CrucesCampeonatos2022.SF2));
		this.enfrentamientosCuartosFinal.put(3, new ModeloIndividualEnfrentamientoCuartosFinal2022(CrucesCampeonatos2022.CFC, 1, CrucesCampeonatos2022.CB3, CrucesCampeonatos2022.CB6, 3, 6, 0, 0, TipoSorteo2022.LOCAL, CrucesCampeonatos2022.SF2));
		this.enfrentamientosCuartosFinal.put(4, new ModeloIndividualEnfrentamientoCuartosFinal2022(CrucesCampeonatos2022.CFD, 1, CrucesCampeonatos2022.CB4, CrucesCampeonatos2022.CB5, 4, 5, 0, 0, TipoSorteo2022.LOCAL, CrucesCampeonatos2022.SF1));

	}

}
