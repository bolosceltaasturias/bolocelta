package com.bolocelta.entities.sorteos.modelosGrupos;

import java.util.LinkedHashMap;

import com.bolocelta.bbdd.constants.CrucesCampeonatos2022;
import com.bolocelta.bbdd.constants.TipoSorteo2022;

public class ModeloParejasFinal4SF2022 {

	private String nombre = "Final";
	private LinkedHashMap<Integer, ModeloParejasEnfrentamientoFinal2022> enfrentamientosFinal = new LinkedHashMap<>();

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedHashMap<Integer, ModeloParejasEnfrentamientoFinal2022> getEnfrentamientosFinal() {
		return enfrentamientosFinal;
	}

	public void setEnfrentamientosFinal(LinkedHashMap<Integer, ModeloParejasEnfrentamientoFinal2022> enfrentamientosFinal) {
		this.enfrentamientosFinal = enfrentamientosFinal;
	}
	
	public ModeloParejasFinal4SF2022() {
		this.enfrentamientosFinal.put(1, new ModeloParejasEnfrentamientoFinal2022(CrucesCampeonatos2022.FF1, 1, CrucesCampeonatos2022.SF1, CrucesCampeonatos2022.SF2, 0, 0, 0, 0, TipoSorteo2022.MONEDA, null));
	}

}
