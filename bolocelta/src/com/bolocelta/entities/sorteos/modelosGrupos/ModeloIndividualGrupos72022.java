package com.bolocelta.entities.sorteos.modelosGrupos;

import java.util.LinkedHashMap;

import com.bolocelta.bbdd.constants.TipoSorteo2022;

public class ModeloIndividualGrupos72022 {

	private String nombre = "Grupo 7";
	private LinkedHashMap<Integer, ModeloIndividualEnfrentamientoGruposFaseI2022> enfrentamientosGrupo = new LinkedHashMap<>();

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedHashMap<Integer, ModeloIndividualEnfrentamientoGruposFaseI2022> getEnfrentamientosGrupo() {
		return enfrentamientosGrupo;
	}

	public void setEnfrentamientosGrupo(LinkedHashMap<Integer, ModeloIndividualEnfrentamientoGruposFaseI2022> enfrentamientosGrupo) {
		this.enfrentamientosGrupo = enfrentamientosGrupo;
	}
	
	public ModeloIndividualGrupos72022() {
		this.enfrentamientosGrupo.put(1, new ModeloIndividualEnfrentamientoGruposFaseI2022(1, 2, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(2, new ModeloIndividualEnfrentamientoGruposFaseI2022(3, 4, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(3, new ModeloIndividualEnfrentamientoGruposFaseI2022(5, 6, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(4, new ModeloIndividualEnfrentamientoGruposFaseI2022(7, 1, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(5, new ModeloIndividualEnfrentamientoGruposFaseI2022(2, 3, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(6, new ModeloIndividualEnfrentamientoGruposFaseI2022(4, 5, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(7, new ModeloIndividualEnfrentamientoGruposFaseI2022(6, 7, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(8, new ModeloIndividualEnfrentamientoGruposFaseI2022(3, 1, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(9, new ModeloIndividualEnfrentamientoGruposFaseI2022(4, 2, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(10, new ModeloIndividualEnfrentamientoGruposFaseI2022(7, 5, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(11, new ModeloIndividualEnfrentamientoGruposFaseI2022(3, 6, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(12, new ModeloIndividualEnfrentamientoGruposFaseI2022(1, 4, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(13, new ModeloIndividualEnfrentamientoGruposFaseI2022(2, 7, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(14, new ModeloIndividualEnfrentamientoGruposFaseI2022(5, 3, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(15, new ModeloIndividualEnfrentamientoGruposFaseI2022(1, 6, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(16, new ModeloIndividualEnfrentamientoGruposFaseI2022(4, 7, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(17, new ModeloIndividualEnfrentamientoGruposFaseI2022(2, 5, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(18, new ModeloIndividualEnfrentamientoGruposFaseI2022(6, 4, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(19, new ModeloIndividualEnfrentamientoGruposFaseI2022(5, 1, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(20, new ModeloIndividualEnfrentamientoGruposFaseI2022(7, 3, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(21, new ModeloIndividualEnfrentamientoGruposFaseI2022(6, 2, TipoSorteo2022.LOCAL));
	}

}
