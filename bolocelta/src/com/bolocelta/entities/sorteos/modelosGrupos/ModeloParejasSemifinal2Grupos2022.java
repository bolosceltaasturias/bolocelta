package com.bolocelta.entities.sorteos.modelosGrupos;

import java.util.LinkedHashMap;

import com.bolocelta.bbdd.constants.CrucesCampeonatos2022;
import com.bolocelta.bbdd.constants.GruposLetra2022;
import com.bolocelta.bbdd.constants.TipoSorteo2022;

public class ModeloParejasSemifinal2Grupos2022 {

	private String nombre = "Semifinal";
	private LinkedHashMap<Integer, ModeloParejasEnfrentamientoSemiFinal2022> enfrentamientosSemiFinal = new LinkedHashMap<>();

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedHashMap<Integer, ModeloParejasEnfrentamientoSemiFinal2022> getEnfrentamientosSemiFinal() {
		return enfrentamientosSemiFinal;
	}

	public void getEnfrentamientosSemifinal(LinkedHashMap<Integer, ModeloParejasEnfrentamientoSemiFinal2022> enfrentamientosSemiFinal) {
		this.enfrentamientosSemiFinal = enfrentamientosSemiFinal;
	}
	
	public ModeloParejasSemifinal2Grupos2022() {
		this.enfrentamientosSemiFinal.put(1, new ModeloParejasEnfrentamientoSemiFinal2022(CrucesCampeonatos2022.SF1, 1, GruposLetra2022.FASE_I_G_A, GruposLetra2022.FASE_I_G_B, 1, 2, 0, 0, TipoSorteo2022.MONEDA, CrucesCampeonatos2022.FF1));
		this.enfrentamientosSemiFinal.put(2, new ModeloParejasEnfrentamientoSemiFinal2022(CrucesCampeonatos2022.SF2, 1, GruposLetra2022.FASE_I_G_B, GruposLetra2022.FASE_I_G_A, 1, 2, 0, 0, TipoSorteo2022.MONEDA, CrucesCampeonatos2022.FF1));
	}

}
