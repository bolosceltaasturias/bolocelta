package com.bolocelta.entities.sorteos.modelosGrupos;

import java.util.LinkedHashMap;

import com.bolocelta.bbdd.constants.TipoSorteo2022;

public class ModeloIndividualGrupos82022 {

	private String nombre = "Grupos 8";
	private LinkedHashMap<Integer, ModeloIndividualEnfrentamientoGruposFaseI2022> enfrentamientosGrupo = new LinkedHashMap<>();

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedHashMap<Integer, ModeloIndividualEnfrentamientoGruposFaseI2022> getEnfrentamientosGrupo() {
		return enfrentamientosGrupo;
	}

	public void setEnfrentamientosGrupo(LinkedHashMap<Integer, ModeloIndividualEnfrentamientoGruposFaseI2022> enfrentamientosGrupo) {
		this.enfrentamientosGrupo = enfrentamientosGrupo;
	}
	
	public ModeloIndividualGrupos82022() {
		this.enfrentamientosGrupo.put(1, new ModeloIndividualEnfrentamientoGruposFaseI2022(1, 2, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(2, new ModeloIndividualEnfrentamientoGruposFaseI2022(3, 4, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(3, new ModeloIndividualEnfrentamientoGruposFaseI2022(5, 6, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(4, new ModeloIndividualEnfrentamientoGruposFaseI2022(7, 8, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(5, new ModeloIndividualEnfrentamientoGruposFaseI2022(4, 1, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(6, new ModeloIndividualEnfrentamientoGruposFaseI2022(2, 3, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(7, new ModeloIndividualEnfrentamientoGruposFaseI2022(6, 7, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(8, new ModeloIndividualEnfrentamientoGruposFaseI2022(8, 5, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(9, new ModeloIndividualEnfrentamientoGruposFaseI2022(1, 6, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(10, new ModeloIndividualEnfrentamientoGruposFaseI2022(7, 4, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(11, new ModeloIndividualEnfrentamientoGruposFaseI2022(5, 2, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(12, new ModeloIndividualEnfrentamientoGruposFaseI2022(3, 8, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(13, new ModeloIndividualEnfrentamientoGruposFaseI2022(2, 7, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(14, new ModeloIndividualEnfrentamientoGruposFaseI2022(4, 5, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(15, new ModeloIndividualEnfrentamientoGruposFaseI2022(6, 3, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(16, new ModeloIndividualEnfrentamientoGruposFaseI2022(8, 1, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(17, new ModeloIndividualEnfrentamientoGruposFaseI2022(5, 7, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(18, new ModeloIndividualEnfrentamientoGruposFaseI2022(2, 4, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(19, new ModeloIndividualEnfrentamientoGruposFaseI2022(6, 8, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(20, new ModeloIndividualEnfrentamientoGruposFaseI2022(1, 3, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(21, new ModeloIndividualEnfrentamientoGruposFaseI2022(4, 6, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(22, new ModeloIndividualEnfrentamientoGruposFaseI2022(8, 2, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(23, new ModeloIndividualEnfrentamientoGruposFaseI2022(3, 5, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(24, new ModeloIndividualEnfrentamientoGruposFaseI2022(7, 1, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(25, new ModeloIndividualEnfrentamientoGruposFaseI2022(2, 6, TipoSorteo2022.MONEDA));
		this.enfrentamientosGrupo.put(26, new ModeloIndividualEnfrentamientoGruposFaseI2022(4, 8, TipoSorteo2022.MONEDA));
		this.enfrentamientosGrupo.put(27, new ModeloIndividualEnfrentamientoGruposFaseI2022(1, 5, TipoSorteo2022.MONEDA));
		this.enfrentamientosGrupo.put(28, new ModeloIndividualEnfrentamientoGruposFaseI2022(3, 7, TipoSorteo2022.MONEDA));
	}

}
