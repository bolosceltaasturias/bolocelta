package com.bolocelta.entities.sorteos.modelosGrupos;

import java.util.LinkedHashMap;

import com.bolocelta.bbdd.constants.TipoSorteo2022;

public class ModeloParejasGrupos82022 {

	private String nombre = "Grupos 8";
	private LinkedHashMap<Integer, ModeloParejasEnfrentamientoGruposFaseI2022> enfrentamientosGrupo = new LinkedHashMap<>();

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedHashMap<Integer, ModeloParejasEnfrentamientoGruposFaseI2022> getEnfrentamientosGrupo() {
		return enfrentamientosGrupo;
	}

	public void setEnfrentamientosGrupo(LinkedHashMap<Integer, ModeloParejasEnfrentamientoGruposFaseI2022> enfrentamientosGrupo) {
		this.enfrentamientosGrupo = enfrentamientosGrupo;
	}
	
	public ModeloParejasGrupos82022() {
		this.enfrentamientosGrupo.put(1, new ModeloParejasEnfrentamientoGruposFaseI2022(1, 2, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(2, new ModeloParejasEnfrentamientoGruposFaseI2022(3, 4, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(3, new ModeloParejasEnfrentamientoGruposFaseI2022(5, 6, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(4, new ModeloParejasEnfrentamientoGruposFaseI2022(7, 8, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(5, new ModeloParejasEnfrentamientoGruposFaseI2022(4, 1, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(6, new ModeloParejasEnfrentamientoGruposFaseI2022(2, 3, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(7, new ModeloParejasEnfrentamientoGruposFaseI2022(6, 7, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(8, new ModeloParejasEnfrentamientoGruposFaseI2022(8, 5, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(9, new ModeloParejasEnfrentamientoGruposFaseI2022(1, 6, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(10, new ModeloParejasEnfrentamientoGruposFaseI2022(7, 4, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(11, new ModeloParejasEnfrentamientoGruposFaseI2022(5, 2, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(12, new ModeloParejasEnfrentamientoGruposFaseI2022(3, 8, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(13, new ModeloParejasEnfrentamientoGruposFaseI2022(2, 7, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(14, new ModeloParejasEnfrentamientoGruposFaseI2022(4, 5, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(15, new ModeloParejasEnfrentamientoGruposFaseI2022(6, 3, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(16, new ModeloParejasEnfrentamientoGruposFaseI2022(8, 1, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(17, new ModeloParejasEnfrentamientoGruposFaseI2022(5, 7, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(18, new ModeloParejasEnfrentamientoGruposFaseI2022(2, 4, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(19, new ModeloParejasEnfrentamientoGruposFaseI2022(6, 8, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(20, new ModeloParejasEnfrentamientoGruposFaseI2022(1, 3, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(21, new ModeloParejasEnfrentamientoGruposFaseI2022(4, 6, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(22, new ModeloParejasEnfrentamientoGruposFaseI2022(8, 2, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(23, new ModeloParejasEnfrentamientoGruposFaseI2022(3, 5, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(24, new ModeloParejasEnfrentamientoGruposFaseI2022(7, 1, TipoSorteo2022.LOCAL));
		this.enfrentamientosGrupo.put(25, new ModeloParejasEnfrentamientoGruposFaseI2022(2, 6, TipoSorteo2022.MONEDA));
		this.enfrentamientosGrupo.put(26, new ModeloParejasEnfrentamientoGruposFaseI2022(4, 8, TipoSorteo2022.MONEDA));
		this.enfrentamientosGrupo.put(27, new ModeloParejasEnfrentamientoGruposFaseI2022(1, 5, TipoSorteo2022.MONEDA));
		this.enfrentamientosGrupo.put(28, new ModeloParejasEnfrentamientoGruposFaseI2022(3, 7, TipoSorteo2022.MONEDA));
	}

}
