package com.bolocelta.entities.sorteos.modelosGrupos;

import java.util.LinkedHashMap;

import com.bolocelta.bbdd.constants.CrucesCampeonatos2022;
import com.bolocelta.bbdd.constants.TipoSorteo2022;

public class ModeloIndividualSemiFinal4Boladas2022 {

	private String nombre = "Semifinal";
	private LinkedHashMap<Integer, ModeloIndividualEnfrentamientoSemiFinal2022> enfrentamientosSemiFinal = new LinkedHashMap<>();

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedHashMap<Integer, ModeloIndividualEnfrentamientoSemiFinal2022> getEnfrentamientosSemiFinal() {
		return enfrentamientosSemiFinal;
	}

	public void setEnfrentamientosSemiFinal(LinkedHashMap<Integer, ModeloIndividualEnfrentamientoSemiFinal2022> enfrentamientosSemiFinal) {
		this.enfrentamientosSemiFinal = enfrentamientosSemiFinal;
	}
	
	public ModeloIndividualSemiFinal4Boladas2022() {
		this.enfrentamientosSemiFinal.put(1, new ModeloIndividualEnfrentamientoSemiFinal2022(CrucesCampeonatos2022.SF1, 1, CrucesCampeonatos2022.CB1, CrucesCampeonatos2022.CB4, 1, 4, 0, 0, TipoSorteo2022.LOCAL, CrucesCampeonatos2022.FF1));
		this.enfrentamientosSemiFinal.put(2, new ModeloIndividualEnfrentamientoSemiFinal2022(CrucesCampeonatos2022.SF2, 1, CrucesCampeonatos2022.CB2, CrucesCampeonatos2022.CB3, 2, 3, 0, 0, TipoSorteo2022.LOCAL, CrucesCampeonatos2022.FF1));
	}

}
