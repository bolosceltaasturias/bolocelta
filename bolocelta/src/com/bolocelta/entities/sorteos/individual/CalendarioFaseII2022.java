package com.bolocelta.entities.sorteos.individual;

public class CalendarioFaseII2022 {

	private Integer id;
	private Integer categoriaId;
	private String grupo;
	private Integer jugador1Id;
	private Integer juegosJugador1;
	private Integer juegosJugador2;
	private Integer jugador2Id;
	private Integer orden;
	private String activo;
	private Long rowNum;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCategoriaId() {
		return categoriaId;
	}

	public void setCategoriaId(Integer categoriaId) {
		this.categoriaId = categoriaId;
	}

	public String getGrupo() {
		return grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

	public Integer getJugador1Id() {
		return jugador1Id;
	}

	public void setJugador1Id(Integer jugador1Id) {
		this.jugador1Id = jugador1Id;
	}

	public Integer getJuegosJugador1() {
		return juegosJugador1;
	}

	public void setJuegosJugador1(Integer juegosJugador1) {
		this.juegosJugador1 = juegosJugador1;
	}

	public Integer getJuegosJugador2() {
		return juegosJugador2;
	}

	public void setJuegosJugador2(Integer juegosJugador2) {
		this.juegosJugador2 = juegosJugador2;
	}

	public Integer getJugador2Id() {
		return jugador2Id;
	}

	public void setJugador2Id(Integer jugador2Id) {
		this.jugador2Id = jugador2Id;
	}

	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}

	public String getActivo() {
		return activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	public Long getRowNum() {
		return rowNum;
	}

	public void setRowNum(Long rowNum) {
		this.rowNum = rowNum;
	}

}
