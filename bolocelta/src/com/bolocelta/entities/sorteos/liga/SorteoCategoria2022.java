package com.bolocelta.entities.sorteos.liga;

import java.util.HashMap;

import com.bolocelta.entities.Categorias2022;
import com.bolocelta.entities.Equipos2022;

public class SorteoCategoria2022 {

	private Categorias2022 categoria;
	private HashMap<Integer, Equipos2022> equiposPorCategoria = new HashMap<>();

	public Categorias2022 getCategoria() {
		return categoria;
	}

	public void setCategoria(Categorias2022 categoria) {
		this.categoria = categoria;
	}

	public HashMap<Integer, Equipos2022> getEquiposPorCategoria() {
		return equiposPorCategoria;
	}

	public void setEquiposPorCategoria(HashMap<Integer, Equipos2022> equiposPorCategoria) {
		this.equiposPorCategoria = equiposPorCategoria;
	}

}
