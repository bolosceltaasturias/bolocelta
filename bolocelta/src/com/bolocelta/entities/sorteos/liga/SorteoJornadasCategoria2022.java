package com.bolocelta.entities.sorteos.liga;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import com.bolocelta.entities.JornadaCategoria2022;

public class SorteoJornadasCategoria2022 {

	private String id;
	private Date fechaDesde;
	private Date fechaHasta;
	private HashMap<String, JornadaCategoria2022> jornadasCategoriasMap = new HashMap<>();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public Date getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public HashMap<String, JornadaCategoria2022> getJornadasCategoriasMap() {
		return jornadasCategoriasMap;
	}

	public void setJornadasCategoriasMap(HashMap<String, JornadaCategoria2022> jornadasCategoriasMap) {
		this.jornadasCategoriasMap = jornadasCategoriasMap;
	}
	
	public String getFechaDesdeText(){
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String fechaDesdeText = df.format(this.fechaDesde);
		return fechaDesdeText;
	}
	
	public String getFechaHastaText(){
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String fechaHastaText = df.format(this.fechaHasta);
		return fechaHastaText;
	}

}
