package com.bolocelta.entities.sorteos.liga;

import java.util.HashMap;

import com.bolocelta.entities.Boleras2022;
import com.bolocelta.entities.Equipos2022;

public class SorteoBolerasEquipos2022 {

	private Boleras2022 bolera;
	private HashMap<Integer, Equipos2022> equipos = new HashMap<>();

	public Boleras2022 getBolera() {
		return bolera;
	}

	public void setBolera(Boleras2022 bolera) {
		this.bolera = bolera;
	}

	public HashMap<Integer, Equipos2022> getEquipos() {
		return equipos;
	}

	public void setEquipos(HashMap<Integer, Equipos2022> equipos) {
		this.equipos = equipos;
	}
	
	public Integer getNumeroEquipos(){
		return this.equipos.size();
	}

}
