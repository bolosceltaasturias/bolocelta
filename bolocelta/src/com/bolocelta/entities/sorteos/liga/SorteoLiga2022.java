package com.bolocelta.entities.sorteos.liga;

import java.util.HashMap;
import java.util.LinkedHashMap;

import com.bolocelta.entities.CampeonatoEquiposCalendario2022;

public class SorteoLiga2022 {
	
	private LinkedHashMap<String, SorteoBolerasCrucesLiga2022> sorteoBolerasCrucesLigaMap = new LinkedHashMap<>();
	private LinkedHashMap<Integer, SorteoTuplaEnfrentamientosLiga2022> sorteoTuplaEnfrentamientosLigaMap = new LinkedHashMap<>();
	
	

	private LinkedHashMap<Integer, SorteoCategoria2022> sorteoCategoriasMap = new LinkedHashMap<>();
	private LinkedHashMap<Integer, SorteoBolerasEquipos2022> sorteoBolerasEquiposMap = new LinkedHashMap<>();
	private HashMap<Integer, SorteoEnfrentamientosCategoria2022> sorteoEnfrentamientosCategoriaMap = new HashMap<>();
	private LinkedHashMap<String, SorteoJornadasCategoria2022> sorteoJornadasCategoriaMap = new LinkedHashMap<>();
	
	//Enfrentamientos jornada asignados
	private LinkedHashMap<String, CampeonatoEquiposCalendario2022> enfrentamientosJornadaAsignados = new LinkedHashMap<>();

	public LinkedHashMap<Integer, SorteoCategoria2022> getSorteoCategoriasMap() {
		return sorteoCategoriasMap;
	}

	public void setSorteoCategoriasMap(LinkedHashMap<Integer, SorteoCategoria2022> sorteoCategoriasMap) {
		this.sorteoCategoriasMap = sorteoCategoriasMap;
	}

	public LinkedHashMap<Integer, SorteoBolerasEquipos2022> getSorteoBolerasEquiposMap() {
		return sorteoBolerasEquiposMap;
	}

	public void setSorteoBolerasEquiposMap(LinkedHashMap<Integer, SorteoBolerasEquipos2022> sorteoBolerasEquiposMap) {
		this.sorteoBolerasEquiposMap = sorteoBolerasEquiposMap;
	}

	public HashMap<Integer, SorteoEnfrentamientosCategoria2022> getSorteoEnfrentamientosCategoriaMap() {
		return sorteoEnfrentamientosCategoriaMap;
	}

	public void setSorteoEnfrentamientosCategoriaMap(
			LinkedHashMap<Integer, SorteoEnfrentamientosCategoria2022> sorteoEnfrentamientosCategoriaMap) {
		this.sorteoEnfrentamientosCategoriaMap = sorteoEnfrentamientosCategoriaMap;
	}

	public LinkedHashMap<String, SorteoJornadasCategoria2022> getSorteoJornadasCategoriaMap() {
		return sorteoJornadasCategoriaMap;
	}

	public void setSorteoJornadasCategoriaMap(LinkedHashMap<String, SorteoJornadasCategoria2022> sorteoJornadasCategoriaMap) {
		this.sorteoJornadasCategoriaMap = sorteoJornadasCategoriaMap;
	}

	public LinkedHashMap<String, CampeonatoEquiposCalendario2022> getEnfrentamientosJornadaAsignados() {
		return enfrentamientosJornadaAsignados;
	}

	public void setEnfrentamientosJornadaAsignados(
			LinkedHashMap<String, CampeonatoEquiposCalendario2022> enfrentamientosJornadaAsignados) {
		this.enfrentamientosJornadaAsignados = enfrentamientosJornadaAsignados;
	}

	public LinkedHashMap<String, SorteoBolerasCrucesLiga2022> getSorteoBolerasCrucesLigaMap() {
		return sorteoBolerasCrucesLigaMap;
	}

	public void setSorteoBolerasCrucesLigaMap(LinkedHashMap<String, SorteoBolerasCrucesLiga2022> sorteoBolerasCrucesLigaMap) {
		this.sorteoBolerasCrucesLigaMap = sorteoBolerasCrucesLigaMap;
	}

	public LinkedHashMap<Integer, SorteoTuplaEnfrentamientosLiga2022> getSorteoTuplaEnfrentamientosLigaMap() {
		return sorteoTuplaEnfrentamientosLigaMap;
	}

	public void setSorteoTuplaEnfrentamientosLigaMap(
			LinkedHashMap<Integer, SorteoTuplaEnfrentamientosLiga2022> sorteoTuplaEnfrentamientosLigaMap) {
		this.sorteoTuplaEnfrentamientosLigaMap = sorteoTuplaEnfrentamientosLigaMap;
	}

}
