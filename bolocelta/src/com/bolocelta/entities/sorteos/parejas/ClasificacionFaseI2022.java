package com.bolocelta.entities.sorteos.parejas;

import com.bolocelta.entities.Parejas2022;

public class ClasificacionFaseI2022 {

	private Integer id;
	private Integer categoriaId;
	private String grupo;
	private Integer parejaId;
	private Parejas2022 pareja;
	private Integer pj;
	private Integer pg;
	private Integer pe;
	private Integer pp;
	private Integer pf;
	private Integer pc;
	private Integer pt;
	private Long rowNum;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCategoriaId() {
		return categoriaId;
	}

	public void setCategoriaId(Integer categoriaId) {
		this.categoriaId = categoriaId;
	}

	public String getGrupo() {
		return grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

	public Integer getParejaId() {
		return parejaId;
	}

	public void setParejaId(Integer parejaId) {
		this.parejaId = parejaId;
	}

	public Integer getPj() {
		return pj;
	}

	public void setPj(Integer pj) {
		this.pj = pj;
	}

	public Integer getPg() {
		return pg;
	}

	public void setPg(Integer pg) {
		this.pg = pg;
	}

	public Integer getPe() {
		return pe;
	}

	public void setPe(Integer pe) {
		this.pe = pe;
	}

	public Integer getPp() {
		return pp;
	}

	public void setPp(Integer pp) {
		this.pp = pp;
	}

	public Integer getPf() {
		return pf;
	}

	public void setPf(Integer pf) {
		this.pf = pf;
	}

	public Integer getPc() {
		return pc;
	}

	public void setPc(Integer pc) {
		this.pc = pc;
	}

	public Integer getPt() {
		return pt;
	}

	public void setPt(Integer pt) {
		this.pt = pt;
	}

	public Long getRowNum() {
		return rowNum;
	}

	public void setRowNum(Long rowNum) {
		this.rowNum = rowNum;
	}
	
	public String getInsertRow(){
		return (this.id + ";" +
					this.categoriaId + ";" +
					this.grupo + ";" +
					this.parejaId + ";" +
					this.pj + ";" +
					this.pg + ";" +
					this.pe + ";" +
					this.pp + ";" +
					this.pf + ";" +
					this.pc + ";" +
					this.pt
					);
	}

	public Parejas2022 getPareja() {
		return pareja;
	}

	public void setPareja(Parejas2022 pareja) {
		this.pareja = pareja;
	}
	
	public String getKey(){
		return this.id + "-" + this.categoriaId + "-" + this.grupo;
	}

}
