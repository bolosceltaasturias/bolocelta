package com.bolocelta.entities;

public class Fases2022 {

	private Integer id;
	private Integer numero;
	private String nombre;
	private String tipoEnfrentamiento;
	private Integer numeroEnfrentamientos;
	private Integer numeroPartidas;
	private Integer numeroJuegos;
	private Integer clasifican;
	private String estado;
	private String activo;
	private String fecha;
	private String hora;
	private Long rowNum;
	private Integer faseSiguiente;

	public Fases2022() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getActivo() {
		return activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	public Long getRowNum() {
		return rowNum;
	}

	public void setRowNum(Long rowNum) {
		this.rowNum = rowNum;
	}

	public String getTipoEnfrentamiento() {
		return tipoEnfrentamiento;
	}

	public void setTipoEnfrentamiento(String tipoEnfrentamiento) {
		this.tipoEnfrentamiento = tipoEnfrentamiento;
	}

	public Integer getNumeroEnfrentamientos() {
		return numeroEnfrentamientos;
	}

	public void setNumeroEnfrentamientos(Integer numeroEnfrentamientos) {
		this.numeroEnfrentamientos = numeroEnfrentamientos;
	}

	public Integer getNumeroPartidas() {
		return numeroPartidas;
	}

	public void setNumeroPartidas(Integer numeroPartidas) {
		this.numeroPartidas = numeroPartidas;
	}

	public Integer getNumeroJuegos() {
		return numeroJuegos;
	}

	public void setNumeroJuegos(Integer numeroJuegos) {
		this.numeroJuegos = numeroJuegos;
	}

	public Integer getClasifican() {
		return clasifican;
	}

	public void setClasifican(Integer clasifican) {
		this.clasifican = clasifican;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public Integer getFaseSiguiente() {
		return faseSiguiente;
	}

	public void setFaseSiguiente(Integer faseSiguiente) {
		this.faseSiguiente = faseSiguiente;
	}

}
