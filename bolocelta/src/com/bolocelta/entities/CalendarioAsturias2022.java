package com.bolocelta.entities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CalendarioAsturias2022 {

	private Integer id;
	private Date fechaDesde;
	private Date fechaHasta;
	private Integer modalidadId;
	private Integer categoriaId;
	private Integer jornada;
	private Integer jornadaVuelta;
	private String nombreCampeonato;
	private Categorias2022 categoria;
	private Modalidades2022 modalidad;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public Date getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public Integer getModalidadId() {
		return modalidadId;
	}

	public void setModalidadId(Integer modalidadId) {
		this.modalidadId = modalidadId;
	}

	public Integer getCategoriaId() {
		return categoriaId;
	}

	public void setCategoriaId(Integer categoriaId) {
		this.categoriaId = categoriaId;
	}

	public Integer getJornada() {
		return jornada;
	}

	public void setJornada(Integer jornada) {
		this.jornada = jornada;
	}
	
	public Integer getJornadaVuelta() {
		return jornadaVuelta;
	}

	public void setJornadaVuelta(Integer jornadaVuelta) {
		this.jornadaVuelta = jornadaVuelta;
	}

	public String getNombreCampeonato() {
		return nombreCampeonato;
	}

	public void setNombreCampeonato(String nombreCampeonato) {
		this.nombreCampeonato = nombreCampeonato;
	}

	public Categorias2022 getCategoria() {
		return categoria;
	}

	public void setCategoria(Categorias2022 categoria) {
		this.categoria = categoria;
	}

	public Modalidades2022 getModalidad() {
		return modalidad;
	}

	public void setModalidad(Modalidades2022 modalidad) {
		this.modalidad = modalidad;
	}
	
	public String getIdFechasJornada(){
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String fechaDesdeText = df.format(this.fechaDesde);
		String fechaHastaText = df.format(this.fechaHasta);
		return fechaDesdeText + "_" + fechaHastaText;
	}

}
