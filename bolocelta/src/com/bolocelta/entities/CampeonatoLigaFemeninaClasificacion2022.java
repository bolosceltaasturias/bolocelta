package com.bolocelta.entities;

public class CampeonatoLigaFemeninaClasificacion2022 implements Comparable<CampeonatoLigaFemeninaClasificacion2022>{

	private Integer id;
	private Integer jugadoraId;
	private Integer puntosTirada;
	private Integer puntosSacada;
	private Integer puntos;
	private Jugadores2022 jugadora;
	private Long rowNum;

	public CampeonatoLigaFemeninaClasificacion2022() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getJugadoraId() {
		return jugadoraId;
	}

	public void setJugadoraId(Integer jugadoraId) {
		this.jugadoraId = jugadoraId;
	}

	public Integer getPuntosTirada() {
		return puntosTirada;
	}

	public void setPuntosTirada(Integer puntosTirada) {
		this.puntosTirada = puntosTirada;
	}

	public Integer getPuntosSacada() {
		return puntosSacada;
	}

	public void setPuntosSacada(Integer puntosSacada) {
		this.puntosSacada = puntosSacada;
	}

	public Jugadores2022 getJugadora() {
		return jugadora;
	}

	public void setJugadora(Jugadores2022 jugadora) {
		this.jugadora = jugadora;
	}

	public Integer getPuntos() {
		return puntos;
	}

	public void setPuntos(Integer puntos) {
		this.puntos = puntos;
	}
	
	public Integer getTotalTantos(){
		return this.puntosTirada + this.puntosSacada;
	}


	@Override
	public int compareTo(CampeonatoLigaFemeninaClasificacion2022 o) {
		return (int)(o.getPuntos());
	}
	
	public Long getRowNum() {
		return rowNum;
	}

	public void setRowNum(Long rowNum) {
		this.rowNum = rowNum;
	}

}
