package com.bolocelta.entities;

import com.bolocelta.bbdd.constants.Activo2022;

public class CampeonatoJuvenilesIndividualClasificacion2022 implements Comparable<CampeonatoJuvenilesIndividualClasificacion2022>{

	private Integer id;
	private String fecha;
	private String hora;
	private Integer boleraId;
	private Integer jugadorId;
	private Integer puntosTirada1;
	private Integer puntosSacada1;
	private Integer puntosTirada2;
	private Integer puntosSacada2;
	private Integer puntosTirada3;
	private Integer puntosSacada3;
	private Integer puntosTirada4;
	private Integer puntosSacada4;
	private Integer puntosTirada5;
	private Integer puntosSacada5;
	private Integer puntosTirada6;
	private Integer puntosSacada6;
	private Integer puntosTirada7;
	private Integer puntosSacada7;
	private Integer puntosTirada8;
	private Integer puntosSacada8;
	private Integer puntosTirada9;
	private Integer puntosSacada9;
	private Integer puntosTirada10;
	private Integer puntosSacada10;
	private Integer puntosTirada11;
	private Integer puntosSacada11;
	private Integer puntosTirada12;
	private Integer puntosSacada12;
	private Integer puntosTirada13;
	private Integer puntosSacada13;
	private Integer puntosTirada14;
	private Integer puntosSacada14;
	private Integer puntosTirada15;
	private Integer puntosSacada15;
	private Integer puntosTirada16;
	private Integer puntosSacada16;
	private Integer puntosTirada17;
	private Integer puntosSacada17;
	private Integer puntosTirada18;
	private Integer puntosSacada18;
	private Integer puntosTirada19;
	private Integer puntosSacada19;
	private Integer puntosTirada20;
	private Integer puntosSacada20;
	private Jugadores2022 jugador;
	private Integer activo;
	private Long rowNum;

	public CampeonatoJuvenilesIndividualClasificacion2022() {
		this.puntosTirada1 = 0;
		this.puntosTirada2 = 0;
		this.puntosTirada3 = 0;
		this.puntosTirada4 = 0;
		this.puntosTirada5 = 0;
		this.puntosTirada6 = 0;
		this.puntosTirada7 = 0;
		this.puntosTirada8 = 0;
		this.puntosTirada9 = 0;
		this.puntosTirada10 = 0;
		this.puntosTirada11 = 0;
		this.puntosTirada12 = 0;
		this.puntosTirada13 = 0;
		this.puntosTirada14 = 0;
		this.puntosTirada15 = 0;
		this.puntosTirada16 = 0;
		this.puntosTirada17 = 0;
		this.puntosTirada18 = 0;
		this.puntosTirada19 = 0;
		this.puntosTirada20 = 0;
		this.puntosSacada1 = 0;
		this.puntosSacada2 = 0;
		this.puntosSacada3 = 0;
		this.puntosSacada4 = 0;
		this.puntosSacada5 = 0;
		this.puntosSacada6 = 0;
		this.puntosSacada7 = 0;
		this.puntosSacada8 = 0;
		this.puntosSacada9 = 0;
		this.puntosSacada10 = 0;
		this.puntosSacada11 = 0;
		this.puntosSacada12 = 0;
		this.puntosSacada13 = 0;
		this.puntosSacada14 = 0;
		this.puntosSacada15 = 0;
		this.puntosSacada16 = 0;
		this.puntosSacada17 = 0;
		this.puntosSacada18 = 0;
		this.puntosSacada19 = 0;
		this.puntosSacada20 = 0;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public Integer getBoleraId() {
		return boleraId;
	}

	public void setBoleraId(Integer boleraId) {
		this.boleraId = boleraId;
	}

	public Integer getJugadorId() {
		return jugadorId;
	}

	public void setJugadorId(Integer jugadorId) {
		this.jugadorId = jugadorId;
	}

	public Integer getPuntosTirada1() {
		return puntosTirada1;
	}

	public void setPuntosTirada1(Integer puntosTirada1) {
		this.puntosTirada1 = puntosTirada1;
	}

	public Integer getPuntosSacada1() {
		return puntosSacada1;
	}

	public void setPuntosSacada1(Integer puntosSacada1) {
		this.puntosSacada1 = puntosSacada1;
	}

	public Integer getPuntosTirada2() {
		return puntosTirada2;
	}

	public void setPuntosTirada2(Integer puntosTirada2) {
		this.puntosTirada2 = puntosTirada2;
	}

	public Integer getPuntosSacada2() {
		return puntosSacada2;
	}

	public void setPuntosSacada2(Integer puntosSacada2) {
		this.puntosSacada2 = puntosSacada2;
	}

	public Integer getPuntosTirada3() {
		return puntosTirada3;
	}

	public void setPuntosTirada3(Integer puntosTirada3) {
		this.puntosTirada3 = puntosTirada3;
	}

	public Integer getPuntosSacada3() {
		return puntosSacada3;
	}

	public void setPuntosSacada3(Integer puntosSacada3) {
		this.puntosSacada3 = puntosSacada3;
	}

	public Integer getPuntosTirada4() {
		return puntosTirada4;
	}

	public void setPuntosTirada4(Integer puntosTirada4) {
		this.puntosTirada4 = puntosTirada4;
	}

	public Integer getPuntosSacada4() {
		return puntosSacada4;
	}

	public void setPuntosSacada4(Integer puntosSacada4) {
		this.puntosSacada4 = puntosSacada4;
	}

	public Integer getPuntosTirada5() {
		return puntosTirada5;
	}

	public void setPuntosTirada5(Integer puntosTirada5) {
		this.puntosTirada5 = puntosTirada5;
	}

	public Integer getPuntosSacada5() {
		return puntosSacada5;
	}

	public void setPuntosSacada5(Integer puntosSacada5) {
		this.puntosSacada5 = puntosSacada5;
	}

	public Integer getPuntosTirada6() {
		return puntosTirada6;
	}

	public void setPuntosTirada6(Integer puntosTirada6) {
		this.puntosTirada6 = puntosTirada6;
	}

	public Integer getPuntosSacada6() {
		return puntosSacada6;
	}

	public void setPuntosSacada6(Integer puntosSacada6) {
		this.puntosSacada6 = puntosSacada6;
	}

	public Integer getPuntosTirada7() {
		return puntosTirada7;
	}

	public void setPuntosTirada7(Integer puntosTirada7) {
		this.puntosTirada7 = puntosTirada7;
	}

	public Integer getPuntosSacada7() {
		return puntosSacada7;
	}

	public void setPuntosSacada7(Integer puntosSacada7) {
		this.puntosSacada7 = puntosSacada7;
	}

	public Integer getPuntosTirada8() {
		return puntosTirada8;
	}

	public void setPuntosTirada8(Integer puntosTirada8) {
		this.puntosTirada8 = puntosTirada8;
	}

	public Integer getPuntosSacada8() {
		return puntosSacada8;
	}

	public void setPuntosSacada8(Integer puntosSacada8) {
		this.puntosSacada8 = puntosSacada8;
	}

	public Integer getPuntosTirada9() {
		return puntosTirada9;
	}

	public void setPuntosTirada9(Integer puntosTirada9) {
		this.puntosTirada9 = puntosTirada9;
	}

	public Integer getPuntosSacada9() {
		return puntosSacada9;
	}

	public void setPuntosSacada9(Integer puntosSacada9) {
		this.puntosSacada9 = puntosSacada9;
	}

	public Integer getPuntosTirada10() {
		return puntosTirada10;
	}

	public void setPuntosTirada10(Integer puntosTirada10) {
		this.puntosTirada10 = puntosTirada10;
	}

	public Integer getPuntosSacada10() {
		return puntosSacada10;
	}

	public void setPuntosSacada10(Integer puntosSacada10) {
		this.puntosSacada10 = puntosSacada10;
	}

	public Integer getPuntosTirada11() {
		return puntosTirada11;
	}

	public void setPuntosTirada11(Integer puntosTirada11) {
		this.puntosTirada11 = puntosTirada11;
	}

	public Integer getPuntosSacada11() {
		return puntosSacada11;
	}

	public void setPuntosSacada11(Integer puntosSacada11) {
		this.puntosSacada11 = puntosSacada11;
	}

	public Integer getPuntosTirada12() {
		return puntosTirada12;
	}

	public void setPuntosTirada12(Integer puntosTirada12) {
		this.puntosTirada12 = puntosTirada12;
	}

	public Integer getPuntosSacada12() {
		return puntosSacada12;
	}

	public void setPuntosSacada12(Integer puntosSacada12) {
		this.puntosSacada12 = puntosSacada12;
	}

	public Integer getPuntosTirada13() {
		return puntosTirada13;
	}

	public void setPuntosTirada13(Integer puntosTirada13) {
		this.puntosTirada13 = puntosTirada13;
	}

	public Integer getPuntosSacada13() {
		return puntosSacada13;
	}

	public void setPuntosSacada13(Integer puntosSacada13) {
		this.puntosSacada13 = puntosSacada13;
	}

	public Integer getPuntosTirada14() {
		return puntosTirada14;
	}

	public void setPuntosTirada14(Integer puntosTirada14) {
		this.puntosTirada14 = puntosTirada14;
	}

	public Integer getPuntosSacada14() {
		return puntosSacada14;
	}

	public void setPuntosSacada14(Integer puntosSacada14) {
		this.puntosSacada14 = puntosSacada14;
	}

	public Integer getPuntosTirada15() {
		return puntosTirada15;
	}

	public void setPuntosTirada15(Integer puntosTirada15) {
		this.puntosTirada15 = puntosTirada15;
	}

	public Integer getPuntosSacada15() {
		return puntosSacada15;
	}

	public void setPuntosSacada15(Integer puntosSacada15) {
		this.puntosSacada15 = puntosSacada15;
	}

	public Integer getPuntosTirada16() {
		return puntosTirada16;
	}

	public void setPuntosTirada16(Integer puntosTirada16) {
		this.puntosTirada16 = puntosTirada16;
	}

	public Integer getPuntosSacada16() {
		return puntosSacada16;
	}

	public void setPuntosSacada16(Integer puntosSacada16) {
		this.puntosSacada16 = puntosSacada16;
	}

	public Integer getPuntosTirada17() {
		return puntosTirada17;
	}

	public void setPuntosTirada17(Integer puntosTirada17) {
		this.puntosTirada17 = puntosTirada17;
	}

	public Integer getPuntosSacada17() {
		return puntosSacada17;
	}

	public void setPuntosSacada17(Integer puntosSacada17) {
		this.puntosSacada17 = puntosSacada17;
	}

	public Integer getPuntosTirada18() {
		return puntosTirada18;
	}

	public void setPuntosTirada18(Integer puntosTirada18) {
		this.puntosTirada18 = puntosTirada18;
	}

	public Integer getPuntosSacada18() {
		return puntosSacada18;
	}

	public void setPuntosSacada18(Integer puntosSacada18) {
		this.puntosSacada18 = puntosSacada18;
	}

	public Integer getPuntosTirada19() {
		return puntosTirada19;
	}

	public void setPuntosTirada19(Integer puntosTirada19) {
		this.puntosTirada19 = puntosTirada19;
	}

	public Integer getPuntosSacada19() {
		return puntosSacada19;
	}

	public void setPuntosSacada19(Integer puntosSacada19) {
		this.puntosSacada19 = puntosSacada19;
	}

	public Integer getPuntosTirada20() {
		return puntosTirada20;
	}

	public void setPuntosTirada20(Integer puntosTirada20) {
		this.puntosTirada20 = puntosTirada20;
	}

	public Integer getPuntosSacada20() {
		return puntosSacada20;
	}

	public void setPuntosSacada20(Integer puntosSacada20) {
		this.puntosSacada20 = puntosSacada20;
	}

	public Jugadores2022 getJugador() {
		return jugador;
	}

	public void setJugador(Jugadores2022 jugador) {
		this.jugador = jugador;
	}

	public Integer getActivo() {
		return activo;
	}

	public void setActivo(Integer activo) {
		this.activo = activo;
	}

	public Long getRowNum() {
		return rowNum;
	}

	public void setRowNum(Long rowNum) {
		this.rowNum = rowNum;
	}

	public Integer getTotalPuntos() {
		return this.puntosTirada1 + this.puntosTirada2 + this.puntosTirada3 + this.puntosTirada4 + this.puntosTirada5 
				+ this.puntosTirada6 + this.puntosTirada7 + this.puntosTirada8 + this.puntosTirada9 + this.puntosTirada10
				 + this.puntosTirada11 + this.puntosTirada12 + this.puntosTirada13 + this.puntosTirada14 + this.puntosTirada15
				 + this.puntosTirada16 + this.puntosTirada17 + this.puntosTirada18 + this.puntosTirada19 + this.puntosTirada20;
	}

	public Integer getTotalSacada() {
		return this.puntosSacada1 + this.puntosSacada2 + this.puntosSacada3 + this.puntosSacada4 + this.puntosSacada5 
				+ this.puntosSacada6 + this.puntosSacada7 + this.puntosSacada8 + this.puntosSacada9 + this.puntosSacada10
				 + this.puntosSacada11 + this.puntosSacada12 + this.puntosSacada13 + this.puntosSacada14 + this.puntosSacada15
				 + this.puntosSacada16 + this.puntosSacada17 + this.puntosSacada18 + this.puntosSacada19 + this.puntosSacada20;
	}

	
	public Integer getTotalPuntosFinal(){
		return this.getTotalPuntos() + this.getTotalSacada();
	}

	@Override
	public int compareTo(CampeonatoJuvenilesIndividualClasificacion2022 o) {
		return (int)(o.getTotalPuntosFinal());
	}

	public boolean isModificable(){
		if(this.activo == Activo2022.SI_NUMBER){
			return false;
		}
		return true;
	}

	public String getInsertRow(){
		return (this.id + ";" +
					this.fecha + ";" +
					this.hora + ";" +
					this.boleraId + ";" +
					this.jugadorId + ";" +
					this.puntosTirada1 + ";" +
					this.puntosSacada1 + ";" +
					this.puntosTirada2 + ";" +
					this.puntosSacada2 + ";" +
					this.puntosTirada3 + ";" +
					this.puntosSacada3 + ";" +
					this.puntosTirada4 + ";" +
					this.puntosSacada4 + ";" +
					this.puntosTirada5 + ";" +
					this.puntosSacada5 + ";" +
					this.puntosTirada6 + ";" +
					this.puntosSacada6 + ";" +
					this.puntosTirada7 + ";" +
					this.puntosSacada7 + ";" +
					this.puntosTirada8 + ";" +
					this.puntosSacada8 + ";" +
					this.puntosTirada9 + ";" +
					this.puntosSacada9 + ";" +
					this.puntosTirada10 + ";" +
					this.puntosSacada10 + ";" +
					this.puntosTirada11 + ";" +
					this.puntosSacada11 + ";" +
					this.puntosTirada12 + ";" +
					this.puntosSacada12 + ";" +
					this.puntosTirada13 + ";" +
					this.puntosSacada13 + ";" +
					this.puntosTirada14 + ";" +
					this.puntosSacada14 + ";" +
					this.puntosTirada15 + ";" +
					this.puntosSacada15 + ";" +
					this.puntosTirada16 + ";" +
					this.puntosSacada16 + ";" +
					this.puntosTirada17 + ";" +
					this.puntosSacada17 + ";" +
					this.puntosTirada18 + ";" +
					this.puntosSacada18 + ";" +
					this.puntosTirada19 + ";" +
					this.puntosSacada19 + ";" +
					this.puntosTirada20 + ";" +
					this.puntosSacada20 + ";" +
					this.activo
					);
	}

}
