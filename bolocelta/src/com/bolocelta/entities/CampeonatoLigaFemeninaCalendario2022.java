package com.bolocelta.entities;

import com.bolocelta.bbdd.constants.Activo2022;

public class CampeonatoLigaFemeninaCalendario2022 {

	private Integer id;
	private Integer jornada;
	private String fecha;
	private String hora;
	private Integer jugadoraId;
	private Integer tirada;
	private Integer sacada;
	private Integer puntos;
	private Integer activo;
	private Jugadores2022 jugadora;
	private Integer boleraId;
	private Boleras2022 bolera;
	public Integer getBoleraId() {
		return boleraId;
	}

	public void setBoleraId(Integer boleraId) {
		this.boleraId = boleraId;
	}

	public Boleras2022 getBolera() {
		return bolera;
	}

	public void setBolera(Boleras2022 bolera) {
		this.bolera = bolera;
	}

	private Long rowNum;

	public CampeonatoLigaFemeninaCalendario2022() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getJornada() {
		return jornada;
	}

	public void setJornada(Integer jornada) {
		this.jornada = jornada;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public Integer getActivo() {
		return activo;
	}

	public void setActivo(Integer activo) {
		this.activo = activo;
	}

	
	public String getFechaText(){
		return this.fecha;
	}
	
	public Integer getJugadoraId() {
		return jugadoraId;
	}

	public void setJugadoraId(Integer jugadoraId) {
		this.jugadoraId = jugadoraId;
	}

	public Integer getTirada() {
		return tirada;
	}

	public void setTirada(Integer tirada) {
		this.tirada = tirada;
	}

	public Integer getSacada() {
		return sacada;
	}

	public void setSacada(Integer sacada) {
		this.sacada = sacada;
	}

	public Integer getPuntos() {
		return puntos;
	}

	public void setPuntos(Integer puntos) {
		this.puntos = puntos;
	}

	public Jugadores2022 getJugadora() {
		return jugadora;
	}

	public void setJugadora(Jugadores2022 jugadora) {
		this.jugadora = jugadora;
	}
	
	public Integer getTotalTantos(){
		return this.tirada + this.sacada;
	}

	public boolean isModificable(){
		if(this.activo != null && this.activo == Activo2022.SI_NUMBER){
			return true;
		}
		return false;
	}
	
	public Long getRowNum() {
		return rowNum;
	}

	public void setRowNum(Long rowNum) {
		this.rowNum = rowNum;
	}
	
	

}
