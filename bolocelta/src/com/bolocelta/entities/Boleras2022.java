package com.bolocelta.entities;

public class Boleras2022 {

	private Integer id;
	private String bolera;
	private String localizacion;
	private String federada;
	private String ubicacion;

	public Boleras2022() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBolera() {
		return bolera;
	}

	public void setBolera(String bolera) {
		this.bolera = bolera;
	}

	public String getLocalizacion() {
		return localizacion;
	}

	public void setLocalizacion(String localizacion) {
		this.localizacion = localizacion;
	}

	public String getFederada() {
		return federada;
	}

	public void setFederada(String federada) {
		this.federada = federada;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

}
