package com.bolocelta.entities;

public class Categorias2022 {

	private Integer id;
	private String nombreCategoria;
	private String individual;
	private String parejas;
	private String liga;

	public Categorias2022() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombreCategoria() {
		return nombreCategoria;
	}

	public void setNombreCategoria(String nombreCategoria) {
		this.nombreCategoria = nombreCategoria;
	}

	public String getIndividual() {
		return individual;
	}

	public void setIndividual(String individual) {
		this.individual = individual;
	}

	public String getParejas() {
		return parejas;
	}

	public void setParejas(String parejas) {
		this.parejas = parejas;
	}

	public String getLiga() {
		return liga;
	}

	public void setLiga(String liga) {
		this.liga = liga;
	}
	
	@Override
	public int hashCode() {
		if(id == null){
			return 0;
		}
		return id;
	}
	
	public String getHashCode(){
		return id.toString() + "_" + nombreCategoria;
	}

}
