package com.bolocelta.entities;

import com.bolocelta.bbdd.constants.Activo2022;

public class CampeonatoLigaInfantilCalendario2022 {

	private Long rowNum;
	private Integer id;
	private Integer jornada;
	private String fecha;
	private String hora;
	private Integer ronda1;
	private Integer ronda2;
	private Integer ronda3;
	private Integer ronda4;
	private Integer puntos;
	private Integer activo;
	private Integer boleraId;
	private Boleras2022 bolera;
	private String nombre;
	private String categoria;
	private boolean contarFinal;


	public Integer getTotalTantos(){
		return this.ronda1 + this.ronda2 + this.ronda3 + this.ronda4;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getJornada() {
		return jornada;
	}

	public void setJornada(Integer jornada) {
		this.jornada = jornada;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public Integer getRonda1() {
		return ronda1;
	}

	public void setRonda1(Integer ronda1) {
		this.ronda1 = ronda1;
	}

	public Integer getRonda2() {
		return ronda2;
	}

	public void setRonda2(Integer ronda2) {
		this.ronda2 = ronda2;
	}

	public Integer getRonda3() {
		return ronda3;
	}

	public void setRonda3(Integer ronda3) {
		this.ronda3 = ronda3;
	}

	public Integer getRonda4() {
		return ronda4;
	}

	public void setRonda4(Integer ronda4) {
		this.ronda4 = ronda4;
	}

	public Integer getPuntos() {
		return puntos;
	}

	public void setPuntos(Integer puntos) {
		this.puntos = puntos;
	}

	public Integer getActivo() {
		return activo;
	}

	public void setActivo(Integer activo) {
		this.activo = activo;
	}

	public Integer getBoleraId() {
		return boleraId;
	}

	public void setBoleraId(Integer boleraId) {
		this.boleraId = boleraId;
	}

	public Boleras2022 getBolera() {
		return bolera;
	}

	public void setBolera(Boleras2022 bolera) {
		this.bolera = bolera;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public boolean isModificable(){
		if(this.activo != null && this.activo == Activo2022.SI_NUMBER){
			return true;
		}
		return false;
	}
	
	public Long getRowNum() {
		return rowNum;
	}

	public void setRowNum(Long rowNum) {
		this.rowNum = rowNum;
	}
	
	

	public boolean isContarFinal() {
		return contarFinal;
	}

	public void setContarFinal(boolean contarFinal) {
		this.contarFinal = contarFinal;
	}	

}
