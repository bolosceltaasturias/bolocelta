package com.bolocelta.entities.usuarios;

public class Permisos2022 {

	private Integer id;
	private Integer rolId;
	private Roles2022 rol;
	private String permiso;

	public Permisos2022() {
		// TODO Auto-generated constructor stub
	}

	public Permisos2022(Integer id, String permiso) {
		this.id = id;
		this.permiso = permiso;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPermiso() {
		return permiso;
	}

	public void setPermiso(String permiso) {
		this.permiso = permiso;
	}

	public Integer getRolId() {
		return rolId;
	}

	public void setRolId(Integer rolId) {
		this.rolId = rolId;
	}

	public Roles2022 getRol() {
		return rol;
	}

	public void setRol(Roles2022 rol) {
		this.rol = rol;
	}

}
