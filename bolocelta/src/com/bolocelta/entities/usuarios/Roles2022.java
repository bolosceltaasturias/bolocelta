package com.bolocelta.entities.usuarios;

public class Roles2022 {

	private Integer id;
	private String rol;
	private String admin;

	public Roles2022() {
		// TODO Auto-generated constructor stub
	}

	public Roles2022(Integer id, String rol) {
		this.id = id;
		this.rol = rol;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}

	public String getAdmin() {
		return admin;
	}

	public void setAdmin(String admin) {
		this.admin = admin;
	}

}
