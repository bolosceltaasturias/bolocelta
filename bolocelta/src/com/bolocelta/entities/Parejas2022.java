package com.bolocelta.entities;

public class Parejas2022 {

	private Integer id;
	private Integer idJugador1;
	private Jugadores2022 jugador1;
	private Integer idJugador2;
	private Jugadores2022 jugador2;

	public Parejas2022() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdJugador1() {
		return idJugador1;
	}

	public void setIdJugador1(Integer idJugador1) {
		this.idJugador1 = idJugador1;
	}
	
	public Integer getIdJugador2() {
		return idJugador2;
	}

	public void setIdJugador2(Integer idJugador2) {
		this.idJugador2 = idJugador2;
	}

	public Jugadores2022 getJugador1() {
		return jugador1;
	}

	public void setJugador1(Jugadores2022 jugador1) {
		this.jugador1 = jugador1;
	}
	
	public Jugadores2022 getJugador2() {
		return jugador2;
	}

	public void setJugador2(Jugadores2022 jugador2) {
		this.jugador2 = jugador2;
	}

	public Equipos2022 getEquipo1() {
		if(jugador1 != null && jugador1.getEquipo() != null){
			return jugador1.getEquipo();
		}
		return null;
	}
	
	public Equipos2022 getEquipo2() {
		if(jugador2 != null && jugador2.getEquipo() != null){
			return jugador2.getEquipo();
		}
		return null;
	}


	public String getNombreEquipoShow() {
		String nombreEquipo = null;
		if (this.jugador1.getApodo() != null && !this.jugador1.getApodo().isEmpty()) {
			nombreEquipo = this.jugador1.getApodo().toUpperCase();
		} else if (this.jugador1.getNombre()!= null && !this.jugador1.getNombre().isEmpty()) {
			nombreEquipo = this.jugador1.getNombre();
		}

		if (this.jugador1.getEquipo() != null && this.jugador1.getEquipo().getNombre() != null && !this.jugador1.getEquipo().getNombre().isEmpty()) {
			nombreEquipo += " (" + this.jugador1.getEquipo().getNombre() + ")";
		}
		
		nombreEquipo += " / ";
		
		if (this.jugador2.getApodo() != null && !this.jugador2.getApodo().isEmpty()) {
			nombreEquipo += this.jugador2.getApodo().toUpperCase();
		} else if (this.jugador2.getNombre()!= null && !this.jugador2.getNombre().isEmpty()) {
			nombreEquipo += this.jugador2.getNombre();
		}

		if (this.jugador2.getEquipo() != null && this.jugador2.getEquipo().getNombre() != null && !this.jugador2.getEquipo().getNombre().isEmpty()) {
			nombreEquipo += " (" + this.jugador2.getEquipo().getNombre() + ")";
		}
		
		return nombreEquipo;
	}
	
	public String getNombreEquipoShowCuadro() {
		String nombreEquipo = null;
		if (this.jugador1.getApodo() != null && !this.jugador1.getApodo().isEmpty()) {
			nombreEquipo = this.jugador1.getApodo().toUpperCase();
		} else if (this.jugador1.getNombre() != null && !this.jugador1.getNombre().isEmpty()) {
			nombreEquipo = this.jugador1.getNombre();
		}
		
		nombreEquipo += " / ";
		
		if (this.jugador2.getApodo() != null && !this.jugador2.getApodo().isEmpty()) {
			nombreEquipo += this.jugador2.getApodo().toUpperCase();
		} else if (this.jugador2.getNombre() != null && !this.jugador2.getNombre().isEmpty()) {
			nombreEquipo += this.jugador2.getNombre();
		}
		

		if (this.jugador1.getEquipo() != null && this.jugador1.getEquipo().getNombre() != null && !this.jugador1.getEquipo().getNombre().isEmpty()) {
			nombreEquipo += " # " + this.jugador1.getEquipo().getNombre() + "";
		}
		
		nombreEquipo += " / ";
		
		if (this.jugador2.getEquipo() != null && this.jugador2.getEquipo().getNombre() != null && !this.jugador2.getEquipo().getNombre().isEmpty()) {
			nombreEquipo += this.jugador2.getEquipo().getNombre() + "";
			nombreEquipo += " # ";
		}		
		
		return nombreEquipo;
	}
}
