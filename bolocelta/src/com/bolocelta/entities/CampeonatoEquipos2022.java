package com.bolocelta.entities;

import java.util.List;

public class CampeonatoEquipos2022 {

	private List<CampeonatoEquiposClasificacion2022> clasificacion;
	private List<CampeonatoEquiposCalendario2022> calendario;

	public CampeonatoEquipos2022() {
		// TODO Auto-generated constructor stub
	}

	public List<CampeonatoEquiposClasificacion2022> getClasificacion() {
		return clasificacion;
	}

	public void setClasificacion(List<CampeonatoEquiposClasificacion2022> clasificacion) {
		this.clasificacion = clasificacion;
	}

	public List<CampeonatoEquiposCalendario2022> getCalendario() {
		return calendario;
	}

	public void setCalendario(List<CampeonatoEquiposCalendario2022> calendario) {
		this.calendario = calendario;
	}



}
