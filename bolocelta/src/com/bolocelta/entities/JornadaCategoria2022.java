package com.bolocelta.entities;

public class JornadaCategoria2022 {

	private Categorias2022 categoria;
	private Integer jornada;
	private Integer jornadaVta;

	public JornadaCategoria2022() {
		// TODO Auto-generated constructor stub
	}

	public Categorias2022 getCategoria() {
		return categoria;
	}

	public void setCategoria(Categorias2022 categoria) {
		this.categoria = categoria;
	}

	public Integer getJornada() {
		return jornada;
	}

	public void setJornada(Integer jornada) {
		this.jornada = jornada;
	}
	
	public String getId(){
		return this.categoria.getId().toString() + "-" + this.jornada;
	}

	public Integer getJornadaVta() {
		return jornadaVta;
	}

	public void setJornadaVta(Integer jornadaVta) {
		this.jornadaVta = jornadaVta;
	}

}
