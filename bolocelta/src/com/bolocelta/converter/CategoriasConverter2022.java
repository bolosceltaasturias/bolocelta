package com.bolocelta.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.bolocelta.bbdd.readTables.LeerCategorias2022;
import com.bolocelta.entities.Categorias2022;
import com.bolocelta.entities.Jugadores2022;

/**
 * The Class SelectOneMenuConverter.
 */
@FacesConverter("categoriasConverter")
public class CategoriasConverter2022 implements Converter {
	
	private LeerCategorias2022 leerCategorias = new LeerCategorias2022();
 
    @Override
    public Object getAsObject(final FacesContext arg0, final UIComponent arg1, final String objectString) {
        if (objectString == null) {
            return null;
        }
 
        return fromSelect(arg1, objectString);
    }
 
    /**
     * Serialize.
     *
     * @param object
     *            the object
     * @return the string
     */
    private String serialize(final Object object) {
        if (object == null) {
            return null;
        }
        return String.valueOf(object.hashCode());
    }
 
    /**
     * From select.
     *
     * @param currentcomponent
     *            the currentcomponent
     * @param objectString
     *            the object string
     * @return the object
     */
    private Object fromSelect(final UIComponent currentcomponent, final String objectString) {
    	
    	
    	if(leerCategorias.listResult() != null){
    		for (Object object : leerCategorias.listResult()) {
    			Categorias2022 categoria = (Categorias2022) object;
    			String serialize = serialize(categoria);
    			
    			if(serialize.equals(objectString)){
    				return categoria;
    			}
				
			}
    	}
    	
        return null;
    }
 
    @Override
    public String getAsString(final FacesContext arg0, final UIComponent arg1, final Object object) {
        return serialize(object);
    }
 
}