package com.bolocelta.facade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.inject.Named;

import com.bolocelta.application.enumerations.CategoriasEnumeration2022;
import com.bolocelta.application.session.SessionState;
import com.bolocelta.bbdd.constants.Activo2022;
import com.bolocelta.bbdd.constants.Categorias2022;
import com.bolocelta.bbdd.readTables.LeerEquipos2022;
import com.bolocelta.bbdd.readTables.LeerPremiosMVP2022;
import com.bolocelta.bbdd.readTables.LeerVotacionesMVP2022;
import com.bolocelta.entities.Equipos2022;
import com.bolocelta.entities.PremiosMVP2022;
import com.bolocelta.entities.VotacionesMVP2022;

@Named
@ConversationScoped
@ManagedBean
public class PremiosMVPFacade2022 implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private SessionState sessionState;
	
	private LeerPremiosMVP2022 leerPremiosMVP = new LeerPremiosMVP2022();
	private LeerVotacionesMVP2022 leerVotacionesMVP = new LeerVotacionesMVP2022();
	
	private LeerEquipos2022 leerEquipos = new LeerEquipos2022();
	
	//private CrearCampeonatoLigaInfantil campeonatoLigaInfantil = new CrearCampeonatoLigaInfantil();
	
	private List<PremiosMVP2022> resultListPremiosPrimera = null;
	private List<PremiosMVP2022> resultListPremiosSegunda = null;
	private List<PremiosMVP2022> resultListPremiosTercera = null;
	private List<PremiosMVP2022> resultListPremiosJuvenil = null;
	private List<PremiosMVP2022> resultListPremiosCadete = null;
	private List<PremiosMVP2022> resultListPremiosVeterano = null;
	private List<PremiosMVP2022> resultListPremiosFemenino = null;
	
	
	private List<VotacionesMVP2022> resultListVotacionesPrimera = null;
	private List<VotacionesMVP2022> resultListVotacionesSegunda = null;
	private List<VotacionesMVP2022> resultListVotacionesTercera = null;
	private List<VotacionesMVP2022> resultListVotacionesJuvenil = null;
	private List<VotacionesMVP2022> resultListVotacionesCadete = null;
	private List<VotacionesMVP2022> resultListVotacionesVeterano = null;
	private List<VotacionesMVP2022> resultListVotacionesFemenino = null;
	
	private List<Equipos2022> resultListEquiposVotaciones = null;

	public List<PremiosMVP2022> getResultListPremiosPrimera() {
		if(resultListPremiosPrimera == null){
			resultListPremiosPrimera = (List<PremiosMVP2022>) leerPremiosMVP.listResultPremios(Categorias2022.PRIMERA);
		}
		
		//Ordenar la clasificacion
		Collections.sort(resultListPremiosPrimera, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				PremiosMVP2022 cec1 = (PremiosMVP2022) o1;
				PremiosMVP2022 cec2 = (PremiosMVP2022) o2;
				
				int rpuntos = cec1.getTotalPuntos().compareTo(cec2.getTotalPuntos());
				if (rpuntos == 0) {
					String nomb1 = cec1.getJugador().getApodo();
					String nomb2 = cec2.getJugador().getApodo();
					int rdifnomb = nomb2.compareTo(nomb1);
					return rdifnomb;
				}
				return rpuntos;
			}
		});
		
		Collections.reverse(resultListPremiosPrimera);
		
		return resultListPremiosPrimera;
	}
	
	public List<PremiosMVP2022> getResultListPremiosSegunda() {
		if(resultListPremiosSegunda == null){
			resultListPremiosSegunda = (List<PremiosMVP2022>) leerPremiosMVP.listResultPremios(Categorias2022.SEGUNDA);
		}
		
		//Ordenar la clasificacion
		Collections.sort(resultListPremiosSegunda, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				PremiosMVP2022 cec1 = (PremiosMVP2022) o1;
				PremiosMVP2022 cec2 = (PremiosMVP2022) o2;
				
				int rpuntos = cec1.getTotalPuntos().compareTo(cec2.getTotalPuntos());
				if (rpuntos == 0) {
					String nomb1 = cec1.getJugador().getApodo();
					String nomb2 = cec2.getJugador().getApodo();
					int rdifnomb = nomb2.compareTo(nomb1);
					return rdifnomb;
				}
				return rpuntos;
			}
		});
		
		Collections.reverse(resultListPremiosSegunda);
		
		return resultListPremiosSegunda;
	}
	
	public List<PremiosMVP2022> getResultListPremiosTercera() {
		if(resultListPremiosTercera == null){
			resultListPremiosTercera = (List<PremiosMVP2022>) leerPremiosMVP.listResultPremios(Categorias2022.TERCERA);
		}
		
		//Ordenar la clasificacion
		Collections.sort(resultListPremiosTercera, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				PremiosMVP2022 cec1 = (PremiosMVP2022) o1;
				PremiosMVP2022 cec2 = (PremiosMVP2022) o2;
				
				int rpuntos = cec1.getTotalPuntos().compareTo(cec2.getTotalPuntos());
				if (rpuntos == 0) {
					String nomb1 = cec1.getJugador().getApodo();
					String nomb2 = cec2.getJugador().getApodo();
					int rdifnomb = nomb2.compareTo(nomb1);
					return rdifnomb;
				}
				return rpuntos;
			}
		});
		
		Collections.reverse(resultListPremiosTercera);
		
		return resultListPremiosTercera;
	}
	
	public List<PremiosMVP2022> getResultListPremiosFemenino() {
		if(resultListPremiosFemenino == null){
			resultListPremiosFemenino = (List<PremiosMVP2022>) leerPremiosMVP.listResultPremios(Categorias2022.FEMENINO);
		}
		
		//Ordenar la clasificacion
		Collections.sort(resultListPremiosFemenino, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				PremiosMVP2022 cec1 = (PremiosMVP2022) o1;
				PremiosMVP2022 cec2 = (PremiosMVP2022) o2;
				
				int rpuntos = cec1.getTotalPuntos().compareTo(cec2.getTotalPuntos());
				if (rpuntos == 0) {
					String nomb1 = cec1.getJugador().getApodo();
					String nomb2 = cec2.getJugador().getApodo();
					int rdifnomb = nomb2.compareTo(nomb1);
					return rdifnomb;
				}
				return rpuntos;
			}
		});

		Collections.reverse(resultListPremiosFemenino);
		
		return resultListPremiosFemenino;
	}
	
	public List<PremiosMVP2022> getResultListPremiosJuvenil() {
		if(resultListPremiosJuvenil == null){
			resultListPremiosJuvenil = new ArrayList<PremiosMVP2022>();
			for (PremiosMVP2022 premiosMVP : getResultListPremiosPrimera()) {
				if(premiosMVP.getEsJuvenil().equalsIgnoreCase(Activo2022.SI)){
					resultListPremiosJuvenil.add(premiosMVP);
				}
			}
			for (PremiosMVP2022 premiosMVP : getResultListPremiosSegunda()) {
				if(premiosMVP.getEsJuvenil().equalsIgnoreCase(Activo2022.SI)){
					resultListPremiosJuvenil.add(premiosMVP);
				}
			}
			for (PremiosMVP2022 premiosMVP : getResultListPremiosTercera()) {
				if(premiosMVP.getEsJuvenil().equalsIgnoreCase(Activo2022.SI)){
					resultListPremiosJuvenil.add(premiosMVP);
				}
			}
		}
		
		//Ordenar la clasificacion
		Collections.sort(resultListPremiosJuvenil, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				PremiosMVP2022 cec1 = (PremiosMVP2022) o1;
				PremiosMVP2022 cec2 = (PremiosMVP2022) o2;
				
				int rpuntos = cec1.getTotalPuntos().compareTo(cec2.getTotalPuntos());
				if (rpuntos == 0) {
					String nomb1 = cec1.getJugador().getApodo();
					String nomb2 = cec2.getJugador().getApodo();
					int rdifnomb = nomb2.compareTo(nomb1);
					return rdifnomb;
				}
				return rpuntos;
			}
		});
		
		Collections.reverse(resultListPremiosJuvenil);
		
		return resultListPremiosJuvenil;
	}
	
	public List<PremiosMVP2022> getResultListPremiosCadete() {
		if(resultListPremiosCadete == null){
			resultListPremiosCadete = new ArrayList<PremiosMVP2022>();
			for (PremiosMVP2022 premiosMVP : getResultListPremiosPrimera()) {
				if(premiosMVP.getEsCadete().equalsIgnoreCase(Activo2022.SI)){
					resultListPremiosCadete.add(premiosMVP);
				}
			}
			for (PremiosMVP2022 premiosMVP : getResultListPremiosSegunda()) {
				if(premiosMVP.getEsCadete().equalsIgnoreCase(Activo2022.SI)){
					resultListPremiosCadete.add(premiosMVP);
				}
			}
			for (PremiosMVP2022 premiosMVP : getResultListPremiosTercera()) {
				if(premiosMVP.getEsCadete().equalsIgnoreCase(Activo2022.SI)){
					resultListPremiosCadete.add(premiosMVP);
				}
			}
		}
		
		//Ordenar la clasificacion
		Collections.sort(resultListPremiosCadete, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				PremiosMVP2022 cec1 = (PremiosMVP2022) o1;
				PremiosMVP2022 cec2 = (PremiosMVP2022) o2;
				
				int rpuntos = cec1.getTotalPuntos().compareTo(cec2.getTotalPuntos());
				if (rpuntos == 0) {
					String nomb1 = cec1.getJugador().getApodo();
					String nomb2 = cec2.getJugador().getApodo();
					int rdifnomb = nomb2.compareTo(nomb1);
					return rdifnomb;
				}
				return rpuntos;
			}
		});
		
		Collections.reverse(resultListPremiosCadete);
		
		return resultListPremiosCadete;
	}
	
	public List<PremiosMVP2022> getResultListPremiosVeterano() {
		if(resultListPremiosVeterano == null){
			resultListPremiosVeterano = new ArrayList<PremiosMVP2022>();
			for (PremiosMVP2022 premiosMVP : getResultListPremiosPrimera()) {
				if(premiosMVP.getEsVeterano().equalsIgnoreCase(Activo2022.SI)){
					resultListPremiosVeterano.add(premiosMVP);
				}
			}
			for (PremiosMVP2022 premiosMVP : getResultListPremiosSegunda()) {
				if(premiosMVP.getEsVeterano().equalsIgnoreCase(Activo2022.SI)){
					resultListPremiosVeterano.add(premiosMVP);
				}
			}
			for (PremiosMVP2022 premiosMVP : getResultListPremiosTercera()) {
				if(premiosMVP.getEsVeterano().equalsIgnoreCase(Activo2022.SI)){
					resultListPremiosVeterano.add(premiosMVP);
				}
			}
		}
		
		//Ordenar la clasificacion
		Collections.sort(resultListPremiosVeterano, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				PremiosMVP2022 cec1 = (PremiosMVP2022) o1;
				PremiosMVP2022 cec2 = (PremiosMVP2022) o2;
				
				int rpuntos = cec1.getTotalPuntos().compareTo(cec2.getTotalPuntos());
				if (rpuntos == 0) {
					String nomb1 = cec1.getJugador().getApodo();
					String nomb2 = cec2.getJugador().getApodo();
					int rdifnomb = nomb2.compareTo(nomb1);
					return rdifnomb;
				}
				return rpuntos;
			}
		});
		
		Collections.reverse(resultListPremiosVeterano);
		
		return resultListPremiosVeterano;
	}

	public Object selected() {
		// TODO Auto-generated method stub
		return null;
	}

	public Object read(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	public String insert(Object insert) {
		// TODO Auto-generated method stub
		return null;
	}

	public String update(Object update) {
		// TODO Auto-generated method stub
		return null;
	}

	public String delete(Object delete) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Integer getRowsPaginatorClasificacion(){
		return 10;
	}
	
	public List<Equipos2022> getResultListEquiposVotaciones() {
		if(resultListEquiposVotaciones == null){
			resultListEquiposVotaciones = (List<Equipos2022>) leerEquipos.listResultVotaciones();
		}
			
		return resultListEquiposVotaciones;
	}
	
	public List<VotacionesMVP2022> getResultListVotacionesPrimera() {
		if(resultListVotacionesPrimera == null){
			resultListVotacionesPrimera = (List<VotacionesMVP2022>) leerVotacionesMVP.listResultVotaciones(Categorias2022.PRIMERA);
		}
		
		//Ordenar la clasificacion
		Collections.sort(resultListVotacionesPrimera, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				VotacionesMVP2022 cec1 = (VotacionesMVP2022) o1;
				VotacionesMVP2022 cec2 = (VotacionesMVP2022) o2;
				
				int rpuntos = cec1.getTotalPuntos().compareTo(cec2.getTotalPuntos());
				if (rpuntos == 0) {
					String nomb1 = cec1.getJugador().getApodo();
					String nomb2 = cec2.getJugador().getApodo();
					int rdifnomb = nomb2.compareTo(nomb1);
					return rdifnomb;
				}
				return rpuntos;
			}
		});
		
		Collections.reverse(resultListVotacionesPrimera);
		
		return resultListVotacionesPrimera;
	}
	
	public List<VotacionesMVP2022> getResultListVotacionesSegunda() {
		if(resultListVotacionesSegunda == null){
			resultListVotacionesSegunda = (List<VotacionesMVP2022>) leerVotacionesMVP.listResultVotaciones(Categorias2022.SEGUNDA);
		}
		
		//Ordenar la clasificacion
		Collections.sort(resultListVotacionesSegunda, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				VotacionesMVP2022 cec1 = (VotacionesMVP2022) o1;
				VotacionesMVP2022 cec2 = (VotacionesMVP2022) o2;
				
				int rpuntos = cec1.getTotalPuntos().compareTo(cec2.getTotalPuntos());
				if (rpuntos == 0) {
					String nomb1 = cec1.getJugador().getApodo();
					String nomb2 = cec2.getJugador().getApodo();
					int rdifnomb = nomb2.compareTo(nomb1);
					return rdifnomb;
				}
				return rpuntos;
			}
		});
		
		Collections.reverse(resultListVotacionesSegunda);
		
		return resultListVotacionesSegunda;
	}
	
	public List<VotacionesMVP2022> getResultListVotacionesTercera() {
		if(resultListVotacionesTercera == null){
			resultListVotacionesTercera = (List<VotacionesMVP2022>) leerVotacionesMVP.listResultVotaciones(Categorias2022.TERCERA);
		}
		
		//Ordenar la clasificacion
		Collections.sort(resultListVotacionesTercera, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				VotacionesMVP2022 cec1 = (VotacionesMVP2022) o1;
				VotacionesMVP2022 cec2 = (VotacionesMVP2022) o2;
				
				int rpuntos = cec1.getTotalPuntos().compareTo(cec2.getTotalPuntos());
				if (rpuntos == 0) {
					String nomb1 = cec1.getJugador().getApodo();
					String nomb2 = cec2.getJugador().getApodo();
					int rdifnomb = nomb2.compareTo(nomb1);
					return rdifnomb;
				}
				return rpuntos;
			}
		});
		
		Collections.reverse(resultListVotacionesTercera);
		
		return resultListVotacionesTercera;
	}
	
	public List<VotacionesMVP2022> getResultListVotacionesVeterano() {
		if(resultListVotacionesVeterano == null){
			resultListVotacionesVeterano = (List<VotacionesMVP2022>) leerVotacionesMVP.listResultVotaciones(Categorias2022.VETERANOS);
		}
		
		//Ordenar la clasificacion
		Collections.sort(resultListVotacionesVeterano, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				VotacionesMVP2022 cec1 = (VotacionesMVP2022) o1;
				VotacionesMVP2022 cec2 = (VotacionesMVP2022) o2;
				
				int rpuntos = cec1.getTotalPuntos().compareTo(cec2.getTotalPuntos());
				if (rpuntos == 0) {
					String nomb1 = cec1.getJugador().getApodo();
					String nomb2 = cec2.getJugador().getApodo();
					int rdifnomb = nomb2.compareTo(nomb1);
					return rdifnomb;
				}
				return rpuntos;
			}
		});
		
		Collections.reverse(resultListVotacionesVeterano);
		
		return resultListVotacionesVeterano;
	}
	
	public List<VotacionesMVP2022> getResultListVotacionesJuvenil() {
		if(resultListVotacionesJuvenil == null){
			resultListVotacionesJuvenil = (List<VotacionesMVP2022>) leerVotacionesMVP.listResultVotaciones(Categorias2022.JUVENILES);
		}
		
		//Ordenar la clasificacion
		Collections.sort(resultListVotacionesJuvenil, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				VotacionesMVP2022 cec1 = (VotacionesMVP2022) o1;
				VotacionesMVP2022 cec2 = (VotacionesMVP2022) o2;
				
				int rpuntos = cec1.getTotalPuntos().compareTo(cec2.getTotalPuntos());
				if (rpuntos == 0) {
					String nomb1 = cec1.getJugador().getApodo();
					String nomb2 = cec2.getJugador().getApodo();
					int rdifnomb = nomb2.compareTo(nomb1);
					return rdifnomb;
				}
				return rpuntos;
			}
		});
		
		Collections.reverse(resultListVotacionesJuvenil);
		
		return resultListVotacionesJuvenil;
	}
	
	public List<VotacionesMVP2022> getResultListVotacionesCadete() {
		if(resultListVotacionesCadete == null){
			resultListVotacionesCadete = (List<VotacionesMVP2022>) leerVotacionesMVP.listResultVotaciones(Categorias2022.CADETES);
		}
		
		//Ordenar la clasificacion
		Collections.sort(resultListVotacionesCadete, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				VotacionesMVP2022 cec1 = (VotacionesMVP2022) o1;
				VotacionesMVP2022 cec2 = (VotacionesMVP2022) o2;
				
				int rpuntos = cec1.getTotalPuntos().compareTo(cec2.getTotalPuntos());
				if (rpuntos == 0) {
					String nomb1 = cec1.getJugador().getApodo();
					String nomb2 = cec2.getJugador().getApodo();
					int rdifnomb = nomb2.compareTo(nomb1);
					return rdifnomb;
				}
				return rpuntos;
			}
		});
		
		Collections.reverse(resultListVotacionesCadete);
		
		return resultListVotacionesCadete;
	}
	
	public List<VotacionesMVP2022> getResultListVotacionesFemenino() {
		if(resultListVotacionesFemenino == null){
			resultListVotacionesFemenino = (List<VotacionesMVP2022>) leerVotacionesMVP.listResultVotaciones(Categorias2022.FEMENINO);
		}
		
		//Ordenar la clasificacion
		Collections.sort(resultListVotacionesFemenino, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				VotacionesMVP2022 cec1 = (VotacionesMVP2022) o1;
				VotacionesMVP2022 cec2 = (VotacionesMVP2022) o2;
				
				int rpuntos = cec1.getTotalPuntos().compareTo(cec2.getTotalPuntos());
				if (rpuntos == 0) {
					String nomb1 = cec1.getJugador().getApodo();
					String nomb2 = cec2.getJugador().getApodo();
					int rdifnomb = nomb2.compareTo(nomb1);
					return rdifnomb;
				}
				return rpuntos;
			}
		});
		
		Collections.reverse(resultListVotacionesFemenino);
		
		return resultListVotacionesFemenino;
	}
	
	public Integer buscarValorVotacionEquipo(Integer categoria, Integer equipo, Integer idJugador){
		if(categoria.equals(Categorias2022.FEMENINO)){
			for (VotacionesMVP2022 votacionesMVP : getResultListVotacionesFemenino()) {
				if(votacionesMVP.getIdJugador().equals(idJugador)){
					return votacionesMVP.getEquipoPuntos().get(equipo);
				}
			}
		}
		if(categoria.equals(Categorias2022.JUVENILES)){
			for (VotacionesMVP2022 votacionesMVP : getResultListVotacionesJuvenil()) {
				if(votacionesMVP.getIdJugador().equals(idJugador)){
					return votacionesMVP.getEquipoPuntos().get(equipo);
				}
			}
		}
		if(categoria.equals(Categorias2022.CADETES)){
			for (VotacionesMVP2022 votacionesMVP : getResultListVotacionesCadete()) {
				if(votacionesMVP.getIdJugador().equals(idJugador)){
					return votacionesMVP.getEquipoPuntos().get(equipo);
				}
			}
		}
		if(categoria.equals(Categorias2022.VETERANOS)){
			for (VotacionesMVP2022 votacionesMVP : getResultListVotacionesVeterano()) {
				if(votacionesMVP.getIdJugador().equals(idJugador)){
					return votacionesMVP.getEquipoPuntos().get(equipo);
				}
			}
		}
		if(categoria.equals(Categorias2022.TERCERA)){
			for (VotacionesMVP2022 votacionesMVP : getResultListVotacionesTercera()) {
				if(votacionesMVP.getIdJugador().equals(idJugador)){
					return votacionesMVP.getEquipoPuntos().get(equipo);
				}
			}
		}
		if(categoria.equals(Categorias2022.SEGUNDA)){
			for (VotacionesMVP2022 votacionesMVP : getResultListVotacionesSegunda()) {
				if(votacionesMVP.getIdJugador().equals(idJugador)){
					return votacionesMVP.getEquipoPuntos().get(equipo);
				}
			}
		}
		if(categoria.equals(Categorias2022.PRIMERA)){
			for (VotacionesMVP2022 votacionesMVP : getResultListVotacionesPrimera()) {
				if(votacionesMVP.getIdJugador().equals(idJugador)){
					return votacionesMVP.getEquipoPuntos().get(equipo);
				}
			}
		}
		return 0;
	}
	
}
