package com.bolocelta.facade;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.inject.Named;

@Named
@ManagedBean
public class HistoriaFacade2022 implements Serializable {

	private static final long serialVersionUID = 1L;

	public String titleHistoria = "La Historia";
	public String titleBolera = "La Bolera";
	public String titlePuntuacion = "Puntuacion";
	public String titleBolosBolas = "Bolos y Bolas";

	public String parrafoHistoria1 = "Los griegos, los romanos y los egipcios ya jugaban con bolos aunque, respecto a nuestros d�as, jugaban de manera diferente. En Asturias, se tienen datos de esta modalidad desde el siglo IX (a�o 1070). Cuentan que los moros, cuando escaparon de Asturias, dejaron los bolos y las bolas de oro.";
	public String parrafoHistoria2 = "Las leyendas mencionan que el 'trasgu' cuando se cuela por nuestras casas haciendo ruido es que est�n jugando a los bolos.";
	public String parrafoHistoria3 = "Si nos acercamos al siglo pasado, sabemos que los bolos de Tineo han sido siempre el deporte aut�ctono de esta zona. Su pr�ctica y afici�n siempre estuvieron unidas a la vida cotidiana de los habitantes de Tineo. Las boleras se ubicaban, en su mayor�a, cerca de las iglesias, aprovechando la salida de misa para jugar una partida entre los amigos.";
	public String parrafoHistoria4 = "Con la emigraci�n, muchos fueron los tinetenses que se llevaron con ellos esta modalidad, no s�lo dentro de Asturias y del resto de Espa�a, sino tambi�n a Latinoam�rica: Venezuela, M�xico, Argentina...";
	public String parrafoHistoria5 = "Hoy en d�a, el juego de bolos de Tineo ha progresado, no s�lo en cuanto a la mejora de las instalaciones, sino que adem�s en la actualidad formamos parte de la Federaci�n Asturiana de Bolos, por lo que ha dejado de ser una modalidad de unos cuantos amigos para convertirse en un deporte de alta competici�n.";
	public String parrafoHistoria6 = "Y gracias a toda esta historia: 'De las modalidades de bolos existentes en Asturias, la tinetense es una de las m�s espectaculares y que m�s sorprende al que los contempla por primera vez'.";

	public String parrafoBolera1 = "En esta modalidad, destaca sobre todo su abultado n�mero de bolos, record no s�lo en las modalidades y en los juegos de Espa�a, sino tambi�n de Europa.";
	public String parrafoBolera2 = "Actualmente se practica en un espacio rectangular de 30 a 35 metros de largo por 10 de ancho. Dentro de este espacio se sit�an los siguientes elementos: la losa o losera, a unos cuantos metros del poyo, con una suave inclinaci�n hacia la cueva, lugar donde se sit�an los bolos. Las dimensiones de la losa var�an entre los 80 y los 130 cm. de largo por 50 � 110 cm. de ancho. Se colocaba una piedra lisa, normalmente de r�o o cantera. La condici�n indispensable era que fuese lo m�s uniforme posible. Sin embargo, en el pasado las dimensiones se ajustaban a las condiciones del campo, que generalmente se encontraba al lado de edificaciones religiosas.";
	public String parrafoBolera3 = "El poyo mide unos 40 cm. de alto. Es el lugar de tiro de la bola, que antiguamente tambi�n se trataba de una piedra, pero esta vez de mayor tama�o y no necesariamente regular. Se colocaban dos poyos, uno para los zurdos y otro para los diestros. Delante del poyo se abre una zanja de aproximadamente medio metro que se llama cueva y es el lugar donde caen los tiradores de tal forma que el brazo ejecutor quede a una altura similar a la de la l�nea de bolos.";
	public String parrafoBolera4 = "A unos 25 metros por delante de la losera o losa se marca en el suelo una l�nea, la raya del 10, bien mediante una l�nea pintada en blanco o seg�ndola m�s profundamente. Desde esta l�nea y a otros 4 metros, se coloca un muro de 6 metros de alto llamado la viga o cuerda, considerado el l�mite de la bolera, aunque como veremos en la forma de puntuaci�n, esta viga tiene mucha importancia y en varias ocasiones se ha de rodear para recoger los bolos ca�dos.";
	public String parrafoBolera5 = "En esta modalidad, destaca sobre todo su abultado n�mero de bolos, record no s�lo en las modalidades y en los juegos de Espa�a, sino tambi�n de Europa.";

	public String parrafoPuntuacion1 = "La partida se juega a cuatro juegos de 50 puntos cada uno, y en ella se hacen dos tipos de tirada. Se puede jugar uno contra uno, por parejas o por equipos de cuatro jugadores.";
	public String parrafoPuntuacion2 = "La primera tirada se llama bajar y se hace desde lo alto del poyo al tiempo que el jugador se lanza dentro de la cueva. Cada bolo tirado vale un punto, salvo los que pasan de la raya del 10 que valen 10 puntos cada uno, y los que saltan la viga, que valen 50 puntos, y que, por lo tanto, conceden la victoria al participante. A estos bolos se les llama acabones, porque con ellos finaliza el juego. Es fundamental para poder contar estos puntos que la bola pase de la ralla del 10 o si no la tirada es nula y se denomina que la Bola queix�.";
	public String parrafoPuntuacion3 = "La otra tirada se llama subir, y se hace desde la viga lanzando la bola rodando por el suelo contra los bolos desde la l�nea de 10, valiendo un punto cada bolo ca�do. Si el jugador no tira ning�n bolo, pierde la mano u orden de tirada, lo cual es muy importante en este juego, ya que se trata de ser el primero en llegar a obtener los 50 puntos. Por eso, antes de empezar, se sortea el orden de tirada que luego puede cambiar.";

	public String parrafoBolosBolas1 = "Los bolos con los que se juegan son 20, pese a que antiguamente se jugaba con un n�mero variable que depend�a de las dimensiones de la losa.";
	public String parrafoBolosBolas2 = "Los bolos eran hechos de madera de haya o manzano, salvo en el caso de competiciones federadas que siempre son de encina, y tienen entre 15 y 18 cent�metros de altura, 3 cent�metros de di�metro y una circunferencia de 12 cent�metros. Se tallan con un hacha, d�ndoles 6 lados, y la base cortada a bisela o un poco inclinada para que se sostengan bien sobre la losera. Su peso es de 200 gramos aproximadamente.";
	public String parrafoBolosBolas3 = "Las bolas estaban hechas de maderas muy duras, como el haya, encina o quebracho (�rbol argentino tra�do por los espa�oles emigrantes), y generalmente torneadas. Su peso y di�metro var�a en funci�n de la mano del jugador que la vaya a usar. La necesidad de que tengan un peso alto obliga a practicarles un agujero, en el cual se echa plomo fundido, con lo que se lastra y el peso de la bola aumenta. En la actualidad esta operaci�n ya casi no se realiza, pues se hacen de fibra de pl�stico con plomo en el interior. En las competiciones federadas se proh�be el uso de bolas de mayor di�metro que 35 cm.";

	public String getTitleHistoria() {
		return titleHistoria;
	}

	public void setTitleHistoria(String titleHistoria) {
		this.titleHistoria = titleHistoria;
	}

	public String getTitleBolera() {
		return titleBolera;
	}

	public void setTitleBolera(String titleBolera) {
		this.titleBolera = titleBolera;
	}

	public String getTitlePuntuacion() {
		return titlePuntuacion;
	}

	public void setTitlePuntuacion(String titlePuntuacion) {
		this.titlePuntuacion = titlePuntuacion;
	}

	public String getTitleBolosBolas() {
		return titleBolosBolas;
	}

	public void setTitleBolosBolas(String titleBolosBolas) {
		this.titleBolosBolas = titleBolosBolas;
	}

	public String getParrafoHistoria1() {
		return parrafoHistoria1;
	}

	public void setParrafoHistoria1(String parrafoHistoria1) {
		this.parrafoHistoria1 = parrafoHistoria1;
	}

	public String getParrafoHistoria2() {
		return parrafoHistoria2;
	}

	public void setParrafoHistoria2(String parrafoHistoria2) {
		this.parrafoHistoria2 = parrafoHistoria2;
	}

	public String getParrafoHistoria3() {
		return parrafoHistoria3;
	}

	public void setParrafoHistoria3(String parrafoHistoria3) {
		this.parrafoHistoria3 = parrafoHistoria3;
	}

	public String getParrafoHistoria4() {
		return parrafoHistoria4;
	}

	public void setParrafoHistoria4(String parrafoHistoria4) {
		this.parrafoHistoria4 = parrafoHistoria4;
	}

	public String getParrafoHistoria5() {
		return parrafoHistoria5;
	}

	public void setParrafoHistoria5(String parrafoHistoria5) {
		this.parrafoHistoria5 = parrafoHistoria5;
	}

	public String getParrafoHistoria6() {
		return parrafoHistoria6;
	}

	public void setParrafoHistoria6(String parrafoHistoria6) {
		this.parrafoHistoria6 = parrafoHistoria6;
	}

	public String getParrafoBolera1() {
		return parrafoBolera1;
	}

	public void setParrafoBolera1(String parrafoBolera1) {
		this.parrafoBolera1 = parrafoBolera1;
	}

	public String getParrafoBolera2() {
		return parrafoBolera2;
	}

	public void setParrafoBolera2(String parrafoBolera2) {
		this.parrafoBolera2 = parrafoBolera2;
	}

	public String getParrafoBolera3() {
		return parrafoBolera3;
	}

	public void setParrafoBolera3(String parrafoBolera3) {
		this.parrafoBolera3 = parrafoBolera3;
	}

	public String getParrafoBolera4() {
		return parrafoBolera4;
	}

	public void setParrafoBolera4(String parrafoBolera4) {
		this.parrafoBolera4 = parrafoBolera4;
	}

	public String getParrafoBolera5() {
		return parrafoBolera5;
	}

	public void setParrafoBolera5(String parrafoBolera5) {
		this.parrafoBolera5 = parrafoBolera5;
	}

	public String getParrafoPuntuacion1() {
		return parrafoPuntuacion1;
	}

	public void setParrafoPuntuacion1(String parrafoPuntuacion1) {
		this.parrafoPuntuacion1 = parrafoPuntuacion1;
	}

	public String getParrafoPuntuacion2() {
		return parrafoPuntuacion2;
	}

	public void setParrafoPuntuacion2(String parrafoPuntuacion2) {
		this.parrafoPuntuacion2 = parrafoPuntuacion2;
	}

	public String getParrafoPuntuacion3() {
		return parrafoPuntuacion3;
	}

	public void setParrafoPuntuacion3(String parrafoPuntuacion3) {
		this.parrafoPuntuacion3 = parrafoPuntuacion3;
	}

	public String getParrafoBolosBolas1() {
		return parrafoBolosBolas1;
	}

	public void setParrafoBolosBolas1(String parrafoBolosBolas1) {
		this.parrafoBolosBolas1 = parrafoBolosBolas1;
	}

	public String getParrafoBolosBolas2() {
		return parrafoBolosBolas2;
	}

	public void setParrafoBolosBolas2(String parrafoBolosBolas2) {
		this.parrafoBolosBolas2 = parrafoBolosBolas2;
	}

	public String getParrafoBolosBolas3() {
		return parrafoBolosBolas3;
	}

	public void setParrafoBolosBolas3(String parrafoBolosBolas3) {
		this.parrafoBolosBolas3 = parrafoBolosBolas3;
	}
}
