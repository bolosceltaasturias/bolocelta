package com.bolocelta.facade.edit;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.bolocelta.application.Redirect;
import com.bolocelta.application.session.SessionState;
import com.bolocelta.bbdd.constants.Activo2022;
import com.bolocelta.bbdd.createTable.CrearJugadores2022;
import com.bolocelta.bbdd.readTables.LeerJugadores2022;
import com.bolocelta.entities.Jugadores2022;

@Named
@ManagedBean
@SessionScoped
public class FichaJugadorFacade2022 implements Serializable {

	private static final String GUION = "-";

	private static final long serialVersionUID = 1L;

	@Inject
	private Redirect redirect;

	@Inject
	private SessionState sessionState;
	
	@Inject
	private FichaEquipoFacade2022 fichaEquipoFacade;

	private LeerJugadores2022 leerJugadores = new LeerJugadores2022();
	private CrearJugadores2022 crearJugadores = new CrearJugadores2022();

	private Jugadores2022 jugador = null;
	
	public boolean isNew(){
		if(this.jugador == null || (this.jugador != null && this.jugador.getRowNum() == null)){
			return true;
		}
		return false;
	}

	public void doSelected(Integer id) {
		this.jugador = (Jugadores2022) leerJugadores.read(id);
		redirect.getJugadorDetalle();
	}
	
	public void doCancel() {
		this.jugador = null;
		redirect.getMiEquipo();
	}
	
	public void doInsert() {
		if(crearJugadores.validate(this.jugador)){
			Long newRowNum = leerJugadores.getLastRowSheet();
			this.jugador.setRowNum(newRowNum+2);
			crearJugadores.preparateInsertRow(this.jugador);
			leerJugadores.refreshList();
			fichaEquipoFacade.doSelectedMyTeam();
		}
	}
	
	public void doSave() {
		if(crearJugadores.validate(this.jugador)){
			crearJugadores.preparateUpdateRow(this.jugador);
			fichaEquipoFacade.doSelectedMyTeam();
		}
	}
	
	public void doDelete() {
		this.jugador.setActivo(Activo2022.NO);
		crearJugadores.preparateDeleteLogicRow(this.jugador);
		fichaEquipoFacade.doSelectedMyTeam();
	}
	
	public void doCreate(){
		this.jugador = new Jugadores2022(fichaEquipoFacade.getEquipo().getId(), Activo2022.SI, Activo2022.NO, Activo2022.NO, Activo2022.NO, Activo2022.NO, GUION, Activo2022.NO, Activo2022.NO, Activo2022.NO);
		redirect.getJugadorDetalle();
	}

	public Jugadores2022 getJugador() {
		return jugador;
	}

	public void setJugador(Jugadores2022 jugador) {
		this.jugador = jugador;
	}
	
}
