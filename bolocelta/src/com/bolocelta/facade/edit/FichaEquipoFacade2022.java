package com.bolocelta.facade.edit;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.bolocelta.application.Redirect;
import com.bolocelta.application.enumerations.CategoriasEnumeration2022;
import com.bolocelta.application.session.SessionState;
import com.bolocelta.bbdd.constants.Activo2022;
import com.bolocelta.bbdd.constants.Modalidad2022;
import com.bolocelta.bbdd.constants.structure.EstructuraEquipos2022;
import com.bolocelta.bbdd.createTable.CrearEquipos2022;
import com.bolocelta.bbdd.createTable.CrearJugadores2022;
import com.bolocelta.bbdd.readTables.LeerCampeonatoMasculinoParejasPrimera2022;
import com.bolocelta.bbdd.readTables.LeerCampeonatoMasculinoParejasSegunda2022;
import com.bolocelta.bbdd.readTables.LeerCampeonatoMasculinoParejasTercera2022;
import com.bolocelta.bbdd.readTables.LeerCampeonatoParejasFemenino2022;
import com.bolocelta.bbdd.readTables.LeerCampeonatoParejasMixto2022;
import com.bolocelta.bbdd.readTables.LeerCategorias2022;
import com.bolocelta.bbdd.readTables.LeerEquipos2022;
import com.bolocelta.bbdd.readTables.LeerJugadores2022;
import com.bolocelta.entities.Categorias2022;
import com.bolocelta.entities.Equipos2022;
import com.bolocelta.entities.Jugadores2022;
import com.bolocelta.entities.Parejas2022;
import com.bolocelta.entities.ParticipantesParejas2022;
import com.bolocelta.facade.add.CampeonatoCadetesIndividualAddFacade2022;
import com.bolocelta.facade.add.CampeonatoFemeninoIndividualAddFacade2022;
import com.bolocelta.facade.add.CampeonatoJuvenilesIndividualAddFacade2022;
import com.bolocelta.facade.add.CampeonatoMasculinoIndividualPrimeraAddFacade2022;
import com.bolocelta.facade.add.CampeonatoMasculinoIndividualSegundaAddFacade2022;
import com.bolocelta.facade.add.CampeonatoMasculinoIndividualTerceraAddFacade2022;
import com.bolocelta.facade.add.CampeonatoMasculinoParejasPrimeraAddFacade2022;
import com.bolocelta.facade.add.CampeonatoMasculinoParejasSegundaAddFacade2022;
import com.bolocelta.facade.add.CampeonatoMasculinoParejasTerceraAddFacade2022;
import com.bolocelta.facade.add.CampeonatoParejasFemeninoAddFacade2022;
import com.bolocelta.facade.add.CampeonatoParejasMixtoAddFacade2022;
import com.bolocelta.facade.add.CampeonatoVeteranosIndividualAddFacade2022;

@Named
@ManagedBean
@SessionScoped
public class FichaEquipoFacade2022 implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Redirect redirect;
	
	@Inject
	private SessionState sessionState;
	//Individual
	@Inject
	private CampeonatoMasculinoIndividualPrimeraAddFacade2022 campeonatoMasculinoIndividualPrimeraAddFacade;
	@Inject
	private CampeonatoMasculinoIndividualSegundaAddFacade2022 campeonatoMasculinoIndividualSegundaAddFacade;
	@Inject
	private CampeonatoMasculinoIndividualTerceraAddFacade2022 campeonatoMasculinoIndividualTerceraAddFacade;
	@Inject
	private CampeonatoFemeninoIndividualAddFacade2022 campeonatoFemeninoIndividualAddFacade;
	@Inject
	private CampeonatoVeteranosIndividualAddFacade2022 campeonatoVeteranosIndividualAddFacade;
	@Inject
	private CampeonatoJuvenilesIndividualAddFacade2022 campeonatoJuvenilesIndividualAddFacade;
	@Inject
	private CampeonatoCadetesIndividualAddFacade2022 campeonatoCadetesIndividualAddFacade;
	//Parejas
	@Inject
	private CampeonatoMasculinoParejasPrimeraAddFacade2022 campeonatoMasculinoParejasPrimeraAddFacade;
	@Inject
	private CampeonatoMasculinoParejasSegundaAddFacade2022 campeonatoMasculinoParejasSegundaAddFacade;
	@Inject
	private CampeonatoMasculinoParejasTerceraAddFacade2022 campeonatoMasculinoParejasTerceraAddFacade;
//	@Inject
//	private CampeonatoFemeninoParejasAddFacade campeonatoFemeninoParejasAddFacade;
	@Inject
	private CampeonatoParejasFemeninoAddFacade2022 campeonatoParejasFemeninoAddFacade;
	@Inject
	private CampeonatoParejasMixtoAddFacade2022 campeonatoParejasMixtoAddFacade;
	
	
	private LeerEquipos2022 leerEquipos = new LeerEquipos2022();
	private LeerCategorias2022 leerCategorias = new LeerCategorias2022();
	
	private LeerJugadores2022 leerJugadores = new LeerJugadores2022();
	
	private CrearJugadores2022 crearJugadores = new CrearJugadores2022();
	private CrearEquipos2022 crearEquipos = new CrearEquipos2022();
	
	private LeerCampeonatoMasculinoParejasPrimera2022 leerCampeonatoMasculinoParejasPrimera = new LeerCampeonatoMasculinoParejasPrimera2022();
	private LeerCampeonatoMasculinoParejasSegunda2022 leerCampeonatoMasculinoParejasSegunda = new LeerCampeonatoMasculinoParejasSegunda2022();
	private LeerCampeonatoMasculinoParejasTercera2022 leerCampeonatoMasculinoParejasTercera = new LeerCampeonatoMasculinoParejasTercera2022();
	
	private LeerCampeonatoParejasMixto2022 leerCampeonatoParejasMixto = new LeerCampeonatoParejasMixto2022();
	
	private LeerCampeonatoParejasFemenino2022 leerCampeonatoParejasFemenino = new LeerCampeonatoParejasFemenino2022();
	
	private Equipos2022 equipo = null;

	public Equipos2022 getEquipo() {
		return equipo;
	}

	public void setEquipo(Equipos2022 equipo) {
		this.equipo = equipo;
	}
	
	public Equipos2022 getMiEquipo() {
		setEquipo((Equipos2022) sessionState.getUserEquipo());
		return equipo;
	}
	
	public void doSelectedMyTeam(){
		setEquipo((Equipos2022) sessionState.getUserEquipo());
		redirect.getMiEquipo();
	}
	
	public void doSelected(Integer id){
		this.equipo = (Equipos2022) leerEquipos.read(id, true);
		redirect.getEquipoDetalle();
	}
	
	public void doSelectedParejasJugador1(Integer id){
		this.jugador1 = leerJugadores.read(id);
		this.jugador1.setEquipo(leerEquipos.read(jugador1.getEquipoId()));
		redirect.getAddInscripcionParejas();
	}
	
	public void doSelectedParejasJugador2Equipo(Integer id){
		this.jugador2 = leerJugadores.read(id);
		this.jugador2.setEquipo(leerEquipos.read(jugador2.getEquipoId()));
		redirect.getAddInscripcionParejas();
	}
	
	public void doSelectedParejasJugador2OtrosEquipos(Integer id){
		this.jugador2 = leerJugadores.read(id);
		this.jugador2.setEquipo(leerEquipos.read(jugador2.getEquipoId()));
		redirect.getAddInscripcionParejas();
	}
	
	public void doSelectedParejasFemeninoJugador1(Integer id){
		this.jugador1 = leerJugadores.read(id);
		this.jugador1.setEquipo(leerEquipos.read(jugador1.getEquipoId()));
		redirect.getAddInscripcionParejasFemenino();
	}
	
	public void doSelectedParejasFemeninoJugador2Equipo(Integer id){
		this.jugador2 = leerJugadores.read(id);
		this.jugador2.setEquipo(leerEquipos.read(jugador2.getEquipoId()));
		redirect.getAddInscripcionParejasFemenino();
	}
	
	public void doSelectedParejasFemeninoJugador2OtrosEquipos(Integer id){
		this.jugador2 = leerJugadores.read(id);
		this.jugador2.setEquipo(leerEquipos.read(jugador2.getEquipoId()));
		redirect.getAddInscripcionParejasFemenino();
	}
	
	public void doSelectedCategoria(Integer id){
		this.categoria = leerCategorias.read(id);
		redirect.getAddInscripcionParejas();
	}
	
	public void doInscripcionIndividual(){
		redirect.getInscripcionIndividual();
	}
	
	public void doInscripcionParejas(){
		redirect.getInscripcionParejas();
	}
	
	public void doInscripcionParejasMixto(){
		redirect.getInscripcionParejasMixto();
	}
	
	public void doInscripcionParejasFemenino(){
		redirect.getInscripcionParejasFemenino();
	}
	
	public void addInscripcionParejas(){
		this.jugador1 = new Jugadores2022();
		this.jugador2 = new Jugadores2022();
		this.categoria = new Categorias2022();
		redirect.getAddInscripcionParejas();
	}
	
	public void addInscripcionParejasMixto(){
		this.jugador1 = new Jugadores2022();
		this.jugador2 = new Jugadores2022();
		this.categoria = leerCategorias.read(CategoriasEnumeration2022.CATEGORIA_MIXTO);
		redirect.getAddInscripcionParejasMixto();
	}
	
	public void addInscripcionParejasFemenino(){
		this.jugador1 = new Jugadores2022();
		this.jugador2 = new Jugadores2022();
		this.categoria = leerCategorias.read(CategoriasEnumeration2022.CATEGORIA_FEMENINO);
		redirect.getAddInscripcionParejasFemenino();
	}
	
	public void doInsertParejaMasculina() {
		doInscribirPareja(this.jugador1, this.jugador2, this.categoria);
	}
	
	public void doInsertParejaMixto() {
		doInscribirPareja(this.jugador1, this.jugador2, this.categoria);
	}
	
	public void doInsertParejaFemenino() {
		doInscribirPareja(this.jugador1, this.jugador2, this.categoria);
	}
	
	public void addCategoria(){
		redirect.getAddCategoria();
	}
	
	public void addJugador1Equipo(){
		redirect.getAddJugador1Equipo();
	}
	
	public void addJugador2Equipo(){
		redirect.getAddJugador2Equipo();
	}
	
	public void addJugador2OtroEquipo(){
		redirect.getAddJugador2OtroEquipo();
	}
	
	public void addJugador1EquipoMixto(){
		redirect.getAddJugador1EquipoMixto();
	}
	
	public void addJugador2EquipoMixto(){
		redirect.getAddJugador2EquipoMixto();
	}
	
	public void addJugador2OtroEquipoMixto(){
		redirect.getAddJugador2OtroEquipoMixto();
	}
	
	public void addJugador1EquipoFemenino(){
		redirect.getAddJugador1EquipoFemenino();
	}
	
	public void addJugador2EquipoFemenino(){
		redirect.getAddJugador2EquipoFemenino();
	}
	
	public void addJugador2OtroEquipoFemenino(){
		redirect.getAddJugador2OtroEquipoFemenino();
	}
	
	public Integer getMaxRowsPaginator(){
		return 8;
	}
	
	public boolean isEnabledPaginator(){
		return (getEquipo() != null && getEquipo().getJugadoresList() != null) ? (getEquipo().getJugadoresList().size() > getMaxRowsPaginator()) : false;
	}
	
	public void doCancel() {
		redirect.getMiEquipo();
	}
	
	public void doCancelFichaPareja() {
		redirect.getAddInscripcionParejas();
	}
	
	public void doCancelFichaParejaMixto() {
		redirect.getAddInscripcionParejasMixto();
	}
	
	public void doCancelFichaParejaFemenino() {
		redirect.getAddInscripcionParejasFemenino();
	}
	
	//Actualizar Email
	public void doActualizarEmail(){
		if(crearEquipos.validateEmail(getEquipo())){
			doActualizarEmail(getEquipo());
		}
	}
	
	private void doActualizarEmail(Equipos2022 equipo) {
		if(equipo != null){
//			System.out.println("Actualizando email");
			//Actualizar email equipo
			crearEquipos.preparateUpdateEmail(equipo);
		}
	}
	
	//Inscripcion de jugador en campeonato individual
	
	public void doInscribirPrimera(Jugadores2022 jugador){
		doInscribirJugador(jugador, CategoriasEnumeration2022.CATEGORIA_PRIMERA, Activo2022.SI);
	}
	
	public void doInscribirSegunda(Jugadores2022 jugador){
		doInscribirJugador(jugador, CategoriasEnumeration2022.CATEGORIA_SEGUNDA, Activo2022.SI);
	}
	
	public void doInscribirTercera(Jugadores2022 jugador){
		doInscribirJugador(jugador, CategoriasEnumeration2022.CATEGORIA_TERCERA, Activo2022.SI);
	}
	
	public void doInscribirFemenino(Jugadores2022 jugador){
		doInscribirJugador(jugador, CategoriasEnumeration2022.CATEGORIA_FEMENINO, Activo2022.SI);
	}
	
	public void doInscribirVeteranos(Jugadores2022 jugador){
		doInscribirJugador(jugador, CategoriasEnumeration2022.CATEGORIA_VETERANO, Activo2022.SI);
	}
	
	public void doInscribirJuveniles(Jugadores2022 jugador){
		doInscribirJugador(jugador, CategoriasEnumeration2022.CATEGORIA_JUVENIL, Activo2022.SI);
	}
	
	public void doInscribirCadetes(Jugadores2022 jugador){
		doInscribirJugador(jugador, CategoriasEnumeration2022.CATEGORIA_CADETE, Activo2022.SI);
	}
	
	public void doDesinscribirPrimera(Jugadores2022 jugador){
		doInscribirJugador(jugador, CategoriasEnumeration2022.CATEGORIA_PRIMERA, Activo2022.NO);
	}
	
	public void doDesinscribirSegunda(Jugadores2022 jugador){
		doInscribirJugador(jugador, CategoriasEnumeration2022.CATEGORIA_SEGUNDA, Activo2022.NO);
	}
	
	public void doDesinscribirTercera(Jugadores2022 jugador){
		doInscribirJugador(jugador, CategoriasEnumeration2022.CATEGORIA_TERCERA, Activo2022.NO);
	}
	
	public void doDesinscribirFemenino(Jugadores2022 jugador){
		doInscribirJugador(jugador, CategoriasEnumeration2022.CATEGORIA_FEMENINO, Activo2022.NO);
	}
	
	public void doDesinscribirVeteranos(Jugadores2022 jugador){
		doInscribirJugador(jugador, CategoriasEnumeration2022.CATEGORIA_VETERANO, Activo2022.NO);
	}
	
	public void doDesinscribirJuveniles(Jugadores2022 jugador){
		doInscribirJugador(jugador, CategoriasEnumeration2022.CATEGORIA_JUVENIL, Activo2022.NO);
	}
	
	public void doDesinscribirCadetes(Jugadores2022 jugador){
		doInscribirJugador(jugador, CategoriasEnumeration2022.CATEGORIA_CADETE, Activo2022.NO);
	}
	
	public void doInscribirJugador(Jugadores2022 jugador, Integer categoria, String activo) {
		if(jugador != null && categoria != null){
			
			if(categoria == 1){
//				System.out.println("Inscripcion en la categoria 1");
				
				//Actualizar ficha jugador
				jugador.setIndividualPrimera(activo);
				crearJugadores.preparateUpdateRow(jugador);
				//Realizar inscripcion en campeonato
				campeonatoMasculinoIndividualPrimeraAddFacade.doInscribirJugador(
						jugador, leerCategorias.read(categoria), 
						sessionState.getUser(), activo);
				
			}else if(categoria == 2){
//				System.out.println("Inscripcion en la categoria 2");
				
				//Actualizar ficha jugador
				jugador.setIndividualSegunda(activo);
				crearJugadores.preparateUpdateRow(jugador);
				//Realizar inscripcion en campeonato
				campeonatoMasculinoIndividualSegundaAddFacade.doInscribirJugador(
						jugador, leerCategorias.read(categoria), 
						sessionState.getUser(), activo);
				
			}else if(categoria == 3){
//				System.out.println("Inscripcion en la categoria 3");
				
				//Actualizar ficha jugador
				jugador.setIndividualTercera(activo);
				crearJugadores.preparateUpdateRow(jugador);
				//Realizar inscripcion en campeonato
				campeonatoMasculinoIndividualTerceraAddFacade.doInscribirJugador(
						jugador, leerCategorias.read(categoria), 
						sessionState.getUser(), activo);
				
			}else if(categoria == 4){
//				System.out.println("Inscripcion en la categoria 4");
				
				//Actualizar ficha jugador
				jugador.setIndividualFemenino(activo);
				crearJugadores.preparateUpdateRow(jugador);
				//Realizar inscripcion en campeonato
				campeonatoFemeninoIndividualAddFacade.doInscribirJugador(
						jugador, leerCategorias.read(categoria), 
						sessionState.getUser(), activo);
				
			}else if(categoria == 5){
//				System.out.println("Inscripcion en la categoria 5");
				
				//Actualizar ficha jugador
				jugador.setIndividualVeterano(activo);
				crearJugadores.preparateUpdateRow(jugador);
				//Realizar inscripcion en campeonato
				campeonatoVeteranosIndividualAddFacade.doInscribirJugador(
						jugador, leerCategorias.read(categoria), 
						sessionState.getUser(), activo);
				
			}else if(categoria == 6){
//				System.out.println("Inscripcion en la categoria 4");
				
				//Actualizar ficha jugador
				jugador.setIndividualJuvenil(activo);
				crearJugadores.preparateUpdateRow(jugador);
				//Realizar inscripcion en campeonato
				campeonatoJuvenilesIndividualAddFacade.doInscribirJugador(
						jugador, leerCategorias.read(categoria), 
						sessionState.getUser(), activo);
				
			}else if(categoria == 10){
//				System.out.println("Inscripcion en la categoria 4");
				
				//Actualizar ficha jugador
				jugador.setIndividualCadete(activo);
				crearJugadores.preparateUpdateRow(jugador);
				//Realizar inscripcion en campeonato
				campeonatoCadetesIndividualAddFacade.doInscribirJugador(
						jugador, leerCategorias.read(categoria), 
						sessionState.getUser(), activo);
				
			}
			
			//redirect.getMiEquipo();
		}
		
	}
	
	
	// Filtrar los botones de inscripcion para el jugador
	
	public boolean isInscribibleMasculino(Jugadores2022 jugador, Integer categoria){
		boolean inscribible = false;
		
		if(isMasculino(jugador) && isJugadorActivo(jugador)){
			if(categoria == 1 && !isInscritoIndividualPrimera(jugador) && !isInscritoIndividualOtraCategoria(jugador) && 
					(getEquipo().getCategoriaId() == 1 || getEquipo().getCategoriaId() == 2 || getEquipo().getCategoriaId() == 3)){
				inscribible = true;
			}else if(categoria == 2 && !isInscritoIndividualSegunda(jugador) && !isInscritoIndividualOtraCategoria(jugador) && 
						(getEquipo().getCategoriaId() == 2 || getEquipo().getCategoriaId() == 3)){
				inscribible = true;
			}else if(categoria == 3 && !isInscritoIndividualTercera(jugador) && !isInscritoIndividualOtraCategoria(jugador) && 
					(getEquipo().getCategoriaId() == 3)){
				inscribible = true;
			}
		}
		return inscribible;
	}
	
	public boolean isDesinscribibleMasculino(Jugadores2022 jugador, Integer categoria){
		boolean inscribible = false;
		
		if(isMasculino(jugador) && isJugadorActivo(jugador)){
			if(categoria == 1 && isInscritoIndividualPrimera(jugador) && isInscritoIndividualOtraCategoria(jugador)){
				inscribible = true;
			}else if(categoria == 2 && isInscritoIndividualSegunda(jugador) && isInscritoIndividualOtraCategoria(jugador)){
				inscribible = true;
			}else if(categoria == 3 && isInscritoIndividualTercera(jugador) && isInscritoIndividualOtraCategoria(jugador)){
				inscribible = true;
			}
		}
		return inscribible;
	}
	
	public boolean isInscribibleFemenino(Jugadores2022 jugador, Integer categoria){
		boolean inscribible = false;
		
		if(isFemenino(jugador) && isJugadorActivo(jugador)){
			if(categoria == 4 && !isInscritoIndividualFemenino(jugador)){
				inscribible = true;
			}
		}
		return inscribible;
	}
	
	public boolean isInscribibleVeteranos(Jugadores2022 jugador, Integer categoria){
		boolean inscribible = false;
		
		if(isVeterano(jugador) && isJugadorActivo(jugador)){
			if(categoria == 5 && !isInscritoIndividualVeteranos(jugador)){
				inscribible = true;
			}
		}
		return inscribible;
	}
	
	public boolean isInscribibleJuveniles(Jugadores2022 jugador, Integer categoria){
		boolean inscribible = false;
		
		if(isJuvenil(jugador) && isJugadorActivo(jugador)){
			if(categoria == 6 && !isInscritoIndividualJuveniles(jugador)){
				inscribible = true;
			}
		}
		return inscribible;
	}
	
	public boolean isInscribibleCadetes(Jugadores2022 jugador, Integer categoria){
		boolean inscribible = false;
		
		if(isCadete(jugador) && isJugadorActivo(jugador)){
			if(categoria == 10 && !isInscritoIndividualCadetes(jugador)){
				inscribible = true;
			}
		}
		return inscribible;
	}
	
	public boolean isDesinscribibleFemenino(Jugadores2022 jugador, Integer categoria){
		boolean inscribible = false;
		
		if(isFemenino(jugador) && isJugadorActivo(jugador)){
			if(categoria == 4 && isInscritoIndividualFemenino(jugador)){
				inscribible = true;
			}
		}
		return inscribible;
	}
	
	public boolean isDesinscribibleVeteranos(Jugadores2022 jugador, Integer categoria){
		boolean inscribible = false;
		
		if(isVeterano(jugador) && isJugadorActivo(jugador)){
			if(categoria == 5 && isInscritoIndividualVeteranos(jugador)){
				inscribible = true;
			}
		}
		return inscribible;
	}
	
	public boolean isDesinscribibleJuveniles(Jugadores2022 jugador, Integer categoria){
		boolean inscribible = false;
		
		if(isJuvenil(jugador) && isJugadorActivo(jugador)){
			if(categoria == 6 && isInscritoIndividualJuveniles(jugador)){
				inscribible = true;
			}
		}
		return inscribible;
	}
	
	public boolean isDesinscribibleCadetes(Jugadores2022 jugador, Integer categoria){
		boolean inscribible = false;
		
		if(isCadete(jugador) && isJugadorActivo(jugador)){
			if(categoria == 10 && isInscritoIndividualCadetes(jugador)){
				inscribible = true;
			}
		}
		return inscribible;
	}
	
	public boolean isMasculino(Jugadores2022 jugador){
		if(jugador.getModalidad().equalsIgnoreCase(Modalidad2022.MASCULINO)){
			return true;
		}
		return false;
	}
	
	public boolean isFemenino(Jugadores2022 jugador){
		if(jugador.getModalidad().equalsIgnoreCase(Modalidad2022.FEMENINO)){
			return true;
		}
		return false;
	}
	
	public boolean isVeterano(Jugadores2022 jugador){
		if(jugador.getEdad() > 57){
			return true;
		}
		return false;
	}
	
	public boolean isJuvenil(Jugadores2022 jugador){
		if(jugador.getEdad() < 19){
			return true;
		}
		return false;
	}
	
	public boolean isCadete(Jugadores2022 jugador){
		if(jugador.getEdad() < 16){
			return true;
		}
		return false;
	}
	
	public boolean isInscritoIndividualPrimera(Jugadores2022 jugador){
		if( jugador.getIndividualPrimera().equalsIgnoreCase(Activo2022.SI)){
			return true;
		}
		return false;
	}
	
	public boolean isInscritoIndividualSegunda(Jugadores2022 jugador){
		if( jugador.getIndividualSegunda().equalsIgnoreCase(Activo2022.SI) ){
			return true;
		}
		return false;
	}
	
	public boolean isInscritoIndividualTercera(Jugadores2022 jugador){
		if( jugador.getIndividualTercera().equalsIgnoreCase(Activo2022.SI) ){
			return true;
		}
		return false;
	}
	
	public boolean isInscritoIndividualFemenino(Jugadores2022 jugador){
		if( jugador.getIndividualFemenino().equalsIgnoreCase(Activo2022.SI) ){
			return true;
		}
		return false;
	}
	
	public boolean isInscritoIndividualVeteranos(Jugadores2022 jugador){
		if( jugador.getIndividualVeterano().equalsIgnoreCase(Activo2022.SI) ){
			return true;
		}
		return false;
	}
	
	public boolean isInscritoIndividualJuveniles(Jugadores2022 jugador){
		if( jugador.getIndividualJuvenil().equalsIgnoreCase(Activo2022.SI) ){
			return true;
		}
		return false;
	}
	
	public boolean isInscritoIndividualCadetes(Jugadores2022 jugador){
		if( jugador.getIndividualCadete().equalsIgnoreCase(Activo2022.SI) ){
			return true;
		}
		return false;
	}
	
	public boolean isInscritoIndividualOtraCategoria(Jugadores2022 jugador){
		if( jugador.getIndividualPrimera().equalsIgnoreCase(Activo2022.SI) || jugador.getIndividualSegunda().equalsIgnoreCase(Activo2022.SI) ||
			jugador.getIndividualTercera().equalsIgnoreCase(Activo2022.SI) || jugador.getIndividualFemenino().equalsIgnoreCase(Activo2022.SI) ){
			return true;
		}
		return false;
	}
	
	public boolean isJugadorActivo(Jugadores2022 jugador){
		if( jugador.getActivo().equalsIgnoreCase(Activo2022.SI)){
			return true;
		}
		return false;
	}
	
	//Horarios preferentes
	
	public boolean isCerradoHorarioPreferente(){
		return getEquipo().isCerradoHorarioPreferente();
	}
	
	public boolean isHorarioPreferenteSabado1100Si(){
		if(getEquipo().getHorarioPreferenteSabadoMañana().equalsIgnoreCase(Activo2022.SI)){
			return true;
		}
		return false;
	}
	
	public boolean isHorarioPreferenteSabado1100No(){
		if(getEquipo().getHorarioPreferenteSabadoMañana().equalsIgnoreCase(Activo2022.NO)){
			return true;
		}
		return false;
	}
	
	public void doHorarioPreferenteSabado1100(){
		if(isCerradoHorarioPreferente()){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Se ha cerrado el plazo para modificar el horario preferente", null));
		}else{
			if(getEquipo().getHorarioPreferenteSabadoMañana().equalsIgnoreCase(Activo2022.SI)){
				getEquipo().setHorarioPreferenteSabadoMañana(Activo2022.NO);
			}else if(getEquipo().getHorarioPreferenteSabadoMañana().equalsIgnoreCase(Activo2022.NO)){
				getEquipo().setHorarioPreferenteSabadoMañana(Activo2022.SI);
			}
			doActualizarHorarioPreferente(getEquipo(), EstructuraEquipos2022.COL_HORARIO_PSM);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha modificado el horario preferente.", null));
		}
	}
	
	public boolean isHorarioPreferenteSabado1600Si(){
		if(getEquipo().getHorarioPreferenteSabadoTarde().equalsIgnoreCase(Activo2022.SI)){
			return true;
		}
		return false;
	}
	
	public boolean isHorarioPreferenteSabado1600No(){
		if(getEquipo().getHorarioPreferenteSabadoTarde().equalsIgnoreCase(Activo2022.NO)){
			return true;
		}
		return false;
	}
	
	public void doHorarioPreferenteSabado1600(){
		if(isCerradoHorarioPreferente()){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Se ha cerrado el plazo para modificar el horario preferente", null));
		}else{
			if(getEquipo().getHorarioPreferenteSabadoTarde().equalsIgnoreCase(Activo2022.SI)){
				getEquipo().setHorarioPreferenteSabadoTarde(Activo2022.NO);
			}else if(getEquipo().getHorarioPreferenteSabadoTarde().equalsIgnoreCase(Activo2022.NO)){
				getEquipo().setHorarioPreferenteSabadoTarde(Activo2022.SI);
			}
			doActualizarHorarioPreferente(getEquipo(), EstructuraEquipos2022.COL_HORARIO_PST);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha modificado el horario preferente.", null));
		}
	}
	
	public boolean isHorarioPreferenteDomingo1100Si(){
		if(getEquipo().getHorarioPreferenteDomingoMañana().equalsIgnoreCase(Activo2022.SI)){
			return true;
		}
		return false;
	}
	
	public boolean isHorarioPreferenteDomingo1100No(){
		if(getEquipo().getHorarioPreferenteDomingoMañana().equalsIgnoreCase(Activo2022.NO)){
			return true;
		}
		return false;
	}
	
	public void doHorarioPreferenteDomingo1100(){
		if(isCerradoHorarioPreferente()){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Se ha cerrado el plazo para modificar el horario preferente", null));
		}else{
			if(getEquipo().getHorarioPreferenteDomingoMañana().equalsIgnoreCase(Activo2022.SI)){
				getEquipo().setHorarioPreferenteDomingoMañana(Activo2022.NO);
			}else if(getEquipo().getHorarioPreferenteDomingoMañana().equalsIgnoreCase(Activo2022.NO)){
				getEquipo().setHorarioPreferenteDomingoMañana(Activo2022.SI);
			}
			doActualizarHorarioPreferente(getEquipo(), EstructuraEquipos2022.COL_HORARIO_PDM);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha modificado el horario preferente.", null));
		}
	}
	
	public boolean isHorarioPreferenteDomingo1600Si(){
		if(getEquipo().getHorarioPreferenteDomingoTarde().equalsIgnoreCase(Activo2022.SI)){
			return true;
		}
		return false;
	}
	
	public boolean isHorarioPreferenteDomingo1600No(){
		if(getEquipo().getHorarioPreferenteDomingoTarde().equalsIgnoreCase(Activo2022.NO)){
			return true;
		}
		return false;
	}
	
	public void doHorarioPreferenteDomingo1600(){
		if(isCerradoHorarioPreferente()){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Se ha cerrado el plazo para modificar el horario preferente", null));
		}else{
			if(getEquipo().getHorarioPreferenteDomingoTarde().equalsIgnoreCase(Activo2022.SI)){
				getEquipo().setHorarioPreferenteDomingoTarde(Activo2022.NO);
			}else if(getEquipo().getHorarioPreferenteDomingoTarde().equalsIgnoreCase(Activo2022.NO)){
				getEquipo().setHorarioPreferenteDomingoTarde(Activo2022.SI);
			}
			doActualizarHorarioPreferente(getEquipo(), EstructuraEquipos2022.COL_HORARIO_PDT);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha modificado el horario preferente.", null));
		}
	}
	
	private void doActualizarHorarioPreferente(Equipos2022 equipo, String colHorarioPreferente) {
		if(equipo != null && colHorarioPreferente != null){
//			System.out.println("Actualizando horario preferente " + colHorarioPreferente);
			//Actualizar email equipo
			crearEquipos.preparateUpdateHorarioPreferente(equipo, colHorarioPreferente);
		}
	}
	

	private List<Jugadores2022> jugadoresEquipoMasculinoList;
	private List<Jugadores2022> jugadoresEquipoFemeninoList;
	private List<Jugadores2022> jugadoresTodosEquiposMasculinoList;
	private List<Jugadores2022> jugadoresTodosEquiposFemeninoList;
	private List<Categorias2022> categoriasList;
	private List<Categorias2022> categoriasMasculinoList;
	private boolean parejaDiferentesEquipos = false;
	
	public boolean isParejaDiferentesEquipos() {
		return parejaDiferentesEquipos;
	}

	public void setParejaDiferentesEquipos(boolean parejaDiferentesEquipos) {
		this.parejaDiferentesEquipos = parejaDiferentesEquipos;
	}

	public List<Jugadores2022> getJugadoresEquipoMasculinoList(){
		if(jugadoresEquipoMasculinoList == null){
			jugadoresEquipoMasculinoList = new ArrayList<Jugadores2022>();
			
			for (Jugadores2022 jugador : this.equipo.getJugadoresList()) {
				if(jugador.isMasculino() && jugador.getActivo().equalsIgnoreCase("SI")){
					jugadoresEquipoMasculinoList.add(jugador);
				}
			}
			
			//Ordenar el jugadores por nombre
			Collections.sort(jugadoresEquipoMasculinoList, new Comparator() {
				@Override
				public int compare(Object o1, Object o2) {
					Jugadores2022 j1 = (Jugadores2022) o1;
					Jugadores2022 j2 = (Jugadores2022) o2;
					
					int comp1 = j1.getNombreEquipo().compareTo(j2.getNombreEquipo());
					return comp1;
				}
			});
			
		}
		return jugadoresEquipoMasculinoList;
	}
	
	public List<Jugadores2022> getJugadoresEquipoFemeninoList(){
		if(jugadoresEquipoFemeninoList == null){
			jugadoresEquipoFemeninoList = new ArrayList<Jugadores2022>();
			
			for (Jugadores2022 jugador : this.equipo.getJugadoresList()) {
				if(jugador.isFemenino() && jugador.getActivo().equalsIgnoreCase("SI")){
					jugadoresEquipoFemeninoList.add(jugador);
				}
			}
			
		}
		
		//Ordenar el jugadores por nombre
		Collections.sort(jugadoresEquipoFemeninoList, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				Jugadores2022 j1 = (Jugadores2022) o1;
				Jugadores2022 j2 = (Jugadores2022) o2;
				
				int comp1 = j1.getNombreEquipo().compareTo(j2.getNombreEquipo());
				return comp1;
			}
		});
		
		return jugadoresEquipoFemeninoList;
	}
	
	public List<Jugadores2022> getJugadoresTodosEquiposMasculinoList(){
		if(jugadoresTodosEquiposMasculinoList == null){
			jugadoresTodosEquiposMasculinoList = new ArrayList<Jugadores2022>();
			
			for (Jugadores2022 jugador : (List<Jugadores2022>) leerJugadores.listResult()) {
				if(jugador.isMasculino() && jugador.getActivo().equalsIgnoreCase("SI")){
		    		if(leerEquipos != null){
		    			jugador.setEquipo(leerEquipos.read(jugador.getEquipoId()));
		    		}
					jugadoresTodosEquiposMasculinoList.add(jugador);
				}
			}
			
		}
		
		//Ordenar el jugadores por nombre
		Collections.sort(jugadoresTodosEquiposMasculinoList, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				Jugadores2022 j1 = (Jugadores2022) o1;
				Jugadores2022 j2 = (Jugadores2022) o2;
				
				int comp1 = j1.getNombreEquipo().compareTo(j2.getNombreEquipo());
				return comp1;
			}
		});
		
		return jugadoresTodosEquiposMasculinoList;
	}
	
	public List<Jugadores2022> getJugadoresTodosEquiposFemeninoList(){
		if(jugadoresTodosEquiposFemeninoList == null){
			jugadoresTodosEquiposFemeninoList = new ArrayList<Jugadores2022>();
			
			for (Jugadores2022 jugador : (List<Jugadores2022>) leerJugadores.listResult()) {
				if(jugador.isFemenino() && jugador.getActivo().equalsIgnoreCase("SI")){
		    		if(leerEquipos != null){
		    			jugador.setEquipo(leerEquipos.read(jugador.getEquipoId()));
		    		}
					jugadoresTodosEquiposFemeninoList.add(jugador);
				}
			}
			
		}
		
		//Ordenar el jugadores por nombre
		Collections.sort(jugadoresTodosEquiposFemeninoList, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				Jugadores2022 j1 = (Jugadores2022) o1;
				Jugadores2022 j2 = (Jugadores2022) o2;
				
				int comp1 = j1.getNombreEquipo().compareTo(j2.getNombreEquipo());
				return comp1;
			}
		});
		
		return jugadoresTodosEquiposFemeninoList;
	}
	
	public List<Categorias2022> getCategoriasList(){
		if(categoriasList == null){
			categoriasList = new ArrayList<Categorias2022>();
			
			for (Categorias2022 categoria : (List<Categorias2022>) leerCategorias.listResultParejas()) {
				categoriasList.add(categoria);
			}
			
		}
		
		return categoriasList;
	}
	
	public List<Categorias2022> getCategoriasMasculinoList(){
		if(categoriasMasculinoList == null){
			categoriasMasculinoList = new ArrayList<Categorias2022>();
			
			for (Categorias2022 categoria : (List<Categorias2022>) leerCategorias.listResultParejas()) {
				if(categoria.getId() == 1 || categoria.getId() == 2 || categoria.getId() == 3){
					categoriasMasculinoList.add(categoria);
				}
			}
			
		}
		
		return categoriasMasculinoList;
	}
	
	private Jugadores2022 jugador1;
	private Jugadores2022 jugador2;
	private Categorias2022 categoria;
	
	public Categorias2022 getCategoria() {
		return categoria;
	}

	public void setCategoria(Categorias2022 categoria) {
		this.categoria = categoria;
	}

	public Jugadores2022 getJugador1() {
		return jugador1;
	}

	public void setJugador1(Jugadores2022 jugador1) {
		this.jugador1 = jugador1;
	}

	public Jugadores2022 getJugador2() {
		return jugador2;
	}

	public void setJugador2(Jugadores2022 jugador2) {
		this.jugador2 = jugador2;
	}
	
	
	private List<ParticipantesParejas2022> resultListParticipantes = null;
	private List<ParticipantesParejas2022> resultListParticipantesMixto = null;
	private List<ParticipantesParejas2022> resultListParticipantesByEquipo = null;
	private List<ParticipantesParejas2022> resultListParticipantesMixtoByEquipo = null;
	private List<ParticipantesParejas2022> resultListParticipantesFemenino = null;
	private List<ParticipantesParejas2022> resultListParticipantesFemeninoByEquipo = null;
	
	public void refrescarParticipantesParejas(){
		this.resultListParticipantes = null;
		this.resultListParticipantesByEquipo = null;
		this.resultListParticipantesMixto = null;
		this.resultListParticipantesMixtoByEquipo = null;
		this.resultListParticipantesFemenino = null;
		this.resultListParticipantesFemeninoByEquipo = null;
		getResultListParticipantesByEquipo(getEquipo().getId());			
	}

	public List<ParticipantesParejas2022> getResultListParticipantes() {
		if(resultListParticipantes == null){
			resultListParticipantes = new ArrayList<>();
			resultListParticipantes.addAll((List<ParticipantesParejas2022>) leerCampeonatoMasculinoParejasPrimera.listResultParticipantesOrderByEquipo());
			resultListParticipantes.addAll((List<ParticipantesParejas2022>) leerCampeonatoMasculinoParejasSegunda.listResultParticipantesOrderByEquipo());
			resultListParticipantes.addAll((List<ParticipantesParejas2022>) leerCampeonatoMasculinoParejasTercera.listResultParticipantesOrderByEquipo());
		}
		return resultListParticipantes;
	}
	
	public List<ParticipantesParejas2022> getResultListParticipantesMixto() {
		if(resultListParticipantesMixto == null){
			resultListParticipantesMixto = new ArrayList<>();
			resultListParticipantesMixto.addAll((List<ParticipantesParejas2022>) leerCampeonatoParejasMixto.listResultParticipantesOrderByEquipo());
		}
		return resultListParticipantesMixto;
	}
	
	public List<ParticipantesParejas2022> getResultListParticipantesFemenino() {
		if(resultListParticipantesFemenino == null){
			resultListParticipantesFemenino = new ArrayList<>();
			resultListParticipantesFemenino.addAll((List<ParticipantesParejas2022>) leerCampeonatoParejasFemenino.listResultParticipantesOrderByEquipo());
		}
		return resultListParticipantesFemenino;
	}
	
	public List<ParticipantesParejas2022> getResultListParticipantesByEquipo(Integer idEquipo) {
		if(resultListParticipantesByEquipo == null){
			resultListParticipantesByEquipo = new ArrayList<>();

			if(resultListParticipantes == null){
				resultListParticipantes = getResultListParticipantes();
			}
			
			for (ParticipantesParejas2022 participantesParejas : resultListParticipantes) {
				if(participantesParejas != null && participantesParejas.getPareja() != null && participantesParejas.getPareja().getId() != null){
					if(participantesParejas.getPareja().getJugador1().getEquipoId().equals(idEquipo) || participantesParejas.getPareja().getJugador2().getEquipoId().equals(idEquipo)){
						resultListParticipantesByEquipo.add(participantesParejas);
					}
				}
			}
		}
		
		return resultListParticipantesByEquipo;
	}
	
	
	public List<ParticipantesParejas2022> getResultListParticipantesMixtoByEquipo(Integer idEquipo) {
		if(resultListParticipantesMixtoByEquipo == null){
			resultListParticipantesMixtoByEquipo = new ArrayList<>();

			if(resultListParticipantesMixto == null){
				resultListParticipantesMixto = getResultListParticipantesMixto();
			}
			
			for (ParticipantesParejas2022 participantesParejas : resultListParticipantesMixto) {
				if(participantesParejas != null && participantesParejas.getPareja() != null && participantesParejas.getPareja().getId() != null){
					if(participantesParejas.getPareja().getJugador1().getEquipoId().equals(idEquipo) || participantesParejas.getPareja().getJugador2().getEquipoId().equals(idEquipo)){
						resultListParticipantesMixtoByEquipo.add(participantesParejas);
					}
				}
			}
		}
		
		return resultListParticipantesMixtoByEquipo;
	}
	

	public List<ParticipantesParejas2022> getResultListParticipantesFemeninoByEquipo(Integer idEquipo) {
		if(resultListParticipantesFemeninoByEquipo == null){
			resultListParticipantesFemeninoByEquipo = new ArrayList<>();

			if(resultListParticipantesFemenino == null){
				resultListParticipantesFemenino = getResultListParticipantesFemenino();
			}
			
			for (ParticipantesParejas2022 participantesParejas : resultListParticipantesFemenino) {
				if(participantesParejas != null && participantesParejas.getPareja() != null && participantesParejas.getPareja().getId() != null){
					if(participantesParejas.getPareja().getJugador1().getEquipoId().equals(idEquipo) || participantesParejas.getPareja().getJugador2().getEquipoId().equals(idEquipo)){
						resultListParticipantesFemeninoByEquipo.add(participantesParejas);
					}
				}
			}
		}
		
		return resultListParticipantesFemeninoByEquipo;
	}
	
	
	//Inscripcion de jugador en campeonato parejas
	
	public void doInscribirPareja(Jugadores2022 jugador1, Jugadores2022 jugador2, Categorias2022 categoria){
		if(jugador1 == null || jugador1.getId() == null){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El jugador 1 de la pareja es requerido.", null));
		} else if(jugador2 == null || jugador2.getId() == null){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El jugador 2 de la pareja es requerido.", null));
		} else if(categoria == null || categoria.getId() == null){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "La Categoria de la pareja es requerida.", null));
		} else if(
				((jugador1.getEquipo().getCategoriaId() == com.bolocelta.bbdd.constants.Categorias2022.PRIMERA || jugador2.getEquipo().getCategoriaId() == com.bolocelta.bbdd.constants.Categorias2022.PRIMERA) && (categoria.getId() == com.bolocelta.bbdd.constants.Categorias2022.SEGUNDA || categoria.getId() == com.bolocelta.bbdd.constants.Categorias2022.TERCERA))
				|| (jugador1.getEquipo().getCategoriaId() == com.bolocelta.bbdd.constants.Categorias2022.SEGUNDA || jugador2.getEquipo().getCategoriaId() == com.bolocelta.bbdd.constants.Categorias2022.SEGUNDA) && (categoria.getId() == com.bolocelta.bbdd.constants.Categorias2022.TERCERA)){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Uno de los jugadores pertenece a una categoria superior, no puede participar en esta categoria.", null));
		} else {
			
			Parejas2022 parejaNew = new Parejas2022();
			parejaNew.setIdJugador1(jugador1.getId());
			parejaNew.setIdJugador2(jugador2.getId());
			parejaNew.setJugador1(jugador1);
			parejaNew.setJugador2(jugador2);
			
			ParticipantesParejas2022 participantesParejasPrimera = leerCampeonatoMasculinoParejasPrimera.existeParejaComoParticipante(parejaNew);
			ParticipantesParejas2022 participantesParejasSegunda = leerCampeonatoMasculinoParejasSegunda.existeParejaComoParticipante(parejaNew);
			ParticipantesParejas2022 participantesParejasTercera = leerCampeonatoMasculinoParejasTercera.existeParejaComoParticipante(parejaNew);
			if(participantesParejasPrimera != null || participantesParejasSegunda != null || participantesParejasTercera != null){
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "La pareja ya esta inscrita.", null));
			}else{
				participantesParejasPrimera = leerCampeonatoMasculinoParejasPrimera.existeJugadoresParejaComoParticipante(parejaNew);
				participantesParejasSegunda = leerCampeonatoMasculinoParejasSegunda.existeJugadoresParejaComoParticipante(parejaNew);
				participantesParejasTercera = leerCampeonatoMasculinoParejasTercera.existeJugadoresParejaComoParticipante(parejaNew);

				if(participantesParejasPrimera != null || participantesParejasSegunda != null || participantesParejasTercera != null){
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Algún jugador de la pareja ya esta inscrito con otro jugador.", null));
				}else{
					doInscribirPareja(jugador1, jugador2, categoria, Activo2022.SI);
				}
			}
		}
	}
	
	public void doInscribirPareja(Jugadores2022 jugador1, Jugadores2022 jugador2, Categorias2022 categoria, String activo) {
		if(jugador1 != null && jugador2 != null && categoria != null){
			
			if(categoria.getId() == 1){
				
				//Actualizar ficha pareja
				//Realizar inscripcion en campeonato
				campeonatoMasculinoParejasPrimeraAddFacade.doInscribirPareja(
						jugador1, jugador2, categoria, sessionState.getUser(), activo);
				
			}else if(categoria.getId() == 2){
				
				//Actualizar ficha pareja
				//Realizar inscripcion en campeonato
				campeonatoMasculinoParejasSegundaAddFacade.doInscribirPareja(
						jugador1, jugador2, categoria, sessionState.getUser(), activo);
				
			}else if(categoria.getId() == 3){

				//Actualizar ficha pareja
				//Realizar inscripcion en campeonato
				campeonatoMasculinoParejasTerceraAddFacade.doInscribirPareja(
						jugador1, jugador2, categoria, sessionState.getUser(), activo);
				
			}else if(categoria.getId() == 4){

				//Actualizar ficha pareja
				//Realizar inscripcion en campeonato
				campeonatoParejasFemeninoAddFacade.doInscribirPareja(
						jugador1, jugador2, categoria, sessionState.getUser(), activo);
				
			}else if(categoria.getId() == 8){

				//Actualizar ficha pareja
				//Realizar inscripcion en campeonato
				campeonatoParejasMixtoAddFacade.doInscribirPareja(
						jugador1, jugador2, categoria, sessionState.getUser(), activo);
				
			}
			
			redirect.getMiEquipo();
		}
		
	}
	
	public void doDesinscribirPareja(Integer id, Integer idCategoria){
		if(idCategoria == com.bolocelta.bbdd.constants.Categorias2022.PRIMERA){
			ParticipantesParejas2022 participantesParejasPrimera = leerCampeonatoMasculinoParejasPrimera.readParticipante(id);
			if(participantesParejasPrimera != null){
				campeonatoMasculinoParejasPrimeraAddFacade.doDesinscribirPareja(participantesParejasPrimera, sessionState.getUser(), Activo2022.NO);
			}else{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "La pareja no se puede desinscribir.", null));
			}
		}else if(idCategoria == com.bolocelta.bbdd.constants.Categorias2022.SEGUNDA){
			ParticipantesParejas2022 participantesParejasSegunda = leerCampeonatoMasculinoParejasSegunda.readParticipante(id);
			if(participantesParejasSegunda != null){
				campeonatoMasculinoParejasSegundaAddFacade.doDesinscribirPareja(participantesParejasSegunda, sessionState.getUser(), Activo2022.NO);
			}else{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "La pareja no se puede desinscribir.", null));
			}
		}else if(idCategoria == com.bolocelta.bbdd.constants.Categorias2022.TERCERA){
			ParticipantesParejas2022 participantesParejasTercera = leerCampeonatoMasculinoParejasTercera.readParticipante(id);
			if(participantesParejasTercera != null){
				campeonatoMasculinoParejasTerceraAddFacade.doDesinscribirPareja(participantesParejasTercera, sessionState.getUser(), Activo2022.NO);
			}else{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "La pareja no se puede desinscribir.", null));
			}
		}
			
	}
	
	public void doDesinscribirParejaMixto(Integer id){
		ParticipantesParejas2022 participantesParejasPrimera = leerCampeonatoParejasMixto.readParticipante(id);
		if(participantesParejasPrimera != null){
			campeonatoParejasMixtoAddFacade.doDesinscribirPareja(participantesParejasPrimera, sessionState.getUser(), Activo2022.NO);
		}else{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "La pareja no se puede desinscribir.", null));
		}	
	}
	
	public void doInscribirParejaMixto(Jugadores2022 jugador1, Jugadores2022 jugador2, Categorias2022 categoria){
		if(jugador1 == null || jugador1.getId() == null){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El jugador 1 de la pareja es requerido.", null));
		} else if(jugador2 == null || jugador2.getId() == null){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El jugador 2 de la pareja es requerido.", null));
		} else if(categoria == null || categoria.getId() == null){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "La Categoria de la pareja es requerida.", null));
		} else {
			
			Parejas2022 parejaNew = new Parejas2022();
			parejaNew.setIdJugador1(jugador1.getId());
			parejaNew.setIdJugador2(jugador2.getId());
			parejaNew.setJugador1(jugador1);
			parejaNew.setJugador2(jugador2);
			
			ParticipantesParejas2022 participantesParejas = leerCampeonatoParejasMixto.existeParejaComoParticipante(parejaNew);
			if(participantesParejas != null){
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "La pareja ya esta inscrita.", null));
			}else{
				participantesParejas = leerCampeonatoParejasMixto.existeJugadoresParejaComoParticipante(parejaNew);

				if(participantesParejas != null){
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Algún jugador de la pareja ya esta inscrito con otro jugador.", null));
				}else{
					doInscribirPareja(jugador1, jugador2, categoria, Activo2022.SI);
				}
			}
		}
	}
	
	
	public void doDesinscribirParejaFemenino(Integer id){
		ParticipantesParejas2022 participantesParejasPrimera = leerCampeonatoParejasFemenino.readParticipante(id);
		if(participantesParejasPrimera != null){
			campeonatoParejasFemeninoAddFacade.doDesinscribirPareja(participantesParejasPrimera, sessionState.getUser(), Activo2022.NO);
		}else{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "La pareja no se puede desinscribir.", null));
		}	
	}
	
	public void doInscribirParejaFemenino(Jugadores2022 jugador1, Jugadores2022 jugador2, Categorias2022 categoria){
		if(jugador1 == null || jugador1.getId() == null){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El jugador 1 de la pareja es requerido.", null));
		} else if(jugador2 == null || jugador2.getId() == null){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El jugador 2 de la pareja es requerido.", null));
		} else if(categoria == null || categoria.getId() == null){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "La Categoria de la pareja es requerida.", null));
		} else {
			
			Parejas2022 parejaNew = new Parejas2022();
			parejaNew.setIdJugador1(jugador1.getId());
			parejaNew.setIdJugador2(jugador2.getId());
			parejaNew.setJugador1(jugador1);
			parejaNew.setJugador2(jugador2);
			
			ParticipantesParejas2022 participantesParejas = leerCampeonatoParejasFemenino.existeParejaComoParticipante(parejaNew);
			if(participantesParejas != null){
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "La pareja ya esta inscrita.", null));
			}else{
				participantesParejas = leerCampeonatoParejasFemenino.existeJugadoresParejaComoParticipante(parejaNew);

				if(participantesParejas != null){
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Algún jugador de la pareja ya esta inscrito con otro jugador.", null));
				}else{
					doInscribirPareja(jugador1, jugador2, categoria, Activo2022.SI);
				}
			}
		}
	}
	
	
	

}
