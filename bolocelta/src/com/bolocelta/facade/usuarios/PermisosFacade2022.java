package com.bolocelta.facade.usuarios;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;

import com.bolocelta.bbdd.readTables.usuarios.LeerPermisos2022;
import com.bolocelta.entities.usuarios.Permisos2022;

@Named
@ConversationScoped
@ManagedBean
public class PermisosFacade2022 implements Serializable {

	private static final long serialVersionUID = 1L;

	private LeerPermisos2022 leerPermisos = new LeerPermisos2022();

	private List<Permisos2022> resultList = null;

	public List<Permisos2022> getResultList() {
		if (resultList == null) {
			resultList = (List<Permisos2022>) leerPermisos.listResult();
		}
		return resultList;
	}
	
	public List<Permisos2022> getResultListByRol(Integer rolId) {
		if (resultList == null) {
			resultList = (List<Permisos2022>) leerPermisos.listResultByRol(rolId);
		}
		return resultList;
	}

	public Object selected() {
		// TODO Auto-generated method stub
		return null;
	}

	public Object read(Integer id) {
		return leerPermisos.read(id);
	}

	public String insert(Object insert) {
		// TODO Auto-generated method stub
		return null;
	}

	public String update(Object update) {
		// TODO Auto-generated method stub
		return null;
	}

	public String delete(Object delete) {
		// TODO Auto-generated method stub
		return null;
	}

}
