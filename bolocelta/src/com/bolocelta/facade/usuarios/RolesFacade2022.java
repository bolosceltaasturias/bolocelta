package com.bolocelta.facade.usuarios;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;

import com.bolocelta.bbdd.readTables.usuarios.LeerRoles2022;
import com.bolocelta.entities.usuarios.Roles2022;

@Named
@ConversationScoped
@ManagedBean
public class RolesFacade2022 implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private LeerRoles2022 leerRoles = new LeerRoles2022();
	
	private List<Roles2022> resultList = null;

	public List<Roles2022> getResultList() {
		if(resultList == null){
			resultList = (List<Roles2022>) leerRoles.listResult();
		}
		return resultList;
	}

	public Object selected() {
		// TODO Auto-generated method stub
		return null;
	}

	public Object read(Integer id) {
		return leerRoles.read(id);
	}

	public String insert(Object insert) {
		// TODO Auto-generated method stub
		return null;
	}

	public String update(Object update) {
		// TODO Auto-generated method stub
		return null;
	}

	public String delete(Object delete) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
	

}
