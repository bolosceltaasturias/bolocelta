package com.bolocelta.facade.usuarios;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;

import com.bolocelta.bbdd.readTables.usuarios.LeerUsuarios2022;
import com.bolocelta.entities.usuarios.Usuarios2022;

@Named
@ConversationScoped
@ManagedBean
public class UsuariosFacade2022 implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private LeerUsuarios2022 leerUsuarios = new LeerUsuarios2022();
	
	private List<Usuarios2022> resultList = null;

	public List<Usuarios2022> getResultList() {
		if(resultList == null){
			resultList = (List<Usuarios2022>) leerUsuarios.listResult();
		}
		return resultList;
	}

	public Object selected() {
		// TODO Auto-generated method stub
		return null;
	}

	public Object read(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Usuarios2022 read(String user, String pass) {
		return leerUsuarios.read(user, pass);
	}

	public String insert(Object insert) {
		// TODO Auto-generated method stub
		return null;
	}

	public String update(Object update) {
		// TODO Auto-generated method stub
		return null;
	}

	public String delete(Object delete) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
	

}
