package com.bolocelta.facade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.bolocelta.application.enumerations.CategoriasInfantilEnumeration2023;
import com.bolocelta.application.session.SessionState;
import com.bolocelta.bbdd.constants.Activo2022;
import com.bolocelta.bbdd.createTable.CrearCampeonatoLigaInfantil2023;
import com.bolocelta.bbdd.readTables.LeerCampeonatoLigaInfantil2023;
import com.bolocelta.entities.CampeonatoLigaInfantilCalendario2023;
import com.bolocelta.entities.CampeonatoLigaInfantilClasificacion2023;

@Named
@ConversationScoped
@ManagedBean
public class CampeonatoLigaInfantilFacade2023 implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private SessionState sessionState;
	
	private LeerCampeonatoLigaInfantil2023 leerCampeonatoLigaInfantil = new LeerCampeonatoLigaInfantil2023();
	
	private CrearCampeonatoLigaInfantil2023 campeonatoLigaInfantil = new CrearCampeonatoLigaInfantil2023();
	
	private List<CampeonatoLigaInfantilCalendario2023> resultListClasificacionMini = null;
	private List<CampeonatoLigaInfantilCalendario2023> resultListClasificacionBenjamin = null;
	private List<CampeonatoLigaInfantilCalendario2023> resultListClasificacionAlevin = null;
	private List<CampeonatoLigaInfantilCalendario2023> resultListClasificacionInfantil = null;
	private List<CampeonatoLigaInfantilCalendario2023> resultListClasificacionCadete = null;
	private List<CampeonatoLigaInfantilCalendario2023> resultListCalendario = null;

	public List<CampeonatoLigaInfantilCalendario2023> getResultListClasificacionMini() {
		if(resultListClasificacionMini == null){
			resultListClasificacionMini = (List<CampeonatoLigaInfantilCalendario2023>) leerCampeonatoLigaInfantil.listResultClasificacion(CategoriasInfantilEnumeration2023.CATEGORIA_MINI);
		}
		return resultListClasificacionMini;
	}
	
	public List<CampeonatoLigaInfantilCalendario2023> getResultListClasificacionBenjamin() {
		if(resultListClasificacionBenjamin == null){
			resultListClasificacionBenjamin = (List<CampeonatoLigaInfantilCalendario2023>) leerCampeonatoLigaInfantil.listResultClasificacion(CategoriasInfantilEnumeration2023.CATEGORIA_BENJAMIN);
		}
		return resultListClasificacionBenjamin;
	}
	
	public List<CampeonatoLigaInfantilCalendario2023> getResultListClasificacionAlevin() {
		if(resultListClasificacionAlevin == null){
			resultListClasificacionAlevin = (List<CampeonatoLigaInfantilCalendario2023>) leerCampeonatoLigaInfantil.listResultClasificacion(CategoriasInfantilEnumeration2023.CATEGORIA_ALEVIN);
		}
		return resultListClasificacionAlevin;
	}
	
	public List<CampeonatoLigaInfantilCalendario2023> getResultListClasificacionInfantil() {
		if(resultListClasificacionInfantil == null){
			resultListClasificacionInfantil = (List<CampeonatoLigaInfantilCalendario2023>) leerCampeonatoLigaInfantil.listResultClasificacion(CategoriasInfantilEnumeration2023.CATEGORIA_INFANTIL);
		}
		return resultListClasificacionInfantil;
	}
	
	public List<CampeonatoLigaInfantilCalendario2023> getResultListClasificacionCadete() {
		if(resultListClasificacionCadete == null){
			resultListClasificacionCadete = (List<CampeonatoLigaInfantilCalendario2023>) leerCampeonatoLigaInfantil.listResultClasificacion(CategoriasInfantilEnumeration2023.CATEGORIA_CADETE);
		}
		return resultListClasificacionCadete;
	}
	
	public List<CampeonatoLigaInfantilCalendario2023> getResultListCalendario() {
		if(resultListCalendario == null){
			resultListCalendario = (List<CampeonatoLigaInfantilCalendario2023>) leerCampeonatoLigaInfantil.listResultCalendario();
		}
		return resultListCalendario;
	}

	public Object selected() {
		// TODO Auto-generated method stub
		return null;
	}

	public Object read(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	public String insert(Object insert) {
		// TODO Auto-generated method stub
		return null;
	}

	public String update(Object update) {
		// TODO Auto-generated method stub
		return null;
	}

	public String delete(Object delete) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Integer getRowsPaginatorClasificacion(){
		return getResultListCalendario().size()/14;
	}
	
	public Integer getRowsPaginatorCalendario(){
		return getResultListCalendario().size()/14;
	}
	
//	public Integer getTotalRowsClasificacion(){
//		return getResultListClasificacion().size();
//	}
	
	public String getPermisoActualizarResultado(){
    	if(sessionState != null && sessionState.getUserEquipo() != null && sessionState.getUserEquipo().getId().equals(0)){
			return "BCMEGEARLI";
		}
    	return "SIN PERMISO";
    }

	public String getPermisoConfirmarResultado() {
		if(sessionState != null && sessionState.getUserEquipo() != null && sessionState.getUserEquipo().getId().equals(0)){
			return "BCMEGEARLI";
		}
    	return "SIN PERMISO";
	}
		
    public void doActualizarResultadoLiga(CampeonatoLigaInfantilCalendario2023 cec){
    	boolean error = false;
		if(cec.isModificable()){
			//Validaciones
			if(cec.getRonda1() < 0 || cec.getRonda2() < 0 || cec.getRonda3() < 0 || cec.getRonda4() < 0 || cec.getPuntos() < 0){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Las rondas han de tener un valor igual o mayor de 0.", null));
			}
			
			//Si todo correcto actualizar resultado
			if(!error){
				campeonatoLigaInfantil.actualizarResultadoCalendarioLiga(cec);
				leerCampeonatoLigaInfantil = new LeerCampeonatoLigaInfantil2023();
				resultListClasificacionMini = (List<CampeonatoLigaInfantilCalendario2023>) leerCampeonatoLigaInfantil.listResultClasificacion(CategoriasInfantilEnumeration2023.CATEGORIA_MINI);
				resultListClasificacionBenjamin = (List<CampeonatoLigaInfantilCalendario2023>) leerCampeonatoLigaInfantil.listResultClasificacion(CategoriasInfantilEnumeration2023.CATEGORIA_BENJAMIN);
				resultListClasificacionAlevin = (List<CampeonatoLigaInfantilCalendario2023>) leerCampeonatoLigaInfantil.listResultClasificacion(CategoriasInfantilEnumeration2023.CATEGORIA_ALEVIN);
				resultListClasificacionInfantil = (List<CampeonatoLigaInfantilCalendario2023>) leerCampeonatoLigaInfantil.listResultClasificacion(CategoriasInfantilEnumeration2023.CATEGORIA_INFANTIL);
				resultListClasificacionCadete = (List<CampeonatoLigaInfantilCalendario2023>) leerCampeonatoLigaInfantil.listResultClasificacion(CategoriasInfantilEnumeration2023.CATEGORIA_CADETE);
				resultListCalendario = (List<CampeonatoLigaInfantilCalendario2023>) leerCampeonatoLigaInfantil.listResultCalendario();
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha actualizado el resultado de la jugadora " + cec.getNombre(), null));
			}
		}
		
	}
	    
		public void doConfirmarResultado(CampeonatoLigaInfantilCalendario2023 cec){
			boolean error = false;
			if(cec.isModificable()){
				//Validaciones
				if(cec.getRonda1() < 0 || cec.getRonda2() < 0 || cec.getRonda3() < 0 || cec.getRonda4() < 0 || cec.getPuntos() < 0){
					error = true;
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Las rondas han de tener un valor igual o mayor de 0.", null));
				}
				
				//Si todo correcto confirmar resultado y actualizar clasificacion
				if(!error){
					cec.setActivo(Activo2022.NO_NUMBER);
					campeonatoLigaInfantil.actualizarResultadoCalendarioLigaConfirmar(cec);
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha confirmado el resultado de la jugador " + cec.getNombre(), null));
					leerCampeonatoLigaInfantil = new LeerCampeonatoLigaInfantil2023();
					resultListClasificacionMini = (List<CampeonatoLigaInfantilCalendario2023>) leerCampeonatoLigaInfantil.listResultClasificacion(CategoriasInfantilEnumeration2023.CATEGORIA_MINI);
					resultListClasificacionBenjamin = (List<CampeonatoLigaInfantilCalendario2023>) leerCampeonatoLigaInfantil.listResultClasificacion(CategoriasInfantilEnumeration2023.CATEGORIA_BENJAMIN);
					resultListClasificacionAlevin = (List<CampeonatoLigaInfantilCalendario2023>) leerCampeonatoLigaInfantil.listResultClasificacion(CategoriasInfantilEnumeration2023.CATEGORIA_ALEVIN);
					resultListClasificacionInfantil = (List<CampeonatoLigaInfantilCalendario2023>) leerCampeonatoLigaInfantil.listResultClasificacion(CategoriasInfantilEnumeration2023.CATEGORIA_INFANTIL);
					resultListClasificacionCadete = (List<CampeonatoLigaInfantilCalendario2023>) leerCampeonatoLigaInfantil.listResultClasificacion(CategoriasInfantilEnumeration2023.CATEGORIA_CADETE);
					resultListCalendario = (List<CampeonatoLigaInfantilCalendario2023>) leerCampeonatoLigaInfantil.listResultCalendario();
				}
			}
			
			
		}		
		
		
		public void doCalcularClasificacionFinal(){
			
			actualizarClasificacionFinalMini();
			actualizarClasificacionFinalBenajamin();
			actualizarClasificacionFinalAlevin();
			actualizarClasificacionFinalInfantil();
			actualizarClasificacionFinalCadete();

		}

		private void actualizarClasificacionFinalMini() {
			//MINI
			List<CampeonatoLigaInfantilCalendario2023> resultListClasificacionMini = (List<CampeonatoLigaInfantilCalendario2023>) leerCampeonatoLigaInfantil.listResultCalendario();
			List<String> jugadoresMini = new ArrayList<String>();
			//OBTENER LA LISTA DE JUGADORES POR CATEGORIA
			for (CampeonatoLigaInfantilCalendario2023 campeonatoLigaInfantilCalendario2023 : resultListClasificacionMini) {
				if(campeonatoLigaInfantilCalendario2023.getCategoria().equals(CategoriasInfantilEnumeration2023.CATEGORIA_MINI)){
					if(!jugadoresMini.contains(campeonatoLigaInfantilCalendario2023.getNombre())){
						jugadoresMini.add(campeonatoLigaInfantilCalendario2023.getNombre());
					}
				}
			}
			//OBTENER LAS CLASIFICACIONES DE CADA JUGADOR
			for (String jugador: jugadoresMini) {
				List<CampeonatoLigaInfantilCalendario2023> jornadasJugador = new ArrayList<>();
				for(CampeonatoLigaInfantilCalendario2023 jornadasTodos : resultListClasificacionMini){
					if(jornadasTodos.getNombre().equals(jugador)){
						jornadasJugador.add(jornadasTodos);
					}
				}
				
				// ORDENAR LA LISTA POR POSICION Y PUNTOS DE CADA JUGADOR
				Collections.sort(jornadasJugador, new Comparator() {
					@Override
					public int compare(Object o1, Object o2) {
						CampeonatoLigaInfantilCalendario2023 cec1 = (CampeonatoLigaInfantilCalendario2023) o1;
						CampeonatoLigaInfantilCalendario2023 cec2 = (CampeonatoLigaInfantilCalendario2023) o2;
						
						int rpuntos = cec1.getPuntos().compareTo(cec2.getPuntos());
						if (rpuntos == 0) {
							Integer part1 = cec1.getTotalTantos();
							Integer part2 = cec2.getTotalTantos();
							int rdifpartidas = part1.compareTo(part2);
							if (rdifpartidas == 0) {
								Integer jornada1 = cec1.getJornada();
								Integer jornada2 = cec2.getJornada();
								int rdifJornada = jornada1.compareTo(jornada2);
								return rdifJornada;
							}
						}
						return rpuntos;
					}
				});
				
				//RECORRER LA LISTA ACTUALIZADA Y PONER A 1 CLASIFICACION FINAL
				int cont = 0;
				for (CampeonatoLigaInfantilCalendario2023 campeonatoLigaInfantilCalendario2023 : jornadasJugador) {
					if(cont > 5){
						campeonatoLigaInfantilCalendario2023.setContarFinal(true);
						cont++;
						campeonatoLigaInfantil.actualizarClasificacionFinal(campeonatoLigaInfantilCalendario2023);
					}else{
						cont++;
					}
				}
				
			}
		}
		
		private void actualizarClasificacionFinalBenajamin() {
			//MINI
			List<CampeonatoLigaInfantilCalendario2023> resultListClasificacionMini = (List<CampeonatoLigaInfantilCalendario2023>) leerCampeonatoLigaInfantil.listResultCalendario();
			List<String> jugadoresMini = new ArrayList<String>();
			//OBTENER LA LISTA DE JUGADORES POR CATEGORIA
			for (CampeonatoLigaInfantilCalendario2023 campeonatoLigaInfantilCalendario2023 : resultListClasificacionMini) {
				if(campeonatoLigaInfantilCalendario2023.getCategoria().equals(CategoriasInfantilEnumeration2023.CATEGORIA_BENJAMIN)){
					if(!jugadoresMini.contains(campeonatoLigaInfantilCalendario2023.getNombre())){
						jugadoresMini.add(campeonatoLigaInfantilCalendario2023.getNombre());
					}
				}
			}
			//OBTENER LAS CLASIFICACIONES DE CADA JUGADOR
			for (String jugador: jugadoresMini) {
				List<CampeonatoLigaInfantilCalendario2023> jornadasJugador = new ArrayList<>();
				for(CampeonatoLigaInfantilCalendario2023 jornadasTodos : resultListClasificacionMini){
					if(jornadasTodos.getNombre().equals(jugador)){
						jornadasJugador.add(jornadasTodos);
					}
				}
				
				// ORDENAR LA LISTA POR POSICION Y PUNTOS DE CADA JUGADOR
				Collections.sort(jornadasJugador, new Comparator() {
					@Override
					public int compare(Object o1, Object o2) {
						CampeonatoLigaInfantilCalendario2023 cec1 = (CampeonatoLigaInfantilCalendario2023) o1;
						CampeonatoLigaInfantilCalendario2023 cec2 = (CampeonatoLigaInfantilCalendario2023) o2;
						
						int rpuntos = cec1.getPuntos().compareTo(cec2.getPuntos());
						if (rpuntos == 0) {
							Integer part1 = cec1.getTotalTantos();
							Integer part2 = cec2.getTotalTantos();
							int rdifpartidas = part1.compareTo(part2);
							if (rdifpartidas == 0) {
								Integer jornada1 = cec1.getJornada();
								Integer jornada2 = cec2.getJornada();
								int rdifJornada = jornada1.compareTo(jornada2);
								return rdifJornada;
							}
						}
						return rpuntos;
					}
				});
				
				//RECORRER LA LISTA ACTUALIZADA Y PONER A 1 CLASIFICACION FINAL
				int cont = 0;
				for (CampeonatoLigaInfantilCalendario2023 campeonatoLigaInfantilCalendario2023 : jornadasJugador) {
					if(cont > 5){
						campeonatoLigaInfantilCalendario2023.setContarFinal(true);
						cont++;
						campeonatoLigaInfantil.actualizarClasificacionFinal(campeonatoLigaInfantilCalendario2023);
					}else{
						cont++;
					}
				}
				
			}
		}
		
		private void actualizarClasificacionFinalAlevin() {
			//MINI
			List<CampeonatoLigaInfantilCalendario2023> resultListClasificacionMini = (List<CampeonatoLigaInfantilCalendario2023>) leerCampeonatoLigaInfantil.listResultCalendario();
			List<String> jugadoresMini = new ArrayList<String>();
			//OBTENER LA LISTA DE JUGADORES POR CATEGORIA
			for (CampeonatoLigaInfantilCalendario2023 campeonatoLigaInfantilCalendario2023 : resultListClasificacionMini) {
				if(campeonatoLigaInfantilCalendario2023.getCategoria().equals(CategoriasInfantilEnumeration2023.CATEGORIA_ALEVIN)){
					if(!jugadoresMini.contains(campeonatoLigaInfantilCalendario2023.getNombre())){
						jugadoresMini.add(campeonatoLigaInfantilCalendario2023.getNombre());
					}
				}
			}
			//OBTENER LAS CLASIFICACIONES DE CADA JUGADOR
			for (String jugador: jugadoresMini) {
				List<CampeonatoLigaInfantilCalendario2023> jornadasJugador = new ArrayList<>();
				for(CampeonatoLigaInfantilCalendario2023 jornadasTodos : resultListClasificacionMini){
					if(jornadasTodos.getNombre().equals(jugador)){
						jornadasJugador.add(jornadasTodos);
					}
				}
				
				// ORDENAR LA LISTA POR POSICION Y PUNTOS DE CADA JUGADOR
				Collections.sort(jornadasJugador, new Comparator() {
					@Override
					public int compare(Object o1, Object o2) {
						CampeonatoLigaInfantilCalendario2023 cec1 = (CampeonatoLigaInfantilCalendario2023) o1;
						CampeonatoLigaInfantilCalendario2023 cec2 = (CampeonatoLigaInfantilCalendario2023) o2;
						
						int rpuntos = cec1.getPuntos().compareTo(cec2.getPuntos());
						if (rpuntos == 0) {
							Integer part1 = cec1.getTotalTantos();
							Integer part2 = cec2.getTotalTantos();
							int rdifpartidas = part1.compareTo(part2);
							if (rdifpartidas == 0) {
								Integer jornada1 = cec1.getJornada();
								Integer jornada2 = cec2.getJornada();
								int rdifJornada = jornada1.compareTo(jornada2);
								return rdifJornada;
							}
						}
						return rpuntos;
					}
				});
				
				//RECORRER LA LISTA ACTUALIZADA Y PONER A 1 CLASIFICACION FINAL
				int cont = 0;
				for (CampeonatoLigaInfantilCalendario2023 campeonatoLigaInfantilCalendario2023 : jornadasJugador) {
					if(cont > 5){
						campeonatoLigaInfantilCalendario2023.setContarFinal(true);
						cont++;
						campeonatoLigaInfantil.actualizarClasificacionFinal(campeonatoLigaInfantilCalendario2023);
					}else{
						cont++;
					}
				}
				
			}
		}
		
		private void actualizarClasificacionFinalInfantil() {
			//MINI
			List<CampeonatoLigaInfantilCalendario2023> resultListClasificacionMini = (List<CampeonatoLigaInfantilCalendario2023>) leerCampeonatoLigaInfantil.listResultCalendario();
			List<String> jugadoresMini = new ArrayList<String>();
			//OBTENER LA LISTA DE JUGADORES POR CATEGORIA
			for (CampeonatoLigaInfantilCalendario2023 campeonatoLigaInfantilCalendario2023 : resultListClasificacionMini) {
				if(campeonatoLigaInfantilCalendario2023.getCategoria().equals(CategoriasInfantilEnumeration2023.CATEGORIA_INFANTIL)){
					if(!jugadoresMini.contains(campeonatoLigaInfantilCalendario2023.getNombre())){
						jugadoresMini.add(campeonatoLigaInfantilCalendario2023.getNombre());
					}
				}
			}
			//OBTENER LAS CLASIFICACIONES DE CADA JUGADOR
			for (String jugador: jugadoresMini) {
				List<CampeonatoLigaInfantilCalendario2023> jornadasJugador = new ArrayList<>();
				for(CampeonatoLigaInfantilCalendario2023 jornadasTodos : resultListClasificacionMini){
					if(jornadasTodos.getNombre().equals(jugador)){
						jornadasJugador.add(jornadasTodos);
					}
				}
				
				// ORDENAR LA LISTA POR POSICION Y PUNTOS DE CADA JUGADOR
				Collections.sort(jornadasJugador, new Comparator() {
					@Override
					public int compare(Object o1, Object o2) {
						CampeonatoLigaInfantilCalendario2023 cec1 = (CampeonatoLigaInfantilCalendario2023) o1;
						CampeonatoLigaInfantilCalendario2023 cec2 = (CampeonatoLigaInfantilCalendario2023) o2;
						
						int rpuntos = cec1.getPuntos().compareTo(cec2.getPuntos());
						if (rpuntos == 0) {
							Integer part1 = cec1.getTotalTantos();
							Integer part2 = cec2.getTotalTantos();
							int rdifpartidas = part1.compareTo(part2);
							if (rdifpartidas == 0) {
								Integer jornada1 = cec1.getJornada();
								Integer jornada2 = cec2.getJornada();
								int rdifJornada = jornada1.compareTo(jornada2);
								return rdifJornada;
							}
						}
						return rpuntos;
					}
				});
				
				//RECORRER LA LISTA ACTUALIZADA Y PONER A 1 CLASIFICACION FINAL
				int cont = 0;
				for (CampeonatoLigaInfantilCalendario2023 campeonatoLigaInfantilCalendario2023 : jornadasJugador) {
					if(cont > 5){
						campeonatoLigaInfantilCalendario2023.setContarFinal(true);
						cont++;
						campeonatoLigaInfantil.actualizarClasificacionFinal(campeonatoLigaInfantilCalendario2023);
					}else{
						cont++;
					}
				}
				
			}
		}
		
		private void actualizarClasificacionFinalCadete() {
			//MINI
			List<CampeonatoLigaInfantilCalendario2023> resultListClasificacionMini = (List<CampeonatoLigaInfantilCalendario2023>) leerCampeonatoLigaInfantil.listResultCalendario();
			List<String> jugadoresMini = new ArrayList<String>();
			//OBTENER LA LISTA DE JUGADORES POR CATEGORIA
			for (CampeonatoLigaInfantilCalendario2023 campeonatoLigaInfantilCalendario2023 : resultListClasificacionMini) {
				if(campeonatoLigaInfantilCalendario2023.getCategoria().equals(CategoriasInfantilEnumeration2023.CATEGORIA_CADETE)){
					if(!jugadoresMini.contains(campeonatoLigaInfantilCalendario2023.getNombre())){
						jugadoresMini.add(campeonatoLigaInfantilCalendario2023.getNombre());
					}
				}
			}
			//OBTENER LAS CLASIFICACIONES DE CADA JUGADOR
			for (String jugador: jugadoresMini) {
				List<CampeonatoLigaInfantilCalendario2023> jornadasJugador = new ArrayList<>();
				for(CampeonatoLigaInfantilCalendario2023 jornadasTodos : resultListClasificacionMini){
					if(jornadasTodos.getNombre().equals(jugador)){
						jornadasJugador.add(jornadasTodos);
					}
				}
				
				// ORDENAR LA LISTA POR POSICION Y PUNTOS DE CADA JUGADOR
				Collections.sort(jornadasJugador, new Comparator() {
					@Override
					public int compare(Object o1, Object o2) {
						CampeonatoLigaInfantilCalendario2023 cec1 = (CampeonatoLigaInfantilCalendario2023) o1;
						CampeonatoLigaInfantilCalendario2023 cec2 = (CampeonatoLigaInfantilCalendario2023) o2;
						
						int rpuntos = cec1.getPuntos().compareTo(cec2.getPuntos());
						if (rpuntos == 0) {
							Integer part1 = cec1.getTotalTantos();
							Integer part2 = cec2.getTotalTantos();
							int rdifpartidas = part1.compareTo(part2);
							if (rdifpartidas == 0) {
								Integer jornada1 = cec1.getJornada();
								Integer jornada2 = cec2.getJornada();
								int rdifJornada = jornada1.compareTo(jornada2);
								return rdifJornada;
							}
						}
						return rpuntos;
					}
				});
				
				//RECORRER LA LISTA ACTUALIZADA Y PONER A 1 CLASIFICACION FINAL
				int cont = 0;
				for (CampeonatoLigaInfantilCalendario2023 campeonatoLigaInfantilCalendario2023 : jornadasJugador) {
					if(cont > 5){
						campeonatoLigaInfantilCalendario2023.setContarFinal(true);
						cont++;
						campeonatoLigaInfantil.actualizarClasificacionFinal(campeonatoLigaInfantilCalendario2023);
					}else{
						cont++;
					}
				}
				
			}
		}

}
