package com.bolocelta.facade;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;

import com.bolocelta.bbdd.readTables.LeerCalendarioAsturias2022;
import com.bolocelta.entities.CalendarioAsturias2022;

@Named
@ConversationScoped
@ManagedBean
public class CalendarioAsturiasFacade2022 implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private LeerCalendarioAsturias2022 leerCalendarioAsturias = new LeerCalendarioAsturias2022();
	
	private List<CalendarioAsturias2022> resultList = null;

	public List<CalendarioAsturias2022> getResultList() {
		if(resultList == null){
			resultList = (List<CalendarioAsturias2022>) leerCalendarioAsturias.listResult();
		}
		return resultList;
	}

	public Object selected() {
		// TODO Auto-generated method stub
		return null;
	}

	public Object read(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	public String insert(Object insert) {
		// TODO Auto-generated method stub
		return null;
	}

	public String update(Object update) {
		// TODO Auto-generated method stub
		return null;
	}

	public String delete(Object delete) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Integer getMaxRowsPaginator(){
		return 10;
	}
	
	public boolean isEnabledPaginator(){
		return getResultList() != null ? (getResultList().size() > getMaxRowsPaginator()) : false;
	}	
	
	

}
