package com.bolocelta.facade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.inject.Named;

import com.bolocelta.application.session.SessionState;
import com.bolocelta.bbdd.readTables.LeerHistoricoCampeonatosAsturias2022;
import com.bolocelta.entities.HistoricoCampeonatosAsturias2022;

@Named
@ConversationScoped
@ManagedBean
public class HistoriaCampeonatoAsturias2022 implements Serializable {
	
	@Inject
	private SessionState sessionState;
	
	private static final long serialVersionUID = 1L;
	
	private LeerHistoricoCampeonatosAsturias2022 leerHistoricoCampeonatosAsturias = new LeerHistoricoCampeonatosAsturias2022();
	
	private List<HistoricoCampeonatosAsturias2022> resultListHistoricoCampeonatoAsturias = null;
	private List<HistoricoCampeonatosAsturias2022> resultListHistoricoCampeonatoAsturiasEquipos = null;
	private List<HistoricoCampeonatosAsturias2022> resultListHistoricoCampeonatoAsturiasIndividual = null;
	private List<HistoricoCampeonatosAsturias2022> resultListHistoricoCampeonatoAsturiasParejas = null;
	private List<HistoricoCampeonatosAsturias2022> resultListHistoricoCampeonatoAsturiasIndividualFemenino = null;
	private List<HistoricoCampeonatosAsturias2022> resultListHistoricoCampeonatoAsturiasParejasFemenino = null;
	private List<HistoricoCampeonatosAsturias2022> resultListHistoricoCampeonatoAsturiasMixto = null;
	private List<HistoricoCampeonatosAsturias2022> resultListHistoricoCampeonatoAsturiasVeteranos = null;
	private List<HistoricoCampeonatosAsturias2022> resultListHistoricoCampeonatoAsturiasJuveniles = null;
	private List<HistoricoCampeonatosAsturias2022> resultListHistoricoCampeonatoAsturiasCadetes = null;
	private List<HistoricoCampeonatosAsturias2022> resultListHistoricoCampeonatoAsturiasParejasJuveniles = null;
	private List<HistoricoCampeonatosAsturias2022> resultListHistoricoCampeonatoAsturiasParejasCadetes = null;

	
	private List<Integer> resultListAnyoFasesTabShow = null;
	
	public List<HistoricoCampeonatosAsturias2022> getResultListHistoricoCampeonatoAsturiasAll() {
		if(resultListHistoricoCampeonatoAsturias == null){
			resultListHistoricoCampeonatoAsturias = (List<HistoricoCampeonatosAsturias2022>) leerHistoricoCampeonatosAsturias.listResult();
		}
		return resultListHistoricoCampeonatoAsturias;
	}
	
	public List<HistoricoCampeonatosAsturias2022> getResultListHistoricoCampeonatoAsturiasEquipos() {
		if(resultListHistoricoCampeonatoAsturiasEquipos == null){
			resultListHistoricoCampeonatoAsturiasEquipos = (List<HistoricoCampeonatosAsturias2022>) leerHistoricoCampeonatosAsturias.listResultEquipos();
		}
		return resultListHistoricoCampeonatoAsturiasEquipos;
	}

	public List<HistoricoCampeonatosAsturias2022> getResultListHistoricoCampeonatoAsturiasIndividual() {
		if(resultListHistoricoCampeonatoAsturiasIndividual == null){
			resultListHistoricoCampeonatoAsturiasIndividual = (List<HistoricoCampeonatosAsturias2022>) leerHistoricoCampeonatosAsturias.listResultIndividual();
		}
		return resultListHistoricoCampeonatoAsturiasIndividual;
	}
	
	public List<HistoricoCampeonatosAsturias2022> getResultListHistoricoCampeonatoAsturiasParejas() {
		if(resultListHistoricoCampeonatoAsturiasParejas == null){
			resultListHistoricoCampeonatoAsturiasParejas = (List<HistoricoCampeonatosAsturias2022>) leerHistoricoCampeonatosAsturias.listResultParejas();
		}
		return resultListHistoricoCampeonatoAsturiasParejas;
	}
	
	public List<HistoricoCampeonatosAsturias2022> getResultListHistoricoCampeonatoAsturiasIndividualFemenino() {
		if(resultListHistoricoCampeonatoAsturiasIndividualFemenino == null){
			resultListHistoricoCampeonatoAsturiasIndividualFemenino = (List<HistoricoCampeonatosAsturias2022>) leerHistoricoCampeonatosAsturias.listResultIndividualFemenino();
		}
		return resultListHistoricoCampeonatoAsturiasIndividualFemenino;
	}
	
	public List<HistoricoCampeonatosAsturias2022> getResultListHistoricoCampeonatoAsturiasParejasFemenino() {
		if(resultListHistoricoCampeonatoAsturiasParejasFemenino == null){
			resultListHistoricoCampeonatoAsturiasParejasFemenino = (List<HistoricoCampeonatosAsturias2022>) leerHistoricoCampeonatosAsturias.listResultParejasFemenino();
		}
		return resultListHistoricoCampeonatoAsturiasParejasFemenino;
	}
	
	public List<HistoricoCampeonatosAsturias2022> getResultListHistoricoCampeonatoAsturiasMixto() {
		if(resultListHistoricoCampeonatoAsturiasMixto == null){
			resultListHistoricoCampeonatoAsturiasMixto = (List<HistoricoCampeonatosAsturias2022>) leerHistoricoCampeonatosAsturias.listResultMixto();
		}
		return resultListHistoricoCampeonatoAsturiasMixto;
	}
	
	public List<HistoricoCampeonatosAsturias2022> getResultListHistoricoCampeonatoAsturiasVeteranos() {
		if(resultListHistoricoCampeonatoAsturiasVeteranos == null){
			resultListHistoricoCampeonatoAsturiasVeteranos = (List<HistoricoCampeonatosAsturias2022>) leerHistoricoCampeonatosAsturias.listResultVeteranos();
		}
		return resultListHistoricoCampeonatoAsturiasVeteranos;
	}
	
	public List<HistoricoCampeonatosAsturias2022> getResultListHistoricoCampeonatoAsturiasJuveniles() {
		if(resultListHistoricoCampeonatoAsturiasJuveniles == null){
			resultListHistoricoCampeonatoAsturiasJuveniles = (List<HistoricoCampeonatosAsturias2022>) leerHistoricoCampeonatosAsturias.listResultJuveniles();
		}
		return resultListHistoricoCampeonatoAsturiasJuveniles;
	}
	
	public List<HistoricoCampeonatosAsturias2022> getResultListHistoricoCampeonatoAsturiasParejasJuveniles() {
		if(resultListHistoricoCampeonatoAsturiasParejasJuveniles == null){
			resultListHistoricoCampeonatoAsturiasParejasJuveniles = (List<HistoricoCampeonatosAsturias2022>) leerHistoricoCampeonatosAsturias.listResultParejasJuveniles();
		}
		return resultListHistoricoCampeonatoAsturiasParejasJuveniles;
	}
	
	public List<HistoricoCampeonatosAsturias2022> getResultListHistoricoCampeonatoAsturiasCadetes() {
		if(resultListHistoricoCampeonatoAsturiasCadetes == null){
			resultListHistoricoCampeonatoAsturiasCadetes = (List<HistoricoCampeonatosAsturias2022>) leerHistoricoCampeonatosAsturias.listResultCadetes();
		}
		return resultListHistoricoCampeonatoAsturiasCadetes;
	}
	
	public List<HistoricoCampeonatosAsturias2022> getResultListHistoricoCampeonatoAsturiasParejasCadetes() {
		if(resultListHistoricoCampeonatoAsturiasParejasCadetes == null){
			resultListHistoricoCampeonatoAsturiasParejasCadetes = (List<HistoricoCampeonatosAsturias2022>) leerHistoricoCampeonatosAsturias.listResultParejasCadetes();
		}
		return resultListHistoricoCampeonatoAsturiasParejasCadetes;
	}
	
	
	public List<HistoricoCampeonatosAsturias2022> getResultListHistoricoCampeonatoAsturiasEquiposByAnyo(Integer anyo) {
		List<HistoricoCampeonatosAsturias2022> result = new ArrayList<HistoricoCampeonatosAsturias2022>();
		for (HistoricoCampeonatosAsturias2022 historicoCampeonatosAsturias : getResultListHistoricoCampeonatoAsturiasEquipos()) {
			if(historicoCampeonatosAsturias.getAnyo().equals(anyo)){
				result.add(historicoCampeonatosAsturias);
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(result, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				HistoricoCampeonatosAsturias2022 cec1 = (HistoricoCampeonatosAsturias2022) o1;
				HistoricoCampeonatosAsturias2022 cec2 = (HistoricoCampeonatosAsturias2022) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPosicion().compareTo(cec2.getPosicion());
					return rpuntos;
			}
		});
		
		return result;
	}
	
	public List<HistoricoCampeonatosAsturias2022> getResultListHistoricoCampeonatoAsturiasIndividualByAnyo(Integer anyo) {
		List<HistoricoCampeonatosAsturias2022> result = new ArrayList<HistoricoCampeonatosAsturias2022>();
		for (HistoricoCampeonatosAsturias2022 historicoCampeonatosAsturias : getResultListHistoricoCampeonatoAsturiasIndividual()) {
			if(historicoCampeonatosAsturias.getAnyo().equals(anyo)){
				result.add(historicoCampeonatosAsturias);
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(result, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				HistoricoCampeonatosAsturias2022 cec1 = (HistoricoCampeonatosAsturias2022) o1;
				HistoricoCampeonatosAsturias2022 cec2 = (HistoricoCampeonatosAsturias2022) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPosicion().compareTo(cec2.getPosicion());
					return rpuntos;
			}
		});
		
		return result;
	}
	
	public List<HistoricoCampeonatosAsturias2022> getResultListHistoricoCampeonatoAsturiasParejasByAnyo(Integer anyo) {
		List<HistoricoCampeonatosAsturias2022> result = new ArrayList<HistoricoCampeonatosAsturias2022>();
		for (HistoricoCampeonatosAsturias2022 historicoCampeonatosAsturias : getResultListHistoricoCampeonatoAsturiasParejas()) {
			if(historicoCampeonatosAsturias.getAnyo().equals(anyo)){
				result.add(historicoCampeonatosAsturias);
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(result, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				HistoricoCampeonatosAsturias2022 cec1 = (HistoricoCampeonatosAsturias2022) o1;
				HistoricoCampeonatosAsturias2022 cec2 = (HistoricoCampeonatosAsturias2022) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPosicion().compareTo(cec2.getPosicion());
					return rpuntos;
			}
		});
		
		return result;
	}
	

	public List<HistoricoCampeonatosAsturias2022> getResultListHistoricoCampeonatoAsturiasIndividualFemeninoByAnyo(Integer anyo) {
		List<HistoricoCampeonatosAsturias2022> result = new ArrayList<HistoricoCampeonatosAsturias2022>();
		for (HistoricoCampeonatosAsturias2022 historicoCampeonatosAsturias : getResultListHistoricoCampeonatoAsturiasIndividualFemenino()) {
			if(historicoCampeonatosAsturias.getAnyo().equals(anyo)){
				result.add(historicoCampeonatosAsturias);
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(result, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				HistoricoCampeonatosAsturias2022 cec1 = (HistoricoCampeonatosAsturias2022) o1;
				HistoricoCampeonatosAsturias2022 cec2 = (HistoricoCampeonatosAsturias2022) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPosicion().compareTo(cec2.getPosicion());
					return rpuntos;
			}
		});
		
		return result;
	}
	
	public List<HistoricoCampeonatosAsturias2022> getResultListHistoricoCampeonatoAsturiasParejasFemeninoByAnyo(Integer anyo) {
		List<HistoricoCampeonatosAsturias2022> result = new ArrayList<HistoricoCampeonatosAsturias2022>();
		for (HistoricoCampeonatosAsturias2022 historicoCampeonatosAsturias : getResultListHistoricoCampeonatoAsturiasParejasFemenino()) {
			if(historicoCampeonatosAsturias.getAnyo().equals(anyo)){
				result.add(historicoCampeonatosAsturias);
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(result, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				HistoricoCampeonatosAsturias2022 cec1 = (HistoricoCampeonatosAsturias2022) o1;
				HistoricoCampeonatosAsturias2022 cec2 = (HistoricoCampeonatosAsturias2022) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPosicion().compareTo(cec2.getPosicion());
					return rpuntos;
			}
		});
		
		return result;
	}
	
	public List<HistoricoCampeonatosAsturias2022> getResultListHistoricoCampeonatoAsturiasMixtoByAnyo(Integer anyo) {
		List<HistoricoCampeonatosAsturias2022> result = new ArrayList<HistoricoCampeonatosAsturias2022>();
		for (HistoricoCampeonatosAsturias2022 historicoCampeonatosAsturias : getResultListHistoricoCampeonatoAsturiasMixto()) {
			if(historicoCampeonatosAsturias.getAnyo().equals(anyo)){
				result.add(historicoCampeonatosAsturias);
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(result, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				HistoricoCampeonatosAsturias2022 cec1 = (HistoricoCampeonatosAsturias2022) o1;
				HistoricoCampeonatosAsturias2022 cec2 = (HistoricoCampeonatosAsturias2022) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPosicion().compareTo(cec2.getPosicion());
					return rpuntos;
			}
		});
		
		return result;
	}
	
	public List<HistoricoCampeonatosAsturias2022> getResultListHistoricoCampeonatoAsturiasVeteranosByAnyo(Integer anyo) {
		List<HistoricoCampeonatosAsturias2022> result = new ArrayList<HistoricoCampeonatosAsturias2022>();
		for (HistoricoCampeonatosAsturias2022 historicoCampeonatosAsturias : getResultListHistoricoCampeonatoAsturiasVeteranos()) {
			if(historicoCampeonatosAsturias.getAnyo().equals(anyo)){
				result.add(historicoCampeonatosAsturias);
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(result, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				HistoricoCampeonatosAsturias2022 cec1 = (HistoricoCampeonatosAsturias2022) o1;
				HistoricoCampeonatosAsturias2022 cec2 = (HistoricoCampeonatosAsturias2022) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPosicion().compareTo(cec2.getPosicion());
					return rpuntos;
			}
		});
		
		return result;
	}
	
	public List<HistoricoCampeonatosAsturias2022> getResultListHistoricoCampeonatoAsturiasParejasJuvenilesByAnyo(Integer anyo) {
		List<HistoricoCampeonatosAsturias2022> result = new ArrayList<HistoricoCampeonatosAsturias2022>();
		for (HistoricoCampeonatosAsturias2022 historicoCampeonatosAsturias : getResultListHistoricoCampeonatoAsturiasParejasJuveniles()) {
			if(historicoCampeonatosAsturias.getAnyo().equals(anyo)){
				result.add(historicoCampeonatosAsturias);
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(result, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				HistoricoCampeonatosAsturias2022 cec1 = (HistoricoCampeonatosAsturias2022) o1;
				HistoricoCampeonatosAsturias2022 cec2 = (HistoricoCampeonatosAsturias2022) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPosicion().compareTo(cec2.getPosicion());
					return rpuntos;
			}
		});
		
		return result;
	}
	
	public List<HistoricoCampeonatosAsturias2022> getResultListHistoricoCampeonatoAsturiasJuvenilesByAnyo(Integer anyo) {
		List<HistoricoCampeonatosAsturias2022> result = new ArrayList<HistoricoCampeonatosAsturias2022>();
		for (HistoricoCampeonatosAsturias2022 historicoCampeonatosAsturias : getResultListHistoricoCampeonatoAsturiasJuveniles()) {
			if(historicoCampeonatosAsturias.getAnyo().equals(anyo)){
				result.add(historicoCampeonatosAsturias);
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(result, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				HistoricoCampeonatosAsturias2022 cec1 = (HistoricoCampeonatosAsturias2022) o1;
				HistoricoCampeonatosAsturias2022 cec2 = (HistoricoCampeonatosAsturias2022) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPosicion().compareTo(cec2.getPosicion());
					return rpuntos;
			}
		});
		
		return result;
	}
	
	public List<HistoricoCampeonatosAsturias2022> getResultListHistoricoCampeonatoAsturiasCadetesByAnyo(Integer anyo) {
		List<HistoricoCampeonatosAsturias2022> result = new ArrayList<HistoricoCampeonatosAsturias2022>();
		for (HistoricoCampeonatosAsturias2022 historicoCampeonatosAsturias : getResultListHistoricoCampeonatoAsturiasCadetes()) {
			if(historicoCampeonatosAsturias.getAnyo().equals(anyo)){
				result.add(historicoCampeonatosAsturias);
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(result, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				HistoricoCampeonatosAsturias2022 cec1 = (HistoricoCampeonatosAsturias2022) o1;
				HistoricoCampeonatosAsturias2022 cec2 = (HistoricoCampeonatosAsturias2022) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPosicion().compareTo(cec2.getPosicion());
					return rpuntos;
			}
		});
		
		return result;
	}
	
	public List<HistoricoCampeonatosAsturias2022> getResultListHistoricoCampeonatoAsturiasParejasCadetesByAnyo(Integer anyo) {
		List<HistoricoCampeonatosAsturias2022> result = new ArrayList<HistoricoCampeonatosAsturias2022>();
		for (HistoricoCampeonatosAsturias2022 historicoCampeonatosAsturias : getResultListHistoricoCampeonatoAsturiasParejasCadetes()) {
			if(historicoCampeonatosAsturias.getAnyo().equals(anyo)){
				result.add(historicoCampeonatosAsturias);
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(result, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				HistoricoCampeonatosAsturias2022 cec1 = (HistoricoCampeonatosAsturias2022) o1;
				HistoricoCampeonatosAsturias2022 cec2 = (HistoricoCampeonatosAsturias2022) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPosicion().compareTo(cec2.getPosicion());
					return rpuntos;
			}
		});
		
		return result;
	}
	

	
	public boolean isDataCampeonato() {
		if(getResultListAnyoTabShow().size() > 0){
			return true;
		}
		return false;
	}
	
	public List<Integer> getResultListAnyoTabShow() {
		if(resultListAnyoFasesTabShow == null){
			resultListAnyoFasesTabShow = new ArrayList<>();
			
			for (HistoricoCampeonatosAsturias2022 historicoCampeonatosAsturias : getResultListHistoricoCampeonatoAsturiasAll()) {
				if(!resultListAnyoFasesTabShow.contains(historicoCampeonatosAsturias.getAnyo())){
					resultListAnyoFasesTabShow.add(historicoCampeonatosAsturias.getAnyo());
				}
			}
			
		}
		
		//Ordenar la clasificacion
		Collections.sort(resultListAnyoFasesTabShow, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				Integer cec1 = (Integer) o1;
				Integer cec2 = (Integer) o2;
				//1. Ordenar por anyo descendente 
				int rpuntos = cec1.compareTo(cec2);
					return rpuntos;
			}
		});
		
		Collections.reverse(resultListAnyoFasesTabShow);
		
		return resultListAnyoFasesTabShow;
	}

	public Object selected() {
		// TODO Auto-generated method stub
		return null;
	}

	public Object read(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	public String insert(Object insert) {
		// TODO Auto-generated method stub
		return null;
	}

	public String update(Object update) {
		// TODO Auto-generated method stub
		return null;
	}

	public String delete(Object delete) {
		// TODO Auto-generated method stub
		return null;
	}
		

}
