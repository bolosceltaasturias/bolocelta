package com.bolocelta.facade.add;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.bolocelta.application.MenuView;
import com.bolocelta.application.Redirect;
import com.bolocelta.application.utils.DateUtils2022;
import com.bolocelta.application.utils.NumberUtils2022;
import com.bolocelta.bbdd.constants.Activo2022;
import com.bolocelta.bbdd.constants.EstadosIndividualParejas2022;
import com.bolocelta.bbdd.constants.FasesModelo2022;
import com.bolocelta.bbdd.constants.GruposLetra2022;
import com.bolocelta.bbdd.constants.TipoEnfrentamiento2022;
import com.bolocelta.bbdd.constants.structure.EstructuraCampeonatoMasculinoParejasTercera2022;
import com.bolocelta.bbdd.createTable.CrearCampeonatoMasculinoParejasTercera2022;
import com.bolocelta.bbdd.readTables.LeerBoleras2022;
import com.bolocelta.bbdd.readTables.LeerCampeonatoMasculinoParejasPrimera2022;
import com.bolocelta.bbdd.readTables.LeerCampeonatoMasculinoParejasSegunda2022;
import com.bolocelta.bbdd.readTables.LeerCampeonatoMasculinoParejasTercera2022;
import com.bolocelta.entities.Boleras2022;
import com.bolocelta.entities.Categorias2022;
import com.bolocelta.entities.Configuracion2022;
import com.bolocelta.entities.Fases2022;
import com.bolocelta.entities.Jugadores2022;
import com.bolocelta.entities.Parejas2022;
import com.bolocelta.entities.ParticipantesParejas2022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasCuartosFinal2Grupos2022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasCuartosFinal4Grupos2022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasCuartosFinal4OF2022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasEnfrentamientoCuartosFinal2022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasEnfrentamientoFinal2022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasEnfrentamientoFinalConsolacion2022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasEnfrentamientoGruposFaseI2022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasEnfrentamientoOctavosFinal2022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasEnfrentamientoSemiFinal2022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasFinal4SF2022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasFinalConsolacion4SF2022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasGrupos32022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasGrupos42022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasGrupos52022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasGrupos62022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasGrupos72022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasGrupos82022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasOctavosFinal4Grupos2022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasOctavosFinal8Grupos2022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloParejasSemiFinal4CF2022;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseCF2022;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseFC2022;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseFF2022;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseI2022;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseII2022;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseOF2022;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseSF2022;
import com.bolocelta.entities.sorteos.parejas.ClasificacionFaseI2022;
import com.bolocelta.entities.sorteos.parejas.ClasificacionFaseII2022;
import com.bolocelta.entities.sorteos.parejas.Grupos2022;
import com.bolocelta.facade.edit.FichaEquipoFacade2022;

@Named
@ManagedBean
@RequestScoped
public class CampeonatoMasculinoParejasTerceraAddFacade2022 implements Serializable{
	
	private static final int PAREJA_EQUIPOS_MIXTO = 0;

	@Inject
	private Redirect redirect;
	
	@Inject
	private MenuView menuView;
	
	@Inject
	private FichaEquipoFacade2022 fichaEquipoFacade;
	
	private static final long serialVersionUID = 1L;
	
	private LeerCampeonatoMasculinoParejasPrimera2022 leerCampeonatoMasculinoParejasPrimera = new LeerCampeonatoMasculinoParejasPrimera2022();
	private CrearCampeonatoMasculinoParejasTercera2022 crearCampeonatoMasculinoParejas = new CrearCampeonatoMasculinoParejasTercera2022();
	private LeerCampeonatoMasculinoParejasSegunda2022 leerCampeonatoMasculinoParejasSegunda = new LeerCampeonatoMasculinoParejasSegunda2022();
	private LeerCampeonatoMasculinoParejasTercera2022 leerCampeonatoMasculinoParejasTercera = new LeerCampeonatoMasculinoParejasTercera2022();
	
	private LeerBoleras2022 leerBoleras = new LeerBoleras2022();
	
	private String diaFechaMaxInscripcion = null;
	private String mesFechaMaxInscripcion = null;
	private String anyoFechaMaxInscripcion = null;
	
	private Date fechaMaxInscripcion = null;
	private String estadoCampeonato = null;
	private String bolera = null;
	private String observacionesCampeonato1 = null;
	private String observacionesCampeonato2 = null;
	private String observacionesCampeonato3 = null;
	private String observacionesCampeonato4 = null;
	private String observacionesCampeonato5 = null;
	private String observacionesCampeonato6 = null;
	private String observacionesCampeonato7 = null;
	private String bolerasOcupadasFI = null;
	private String bolerasOcupadasOFCF = null;
	
	private List<Configuracion2022> resultListConfiguracion = null;
	private List<Fases2022> resultListFases = null;
	private List<ParticipantesParejas2022> resultListParticipantes = null;
	private List<ParticipantesParejas2022> resultListParticipantesByEquipo = null;
	
	public CampeonatoMasculinoParejasTerceraAddFacade2022() {
		Date fechaMaxInscripcion = leerCampeonatoMasculinoParejasTercera.getFechaMaxInscripcion();
		if(fechaMaxInscripcion != null){
			setDiaFechaMaxInscripcion(String.valueOf(fechaMaxInscripcion.getDate()));
			setMesFechaMaxInscripcion(String.valueOf(fechaMaxInscripcion.getMonth()+1));
			setAnyoFechaMaxInscripcion(String.valueOf(fechaMaxInscripcion.getYear()+1900));
		}
		setEstadoCampeonato(leerCampeonatoMasculinoParejasTercera.getEstadoCampeonato());
		setBolera(leerCampeonatoMasculinoParejasTercera.getBoleraFinal());
		setObservacionesCampeonato1(leerCampeonatoMasculinoParejasTercera.getObservacionesCampeonato1());
		setObservacionesCampeonato2(leerCampeonatoMasculinoParejasTercera.getObservacionesCampeonato2());
		setObservacionesCampeonato3(leerCampeonatoMasculinoParejasTercera.getObservacionesCampeonato3());
		setObservacionesCampeonato4(leerCampeonatoMasculinoParejasTercera.getObservacionesCampeonato4());
		setObservacionesCampeonato5(leerCampeonatoMasculinoParejasTercera.getObservacionesCampeonato5());
		setObservacionesCampeonato6(leerCampeonatoMasculinoParejasTercera.getObservacionesCampeonato6());
		setObservacionesCampeonato7(leerCampeonatoMasculinoParejasTercera.getObservacionesCampeonato7());
		setBolerasOcupadasFI(leerCampeonatoMasculinoParejasTercera.getBolerasOcupadasFI());
		setBolerasOcupadasOFCF(leerCampeonatoMasculinoParejasTercera.getBolerasOcupadasOFCF());
	}

	public List<Configuracion2022> getResultListConfiguracion() {
		if(resultListConfiguracion == null){
			resultListConfiguracion = (List<Configuracion2022>) leerCampeonatoMasculinoParejasTercera.listResultConfig();
		}
		return resultListConfiguracion;
	}
	
	public List<Fases2022> getResultListFases() {
		if(resultListFases == null){
			resultListFases = (List<Fases2022>) leerCampeonatoMasculinoParejasTercera.listResultFases();
		}
		return resultListFases;
	}
	
	public List<ParticipantesParejas2022> getResultListParticipantes() {
		if(resultListParticipantes == null){
			resultListParticipantes = (List<ParticipantesParejas2022>) leerCampeonatoMasculinoParejasTercera.listResultParticipantesOrderByEquipo();
		}
		return resultListParticipantes;
	}
	
	public List<ParticipantesParejas2022> getResultListParticipantesByEquipo(Integer idEquipo) {
		if(resultListParticipantesByEquipo == null){
			
			resultListParticipantesByEquipo = new ArrayList<>();

			if(resultListParticipantes == null){
				resultListParticipantes = (List<ParticipantesParejas2022>) leerCampeonatoMasculinoParejasTercera.listResultParticipantesOrderByEquipo();
			}
			
			for (ParticipantesParejas2022 participantesParejas : resultListParticipantes) {
				if(participantesParejas != null && participantesParejas.getPareja() != null && participantesParejas.getPareja().getIdJugador1() != null && participantesParejas.getPareja().getIdJugador2() != null){
					if(participantesParejas.getPareja().getJugador1().getEquipoId().equals(idEquipo) || participantesParejas.getPareja().getJugador2().getEquipoId().equals(idEquipo)){
						resultListParticipantesByEquipo.add(participantesParejas);
					}
				}
			}
		}
		
		return resultListParticipantesByEquipo;
	}
	
	//NUEVO
	public List<ParticipantesParejas2022> getResultListParticipantesByEquipos(Set<Integer> keySetequipoMasParejasQueGruposLibreGrupo) {
		if(resultListParticipantes == null){
			resultListParticipantes = (List<ParticipantesParejas2022>) leerCampeonatoMasculinoParejasTercera.listResultParticipantesOrderByEquipo();
		}
		List<ParticipantesParejas2022> resultListParticipantesParejasMasEquiposQueGrupos = new ArrayList<>();
		for (ParticipantesParejas2022 participantesParejas : resultListParticipantes) {
			if(keySetequipoMasParejasQueGruposLibreGrupo.contains(participantesParejas.getPareja().getJugador1().getEquipoId()) && keySetequipoMasParejasQueGruposLibreGrupo.contains(participantesParejas.getPareja().getJugador2().getEquipoId())){
				resultListParticipantesParejasMasEquiposQueGrupos.add(participantesParejas);
			}
		}
		return resultListParticipantesParejasMasEquiposQueGrupos;
	}
	//F-NUEVO
	
	public Integer getTotalRowsParticipantes(){
		if(resultListParticipantes == null){
			getResultListParticipantes();
		}
		if(resultListParticipantes != null){
			return resultListParticipantes.size();
		}
		return 0;
	}
	
	public Integer getMaxRowsPaginator(){
		return 8;
	}
	
	public boolean isEnabledPaginatorParticipantes(){
		return (getResultListParticipantes() != null && getResultListParticipantes() != null) ? 
					(getResultListParticipantes().size() > getMaxRowsPaginator()) : false;
	}
	
	//Manejo de estados
	public boolean isEstadoCampeonatoSinCrear(){
		return leerCampeonatoMasculinoParejasTercera.isEstadoCampeonatoSinCrear();
	}
	
	public boolean isAbrirInscripciones(){
		return leerCampeonatoMasculinoParejasTercera.isAbrirInscripciones();
	}
	
	public boolean isCerrarInscripciones(){
		return leerCampeonatoMasculinoParejasTercera.isCerrarInscripciones();
	}
	
	public boolean isInscripcionesCerradas(){
		return leerCampeonatoMasculinoParejasTercera.isInscripcionesCerradas();
	}
	
	public boolean isFasesCerradas(){
		return leerCampeonatoMasculinoParejasTercera.isFasesCerradas();
	}
	
	public boolean isSorteoRealizado(){
		return leerCampeonatoMasculinoParejasTercera.isSorteoRealizado();
	}
	
	public boolean isSorteoFinalizado(){
		return leerCampeonatoMasculinoParejasTercera.isSorteoFinalizado();
	}
	
	//Acciones para los diferentes estados
	
	private boolean validateFecha() {
		if(getAnyoFechaMaxInscripcion() != null && getMesFechaMaxInscripcion() != null && getDiaFechaMaxInscripcion() != null){
			if(NumberUtils2022.isNumber(getAnyoFechaMaxInscripcion()) && NumberUtils2022.isNumber(getMesFechaMaxInscripcion()) && NumberUtils2022.isNumber(getDiaFechaMaxInscripcion())){
				Integer dia = Integer.valueOf(getDiaFechaMaxInscripcion());
				if(dia > 31){
					return false;
				}
				Integer mes = Integer.valueOf(getMesFechaMaxInscripcion());
				if(mes > 12){
					return false;
				}
				Integer anyo = Integer.valueOf(getAnyoFechaMaxInscripcion());
				String fecha = (dia < 10 ? "0" + dia : dia) + "/" + (mes < 10 ? "0" + mes : mes) + "/" + anyo;
				Date fechaConvert = DateUtils2022.convertStringToDate(fecha);
				if(fechaConvert == null){
					return false;
				}else{
					setFechaMaxInscripcion(fechaConvert);
					return true;
				}
			}
		}
		return false;
		
	}
	
	public void doCrearBaseCampeonato(){
		
		boolean error = validateFecha();
		if(error){
			crearCampeonatoMasculinoParejas.crearBaseCampeonato(getFechaMaxInscripcion(), EstadosIndividualParejas2022.CONFIG_ESTADO_CREAR, getBolera());
			//Inicializar variables de nuevo
			leerCampeonatoMasculinoParejasTercera = new LeerCampeonatoMasculinoParejasTercera2022();
			//Redirigir
			redirect.redirect(menuView.PAG_OM_ADD_CAMPEONATO_MASCULINO_PAREJAS_TERCERA);
		}else{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "La Fecha max. inscripci�n no es valida.", null));
		}
	}
	
	public void doAbrirInscripcionesCampeonato(){
		crearCampeonatoMasculinoParejas.actualizarEstadoConfiguracion(EstadosIndividualParejas2022.CONFIG_ESTADO_ABRIR_INSCRIPCIONES);
		//Inicializar variables de nuevo
		leerCampeonatoMasculinoParejasTercera = new LeerCampeonatoMasculinoParejasTercera2022();
		//Redirigir
		redirect.redirect(menuView.PAG_OM_ADD_CAMPEONATO_MASCULINO_PAREJAS_TERCERA);
	}
	
	public void doCerrarInscripcionesCampeonato(){
		crearCampeonatoMasculinoParejas.actualizarEstadoConfiguracion(EstadosIndividualParejas2022.CONFIG_ESTADO_CERRAR_INSCRIPCIONES);
		//Inicializar variables de nuevo
		leerCampeonatoMasculinoParejasTercera = new LeerCampeonatoMasculinoParejasTercera2022();
		//Redirigir
		redirect.redirect(menuView.PAG_OM_ADD_CAMPEONATO_MASCULINO_PAREJAS_TERCERA);
	}
	
	public void doInscribirPareja(Jugadores2022 jugador1, Jugadores2022 jugador2, Categorias2022 categoria, String usuario, String activo){
		
		Parejas2022 parejaNew = new Parejas2022();
		parejaNew.setIdJugador1(jugador1.getId());
		parejaNew.setIdJugador2(jugador2.getId());
		parejaNew.setJugador1(jugador1);
		parejaNew.setJugador2(jugador2);
		
		crearCampeonatoMasculinoParejas.inscribirNewPareja(parejaNew, categoria, usuario, activo);
		//Inicializar variables de nuevo
		fichaEquipoFacade.refrescarParticipantesParejas();
		this.resultListParticipantesByEquipo = null;
		this.resultListParticipantes = null;
		leerCampeonatoMasculinoParejasTercera = new LeerCampeonatoMasculinoParejasTercera2022();
		getResultListParticipantesByEquipo(fichaEquipoFacade.getEquipo().getId());
		redirect.getInscripcionParejas();
	}
	
	public void doDesinscribirPareja(ParticipantesParejas2022 pareja,String usuario, String activo){
		crearCampeonatoMasculinoParejas.desinscribirPareja(pareja, usuario, activo);
		//Inicializar variables de nuevo
		fichaEquipoFacade.refrescarParticipantesParejas();
		this.resultListParticipantesByEquipo = null;
		this.resultListParticipantes = null;
		leerCampeonatoMasculinoParejasTercera = new LeerCampeonatoMasculinoParejasTercera2022();
		getResultListParticipantesByEquipo(fichaEquipoFacade.getEquipo().getId());
		redirect.getInscripcionParejas();
	}
	
	public Date getFechaMaxInscripcion() {
		return fechaMaxInscripcion;
	}

	public void setFechaMaxInscripcion(Date fechaMaxInscripcion) {
		this.fechaMaxInscripcion = fechaMaxInscripcion;
	}
	
	public String getBolera() {
		return bolera;
	}

	public void setBolera(String bolera) {
		this.bolera = bolera;
	}
	
	public String getNombreBolera(){
		if(this.bolera != null && this.bolera.length() > 0){
			return leerBoleras.read(Integer.valueOf(this.bolera)).getBolera();
		}
		return "";
	}
	
	public String getObservacionesCampeonato1() {
		return observacionesCampeonato1;
	}

	public void setObservacionesCampeonato1(String observacionesCampeonato1) {
		this.observacionesCampeonato1 = observacionesCampeonato1;
	}
	
	public String getObservacionesCampeonato2() {
		return observacionesCampeonato2;
	}

	public void setObservacionesCampeonato2(String observacionesCampeonato2) {
		this.observacionesCampeonato2 = observacionesCampeonato2;
	}
	
	public String getObservacionesCampeonato3() {
		return observacionesCampeonato3;
	}

	public void setObservacionesCampeonato3(String observacionesCampeonato3) {
		this.observacionesCampeonato3 = observacionesCampeonato3;
	}
	
	public String getObservacionesCampeonato4() {
		return observacionesCampeonato4;
	}

	public void setObservacionesCampeonato4(String observacionesCampeonato4) {
		this.observacionesCampeonato4 = observacionesCampeonato4;
	}
	
	public String getObservacionesCampeonato5() {
		return observacionesCampeonato5;
	}

	public void setObservacionesCampeonato5(String observacionesCampeonato5) {
		this.observacionesCampeonato5 = observacionesCampeonato5;
	}
	
	public String getObservacionesCampeonato6() {
		return observacionesCampeonato6;
	}

	public void setObservacionesCampeonato6(String observacionesCampeonato6) {
		this.observacionesCampeonato6 = observacionesCampeonato6;
	}
	
	public String getObservacionesCampeonato7() {
		return observacionesCampeonato7;
	}

	public void setObservacionesCampeonato7(String observacionesCampeonato7) {
		this.observacionesCampeonato7 = observacionesCampeonato7;
	}

	public String getEstadoCampeonato() {
		return estadoCampeonato;
	}

	public void setEstadoCampeonato(String estadoCampeonato) {
		this.estadoCampeonato = estadoCampeonato;
	}
	
	public String getDiaFechaMaxInscripcion() {
		return diaFechaMaxInscripcion;
	}

	public void setDiaFechaMaxInscripcion(String diaFechaMaxInscripcion) {
		this.diaFechaMaxInscripcion = diaFechaMaxInscripcion;
	}

	public String getMesFechaMaxInscripcion() {
		return mesFechaMaxInscripcion;
	}

	public void setMesFechaMaxInscripcion(String mesFechaMaxInscripcion) {
		this.mesFechaMaxInscripcion = mesFechaMaxInscripcion;
	}

	public String getAnyoFechaMaxInscripcion() {
		return anyoFechaMaxInscripcion;
	}

	public void setAnyoFechaMaxInscripcion(String anyoFechaMaxInscripcion) {
		this.anyoFechaMaxInscripcion = anyoFechaMaxInscripcion;
	}
	
	public void doCambiarActivarFase(Fases2022 fase){
		if(fase.getActivo().equalsIgnoreCase(Activo2022.SI)){
			fase.setActivo(Activo2022.NO);
		}else if(fase.getActivo().equalsIgnoreCase(Activo2022.NO)){
			fase.setActivo(Activo2022.SI);
		}
		crearCampeonatoMasculinoParejas.actualizarActivoFase(fase);
		leerCampeonatoMasculinoParejasTercera = new LeerCampeonatoMasculinoParejasTercera2022();
	}
	
	public void doActualizarDatos(Fases2022 fase){
		crearCampeonatoMasculinoParejas.actualizarFase(fase);
		leerCampeonatoMasculinoParejasTercera = new LeerCampeonatoMasculinoParejasTercera2022();
	}

	public void doCerrarFasesCampeonato(){
		//Inicializar variables de nuevo
		leerCampeonatoMasculinoParejasTercera = new LeerCampeonatoMasculinoParejasTercera2022();
		for(Fases2022 fase : getResultListFases()){
			if(fase.getActivo().equals(Activo2022.SI)){
				if(fase.getNumero().equals(FasesModelo2022.FASE_I)){
					crearCampeonatoMasculinoParejas.crearClasificacionFaseI();
					crearCampeonatoMasculinoParejas.crearCalendarioFaseI();
				}else if(fase.getNumero().equals(FasesModelo2022.FASE_II)){
					crearCampeonatoMasculinoParejas.crearCalendarioFaseII();
					crearCampeonatoMasculinoParejas.crearClasificacionFaseII();
				}else if(fase.getNumero().equals(FasesModelo2022.FASE_OF)){
					crearCampeonatoMasculinoParejas.crearCalendarioFaseOF();
				}else if(fase.getNumero().equals(FasesModelo2022.FASE_CF)){
					crearCampeonatoMasculinoParejas.crearCalendarioFaseCF();
				}else if(fase.getNumero().equals(FasesModelo2022.FASE_SF)){
					crearCampeonatoMasculinoParejas.crearCalendarioFaseSF();
				}else if(fase.getNumero().equals(FasesModelo2022.FASE_FC)){
					crearCampeonatoMasculinoParejas.crearCalendarioFaseFC();
				}else if(fase.getNumero().equals(FasesModelo2022.FASE_FF)){
					crearCampeonatoMasculinoParejas.crearCalendarioFaseFF();
				}
			}
		}
		crearCampeonatoMasculinoParejas.actualizarEstadoConfiguracion(EstadosIndividualParejas2022.CONFIG_ESTADO_CERRAR_FASES);
		//Inicializar variables de nuevo
		leerCampeonatoMasculinoParejasTercera = new LeerCampeonatoMasculinoParejasTercera2022();
		//Redirigir
		redirect.redirect(menuView.PAG_OM_ADD_CAMPEONATO_MASCULINO_PAREJAS_TERCERA);
	}
	
	public void doRealizarSorteoCampeonato(){
		
		boolean sorteoRealizado = false;
		
		try {
		
			//Asignar las observaciones del campeonato
			EstructuraCampeonatoMasculinoParejasTercera2022 ecmi = new EstructuraCampeonatoMasculinoParejasTercera2022();
			crearCampeonatoMasculinoParejas.updateRow(ecmi.getEstructuraObservacionesCampeonato1(), ecmi.getEstructuraObservacionesCampeonato1().getFila(), observacionesCampeonato1);
			crearCampeonatoMasculinoParejas.updateRow(ecmi.getEstructuraObservacionesCampeonato2(), ecmi.getEstructuraObservacionesCampeonato2().getFila(), observacionesCampeonato2);
			crearCampeonatoMasculinoParejas.updateRow(ecmi.getEstructuraObservacionesCampeonato3(), ecmi.getEstructuraObservacionesCampeonato3().getFila(), observacionesCampeonato3);
			crearCampeonatoMasculinoParejas.updateRow(ecmi.getEstructuraObservacionesCampeonato4(), ecmi.getEstructuraObservacionesCampeonato4().getFila(), observacionesCampeonato4);
			crearCampeonatoMasculinoParejas.updateRow(ecmi.getEstructuraObservacionesCampeonato5(), ecmi.getEstructuraObservacionesCampeonato5().getFila(), observacionesCampeonato5);
			crearCampeonatoMasculinoParejas.updateRow(ecmi.getEstructuraObservacionesCampeonato6(), ecmi.getEstructuraObservacionesCampeonato6().getFila(), observacionesCampeonato6);
			crearCampeonatoMasculinoParejas.updateRow(ecmi.getEstructuraObservacionesCampeonato7(), ecmi.getEstructuraObservacionesCampeonato7().getFila(), observacionesCampeonato7);
			crearCampeonatoMasculinoParejas.updateRow(ecmi.getEstructuraBolerasOcupadasFI(), ecmi.getEstructuraBolerasOcupadasFI().getFila(), bolerasOcupadasFI);
			crearCampeonatoMasculinoParejas.updateRow(ecmi.getEstructuraBolerasOcupadasOFCF(), ecmi.getEstructuraBolerasOcupadasOFCF().getFila(), bolerasOcupadasOFCF);
			
			
			
			//Inicializar variables de nuevo
			leerCampeonatoMasculinoParejasTercera = new LeerCampeonatoMasculinoParejasTercera2022();
			//Crear los mapas para el sorteo
			//FaseI
			LinkedHashMap<Integer, CalendarioFaseI2022> calendarioFaseIMap = new LinkedHashMap<>();
			LinkedHashMap<Integer, ClasificacionFaseI2022> clasificacionFaseIMap = new LinkedHashMap<>();
			//FaseII
			LinkedHashMap<Integer, CalendarioFaseII2022> calendarioFaseIIMap = new LinkedHashMap<>();
			LinkedHashMap<Integer, ClasificacionFaseII2022> clasificacionFaseIIMap = new LinkedHashMap<>();
			//FaseOF
			LinkedHashMap<Integer, CalendarioFaseOF2022> calendarioFaseOFMap = new LinkedHashMap<>();
			//FaseCF
			LinkedHashMap<Integer, CalendarioFaseCF2022> calendarioFaseCFMap = new LinkedHashMap<>();
			//FaseSF
			LinkedHashMap<Integer, CalendarioFaseSF2022> calendarioFaseSFMap = new LinkedHashMap<>();
			//FaseFC
			LinkedHashMap<Integer, CalendarioFaseFC2022> calendarioFaseFCMap = new LinkedHashMap<>();
			//FaseFF
			LinkedHashMap<Integer, CalendarioFaseFF2022> calendarioFaseFFMap = new LinkedHashMap<>();
			
			//1. Obtener las boleras disponibles, en principio solo las federadas.
			LinkedHashMap<Integer, String> bolerasGrupoAsignadasMap = new LinkedHashMap<>();
			LeerBoleras2022 leerBoleras = new LeerBoleras2022();
			List<Boleras2022> resultListBoleras = (List<Boleras2022>) leerBoleras.listResultFederadas();
			
			//Fase primera
			Integer numerofaseAnterior = 1;
			Integer numerofaseSiguiente = 1;
			
			//Bolera de la final
			Boleras2022 boleraFinal = null;
			for (Configuracion2022 configuracion : getResultListConfiguracion()) {
				if(configuracion.getNombre().equals("Bolera Final")){
					boleraFinal = leerBoleras.read(Integer.valueOf(configuracion.getValor()));
				}
			}
			
			//Bolera Ocupadas FI
			LinkedHashMap<Integer, Boleras2022> bolerasOcupadasFIMap =  new LinkedHashMap<>();
			for (Configuracion2022 configuracion : getResultListConfiguracion()) {
				if(configuracion.getNombre().equals("Boleras Ocupadas FI")){
					String bolerasOcupadas = configuracion.getValor();
					String[] bolerasOcupadasList = bolerasOcupadas.split("-");
					for (String bolera : bolerasOcupadasList) {
						Integer idBolera = Integer.valueOf(bolera);
						if(idBolera != null && idBolera > 0){
							Boleras2022 boleraObject = leerBoleras.read(idBolera);
							if(boleraObject != null){
								bolerasOcupadasFIMap.put(idBolera, boleraObject);
							}
						}
					}
				}
			}
			
			//Boleras Ocupadas OFCF
			LinkedHashMap<Integer, Boleras2022> bolerasOcupadasOFCFMap =  new LinkedHashMap<>();
			for (Configuracion2022 configuracion : getResultListConfiguracion()) {
				if(configuracion.getNombre().equals("Boleras Ocupadas OFCF")){
					String bolerasOcupadas = configuracion.getValor();
					String[] bolerasOcupadasList = bolerasOcupadas.split("-");
					for (String bolera : bolerasOcupadasList) {
						Integer idBolera = Integer.valueOf(bolera);
						if(idBolera != null && idBolera > 0){
							Boleras2022 boleraObject = leerBoleras.read(idBolera);
							if(boleraObject != null){
								bolerasOcupadasOFCFMap.put(idBolera, boleraObject);
							}
						}
					}
				}
			}
			
			//2. Definir la faseI
			for(Fases2022 fase : getResultListFases()){
				if(fase.getActivo().equals(Activo2022.SI)){
					if(fase.getNumero().equals(FasesModelo2022.FASE_I)){
						if(numerofaseSiguiente.equals(fase.getNumero())){
						
							//Objetos
							//Grupos Fase I
							LinkedHashMap<String, Grupos2022> gruposFaseIMap = new LinkedHashMap<>();
							//Parejas Asignados
							LinkedHashMap<Integer, ParticipantesParejas2022> parejasAsignados = new LinkedHashMap<>();
							
							//2.1. Obtener el numero de grupos de la fase
							Integer numeroGruposFase = fase.getNumeroEnfrentamientos();
							Integer numeroGruposFaseAsignados = 0;
							
							//2.2. Obtener el numero de participantes para saber el numero de parejas por grupo y los asignados al grupo para
							Integer numeroParticipantes = getResultListParticipantes().size();
							Integer parejasPorGrupoAsignados = 0;
							
							
							//NUEVO
							//Equipos mismos o mas parejas que grupos (idEquipo, numeroParejas)
							LinkedHashMap<Integer, Integer> equipoMasParejasQueGrupos = new LinkedHashMap<>();
							for (ParticipantesParejas2022 participante : getResultListParticipantes()) {
								
								if(participante.getPareja().getJugador1().getEquipoId().equals(participante.getPareja().getJugador2().getEquipoId())){
									if(equipoMasParejasQueGrupos.containsKey(participante.getPareja().getJugador1().getEquipoId())){
										Integer parejasEquipoValue = equipoMasParejasQueGrupos.get(participante.getPareja().getJugador1().getEquipoId());
										equipoMasParejasQueGrupos.put(participante.getPareja().getJugador1().getEquipoId(), parejasEquipoValue + 1 );
									}else{
										equipoMasParejasQueGrupos.put(participante.getPareja().getJugador1().getEquipoId(), 1);
									}
								}else{
									if(!equipoMasParejasQueGrupos.containsKey(PAREJA_EQUIPOS_MIXTO)){
										equipoMasParejasQueGrupos.put(PAREJA_EQUIPOS_MIXTO, 1);
									}
								}
								
							}
							//Lista de equipos con mas parejas que grupos
							LinkedHashMap<Integer, Integer> equipoMasParejasQueGruposLibre = (LinkedHashMap<Integer, Integer>) equipoMasParejasQueGrupos.clone();
							//Limpiar equipos que tengan menos parejas que grupos
							Set<Integer> keySet = equipoMasParejasQueGrupos.keySet();
							for (Integer idEquipoKey : keySet) {
								Integer parejaEquipoValue = equipoMasParejasQueGrupos.get(idEquipoKey);
								if(parejaEquipoValue < numeroGruposFase){
									equipoMasParejasQueGruposLibre.remove(idEquipoKey);
								}
							}
							//F-NUEVO
							
							
							
							//2.3. Crear los grupos a participar
							Integer gruposCreados = 1;
							while(gruposCreados <= numeroGruposFase){
								Grupos2022 grupos = new Grupos2022();
								grupos.setId(GruposLetra2022.getGruposLetraFaseI(gruposCreados));
								grupos.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2022.TERCERA);
								grupos.setFecha(fase.getFecha());
								grupos.setHora(fase.getHora());
								//Asignar bolera aleatoriamente
								Boleras2022 boleraAsignar = null;
								boolean boleraAsignada = false;
								
								//Quito las boleras ocupadas
								List<Boleras2022> resultListBolerasLibres = new ArrayList<>();
								for (Boleras2022 boleras : resultListBoleras) {
									if(!bolerasOcupadasFIMap.containsKey(boleras.getId())){
										resultListBolerasLibres.add(boleras);
									}
								}
								
								
								
								while(!boleraAsignada){
									Random rand = new Random();
									boleraAsignar = resultListBolerasLibres.get(rand.nextInt(resultListBolerasLibres.size()));
									//if(!boleraAsignar.getId().equals(boleraFinal.getId())){
										if(!bolerasOcupadasFIMap.containsKey(boleraAsignar.getId())){
											if(!bolerasGrupoAsignadasMap.containsKey(boleraAsignar.getId())){
												boleraAsignada = true;
											}
										}
									//}
								}
								grupos.setBoleraId(boleraAsignar.getId());
								
								
								//Obtener el numero de parejas del grupo
								BigDecimal parejasPendientesAsignar = new BigDecimal((numeroParticipantes) - parejasPorGrupoAsignados);
								BigDecimal gruposPendientesAsignar = new BigDecimal(numeroGruposFase - numeroGruposFaseAsignados);
								BigDecimal parejasPorGrupoCalculo = parejasPendientesAsignar.divide(gruposPendientesAsignar, RoundingMode.UP);
								grupos.setParejas(parejasPorGrupoCalculo.intValue());
								
								//Agregar los parejas para cada grupo a sorteo
								//A cada grupo sortear los parejas, se obtienen por equipos de mayor numero de parejas a menor
								//Limitaciones: 1-  Se han de repartir los parejas de equipo por grupo, 
								//					si hay mas parejas del equipo por grupo se asigna a alguno de los grupos
								Integer parejasAsignadosGrupo = 0;
								
								//NUEVO
								//Lista de equipos con mas parejas que grupos para cada grupo
								LinkedHashMap<Integer, Integer> equipoMasParejasQueGruposLibreGrupo = (LinkedHashMap<Integer, Integer>) equipoMasParejasQueGruposLibre.clone();
								//F-NUEVO
								
								Integer reintentosEquipoRepetido = 0;
								while(parejasAsignadosGrupo < parejasPorGrupoCalculo.intValue()){
									
									Random rand = new Random();
									
									
									//NUEVO
									ParticipantesParejas2022 parejasSorteo = null;
									
									//Primero buscar parejas pertenecientes a los equipos que tienen los mismos o mas parejas que grupos
									Set<Integer> keySetequipoMasParejasQueGruposLibreGrupo = equipoMasParejasQueGruposLibreGrupo.keySet();
									if(keySetequipoMasParejasQueGruposLibreGrupo.size() > 0){
										List<ParticipantesParejas2022> list = getResultListParticipantesByEquipos(keySetequipoMasParejasQueGruposLibreGrupo);
										boolean parejaCorrecto = false;
										while(!parejaCorrecto){
											parejasSorteo = list.get(rand.nextInt(list.size()));
											if(!parejasAsignados.containsKey(parejasSorteo.getPareja().getId())){
												equipoMasParejasQueGruposLibreGrupo.remove(parejasSorteo.getPareja().getJugador1().getEquipoId());
												parejaCorrecto = true;
											}
										}
									}else{
										boolean parejaCorrecto = false;
										while(!parejaCorrecto){
											parejasSorteo = getResultListParticipantes().get(rand.nextInt(getResultListParticipantes().size()));
											if(!parejasAsignados.containsKey(parejasSorteo.getPareja().getId())){
												if(equipoMasParejasQueGruposLibreGrupo.size() > 0){
													equipoMasParejasQueGruposLibreGrupo.remove(parejasSorteo.getPareja().getJugador1().getEquipoId());
												}
												parejaCorrecto = true;
											}
										}
										
									}
									
									//F-NUEVO
									
									
									
									if(!grupos.getParejasGrupoList().containsKey(parejasSorteo.getPareja().getId())){
										if(!parejasAsignados.containsKey(parejasSorteo.getPareja().getId())){
											if( !grupos.isExisteParejaMismoEquipo(parejasSorteo.getPareja().getJugador1().getEquipoId(), equipoMasParejasQueGrupos, numeroGruposFase)){
												parejasSorteo.setNumeroParejaGrupo(parejasAsignadosGrupo + 1);
												grupos.getParejasGrupoList().put(parejasSorteo.getPareja().getId(), parejasSorteo);
												parejasAsignadosGrupo++;
												parejasAsignados.put(parejasSorteo.getPareja().getId(), parejasSorteo);
											}else{
												if(reintentosEquipoRepetido >50){
													parejasSorteo.setNumeroParejaGrupo(parejasAsignadosGrupo + 1);
													grupos.getParejasGrupoList().put(parejasSorteo.getPareja().getId(), parejasSorteo);
													parejasAsignadosGrupo++;
													reintentosEquipoRepetido = 0;
													parejasAsignados.put(parejasSorteo.getPareja().getId(), parejasSorteo);
												}else{
													reintentosEquipoRepetido++;
												}
											}
										}
									}
								}
								
								//Agregar a los mapas
								gruposFaseIMap.put(grupos.getId(), grupos);
								bolerasGrupoAsignadasMap.put(boleraAsignar.getId(), grupos.getId());
								
								//Actualizar variables
								gruposCreados++;
								numeroGruposFaseAsignados++;
								parejasPorGrupoAsignados += parejasPorGrupoCalculo.intValue();
								
								
							}
							
							//A�adir los datos a el calendario y clasificacion de la fase I
							List<CalendarioFaseI2022> calendarioFaseIList = new ArrayList<CalendarioFaseI2022>();
							List<ClasificacionFaseI2022> clasificacionFaseIList = new ArrayList<ClasificacionFaseI2022>();
							//Se recorren los grupos obtenidos
							//Se a�aden a la clasificacion
							for (Entry<String, Grupos2022> entry : gruposFaseIMap.entrySet()) {
							    Object value = entry.getValue();
							    Grupos2022 grupo = (Grupos2022) value;
							    for (Entry<Integer, ParticipantesParejas2022> entryPi : grupo.getParejasGrupoList().entrySet()) {
							    	Object valuePi = entryPi.getValue();
							    	ParticipantesParejas2022 participantesParejas = (ParticipantesParejas2022) valuePi;
							    	
							    	//Agrego a clasificacion el pareja para el grupo
							    	ClasificacionFaseI2022 cf1 = new ClasificacionFaseI2022();
							    	cf1.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2022.TERCERA);
							    	cf1.setGrupo(grupo.getId());
							    	cf1.setId(participantesParejas.getNumeroParejaGrupo());
							    	cf1.setParejaId(participantesParejas.getPareja().getId());
							    	cf1.setPj(0);
							    	cf1.setPg(0);
							    	cf1.setPe(0);
							    	cf1.setPp(0);
							    	cf1.setPf(0);
							    	cf1.setPc(0);
							    	cf1.setPt(0);
							    	clasificacionFaseIList.add(cf1);
							    }
							}
							//Se a�aden al calendario
							for (Entry<String, Grupos2022> entry : gruposFaseIMap.entrySet()) {
							    Object value = entry.getValue();
							    Grupos2022 grupo = (Grupos2022) value;
							    //Recorro los enfrentamientos para el modelo del grupo
							    if(grupo.getParejas().equals(8)){
							    	calendarioFaseIList = prepareCalendarioFaseIModeloGrupos8(calendarioFaseIList, grupo);
							    }else if(grupo.getParejas().equals(7)){
							    	calendarioFaseIList = prepareCalendarioFaseIModeloGrupos7(calendarioFaseIList, grupo);
							    }else if(grupo.getParejas().equals(6)){
							    	calendarioFaseIList = prepareCalendarioFaseIModeloGrupos6(calendarioFaseIList, grupo);
							    }else if(grupo.getParejas().equals(5)){
							    	calendarioFaseIList = prepareCalendarioFaseIModeloGrupos5(calendarioFaseIList, grupo);
							    }else if(grupo.getParejas().equals(4)){
							    	calendarioFaseIList = prepareCalendarioFaseIModeloGrupos4(calendarioFaseIList, grupo);
							    }else if(grupo.getParejas().equals(3)){
							    	calendarioFaseIList = prepareCalendarioFaseIModeloGrupos3(calendarioFaseIList, grupo);
							    }
							    
							}
							
							//Insertar clasificacion y calendario fase I
							//Clasificacion
							for (ClasificacionFaseI2022 clasificacionFaseI : clasificacionFaseIList) {
								crearCampeonatoMasculinoParejas.insertarRowClasificacionFaseI(clasificacionFaseI.getInsertRow());
							}
							//Calendario
							for (CalendarioFaseI2022 calendarioFaseI : calendarioFaseIList) {
								crearCampeonatoMasculinoParejas.insertarRowCalendarioFaseI(calendarioFaseI.getInsertRow());
							}
							
							numerofaseSiguiente = fase.getFaseSiguiente();
						}
						
					}
				}
			}
			
			
			//3. Definir la faseII
			for(Fases2022 fase : getResultListFases()){
				if(fase.getActivo().equals(Activo2022.SI)){
					if(fase.getNumero().equals(FasesModelo2022.FASE_II)){
						if(numerofaseSiguiente.equals(fase.getNumero())){
							
						}
					}
				}
			}
			
			//4. Definir la faseOF
			
			Boleras2022 boleraFaseAnterior1 = null;
			Boleras2022 boleraFaseAnterior2 = null;
			
			for(Fases2022 fase : getResultListFases()){
				if(fase.getActivo().equals(Activo2022.SI)){
					if(fase.getNumero().equals(FasesModelo2022.FASE_OF)){
						if(numerofaseSiguiente.equals(fase.getNumero())){
							//Recuperar la fase anterior, para averiguar si era grupos escoger el modelo
							Fases2022 faseAnterior = searchFase(numerofaseAnterior);
							//Asignar boleras aleatoriamente
							Boleras2022 boleraAsignar1 = null;
							Boleras2022 boleraAsignar2 = null;
							boolean boleraAsignada1 = false;
							boolean boleraAsignada2 = false;
							LinkedHashMap<Integer, String> bolerasGrupoAsignadasCFMap = new LinkedHashMap<>();
							while(!boleraAsignada1){
								Random rand = new Random();
								boleraAsignar1 = resultListBoleras.get(rand.nextInt(resultListBoleras.size()));
								if(!boleraAsignar1.getId().equals(boleraFinal.getId())){
									if(!bolerasOcupadasOFCFMap.containsKey(boleraAsignar1.getId())){
										if(!bolerasGrupoAsignadasCFMap.containsKey(boleraAsignar1.getId())){
											boleraAsignada1 = true;
											boleraFaseAnterior1 = boleraAsignar1;
										}
									}
								}
							}
							while(!boleraAsignada2){
								Random rand = new Random();
								boleraAsignar2 = resultListBoleras.get(rand.nextInt(resultListBoleras.size()));
								if(!boleraAsignar2.getId().equals(boleraFinal.getId())){
									if(!bolerasOcupadasOFCFMap.containsKey(boleraAsignar2.getId())){
										if(!bolerasGrupoAsignadasCFMap.containsKey(boleraAsignar2.getId())){
											boleraAsignada2 = true;
											boleraFaseAnterior2 = boleraAsignar2;
										}
									}
								}
							}
							//Si en la fase previa eran 4 grupos
							if(faseAnterior.getTipoEnfrentamiento().equals(TipoEnfrentamiento2022.GRUPOS)){
								if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra2022.FASE_I_II_G_4)){
									List<CalendarioFaseOF2022> calendarioFaseOFList = new ArrayList<CalendarioFaseOF2022>();
									calendarioFaseOFList = prepareCalendarioFaseOFModeloGrupos4(calendarioFaseOFList, faseAnterior, boleraAsignar1, boleraAsignar2);
									//Calendario
									for (CalendarioFaseOF2022 calendarioFaseOF : calendarioFaseOFList) {
										crearCampeonatoMasculinoParejas.insertarRowCalendarioFaseOF(calendarioFaseOF.getInsertRow());
									}
									numerofaseSiguiente = fase.getFaseSiguiente();
									numerofaseAnterior = fase.getNumero();
								}
								//Si en la fase previa eran 8 grupos
								else if(faseAnterior.getTipoEnfrentamiento().equals(TipoEnfrentamiento2022.GRUPOS)){
									if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra2022.FASE_I_II_G_8)){
										List<CalendarioFaseOF2022> calendarioFaseOFList = new ArrayList<CalendarioFaseOF2022>();
										calendarioFaseOFList = prepareCalendarioFaseOFModeloGrupos8(calendarioFaseOFList, faseAnterior, boleraAsignar1, boleraAsignar2);
										//Calendario
										for (CalendarioFaseOF2022 calendarioFaseOF : calendarioFaseOFList) {
											crearCampeonatoMasculinoParejas.insertarRowCalendarioFaseOF(calendarioFaseOF.getInsertRow());
										}
										numerofaseSiguiente = fase.getFaseSiguiente();
										numerofaseAnterior = fase.getNumero();
									}
								}
							}
							
						}
					}
				}
			}
			
			//5. Definir la faseCF
			for(Fases2022 fase : getResultListFases()){
				if(fase.getActivo().equals(Activo2022.SI)){
					if(fase.getNumero().equals(FasesModelo2022.FASE_CF)){
						if(numerofaseSiguiente.equals(fase.getNumero())){
							//Recuperar la fase anterior, para averiguar si era grupos escoger el modelo
							Fases2022 faseAnterior = searchFase(numerofaseAnterior);
							//Si en la fase previa eran 4 grupos
							if(faseAnterior.getTipoEnfrentamiento().equals(TipoEnfrentamiento2022.GRUPOS)){
								if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra2022.FASE_I_II_G_4)){
									
									//Asignar boleras aleatoriamente
									Boleras2022 boleraAsignar1 = null;
									Boleras2022 boleraAsignar2 = null;
									boolean boleraAsignada1 = false;
									boolean boleraAsignada2 = false;
									LinkedHashMap<Integer, String> bolerasGrupoAsignadasCFMap = new LinkedHashMap<>();
									while(!boleraAsignada1){
										Random rand = new Random();
										boleraAsignar1 = resultListBoleras.get(rand.nextInt(resultListBoleras.size()));
										if(!boleraAsignar1.getId().equals(boleraFinal.getId())){
											if(!bolerasOcupadasOFCFMap.containsKey(boleraAsignar1.getId())){
												if(!bolerasGrupoAsignadasCFMap.containsKey(boleraAsignar1.getId())){
													boleraAsignada1 = true;
												}
											}
										}
									}
									while(!boleraAsignada2){
										Random rand = new Random();
										boleraAsignar2 = resultListBoleras.get(rand.nextInt(resultListBoleras.size()));
										if(!boleraAsignar2.getId().equals(boleraFinal.getId())){
											if(!bolerasOcupadasOFCFMap.containsKey(boleraAsignar2.getId())){
												if(!bolerasGrupoAsignadasCFMap.containsKey(boleraAsignar2.getId())){
													boleraAsignada2 = true;
												}
											}
										}
									}
									
									List<CalendarioFaseCF2022> calendarioFaseCFList = new ArrayList<CalendarioFaseCF2022>();
									calendarioFaseCFList = prepareCalendarioFaseCFModeloGrupos4(calendarioFaseCFList, faseAnterior, boleraAsignar1, boleraAsignar2);
									//Calendario
									for (CalendarioFaseCF2022 calendarioFaseCF : calendarioFaseCFList) {
										crearCampeonatoMasculinoParejas.insertarRowCalendarioFaseCF(calendarioFaseCF.getInsertRow());
									}
									numerofaseSiguiente = fase.getFaseSiguiente();
									numerofaseAnterior = fase.getNumero();
								}else if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra2022.FASE_I_II_G_2)){
									
									//Asignar boleras aleatoriamente
									Boleras2022 boleraAsignar1 = null;
									Boleras2022 boleraAsignar2 = null;
									boolean boleraAsignada1 = false;
									boolean boleraAsignada2 = false;
									LinkedHashMap<Integer, String> bolerasGrupoAsignadasCFMap = new LinkedHashMap<>();
									while(!boleraAsignada1){
										Random rand = new Random();
										boleraAsignar1 = resultListBoleras.get(rand.nextInt(resultListBoleras.size()));
										if(!boleraAsignar1.getId().equals(boleraFinal.getId())){
											if(!bolerasOcupadasOFCFMap.containsKey(boleraAsignar1.getId())){
												if(!bolerasGrupoAsignadasCFMap.containsKey(boleraAsignar1.getId())){
													boleraAsignada1 = true;
												}
											}
										}
									}
									while(!boleraAsignada2){
										Random rand = new Random();
										boleraAsignar2 = resultListBoleras.get(rand.nextInt(resultListBoleras.size()));
										if(!boleraAsignar2.getId().equals(boleraFinal.getId())){
											if(!bolerasOcupadasOFCFMap.containsKey(boleraAsignar2.getId())){
												if(!bolerasGrupoAsignadasCFMap.containsKey(boleraAsignar2.getId())){
													boleraAsignada2 = true;
												}
											}
										}
									}
									
									List<CalendarioFaseCF2022> calendarioFaseCFList = new ArrayList<CalendarioFaseCF2022>();
									calendarioFaseCFList = prepareCalendarioFaseCFModeloGrupos2(calendarioFaseCFList, faseAnterior, boleraAsignar1, boleraAsignar2);
									//Calendario
									for (CalendarioFaseCF2022 calendarioFaseCF : calendarioFaseCFList) {
										crearCampeonatoMasculinoParejas.insertarRowCalendarioFaseCF(calendarioFaseCF.getInsertRow());
									}
									numerofaseSiguiente = fase.getFaseSiguiente();
									numerofaseAnterior = fase.getNumero();
								}
							}
							//Si en la fase previa eran Octavos de final
							else if(fase.getTipoEnfrentamiento().equals(TipoEnfrentamiento2022.DIRECTO)){
								if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra2022.FASE_OF)){
									List<CalendarioFaseCF2022> calendarioFaseCFList = new ArrayList<CalendarioFaseCF2022>();
									calendarioFaseCFList = prepareCalendarioFaseCFModeloOF(calendarioFaseCFList, faseAnterior, boleraFaseAnterior1, boleraFaseAnterior2);
									//Calendario
									for (CalendarioFaseCF2022 calendarioFaseCF : calendarioFaseCFList) {
										crearCampeonatoMasculinoParejas.insertarRowCalendarioFaseCF(calendarioFaseCF.getInsertRow());
									}
									numerofaseSiguiente = fase.getFaseSiguiente();
									numerofaseAnterior = fase.getNumero();
								}
							}
						}
					}
				}
			}
			//6. Definir la faseSF
			
			for(Fases2022 fase : getResultListFases()){
				if(fase.getActivo().equals(Activo2022.SI)){
					if(fase.getNumero().equals(FasesModelo2022.FASE_SF)){
						if(numerofaseSiguiente.equals(fase.getNumero())){
							//Recuperar la fase anterior, para averiguar si era grupos escoger el modelo
							Fases2022 faseAnterior = searchFase(numerofaseAnterior);
							//Si en la fase previa eran Cuartos de final
							if(fase.getTipoEnfrentamiento().equals(TipoEnfrentamiento2022.DIRECTO)){
								if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra2022.FASE_CF)){
									List<CalendarioFaseSF2022> calendarioFaseSFList = new ArrayList<CalendarioFaseSF2022>();
									calendarioFaseSFList = prepareCalendarioFaseSFModeloCF(calendarioFaseSFList, faseAnterior, boleraFinal);
									//Calendario
									for (CalendarioFaseSF2022 calendarioFaseSF : calendarioFaseSFList) {
										crearCampeonatoMasculinoParejas.insertarRowCalendarioFaseSF(calendarioFaseSF.getInsertRow());
									}
									numerofaseSiguiente = fase.getFaseSiguiente();
									numerofaseAnterior = fase.getNumero();
								}
							}
						}
					}
				}
			}
			
			//7. Definir la faseFC
			
			for(Fases2022 fase : getResultListFases()){
				if(fase.getActivo().equals(Activo2022.SI)){
					if(fase.getNumero().equals(FasesModelo2022.FASE_FC)){
						//if(numerofaseSiguiente.equals(fase.getNumero())){
							//Recuperar la fase anterior, para averiguar si era grupos escoger el modelo
							Fases2022 faseAnterior = searchFase(numerofaseAnterior);
							//Si en la fase previa eran Semifinal
							if(fase.getTipoEnfrentamiento().equals(TipoEnfrentamiento2022.DIRECTO)){
								if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra2022.FASE_SF)){
									List<CalendarioFaseFC2022> calendarioFaseFCList = new ArrayList<CalendarioFaseFC2022>();
									calendarioFaseFCList = prepareCalendarioFaseFCModeloSF(calendarioFaseFCList, faseAnterior, boleraFinal);
									//Calendario
									for (CalendarioFaseFC2022 calendarioFaseFC : calendarioFaseFCList) {
										crearCampeonatoMasculinoParejas.insertarRowCalendarioFaseFC(calendarioFaseFC.getInsertRow());
									}
									//numerofaseSiguiente = fase.getFaseSiguiente();
								}
							}
						//}
					}
				}
			}
			
			//8. Definir la faseFF
			
			for(Fases2022 fase : getResultListFases()){
				if(fase.getActivo().equals(Activo2022.SI)){
					if(fase.getNumero().equals(FasesModelo2022.FASE_FF)){
						if(numerofaseSiguiente.equals(fase.getNumero())){
							//Recuperar la fase anterior, para averiguar si era grupos escoger el modelo
							Fases2022 faseAnterior = searchFase(numerofaseAnterior);
							//Si en la fase previa eran Semifinal
							if(fase.getTipoEnfrentamiento().equals(TipoEnfrentamiento2022.DIRECTO)){
								if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra2022.FASE_SF)){
									List<CalendarioFaseFF2022> calendarioFaseFFList = new ArrayList<CalendarioFaseFF2022>();
									calendarioFaseFFList = prepareCalendarioFaseFFModeloSF(calendarioFaseFFList, faseAnterior, boleraFinal);
									//Calendario
									for (CalendarioFaseFF2022 calendarioFaseFF : calendarioFaseFFList) {
										crearCampeonatoMasculinoParejas.insertarRowCalendarioFaseFF(calendarioFaseFF.getInsertRow());
										sorteoRealizado = true;
									}
									numerofaseSiguiente = fase.getFaseSiguiente();
								}
							}
						}
					}
				}
			}
		
			if(!sorteoRealizado){
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El Sorteo no se ha realizado.", null));
			}else{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "El Sorteo se ha realizado correctamente.", null));
				crearCampeonatoMasculinoParejas.actualizarEstadoConfiguracion(EstadosIndividualParejas2022.CONFIG_ESTADO_SORTEADO_CAMPEONATO);
				//Inicializar variables de nuevo
				leerCampeonatoMasculinoParejasTercera = new LeerCampeonatoMasculinoParejasTercera2022();
				//Redirigir
				//redirect.redirect(menuView.PAG_OM_ADD_CAMPEONATO_MASCULINO_PAREJAS_TERCERA);
				
			}
			
			
		}catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El Sorteo no se ha realizado.", null));
		}
		
	}

	private List<CalendarioFaseI2022> prepareCalendarioFaseIModeloGrupos8(List<CalendarioFaseI2022> calendarioFaseIList, Grupos2022 grupo) {
		ModeloParejasGrupos82022 modeloGrupos8 = new ModeloParejasGrupos82022();
		Integer id = 1;
		for (Entry<Integer, ModeloParejasEnfrentamientoGruposFaseI2022> entryPi : modeloGrupos8.getEnfrentamientosGrupo().entrySet()) {
			Integer key = entryPi.getKey();
			Object valuePi = entryPi.getValue();
			ModeloParejasEnfrentamientoGruposFaseI2022 enfrentamiento = (ModeloParejasEnfrentamientoGruposFaseI2022) valuePi;
			//Agregar calendario
			CalendarioFaseI2022 calendarioFaseI = new CalendarioFaseI2022();
			calendarioFaseI.setActivo(Activo2022.SI);
			calendarioFaseI.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2022.TERCERA);
			calendarioFaseI.setGrupo(grupo.getId());
			calendarioFaseI.setId(id);
			calendarioFaseI.setJuegosPareja1(0);
			ParticipantesParejas2022 pi1 = grupo.searchParejaGrupo(enfrentamiento.getPareja1(), grupo.getId());
			calendarioFaseI.setPareja1Id(pi1.getPareja().getId());
			calendarioFaseI.setJuegosPareja2(0);
			ParticipantesParejas2022 pi2 = grupo.searchParejaGrupo(enfrentamiento.getPareja2(), grupo.getId());
			calendarioFaseI.setPareja2Id(pi2.getPareja().getId());
			calendarioFaseI.setOrden(key);
			calendarioFaseI.setInicia(enfrentamiento.getSorteo());
			calendarioFaseI.setBoleraId(grupo.getBoleraId());
			id++;
			calendarioFaseIList.add(calendarioFaseI);
		}
		return calendarioFaseIList;
	}
	
	private List<CalendarioFaseI2022> prepareCalendarioFaseIModeloGrupos7(List<CalendarioFaseI2022> calendarioFaseIList, Grupos2022 grupo) {
		ModeloParejasGrupos72022 modeloGrupos = new ModeloParejasGrupos72022();
		Integer id = 1;
		for (Entry<Integer, ModeloParejasEnfrentamientoGruposFaseI2022> entryPi : modeloGrupos.getEnfrentamientosGrupo().entrySet()) {
			Integer key = entryPi.getKey();
			Object valuePi = entryPi.getValue();
			ModeloParejasEnfrentamientoGruposFaseI2022 enfrentamiento = (ModeloParejasEnfrentamientoGruposFaseI2022) valuePi;
			//Agregar calendario
			CalendarioFaseI2022 calendarioFaseI = new CalendarioFaseI2022();
			calendarioFaseI.setActivo(Activo2022.SI);
			calendarioFaseI.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2022.TERCERA);
			calendarioFaseI.setGrupo(grupo.getId());
			calendarioFaseI.setId(id);
			calendarioFaseI.setJuegosPareja1(0);
			ParticipantesParejas2022 pi1 = grupo.searchParejaGrupo(enfrentamiento.getPareja1(), grupo.getId());
			calendarioFaseI.setPareja1Id(pi1.getPareja().getId());
			calendarioFaseI.setJuegosPareja2(0);
			ParticipantesParejas2022 pi2 = grupo.searchParejaGrupo(enfrentamiento.getPareja2(), grupo.getId());
			calendarioFaseI.setPareja2Id(pi2.getPareja().getId());
			calendarioFaseI.setOrden(key);
			calendarioFaseI.setInicia(enfrentamiento.getSorteo());
			calendarioFaseI.setBoleraId(grupo.getBoleraId());
			id++;
			calendarioFaseIList.add(calendarioFaseI);
		}
		return calendarioFaseIList;
	}
	
	private List<CalendarioFaseI2022> prepareCalendarioFaseIModeloGrupos6(List<CalendarioFaseI2022> calendarioFaseIList, Grupos2022 grupo) {
		ModeloParejasGrupos62022 modeloGrupos = new ModeloParejasGrupos62022();
		Integer id = 1;
		for (Entry<Integer, ModeloParejasEnfrentamientoGruposFaseI2022> entryPi : modeloGrupos.getEnfrentamientosGrupo().entrySet()) {
			Integer key = entryPi.getKey();
			Object valuePi = entryPi.getValue();
			ModeloParejasEnfrentamientoGruposFaseI2022 enfrentamiento = (ModeloParejasEnfrentamientoGruposFaseI2022) valuePi;
			//Agregar calendario
			CalendarioFaseI2022 calendarioFaseI = new CalendarioFaseI2022();
			calendarioFaseI.setActivo(Activo2022.SI);
			calendarioFaseI.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2022.TERCERA);
			calendarioFaseI.setGrupo(grupo.getId());
			calendarioFaseI.setId(id);
			calendarioFaseI.setJuegosPareja1(0);
			ParticipantesParejas2022 pi1 = grupo.searchParejaGrupo(enfrentamiento.getPareja1(), grupo.getId());
			calendarioFaseI.setPareja1Id(pi1.getPareja().getId());
			calendarioFaseI.setJuegosPareja2(0);
			ParticipantesParejas2022 pi2 = grupo.searchParejaGrupo(enfrentamiento.getPareja2(), grupo.getId());
			calendarioFaseI.setPareja2Id(pi2.getPareja().getId());
			calendarioFaseI.setOrden(key);
			calendarioFaseI.setInicia(enfrentamiento.getSorteo());
			calendarioFaseI.setBoleraId(grupo.getBoleraId());
			id++;
			calendarioFaseIList.add(calendarioFaseI);
		}
		return calendarioFaseIList;
	}
	
	private List<CalendarioFaseI2022> prepareCalendarioFaseIModeloGrupos5(List<CalendarioFaseI2022> calendarioFaseIList, Grupos2022 grupo) {
		ModeloParejasGrupos52022 modeloGrupos = new ModeloParejasGrupos52022();
		Integer id = 1;
		for (Entry<Integer, ModeloParejasEnfrentamientoGruposFaseI2022> entryPi : modeloGrupos.getEnfrentamientosGrupo().entrySet()) {
			Integer key = entryPi.getKey();
			Object valuePi = entryPi.getValue();
			ModeloParejasEnfrentamientoGruposFaseI2022 enfrentamiento = (ModeloParejasEnfrentamientoGruposFaseI2022) valuePi;
			//Agregar calendario
			CalendarioFaseI2022 calendarioFaseI = new CalendarioFaseI2022();
			calendarioFaseI.setActivo(Activo2022.SI);
			calendarioFaseI.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2022.TERCERA);
			calendarioFaseI.setGrupo(grupo.getId());
			calendarioFaseI.setId(id);
			calendarioFaseI.setJuegosPareja1(0);
			ParticipantesParejas2022 pi1 = grupo.searchParejaGrupo(enfrentamiento.getPareja1(), grupo.getId());
			calendarioFaseI.setPareja1Id(pi1.getPareja().getId());
			calendarioFaseI.setJuegosPareja2(0);
			ParticipantesParejas2022 pi2 = grupo.searchParejaGrupo(enfrentamiento.getPareja2(), grupo.getId());
			calendarioFaseI.setPareja2Id(pi2.getPareja().getId());
			calendarioFaseI.setOrden(key);
			calendarioFaseI.setInicia(enfrentamiento.getSorteo());
			calendarioFaseI.setBoleraId(grupo.getBoleraId());
			id++;
			calendarioFaseIList.add(calendarioFaseI);
		}
		return calendarioFaseIList;
	}
	
	private List<CalendarioFaseI2022> prepareCalendarioFaseIModeloGrupos4(List<CalendarioFaseI2022> calendarioFaseIList, Grupos2022 grupo) {
		ModeloParejasGrupos42022 modeloGrupos = new ModeloParejasGrupos42022();
		Integer id = 1;
		for (Entry<Integer, ModeloParejasEnfrentamientoGruposFaseI2022> entryPi : modeloGrupos.getEnfrentamientosGrupo().entrySet()) {
			Integer key = entryPi.getKey();
			Object valuePi = entryPi.getValue();
			ModeloParejasEnfrentamientoGruposFaseI2022 enfrentamiento = (ModeloParejasEnfrentamientoGruposFaseI2022) valuePi;
			//Agregar calendario
			CalendarioFaseI2022 calendarioFaseI = new CalendarioFaseI2022();
			calendarioFaseI.setActivo(Activo2022.SI);
			calendarioFaseI.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2022.TERCERA);
			calendarioFaseI.setGrupo(grupo.getId());
			calendarioFaseI.setId(id);
			calendarioFaseI.setJuegosPareja1(0);
			ParticipantesParejas2022 pi1 = grupo.searchParejaGrupo(enfrentamiento.getPareja1(), grupo.getId());
			calendarioFaseI.setPareja1Id(pi1.getPareja().getId());
			calendarioFaseI.setJuegosPareja2(0);
			ParticipantesParejas2022 pi2 = grupo.searchParejaGrupo(enfrentamiento.getPareja2(), grupo.getId());
			calendarioFaseI.setPareja2Id(pi2.getPareja().getId());
			calendarioFaseI.setOrden(key);
			calendarioFaseI.setInicia(enfrentamiento.getSorteo());
			calendarioFaseI.setBoleraId(grupo.getBoleraId());
			id++;
			calendarioFaseIList.add(calendarioFaseI);
		}
		return calendarioFaseIList;
	}
	
	private List<CalendarioFaseI2022> prepareCalendarioFaseIModeloGrupos3(List<CalendarioFaseI2022> calendarioFaseIList, Grupos2022 grupo) {
		ModeloParejasGrupos32022 modeloGrupos = new ModeloParejasGrupos32022();
		Integer id = 1;
		for (Entry<Integer, ModeloParejasEnfrentamientoGruposFaseI2022> entryPi : modeloGrupos.getEnfrentamientosGrupo().entrySet()) {
			Integer key = entryPi.getKey();
			Object valuePi = entryPi.getValue();
			ModeloParejasEnfrentamientoGruposFaseI2022 enfrentamiento = (ModeloParejasEnfrentamientoGruposFaseI2022) valuePi;
			//Agregar calendario
			CalendarioFaseI2022 calendarioFaseI = new CalendarioFaseI2022();
			calendarioFaseI.setActivo(Activo2022.SI);
			calendarioFaseI.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2022.TERCERA);
			calendarioFaseI.setGrupo(grupo.getId());
			calendarioFaseI.setId(id);
			calendarioFaseI.setJuegosPareja1(0);
			ParticipantesParejas2022 pi1 = grupo.searchParejaGrupo(enfrentamiento.getPareja1(), grupo.getId());
			calendarioFaseI.setPareja1Id(pi1.getPareja().getId());
			calendarioFaseI.setJuegosPareja2(0);
			ParticipantesParejas2022 pi2 = grupo.searchParejaGrupo(enfrentamiento.getPareja2(), grupo.getId());
			calendarioFaseI.setPareja2Id(pi2.getPareja().getId());
			calendarioFaseI.setOrden(key);
			calendarioFaseI.setInicia(enfrentamiento.getSorteo());
			calendarioFaseI.setBoleraId(grupo.getBoleraId());
			id++;
			calendarioFaseIList.add(calendarioFaseI);
		}
		return calendarioFaseIList;
	}

	public Fases2022 searchFase(Integer numeroFase){
		for(Fases2022 fase : getResultListFases()){
			if(fase.getNumero().equals(numeroFase)){
				return fase;
			}
		}
		return null;
	}
	
	private List<CalendarioFaseOF2022> prepareCalendarioFaseOFModeloGrupos4(List<CalendarioFaseOF2022> calendarioFaseOFList, Fases2022 faseAnterior, Boleras2022 boleraAsignar1, Boleras2022 boleraAsignar2) {
		ModeloParejasOctavosFinal4Grupos2022 modeloOctavosFinal4Grupos = new ModeloParejasOctavosFinal4Grupos2022();
		Integer id = 1;

		for (Entry<Integer, ModeloParejasEnfrentamientoOctavosFinal2022> entry : modeloOctavosFinal4Grupos.getEnfrentamientosOctavosFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloParejasEnfrentamientoOctavosFinal2022 enfrentamiento = (ModeloParejasEnfrentamientoOctavosFinal2022) value;
			//Agregar calendario
			CalendarioFaseOF2022 calendarioFaseOF = new CalendarioFaseOF2022();
			calendarioFaseOF.setId(id);
			calendarioFaseOF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2022.TERCERA);
			calendarioFaseOF.setNumeroEnfrentamiento(id);
			calendarioFaseOF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseOF.setPareja1Id(0);
			calendarioFaseOF.setJuegosPareja1P1(0);
			calendarioFaseOF.setJuegosPareja1P2(0);
			calendarioFaseOF.setJuegosPareja1P3(0);
			calendarioFaseOF.setJuegosPareja2P1(0);
			calendarioFaseOF.setJuegosPareja2P2(0);
			calendarioFaseOF.setJuegosPareja2P3(0);
			calendarioFaseOF.setPareja2Id(0);
			calendarioFaseOF.setOrden(key);
			calendarioFaseOF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseOF.setActivo(Activo2022.SI);
			calendarioFaseOF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseOF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseOF.setGrupoProcedenciaPareja1(enfrentamiento.getGrupoProcedenciaPareja1());
			calendarioFaseOF.setPosicionProcedenciaPareja1(enfrentamiento.getPosicionProcedenciaPareja1());
			calendarioFaseOF.setGrupoProcedenciaPareja2(enfrentamiento.getGrupoProcedenciaPareja2());
			calendarioFaseOF.setPosicionProcedenciaPareja2(enfrentamiento.getPosicionProcedenciaPareja2());
			
			//Asignar la bolera por parte del cuadro
			if(id > 0 && id <5 ){
				calendarioFaseOF.setBoleraId(boleraAsignar1.getId());
			}else if(id > 4 && id <9 ){
				calendarioFaseOF.setBoleraId(boleraAsignar2.getId());
			}
			id++;
			calendarioFaseOFList.add(calendarioFaseOF);
		}
		return calendarioFaseOFList;
	}
	
	private List<CalendarioFaseOF2022> prepareCalendarioFaseOFModeloGrupos8(List<CalendarioFaseOF2022> calendarioFaseOFList, Fases2022 faseAnterior, Boleras2022 boleraAsignar1, Boleras2022 boleraAsignar2) {
		ModeloParejasOctavosFinal8Grupos2022 modeloOctavosFinal8Grupos = new ModeloParejasOctavosFinal8Grupos2022();
		Integer id = 1;

		for (Entry<Integer, ModeloParejasEnfrentamientoOctavosFinal2022> entry : modeloOctavosFinal8Grupos.getEnfrentamientosOctavosFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloParejasEnfrentamientoOctavosFinal2022 enfrentamiento = (ModeloParejasEnfrentamientoOctavosFinal2022) value;
			//Agregar calendario
			CalendarioFaseOF2022 calendarioFaseOF = new CalendarioFaseOF2022();
			calendarioFaseOF.setId(id);
			calendarioFaseOF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2022.TERCERA);
			calendarioFaseOF.setNumeroEnfrentamiento(id);
			calendarioFaseOF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseOF.setPareja1Id(0);
			calendarioFaseOF.setJuegosPareja1P1(0);
			calendarioFaseOF.setJuegosPareja1P2(0);
			calendarioFaseOF.setJuegosPareja1P3(0);
			calendarioFaseOF.setJuegosPareja2P1(0);
			calendarioFaseOF.setJuegosPareja2P2(0);
			calendarioFaseOF.setJuegosPareja2P3(0);
			calendarioFaseOF.setPareja2Id(0);
			calendarioFaseOF.setOrden(key);
			calendarioFaseOF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseOF.setActivo(Activo2022.SI);
			calendarioFaseOF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseOF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseOF.setGrupoProcedenciaPareja1(enfrentamiento.getGrupoProcedenciaPareja1());
			calendarioFaseOF.setPosicionProcedenciaPareja1(enfrentamiento.getPosicionProcedenciaPareja1());
			calendarioFaseOF.setGrupoProcedenciaPareja2(enfrentamiento.getGrupoProcedenciaPareja2());
			calendarioFaseOF.setPosicionProcedenciaPareja2(enfrentamiento.getPosicionProcedenciaPareja2());
			
			//Asignar la bolera por parte del cuadro
			if(id > 0 && id <5 ){
				calendarioFaseOF.setBoleraId(boleraAsignar1.getId());
			}else if(id > 4 && id <9 ){
				calendarioFaseOF.setBoleraId(boleraAsignar2.getId());
			}
			id++;
			calendarioFaseOFList.add(calendarioFaseOF);
		}
		return calendarioFaseOFList;
	}
	
	private List<CalendarioFaseCF2022> prepareCalendarioFaseCFModeloGrupos2(List<CalendarioFaseCF2022> calendarioFaseCFList, Fases2022 faseAnterior, Boleras2022 boleraAsignar1, Boleras2022 boleraAsignar2) {
		ModeloParejasCuartosFinal2Grupos2022 modeloCuartosFinal2Grupos = new ModeloParejasCuartosFinal2Grupos2022();
		Integer id = 1;
		
		for (Entry<Integer, ModeloParejasEnfrentamientoCuartosFinal2022> entry : modeloCuartosFinal2Grupos.getEnfrentamientosCuartosFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloParejasEnfrentamientoCuartosFinal2022 enfrentamiento = (ModeloParejasEnfrentamientoCuartosFinal2022) value;
			//Agregar calendario
			CalendarioFaseCF2022 calendarioFaseCF = new CalendarioFaseCF2022();
			calendarioFaseCF.setId(id);
			calendarioFaseCF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2022.TERCERA);
			calendarioFaseCF.setNumeroEnfrentamiento(id);
			calendarioFaseCF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseCF.setPareja1Id(0);
			calendarioFaseCF.setJuegosPareja1P1(0);
			calendarioFaseCF.setJuegosPareja1P2(0);
			calendarioFaseCF.setJuegosPareja1P3(0);
			calendarioFaseCF.setJuegosPareja2P1(0);
			calendarioFaseCF.setJuegosPareja2P2(0);
			calendarioFaseCF.setJuegosPareja2P3(0);
			calendarioFaseCF.setPareja2Id(0);
			calendarioFaseCF.setOrden(key);
			calendarioFaseCF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseCF.setActivo(Activo2022.SI);
			calendarioFaseCF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseCF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseCF.setGrupoProcedenciaPareja1(enfrentamiento.getGrupoProcedenciaPareja1());
			calendarioFaseCF.setPosicionProcedenciaPareja1(enfrentamiento.getPosicionProcedenciaPareja1());
			calendarioFaseCF.setGrupoProcedenciaPareja2(enfrentamiento.getGrupoProcedenciaPareja2());
			calendarioFaseCF.setPosicionProcedenciaPareja2(enfrentamiento.getPosicionProcedenciaPareja2());
			
			//Asignar la bolera por parte del cuadro
			if(id > 0 && id <3 ){
				calendarioFaseCF.setBoleraId(boleraAsignar1.getId());
			}else if(id > 2 && id <5 ){
				calendarioFaseCF.setBoleraId(boleraAsignar2.getId());
			}
			id++;
			calendarioFaseCFList.add(calendarioFaseCF);
		}
		return calendarioFaseCFList;
	}
	
	private List<CalendarioFaseCF2022> prepareCalendarioFaseCFModeloGrupos4(List<CalendarioFaseCF2022> calendarioFaseCFList, Fases2022 faseAnterior, Boleras2022 boleraAsignar1, Boleras2022 boleraAsignar2) {
		ModeloParejasCuartosFinal4Grupos2022 modeloCuartosFinal4Grupos = new ModeloParejasCuartosFinal4Grupos2022();
		Integer id = 1;
		
		for (Entry<Integer, ModeloParejasEnfrentamientoCuartosFinal2022> entry : modeloCuartosFinal4Grupos.getEnfrentamientosCuartosFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloParejasEnfrentamientoCuartosFinal2022 enfrentamiento = (ModeloParejasEnfrentamientoCuartosFinal2022) value;
			//Agregar calendario
			CalendarioFaseCF2022 calendarioFaseCF = new CalendarioFaseCF2022();
			calendarioFaseCF.setId(id);
			calendarioFaseCF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2022.TERCERA);
			calendarioFaseCF.setNumeroEnfrentamiento(id);
			calendarioFaseCF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseCF.setPareja1Id(0);
			calendarioFaseCF.setJuegosPareja1P1(0);
			calendarioFaseCF.setJuegosPareja1P2(0);
			calendarioFaseCF.setJuegosPareja1P3(0);
			calendarioFaseCF.setJuegosPareja2P1(0);
			calendarioFaseCF.setJuegosPareja2P2(0);
			calendarioFaseCF.setJuegosPareja2P3(0);
			calendarioFaseCF.setPareja2Id(0);
			calendarioFaseCF.setOrden(key);
			calendarioFaseCF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseCF.setActivo(Activo2022.SI);
			calendarioFaseCF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseCF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseCF.setGrupoProcedenciaPareja1(enfrentamiento.getGrupoProcedenciaPareja1());
			calendarioFaseCF.setPosicionProcedenciaPareja1(enfrentamiento.getPosicionProcedenciaPareja1());
			calendarioFaseCF.setGrupoProcedenciaPareja2(enfrentamiento.getGrupoProcedenciaPareja2());
			calendarioFaseCF.setPosicionProcedenciaPareja2(enfrentamiento.getPosicionProcedenciaPareja2());
			
			//Asignar la bolera por parte del cuadro
			if(id > 0 && id <3 ){
				calendarioFaseCF.setBoleraId(boleraAsignar1.getId());
			}else if(id > 2 && id <5 ){
				calendarioFaseCF.setBoleraId(boleraAsignar2.getId());
			}
			id++;
			calendarioFaseCFList.add(calendarioFaseCF);
		}
		return calendarioFaseCFList;
	}
	
	private List<CalendarioFaseCF2022> prepareCalendarioFaseCFModeloOF(List<CalendarioFaseCF2022> calendarioFaseCFList, Fases2022 faseAnterior, Boleras2022 boleraAsignar1, Boleras2022 boleraAsignar2) {
		ModeloParejasCuartosFinal4OF2022 modeloCuartosFinal4OF = new ModeloParejasCuartosFinal4OF2022();
		Integer id = 1;
		
		for (Entry<Integer, ModeloParejasEnfrentamientoCuartosFinal2022> entry : modeloCuartosFinal4OF.getEnfrentamientosCuartosFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloParejasEnfrentamientoCuartosFinal2022 enfrentamiento = (ModeloParejasEnfrentamientoCuartosFinal2022) value;
			//Agregar calendario
			CalendarioFaseCF2022 calendarioFaseCF = new CalendarioFaseCF2022();
			calendarioFaseCF.setId(id);
			calendarioFaseCF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2022.TERCERA);
			calendarioFaseCF.setNumeroEnfrentamiento(id);
			calendarioFaseCF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseCF.setPareja1Id(0);
			calendarioFaseCF.setJuegosPareja1P1(0);
			calendarioFaseCF.setJuegosPareja1P2(0);
			calendarioFaseCF.setJuegosPareja1P3(0);
			calendarioFaseCF.setJuegosPareja2P1(0);
			calendarioFaseCF.setJuegosPareja2P2(0);
			calendarioFaseCF.setJuegosPareja2P3(0);
			calendarioFaseCF.setPareja2Id(0);
			calendarioFaseCF.setOrden(key);
			calendarioFaseCF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseCF.setActivo(Activo2022.SI);
			calendarioFaseCF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseCF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseCF.setGrupoProcedenciaPareja1(enfrentamiento.getGrupoProcedenciaPareja1());
			calendarioFaseCF.setPosicionProcedenciaPareja1(enfrentamiento.getPosicionProcedenciaPareja1());
			calendarioFaseCF.setGrupoProcedenciaPareja2(enfrentamiento.getGrupoProcedenciaPareja2());
			calendarioFaseCF.setPosicionProcedenciaPareja2(enfrentamiento.getPosicionProcedenciaPareja2());
			
			//Asignar la bolera por parte del cuadro
			if(id > 0 && id <3 ){
				calendarioFaseCF.setBoleraId(boleraAsignar1.getId());
			}else if(id > 2 && id <5 ){
				calendarioFaseCF.setBoleraId(boleraAsignar2.getId());
			}
			id++;
			calendarioFaseCFList.add(calendarioFaseCF);
		}
		return calendarioFaseCFList;
	}
	
	private List<CalendarioFaseSF2022> prepareCalendarioFaseSFModeloCF(List<CalendarioFaseSF2022> calendarioFaseSFList, Fases2022 faseAnterior, Boleras2022 boleraFinal) {
		ModeloParejasSemiFinal4CF2022 modeloSemiFinal4CF = new ModeloParejasSemiFinal4CF2022();
		Integer id = 1;
		
		for (Entry<Integer, ModeloParejasEnfrentamientoSemiFinal2022> entry : modeloSemiFinal4CF.getEnfrentamientosSemiFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloParejasEnfrentamientoSemiFinal2022 enfrentamiento = (ModeloParejasEnfrentamientoSemiFinal2022) value;
			//Agregar calendario
			CalendarioFaseSF2022 calendarioFaseSF = new CalendarioFaseSF2022();
			calendarioFaseSF.setId(id);
			calendarioFaseSF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2022.TERCERA);
			calendarioFaseSF.setNumeroEnfrentamiento(id);
			calendarioFaseSF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseSF.setPareja1Id(0);
			calendarioFaseSF.setJuegosPareja1P1(0);
			calendarioFaseSF.setJuegosPareja1P2(0);
			calendarioFaseSF.setJuegosPareja1P3(0);
			calendarioFaseSF.setJuegosPareja2P1(0);
			calendarioFaseSF.setJuegosPareja2P2(0);
			calendarioFaseSF.setJuegosPareja2P3(0);
			calendarioFaseSF.setPareja2Id(0);
			calendarioFaseSF.setOrden(key);
			calendarioFaseSF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseSF.setActivo(Activo2022.SI);
			calendarioFaseSF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseSF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseSF.setGrupoProcedenciaPareja1(enfrentamiento.getGrupoProcedenciaPareja1());
			calendarioFaseSF.setPosicionProcedenciaPareja1(enfrentamiento.getPosicionProcedenciaPareja1());
			calendarioFaseSF.setGrupoProcedenciaPareja2(enfrentamiento.getGrupoProcedenciaPareja2());
			calendarioFaseSF.setPosicionProcedenciaPareja2(enfrentamiento.getPosicionProcedenciaPareja2());
			
			//Asignar la bolera por parte del cuadro
			calendarioFaseSF.setBoleraId(boleraFinal.getId());
			id++;
			calendarioFaseSFList.add(calendarioFaseSF);
		}
		return calendarioFaseSFList;
	}
	
	private List<CalendarioFaseFC2022> prepareCalendarioFaseFCModeloSF(List<CalendarioFaseFC2022> calendarioFaseFCList, Fases2022 faseAnterior, Boleras2022 boleraFinal) {
		ModeloParejasFinalConsolacion4SF2022 modeloFinalConsolacion4SF = new ModeloParejasFinalConsolacion4SF2022();
		Integer id = 1;
		
		for (Entry<Integer, ModeloParejasEnfrentamientoFinalConsolacion2022> entry : modeloFinalConsolacion4SF.getEnfrentamientosFinalConsolacion().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloParejasEnfrentamientoFinalConsolacion2022 enfrentamiento = (ModeloParejasEnfrentamientoFinalConsolacion2022) value;
			//Agregar calendario
			CalendarioFaseFC2022 calendarioFaseFC = new CalendarioFaseFC2022();
			calendarioFaseFC.setId(id);
			calendarioFaseFC.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2022.TERCERA);
			calendarioFaseFC.setNumeroEnfrentamiento(id);
			calendarioFaseFC.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseFC.setPareja1Id(0);
			calendarioFaseFC.setJuegosPareja1P1(0);
			calendarioFaseFC.setJuegosPareja1P2(0);
			calendarioFaseFC.setJuegosPareja1P3(0);
			calendarioFaseFC.setJuegosPareja2P1(0);
			calendarioFaseFC.setJuegosPareja2P2(0);
			calendarioFaseFC.setJuegosPareja2P3(0);
			calendarioFaseFC.setPareja2Id(0);
			calendarioFaseFC.setOrden(key);
			calendarioFaseFC.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseFC.setActivo(Activo2022.SI);
			calendarioFaseFC.setInicia(enfrentamiento.getSorteo());
			calendarioFaseFC.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseFC.setGrupoProcedenciaPareja1(enfrentamiento.getGrupoProcedenciaPareja1());
			calendarioFaseFC.setPosicionProcedenciaPareja1(enfrentamiento.getPosicionProcedenciaPareja1());
			calendarioFaseFC.setGrupoProcedenciaPareja2(enfrentamiento.getGrupoProcedenciaPareja2());
			calendarioFaseFC.setPosicionProcedenciaPareja2(enfrentamiento.getPosicionProcedenciaPareja2());
			
			//Asignar la bolera por parte del cuadro
			calendarioFaseFC.setBoleraId(boleraFinal.getId());
			id++;
			calendarioFaseFCList.add(calendarioFaseFC);
		}
		return calendarioFaseFCList;
	}
	
	private List<CalendarioFaseFF2022> prepareCalendarioFaseFFModeloSF(List<CalendarioFaseFF2022> calendarioFaseFFList, Fases2022 faseAnterior, Boleras2022 boleraFinal) {
		ModeloParejasFinal4SF2022 modeloFinal4SF = new ModeloParejasFinal4SF2022();
		Integer id = 1;
		
		for (Entry<Integer, ModeloParejasEnfrentamientoFinal2022> entry : modeloFinal4SF.getEnfrentamientosFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloParejasEnfrentamientoFinal2022 enfrentamiento = (ModeloParejasEnfrentamientoFinal2022) value;
			//Agregar calendario
			CalendarioFaseFF2022 calendarioFaseFF = new CalendarioFaseFF2022();
			calendarioFaseFF.setId(id);
			calendarioFaseFF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2022.TERCERA);
			calendarioFaseFF.setNumeroEnfrentamiento(id);
			calendarioFaseFF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseFF.setPareja1Id(0);
			calendarioFaseFF.setJuegosPareja1P1(0);
			calendarioFaseFF.setJuegosPareja1P2(0);
			calendarioFaseFF.setJuegosPareja1P3(0);
			calendarioFaseFF.setJuegosPareja2P1(0);
			calendarioFaseFF.setJuegosPareja2P2(0);
			calendarioFaseFF.setJuegosPareja2P3(0);
			calendarioFaseFF.setPareja2Id(0);
			calendarioFaseFF.setOrden(key);
			calendarioFaseFF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseFF.setActivo(Activo2022.SI);
			calendarioFaseFF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseFF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseFF.setGrupoProcedenciaPareja1(enfrentamiento.getGrupoProcedenciaPareja1());
			calendarioFaseFF.setPosicionProcedenciaPareja1(enfrentamiento.getPosicionProcedenciaPareja1());
			calendarioFaseFF.setGrupoProcedenciaPareja2(enfrentamiento.getGrupoProcedenciaPareja2());
			calendarioFaseFF.setPosicionProcedenciaPareja2(enfrentamiento.getPosicionProcedenciaPareja2());
			
			//Asignar la bolera por parte del cuadro
			calendarioFaseFF.setBoleraId(boleraFinal.getId());
			id++;
			calendarioFaseFFList.add(calendarioFaseFF);
		}
		return calendarioFaseFFList;
	}

	public String getBolerasOcupadasFI() {
		return bolerasOcupadasFI;
	}

	public void setBolerasOcupadasFI(String bolerasOcupadasFI) {
		this.bolerasOcupadasFI = bolerasOcupadasFI;
	}

	public String getBolerasOcupadasOFCF() {
		return bolerasOcupadasOFCF;
	}

	public void setBolerasOcupadasOFCF(String bolerasOcupadasOFCF) {
		this.bolerasOcupadasOFCF = bolerasOcupadasOFCF;
	}
	
	public void doBorrarSorteo(){
		crearCampeonatoMasculinoParejas.borrarSorteo();
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "El Sorteo se ha borrado correctamente. Se puede volver a sortear.", null));
		//Inicializar variables de nuevo
		leerCampeonatoMasculinoParejasTercera = new LeerCampeonatoMasculinoParejasTercera2022();
		//Redirigir
		//redirect.redirect(menuView.PAG_OM_ADD_CAMPEONATO_MASCULINO_PAREJAS_TERCERA);
	}
	
	public void doConfirmarSorteoCampeonato(){
		crearCampeonatoMasculinoParejas.actualizarEstadoConfiguracion(EstadosIndividualParejas2022.CONFIG_ESTADO_FINALIZADO);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "El Sorteo se ha confirmado correctamente.", null));
		//Inicializar variables de nuevo
		leerCampeonatoMasculinoParejasTercera = new LeerCampeonatoMasculinoParejasTercera2022();
		//Redirigir
		//redirect.redirect(menuView.PAG_OM_ADD_CAMPEONATO_MASCULINO_PAREJAS_TERCERA);		
	}
	
	

}
