package com.bolocelta.facade.add;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.bolocelta.application.MenuView;
import com.bolocelta.application.Redirect;
import com.bolocelta.application.utils.DateUtils2022;
import com.bolocelta.application.utils.NumberUtils2022;
import com.bolocelta.bbdd.constants.Activo2022;
import com.bolocelta.bbdd.constants.EstadosIndividualParejas2022;
import com.bolocelta.bbdd.constants.FasesModelo2022;
import com.bolocelta.bbdd.constants.GruposLetra2022;
import com.bolocelta.bbdd.constants.TipoEnfrentamiento2022;
import com.bolocelta.bbdd.constants.structure.EstructuraCampeonatoFemeninoIndividual2022;
import com.bolocelta.bbdd.createTable.CrearCampeonatoIndividualCadetes2022;
import com.bolocelta.bbdd.readTables.LeerBoleras2022;
import com.bolocelta.bbdd.readTables.LeerCampeonatoCadetesIndividual2022;
import com.bolocelta.entities.Boleras2022;
import com.bolocelta.entities.CampeonatoCadetesIndividualClasificacion2022;
import com.bolocelta.entities.Categorias2022;
import com.bolocelta.entities.Configuracion2022;
import com.bolocelta.entities.Fases2022;
import com.bolocelta.entities.Jugadores2022;
import com.bolocelta.entities.ParticipantesIndividual2022;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseCF2022;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseFC2022;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseFF2022;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseI2022;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseII2022;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseOF2022;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseSF2022;
import com.bolocelta.entities.sorteos.individual.ClasificacionFaseI2022;
import com.bolocelta.entities.sorteos.individual.ClasificacionFaseII2022;
import com.bolocelta.entities.sorteos.individual.Grupos2022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualCuartosFinal2Grupos2022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualCuartosFinal4Boladas2022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualCuartosFinal4Grupos2022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualCuartosFinal4OF2022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualEnfrentamientoCuartosFinal2022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualEnfrentamientoFinal2022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualEnfrentamientoFinalConsolacion2022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualEnfrentamientoGruposFaseI2022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualEnfrentamientoOctavosFinal2022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualEnfrentamientoSemiFinal2022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualFinal4SF2022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualFinalConsolacion4SF2022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualGrupos32022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualGrupos42022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualGrupos52022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualGrupos62022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualGrupos72022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualGrupos82022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualOctavosFinal4Grupos2022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualOctavosFinal8Grupos2022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualSemiFinal4Boladas2022;
import com.bolocelta.entities.sorteos.modelosGrupos.ModeloIndividualSemiFinal4CF2022;

@Named
@ManagedBean
@RequestScoped
public class CampeonatoCadetesIndividualAddFacade2022 implements Serializable{

	@Inject
	private Redirect redirect;
	
	@Inject
	private MenuView menuView;
	
	private static final long serialVersionUID = 1L;
	
	private LeerCampeonatoCadetesIndividual2022 leerCampeonatoCadetesIndividual = new LeerCampeonatoCadetesIndividual2022();
	private CrearCampeonatoIndividualCadetes2022 crearCampeonatoCadetesIndividual = new CrearCampeonatoIndividualCadetes2022();
	
	private LeerBoleras2022 leerBoleras = new LeerBoleras2022();
	
	private String diaFechaMaxInscripcion = null;
	private String mesFechaMaxInscripcion = null;
	private String anyoFechaMaxInscripcion = null;
	
	private Date fechaMaxInscripcion = null;
	private String estadoCampeonato = null;
	private String bolera = null;
	private String observacionesCampeonato1 = null;
	private String observacionesCampeonato2 = null;
	private String observacionesCampeonato3 = null;
	private String observacionesCampeonato4 = null;
	private String observacionesCampeonato5 = null;
	private String observacionesCampeonato6 = null;
	private String observacionesCampeonato7 = null;
	private String bolerasOcupadasFI = null;
	private String bolerasOcupadasOFCF = null;
	
	private List<Configuracion2022> resultListConfiguracion = null;
	private List<Fases2022> resultListFases = null;
	private List<ParticipantesIndividual2022> resultListParticipantesIndividual = null;
	
	public CampeonatoCadetesIndividualAddFacade2022() {
		Date fechaMaxInscripcion = leerCampeonatoCadetesIndividual.getFechaMaxInscripcion();
		if(fechaMaxInscripcion != null){
			setDiaFechaMaxInscripcion(String.valueOf(fechaMaxInscripcion.getDate()));
			setMesFechaMaxInscripcion(String.valueOf(fechaMaxInscripcion.getMonth()+1));
			setAnyoFechaMaxInscripcion(String.valueOf(fechaMaxInscripcion.getYear()+1900));
		}
		setEstadoCampeonato(leerCampeonatoCadetesIndividual.getEstadoCampeonato());
		setBolera(leerCampeonatoCadetesIndividual.getBoleraFinal());
		setObservacionesCampeonato1(leerCampeonatoCadetesIndividual.getObservacionesCampeonato1());
		setObservacionesCampeonato2(leerCampeonatoCadetesIndividual.getObservacionesCampeonato2());
		setObservacionesCampeonato3(leerCampeonatoCadetesIndividual.getObservacionesCampeonato3());
		setObservacionesCampeonato4(leerCampeonatoCadetesIndividual.getObservacionesCampeonato4());
		setObservacionesCampeonato5(leerCampeonatoCadetesIndividual.getObservacionesCampeonato5());
		setObservacionesCampeonato6(leerCampeonatoCadetesIndividual.getObservacionesCampeonato6());
		setObservacionesCampeonato7(leerCampeonatoCadetesIndividual.getObservacionesCampeonato7());
		setBolerasOcupadasFI(leerCampeonatoCadetesIndividual.getBolerasOcupadasFI());
		setBolerasOcupadasOFCF(leerCampeonatoCadetesIndividual.getBolerasOcupadasOFCF());
	}

	public List<Configuracion2022> getResultListConfiguracion() {
		if(resultListConfiguracion == null){
			resultListConfiguracion = (List<Configuracion2022>) leerCampeonatoCadetesIndividual.listResultConfig();
		}
		return resultListConfiguracion;
	}
	
//	public List<Fases2022> getResultListFases() {
//		if(resultListFases == null){
//			resultListFases = (List<Fases2022>) leerCampeonatoCadetesIndividual.listResultFases();
//		}
//		return resultListFases;
//	}
	
	public List<ParticipantesIndividual2022> getResultListParticipantes() {
		if(resultListParticipantesIndividual == null){
			resultListParticipantesIndividual = (List<ParticipantesIndividual2022>) leerCampeonatoCadetesIndividual.listResultParticipantesOrderByEquipo();
		}
		return resultListParticipantesIndividual;
	}
	
	//NUEVO
	public List<ParticipantesIndividual2022> getResultListParticipantesByEquipos(Set<Integer> keySetequipoMasJugadoresQueGruposLibreGrupo) {
		if(resultListParticipantesIndividual == null){
			resultListParticipantesIndividual = (List<ParticipantesIndividual2022>) leerCampeonatoCadetesIndividual.listResultParticipantesOrderByEquipo();
		}
		List<ParticipantesIndividual2022> resultListParticipantesIndividualMasJugadoresQueGrupos = new ArrayList<>();
		for (ParticipantesIndividual2022 participantesIndividual : resultListParticipantesIndividual) {
			if(keySetequipoMasJugadoresQueGruposLibreGrupo.contains(participantesIndividual.getJugador().getEquipoId())){
				resultListParticipantesIndividualMasJugadoresQueGrupos.add(participantesIndividual);
			}
		}
		return resultListParticipantesIndividualMasJugadoresQueGrupos;
	}
	//F-NUEVO
	
	public Integer getTotalRowsParticipantes(){
		if(resultListParticipantesIndividual == null){
			getResultListParticipantes();
		}
		if(resultListParticipantesIndividual != null){
			return resultListParticipantesIndividual.size();
		}
		return 0;
	}
	
	public Integer getMaxRowsPaginator(){
		return 8;
	}
	
	public boolean isEnabledPaginatorParticipantes(){
		return (getResultListParticipantes() != null && getResultListParticipantes() != null) ? 
					(getResultListParticipantes().size() > getMaxRowsPaginator()) : false;
	}
	
	//Manejo de estados
	public boolean isEstadoCampeonatoSinCrear(){
		return leerCampeonatoCadetesIndividual.isEstadoCampeonatoSinCrear();
	}
	
	public boolean isAbrirInscripciones(){
		return leerCampeonatoCadetesIndividual.isAbrirInscripciones();
	}
	
	public boolean isCerrarInscripciones(){
		return leerCampeonatoCadetesIndividual.isCerrarInscripciones();
	}
	
	public boolean isInscripcionesCerradas(){
		return leerCampeonatoCadetesIndividual.isInscripcionesCerradas();
	}
	
	public boolean isFasesCerradas(){
		return leerCampeonatoCadetesIndividual.isFasesCerradas();
	}
	
	public boolean isSorteoRealizado(){
		return leerCampeonatoCadetesIndividual.isSorteoRealizado();
	}
	
	public boolean isSorteoFinalizado(){
		return leerCampeonatoCadetesIndividual.isSorteoFinalizado();
	}
	
	//Acciones para los diferentes estados
	
	private boolean validateFecha() {
		if(getAnyoFechaMaxInscripcion() != null && getMesFechaMaxInscripcion() != null && getDiaFechaMaxInscripcion() != null){
			if(NumberUtils2022.isNumber(getAnyoFechaMaxInscripcion()) && NumberUtils2022.isNumber(getMesFechaMaxInscripcion()) && NumberUtils2022.isNumber(getDiaFechaMaxInscripcion())){
				Integer dia = Integer.valueOf(getDiaFechaMaxInscripcion());
				if(dia > 31){
					return false;
				}
				Integer mes = Integer.valueOf(getMesFechaMaxInscripcion());
				if(mes > 12){
					return false;
				}
				Integer anyo = Integer.valueOf(getAnyoFechaMaxInscripcion());
				String fecha = (dia < 10 ? "0" + dia : dia) + "/" + (mes < 10 ? "0" + mes : mes) + "/" + anyo;
				Date fechaConvert = DateUtils2022.convertStringToDate(fecha);
				if(fechaConvert == null){
					return false;
				}else{
					setFechaMaxInscripcion(fechaConvert);
					return true;
				}
			}
		}
		return false;
		
	}
	
	public void doCrearBaseCampeonato(){
		
		boolean error = validateFecha();
		if(error){
			crearCampeonatoCadetesIndividual.crearBaseCampeonato(getFechaMaxInscripcion(), EstadosIndividualParejas2022.CONFIG_ESTADO_CREAR, getBolera());
			//Inicializar variables de nuevo
			leerCampeonatoCadetesIndividual = new LeerCampeonatoCadetesIndividual2022();
			//Redirigir
			redirect.redirect(menuView.PAG_OM_ADD_CAMPEONATO_CADETES_INDIVIDUAL);
		}else{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "La Fecha max. inscripci�n no es valida.", null));
		}
	}
	
	public void doAbrirInscripcionesCampeonato(){
		crearCampeonatoCadetesIndividual.actualizarEstadoConfiguracion(EstadosIndividualParejas2022.CONFIG_ESTADO_ABRIR_INSCRIPCIONES);
		//Inicializar variables de nuevo
		leerCampeonatoCadetesIndividual = new LeerCampeonatoCadetesIndividual2022();
		//Redirigir
		redirect.redirect(menuView.PAG_OM_ADD_CAMPEONATO_CADETES_INDIVIDUAL);
	}
	
	public void doCerrarInscripcionesCampeonato(){
		crearCampeonatoCadetesIndividual.actualizarEstadoConfiguracion(EstadosIndividualParejas2022.CONFIG_ESTADO_CERRAR_INSCRIPCIONES);
		//Inicializar variables de nuevo
		leerCampeonatoCadetesIndividual = new LeerCampeonatoCadetesIndividual2022();
		//Redirigir
		redirect.redirect(menuView.PAG_OM_ADD_CAMPEONATO_CADETES_INDIVIDUAL);
	}
	
	public void doInscribirJugador(Jugadores2022 jugador, Categorias2022 categoria, String usuario, String activo){
		ParticipantesIndividual2022 participantesIndividual = leerCampeonatoCadetesIndividual.existeJugadorComoParticipante(jugador.getId());
		//Si existe se actualiza, sino se a�ade nuevo
		if(participantesIndividual == null){
			crearCampeonatoCadetesIndividual.inscribirNewJugador(jugador, categoria, usuario, activo);
		}else{
			crearCampeonatoCadetesIndividual.inscribirUpdateJugador(participantesIndividual, jugador, categoria, usuario, activo);
		}
		//Inicializar variables de nuevo
		leerCampeonatoCadetesIndividual = new LeerCampeonatoCadetesIndividual2022();
	}
	
	public Date getFechaMaxInscripcion() {
		return fechaMaxInscripcion;
	}

	public void setFechaMaxInscripcion(Date fechaMaxInscripcion) {
		this.fechaMaxInscripcion = fechaMaxInscripcion;
	}
	
	public String getBolera() {
		return bolera;
	}

	public void setBolera(String bolera) {
		this.bolera = bolera;
	}
	
	public String getNombreBolera(){
		if(this.bolera != null && this.bolera.length() > 0){
			return leerBoleras.read(Integer.valueOf(this.bolera)).getBolera();
		}
		return "";
	}
	
	public String getObservacionesCampeonato1() {
		return observacionesCampeonato1;
	}

	public void setObservacionesCampeonato1(String observacionesCampeonato1) {
		this.observacionesCampeonato1 = observacionesCampeonato1;
	}
	
	public String getObservacionesCampeonato2() {
		return observacionesCampeonato2;
	}

	public void setObservacionesCampeonato2(String observacionesCampeonato2) {
		this.observacionesCampeonato2 = observacionesCampeonato2;
	}
	
	public String getObservacionesCampeonato3() {
		return observacionesCampeonato3;
	}

	public void setObservacionesCampeonato3(String observacionesCampeonato3) {
		this.observacionesCampeonato3 = observacionesCampeonato3;
	}
	
	public String getObservacionesCampeonato4() {
		return observacionesCampeonato4;
	}

	public void setObservacionesCampeonato4(String observacionesCampeonato4) {
		this.observacionesCampeonato4 = observacionesCampeonato4;
	}
	
	public String getObservacionesCampeonato5() {
		return observacionesCampeonato5;
	}

	public void setObservacionesCampeonato5(String observacionesCampeonato5) {
		this.observacionesCampeonato5 = observacionesCampeonato5;
	}
	
	public String getObservacionesCampeonato6() {
		return observacionesCampeonato6;
	}

	public void setObservacionesCampeonato6(String observacionesCampeonato6) {
		this.observacionesCampeonato6 = observacionesCampeonato6;
	}
	
	public String getObservacionesCampeonato7() {
		return observacionesCampeonato7;
	}

	public void setObservacionesCampeonato7(String observacionesCampeonato7) {
		this.observacionesCampeonato7 = observacionesCampeonato7;
	}

	public String getEstadoCampeonato() {
		return estadoCampeonato;
	}

	public void setEstadoCampeonato(String estadoCampeonato) {
		this.estadoCampeonato = estadoCampeonato;
	}
	
	public String getDiaFechaMaxInscripcion() {
		return diaFechaMaxInscripcion;
	}

	public void setDiaFechaMaxInscripcion(String diaFechaMaxInscripcion) {
		this.diaFechaMaxInscripcion = diaFechaMaxInscripcion;
	}

	public String getMesFechaMaxInscripcion() {
		return mesFechaMaxInscripcion;
	}

	public void setMesFechaMaxInscripcion(String mesFechaMaxInscripcion) {
		this.mesFechaMaxInscripcion = mesFechaMaxInscripcion;
	}

	public String getAnyoFechaMaxInscripcion() {
		return anyoFechaMaxInscripcion;
	}

	public void setAnyoFechaMaxInscripcion(String anyoFechaMaxInscripcion) {
		this.anyoFechaMaxInscripcion = anyoFechaMaxInscripcion;
	}
	
	public void doCambiarActivarFase(Fases2022 fase){
		if(fase.getActivo().equalsIgnoreCase(Activo2022.SI)){
			fase.setActivo(Activo2022.NO);
		}else if(fase.getActivo().equalsIgnoreCase(Activo2022.NO)){
			fase.setActivo(Activo2022.SI);
		}
		crearCampeonatoCadetesIndividual.actualizarActivoFase(fase);
		leerCampeonatoCadetesIndividual = new LeerCampeonatoCadetesIndividual2022();
	}
	
	public void doActualizarDatos(Fases2022 fase){
		crearCampeonatoCadetesIndividual.actualizarFase(fase);
		leerCampeonatoCadetesIndividual = new LeerCampeonatoCadetesIndividual2022();
	}

	public void doCerrarFasesCampeonato(){
		//Inicializar variables de nuevo
		leerCampeonatoCadetesIndividual = new LeerCampeonatoCadetesIndividual2022();
		for(Fases2022 fase : getResultListFases()){
			if(fase.getActivo().equals(Activo2022.SI)){
				if(fase.getNumero().equals(FasesModelo2022.FASE_CLAS)){
					crearCampeonatoCadetesIndividual.crearClasificacionBoladas();
				}else if(fase.getNumero().equals(FasesModelo2022.FASE_I)){
					crearCampeonatoCadetesIndividual.crearClasificacionFaseI();
					crearCampeonatoCadetesIndividual.crearCalendarioFaseI();
				}else if(fase.getNumero().equals(FasesModelo2022.FASE_II)){
					crearCampeonatoCadetesIndividual.crearCalendarioFaseII();
					crearCampeonatoCadetesIndividual.crearClasificacionFaseII();
				}else if(fase.getNumero().equals(FasesModelo2022.FASE_OF)){
					crearCampeonatoCadetesIndividual.crearCalendarioFaseOF();
				}else if(fase.getNumero().equals(FasesModelo2022.FASE_CF)){
					crearCampeonatoCadetesIndividual.crearCalendarioFaseCF();
				}else if(fase.getNumero().equals(FasesModelo2022.FASE_SF)){
					crearCampeonatoCadetesIndividual.crearCalendarioFaseSF();
				}else if(fase.getNumero().equals(FasesModelo2022.FASE_FC)){
					crearCampeonatoCadetesIndividual.crearCalendarioFaseFC();
				}else if(fase.getNumero().equals(FasesModelo2022.FASE_FF)){
					crearCampeonatoCadetesIndividual.crearCalendarioFaseFF();
				}
			}
		}
		crearCampeonatoCadetesIndividual.actualizarEstadoConfiguracion(EstadosIndividualParejas2022.CONFIG_ESTADO_CERRAR_FASES);
		//Inicializar variables de nuevo
		leerCampeonatoCadetesIndividual = new LeerCampeonatoCadetesIndividual2022();
		//Redirigir
		redirect.redirect(menuView.PAG_OM_ADD_CAMPEONATO_CADETES_INDIVIDUAL);
	}
	
	public void doRealizarSorteoCampeonato(){
		
		boolean sorteoRealizado = false;
		
		try {
		
			//Asignar las observaciones del campeonato
			EstructuraCampeonatoFemeninoIndividual2022 ecmi = new EstructuraCampeonatoFemeninoIndividual2022();
			crearCampeonatoCadetesIndividual.updateRow(ecmi.getEstructuraObservacionesCampeonato1(), ecmi.getEstructuraObservacionesCampeonato1().getFila(), observacionesCampeonato1);
			crearCampeonatoCadetesIndividual.updateRow(ecmi.getEstructuraObservacionesCampeonato2(), ecmi.getEstructuraObservacionesCampeonato2().getFila(), observacionesCampeonato2);
			crearCampeonatoCadetesIndividual.updateRow(ecmi.getEstructuraObservacionesCampeonato3(), ecmi.getEstructuraObservacionesCampeonato3().getFila(), observacionesCampeonato3);
			crearCampeonatoCadetesIndividual.updateRow(ecmi.getEstructuraObservacionesCampeonato4(), ecmi.getEstructuraObservacionesCampeonato4().getFila(), observacionesCampeonato4);
			crearCampeonatoCadetesIndividual.updateRow(ecmi.getEstructuraObservacionesCampeonato5(), ecmi.getEstructuraObservacionesCampeonato5().getFila(), observacionesCampeonato5);
			crearCampeonatoCadetesIndividual.updateRow(ecmi.getEstructuraObservacionesCampeonato6(), ecmi.getEstructuraObservacionesCampeonato6().getFila(), observacionesCampeonato6);
			crearCampeonatoCadetesIndividual.updateRow(ecmi.getEstructuraObservacionesCampeonato7(), ecmi.getEstructuraObservacionesCampeonato7().getFila(), observacionesCampeonato7);
			crearCampeonatoCadetesIndividual.updateRow(ecmi.getEstructuraBolerasOcupadasFI(), ecmi.getEstructuraBolerasOcupadasFI().getFila(), bolerasOcupadasFI);
			crearCampeonatoCadetesIndividual.updateRow(ecmi.getEstructuraBolerasOcupadasOFCF(), ecmi.getEstructuraBolerasOcupadasOFCF().getFila(), bolerasOcupadasOFCF);
			
			
			
			//Inicializar variables de nuevo
			leerCampeonatoCadetesIndividual = new LeerCampeonatoCadetesIndividual2022();
			//Crear los mapas para el sorteo
			//FaseI
			LinkedHashMap<Integer, CalendarioFaseI2022> calendarioFaseIMap = new LinkedHashMap<>();
			LinkedHashMap<Integer, ClasificacionFaseI2022> clasificacionFaseIMap = new LinkedHashMap<>();
			//FaseII
			LinkedHashMap<Integer, CalendarioFaseII2022> calendarioFaseIIMap = new LinkedHashMap<>();
			LinkedHashMap<Integer, ClasificacionFaseII2022> clasificacionFaseIIMap = new LinkedHashMap<>();
			//FaseOF
			LinkedHashMap<Integer, CalendarioFaseOF2022> calendarioFaseOFMap = new LinkedHashMap<>();
			//FaseCF
			LinkedHashMap<Integer, CalendarioFaseCF2022> calendarioFaseCFMap = new LinkedHashMap<>();
			//FaseSF
			LinkedHashMap<Integer, CalendarioFaseSF2022> calendarioFaseSFMap = new LinkedHashMap<>();
			//FaseFC
			LinkedHashMap<Integer, CalendarioFaseFC2022> calendarioFaseFCMap = new LinkedHashMap<>();
			//FaseFF
			LinkedHashMap<Integer, CalendarioFaseFF2022> calendarioFaseFFMap = new LinkedHashMap<>();
			
			//1. Obtener las boleras disponibles, en principio solo las federadas.
			LinkedHashMap<Integer, String> bolerasGrupoAsignadasMap = new LinkedHashMap<>();
			LeerBoleras2022 leerBoleras = new LeerBoleras2022();
			List<Boleras2022> resultListBoleras = (List<Boleras2022>) leerBoleras.listResultFederadas();
			
			//Fase primera
			Integer numerofaseAnterior = 1;
			Integer numerofaseSiguiente = 1;
			
			//Bolera de la final
			Boleras2022 boleraFinal = null;
			for (Configuracion2022 configuracion : getResultListConfiguracion()) {
				if(configuracion.getNombre().equals("Bolera Final")){
					boleraFinal = leerBoleras.read(Integer.valueOf(configuracion.getValor()));
				}
			}
			
			//Bolera de la final
			LinkedHashMap<Integer, Boleras2022> bolerasOcupadasFIMap =  new LinkedHashMap<>();
			for (Configuracion2022 configuracion : getResultListConfiguracion()) {
				if(configuracion.getNombre().equals("Boleras Ocupadas FI")){
					String bolerasOcupadas = configuracion.getValor();
					String[] bolerasOcupadasList = bolerasOcupadas.split("-");
					for (String bolera : bolerasOcupadasList) {
						Integer idBolera = Integer.valueOf(bolera);
						if(idBolera != null && idBolera > 0){
							Boleras2022 boleraObject = leerBoleras.read(idBolera);
							if(boleraObject != null){
								bolerasOcupadasFIMap.put(idBolera, boleraObject);
							}
						}
					}
				}
			}
			
			//Bolera de la final
			LinkedHashMap<Integer, Boleras2022> bolerasOcupadasOFCFMap =  new LinkedHashMap<>();
			for (Configuracion2022 configuracion : getResultListConfiguracion()) {
				if(configuracion.getNombre().equals("Boleras Ocupadas OFCF")){
					String bolerasOcupadas = configuracion.getValor();
					String[] bolerasOcupadasList = bolerasOcupadas.split("-");
					for (String bolera : bolerasOcupadasList) {
						Integer idBolera = Integer.valueOf(bolera);
						if(idBolera != null && idBolera > 0){
							Boleras2022 boleraObject = leerBoleras.read(idBolera);
							if(boleraObject != null){
								bolerasOcupadasOFCFMap.put(idBolera, boleraObject);
							}
						}
					}
				}
			}
			
			//2. Definir la faseI
			for(Fases2022 fase : getResultListFases()){
				if(fase.getActivo().equals(Activo2022.SI)){
					
					
					if(fase.getNumero().equals(FasesModelo2022.FASE_CLAS)){
						//Agregar los participantes
						//Se realiza sorteo de orden de tirada
						Integer totalParticipantes = getResultListParticipantes().size();
						Integer totalParticipantesAsignados = 0;
						HashMap<Integer, ParticipantesIndividual2022> participantesAsignados = new HashMap<>();
						numerofaseSiguiente = fase.getFaseSiguiente();
						numerofaseAnterior = fase.getId();
						while (totalParticipantesAsignados < totalParticipantes){
							Random rand = new Random();
							ParticipantesIndividual2022 participante = getResultListParticipantes().get(rand.nextInt(getResultListParticipantes().size()));
							if(!participantesAsignados.containsKey(participante.getIdJugador())){
								CampeonatoCadetesIndividualClasificacion2022 clasificacionBoladas = new CampeonatoCadetesIndividualClasificacion2022();
								clasificacionBoladas = prepareClasificacionBoladas(clasificacionBoladas, participante, totalParticipantesAsignados, boleraFinal, fase);
								crearCampeonatoCadetesIndividual.insertarRowClasificacionBoladas(clasificacionBoladas.getInsertRow());		
								totalParticipantesAsignados++;
								participantesAsignados.put(participante.getIdJugador(), participante);
							}
						}
					}
					
					
					if(fase.getNumero().equals(FasesModelo2022.FASE_I)){
						if(numerofaseSiguiente.equals(fase.getNumero())){
						
							//Objetos
							//Grupos Fase I
							LinkedHashMap<String, Grupos2022> gruposFaseIMap = new LinkedHashMap<>();
							//Jugadores Asignados
							LinkedHashMap<Integer, ParticipantesIndividual2022> jugadoresAsignados = new LinkedHashMap<>();
							
							//2.1. Obtener el numero de grupos de la fase
							Integer numeroGruposFase = fase.getNumeroEnfrentamientos();
							Integer numeroGruposFaseAsignados = 0;
							
							//2.2. Obtener el numero de participantes para saber el numero de jugadores por grupo y los asignados al grupo para
							Integer numeroParticipantes = getResultListParticipantes().size();
							Integer jugadoresPorGrupoAsignados = 0;
							
							
							//NUEVO
							//Equipos mismos o mas jugadores que grupos (idEquipo, numeroJugadores)
							LinkedHashMap<Integer, Integer> equipoMasJugadoresQueGrupos = new LinkedHashMap<>();
							for (ParticipantesIndividual2022 participante : getResultListParticipantes()) {
								if(equipoMasJugadoresQueGrupos.containsKey(participante.getJugador().getEquipoId())){
									Integer jugadoresEquipoValue = equipoMasJugadoresQueGrupos.get(participante.getJugador().getEquipoId());
									equipoMasJugadoresQueGrupos.put(participante.getJugador().getEquipoId(), jugadoresEquipoValue + 1 );
								}else{
									equipoMasJugadoresQueGrupos.put(participante.getJugador().getEquipoId(), 1);
								}
							}
							//Lista de equipos con mas jugadores que grupos
							LinkedHashMap<Integer, Integer> equipoMasJugadoresQueGruposLibre = (LinkedHashMap<Integer, Integer>) equipoMasJugadoresQueGrupos.clone();
							//Limpiar equipos que tengan menos jugadores que grupos
							Set<Integer> keySet = equipoMasJugadoresQueGrupos.keySet();
							for (Integer idEquipoKey : keySet) {
								Integer jugadoresEquipoValue = equipoMasJugadoresQueGrupos.get(idEquipoKey);
								if(jugadoresEquipoValue < numeroGruposFase){
									equipoMasJugadoresQueGruposLibre.remove(idEquipoKey);
								}
							}
							//F-NUEVO
							
							
							
							//2.3. Crear los grupos a participar
							Integer gruposCreados = 1;
							while(gruposCreados <= numeroGruposFase){
								Grupos2022 grupos = new Grupos2022();
								grupos.setId(GruposLetra2022.getGruposLetraFaseI(gruposCreados));
								grupos.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2022.JUVENILES);
								grupos.setFecha(fase.getFecha());
								grupos.setHora(fase.getHora());
								//Asignar bolera aleatoriamente
								Boleras2022 boleraAsignar = null;
								boolean boleraAsignada = false;
								
								//Quito las boleras ocupadas
								List<Boleras2022> resultListBolerasLibres = new ArrayList<>();
								for (Boleras2022 boleras : resultListBoleras) {
									if(!bolerasOcupadasFIMap.containsKey(boleras.getId())){
										resultListBolerasLibres.add(boleras);
									}
								}
								
								
								
								while(!boleraAsignada){
									Random rand = new Random();
									boleraAsignar = resultListBolerasLibres.get(rand.nextInt(resultListBolerasLibres.size()));
									//if(!boleraAsignar.getId().equals(boleraFinal.getId())){
										if(!bolerasOcupadasFIMap.containsKey(boleraAsignar.getId())){
											if(!bolerasGrupoAsignadasMap.containsKey(boleraAsignar.getId())){
												boleraAsignada = true;
											}
										}
									//}
								}
								grupos.setBoleraId(boleraAsignar.getId());
								
								
								//Obtener el numero de jugadores del grupo
								BigDecimal jugadoresPendientesAsignar = new BigDecimal(numeroParticipantes - jugadoresPorGrupoAsignados);
								BigDecimal gruposPendientesAsignar = new BigDecimal(numeroGruposFase - numeroGruposFaseAsignados);
								BigDecimal jugadoresPorGrupoCalculo = jugadoresPendientesAsignar.divide(gruposPendientesAsignar, RoundingMode.UP);
								grupos.setJugadores(jugadoresPorGrupoCalculo.intValue());
								
								//Agregar los jugadores para cada grupo a sorteo
								//A cada grupo sortear los jugadores, se obtienen por equipos de mayor numero de jugadores a menor
								//Limitaciones: 1-  Se han de repartir los jugadores de equipo por grupo, 
								//					si hay mas jugadores del equipo por grupo se asigna a alguno de los grupos
								Integer jugadoresAsignadosGrupo = 0;
								
								//NUEVO
								//Lista de equipos con mas jugadores que grupos para cada grupo
								LinkedHashMap<Integer, Integer> equipoMasJugadoresQueGruposLibreGrupo = (LinkedHashMap<Integer, Integer>) equipoMasJugadoresQueGruposLibre.clone();
								//F-NUEVO
								
								Integer reintentosCancelarSorteo = 0;
								
								Integer reintentosEquipoRepetido = 0;
								while(jugadoresAsignadosGrupo < jugadoresPorGrupoCalculo.intValue()){
									
									Random rand = new Random();
									
									
									//NUEVO
									ParticipantesIndividual2022 jugadorSorteo = null;
									
									//Primero buscar jugadores pertenecientes a los equipos que tienen los mismos o mas jugadores que grupos
									Set<Integer> keySetequipoMasJugadoresQueGruposLibreGrupo = equipoMasJugadoresQueGruposLibreGrupo.keySet();
									if(keySetequipoMasJugadoresQueGruposLibreGrupo.size() > 0){
										List<ParticipantesIndividual2022> list = getResultListParticipantesByEquipos(keySetequipoMasJugadoresQueGruposLibreGrupo);
										boolean jugadorCorrecto = false;
										while(!jugadorCorrecto){

											reintentosCancelarSorteo++;
											
											if(reintentosCancelarSorteo > 250000){
												throw new Exception();
											}
											
											jugadorSorteo = list.get(rand.nextInt(list.size()));
											if(!jugadoresAsignados.containsKey(jugadorSorteo.getIdJugador())){
												equipoMasJugadoresQueGruposLibreGrupo.remove(jugadorSorteo.getJugador().getEquipoId());
												jugadorCorrecto = true;
											}
										}
									}else{
										boolean jugadorCorrecto = false;
										while(!jugadorCorrecto){
											
											reintentosCancelarSorteo++;
											
											if(reintentosCancelarSorteo > 250000){
												throw new Exception();
											}
											
											jugadorSorteo = getResultListParticipantes().get(rand.nextInt(getResultListParticipantes().size()));
											if(!jugadoresAsignados.containsKey(jugadorSorteo.getIdJugador())){
												if(equipoMasJugadoresQueGruposLibreGrupo.size() > 0){
													equipoMasJugadoresQueGruposLibreGrupo.remove(jugadorSorteo.getJugador().getEquipoId());
												}
												jugadorCorrecto = true;
											}
										}
										
									}
									
									//F-NUEVO
									
									
									
									if(!grupos.getJugadoresGrupoList().containsKey(jugadorSorteo.getIdJugador())){
										if(!jugadoresAsignados.containsKey(jugadorSorteo.getIdJugador())){
											if(!grupos.isExisteJugadorMismoEquipo(jugadorSorteo.getJugador().getEquipoId(), equipoMasJugadoresQueGrupos, numeroGruposFase)){
												jugadorSorteo.setNumeroJugadorGrupo(jugadoresAsignadosGrupo + 1);
												grupos.getJugadoresGrupoList().put(jugadorSorteo.getIdJugador(), jugadorSorteo);
												jugadoresAsignadosGrupo++;
												jugadoresAsignados.put(jugadorSorteo.getIdJugador(), jugadorSorteo);
											}else{
												if(reintentosEquipoRepetido >50){
													jugadorSorteo.setNumeroJugadorGrupo(jugadoresAsignadosGrupo + 1);
													grupos.getJugadoresGrupoList().put(jugadorSorteo.getIdJugador(), jugadorSorteo);
													jugadoresAsignadosGrupo++;
													reintentosEquipoRepetido = 0;
													jugadoresAsignados.put(jugadorSorteo.getIdJugador(), jugadorSorteo);
												}else{
													reintentosEquipoRepetido++;
												}
											}
										}
									}
								}
								
								//Agregar a los mapas
								gruposFaseIMap.put(grupos.getId(), grupos);
								bolerasGrupoAsignadasMap.put(boleraAsignar.getId(), grupos.getId());
								
								//Actualizar variables
								gruposCreados++;
								numeroGruposFaseAsignados++;
								jugadoresPorGrupoAsignados += jugadoresPorGrupoCalculo.intValue();
								
								
							}
							
							//A�adir los datos a el calendario y clasificacion de la fase I
							List<CalendarioFaseI2022> calendarioFaseIList = new ArrayList<CalendarioFaseI2022>();
							List<ClasificacionFaseI2022> clasificacionFaseIList = new ArrayList<ClasificacionFaseI2022>();
							//Se recorren los grupos obtenidos
							//Se a�aden a la clasificacion
							for (Entry<String, Grupos2022> entry : gruposFaseIMap.entrySet()) {
							    Object value = entry.getValue();
							    Grupos2022 grupo = (Grupos2022) value;
							    for (Entry<Integer, ParticipantesIndividual2022> entryPi : grupo.getJugadoresGrupoList().entrySet()) {
							    	Object valuePi = entryPi.getValue();
							    	ParticipantesIndividual2022 participantesIndividual = (ParticipantesIndividual2022) valuePi;
							    	
							    	//Agrego a clasificacion el jugador para el grupo
							    	ClasificacionFaseI2022 cf1 = new ClasificacionFaseI2022();
							    	cf1.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2022.JUVENILES);
							    	cf1.setGrupo(grupo.getId());
							    	cf1.setId(participantesIndividual.getNumeroJugadorGrupo());
							    	cf1.setJugadorId(participantesIndividual.getIdJugador());
							    	cf1.setPj(0);
							    	cf1.setPg(0);
							    	cf1.setPe(0);
							    	cf1.setPp(0);
							    	cf1.setPf(0);
							    	cf1.setPc(0);
							    	cf1.setPt(0);
							    	clasificacionFaseIList.add(cf1);
							    }
							}
							//Se a�aden al calendario
							for (Entry<String, Grupos2022> entry : gruposFaseIMap.entrySet()) {
							    Object value = entry.getValue();
							    Grupos2022 grupo = (Grupos2022) value;
							    //Recorro los enfrentamientos para el modelo del grupo
							    if(grupo.getJugadores().equals(8)){
							    	calendarioFaseIList = prepareCalendarioFaseIModeloGrupos8(calendarioFaseIList, grupo);
							    }else if(grupo.getJugadores().equals(7)){
							    	calendarioFaseIList = prepareCalendarioFaseIModeloGrupos7(calendarioFaseIList, grupo);
							    }else if(grupo.getJugadores().equals(6)){
							    	calendarioFaseIList = prepareCalendarioFaseIModeloGrupos6(calendarioFaseIList, grupo);
							    }else if(grupo.getJugadores().equals(5)){
							    	calendarioFaseIList = prepareCalendarioFaseIModeloGrupos5(calendarioFaseIList, grupo);
							    }else if(grupo.getJugadores().equals(4)){
							    	calendarioFaseIList = prepareCalendarioFaseIModeloGrupos4(calendarioFaseIList, grupo);
							    }else if(grupo.getJugadores().equals(3)){
							    	calendarioFaseIList = prepareCalendarioFaseIModeloGrupos3(calendarioFaseIList, grupo);
							    }
							    
							}
							
							//Insertar clasificacion y calendario fase I
							//Clasificacion
							for (ClasificacionFaseI2022 clasificacionFaseI : clasificacionFaseIList) {
								crearCampeonatoCadetesIndividual.insertarRowClasificacionFaseI(clasificacionFaseI.getInsertRow());
							}
							//Calendario
							for (CalendarioFaseI2022 calendarioFaseI : calendarioFaseIList) {
								crearCampeonatoCadetesIndividual.insertarRowCalendarioFaseI(calendarioFaseI.getInsertRow());
							}
							
							numerofaseSiguiente = fase.getFaseSiguiente();
						}
						
					}
				}
			}
			
			
			//3. Definir la faseII
			for(Fases2022 fase : getResultListFases()){
				if(fase.getActivo().equals(Activo2022.SI)){
					if(fase.getNumero().equals(FasesModelo2022.FASE_II)){
						if(numerofaseSiguiente.equals(fase.getNumero())){
							
						}
					}
				}
			}
			
			//4. Definir la faseOF
			
			Boleras2022 boleraFaseAnterior1 = null;
			Boleras2022 boleraFaseAnterior2 = null;
			
			for(Fases2022 fase : getResultListFases()){
				if(fase.getActivo().equals(Activo2022.SI)){
					if(fase.getNumero().equals(FasesModelo2022.FASE_OF)){
						if(numerofaseSiguiente.equals(fase.getNumero())){
							//Recuperar la fase anterior, para averiguar si era grupos escoger el modelo
							Fases2022 faseAnterior = searchFase(numerofaseAnterior);
							//Asignar boleras aleatoriamente
							Boleras2022 boleraAsignar1 = null;
							Boleras2022 boleraAsignar2 = null;
							boolean boleraAsignada1 = false;
							boolean boleraAsignada2 = false;
							LinkedHashMap<Integer, String> bolerasGrupoAsignadasCFMap = new LinkedHashMap<>();
							while(!boleraAsignada1){
								Random rand = new Random();
								boleraAsignar1 = resultListBoleras.get(rand.nextInt(resultListBoleras.size()));
								if(!boleraAsignar1.getId().equals(boleraFinal.getId())){
									if(!bolerasOcupadasOFCFMap.containsKey(boleraAsignar1.getId())){
										if(!bolerasGrupoAsignadasCFMap.containsKey(boleraAsignar1.getId())){
											boleraAsignada1 = true;
											boleraFaseAnterior1 = boleraAsignar1;
										}
									}
								}
							}
							while(!boleraAsignada2){
								Random rand = new Random();
								boleraAsignar2 = resultListBoleras.get(rand.nextInt(resultListBoleras.size()));
								if(!boleraAsignar2.getId().equals(boleraFinal.getId())){
									if(!bolerasOcupadasOFCFMap.containsKey(boleraAsignar2.getId())){
										if(!bolerasGrupoAsignadasCFMap.containsKey(boleraAsignar2.getId())){
											boleraAsignada2 = true;
											boleraFaseAnterior2 = boleraAsignar2;
										}
									}
								}
							}
							//Si en la fase previa eran 4 grupos
							if(faseAnterior.getTipoEnfrentamiento().equals(TipoEnfrentamiento2022.GRUPOS)){
								if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra2022.FASE_I_II_G_4)){
									List<CalendarioFaseOF2022> calendarioFaseOFList = new ArrayList<CalendarioFaseOF2022>();
									calendarioFaseOFList = prepareCalendarioFaseOFModeloGrupos4(calendarioFaseOFList, faseAnterior, boleraAsignar1, boleraAsignar2);
									//Calendario
									for (CalendarioFaseOF2022 calendarioFaseOF : calendarioFaseOFList) {
										crearCampeonatoCadetesIndividual.insertarRowCalendarioFaseOF(calendarioFaseOF.getInsertRow());
									}
									numerofaseSiguiente = fase.getFaseSiguiente();
									numerofaseAnterior = fase.getNumero();
								}
								//Si en la fase previa eran 8 grupos
								else if(faseAnterior.getTipoEnfrentamiento().equals(TipoEnfrentamiento2022.GRUPOS)){
									if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra2022.FASE_I_II_G_8)){
										List<CalendarioFaseOF2022> calendarioFaseOFList = new ArrayList<CalendarioFaseOF2022>();
										calendarioFaseOFList = prepareCalendarioFaseOFModeloGrupos8(calendarioFaseOFList, faseAnterior, boleraAsignar1, boleraAsignar2);
										//Calendario
										for (CalendarioFaseOF2022 calendarioFaseOF : calendarioFaseOFList) {
											crearCampeonatoCadetesIndividual.insertarRowCalendarioFaseOF(calendarioFaseOF.getInsertRow());
										}
										numerofaseSiguiente = fase.getFaseSiguiente();
										numerofaseAnterior = fase.getNumero();
									}
								}
							}
							
						}
					}
				}
			}
			
			//5. Definir la faseCF
			for(Fases2022 fase : getResultListFases()){
				if(fase.getActivo().equals(Activo2022.SI)){
					if(fase.getNumero().equals(FasesModelo2022.FASE_CF)){
						if(numerofaseSiguiente.equals(fase.getNumero())){
							//Recuperar la fase anterior, para averiguar si era grupos escoger el modelo
							Fases2022 faseAnterior = searchFase(numerofaseAnterior);
							//Si en la fase previa eran 4 grupos
							if(faseAnterior.getTipoEnfrentamiento().equals(TipoEnfrentamiento2022.GRUPOS)){
								if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra2022.FASE_I_II_G_4)){
									
									//Asignar boleras aleatoriamente
									Boleras2022 boleraAsignar1 = null;
									Boleras2022 boleraAsignar2 = null;
									boolean boleraAsignada1 = false;
									boolean boleraAsignada2 = false;
									LinkedHashMap<Integer, String> bolerasGrupoAsignadasCFMap = new LinkedHashMap<>();
									while(!boleraAsignada1){
										Random rand = new Random();
										boleraAsignar1 = resultListBoleras.get(rand.nextInt(resultListBoleras.size()));
										if(!boleraAsignar1.getId().equals(boleraFinal.getId())){
											if(!bolerasOcupadasOFCFMap.containsKey(boleraAsignar1.getId())){
												if(!bolerasGrupoAsignadasCFMap.containsKey(boleraAsignar1.getId())){
													boleraAsignada1 = true;
												}
											}
										}
									}
									while(!boleraAsignada2){
										Random rand = new Random();
										boleraAsignar2 = resultListBoleras.get(rand.nextInt(resultListBoleras.size()));
										if(!boleraAsignar2.getId().equals(boleraFinal.getId())){
											if(!bolerasOcupadasOFCFMap.containsKey(boleraAsignar2.getId())){
												if(!bolerasGrupoAsignadasCFMap.containsKey(boleraAsignar2.getId())){
													boleraAsignada2 = true;
												}
											}
										}
									}
									
									List<CalendarioFaseCF2022> calendarioFaseCFList = new ArrayList<CalendarioFaseCF2022>();
									calendarioFaseCFList = prepareCalendarioFaseCFModeloGrupos4(calendarioFaseCFList, faseAnterior, boleraAsignar1, boleraAsignar2);
									//Calendario
									for (CalendarioFaseCF2022 calendarioFaseCF : calendarioFaseCFList) {
										crearCampeonatoCadetesIndividual.insertarRowCalendarioFaseCF(calendarioFaseCF.getInsertRow());
									}
									numerofaseSiguiente = fase.getFaseSiguiente();
									numerofaseAnterior = fase.getNumero();
								}else if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra2022.FASE_I_II_G_2)){
									
									//Asignar boleras aleatoriamente
									Boleras2022 boleraAsignar1 = null;
									Boleras2022 boleraAsignar2 = null;
									boolean boleraAsignada1 = false;
									boolean boleraAsignada2 = false;
									LinkedHashMap<Integer, String> bolerasGrupoAsignadasCFMap = new LinkedHashMap<>();
									while(!boleraAsignada1){
										Random rand = new Random();
										boleraAsignar1 = resultListBoleras.get(rand.nextInt(resultListBoleras.size()));
										if(!boleraAsignar1.getId().equals(boleraFinal.getId())){
											if(!bolerasOcupadasOFCFMap.containsKey(boleraAsignar1.getId())){
												if(!bolerasGrupoAsignadasCFMap.containsKey(boleraAsignar1.getId())){
													boleraAsignada1 = true;
												}
											}
										}
									}
									while(!boleraAsignada2){
										Random rand = new Random();
										boleraAsignar2 = resultListBoleras.get(rand.nextInt(resultListBoleras.size()));
										if(!boleraAsignar2.getId().equals(boleraFinal.getId())){
											if(!bolerasOcupadasOFCFMap.containsKey(boleraAsignar2.getId())){
												if(!bolerasGrupoAsignadasCFMap.containsKey(boleraAsignar2.getId())){
													boleraAsignada2 = true;
												}
											}
										}
									}
									
									List<CalendarioFaseCF2022> calendarioFaseCFList = new ArrayList<CalendarioFaseCF2022>();
									calendarioFaseCFList = prepareCalendarioFaseCFModeloGrupos2(calendarioFaseCFList, faseAnterior, boleraAsignar1, boleraAsignar2);
									//Calendario
									for (CalendarioFaseCF2022 calendarioFaseCF : calendarioFaseCFList) {
										crearCampeonatoCadetesIndividual.insertarRowCalendarioFaseCF(calendarioFaseCF.getInsertRow());
									}
									numerofaseSiguiente = fase.getFaseSiguiente();
									numerofaseAnterior = fase.getNumero();
								}
							}else if(faseAnterior.getTipoEnfrentamiento().equals(TipoEnfrentamiento2022.BOLADAS)){
								List<CalendarioFaseCF2022> calendarioFaseCFList = new ArrayList<CalendarioFaseCF2022>();
								calendarioFaseCFList = prepareCalendarioFaseCFModeloBoladas(calendarioFaseCFList, faseAnterior, boleraFinal);
								//Calendario
								for (CalendarioFaseCF2022 calendarioFaseCF : calendarioFaseCFList) {
									crearCampeonatoCadetesIndividual.insertarRowCalendarioFaseCF(calendarioFaseCF.getInsertRow());
								}
								numerofaseSiguiente = fase.getFaseSiguiente();
								numerofaseAnterior = fase.getNumero();
							}
							//Si en la fase previa eran Octavos de final
							else if(fase.getTipoEnfrentamiento().equals(TipoEnfrentamiento2022.DIRECTO)){
								if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra2022.FASE_OF)){
									List<CalendarioFaseCF2022> calendarioFaseCFList = new ArrayList<CalendarioFaseCF2022>();
									calendarioFaseCFList = prepareCalendarioFaseCFModeloOF(calendarioFaseCFList, faseAnterior, boleraFaseAnterior1, boleraFaseAnterior2);
									//Calendario
									for (CalendarioFaseCF2022 calendarioFaseCF : calendarioFaseCFList) {
										crearCampeonatoCadetesIndividual.insertarRowCalendarioFaseCF(calendarioFaseCF.getInsertRow());
									}
									numerofaseSiguiente = fase.getFaseSiguiente();
									numerofaseAnterior = fase.getNumero();
								}
							}
						}
					}
				}
			}
			//6. Definir la faseSF
			
			for(Fases2022 fase : getResultListFases()){
				if(fase.getActivo().equals(Activo2022.SI)){
					if(fase.getNumero().equals(FasesModelo2022.FASE_SF)){
						if(numerofaseSiguiente.equals(fase.getNumero())){
							//Recuperar la fase anterior, para averiguar si era grupos escoger el modelo
							Fases2022 faseAnterior = searchFase(numerofaseAnterior);
							if(fase.getTipoEnfrentamiento().equals(TipoEnfrentamiento2022.DIRECTO)){
								//Si la fase es fase de clasificaci�n por boladas
								if(faseAnterior.getTipoEnfrentamiento().equals(TipoEnfrentamiento2022.BOLADAS)){
									List<CalendarioFaseSF2022> calendarioFaseSFList = new ArrayList<CalendarioFaseSF2022>();
									calendarioFaseSFList = prepareCalendarioFaseSFModeloBoladas(calendarioFaseSFList, faseAnterior, boleraFinal);
									//Calendario
									for (CalendarioFaseSF2022 calendarioFaseSF : calendarioFaseSFList) {
										crearCampeonatoCadetesIndividual.insertarRowCalendarioFaseSF(calendarioFaseSF.getInsertRow());
									}
									numerofaseSiguiente = fase.getFaseSiguiente();
									numerofaseAnterior = fase.getNumero();
								}else
									//Si en la fase previa eran Cuartos de final
								if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra2022.FASE_CF)){
									List<CalendarioFaseSF2022> calendarioFaseSFList = new ArrayList<CalendarioFaseSF2022>();
									calendarioFaseSFList = prepareCalendarioFaseSFModeloCF(calendarioFaseSFList, faseAnterior, boleraFinal);
									//Calendario
									for (CalendarioFaseSF2022 calendarioFaseSF : calendarioFaseSFList) {
										crearCampeonatoCadetesIndividual.insertarRowCalendarioFaseSF(calendarioFaseSF.getInsertRow());
									}
									numerofaseSiguiente = fase.getFaseSiguiente();
									numerofaseAnterior = fase.getNumero();
								}
							}
						}
					}
				}
			}
			
			//7. Definir la faseFC
			
			for(Fases2022 fase : getResultListFases()){
				if(fase.getActivo().equals(Activo2022.SI)){
					if(fase.getNumero().equals(FasesModelo2022.FASE_FC)){
						//if(numerofaseSiguiente.equals(fase.getNumero())){
							//Recuperar la fase anterior, para averiguar si era grupos escoger el modelo
							Fases2022 faseAnterior = searchFase(numerofaseAnterior);
							//Si en la fase previa eran Semifinal
							if(fase.getTipoEnfrentamiento().equals(TipoEnfrentamiento2022.DIRECTO)){
								if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra2022.FASE_SF)){
									List<CalendarioFaseFC2022> calendarioFaseFCList = new ArrayList<CalendarioFaseFC2022>();
									calendarioFaseFCList = prepareCalendarioFaseFCModeloSF(calendarioFaseFCList, faseAnterior, boleraFinal);
									//Calendario
									for (CalendarioFaseFC2022 calendarioFaseFC : calendarioFaseFCList) {
										crearCampeonatoCadetesIndividual.insertarRowCalendarioFaseFC(calendarioFaseFC.getInsertRow());
									}
									//numerofaseSiguiente = fase.getFaseSiguiente();
								}
							}
						//}
					}
				}
			}
			
			//8. Definir la faseFF
			
			for(Fases2022 fase : getResultListFases()){
				if(fase.getActivo().equals(Activo2022.SI)){
					if(fase.getNumero().equals(FasesModelo2022.FASE_FF)){
						if(numerofaseSiguiente.equals(fase.getNumero())){
							//Recuperar la fase anterior, para averiguar si era grupos escoger el modelo
							Fases2022 faseAnterior = searchFase(numerofaseAnterior);
							//Si en la fase previa eran Semifinal
							if(fase.getTipoEnfrentamiento().equals(TipoEnfrentamiento2022.DIRECTO)){
								if(faseAnterior.getNumeroEnfrentamientos().equals(GruposLetra2022.FASE_SF)){
									List<CalendarioFaseFF2022> calendarioFaseFFList = new ArrayList<CalendarioFaseFF2022>();
									calendarioFaseFFList = prepareCalendarioFaseFFModeloSF(calendarioFaseFFList, faseAnterior, boleraFinal);
									//Calendario
									for (CalendarioFaseFF2022 calendarioFaseFF : calendarioFaseFFList) {
										crearCampeonatoCadetesIndividual.insertarRowCalendarioFaseFF(calendarioFaseFF.getInsertRow());
										sorteoRealizado = true;
									}
									numerofaseSiguiente = fase.getFaseSiguiente();
								}
							}
						}
					}
				}
			}
		
			if(!sorteoRealizado){
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El Sorteo no se ha realizado.", null));
			}else{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "El Sorteo se ha realizado correctamente.", null));
				crearCampeonatoCadetesIndividual.actualizarEstadoConfiguracion(EstadosIndividualParejas2022.CONFIG_ESTADO_SORTEADO_CAMPEONATO);
				//Inicializar variables de nuevo
				leerCampeonatoCadetesIndividual = new LeerCampeonatoCadetesIndividual2022();
				//Redirigir
				//redirect.redirect(menuView.PAG_OM_ADD_CAMPEONATO_Cadetes_INDIVIDUAL);
				
			}
			
			
		}catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El Sorteo no se ha realizado.", null));
			doBorrarSorteo();
		}
		
	}
	
	private CampeonatoCadetesIndividualClasificacion2022 prepareClasificacionBoladas(CampeonatoCadetesIndividualClasificacion2022 clasificacionBoladas, ParticipantesIndividual2022 participante, 
			Integer totalParticipantesAsignados, Boleras2022 boleraFinal, Fases2022 fase) {
		clasificacionBoladas.setId(totalParticipantesAsignados+1);
		clasificacionBoladas.setFecha(fase.getFecha());
		clasificacionBoladas.setHora(fase.getHora());
		clasificacionBoladas.setBoleraId(boleraFinal.getId());
		clasificacionBoladas.setJugadorId(participante.getIdJugador());
		clasificacionBoladas.setJugador(participante.getJugador());
		clasificacionBoladas.setActivo(1);
		return clasificacionBoladas;
	}

	private List<CalendarioFaseI2022> prepareCalendarioFaseIModeloGrupos8(List<CalendarioFaseI2022> calendarioFaseIList, Grupos2022 grupo) {
		ModeloIndividualGrupos82022 modeloGrupos8 = new ModeloIndividualGrupos82022();
		Integer id = 1;
		for (Entry<Integer, ModeloIndividualEnfrentamientoGruposFaseI2022> entryPi : modeloGrupos8.getEnfrentamientosGrupo().entrySet()) {
			Integer key = entryPi.getKey();
			Object valuePi = entryPi.getValue();
			ModeloIndividualEnfrentamientoGruposFaseI2022 enfrentamiento = (ModeloIndividualEnfrentamientoGruposFaseI2022) valuePi;
			//Agregar calendario
			CalendarioFaseI2022 calendarioFaseI = new CalendarioFaseI2022();
			calendarioFaseI.setActivo(Activo2022.SI);
			calendarioFaseI.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2022.JUVENILES);
			calendarioFaseI.setGrupo(grupo.getId());
			calendarioFaseI.setId(id);
			calendarioFaseI.setJuegosJugador1(0);
			ParticipantesIndividual2022 pi1 = grupo.searchJugadorGrupo(enfrentamiento.getJugador1(), grupo.getId());
			calendarioFaseI.setJugador1Id(pi1.getIdJugador());
			calendarioFaseI.setJuegosJugador2(0);
			ParticipantesIndividual2022 pi2 = grupo.searchJugadorGrupo(enfrentamiento.getJugador2(), grupo.getId());
			calendarioFaseI.setJugador2Id(pi2.getIdJugador());
			calendarioFaseI.setOrden(key);
			calendarioFaseI.setInicia(enfrentamiento.getSorteo());
			calendarioFaseI.setBoleraId(grupo.getBoleraId());
			id++;
			calendarioFaseIList.add(calendarioFaseI);
		}
		return calendarioFaseIList;
	}
	
	private List<CalendarioFaseI2022> prepareCalendarioFaseIModeloGrupos7(List<CalendarioFaseI2022> calendarioFaseIList, Grupos2022 grupo) {
		ModeloIndividualGrupos72022 modeloGrupos = new ModeloIndividualGrupos72022();
		Integer id = 1;
		for (Entry<Integer, ModeloIndividualEnfrentamientoGruposFaseI2022> entryPi : modeloGrupos.getEnfrentamientosGrupo().entrySet()) {
			Integer key = entryPi.getKey();
			Object valuePi = entryPi.getValue();
			ModeloIndividualEnfrentamientoGruposFaseI2022 enfrentamiento = (ModeloIndividualEnfrentamientoGruposFaseI2022) valuePi;
			//Agregar calendario
			CalendarioFaseI2022 calendarioFaseI = new CalendarioFaseI2022();
			calendarioFaseI.setActivo(Activo2022.SI);
			calendarioFaseI.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2022.JUVENILES);
			calendarioFaseI.setGrupo(grupo.getId());
			calendarioFaseI.setId(id);
			calendarioFaseI.setJuegosJugador1(0);
			ParticipantesIndividual2022 pi1 = grupo.searchJugadorGrupo(enfrentamiento.getJugador1(), grupo.getId());
			calendarioFaseI.setJugador1Id(pi1.getIdJugador());
			calendarioFaseI.setJuegosJugador2(0);
			ParticipantesIndividual2022 pi2 = grupo.searchJugadorGrupo(enfrentamiento.getJugador2(), grupo.getId());
			calendarioFaseI.setJugador2Id(pi2.getIdJugador());
			calendarioFaseI.setOrden(key);
			calendarioFaseI.setInicia(enfrentamiento.getSorteo());
			calendarioFaseI.setBoleraId(grupo.getBoleraId());
			id++;
			calendarioFaseIList.add(calendarioFaseI);
		}
		return calendarioFaseIList;
	}
	
	private List<CalendarioFaseI2022> prepareCalendarioFaseIModeloGrupos6(List<CalendarioFaseI2022> calendarioFaseIList, Grupos2022 grupo) {
		ModeloIndividualGrupos62022 modeloGrupos = new ModeloIndividualGrupos62022();
		Integer id = 1;
		for (Entry<Integer, ModeloIndividualEnfrentamientoGruposFaseI2022> entryPi : modeloGrupos.getEnfrentamientosGrupo().entrySet()) {
			Integer key = entryPi.getKey();
			Object valuePi = entryPi.getValue();
			ModeloIndividualEnfrentamientoGruposFaseI2022 enfrentamiento = (ModeloIndividualEnfrentamientoGruposFaseI2022) valuePi;
			//Agregar calendario
			CalendarioFaseI2022 calendarioFaseI = new CalendarioFaseI2022();
			calendarioFaseI.setActivo(Activo2022.SI);
			calendarioFaseI.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2022.JUVENILES);
			calendarioFaseI.setGrupo(grupo.getId());
			calendarioFaseI.setId(id);
			calendarioFaseI.setJuegosJugador1(0);
			ParticipantesIndividual2022 pi1 = grupo.searchJugadorGrupo(enfrentamiento.getJugador1(), grupo.getId());
			calendarioFaseI.setJugador1Id(pi1.getIdJugador());
			calendarioFaseI.setJuegosJugador2(0);
			ParticipantesIndividual2022 pi2 = grupo.searchJugadorGrupo(enfrentamiento.getJugador2(), grupo.getId());
			calendarioFaseI.setJugador2Id(pi2.getIdJugador());
			calendarioFaseI.setOrden(key);
			calendarioFaseI.setInicia(enfrentamiento.getSorteo());
			calendarioFaseI.setBoleraId(grupo.getBoleraId());
			id++;
			calendarioFaseIList.add(calendarioFaseI);
		}
		return calendarioFaseIList;
	}
	
	private List<CalendarioFaseI2022> prepareCalendarioFaseIModeloGrupos5(List<CalendarioFaseI2022> calendarioFaseIList, Grupos2022 grupo) {
		ModeloIndividualGrupos52022 modeloGrupos = new ModeloIndividualGrupos52022();
		Integer id = 1;
		for (Entry<Integer, ModeloIndividualEnfrentamientoGruposFaseI2022> entryPi : modeloGrupos.getEnfrentamientosGrupo().entrySet()) {
			Integer key = entryPi.getKey();
			Object valuePi = entryPi.getValue();
			ModeloIndividualEnfrentamientoGruposFaseI2022 enfrentamiento = (ModeloIndividualEnfrentamientoGruposFaseI2022) valuePi;
			//Agregar calendario
			CalendarioFaseI2022 calendarioFaseI = new CalendarioFaseI2022();
			calendarioFaseI.setActivo(Activo2022.SI);
			calendarioFaseI.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2022.JUVENILES);
			calendarioFaseI.setGrupo(grupo.getId());
			calendarioFaseI.setId(id);
			calendarioFaseI.setJuegosJugador1(0);
			ParticipantesIndividual2022 pi1 = grupo.searchJugadorGrupo(enfrentamiento.getJugador1(), grupo.getId());
			calendarioFaseI.setJugador1Id(pi1.getIdJugador());
			calendarioFaseI.setJuegosJugador2(0);
			ParticipantesIndividual2022 pi2 = grupo.searchJugadorGrupo(enfrentamiento.getJugador2(), grupo.getId());
			calendarioFaseI.setJugador2Id(pi2.getIdJugador());
			calendarioFaseI.setOrden(key);
			calendarioFaseI.setInicia(enfrentamiento.getSorteo());
			calendarioFaseI.setBoleraId(grupo.getBoleraId());
			id++;
			calendarioFaseIList.add(calendarioFaseI);
		}
		return calendarioFaseIList;
	}
	
	private List<CalendarioFaseI2022> prepareCalendarioFaseIModeloGrupos4(List<CalendarioFaseI2022> calendarioFaseIList, Grupos2022 grupo) {
		ModeloIndividualGrupos42022 modeloGrupos = new ModeloIndividualGrupos42022();
		Integer id = 1;
		for (Entry<Integer, ModeloIndividualEnfrentamientoGruposFaseI2022> entryPi : modeloGrupos.getEnfrentamientosGrupo().entrySet()) {
			Integer key = entryPi.getKey();
			Object valuePi = entryPi.getValue();
			ModeloIndividualEnfrentamientoGruposFaseI2022 enfrentamiento = (ModeloIndividualEnfrentamientoGruposFaseI2022) valuePi;
			//Agregar calendario
			CalendarioFaseI2022 calendarioFaseI = new CalendarioFaseI2022();
			calendarioFaseI.setActivo(Activo2022.SI);
			calendarioFaseI.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2022.JUVENILES);
			calendarioFaseI.setGrupo(grupo.getId());
			calendarioFaseI.setId(id);
			calendarioFaseI.setJuegosJugador1(0);
			ParticipantesIndividual2022 pi1 = grupo.searchJugadorGrupo(enfrentamiento.getJugador1(), grupo.getId());
			calendarioFaseI.setJugador1Id(pi1.getIdJugador());
			calendarioFaseI.setJuegosJugador2(0);
			ParticipantesIndividual2022 pi2 = grupo.searchJugadorGrupo(enfrentamiento.getJugador2(), grupo.getId());
			calendarioFaseI.setJugador2Id(pi2.getIdJugador());
			calendarioFaseI.setOrden(key);
			calendarioFaseI.setInicia(enfrentamiento.getSorteo());
			calendarioFaseI.setBoleraId(grupo.getBoleraId());
			id++;
			calendarioFaseIList.add(calendarioFaseI);
		}
		return calendarioFaseIList;
	}
	
	private List<CalendarioFaseI2022> prepareCalendarioFaseIModeloGrupos3(List<CalendarioFaseI2022> calendarioFaseIList, Grupos2022 grupo) {
		ModeloIndividualGrupos32022 modeloGrupos = new ModeloIndividualGrupos32022();
		Integer id = 1;
		for (Entry<Integer, ModeloIndividualEnfrentamientoGruposFaseI2022> entryPi : modeloGrupos.getEnfrentamientosGrupo().entrySet()) {
			Integer key = entryPi.getKey();
			Object valuePi = entryPi.getValue();
			ModeloIndividualEnfrentamientoGruposFaseI2022 enfrentamiento = (ModeloIndividualEnfrentamientoGruposFaseI2022) valuePi;
			//Agregar calendario
			CalendarioFaseI2022 calendarioFaseI = new CalendarioFaseI2022();
			calendarioFaseI.setActivo(Activo2022.SI);
			calendarioFaseI.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2022.TERCERA);
			calendarioFaseI.setGrupo(grupo.getId());
			calendarioFaseI.setId(id);
			calendarioFaseI.setJuegosJugador1(0);
			ParticipantesIndividual2022 pi1 = grupo.searchJugadorGrupo(enfrentamiento.getJugador1(), grupo.getId());
			calendarioFaseI.setJugador1Id(pi1.getIdJugador());
			calendarioFaseI.setJuegosJugador2(0);
			ParticipantesIndividual2022 pi2 = grupo.searchJugadorGrupo(enfrentamiento.getJugador2(), grupo.getId());
			calendarioFaseI.setJugador2Id(pi2.getIdJugador());
			calendarioFaseI.setOrden(key);
			calendarioFaseI.setInicia(enfrentamiento.getSorteo());
			calendarioFaseI.setBoleraId(grupo.getBoleraId());
			id++;
			calendarioFaseIList.add(calendarioFaseI);
		}
		return calendarioFaseIList;
	}

	public Fases2022 searchFase(Integer numeroFase){
		for(Fases2022 fase : getResultListFases()){
			if(fase.getNumero().equals(numeroFase)){
				return fase;
			}
		}
		return null;
	}
	
	private List<CalendarioFaseOF2022> prepareCalendarioFaseOFModeloGrupos4(List<CalendarioFaseOF2022> calendarioFaseOFList, Fases2022 faseAnterior, Boleras2022 boleraAsignar1, Boleras2022 boleraAsignar2) {
		ModeloIndividualOctavosFinal4Grupos2022 modeloOctavosFinal4Grupos = new ModeloIndividualOctavosFinal4Grupos2022();
		Integer id = 1;

		for (Entry<Integer, ModeloIndividualEnfrentamientoOctavosFinal2022> entry : modeloOctavosFinal4Grupos.getEnfrentamientosOctavosFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloIndividualEnfrentamientoOctavosFinal2022 enfrentamiento = (ModeloIndividualEnfrentamientoOctavosFinal2022) value;
			//Agregar calendario
			CalendarioFaseOF2022 calendarioFaseOF = new CalendarioFaseOF2022();
			calendarioFaseOF.setId(id);
			calendarioFaseOF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2022.JUVENILES);
			calendarioFaseOF.setNumeroEnfrentamiento(id);
			calendarioFaseOF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseOF.setJugador1Id(0);
			calendarioFaseOF.setJuegosJugador1P1(0);
			calendarioFaseOF.setJuegosJugador1P2(0);
			calendarioFaseOF.setJuegosJugador1P3(0);
			calendarioFaseOF.setJuegosJugador2P1(0);
			calendarioFaseOF.setJuegosJugador2P2(0);
			calendarioFaseOF.setJuegosJugador2P3(0);
			calendarioFaseOF.setJugador2Id(0);
			calendarioFaseOF.setOrden(key);
			calendarioFaseOF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseOF.setActivo(Activo2022.SI);
			calendarioFaseOF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseOF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseOF.setGrupoProcedenciaJugador1(enfrentamiento.getGrupoProcedenciaJugador1());
			calendarioFaseOF.setPosicionProcedenciaJugador1(enfrentamiento.getPosicionProcedenciaJugador1());
			calendarioFaseOF.setGrupoProcedenciaJugador2(enfrentamiento.getGrupoProcedenciaJugador2());
			calendarioFaseOF.setPosicionProcedenciaJugador2(enfrentamiento.getPosicionProcedenciaJugador2());
			
			//Asignar la bolera por parte del cuadro
			if(id > 0 && id <5 ){
				calendarioFaseOF.setBoleraId(boleraAsignar1.getId());
			}else if(id > 4 && id <9 ){
				calendarioFaseOF.setBoleraId(boleraAsignar2.getId());
			}
			id++;
			calendarioFaseOFList.add(calendarioFaseOF);
		}
		return calendarioFaseOFList;
	}
	
	private List<CalendarioFaseOF2022> prepareCalendarioFaseOFModeloGrupos8(List<CalendarioFaseOF2022> calendarioFaseOFList, Fases2022 faseAnterior, Boleras2022 boleraAsignar1, Boleras2022 boleraAsignar2) {
		ModeloIndividualOctavosFinal8Grupos2022 modeloOctavosFinal8Grupos = new ModeloIndividualOctavosFinal8Grupos2022();
		Integer id = 1;

		for (Entry<Integer, ModeloIndividualEnfrentamientoOctavosFinal2022> entry : modeloOctavosFinal8Grupos.getEnfrentamientosOctavosFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloIndividualEnfrentamientoOctavosFinal2022 enfrentamiento = (ModeloIndividualEnfrentamientoOctavosFinal2022) value;
			//Agregar calendario
			CalendarioFaseOF2022 calendarioFaseOF = new CalendarioFaseOF2022();
			calendarioFaseOF.setId(id);
			calendarioFaseOF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2022.JUVENILES);
			calendarioFaseOF.setNumeroEnfrentamiento(id);
			calendarioFaseOF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseOF.setJugador1Id(0);
			calendarioFaseOF.setJuegosJugador1P1(0);
			calendarioFaseOF.setJuegosJugador1P2(0);
			calendarioFaseOF.setJuegosJugador1P3(0);
			calendarioFaseOF.setJuegosJugador2P1(0);
			calendarioFaseOF.setJuegosJugador2P2(0);
			calendarioFaseOF.setJuegosJugador2P3(0);
			calendarioFaseOF.setJugador2Id(0);
			calendarioFaseOF.setOrden(key);
			calendarioFaseOF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseOF.setActivo(Activo2022.SI);
			calendarioFaseOF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseOF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseOF.setGrupoProcedenciaJugador1(enfrentamiento.getGrupoProcedenciaJugador1());
			calendarioFaseOF.setPosicionProcedenciaJugador1(enfrentamiento.getPosicionProcedenciaJugador1());
			calendarioFaseOF.setGrupoProcedenciaJugador2(enfrentamiento.getGrupoProcedenciaJugador2());
			calendarioFaseOF.setPosicionProcedenciaJugador2(enfrentamiento.getPosicionProcedenciaJugador2());
			
			//Asignar la bolera por parte del cuadro
			if(id > 0 && id <5 ){
				calendarioFaseOF.setBoleraId(boleraAsignar1.getId());
			}else if(id > 4 && id <9 ){
				calendarioFaseOF.setBoleraId(boleraAsignar2.getId());
			}
			id++;
			calendarioFaseOFList.add(calendarioFaseOF);
		}
		return calendarioFaseOFList;
	}
	
	private List<CalendarioFaseCF2022> prepareCalendarioFaseCFModeloGrupos2(List<CalendarioFaseCF2022> calendarioFaseCFList, Fases2022 faseAnterior, Boleras2022 boleraAsignar1, Boleras2022 boleraAsignar2) {
		ModeloIndividualCuartosFinal2Grupos2022 modeloCuartosFinal2Grupos = new ModeloIndividualCuartosFinal2Grupos2022();
		Integer id = 1;
		
		for (Entry<Integer, ModeloIndividualEnfrentamientoCuartosFinal2022> entry : modeloCuartosFinal2Grupos.getEnfrentamientosCuartosFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloIndividualEnfrentamientoCuartosFinal2022 enfrentamiento = (ModeloIndividualEnfrentamientoCuartosFinal2022) value;
			//Agregar calendario
			CalendarioFaseCF2022 calendarioFaseCF = new CalendarioFaseCF2022();
			calendarioFaseCF.setId(id);
			calendarioFaseCF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2022.PRIMERA);
			calendarioFaseCF.setNumeroEnfrentamiento(id);
			calendarioFaseCF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseCF.setJugador1Id(0);
			calendarioFaseCF.setJuegosJugador1P1(0);
			calendarioFaseCF.setJuegosJugador1P2(0);
			calendarioFaseCF.setJuegosJugador1P3(0);
			calendarioFaseCF.setJuegosJugador2P1(0);
			calendarioFaseCF.setJuegosJugador2P2(0);
			calendarioFaseCF.setJuegosJugador2P3(0);
			calendarioFaseCF.setJugador2Id(0);
			calendarioFaseCF.setOrden(key);
			calendarioFaseCF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseCF.setActivo(Activo2022.SI);
			calendarioFaseCF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseCF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseCF.setGrupoProcedenciaJugador1(enfrentamiento.getGrupoProcedenciaJugador1());
			calendarioFaseCF.setPosicionProcedenciaJugador1(enfrentamiento.getPosicionProcedenciaJugador1());
			calendarioFaseCF.setGrupoProcedenciaJugador2(enfrentamiento.getGrupoProcedenciaJugador2());
			calendarioFaseCF.setPosicionProcedenciaJugador2(enfrentamiento.getPosicionProcedenciaJugador2());
			
			//Asignar la bolera por parte del cuadro
			if(id > 0 && id <5 ){
				calendarioFaseCF.setBoleraId(boleraAsignar1.getId());
			}else if(id > 4 && id <9 ){
				calendarioFaseCF.setBoleraId(boleraAsignar2.getId());
			}
			id++;
			calendarioFaseCFList.add(calendarioFaseCF);
		}
		return calendarioFaseCFList;
	}
	
	private List<CalendarioFaseCF2022> prepareCalendarioFaseCFModeloGrupos4(List<CalendarioFaseCF2022> calendarioFaseCFList, Fases2022 faseAnterior, Boleras2022 boleraAsignar1, Boleras2022 boleraAsignar2) {
		ModeloIndividualCuartosFinal4Grupos2022 modeloCuartosFinal4Grupos = new ModeloIndividualCuartosFinal4Grupos2022();
		Integer id = 1;
		
		for (Entry<Integer, ModeloIndividualEnfrentamientoCuartosFinal2022> entry : modeloCuartosFinal4Grupos.getEnfrentamientosCuartosFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloIndividualEnfrentamientoCuartosFinal2022 enfrentamiento = (ModeloIndividualEnfrentamientoCuartosFinal2022) value;
			//Agregar calendario
			CalendarioFaseCF2022 calendarioFaseCF = new CalendarioFaseCF2022();
			calendarioFaseCF.setId(id);
			calendarioFaseCF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2022.JUVENILES);
			calendarioFaseCF.setNumeroEnfrentamiento(id);
			calendarioFaseCF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseCF.setJugador1Id(0);
			calendarioFaseCF.setJuegosJugador1P1(0);
			calendarioFaseCF.setJuegosJugador1P2(0);
			calendarioFaseCF.setJuegosJugador1P3(0);
			calendarioFaseCF.setJuegosJugador2P1(0);
			calendarioFaseCF.setJuegosJugador2P2(0);
			calendarioFaseCF.setJuegosJugador2P3(0);
			calendarioFaseCF.setJugador2Id(0);
			calendarioFaseCF.setOrden(key);
			calendarioFaseCF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseCF.setActivo(Activo2022.SI);
			calendarioFaseCF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseCF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseCF.setGrupoProcedenciaJugador1(enfrentamiento.getGrupoProcedenciaJugador1());
			calendarioFaseCF.setPosicionProcedenciaJugador1(enfrentamiento.getPosicionProcedenciaJugador1());
			calendarioFaseCF.setGrupoProcedenciaJugador2(enfrentamiento.getGrupoProcedenciaJugador2());
			calendarioFaseCF.setPosicionProcedenciaJugador2(enfrentamiento.getPosicionProcedenciaJugador2());
			
			//Asignar la bolera por parte del cuadro
			if(id > 0 && id <5 ){
				calendarioFaseCF.setBoleraId(boleraAsignar1.getId());
			}else if(id > 4 && id <9 ){
				calendarioFaseCF.setBoleraId(boleraAsignar2.getId());
			}
			id++;
			calendarioFaseCFList.add(calendarioFaseCF);
		}
		return calendarioFaseCFList;
	}
	
	private List<CalendarioFaseCF2022> prepareCalendarioFaseCFModeloBoladas(List<CalendarioFaseCF2022> calendarioFaseCFList, Fases2022 faseAnterior, Boleras2022 boleraFinal) {
		ModeloIndividualCuartosFinal4Boladas2022 modeloCF4Boladas = new ModeloIndividualCuartosFinal4Boladas2022();
		Integer id = 1;
		
		for (Entry<Integer, ModeloIndividualEnfrentamientoCuartosFinal2022> entry : modeloCF4Boladas.getEnfrentamientosCuartosFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloIndividualEnfrentamientoCuartosFinal2022 enfrentamiento = (ModeloIndividualEnfrentamientoCuartosFinal2022) value;
			//Agregar calendario
			CalendarioFaseCF2022 calendarioFaseCF = new CalendarioFaseCF2022();
			calendarioFaseCF.setId(id);
			calendarioFaseCF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2022.CADETES);
			calendarioFaseCF.setNumeroEnfrentamiento(id);
			calendarioFaseCF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseCF.setJugador1Id(0);
			calendarioFaseCF.setJuegosJugador1P1(0);
			calendarioFaseCF.setJuegosJugador1P2(0);
			calendarioFaseCF.setJuegosJugador1P3(0);
			calendarioFaseCF.setJuegosJugador2P1(0);
			calendarioFaseCF.setJuegosJugador2P2(0);
			calendarioFaseCF.setJuegosJugador2P3(0);
			calendarioFaseCF.setJugador2Id(0);
			calendarioFaseCF.setOrden(key);
			calendarioFaseCF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseCF.setActivo(Activo2022.SI);
			calendarioFaseCF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseCF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseCF.setGrupoProcedenciaJugador1(enfrentamiento.getGrupoProcedenciaJugador1());
			calendarioFaseCF.setPosicionProcedenciaJugador1(enfrentamiento.getPosicionProcedenciaJugador1());
			calendarioFaseCF.setGrupoProcedenciaJugador2(enfrentamiento.getGrupoProcedenciaJugador2());
			calendarioFaseCF.setPosicionProcedenciaJugador2(enfrentamiento.getPosicionProcedenciaJugador2());
			
			//Asignar la bolera por parte del cuadro
			calendarioFaseCF.setBoleraId(boleraFinal.getId());
			id++;
			calendarioFaseCFList.add(calendarioFaseCF);
		}
		return calendarioFaseCFList;
	}
	
	private List<CalendarioFaseCF2022> prepareCalendarioFaseCFModeloOF(List<CalendarioFaseCF2022> calendarioFaseCFList, Fases2022 faseAnterior, Boleras2022 boleraAsignar1, Boleras2022 boleraAsignar2) {
		ModeloIndividualCuartosFinal4OF2022 modeloCuartosFinal4OF = new ModeloIndividualCuartosFinal4OF2022();
		Integer id = 1;
		
		for (Entry<Integer, ModeloIndividualEnfrentamientoCuartosFinal2022> entry : modeloCuartosFinal4OF.getEnfrentamientosCuartosFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloIndividualEnfrentamientoCuartosFinal2022 enfrentamiento = (ModeloIndividualEnfrentamientoCuartosFinal2022) value;
			//Agregar calendario
			CalendarioFaseCF2022 calendarioFaseCF = new CalendarioFaseCF2022();
			calendarioFaseCF.setId(id);
			calendarioFaseCF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2022.JUVENILES);
			calendarioFaseCF.setNumeroEnfrentamiento(id);
			calendarioFaseCF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseCF.setJugador1Id(0);
			calendarioFaseCF.setJuegosJugador1P1(0);
			calendarioFaseCF.setJuegosJugador1P2(0);
			calendarioFaseCF.setJuegosJugador1P3(0);
			calendarioFaseCF.setJuegosJugador2P1(0);
			calendarioFaseCF.setJuegosJugador2P2(0);
			calendarioFaseCF.setJuegosJugador2P3(0);
			calendarioFaseCF.setJugador2Id(0);
			calendarioFaseCF.setOrden(key);
			calendarioFaseCF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseCF.setActivo(Activo2022.SI);
			calendarioFaseCF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseCF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseCF.setGrupoProcedenciaJugador1(enfrentamiento.getGrupoProcedenciaJugador1());
			calendarioFaseCF.setPosicionProcedenciaJugador1(enfrentamiento.getPosicionProcedenciaJugador1());
			calendarioFaseCF.setGrupoProcedenciaJugador2(enfrentamiento.getGrupoProcedenciaJugador2());
			calendarioFaseCF.setPosicionProcedenciaJugador2(enfrentamiento.getPosicionProcedenciaJugador2());
			
			//Asignar la bolera por parte del cuadro
			if(id > 0 && id <3 ){
				calendarioFaseCF.setBoleraId(boleraAsignar1.getId());
			}else if(id > 2 && id <5 ){
				calendarioFaseCF.setBoleraId(boleraAsignar2.getId());
			}
			id++;
			calendarioFaseCFList.add(calendarioFaseCF);
		}
		return calendarioFaseCFList;
	}
	
	private List<CalendarioFaseSF2022> prepareCalendarioFaseSFModeloBoladas(List<CalendarioFaseSF2022> calendarioFaseSFList, Fases2022 faseAnterior, Boleras2022 boleraFinal) {
		ModeloIndividualSemiFinal4Boladas2022 modeloSemiFinal4Boladas = new ModeloIndividualSemiFinal4Boladas2022();
		Integer id = 1;
		
		for (Entry<Integer, ModeloIndividualEnfrentamientoSemiFinal2022> entry : modeloSemiFinal4Boladas.getEnfrentamientosSemiFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloIndividualEnfrentamientoSemiFinal2022 enfrentamiento = (ModeloIndividualEnfrentamientoSemiFinal2022) value;
			//Agregar calendario
			CalendarioFaseSF2022 calendarioFaseSF = new CalendarioFaseSF2022();
			calendarioFaseSF.setId(id);
			calendarioFaseSF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2022.JUVENILES);
			calendarioFaseSF.setNumeroEnfrentamiento(id);
			calendarioFaseSF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseSF.setJugador1Id(0);
			calendarioFaseSF.setJuegosJugador1P1(0);
			calendarioFaseSF.setJuegosJugador1P2(0);
			calendarioFaseSF.setJuegosJugador1P3(0);
			calendarioFaseSF.setJuegosJugador2P1(0);
			calendarioFaseSF.setJuegosJugador2P2(0);
			calendarioFaseSF.setJuegosJugador2P3(0);
			calendarioFaseSF.setJugador2Id(0);
			calendarioFaseSF.setOrden(key);
			calendarioFaseSF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseSF.setActivo(Activo2022.SI);
			calendarioFaseSF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseSF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseSF.setGrupoProcedenciaJugador1(enfrentamiento.getGrupoProcedenciaJugador1());
			calendarioFaseSF.setPosicionProcedenciaJugador1(enfrentamiento.getPosicionProcedenciaJugador1());
			calendarioFaseSF.setGrupoProcedenciaJugador2(enfrentamiento.getGrupoProcedenciaJugador2());
			calendarioFaseSF.setPosicionProcedenciaJugador2(enfrentamiento.getPosicionProcedenciaJugador2());
			
			//Asignar la bolera por parte del cuadro
			calendarioFaseSF.setBoleraId(boleraFinal.getId());
			id++;
			calendarioFaseSFList.add(calendarioFaseSF);
		}
		return calendarioFaseSFList;
	}
	
	private List<CalendarioFaseSF2022> prepareCalendarioFaseSFModeloCF(List<CalendarioFaseSF2022> calendarioFaseSFList, Fases2022 faseAnterior, Boleras2022 boleraFinal) {
		ModeloIndividualSemiFinal4CF2022 modeloSemiFinal4CF = new ModeloIndividualSemiFinal4CF2022();
		Integer id = 1;
		
		for (Entry<Integer, ModeloIndividualEnfrentamientoSemiFinal2022> entry : modeloSemiFinal4CF.getEnfrentamientosSemiFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloIndividualEnfrentamientoSemiFinal2022 enfrentamiento = (ModeloIndividualEnfrentamientoSemiFinal2022) value;
			//Agregar calendario
			CalendarioFaseSF2022 calendarioFaseSF = new CalendarioFaseSF2022();
			calendarioFaseSF.setId(id);
			calendarioFaseSF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2022.JUVENILES);
			calendarioFaseSF.setNumeroEnfrentamiento(id);
			calendarioFaseSF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseSF.setJugador1Id(0);
			calendarioFaseSF.setJuegosJugador1P1(0);
			calendarioFaseSF.setJuegosJugador1P2(0);
			calendarioFaseSF.setJuegosJugador1P3(0);
			calendarioFaseSF.setJuegosJugador2P1(0);
			calendarioFaseSF.setJuegosJugador2P2(0);
			calendarioFaseSF.setJuegosJugador2P3(0);
			calendarioFaseSF.setJugador2Id(0);
			calendarioFaseSF.setOrden(key);
			calendarioFaseSF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseSF.setActivo(Activo2022.SI);
			calendarioFaseSF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseSF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseSF.setGrupoProcedenciaJugador1(enfrentamiento.getGrupoProcedenciaJugador1());
			calendarioFaseSF.setPosicionProcedenciaJugador1(enfrentamiento.getPosicionProcedenciaJugador1());
			calendarioFaseSF.setGrupoProcedenciaJugador2(enfrentamiento.getGrupoProcedenciaJugador2());
			calendarioFaseSF.setPosicionProcedenciaJugador2(enfrentamiento.getPosicionProcedenciaJugador2());
			
			//Asignar la bolera por parte del cuadro
			calendarioFaseSF.setBoleraId(boleraFinal.getId());
			id++;
			calendarioFaseSFList.add(calendarioFaseSF);
		}
		return calendarioFaseSFList;
	}
	
	private List<CalendarioFaseFC2022> prepareCalendarioFaseFCModeloSF(List<CalendarioFaseFC2022> calendarioFaseFCList, Fases2022 faseAnterior, Boleras2022 boleraFinal) {
		ModeloIndividualFinalConsolacion4SF2022 modeloFinalConsolacion4SF = new ModeloIndividualFinalConsolacion4SF2022();
		Integer id = 1;
		
		for (Entry<Integer, ModeloIndividualEnfrentamientoFinalConsolacion2022> entry : modeloFinalConsolacion4SF.getEnfrentamientosFinalConsolacion().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloIndividualEnfrentamientoFinalConsolacion2022 enfrentamiento = (ModeloIndividualEnfrentamientoFinalConsolacion2022) value;
			//Agregar calendario
			CalendarioFaseFC2022 calendarioFaseFC = new CalendarioFaseFC2022();
			calendarioFaseFC.setId(id);
			calendarioFaseFC.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2022.JUVENILES);
			calendarioFaseFC.setNumeroEnfrentamiento(id);
			calendarioFaseFC.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseFC.setJugador1Id(0);
			calendarioFaseFC.setJuegosJugador1P1(0);
			calendarioFaseFC.setJuegosJugador1P2(0);
			calendarioFaseFC.setJuegosJugador1P3(0);
			calendarioFaseFC.setJuegosJugador2P1(0);
			calendarioFaseFC.setJuegosJugador2P2(0);
			calendarioFaseFC.setJuegosJugador2P3(0);
			calendarioFaseFC.setJugador2Id(0);
			calendarioFaseFC.setOrden(key);
			calendarioFaseFC.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseFC.setActivo(Activo2022.SI);
			calendarioFaseFC.setInicia(enfrentamiento.getSorteo());
			calendarioFaseFC.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseFC.setGrupoProcedenciaJugador1(enfrentamiento.getGrupoProcedenciaJugador1());
			calendarioFaseFC.setPosicionProcedenciaJugador1(enfrentamiento.getPosicionProcedenciaJugador1());
			calendarioFaseFC.setGrupoProcedenciaJugador2(enfrentamiento.getGrupoProcedenciaJugador2());
			calendarioFaseFC.setPosicionProcedenciaJugador2(enfrentamiento.getPosicionProcedenciaJugador2());
			
			//Asignar la bolera por parte del cuadro
			calendarioFaseFC.setBoleraId(boleraFinal.getId());
			id++;
			calendarioFaseFCList.add(calendarioFaseFC);
		}
		return calendarioFaseFCList;
	}
	
	private List<CalendarioFaseFF2022> prepareCalendarioFaseFFModeloSF(List<CalendarioFaseFF2022> calendarioFaseFFList, Fases2022 faseAnterior, Boleras2022 boleraFinal) {
		ModeloIndividualFinal4SF2022 modeloFinal4SF = new ModeloIndividualFinal4SF2022();
		Integer id = 1;
		
		for (Entry<Integer, ModeloIndividualEnfrentamientoFinal2022> entry : modeloFinal4SF.getEnfrentamientosFinal().entrySet()) {
			Integer key = entry.getKey();
			Object value = entry.getValue();
			ModeloIndividualEnfrentamientoFinal2022 enfrentamiento = (ModeloIndividualEnfrentamientoFinal2022) value;
			//Agregar calendario
			CalendarioFaseFF2022 calendarioFaseFF = new CalendarioFaseFF2022();
			calendarioFaseFF.setId(id);
			calendarioFaseFF.setCategoriaId(com.bolocelta.bbdd.constants.Categorias2022.JUVENILES);
			calendarioFaseFF.setNumeroEnfrentamiento(id);
			calendarioFaseFF.setIdCruce(enfrentamiento.getIdCruce());
			calendarioFaseFF.setJugador1Id(0);
			calendarioFaseFF.setJuegosJugador1P1(0);
			calendarioFaseFF.setJuegosJugador1P2(0);
			calendarioFaseFF.setJuegosJugador1P3(0);
			calendarioFaseFF.setJuegosJugador2P1(0);
			calendarioFaseFF.setJuegosJugador2P2(0);
			calendarioFaseFF.setJuegosJugador2P3(0);
			calendarioFaseFF.setJugador2Id(0);
			calendarioFaseFF.setOrden(key);
			calendarioFaseFF.setCruceCF(enfrentamiento.getCruceCF());
			calendarioFaseFF.setActivo(Activo2022.SI);
			calendarioFaseFF.setInicia(enfrentamiento.getSorteo());
			calendarioFaseFF.setFaseAnterior(faseAnterior.getNumero());
			calendarioFaseFF.setGrupoProcedenciaJugador1(enfrentamiento.getGrupoProcedenciaJugador1());
			calendarioFaseFF.setPosicionProcedenciaJugador1(enfrentamiento.getPosicionProcedenciaJugador1());
			calendarioFaseFF.setGrupoProcedenciaJugador2(enfrentamiento.getGrupoProcedenciaJugador2());
			calendarioFaseFF.setPosicionProcedenciaJugador2(enfrentamiento.getPosicionProcedenciaJugador2());
			
			//Asignar la bolera por parte del cuadro
			calendarioFaseFF.setBoleraId(boleraFinal.getId());
			id++;
			calendarioFaseFFList.add(calendarioFaseFF);
		}
		return calendarioFaseFFList;
	}

	public String getBolerasOcupadasFI() {
		return bolerasOcupadasFI;
	}

	public void setBolerasOcupadasFI(String bolerasOcupadasFI) {
		this.bolerasOcupadasFI = bolerasOcupadasFI;
	}

	public String getBolerasOcupadasOFCF() {
		return bolerasOcupadasOFCF;
	}

	public void setBolerasOcupadasOFCF(String bolerasOcupadasOFCF) {
		this.bolerasOcupadasOFCF = bolerasOcupadasOFCF;
	}
	
	public void doBorrarSorteo(){
		crearCampeonatoCadetesIndividual.borrarSorteo();
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "El Sorteo se ha borrado correctamente. Se puede volver a sortear.", null));
		//Inicializar variables de nuevo
		leerCampeonatoCadetesIndividual = new LeerCampeonatoCadetesIndividual2022();
		//Redirigir
		//redirect.redirect(menuView.PAG_OM_ADD_CAMPEONATO_Cadetes_INDIVIDUAL);
	}
	
	public void doConfirmarSorteoCampeonato(){
		crearCampeonatoCadetesIndividual.actualizarEstadoConfiguracion(EstadosIndividualParejas2022.CONFIG_ESTADO_FINALIZADO);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "El Sorteo se ha confirmado correctamente.", null));
		//Inicializar variables de nuevo
		leerCampeonatoCadetesIndividual = new LeerCampeonatoCadetesIndividual2022();
		//Redirigir
		//redirect.redirect(menuView.PAG_OM_ADD_CAMPEONATO_Cadetes_INDIVIDUAL);		
	}
	
	public List<Fases2022> getResultListFases() {
		if(resultListFases == null){
			resultListFases = (List<Fases2022>) leerCampeonatoCadetesIndividual.listResultFases();
		}
		return resultListFases;
	}	

}
