package com.bolocelta.facade;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;

import com.bolocelta.bbdd.readTables.LeerEquipos2022;
import com.bolocelta.entities.Equipos2022;

@Named
@RequestScoped
@ManagedBean
public class EquiposFacade2022 implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private LeerEquipos2022 leerEquipos= new LeerEquipos2022();
	
	private List<Equipos2022> resultList = null;

	@SuppressWarnings("unchecked")
	public List<Equipos2022> getResultList() {
		if(resultList == null){
			resultList = (List<Equipos2022>) leerEquipos.listResult();
		}
		return resultList;
	}

	public Object selected() {
		// TODO Auto-generated method stub
		return null;
	}

	public Object read(Integer id) {
		return leerEquipos.read(id, true);
	}

	public String insert(Object insert) {
		// TODO Auto-generated method stub
		return null;
	}

	public String update(Object update) {
		// TODO Auto-generated method stub
		return null;
	}

	public String delete(Object delete) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Integer getMaxRowsPaginator(){
		return 10;
	}
	
	public boolean isEnabledPaginator(){
		return getResultList() != null ? (getResultList().size() > getMaxRowsPaginator()) : false;
	}
	
	public String readName(Integer id) {
		Equipos2022 equipo = (Equipos2022) leerEquipos.read(id, false);
		if(equipo != null){
			return equipo.getNombre();
		}
		return null;
	}
	
	

}
