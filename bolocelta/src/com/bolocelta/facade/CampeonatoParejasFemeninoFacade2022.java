package com.bolocelta.facade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import com.bolocelta.application.session.SessionState;
import com.bolocelta.bbdd.constants.Activo2022;
import com.bolocelta.bbdd.constants.FasesModelo2022;
import com.bolocelta.bbdd.constants.FasesTabShow2022;
import com.bolocelta.bbdd.createTable.CrearCampeonatoParejasFemenino2022;
import com.bolocelta.bbdd.readTables.LeerCampeonatoParejasFemenino2022;
import com.bolocelta.entities.Fases2022;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseCF2022;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseFC2022;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseFF2022;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseI2022;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseOF2022;
import com.bolocelta.entities.sorteos.parejas.CalendarioFaseSF2022;
import com.bolocelta.entities.sorteos.parejas.ClasificacionFaseI2022;

@Named
@ConversationScoped
@ManagedBean
public class CampeonatoParejasFemeninoFacade2022 implements Serializable {
	
	@Inject
	private SessionState sessionState;
	
	private static final long serialVersionUID = 1L;
	
	private LeerCampeonatoParejasFemenino2022 leerCampeonatoParejasFemenino = new LeerCampeonatoParejasFemenino2022();
	private CrearCampeonatoParejasFemenino2022 crearCampeonatoParejasFemenino = new CrearCampeonatoParejasFemenino2022();
	
	private List<ClasificacionFaseI2022> resultListClasificacionFaseI = null;
	private List<CalendarioFaseI2022> resultListCalendarioFaseI = null;
	private List<CalendarioFaseOF2022> resultListCalendarioFaseOF = null;
	private List<CalendarioFaseCF2022> resultListCalendarioFaseCF = null;
	private List<CalendarioFaseSF2022> resultListCalendarioFaseSF = null;
	private List<CalendarioFaseFC2022> resultListCalendarioFaseFC = null;
	private List<CalendarioFaseFF2022> resultListCalendarioFaseFF = null;
	
	private List<Fases2022> resultListFases = null;
	private List<FasesTabShow2022> resultListFasesTabShow = null;
	private Fases2022 fase = null;
	
	private String observacionesCampeonato1;
	private String observacionesCampeonato2;
	private String observacionesCampeonato3;
	private String observacionesCampeonato4;
	private String observacionesCampeonato5;
	private String observacionesCampeonato6;
	private String observacionesCampeonato7;
	
    private TreeNode cuadro;
    

	public List<ClasificacionFaseI2022> getResultListClasificacionFaseI() {
		if(resultListClasificacionFaseI == null){
			resultListClasificacionFaseI = (List<ClasificacionFaseI2022>) leerCampeonatoParejasFemenino.listResultClasificacionFaseI();
		}
		return resultListClasificacionFaseI;
	}
	
	public List<ClasificacionFaseI2022> getResultListClasificacionFaseIByGrupo(String grupo) {
		List<ClasificacionFaseI2022> resultListClasificacionFaseIByGrupo = new ArrayList<ClasificacionFaseI2022>();
		if(resultListClasificacionFaseI == null){
			resultListClasificacionFaseI = (List<ClasificacionFaseI2022>) leerCampeonatoParejasFemenino.listResultClasificacionFaseI();
		}else{
			for (ClasificacionFaseI2022 clasificacionFaseI : resultListClasificacionFaseI) {
				if(clasificacionFaseI.getGrupo().equalsIgnoreCase(grupo)){
					resultListClasificacionFaseIByGrupo.add(clasificacionFaseI);
				}
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(resultListClasificacionFaseIByGrupo, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				ClasificacionFaseI2022 cec1 = (ClasificacionFaseI2022) o1;
				ClasificacionFaseI2022 cec2 = (ClasificacionFaseI2022) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPt().compareTo(cec2.getPt());
				//2. Si hay empate ordenar por enfrentamiento directo
				if (rpuntos == 0){
					CalendarioFaseI2022 enfrentamientoDirecto = getResultListEnfrentamientoByParejasGrupo(cec1.getGrupo(), cec1.getParejaId(), cec2.getParejaId());
					if(enfrentamientoDirecto == null){
						enfrentamientoDirecto = getResultListEnfrentamientoByParejasGrupo(cec1.getGrupo(), cec2.getParejaId(), cec1.getParejaId());
						rpuntos = enfrentamientoDirecto.getJuegosPareja2().compareTo(enfrentamientoDirecto.getJuegosPareja1());
					}else{
						rpuntos = enfrentamientoDirecto.getJuegosPareja1().compareTo(enfrentamientoDirecto.getJuegosPareja2());
					}
					
				}
				
				//3. Si hay empate en enfrentamiento directo, mirar average general
				if (rpuntos == 0) {
					Integer part1 = cec1.getPf() - cec1.getPc();
					Integer part2 = cec2.getPf() - cec2.getPc();
					int rdifpartidas = part1.compareTo(part2);
					if (rdifpartidas == 0) {
						return cec1.getPt().compareTo(cec2.getPt());
					}
					return rdifpartidas;
				}
				return rpuntos;
			}
		});
		
		Collections.reverse(resultListClasificacionFaseIByGrupo);
		
		return resultListClasificacionFaseIByGrupo;
	}
	
	public ClasificacionFaseI2022 getResultListClasificacionFaseIByPareja(String grupo, Integer parejaId) {
		if(resultListClasificacionFaseI == null){
			resultListClasificacionFaseI = (List<ClasificacionFaseI2022>) leerCampeonatoParejasFemenino.listResultClasificacionFaseI();
		}
		for (ClasificacionFaseI2022 clasificacionFaseI : resultListClasificacionFaseI) {
			if(clasificacionFaseI.getGrupo().equalsIgnoreCase(grupo)){
				if(clasificacionFaseI.getParejaId().equals(parejaId)){
					return clasificacionFaseI;
				}
			}
		}
		return null;
	}
	
	public List<CalendarioFaseI2022> getResultListCalendarioFaseI() {
		if(resultListCalendarioFaseI == null){
			resultListCalendarioFaseI = (List<CalendarioFaseI2022>) leerCampeonatoParejasFemenino.listResultCalendarioFaseI();
		}
		return resultListCalendarioFaseI;
	}
	
	public List<CalendarioFaseOF2022> getResultListCalendarioFaseOF() {
		if(resultListCalendarioFaseOF == null){
			resultListCalendarioFaseOF = (List<CalendarioFaseOF2022>) leerCampeonatoParejasFemenino.listResultCalendarioFaseOF();
		}
		return resultListCalendarioFaseOF;
	}
	
	public List<CalendarioFaseCF2022> getResultListCalendarioFaseCF() {
		if(resultListCalendarioFaseCF == null){
			resultListCalendarioFaseCF = (List<CalendarioFaseCF2022>) leerCampeonatoParejasFemenino.listResultCalendarioFaseCF();
		}
		return resultListCalendarioFaseCF;
	}
	
	public List<CalendarioFaseSF2022> getResultListCalendarioFaseSF() {
		if(resultListCalendarioFaseSF == null){
			resultListCalendarioFaseSF = (List<CalendarioFaseSF2022>) leerCampeonatoParejasFemenino.listResultCalendarioFaseSF();
		}
		return resultListCalendarioFaseSF;
	}
	
	public List<CalendarioFaseFC2022> getResultListCalendarioFaseFC() {
		if(resultListCalendarioFaseFC == null){
			resultListCalendarioFaseFC = (List<CalendarioFaseFC2022>) leerCampeonatoParejasFemenino.listResultCalendarioFaseFC();
		}
		return resultListCalendarioFaseFC;
	}
	
	public List<CalendarioFaseFF2022> getResultListCalendarioFaseFF() {
		if(resultListCalendarioFaseFF == null){
			resultListCalendarioFaseFF = (List<CalendarioFaseFF2022>) leerCampeonatoParejasFemenino.listResultCalendarioFaseFF();
		}
		return resultListCalendarioFaseFF;
	}
	
	public List<CalendarioFaseI2022> getResultListCalendarioFaseIByGrupo(String grupo) {
		List<CalendarioFaseI2022> resultListCalendarioFaseIByGrupo = new ArrayList<CalendarioFaseI2022>();
		if(resultListCalendarioFaseI == null){
			resultListCalendarioFaseI = (List<CalendarioFaseI2022>) leerCampeonatoParejasFemenino.listResultCalendarioFaseI();
		}else{
			for (CalendarioFaseI2022 calendarioFaseI : resultListCalendarioFaseI) {
				if(calendarioFaseI.getGrupo().equalsIgnoreCase(grupo)){
					resultListCalendarioFaseIByGrupo.add(calendarioFaseI);
				}
			}
		}
		
		//Ordenar la calendario
		Collections.sort(resultListCalendarioFaseIByGrupo, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				CalendarioFaseI2022 cec1 = (CalendarioFaseI2022) o1;
				CalendarioFaseI2022 cec2 = (CalendarioFaseI2022) o2;
				
				int rpuntos = cec1.getOrden().compareTo(cec2.getOrden());
				return rpuntos;
			}
		});
		
		Collections.reverseOrder();
		
		return resultListCalendarioFaseIByGrupo;
	}
	
	public boolean getResultListCalendarioFaseIByGrupoFinalizado(String grupo) {
		List<CalendarioFaseI2022> resultListCalendarioFaseIByGrupo = new ArrayList<CalendarioFaseI2022>();
		if(resultListCalendarioFaseI == null){
			resultListCalendarioFaseI = (List<CalendarioFaseI2022>) leerCampeonatoParejasFemenino.listResultCalendarioFaseI();
		}else{
			for (CalendarioFaseI2022 calendarioFaseI : resultListCalendarioFaseI) {
				if(calendarioFaseI.getGrupo().equalsIgnoreCase(grupo)){
					resultListCalendarioFaseIByGrupo.add(calendarioFaseI);
				}
			}
		}
		
		for (CalendarioFaseI2022 calendarioFaseI : resultListCalendarioFaseIByGrupo) {
			if(calendarioFaseI.isModificable()){
				return false;
			}
		}
		return true;
	}
	
	public CalendarioFaseI2022 getResultListEnfrentamientoByParejasGrupo(String grupo, Integer pareja1Id, Integer pareja2Id) {
		if(resultListCalendarioFaseI == null){
			resultListCalendarioFaseI = (List<CalendarioFaseI2022>) leerCampeonatoParejasFemenino.listResultCalendarioFaseI();
		}
		for (CalendarioFaseI2022 calendarioFaseI : resultListCalendarioFaseI) {
			if(calendarioFaseI.getGrupo().equalsIgnoreCase(grupo)){
				if(calendarioFaseI.getPareja1Id().equals(pareja1Id) && calendarioFaseI.getPareja2Id().equals(pareja2Id)){
					return calendarioFaseI;
				}
			}
		}
		return null;
	}
	
	public List<?> getResultListCalendarioByCruceDirecto(Integer numeroFase) {
		
		List<?> resultListCalendarioByCruceDirecto = new ArrayList<>();
		if(numeroFase.equals(FasesModelo2022.FASE_OF)){
			if(resultListCalendarioFaseOF == null){
				resultListCalendarioFaseOF = (List<CalendarioFaseOF2022>) obtenerCalendarioByCruceDirectoOF();
			}
			return resultListCalendarioFaseOF;
		}else if(numeroFase.equals(FasesModelo2022.FASE_CF)){
			if(resultListCalendarioFaseCF == null){
				resultListCalendarioFaseCF = (List<CalendarioFaseCF2022>) obtenerCalendarioByCruceDirectoCF();
			}
			return resultListCalendarioFaseCF;
		}else if(numeroFase.equals(FasesModelo2022.FASE_SF)){
			if(resultListCalendarioFaseSF == null){
				resultListCalendarioFaseSF = (List<CalendarioFaseSF2022>) obtenerCalendarioByCruceDirectoSF();
			}
			return resultListCalendarioFaseSF;
		}else if(numeroFase.equals(FasesModelo2022.FASE_FC)){
			if(resultListCalendarioFaseFC == null){
				resultListCalendarioFaseFC = (List<CalendarioFaseFC2022>) obtenerCalendarioByCruceDirectoFC();
			}
			return resultListCalendarioFaseFC;
		}else if(numeroFase.equals(FasesModelo2022.FASE_FF)){
			if(resultListCalendarioFaseFF == null){
				resultListCalendarioFaseFF = (List<CalendarioFaseFF2022>) obtenerCalendarioByCruceDirectoFF();
			}
			return resultListCalendarioFaseFF;
		}
		return resultListCalendarioByCruceDirecto;
	}
	
	public List<CalendarioFaseOF2022> obtenerCalendarioByCruceDirectoOF(){
		if(resultListCalendarioFaseOF == null){
			resultListCalendarioFaseOF = (List<CalendarioFaseOF2022>) getResultListCalendarioFaseOF();
		}
		//Ordenar la calendario
		Collections.sort(resultListCalendarioFaseOF, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				CalendarioFaseOF2022 cec1 = (CalendarioFaseOF2022) o1;
				CalendarioFaseOF2022 cec2 = (CalendarioFaseOF2022) o2;
				
				int rpuntos = cec1.getOrden().compareTo(cec2.getOrden());
				return rpuntos;
			}
		});
		
		Collections.reverseOrder();
		return resultListCalendarioFaseOF;
	}
	
	public List<CalendarioFaseCF2022> obtenerCalendarioByCruceDirectoCF(){
		if(resultListCalendarioFaseCF == null){
			resultListCalendarioFaseCF = (List<CalendarioFaseCF2022>) getResultListCalendarioFaseCF();
		}
		//Ordenar la calendario
		Collections.sort(resultListCalendarioFaseCF, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				CalendarioFaseCF2022 cec1 = (CalendarioFaseCF2022) o1;
				CalendarioFaseCF2022 cec2 = (CalendarioFaseCF2022) o2;
				
				int rpuntos = cec1.getOrden().compareTo(cec2.getOrden());
				return rpuntos;
			}
		});
		
		Collections.reverseOrder();
		return resultListCalendarioFaseCF;
	}
	
	public List<CalendarioFaseSF2022> obtenerCalendarioByCruceDirectoSF(){
		if(resultListCalendarioFaseSF == null){
			resultListCalendarioFaseSF = (List<CalendarioFaseSF2022>) getResultListCalendarioFaseSF();
		}
		//Ordenar la calendario
		Collections.sort(resultListCalendarioFaseSF, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				CalendarioFaseSF2022 cec1 = (CalendarioFaseSF2022) o1;
				CalendarioFaseSF2022 cec2 = (CalendarioFaseSF2022) o2;
				
				int rpuntos = cec1.getOrden().compareTo(cec2.getOrden());
				return rpuntos;
			}
		});
		
		Collections.reverseOrder();
		return resultListCalendarioFaseSF;
	}
	
	public List<CalendarioFaseFC2022> obtenerCalendarioByCruceDirectoFC(){
		if(resultListCalendarioFaseFC == null){
			resultListCalendarioFaseFC = (List<CalendarioFaseFC2022>) getResultListCalendarioFaseFC();
		}
		//Ordenar la calendario
		Collections.sort(resultListCalendarioFaseFC, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				CalendarioFaseFC2022 cec1 = (CalendarioFaseFC2022) o1;
				CalendarioFaseFC2022 cec2 = (CalendarioFaseFC2022) o2;
				
				int rpuntos = cec1.getOrden().compareTo(cec2.getOrden());
				return rpuntos;
			}
		});
		
		Collections.reverseOrder();
		return resultListCalendarioFaseFC;
	}
	
	public List<CalendarioFaseFF2022> obtenerCalendarioByCruceDirectoFF(){
		if(resultListCalendarioFaseFF == null){
			resultListCalendarioFaseFF = (List<CalendarioFaseFF2022>) getResultListCalendarioFaseFF();
		}
		//Ordenar la calendario
		Collections.sort(resultListCalendarioFaseFF, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				CalendarioFaseFF2022 cec1 = (CalendarioFaseFF2022) o1;
				CalendarioFaseFF2022 cec2 = (CalendarioFaseFF2022) o2;
				
				int rpuntos = cec1.getOrden().compareTo(cec2.getOrden());
				return rpuntos;
			}
		});
		
		Collections.reverseOrder();
		return resultListCalendarioFaseFF;
	}
	
	
	
	public List<Fases2022> getResultListFases() {
		if(resultListFases == null){
			resultListFases = (List<Fases2022>) leerCampeonatoParejasFemenino.listResultFases();
		}
		return resultListFases;
	}
	
	public boolean isDataCampeonato() {
		if(getResultListFasesTabShow().size() > 0){
			return true;
		}
		return false;
	}
	
	public List<FasesTabShow2022> getResultListFasesTabShow() {
		if(resultListFasesTabShow == null){
			resultListFasesTabShow = new ArrayList<>();
			for(Fases2022 fase : getResultListFases()){
				if(fase.getActivo().equals(Activo2022.SI)){
					if(fase.getNumero().equals(FasesModelo2022.FASE_I)){
						List<FasesTabShow2022> fasesList = (List<FasesTabShow2022>) leerCampeonatoParejasFemenino.listResultGruposFaseI();
						resultListFasesTabShow.addAll(fasesList);
					}else if(fase.getNumero().equals(FasesModelo2022.FASE_II)){
						//De momento nada
					}else if(fase.getNumero().equals(FasesModelo2022.FASE_OF)){
						if(obtenerCalendarioByCruceDirectoOF() != null && obtenerCalendarioByCruceDirectoOF().size() > 0){
							resultListFasesTabShow.add(FasesTabShow2022.searchFaseTabShow(FasesModelo2022.FASE_OF, null));	
						}
					}else if(fase.getNumero().equals(FasesModelo2022.FASE_CF)){
						if(obtenerCalendarioByCruceDirectoCF() != null && obtenerCalendarioByCruceDirectoCF().size() > 0){
							resultListFasesTabShow.add(FasesTabShow2022.searchFaseTabShow(FasesModelo2022.FASE_CF, null));
						}
					}else if(fase.getNumero().equals(FasesModelo2022.FASE_SF)){
						if(obtenerCalendarioByCruceDirectoSF() != null && obtenerCalendarioByCruceDirectoSF().size() > 0){
							resultListFasesTabShow.add(FasesTabShow2022.searchFaseTabShow(FasesModelo2022.FASE_SF, null));
						}
					}else if(fase.getNumero().equals(FasesModelo2022.FASE_FC)){
						if(obtenerCalendarioByCruceDirectoFC() != null && obtenerCalendarioByCruceDirectoFC().size() > 0){
							resultListFasesTabShow.add(FasesTabShow2022.searchFaseTabShow(FasesModelo2022.FASE_FC, null));
						}
					}else if(fase.getNumero().equals(FasesModelo2022.FASE_FF)){
						if(obtenerCalendarioByCruceDirectoFF() != null && obtenerCalendarioByCruceDirectoFF().size() > 0){
							resultListFasesTabShow.add(FasesTabShow2022.searchFaseTabShow(FasesModelo2022.FASE_FF, null));
							//A�adir grafico
							resultListFasesTabShow.add(FasesTabShow2022.FASE_GRAPHIC);
						}
					}
				}
			}
		}
		return resultListFasesTabShow;
	}

	public Object selected() {
		// TODO Auto-generated method stub
		return null;
	}

	public Object read(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	public String insert(Object insert) {
		// TODO Auto-generated method stub
		return null;
	}

	public String update(Object update) {
		// TODO Auto-generated method stub
		return null;
	}

	public String delete(Object delete) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Integer getRowsPaginatorClasificacionFaseI(){
		return getResultListClasificacionFaseI().size();
	}
	
	public Integer getRowsPaginatorCalendarioFaseI(){
		return getResultListCalendarioFaseI().size()/2;
	}
	
	public Integer getTotalRowsClasificacionFaseI(){
		return getResultListClasificacionFaseI().size();
	}
	
	public Integer getRowsPaginatorClasificacionFaseIByGrupo(String grupo){
		return getResultListClasificacionFaseIByGrupo(grupo).size();
	}
	
	public Integer getRowsPaginatorCalendarioFaseIByGrupo(String grupo){
		if(getResultListCalendarioFaseIByGrupo(grupo).size() > 10){
			return getResultListCalendarioFaseIByGrupo(grupo).size()/2;
		}
		return getResultListCalendarioFaseIByGrupo(grupo).size();
	}

	public Fases2022 getFase(Integer numeroFase) {
		this.fase = leerCampeonatoParejasFemenino.readFases(numeroFase);
		return this.fase;
	}

	public String getObservacionesCampeonato1() {
		if(this.observacionesCampeonato1 == null){
			this.observacionesCampeonato1 = leerCampeonatoParejasFemenino.getObservacionesCampeonato1();
			
		}
		return this.observacionesCampeonato1;
	}
	
	public String getObservacionesCampeonato2() {
		if(this.observacionesCampeonato2 == null){
			this.observacionesCampeonato2 = leerCampeonatoParejasFemenino.getObservacionesCampeonato2();
			
		}
		return this.observacionesCampeonato2;
	}
	
	public String getObservacionesCampeonato3() {
		if(this.observacionesCampeonato3 == null){
			this.observacionesCampeonato3 = leerCampeonatoParejasFemenino.getObservacionesCampeonato3();
			
		}
		return this.observacionesCampeonato3;
	}
	
	public String getObservacionesCampeonato4() {
		if(this.observacionesCampeonato4 == null){
			this.observacionesCampeonato4 = leerCampeonatoParejasFemenino.getObservacionesCampeonato4();
			
		}
		return this.observacionesCampeonato4;
	}
	
	public String getObservacionesCampeonato5() {
		if(this.observacionesCampeonato5 == null){
			this.observacionesCampeonato5 = leerCampeonatoParejasFemenino.getObservacionesCampeonato5();
			
		}
		return this.observacionesCampeonato5;
	}
	
	public String getObservacionesCampeonato6() {
		if(this.observacionesCampeonato6 == null){
			this.observacionesCampeonato6 = leerCampeonatoParejasFemenino.getObservacionesCampeonato6();
			
		}
		return this.observacionesCampeonato6;
	}
	
	public String getObservacionesCampeonato7() {
		if(this.observacionesCampeonato7 == null){
			this.observacionesCampeonato7 = leerCampeonatoParejasFemenino.getObservacionesCampeonato7();
			
		}
		return this.observacionesCampeonato7;
	}
	
	
 
    public TreeNode getCuadro() {
    	
    	if(cuadro == null){
    		
    		List<CalendarioFaseFF2022> calendarioFaseFFList = getResultListCalendarioFaseFF();
    		List<CalendarioFaseSF2022> calendarioFaseSFList = getResultListCalendarioFaseSF();
    		List<CalendarioFaseCF2022> calendarioFaseCFList = getResultListCalendarioFaseCF();
    		List<CalendarioFaseOF2022> calendarioFaseOFList = getResultListCalendarioFaseOF();
    		
    		
    		
    		//CAMPEON
            String idNodeCampeonato = "CAMPEON";
            TreeNode nodeCampeonato = null;
            nodeCampeonato = recuperarNodoCalendarioCampeon(nodeCampeonato, calendarioFaseFFList, idNodeCampeonato, nodeCampeonato);
            nodeCampeonato.setExpanded(true);
    		
    		
    		
    		//FINALISTAS
            String idNodeFF1A = "SF1";
            TreeNode nodeFF1A = null;
            nodeFF1A = recuperarNodoFinalistas(nodeCampeonato, calendarioFaseSFList, idNodeFF1A, nodeFF1A);
            nodeFF1A.setExpanded(true);
            
            String idNodeFF1B = "SF2";
            TreeNode nodeFF1B = null;
            nodeFF1B = recuperarNodoFinalistas(nodeCampeonato, calendarioFaseSFList, idNodeFF1B, nodeFF1B);
            nodeFF1B.setExpanded(true);
            
            if(calendarioFaseCFList != null && calendarioFaseCFList.size() > 0){
            
	            Integer faseAnteriorCF = calendarioFaseCFList.get(0).getFaseAnterior();
	            
	    		//SEMIFINALISTAS
	            String idNodeSF1A = "CFA";
	            TreeNode nodeSF1A = null;
	            nodeSF1A = recuperarNodoSemiFinalistas(nodeFF1A, calendarioFaseCFList, idNodeSF1A, nodeSF1A);
	            nodeSF1A.setExpanded(true);
	            
	            String idNodeSF1B = "CFB";
	            TreeNode nodeSF1B = null;
	            nodeSF1B = recuperarNodoSemiFinalistas(nodeFF1A, calendarioFaseCFList, idNodeSF1B, nodeSF1B);
	            nodeSF1B.setExpanded(true);
	            
	            String idNodeSF2A = "CFC";
	            TreeNode nodeSF2A = null;
	            nodeSF2A = recuperarNodoSemiFinalistas(nodeFF1B, calendarioFaseCFList, idNodeSF2A, nodeSF2A);
	            nodeSF2A.setExpanded(true);
	            
	            String idNodeSF2B = "CFD";
	            TreeNode nodeSF2B = null;
	            nodeSF2B = recuperarNodoSemiFinalistas(nodeFF1B, calendarioFaseCFList, idNodeSF2B, nodeSF2B);
	            nodeSF2B.setExpanded(true);
	            
	            
	            
	            
	            //CUARTOS DE FINAL
	            //PROCEDENCIA OCTAVOS DE FINAL
	            
	            if(faseAnteriorCF.equals(FasesModelo2022.FASE_OF)){
	                String idNodeCF1A = "OFA";
	                TreeNode nodeCF1A = null;
	                nodeCF1A = recuperarNodoCuartosFinal(nodeSF1A, calendarioFaseOFList, idNodeCF1A, nodeCF1A);
	                nodeCF1A.setExpanded(true);
	                
	                String idNodeCF1B = "OFB";
	                TreeNode nodeCF1B = null;
	                nodeCF1B = recuperarNodoCuartosFinal(nodeSF1A, calendarioFaseOFList, idNodeCF1B, nodeCF1B);
	                nodeCF1B.setExpanded(true);
	                
	                
	                
	                String idNodeCF2A = "OFC";
	                TreeNode nodeCF2A = null;
	                nodeCF2A = recuperarNodoCuartosFinal(nodeSF1B, calendarioFaseOFList, idNodeCF2A, nodeCF2A);
	                nodeCF2A.setExpanded(true);
	                
	                String idNodeCF2B = "OFD";
	                TreeNode nodeCF2B = null;
	                nodeCF2B = recuperarNodoCuartosFinal(nodeSF1B, calendarioFaseOFList, idNodeCF2B, nodeCF2B);
	                nodeCF2B.setExpanded(true);
	                
	                


	                String idNodeCF3A = "OFE";
	                TreeNode nodeCF3A = null;
	                nodeCF3A = recuperarNodoCuartosFinal(nodeSF2A, calendarioFaseOFList, idNodeCF3A, nodeCF3A);
	                nodeCF3A.setExpanded(true);
	                
	                String idNodeCF3B = "OFF";
	                TreeNode nodeCF3B = null;
	                nodeCF3B = recuperarNodoCuartosFinal(nodeSF2A, calendarioFaseOFList, idNodeCF3B, nodeCF3B);
	                nodeCF3B.setExpanded(true);
	                
	                
	                
	                String idNodeCF4A = "OFG";
	                TreeNode nodeCF4A = null;
	                nodeCF4A = recuperarNodoCuartosFinal(nodeSF2B, calendarioFaseOFList, idNodeCF4A, nodeCF4A);
	                nodeCF4A.setExpanded(true);
	                
	                String idNodeCF4B = "OFH";
	                TreeNode nodeCF4B = null;
	                nodeCF4B = recuperarNodoCuartosFinal(nodeSF2B, calendarioFaseOFList, idNodeCF4B, nodeCF4B);
	                nodeCF4B.setExpanded(true);
	                
	                
	                //OCTAVOS DE FINAL
	                //PROCEDENCIA GRUPOS
	                
	                String idNodeOF1A = "OFA";
	                TreeNode nodeOF1A = null;
	                nodeOF1A = recuperarNodoCalendarioOF(nodeCF1A, calendarioFaseOFList, idNodeOF1A, nodeOF1A, true, false);
	                nodeOF1A.setExpanded(true);
	                
	                String idNodeOF1B = "OFA";
	                TreeNode nodeOF1B = null;
	                nodeOF1B = recuperarNodoCalendarioOF(nodeCF1A, calendarioFaseOFList, idNodeOF1B, nodeOF1B, false, true);
	                nodeOF1B.setExpanded(true);
	                
	                String idNodeOF2A = "OFB";
	                TreeNode nodeOF2A = null;
	                nodeOF2A = recuperarNodoCalendarioOF(nodeCF1B, calendarioFaseOFList, idNodeOF2A, nodeOF2A, true, false);
	                nodeOF2A.setExpanded(true);
	                
	                String idNodeOF2B = "OFB";
	                TreeNode nodeOF2B = null;
	                nodeOF2B = recuperarNodoCalendarioOF(nodeCF1B, calendarioFaseOFList, idNodeOF2B, nodeOF2B, false, true);
	                nodeOF2B.setExpanded(true);
	                
	                
	                
	                
	                
	                
	                
	                String idNodeOF3A = "OFC";
	                TreeNode nodeOF3A = null;
	                nodeOF3A = recuperarNodoCalendarioOF(nodeCF2A, calendarioFaseOFList, idNodeOF3A, nodeOF3A, true, false);
	                nodeOF3A.setExpanded(true);
	                
	                String idNodeOF3B = "OFC";
	                TreeNode nodeOF3B = null;
	                nodeOF3B = recuperarNodoCalendarioOF(nodeCF2A, calendarioFaseOFList, idNodeOF3B, nodeOF3B, false, true);
	                nodeOF3B.setExpanded(true);
	                
	                String idNodeOF4A = "OFD";
	                TreeNode nodeOF4A = null;
	                nodeOF4A = recuperarNodoCalendarioOF(nodeCF2B, calendarioFaseOFList, idNodeOF4A, nodeOF4A, true, false);
	                nodeOF4A.setExpanded(true);
	                
	                String idNodeOF4B = "OFD";
	                TreeNode nodeOF4B = null;
	                nodeOF4B = recuperarNodoCalendarioOF(nodeCF2B, calendarioFaseOFList, idNodeOF4B, nodeOF4B, false, true);
	                nodeOF4B.setExpanded(true);
	                
	                
	                
	                
	                
	                
	                
	                
	                
	                
	                
	                
	                
	                
	                
	                
	                
	                
	                
	                
	                
	                
	                
	                String idNodeOF5A = "OFE";
	                TreeNode nodeOF5A = null;
	                nodeOF5A = recuperarNodoCalendarioOF(nodeCF3A, calendarioFaseOFList, idNodeOF5A, nodeOF5A, true, false);
	                nodeOF5A.setExpanded(true);
	                
	                String idNodeOF5B = "OFE";
	                TreeNode nodeOF5B = null;
	                nodeOF5B = recuperarNodoCalendarioOF(nodeCF3A, calendarioFaseOFList, idNodeOF5B, nodeOF5B, false, true);
	                nodeOF5B.setExpanded(true);
	                
	                String idNodeOF6A = "OFF";
	                TreeNode nodeOF6A = null;
	                nodeOF6A = recuperarNodoCalendarioOF(nodeCF3B, calendarioFaseOFList, idNodeOF6A, nodeOF6A, true, false);
	                nodeOF6A.setExpanded(true);
	                
	                String idNodeOF6B = "OFF";
	                TreeNode nodeOF6B = null;
	                nodeOF6B = recuperarNodoCalendarioOF(nodeCF3B, calendarioFaseOFList, idNodeOF6B, nodeOF6B, false, true);
	                nodeOF6B.setExpanded(true);
	                
	                
	                
	                
	                
	                
	                
	                String idNodeOF7A = "OFG";
	                TreeNode nodeOF7A = null;
	                nodeOF7A = recuperarNodoCalendarioOF(nodeCF4A, calendarioFaseOFList, idNodeOF7A, nodeOF7A, true, false);
	                nodeOF7A.setExpanded(true);
	                
	                String idNodeOF7B = "OFG";
	                TreeNode nodeOF7B = null;
	                nodeOF7B = recuperarNodoCalendarioOF(nodeCF4A, calendarioFaseOFList, idNodeOF7B, nodeOF7B, false, true);
	                nodeOF7B.setExpanded(true);
	                
	                String idNodeOF8A = "OFH";
	                TreeNode nodeOF8A = null;
	                nodeOF8A = recuperarNodoCalendarioOF(nodeCF4B, calendarioFaseOFList, idNodeOF8A, nodeOF8A, true, false);
	                nodeOF8A.setExpanded(true);
	                
	                String idNodeOF8B = "OFH";
	                TreeNode nodeOF8B = null;
	                nodeOF8B = recuperarNodoCalendarioOF(nodeCF4B, calendarioFaseOFList, idNodeOF8B, nodeOF8B, false, true);
	                nodeOF8B.setExpanded(true);
	                
	                
	                
	            }else if(faseAnteriorCF.equals(FasesModelo2022.FASE_I)){
	                String idNodeCF1A = "CFA";
	                TreeNode nodeCF1A = null;
	                nodeCF1A = recuperarNodoCalendarioGruposCF(nodeSF1A, calendarioFaseCFList, idNodeCF1A, nodeCF1A, true, false);
	                nodeCF1A.setExpanded(true);
	                
	                String idNodeCF1B = "CFA";
	                TreeNode nodeCF1B = null;
	                nodeCF1B = recuperarNodoCalendarioGruposCF(nodeSF1A, calendarioFaseCFList, idNodeCF1B, nodeCF1B, false, true);
	                nodeCF1B.setExpanded(true);
	                
	                
	                
	                String idNodeCF2A = "CFB";
	                TreeNode nodeCF2A = null;
	                nodeCF2A = recuperarNodoCalendarioGruposCF(nodeSF1B, calendarioFaseCFList, idNodeCF2A, nodeCF2A, true, false);
	                nodeCF2A.setExpanded(true);
	                
	                String idNodeCF2B = "CFB";
	                TreeNode nodeCF2B = null;
	                nodeCF2B = recuperarNodoCalendarioGruposCF(nodeSF1B, calendarioFaseCFList, idNodeCF2B, nodeCF2B, false, true);
	                nodeCF2B.setExpanded(true);
	                
	                


	                String idNodeCF3A = "CFC";
	                TreeNode nodeCF3A = null;
	                nodeCF3A = recuperarNodoCalendarioGruposCF(nodeSF2A, calendarioFaseCFList, idNodeCF3A, nodeCF3A, true, false);
	                nodeCF3A.setExpanded(true);
	                
	                String idNodeCF3B = "CFC";
	                TreeNode nodeCF3B = null;
	                nodeCF3B = recuperarNodoCalendarioGruposCF(nodeSF2A, calendarioFaseCFList, idNodeCF3B, nodeCF3B, false, true);
	                nodeCF3B.setExpanded(true);
	                
	                
	                
	                String idNodeCF4A = "CFD";
	                TreeNode nodeCF4A = null;
	                nodeCF4A = recuperarNodoCalendarioGruposCF(nodeSF2B, calendarioFaseCFList, idNodeCF4A, nodeCF4A, true, false);
	                nodeCF4A.setExpanded(true);
	                
	                String idNodeCF4B = "CFD";
	                TreeNode nodeCF4B = null;
	                nodeCF4B = recuperarNodoCalendarioGruposCF(nodeSF2B, calendarioFaseCFList, idNodeCF4B, nodeCF4B, false, true);
	                nodeCF4B.setExpanded(true);
	            }
	            
            }
            
            cuadro = nodeCampeonato;

            
    	}
    	
        return cuadro;
    }
    
	private TreeNode recuperarNodoCalendarioCampeon(TreeNode nodeTo, List<CalendarioFaseFF2022> calendarioFaseFFList,
			String idNode, TreeNode node) {
		for (CalendarioFaseFF2022 calendarioFaseFF : calendarioFaseFFList) {
        	if( calendarioFaseFF.getPareja1Id() != null && calendarioFaseFF.getPareja1Id() > 0 && 
            		calendarioFaseFF.getPareja2Id() != null && calendarioFaseFF.getPareja2Id() > 0){
        			if(calendarioFaseFF.isGanaPareja() == 1){
        				node = new DefaultTreeNode(calendarioFaseFF.getPareja1().getNombreEquipoShowCuadro() + "!!! CAMPE�N !!!", node);
        			}else if(calendarioFaseFF.isGanaPareja() == 2){
        				node = new DefaultTreeNode(calendarioFaseFF.getPareja2().getNombreEquipoShowCuadro() + "!!! CAMPE�N !!!", node);
        			}
				}
		}
		if(node == null){
			node = new DefaultTreeNode(idNode, nodeTo);
		}
		return node;
	}
	
	private TreeNode recuperarNodoFinalistas(TreeNode nodeTo, List<CalendarioFaseSF2022> calendarioFaseSFList,
			String idNode, TreeNode node) {
	      for (CalendarioFaseSF2022 calendarioFaseSF : calendarioFaseSFList) {
	    	if(calendarioFaseSF.getIdCruce().equals(idNode)){
	        	if( calendarioFaseSF.getPareja1Id() != null && calendarioFaseSF.getPareja1Id() > 0 && 
	        		calendarioFaseSF.getPareja2Id() != null && calendarioFaseSF.getPareja2Id() > 0){
	    			if(calendarioFaseSF.isGanaPareja() == 1){
	    				node = new DefaultTreeNode(calendarioFaseSF.getPareja1().getNombreEquipoShowCuadro(), nodeTo);
	    			}else if(calendarioFaseSF.isGanaPareja() == 2){
	    				node = new DefaultTreeNode(calendarioFaseSF.getPareja2().getNombreEquipoShowCuadro(), nodeTo);
	    			}
				}
	    	}
		}
		if(node == null){
			node = new DefaultTreeNode(idNode, nodeTo);
		}
		return node;
	}
	
	private TreeNode recuperarNodoSemiFinalistas(TreeNode nodeTo, List<CalendarioFaseCF2022> calendarioFaseCFList,
			String idNode, TreeNode node) {
	      for (CalendarioFaseCF2022 calendarioFaseCF : calendarioFaseCFList) {
	    	if(calendarioFaseCF.getIdCruce().equals(idNode)){
	        	if( calendarioFaseCF.getPareja1Id() != null && calendarioFaseCF.getPareja1Id() > 0 && 
	        		calendarioFaseCF.getPareja2Id() != null && calendarioFaseCF.getPareja2Id() > 0){
	    			if(calendarioFaseCF.isGanaPareja() == 1){
	    				node = new DefaultTreeNode(calendarioFaseCF.getPareja1().getNombreEquipoShowCuadro(), nodeTo);
	    			}else if(calendarioFaseCF.isGanaPareja() == 2){
	    				node = new DefaultTreeNode(calendarioFaseCF.getPareja2().getNombreEquipoShowCuadro(), nodeTo);
	    			}
				}
	    	}
		}
		if(node == null){
			node = new DefaultTreeNode(idNode, nodeTo);
		}
		return node;
	}
	
	private TreeNode recuperarNodoCuartosFinal(TreeNode nodeTo, List<CalendarioFaseOF2022> calendarioFaseOFList,
			String idNode, TreeNode node) {
	      for (CalendarioFaseOF2022 calendarioFaseOF : calendarioFaseOFList) {
	    	if(calendarioFaseOF.getIdCruce().equals(idNode)){
	        	if( calendarioFaseOF.getPareja1Id() != null && calendarioFaseOF.getPareja1Id() > 0 && 
	        		calendarioFaseOF.getPareja2Id() != null && calendarioFaseOF.getPareja2Id() > 0){
	    			if(calendarioFaseOF.isGanaPareja() == 1){
	    				node = new DefaultTreeNode(calendarioFaseOF.getPareja1().getNombreEquipoShowCuadro(), nodeTo);
	    			}else if(calendarioFaseOF.isGanaPareja() == 2){
	    				node = new DefaultTreeNode(calendarioFaseOF.getPareja2().getNombreEquipoShowCuadro(), nodeTo);
	    			}
				}
	    	}
		}
		if(node == null){
			node = new DefaultTreeNode(idNode, nodeTo);
		}
		return node;
	}
	
	private TreeNode recuperarNodoCalendarioGruposCF(TreeNode nodeTo, List<CalendarioFaseCF2022> calendarioFaseCFList,
			String idNode, TreeNode node, boolean pareja1, boolean pareja2) {
		for (CalendarioFaseCF2022 calendarioFaseCF : calendarioFaseCFList) {
			if(pareja1){
				if(calendarioFaseCF.getIdCruce().equals(idNode)){
			    	if( calendarioFaseCF.getPareja1Id() != null && calendarioFaseCF.getPareja1Id() > 0){
						node = new DefaultTreeNode(calendarioFaseCF.getPareja1().getNombreEquipoShowCuadro(), nodeTo);
					}else{
						node = new DefaultTreeNode(calendarioFaseCF.getCruce1IdGrupo(), nodeTo);
					}					
				}
			}else if(pareja2){
				if(calendarioFaseCF.getIdCruce().equals(idNode)){
			    	if(calendarioFaseCF.getPareja2Id() != null && calendarioFaseCF.getPareja2Id() > 0){
						node = new DefaultTreeNode(calendarioFaseCF.getPareja2().getNombreEquipoShowCuadro(), nodeTo);
					}else{
						node = new DefaultTreeNode(calendarioFaseCF.getCruce2IdGrupo(), nodeTo);
					}
				}
			}
		}
		if(node == null){
			node = new DefaultTreeNode(idNode, nodeTo);
		}
		return node;
	}

	private TreeNode recuperarNodoCalendarioOF(TreeNode nodeTo, List<CalendarioFaseOF2022> calendarioFaseOFList,
			String idNode, TreeNode node, boolean pareja1, boolean pareja2) {
		for (CalendarioFaseOF2022 calendarioFaseOF : calendarioFaseOFList) {
			if(pareja1){
				if(calendarioFaseOF.getIdCruce().equals(idNode)){
			    	if( calendarioFaseOF.getPareja1Id() != null && calendarioFaseOF.getPareja1Id() > 0){
						node = new DefaultTreeNode(calendarioFaseOF.getPareja1().getNombreEquipoShowCuadro(), nodeTo);
					}else{
						node = new DefaultTreeNode(calendarioFaseOF.getCruce1IdGrupo(), nodeTo);
					}					
				}
			}else if(pareja2){
				if(calendarioFaseOF.getIdCruce().equals(idNode)){
			    	if(calendarioFaseOF.getPareja2Id() != null && calendarioFaseOF.getPareja2Id() > 0){
						node = new DefaultTreeNode(calendarioFaseOF.getPareja2().getNombreEquipoShowCuadro(), nodeTo);
					}else{
						node = new DefaultTreeNode(calendarioFaseOF.getCruce2IdGrupo(), nodeTo);
					}
				}
			}
		}
		if(node == null){
			node = new DefaultTreeNode(idNode, nodeTo);
		}
		return node;
	}
    
    public String getPermisoGrupo(String grupo){
    	return "BCMEGEI1ARG" + grupo;
    }
    
    public void doActualizarResultadoGrupos(CalendarioFaseI2022 cf1){
    	
    	boolean error = false;
		if(cf1.isModificable()){
			
			Fases2022 fase = getFase(FasesModelo2022.FASE_I);
			Integer numeroJuegosFase = fase.getNumeroJuegos();
			Integer numeroJuegosTotalFase = (numeroJuegosFase * 2) - 2;
			Integer numeroJuegosEmpate = numeroJuegosTotalFase / 2;
			
			//Validaciones
			if(cf1.getJuegosPareja1() > numeroJuegosFase || cf1.getJuegosPareja2() > numeroJuegosFase){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El maximo de juegos es " + numeroJuegosFase + " por pareja/partida.", null));
			}else if((cf1.getJuegosPareja1() + cf1.getJuegosPareja2()) > numeroJuegosTotalFase){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El maximo de juegos es para el enfrentamiento es " + numeroJuegosTotalFase + ".", null));
			}else if((cf1.getJuegosPareja1() + cf1.getJuegosPareja2()) < (numeroJuegosFase-1)){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El minimo de juegos es para el enfrentamiento es " + numeroJuegosFase + ".", null));
			}else if(cf1.getJuegosPareja1() < 0 || cf1.getJuegosPareja2() < 0){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No puede haber menos de 0 juegos", null));
			}else if((cf1.getJuegosPareja1() < (numeroJuegosEmpate-1) && cf1.getJuegosPareja2() < (numeroJuegosEmpate-1)) 
					|| (cf1.getJuegosPareja1() < (numeroJuegosEmpate-1) && cf1.getJuegosPareja2() < (numeroJuegosFase-1))
					|| (cf1.getJuegosPareja1() < (numeroJuegosFase-1) && cf1.getJuegosPareja2() < (numeroJuegosEmpate-1))){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Resultado incorrecto.", null));
			}
			
			//Si todo correcto actualizar resultado
			if(!error){
				ClasificacionFaseI2022 clasificacionFaseIPareja1 = getResultListClasificacionFaseIByPareja(cf1.getGrupo(), cf1.getPareja1Id());
				ClasificacionFaseI2022 clasificacionFaseIPareja2 = getResultListClasificacionFaseIByPareja(cf1.getGrupo(), cf1.getPareja2Id());
				crearCampeonatoParejasFemenino.actualizarResultadoCalendarioFaseI(cf1, clasificacionFaseIPareja1, clasificacionFaseIPareja2);
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha actualizado el resultado del Grupo " + cf1.getGrupo() + ".", null));
				
				leerCampeonatoParejasFemenino = new LeerCampeonatoParejasFemenino2022();
				
				//Si todos los resultados han sido modificados clasificar a siguiente fase
				clasificarSiguienteRonda(FasesModelo2022.FASE_I, cf1.getGrupo());

			}
		}
		
	}
    
    public void clasificarSiguienteRonda(Integer numerofaseActual, String grupo){
    	Fases2022 faseActual = buscarSiguiente(numerofaseActual);
    	Fases2022 faseSiguiente = buscarSiguiente(faseActual.getFaseSiguiente());
    	
    	if(grupo != null){
	    	if(faseSiguiente.getNumero().equals(FasesModelo2022.FASE_OF) && faseActual.getNumero().equals(FasesModelo2022.FASE_I)){
	    		boolean faseFinalizada = getResultListCalendarioFaseIByGrupoFinalizado(grupo);
	    		if(faseFinalizada){
	    			doClasificarToOctavosFinal(grupo);
	    		}
	    	}else if(faseSiguiente.getNumero().equals(FasesModelo2022.FASE_CF) && faseActual.getNumero().equals(FasesModelo2022.FASE_I)){
	    		boolean faseFinalizada = getResultListCalendarioFaseIByGrupoFinalizado(grupo);
	    		if(faseFinalizada){
	    			doClasificarToCuartosFinal(grupo);
	    		}
	    	}else if(faseSiguiente.getNumero().equals(FasesModelo2022.FASE_SF) && faseActual.getNumero().equals(FasesModelo2022.FASE_I)){
	    		boolean faseFinalizada = getResultListCalendarioFaseIByGrupoFinalizado(grupo);
	    		if(faseFinalizada){
	    			doClasificarToSemifinal(grupo);
	    		}
	    	}
    	}else if(grupo == null){
    		if(faseSiguiente.getNumero().equals(FasesModelo2022.FASE_CF) && faseActual.getNumero().equals(FasesModelo2022.FASE_OF)){
    			doClasificarToCuartosFinal();
	    	}else if(faseSiguiente.getNumero().equals(FasesModelo2022.FASE_SF) && faseActual.getNumero().equals(FasesModelo2022.FASE_CF)){
	    		doClasificarToSemifinal();
	    	}else if(faseSiguiente.getNumero().equals(FasesModelo2022.FASE_FC) && faseActual.getNumero().equals(FasesModelo2022.FASE_SF)){
	    		doClasificarToFinalConsolacion();
	    	}else if(faseSiguiente.getNumero().equals(FasesModelo2022.FASE_FF) && faseActual.getNumero().equals(FasesModelo2022.FASE_SF)){
	    		doClasificarToFinalConsolacion();
	    		doClasificarToFinal();
	    	}
    	}

    }
    
    public Fases2022 buscarSiguiente(Integer faseBuscar){
		for(Fases2022 fase : getResultListFases()){
			if(fase.getActivo().equals(Activo2022.SI)){
				if(fase.getNumero().equals(faseBuscar)){
					return fase; 
				}else if(fase.getNumero().equals(faseBuscar)){
					//De momento nada
				}else if(fase.getNumero().equals(faseBuscar)){
					return fase;
				}else if(fase.getNumero().equals(faseBuscar)){
					return fase;
				}else if(fase.getNumero().equals(faseBuscar)){
					return fase;
				}else if(fase.getNumero().equals(faseBuscar)){
					return fase;
				}else if(fase.getNumero().equals(faseBuscar)){
					return fase;
				}
			}
		}
		return null;
    }
    
    public void doClasificarToOctavosFinal(String grupo){
    	//Recuperar la clasificacion del grupo
    	List<ClasificacionFaseI2022> clasificacionGrupoList = getResultListClasificacionFaseIByGrupo(grupo);
    	//Recuperar los octavos de final
    	List<CalendarioFaseOF2022> octavosFinalCalendarioList = getResultListCalendarioFaseOF();
    	//Buscar en la fase de octavos de final la posicion de los clasificados del grupo y asignarlos 
    	for (CalendarioFaseOF2022 calendarioFaseOF : octavosFinalCalendarioList) {
			if(calendarioFaseOF.getGrupoProcedenciaPareja1().equalsIgnoreCase(grupo)){
				ClasificacionFaseI2022 clasificadoPosicion = clasificacionGrupoList.get(calendarioFaseOF.getPosicionProcedenciaPareja1()-1);
				calendarioFaseOF.setPareja1Id(clasificadoPosicion.getParejaId());
				calendarioFaseOF.setPareja1(clasificadoPosicion.getPareja());
				crearCampeonatoParejasFemenino.actualizarParejaOctavosFinal(calendarioFaseOF, true, false);
			}
			if(calendarioFaseOF.getGrupoProcedenciaPareja2().equalsIgnoreCase(grupo)){
				ClasificacionFaseI2022 clasificadoPosicion = clasificacionGrupoList.get(calendarioFaseOF.getPosicionProcedenciaPareja2()-1);
				calendarioFaseOF.setPareja2Id(clasificadoPosicion.getParejaId());
				calendarioFaseOF.setPareja2(clasificadoPosicion.getPareja());
				crearCampeonatoParejasFemenino.actualizarParejaOctavosFinal(calendarioFaseOF, false, true);
			}
		}
    	
    	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Clasificados a Octavos de Final parejas del Grupo " + grupo + ".", null));
    	
    	
    }
    
    public void doClasificarToCuartosFinal(){
    	
    	//Recuperar la clasificacion de los octavos de final
    	List<CalendarioFaseOF2022> octavosFinalCalendarioList = getResultListCalendarioFaseOF();
    	//Recuperar los cuartos de final
    	List<CalendarioFaseCF2022> cuartosFinalCalendarioList = getResultListCalendarioFaseCF();
    	//Buscar en la fase de cuartos de final la posicion de los clasificados del grupo y asignarlos 
    	for (CalendarioFaseCF2022 calendarioFaseCF : cuartosFinalCalendarioList) {
    		for (CalendarioFaseOF2022 calendarioFaseOF : octavosFinalCalendarioList) {
				if(calendarioFaseCF.getGrupoProcedenciaPareja1().equalsIgnoreCase(calendarioFaseOF.getIdCruce())){
					if(calendarioFaseOF.isGanaPareja() == 1){
						calendarioFaseCF.setPareja1Id(calendarioFaseOF.getPareja1Id());
						calendarioFaseCF.setPareja1(calendarioFaseOF.getPareja1());
						crearCampeonatoParejasFemenino.actualizarParejaCuartosFinal(calendarioFaseCF, true, false);
					}else if(calendarioFaseOF.isGanaPareja() == 2){
						calendarioFaseCF.setPareja1Id(calendarioFaseOF.getPareja2Id());
						calendarioFaseCF.setPareja1(calendarioFaseOF.getPareja2());
						crearCampeonatoParejasFemenino.actualizarParejaCuartosFinal(calendarioFaseCF, true, false);
					}
				}else  if(calendarioFaseCF.getGrupoProcedenciaPareja2().equalsIgnoreCase(calendarioFaseOF.getIdCruce())){
					if(calendarioFaseOF.isGanaPareja() == 1){
						calendarioFaseCF.setPareja2Id(calendarioFaseOF.getPareja1Id());
						calendarioFaseCF.setPareja2(calendarioFaseOF.getPareja1());
						crearCampeonatoParejasFemenino.actualizarParejaCuartosFinal(calendarioFaseCF, false, true);
					}else if(calendarioFaseOF.isGanaPareja() == 2){
						calendarioFaseCF.setPareja2Id(calendarioFaseOF.getPareja2Id());
						calendarioFaseCF.setPareja2(calendarioFaseOF.getPareja2());
						crearCampeonatoParejasFemenino.actualizarParejaCuartosFinal(calendarioFaseCF, false, true);
					}
				}
    		}
		}
    	
    	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Clasificados a Cuartos de Final parejas de los Octavos de Final.", null));
    	
    }
    
    public void doClasificarToCuartosFinal(String grupo){
    	
     	//Recuperar la clasificacion del grupo
    	List<ClasificacionFaseI2022> clasificacionGrupoList = getResultListClasificacionFaseIByGrupo(grupo);
    	//Recuperar los cuartos de final
    	List<CalendarioFaseCF2022> cuartosFinalCalendarioList = getResultListCalendarioFaseCF();
    	//Buscar en la fase de cuartos de final la posicion de los clasificados del grupo y asignarlos 
    	for (CalendarioFaseCF2022 calendarioFaseCF : cuartosFinalCalendarioList) {
			if(calendarioFaseCF.getGrupoProcedenciaPareja1().equalsIgnoreCase(grupo)){
				ClasificacionFaseI2022 clasificadoPosicion = clasificacionGrupoList.get(calendarioFaseCF.getPosicionProcedenciaPareja1()-1);
				calendarioFaseCF.setPareja1Id(clasificadoPosicion.getParejaId());
				calendarioFaseCF.setPareja1(clasificadoPosicion.getPareja());
				crearCampeonatoParejasFemenino.actualizarParejaCuartosFinal(calendarioFaseCF, true, false);
			}
			if(calendarioFaseCF.getGrupoProcedenciaPareja2().equalsIgnoreCase(grupo)){
				ClasificacionFaseI2022 clasificadoPosicion = clasificacionGrupoList.get(calendarioFaseCF.getPosicionProcedenciaPareja2()-1);
				calendarioFaseCF.setPareja2Id(clasificadoPosicion.getParejaId());
				calendarioFaseCF.setPareja2(clasificadoPosicion.getPareja());
				crearCampeonatoParejasFemenino.actualizarParejaCuartosFinal(calendarioFaseCF, false, true);
			}
		}
    	
    	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Clasificados a Cuartos de Final parejas del Grupo " + grupo + ".", null));
    	
    }
    
   public void doClasificarToSemifinal(){
    	
    	//Recuperar la clasificacion de los cuartos de final
    	List<CalendarioFaseCF2022> cuartosFinalCalendarioList = getResultListCalendarioFaseCF();
    	//Recuperar los semifinales
    	List<CalendarioFaseSF2022> semifinalCalendarioList = getResultListCalendarioFaseSF();
    	//Buscar en la fase de semifinal la posicion de los clasificados del grupo y asignarlos 
    	for (CalendarioFaseSF2022 calendarioFaseSF : semifinalCalendarioList) {
    		for (CalendarioFaseCF2022 calendarioFaseCF : cuartosFinalCalendarioList) {
				if(calendarioFaseSF.getGrupoProcedenciaPareja1().equalsIgnoreCase(calendarioFaseCF.getIdCruce())){
					if(calendarioFaseCF.isGanaPareja() == 1){
						calendarioFaseSF.setPareja1Id(calendarioFaseCF.getPareja1Id());
						calendarioFaseSF.setPareja1(calendarioFaseCF.getPareja1());
						crearCampeonatoParejasFemenino.actualizarParejaSemiFinal(calendarioFaseSF, true, false);
					}else if(calendarioFaseCF.isGanaPareja() == 2){
						calendarioFaseSF.setPareja1Id(calendarioFaseCF.getPareja2Id());
						calendarioFaseSF.setPareja1(calendarioFaseCF.getPareja2());
						crearCampeonatoParejasFemenino.actualizarParejaSemiFinal(calendarioFaseSF, true, false);
					}
				}else  if(calendarioFaseSF.getGrupoProcedenciaPareja2().equalsIgnoreCase(calendarioFaseCF.getIdCruce())){
					if(calendarioFaseCF.isGanaPareja() == 1){
						calendarioFaseSF.setPareja2Id(calendarioFaseCF.getPareja1Id());
						calendarioFaseSF.setPareja2(calendarioFaseCF.getPareja1());
						crearCampeonatoParejasFemenino.actualizarParejaSemiFinal(calendarioFaseSF, false, true);
					}else if(calendarioFaseCF.isGanaPareja() == 2){
						calendarioFaseSF.setPareja2Id(calendarioFaseCF.getPareja2Id());
						calendarioFaseSF.setPareja2(calendarioFaseCF.getPareja2());
						crearCampeonatoParejasFemenino.actualizarParejaSemiFinal(calendarioFaseSF, false, true);
					}
				}
    		}
		}
    	
    	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Clasificados a Semifinales parejas de los Cuartos de Final.", null));
    	
    }
    
    public void doClasificarToSemifinal(String grupo){
    	

    	//Recuperar la clasificacion del grupo
    	List<ClasificacionFaseI2022> clasificacionGrupoList = getResultListClasificacionFaseIByGrupo(grupo);
    	//Recuperar los semifinal
    	List<CalendarioFaseSF2022> semifinalCalendarioList = getResultListCalendarioFaseSF();
    	//Buscar en la fase de semifinal la posicion de los clasificados del grupo y asignarlos 
    	for (CalendarioFaseSF2022 calendarioFaseSF : semifinalCalendarioList) {
			if(calendarioFaseSF.getGrupoProcedenciaPareja1().equalsIgnoreCase(grupo)){
				ClasificacionFaseI2022 clasificadoPosicion = clasificacionGrupoList.get(calendarioFaseSF.getPosicionProcedenciaPareja1()-1);
				calendarioFaseSF.setPareja1Id(clasificadoPosicion.getParejaId());
				calendarioFaseSF.setPareja1(clasificadoPosicion.getPareja());
				crearCampeonatoParejasFemenino.actualizarParejaSemiFinal(calendarioFaseSF, true, false);
			}
			if(calendarioFaseSF.getGrupoProcedenciaPareja2().equalsIgnoreCase(grupo)){
				ClasificacionFaseI2022 clasificadoPosicion = clasificacionGrupoList.get(calendarioFaseSF.getPosicionProcedenciaPareja2()-1);
				calendarioFaseSF.setPareja2Id(clasificadoPosicion.getParejaId());
				calendarioFaseSF.setPareja2(clasificadoPosicion.getPareja());
				crearCampeonatoParejasFemenino.actualizarParejaSemiFinal(calendarioFaseSF, false, true);
			}
		}
    	
    	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Clasificados a Semifinales parejas del Grupo " + grupo + ".", null));
    	
    }
    
    public void doClasificarToFinalConsolacion(){
    	
    	//Recuperar la clasificacion de las semifinales
    	List<CalendarioFaseSF2022> semifinalCalendarioList = getResultListCalendarioFaseSF();
    	//Recuperar los semifinales
    	List<CalendarioFaseFC2022> finalConsolacionCalendarioList = getResultListCalendarioFaseFC();
    	//Buscar en la fase de semifinal la posicion de los clasificados del grupo y asignarlos 
    	for (CalendarioFaseFC2022 calendarioFaseFC : finalConsolacionCalendarioList) {
    		for (CalendarioFaseSF2022 calendarioFaseSF : semifinalCalendarioList) {
				if(calendarioFaseFC.getGrupoProcedenciaPareja1().equalsIgnoreCase(calendarioFaseSF.getIdCruce())){
					if(calendarioFaseSF.isGanaPareja() == 1){
						calendarioFaseFC.setPareja1Id(calendarioFaseSF.getPareja2Id());
						calendarioFaseFC.setPareja1(calendarioFaseSF.getPareja2());
						crearCampeonatoParejasFemenino.actualizarParejaFinalConsolacion(calendarioFaseFC, true, false);
					}else if(calendarioFaseSF.isGanaPareja() == 2){
						calendarioFaseFC.setPareja1Id(calendarioFaseSF.getPareja1Id());
						calendarioFaseFC.setPareja1(calendarioFaseSF.getPareja1());
						crearCampeonatoParejasFemenino.actualizarParejaFinalConsolacion(calendarioFaseFC, true, false);
					}
				}else  if(calendarioFaseFC.getGrupoProcedenciaPareja2().equalsIgnoreCase(calendarioFaseSF.getIdCruce())){
					if(calendarioFaseSF.isGanaPareja() == 1){
						calendarioFaseFC.setPareja2Id(calendarioFaseSF.getPareja2Id());
						calendarioFaseFC.setPareja2(calendarioFaseSF.getPareja2());
						crearCampeonatoParejasFemenino.actualizarParejaFinalConsolacion(calendarioFaseFC, false, true);
					}else if(calendarioFaseSF.isGanaPareja() == 2){
						calendarioFaseFC.setPareja2Id(calendarioFaseSF.getPareja1Id());
						calendarioFaseFC.setPareja2(calendarioFaseSF.getPareja1());
						crearCampeonatoParejasFemenino.actualizarParejaFinalConsolacion(calendarioFaseFC, false, true);
					}
				}
    		}
		}	
    	
    	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Clasificados a Final de Consolacion parejas de las Semifinales.", null));
    }
    
    public void doClasificarToFinal(){
    	
    	//Recuperar la clasificacion de las semifinales
    	List<CalendarioFaseSF2022> semifinalCalendarioList = getResultListCalendarioFaseSF();
    	//Recuperar los semifinales
    	List<CalendarioFaseFF2022> finalCalendarioList = getResultListCalendarioFaseFF();
    	//Buscar en la fase de semifinal la posicion de los clasificados del grupo y asignarlos 
    	for (CalendarioFaseFF2022 calendarioFaseFF : finalCalendarioList) {
    		for (CalendarioFaseSF2022 calendarioFaseSF : semifinalCalendarioList) {
				if(calendarioFaseFF.getGrupoProcedenciaPareja1().equalsIgnoreCase(calendarioFaseSF.getIdCruce())){
					if(calendarioFaseSF.isGanaPareja() == 1){
						calendarioFaseFF.setPareja1Id(calendarioFaseSF.getPareja1Id());
						calendarioFaseFF.setPareja1(calendarioFaseSF.getPareja1());
						crearCampeonatoParejasFemenino.actualizarParejaFinal(calendarioFaseFF, true, false);
					}else if(calendarioFaseSF.isGanaPareja() == 2){
						calendarioFaseFF.setPareja1Id(calendarioFaseSF.getPareja2Id());
						calendarioFaseFF.setPareja1(calendarioFaseSF.getPareja2());
						crearCampeonatoParejasFemenino.actualizarParejaFinal(calendarioFaseFF, true, false);
					}
				}else  if(calendarioFaseFF.getGrupoProcedenciaPareja2().equalsIgnoreCase(calendarioFaseSF.getIdCruce())){
					if(calendarioFaseSF.isGanaPareja() == 1){
						calendarioFaseFF.setPareja2Id(calendarioFaseSF.getPareja1Id());
						calendarioFaseFF.setPareja2(calendarioFaseSF.getPareja1());
						crearCampeonatoParejasFemenino.actualizarParejaFinal(calendarioFaseFF, false, true);
					}else if(calendarioFaseSF.isGanaPareja() == 2){
						calendarioFaseFF.setPareja2Id(calendarioFaseSF.getPareja2Id());
						calendarioFaseFF.setPareja2(calendarioFaseSF.getPareja2());
						crearCampeonatoParejasFemenino.actualizarParejaFinal(calendarioFaseFF, false, true);
					}
				}
    		}
		}
    	
    	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Clasificados a Final parejas de las Semifinales.", null));
    }
    
    
    
    public void doActualizarResultadosEnfrentamientoDirecto(Integer fase, Object calendarioFase){
    	if(fase.equals(FasesModelo2022.FASE_OF)){
    		CalendarioFaseOF2022 calendarioFaseOF = (CalendarioFaseOF2022) calendarioFase;
    		
        	boolean error = false;
        	if(calendarioFaseOF.isModificable() && calendarioFaseOF.isParejaEnFase()){
    			
    			Fases2022 faseObject = getFase(fase);
    			
    			Integer numeroPartidasFase = faseObject.getNumeroPartidas();
    			
    			Integer numeroPartidasFaseMax = numeroPartidasFase;
    			
    			if(numeroPartidasFase > 1){
    				numeroPartidasFaseMax = numeroPartidasFase + 1;
    			}
    			
    			Integer partidasJugadas = calendarioFaseOF.getPartidasJugadas();
    			
    			Integer numeroJuegosFase = faseObject.getNumeroJuegos();
    			Integer numeroJuegosTotalFase = (numeroJuegosFase * 2) - 1;
    			Integer numeroJuegosEmpate = numeroJuegosTotalFase / 2;
    			
    			boolean resultadoPartidas1 = calendarioFaseOF.isResultadoPartidas1();
    			boolean resultadoPartidas2 = calendarioFaseOF.isResultadoPartidas2();
    			
    			boolean terceraPartidaJugada = calendarioFaseOF.isPartida3Jugada();
    			
    			Integer juegosPareja1P1 = calendarioFaseOF.getJuegosPareja1P1();
    			Integer juegosPareja1P2 = calendarioFaseOF.getJuegosPareja1P2();
    			Integer juegosPareja1P3 = calendarioFaseOF.getJuegosPareja1P3();
    			Integer juegosPareja2P1 = calendarioFaseOF.getJuegosPareja2P1();
    			Integer juegosPareja2P2 = calendarioFaseOF.getJuegosPareja2P2();
    			Integer juegosPareja2P3 = calendarioFaseOF.getJuegosPareja2P3();
    			
    			error = validacionesResultadoOFtoFF(error, numeroPartidasFase, partidasJugadas,
						numeroJuegosFase, numeroJuegosTotalFase, numeroJuegosEmpate, resultadoPartidas1, resultadoPartidas2,
						juegosPareja1P1, juegosPareja1P2, juegosPareja1P3, juegosPareja2P1,
						juegosPareja2P2, juegosPareja2P3, numeroPartidasFaseMax, terceraPartidaJugada);
    			
    			//Si todo correcto actualizar resultado
    			if(!error){
    	    		crearCampeonatoParejasFemenino.actualizarResultadosEnfrentamientoDirectoOF(calendarioFaseOF);
    	    		clasificarSiguienteRonda(FasesModelo2022.FASE_OF, null);
    	    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha actualizado el resultado de Octavos de Final.", null));
    	    		leerCampeonatoParejasFemenino = new LeerCampeonatoParejasFemenino2022();
    			}
    		}else if(!calendarioFaseOF.isParejaEnFase()){
    			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede modificar el resultado, no hay parejas clasificados para la fase.", null));
    		}
    		
    	}else if(fase.equals(FasesModelo2022.FASE_CF)){
    		CalendarioFaseCF2022 calendarioFaseCF = (CalendarioFaseCF2022) calendarioFase;
    		
        	boolean error = false;
        	if(calendarioFaseCF.isModificable() && calendarioFaseCF.isParejasEnFase()){
    			
    			Fases2022 faseObject = getFase(fase);
    			
    			Integer numeroPartidasFase = faseObject.getNumeroPartidas();
    			
    			Integer numeroPartidasFaseMax = numeroPartidasFase;
    			
    			if(numeroPartidasFase > 1){
    				numeroPartidasFaseMax = numeroPartidasFase + 1;
    			}
    			
    			Integer partidasJugadas = calendarioFaseCF.getPartidasJugadas();
    			
    			Integer numeroJuegosFase = faseObject.getNumeroJuegos();
    			Integer numeroJuegosTotalFase = (numeroJuegosFase * 2) - 1;
    			Integer numeroJuegosEmpate = numeroJuegosTotalFase / 2;
    			
    			boolean resultadoPartidas1 = calendarioFaseCF.isResultadoPartidas1();
    			boolean resultadoPartidas2 = calendarioFaseCF.isResultadoPartidas2();
    			
    			boolean terceraPartidaJugada = calendarioFaseCF.isPartida3Jugada();
    			
    			Integer juegosPareja1P1 = calendarioFaseCF.getJuegosPareja1P1();
    			Integer juegosPareja1P2 = calendarioFaseCF.getJuegosPareja1P2();
    			Integer juegosPareja1P3 = calendarioFaseCF.getJuegosPareja1P3();
    			Integer juegosPareja2P1 = calendarioFaseCF.getJuegosPareja2P1();
    			Integer juegosPareja2P2 = calendarioFaseCF.getJuegosPareja2P2();
    			Integer juegosPareja2P3 = calendarioFaseCF.getJuegosPareja2P3();
    			
    			error = validacionesResultadoOFtoFF(error, numeroPartidasFase, partidasJugadas,
						numeroJuegosFase, numeroJuegosTotalFase, numeroJuegosEmpate, resultadoPartidas1, resultadoPartidas2,
						juegosPareja1P1, juegosPareja1P2, juegosPareja1P3, juegosPareja2P1,
						juegosPareja2P2, juegosPareja2P3, numeroPartidasFaseMax, terceraPartidaJugada);
    			
    			//Si todo correcto actualizar resultado
    			if(!error){
    	    		crearCampeonatoParejasFemenino.actualizarResultadosEnfrentamientoDirectoCF(calendarioFaseCF);
    	    		clasificarSiguienteRonda(FasesModelo2022.FASE_CF, null);
    	    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha actualizado el resultado de Cuartos de Final.", null));
    	    		leerCampeonatoParejasFemenino = new LeerCampeonatoParejasFemenino2022();
    			}
    		}else if(!calendarioFaseCF.isParejasEnFase()){
    			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede modificar el resultado, no hay parejas clasificados para la fase.", null));
    		}
    		
    	}else if(fase.equals(FasesModelo2022.FASE_SF)){
    		CalendarioFaseSF2022 calendarioFaseSF = (CalendarioFaseSF2022) calendarioFase;
    		
        	boolean error = false;
        	if(calendarioFaseSF.isModificable() && calendarioFaseSF.isParejaEnFase()){
    			
    			Fases2022 faseObject = getFase(fase);
    			
    			Integer numeroPartidasFase = faseObject.getNumeroPartidas();
    			
    			Integer numeroPartidasFaseMax = numeroPartidasFase;
    			
    			if(numeroPartidasFase > 1){
    				numeroPartidasFaseMax = numeroPartidasFase + 1;
    			}
    			
    			Integer partidasJugadas = calendarioFaseSF.getPartidasJugadas();
    			
    			Integer numeroJuegosFase = faseObject.getNumeroJuegos();
    			Integer numeroJuegosTotalFase = (numeroJuegosFase * 2) - 1;
    			Integer numeroJuegosEmpate = numeroJuegosTotalFase / 2;
    			
    			boolean resultadoPartidas1 = calendarioFaseSF.isResultadoPartidas1();
    			boolean resultadoPartidas2 = calendarioFaseSF.isResultadoPartidas2();
    			
    			boolean terceraPartidaJugada = calendarioFaseSF.isPartida3Jugada();
    			
    			Integer juegosPareja1P1 = calendarioFaseSF.getJuegosPareja1P1();
    			Integer juegosPareja1P2 = calendarioFaseSF.getJuegosPareja1P2();
    			Integer juegosPareja1P3 = calendarioFaseSF.getJuegosPareja1P3();
    			Integer juegosPareja2P1 = calendarioFaseSF.getJuegosPareja2P1();
    			Integer juegosPareja2P2 = calendarioFaseSF.getJuegosPareja2P2();
    			Integer juegosPareja2P3 = calendarioFaseSF.getJuegosPareja2P3();
    			
    			error = validacionesResultadoOFtoFF(error, numeroPartidasFase, partidasJugadas,
						numeroJuegosFase, numeroJuegosTotalFase, numeroJuegosEmpate, resultadoPartidas1, resultadoPartidas2,
						juegosPareja1P1, juegosPareja1P2, juegosPareja1P3, juegosPareja2P1,
						juegosPareja2P2, juegosPareja2P3, numeroPartidasFaseMax, terceraPartidaJugada);
    			
    			//Si todo correcto actualizar resultado
    			if(!error){
    				crearCampeonatoParejasFemenino.actualizarResultadosEnfrentamientoDirectoSF(calendarioFaseSF);
    	    		clasificarSiguienteRonda(FasesModelo2022.FASE_SF, null);
    	    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha actualizado el resultado de Semifinales.", null));
    	    		leerCampeonatoParejasFemenino = new LeerCampeonatoParejasFemenino2022();
    			}
    		}else if(!calendarioFaseSF.isParejaEnFase()){
    			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede modificar el resultado, no hay parejas clasificados para la fase.", null));
    		}
    		
    	}else if(fase.equals(FasesModelo2022.FASE_FC)){
    		CalendarioFaseFC2022 calendarioFaseFC = (CalendarioFaseFC2022) calendarioFase;
    		
        	boolean error = false;
        	if(calendarioFaseFC.isModificable() && calendarioFaseFC.isParejaEnFase()){
    			
    			Fases2022 faseObject = getFase(fase);
    			
    			Integer numeroPartidasFase = faseObject.getNumeroPartidas();
    			
    			Integer numeroPartidasFaseMax = numeroPartidasFase;
    			
    			if(numeroPartidasFase > 1){
    				numeroPartidasFaseMax = numeroPartidasFase + 1;
    			}
    			
    			Integer partidasJugadas = calendarioFaseFC.getPartidasJugadas();
    			
    			Integer numeroJuegosFase = faseObject.getNumeroJuegos();
    			Integer numeroJuegosTotalFase = (numeroJuegosFase * 2) - 1;
    			Integer numeroJuegosEmpate = numeroJuegosTotalFase / 2;
    			
    			boolean resultadoPartidas1 = calendarioFaseFC.isResultadoPartidas1();
    			boolean resultadoPartidas2 = calendarioFaseFC.isResultadoPartidas2();
    			
    			boolean terceraPartidaJugada = calendarioFaseFC.isPartida3Jugada();
    			
    			Integer juegosPareja1P1 = calendarioFaseFC.getJuegosPareja1P1();
    			Integer juegosPareja1P2 = calendarioFaseFC.getJuegosPareja1P2();
    			Integer juegosPareja1P3 = calendarioFaseFC.getJuegosPareja1P3();
    			Integer juegosPareja2P1 = calendarioFaseFC.getJuegosPareja2P1();
    			Integer juegosPareja2P2 = calendarioFaseFC.getJuegosPareja2P2();
    			Integer juegosPareja2P3 = calendarioFaseFC.getJuegosPareja2P3();
    			
    			error = validacionesResultadoOFtoFF(error, numeroPartidasFase, partidasJugadas,
						numeroJuegosFase, numeroJuegosTotalFase, numeroJuegosEmpate, resultadoPartidas1, resultadoPartidas2,
						juegosPareja1P1, juegosPareja1P2, juegosPareja1P3, juegosPareja2P1,
						juegosPareja2P2, juegosPareja2P3, numeroPartidasFaseMax, terceraPartidaJugada);
    			
    			//Si todo correcto actualizar resultado
    			if(!error){
    				crearCampeonatoParejasFemenino.actualizarResultadosEnfrentamientoDirectoFC(calendarioFaseFC);
    	    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha actualizado el resultado de Semifinales.", null));
    	    		leerCampeonatoParejasFemenino = new LeerCampeonatoParejasFemenino2022();
    			}
    		}else if(!calendarioFaseFC.isParejaEnFase()){
    			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede modificar el resultado, no hay parejas clasificados para la fase.", null));
    		}
    		
    		
    	}else if(fase.equals(FasesModelo2022.FASE_FF)){
    		CalendarioFaseFF2022 calendarioFaseFF = (CalendarioFaseFF2022) calendarioFase;
    		
        	boolean error = false;
        	if(calendarioFaseFF.isModificable() && calendarioFaseFF.isParejaEnFase()){
    			
    			Fases2022 faseObject = getFase(fase);
    			
    			Integer numeroPartidasFase = faseObject.getNumeroPartidas();
    			
    			Integer numeroPartidasFaseMax = numeroPartidasFase;
    			
    			if(numeroPartidasFase > 1){
    				numeroPartidasFaseMax = numeroPartidasFase + 1;
    			}
    			
    			Integer partidasJugadas = calendarioFaseFF.getPartidasJugadas();
    			
    			Integer numeroJuegosFase = faseObject.getNumeroJuegos();
    			Integer numeroJuegosTotalFase = (numeroJuegosFase * 2) - 1;
    			Integer numeroJuegosEmpate = numeroJuegosTotalFase / 2;
    			
    			boolean resultadoPartidas1 = calendarioFaseFF.isResultadoPartidas1();
    			boolean resultadoPartidas2 = calendarioFaseFF.isResultadoPartidas2();
    			
    			boolean terceraPartidaJugada = calendarioFaseFF.isPartida3Jugada();
    			
    			Integer juegosPareja1P1 = calendarioFaseFF.getJuegosPareja1P1();
    			Integer juegosPareja1P2 = calendarioFaseFF.getJuegosPareja1P2();
    			Integer juegosPareja1P3 = calendarioFaseFF.getJuegosPareja1P3();
    			Integer juegosPareja2P1 = calendarioFaseFF.getJuegosPareja2P1();
    			Integer juegosPareja2P2 = calendarioFaseFF.getJuegosPareja2P2();
    			Integer juegosPareja2P3 = calendarioFaseFF.getJuegosPareja2P3();
    			
    			error = validacionesResultadoOFtoFF(error, numeroPartidasFase, partidasJugadas,
						numeroJuegosFase, numeroJuegosTotalFase, numeroJuegosEmpate, resultadoPartidas1, resultadoPartidas2,
						juegosPareja1P1, juegosPareja1P2, juegosPareja1P3, juegosPareja2P1,
						juegosPareja2P2, juegosPareja2P3, numeroPartidasFaseMax, terceraPartidaJugada);
    			
    			//Si todo correcto actualizar resultado
    			if(!error){
    	    		crearCampeonatoParejasFemenino.actualizarResultadosEnfrentamientoDirectoFF(calendarioFaseFF);
    	    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha actualizado el resultado de Final.", null));
    	    		leerCampeonatoParejasFemenino = new LeerCampeonatoParejasFemenino2022();
    			}
    		}else if(!calendarioFaseFF.isParejaEnFase()){
    			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede modificar el resultado, no hay parejas clasificados para la fase.", null));
    		}

    	}

	}

	private boolean validacionesResultadoOFtoFF(boolean error,
			Integer numeroPartidasFase, Integer partidasJugadas, Integer numeroJuegosFase,
			Integer numeroJuegosTotalFase, Integer numeroJuegosEmpate, 
			boolean resultadoPartidas1, boolean resultadoPartidas2, 
			Integer juegosPareja1P1, Integer juegosPareja1P2, Integer juegosPareja1P3, 
			Integer juegosPareja2P1, Integer juegosPareja2P2, Integer juegosPareja2P3,
			Integer numeroPartidasFaseMax, boolean terceraPartidaJugada) {
		//Validaciones
		if(partidasJugadas < numeroPartidasFase || (partidasJugadas > numeroPartidasFase && partidasJugadas < numeroPartidasFaseMax)){
			error = true;
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El numero de partidas a jugar es " + numeroPartidasFase + " (ganadas).", null));
		}else if(numeroPartidasFase == 1){
			if(!resultadoPartidas1){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ha de indicar el resultado de la partida 1 o el resultado es incorrecto.", null));
			}else if(juegosPareja1P1 > numeroJuegosFase || juegosPareja2P1 > numeroJuegosFase){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El maximo de juegos de la partida 1 es " + numeroJuegosFase + " por pareja/partida.", null));
			}else if((juegosPareja1P1 + juegosPareja2P1) > numeroJuegosTotalFase){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El maximo de juegos de la partida 1 para el enfrentamiento es " + numeroJuegosTotalFase + ".", null));
			}else if((juegosPareja1P1 + juegosPareja2P1) < numeroJuegosFase){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El minimo de juegos de la partida 1 es para el enfrentamiento es " + numeroJuegosFase + ".", null));
			}else if(juegosPareja1P1 < 0 || juegosPareja2P1 < 0){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No puede haber menos de 0 juegos en la partida 1", null));
			}else if((juegosPareja1P1 < numeroJuegosEmpate && juegosPareja2P1 < numeroJuegosEmpate) 
					|| (juegosPareja1P1 < numeroJuegosEmpate && juegosPareja2P1 < numeroJuegosFase)
					|| (juegosPareja1P1 < numeroJuegosFase && juegosPareja2P1 < numeroJuegosEmpate)){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Resultado incorrecto en partida 1.", null));
			}else if((juegosPareja1P2 > 0 && juegosPareja2P2 > 0) 
					|| (juegosPareja1P3 > 0 && juegosPareja2P3 > 0)){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se pueden indicar valores en las partidas 2 y 3.", null));
			}
		}else if(numeroPartidasFase == 2){
			
			if(!resultadoPartidas2){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ha de indicar el resultado de la partida 1, 2 y 3 o el resultado es incorrecto. (2 partidas ganadas).", null));
			}else if(juegosPareja1P1 > numeroJuegosFase || juegosPareja2P1 > numeroJuegosFase){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El maximo de juegos de la partida 1 es " + numeroJuegosFase + " por pareja/partida.", null));
			}else if((juegosPareja1P1 + juegosPareja2P1) > numeroJuegosTotalFase){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El maximo de juegos de la partida 1 para el enfrentamiento es " + numeroJuegosTotalFase + ".", null));
			}else if((juegosPareja1P1 + juegosPareja2P1) < numeroJuegosFase){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El minimo de juegos de la partida 1 es para el enfrentamiento es " + numeroJuegosFase + ".", null));
			}else if(juegosPareja1P1 < 0 || juegosPareja2P1 < 0){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No puede haber menos de 0 juegos en la partida 1", null));
			}else if((juegosPareja1P1 < numeroJuegosEmpate && juegosPareja2P1 < numeroJuegosEmpate) 
					|| (juegosPareja1P1 < numeroJuegosEmpate && juegosPareja2P1 < numeroJuegosFase)
					|| (juegosPareja1P1 < numeroJuegosFase && juegosPareja2P1 < numeroJuegosEmpate)){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Resultado incorrecto en partida 1.", null));
			}else if(juegosPareja1P2 > numeroJuegosFase || juegosPareja2P2 > numeroJuegosFase){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El maximo de juegos de la partida 2 es " + numeroJuegosFase + " por pareja/partida.", null));
			}else if((juegosPareja1P2 + juegosPareja2P2) > numeroJuegosTotalFase){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El maximo de juegos de la partida 2 para el enfrentamiento es " + numeroJuegosTotalFase + ".", null));
			}else if((juegosPareja1P2 + juegosPareja2P2) < numeroJuegosFase){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El minimo de juegos de la partida 2 es para el enfrentamiento es " + numeroJuegosFase + ".", null));
			}else if(juegosPareja1P2 < 0 || juegosPareja2P2 < 0){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No puede haber menos de 0 juegos en la partida 2", null));
			}else if((juegosPareja1P2 < numeroJuegosEmpate && juegosPareja2P2 < numeroJuegosEmpate) 
					|| (juegosPareja1P2 < numeroJuegosEmpate && juegosPareja2P2 < numeroJuegosFase)
					|| (juegosPareja1P2 < numeroJuegosFase && juegosPareja2P2 < numeroJuegosEmpate)){
				error = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Resultado incorrecto en partida 2.", null));
			}
			if(terceraPartidaJugada){
				if(juegosPareja1P3 > numeroJuegosFase || juegosPareja2P3 > numeroJuegosFase){
					error = true;
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El maximo de juegos de la partida 3 es " + numeroJuegosFase + " por pareja/partida.", null));
				}else if((juegosPareja1P3 + juegosPareja2P3) > numeroJuegosTotalFase){
					error = true;
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El maximo de juegos de la partida 3 para el enfrentamiento es " + numeroJuegosTotalFase + ".", null));
				}else if((juegosPareja1P3 + juegosPareja2P3) < numeroJuegosFase){
					error = true;
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El minimo de juegos de la partida 3 es para el enfrentamiento es " + numeroJuegosFase + ".", null));
				}else if(juegosPareja1P3 < 0 || juegosPareja2P3 < 0){
					error = true;
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No puede haber menos de 0 juegos en la partida 3", null));
				}else if((juegosPareja1P3 < numeroJuegosEmpate && juegosPareja2P3 < numeroJuegosEmpate) 
						|| (juegosPareja1P3 < numeroJuegosEmpate && juegosPareja2P3 < numeroJuegosFase)
						|| (juegosPareja1P3 < numeroJuegosFase && juegosPareja2P3 < numeroJuegosEmpate)){
					error = true;
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Resultado incorrecto en partida 3.", null));
				}
			}else{
				if(juegosPareja1P3 > 0 && juegosPareja2P3 > 0){
					error = true;
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se pueden indicar valores en la partida 3.", null));
				}
			}
			
		}
		return error;
	}
    
    public boolean isActivoClasificarCuartosForGrupos(){
    	List<CalendarioFaseCF2022> calendarioFaseCFList = getResultListCalendarioFaseCF();
    	for (CalendarioFaseCF2022 calendarioFaseCF : calendarioFaseCFList) {
			if(calendarioFaseCF.getFaseAnterior().equals(FasesModelo2022.FASE_I)){
				return true;
			}
		}
    	return false;
    }
    
    public boolean isActivoClasificarCuartosForOctavos(){
    	List<CalendarioFaseCF2022> calendarioFaseCFList = getResultListCalendarioFaseCF();
    	for (CalendarioFaseCF2022 calendarioFaseCF : calendarioFaseCFList) {
			if(calendarioFaseCF.getFaseAnterior().equals(FasesModelo2022.FASE_OF)){
				return true;
			}
		}
    	return false;
    }
    
    public boolean isActivoClasificarSemifinalesForGrupos(){
    	List<CalendarioFaseSF2022> calendarioFaseSFList = getResultListCalendarioFaseSF();
    	for (CalendarioFaseSF2022 calendarioFaseSF : calendarioFaseSFList) {
			if(calendarioFaseSF.getFaseAnterior().equals(FasesModelo2022.FASE_I)){
				return true;
			}
		}
    	return false;
    }
    
    public boolean isActivoClasificarSemifinalesForCuartos(){
    	List<CalendarioFaseSF2022> calendarioFaseSFList = getResultListCalendarioFaseSF();
    	for (CalendarioFaseSF2022 calendarioFaseSF : calendarioFaseSFList) {
			if(calendarioFaseSF.getFaseAnterior().equals(FasesModelo2022.FASE_CF)){
				return true;
			}
		}
    	return false;
    }
    
    public String getExtractParejaNode(Object node){
    	String array[] = ((String) node).split("#");
    	return array[0].toUpperCase();
    }
    
    public String getExtractEquipoNode(Object node){
    	String array[] = ((String) node).split("#");
    	if(array.length > 1){
    		return array[1];
    	}
    	return "";
    }
    
    public String getExtractCampeonNode(Object node){
    	String array[] = ((String) node).split("#");
    	if(array.length > 1){
    		return array[2];
    	}
    	return "";
    }
    
    public boolean isSorteoFinalizado(){
    	boolean sorteoFinalizado = leerCampeonatoParejasFemenino.isSorteoFinalizado();
    	boolean isAdmin = false;
    	if(sessionState != null && sessionState.getIdentityInfo() != null && sessionState.getIdentityInfo().getUsuario() != null){
    		isAdmin = sessionState.getIdentityInfo().getUsuario().getRolId() == 1;
    	}
    	return sorteoFinalizado || isAdmin;
	}
	

}
