package com.bolocelta.facade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.inject.Named;

import com.bolocelta.application.session.SessionState;
import com.bolocelta.bbdd.readTables.LeerHistoricoCampeonatosEspana2022;
import com.bolocelta.entities.HistoricoCampeonatosEspana2022;

@Named
@ConversationScoped
@ManagedBean
public class HistoriaCampeonatoEspana2022 implements Serializable {
	
	@Inject
	private SessionState sessionState;
	
	private static final long serialVersionUID = 1L;
	
	private LeerHistoricoCampeonatosEspana2022 leerHistoricoCampeonatosEspana = new LeerHistoricoCampeonatosEspana2022();
	
	private List<HistoricoCampeonatosEspana2022> resultListHistoricoCampeonatoEspana = null;
	private List<HistoricoCampeonatosEspana2022> resultListHistoricoCampeonatoEspanaEquipos = null;
	private List<HistoricoCampeonatosEspana2022> resultListHistoricoCampeonatoEspanaIndividual = null;
	private List<HistoricoCampeonatosEspana2022> resultListHistoricoCampeonatoEspanaParejas = null;
	private List<HistoricoCampeonatosEspana2022> resultListHistoricoCampeonatoEspanaIndividualFemenino = null;
	private List<HistoricoCampeonatosEspana2022> resultListHistoricoCampeonatoEspanaParejasFemenino = null;
	private List<HistoricoCampeonatosEspana2022> resultListHistoricoCampeonatoEspanaMixto = null;
	private List<HistoricoCampeonatosEspana2022> resultListHistoricoCampeonatoEspanaVeteranos = null;
	private List<HistoricoCampeonatosEspana2022> resultListHistoricoCampeonatoEspanaJuveniles = null;
	private List<HistoricoCampeonatosEspana2022> resultListHistoricoCampeonatoEspanaCadetes = null;
	private List<HistoricoCampeonatosEspana2022> resultListHistoricoCampeonatoEspanaParejasJuveniles = null;
	private List<HistoricoCampeonatosEspana2022> resultListHistoricoCampeonatoEspanaParejasCadetes = null;

	
	private List<Integer> resultListAnyoFasesTabShow = null;
	
	public List<HistoricoCampeonatosEspana2022> getResultListHistoricoCampeonatoEspanaAll() {
		if(resultListHistoricoCampeonatoEspana == null){
			resultListHistoricoCampeonatoEspana = (List<HistoricoCampeonatosEspana2022>) leerHistoricoCampeonatosEspana.listResult();
		}
		return resultListHistoricoCampeonatoEspana;
	}
	
	public List<HistoricoCampeonatosEspana2022> getResultListHistoricoCampeonatoEspanaEquipos() {
		if(resultListHistoricoCampeonatoEspanaEquipos == null){
			resultListHistoricoCampeonatoEspanaEquipos = (List<HistoricoCampeonatosEspana2022>) leerHistoricoCampeonatosEspana.listResultEquipos();
		}
		return resultListHistoricoCampeonatoEspanaEquipos;
	}

	public List<HistoricoCampeonatosEspana2022> getResultListHistoricoCampeonatoEspanaIndividual() {
		if(resultListHistoricoCampeonatoEspanaIndividual == null){
			resultListHistoricoCampeonatoEspanaIndividual = (List<HistoricoCampeonatosEspana2022>) leerHistoricoCampeonatosEspana.listResultIndividual();
		}
		return resultListHistoricoCampeonatoEspanaIndividual;
	}
	
	public List<HistoricoCampeonatosEspana2022> getResultListHistoricoCampeonatoEspanaParejas() {
		if(resultListHistoricoCampeonatoEspanaParejas == null){
			resultListHistoricoCampeonatoEspanaParejas = (List<HistoricoCampeonatosEspana2022>) leerHistoricoCampeonatosEspana.listResultParejas();
		}
		return resultListHistoricoCampeonatoEspanaParejas;
	}
	
	public List<HistoricoCampeonatosEspana2022> getResultListHistoricoCampeonatoEspanaIndividualFemenino() {
		if(resultListHistoricoCampeonatoEspanaIndividualFemenino == null){
			resultListHistoricoCampeonatoEspanaIndividualFemenino = (List<HistoricoCampeonatosEspana2022>) leerHistoricoCampeonatosEspana.listResultIndividualFemenino();
		}
		return resultListHistoricoCampeonatoEspanaIndividualFemenino;
	}
	
	public List<HistoricoCampeonatosEspana2022> getResultListHistoricoCampeonatoEspanaParejasFemenino() {
		if(resultListHistoricoCampeonatoEspanaParejasFemenino == null){
			resultListHistoricoCampeonatoEspanaParejasFemenino = (List<HistoricoCampeonatosEspana2022>) leerHistoricoCampeonatosEspana.listResultParejasFemenino();
		}
		return resultListHistoricoCampeonatoEspanaParejasFemenino;
	}
	
	public List<HistoricoCampeonatosEspana2022> getResultListHistoricoCampeonatoEspanaMixto() {
		if(resultListHistoricoCampeonatoEspanaMixto == null){
			resultListHistoricoCampeonatoEspanaMixto = (List<HistoricoCampeonatosEspana2022>) leerHistoricoCampeonatosEspana.listResultMixto();
		}
		return resultListHistoricoCampeonatoEspanaMixto;
	}
	
	public List<HistoricoCampeonatosEspana2022> getResultListHistoricoCampeonatoEspanaVeteranos() {
		if(resultListHistoricoCampeonatoEspanaVeteranos == null){
			resultListHistoricoCampeonatoEspanaVeteranos = (List<HistoricoCampeonatosEspana2022>) leerHistoricoCampeonatosEspana.listResultVeteranos();
		}
		return resultListHistoricoCampeonatoEspanaVeteranos;
	}
	
	public List<HistoricoCampeonatosEspana2022> getResultListHistoricoCampeonatoEspanaJuveniles() {
		if(resultListHistoricoCampeonatoEspanaJuveniles == null){
			resultListHistoricoCampeonatoEspanaJuveniles = (List<HistoricoCampeonatosEspana2022>) leerHistoricoCampeonatosEspana.listResultJuveniles();
		}
		return resultListHistoricoCampeonatoEspanaJuveniles;
	}
	
	public List<HistoricoCampeonatosEspana2022> getResultListHistoricoCampeonatoEspanaCadetes() {
		if(resultListHistoricoCampeonatoEspanaCadetes == null){
			resultListHistoricoCampeonatoEspanaCadetes = (List<HistoricoCampeonatosEspana2022>) leerHistoricoCampeonatosEspana.listResultCadetes();
		}
		return resultListHistoricoCampeonatoEspanaCadetes;
	}
	
	public List<HistoricoCampeonatosEspana2022> getResultListHistoricoCampeonatoEspanaParejasJuveniles() {
		if(resultListHistoricoCampeonatoEspanaParejasJuveniles == null){
			resultListHistoricoCampeonatoEspanaParejasJuveniles = (List<HistoricoCampeonatosEspana2022>) leerHistoricoCampeonatosEspana.listResultParejasJuveniles();
		}
		return resultListHistoricoCampeonatoEspanaParejasJuveniles;
	}
	
	public List<HistoricoCampeonatosEspana2022> getResultListHistoricoCampeonatoEspanaParejasCadetes() {
		if(resultListHistoricoCampeonatoEspanaParejasCadetes == null){
			resultListHistoricoCampeonatoEspanaParejasCadetes = (List<HistoricoCampeonatosEspana2022>) leerHistoricoCampeonatosEspana.listResultParejasCadetes();
		}
		return resultListHistoricoCampeonatoEspanaParejasCadetes;
	}
	
	
	public List<HistoricoCampeonatosEspana2022> getResultListHistoricoCampeonatoEspanaEquiposByAnyo(Integer anyo) {
		List<HistoricoCampeonatosEspana2022> result = new ArrayList<HistoricoCampeonatosEspana2022>();
		for (HistoricoCampeonatosEspana2022 historicoCampeonatosEspana : getResultListHistoricoCampeonatoEspanaEquipos()) {
			if(historicoCampeonatosEspana.getAnyo().equals(anyo)){
				result.add(historicoCampeonatosEspana);
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(result, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				HistoricoCampeonatosEspana2022 cec1 = (HistoricoCampeonatosEspana2022) o1;
				HistoricoCampeonatosEspana2022 cec2 = (HistoricoCampeonatosEspana2022) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPosicion().compareTo(cec2.getPosicion());
					return rpuntos;
			}
		});
		
		return result;
	}
	
	public List<HistoricoCampeonatosEspana2022> getResultListHistoricoCampeonatoEspanaIndividualByAnyo(Integer anyo) {
		List<HistoricoCampeonatosEspana2022> result = new ArrayList<HistoricoCampeonatosEspana2022>();
		for (HistoricoCampeonatosEspana2022 historicoCampeonatosEspana : getResultListHistoricoCampeonatoEspanaIndividual()) {
			if(historicoCampeonatosEspana.getAnyo().equals(anyo)){
				result.add(historicoCampeonatosEspana);
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(result, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				HistoricoCampeonatosEspana2022 cec1 = (HistoricoCampeonatosEspana2022) o1;
				HistoricoCampeonatosEspana2022 cec2 = (HistoricoCampeonatosEspana2022) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPosicion().compareTo(cec2.getPosicion());
					return rpuntos;
			}
		});
		
		return result;
	}
	
	public List<HistoricoCampeonatosEspana2022> getResultListHistoricoCampeonatoEspanaParejasByAnyo(Integer anyo) {
		List<HistoricoCampeonatosEspana2022> result = new ArrayList<HistoricoCampeonatosEspana2022>();
		for (HistoricoCampeonatosEspana2022 historicoCampeonatosEspana : getResultListHistoricoCampeonatoEspanaParejas()) {
			if(historicoCampeonatosEspana.getAnyo().equals(anyo)){
				result.add(historicoCampeonatosEspana);
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(result, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				HistoricoCampeonatosEspana2022 cec1 = (HistoricoCampeonatosEspana2022) o1;
				HistoricoCampeonatosEspana2022 cec2 = (HistoricoCampeonatosEspana2022) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPosicion().compareTo(cec2.getPosicion());
					return rpuntos;
			}
		});
		
		return result;
	}
	

	public List<HistoricoCampeonatosEspana2022> getResultListHistoricoCampeonatoEspanaIndividualFemeninoByAnyo(Integer anyo) {
		List<HistoricoCampeonatosEspana2022> result = new ArrayList<HistoricoCampeonatosEspana2022>();
		for (HistoricoCampeonatosEspana2022 historicoCampeonatosEspana : getResultListHistoricoCampeonatoEspanaIndividualFemenino()) {
			if(historicoCampeonatosEspana.getAnyo().equals(anyo)){
				result.add(historicoCampeonatosEspana);
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(result, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				HistoricoCampeonatosEspana2022 cec1 = (HistoricoCampeonatosEspana2022) o1;
				HistoricoCampeonatosEspana2022 cec2 = (HistoricoCampeonatosEspana2022) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPosicion().compareTo(cec2.getPosicion());
					return rpuntos;
			}
		});
		
		return result;
	}
	
	public List<HistoricoCampeonatosEspana2022> getResultListHistoricoCampeonatoEspanaParejasFemeninoByAnyo(Integer anyo) {
		List<HistoricoCampeonatosEspana2022> result = new ArrayList<HistoricoCampeonatosEspana2022>();
		for (HistoricoCampeonatosEspana2022 historicoCampeonatosEspana : getResultListHistoricoCampeonatoEspanaParejasFemenino()) {
			if(historicoCampeonatosEspana.getAnyo().equals(anyo)){
				result.add(historicoCampeonatosEspana);
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(result, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				HistoricoCampeonatosEspana2022 cec1 = (HistoricoCampeonatosEspana2022) o1;
				HistoricoCampeonatosEspana2022 cec2 = (HistoricoCampeonatosEspana2022) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPosicion().compareTo(cec2.getPosicion());
					return rpuntos;
			}
		});
		
		return result;
	}
	
	public List<HistoricoCampeonatosEspana2022> getResultListHistoricoCampeonatoEspanaMixtoByAnyo(Integer anyo) {
		List<HistoricoCampeonatosEspana2022> result = new ArrayList<HistoricoCampeonatosEspana2022>();
		for (HistoricoCampeonatosEspana2022 historicoCampeonatosEspana : getResultListHistoricoCampeonatoEspanaMixto()) {
			if(historicoCampeonatosEspana.getAnyo().equals(anyo)){
				result.add(historicoCampeonatosEspana);
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(result, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				HistoricoCampeonatosEspana2022 cec1 = (HistoricoCampeonatosEspana2022) o1;
				HistoricoCampeonatosEspana2022 cec2 = (HistoricoCampeonatosEspana2022) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPosicion().compareTo(cec2.getPosicion());
					return rpuntos;
			}
		});
		
		return result;
	}
	
	public List<HistoricoCampeonatosEspana2022> getResultListHistoricoCampeonatoEspanaVeteranosByAnyo(Integer anyo) {
		List<HistoricoCampeonatosEspana2022> result = new ArrayList<HistoricoCampeonatosEspana2022>();
		for (HistoricoCampeonatosEspana2022 historicoCampeonatosEspana : getResultListHistoricoCampeonatoEspanaVeteranos()) {
			if(historicoCampeonatosEspana.getAnyo().equals(anyo)){
				result.add(historicoCampeonatosEspana);
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(result, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				HistoricoCampeonatosEspana2022 cec1 = (HistoricoCampeonatosEspana2022) o1;
				HistoricoCampeonatosEspana2022 cec2 = (HistoricoCampeonatosEspana2022) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPosicion().compareTo(cec2.getPosicion());
					return rpuntos;
			}
		});
		
		return result;
	}
	
	public List<HistoricoCampeonatosEspana2022> getResultListHistoricoCampeonatoEspanaJuvenilesByAnyo(Integer anyo) {
		List<HistoricoCampeonatosEspana2022> result = new ArrayList<HistoricoCampeonatosEspana2022>();
		for (HistoricoCampeonatosEspana2022 historicoCampeonatosEspana : getResultListHistoricoCampeonatoEspanaJuveniles()) {
			if(historicoCampeonatosEspana.getAnyo().equals(anyo)){
				result.add(historicoCampeonatosEspana);
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(result, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				HistoricoCampeonatosEspana2022 cec1 = (HistoricoCampeonatosEspana2022) o1;
				HistoricoCampeonatosEspana2022 cec2 = (HistoricoCampeonatosEspana2022) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPosicion().compareTo(cec2.getPosicion());
					return rpuntos;
			}
		});
		
		return result;
	}
	
	public List<HistoricoCampeonatosEspana2022> getResultListHistoricoCampeonatoEspanaParejasJuvenilesByAnyo(Integer anyo) {
		List<HistoricoCampeonatosEspana2022> result = new ArrayList<HistoricoCampeonatosEspana2022>();
		for (HistoricoCampeonatosEspana2022 historicoCampeonatosEspana : getResultListHistoricoCampeonatoEspanaParejasJuveniles()) {
			if(historicoCampeonatosEspana.getAnyo().equals(anyo)){
				result.add(historicoCampeonatosEspana);
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(result, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				HistoricoCampeonatosEspana2022 cec1 = (HistoricoCampeonatosEspana2022) o1;
				HistoricoCampeonatosEspana2022 cec2 = (HistoricoCampeonatosEspana2022) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPosicion().compareTo(cec2.getPosicion());
					return rpuntos;
			}
		});
		
		return result;
	}
	
	public List<HistoricoCampeonatosEspana2022> getResultListHistoricoCampeonatoEspanaCadetesByAnyo(Integer anyo) {
		List<HistoricoCampeonatosEspana2022> result = new ArrayList<HistoricoCampeonatosEspana2022>();
		for (HistoricoCampeonatosEspana2022 historicoCampeonatosEspana : getResultListHistoricoCampeonatoEspanaCadetes()) {
			if(historicoCampeonatosEspana.getAnyo().equals(anyo)){
				result.add(historicoCampeonatosEspana);
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(result, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				HistoricoCampeonatosEspana2022 cec1 = (HistoricoCampeonatosEspana2022) o1;
				HistoricoCampeonatosEspana2022 cec2 = (HistoricoCampeonatosEspana2022) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPosicion().compareTo(cec2.getPosicion());
					return rpuntos;
			}
		});
		
		return result;
	}
	
	public List<HistoricoCampeonatosEspana2022> getResultListHistoricoCampeonatoEspanaParejasCadetesByAnyo(Integer anyo) {
		List<HistoricoCampeonatosEspana2022> result = new ArrayList<HistoricoCampeonatosEspana2022>();
		for (HistoricoCampeonatosEspana2022 historicoCampeonatosEspana : getResultListHistoricoCampeonatoEspanaParejasCadetes()) {
			if(historicoCampeonatosEspana.getAnyo().equals(anyo)){
				result.add(historicoCampeonatosEspana);
			}
		}
		
		
		//Ordenar la clasificacion
		Collections.sort(result, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				HistoricoCampeonatosEspana2022 cec1 = (HistoricoCampeonatosEspana2022) o1;
				HistoricoCampeonatosEspana2022 cec2 = (HistoricoCampeonatosEspana2022) o2;
				//1. Ordenar clasificacion por puntos 
				int rpuntos = cec1.getPosicion().compareTo(cec2.getPosicion());
					return rpuntos;
			}
		});
		
		return result;
	}
	

	
	public boolean isDataCampeonato() {
		if(getResultListAnyoTabShow().size() > 0){
			return true;
		}
		return false;
	}
	
	public List<Integer> getResultListAnyoTabShow() {
		if(resultListAnyoFasesTabShow == null){
			resultListAnyoFasesTabShow = new ArrayList<>();
			
			for (HistoricoCampeonatosEspana2022 historicoCampeonatosEspana : getResultListHistoricoCampeonatoEspanaAll()) {
				if(!resultListAnyoFasesTabShow.contains(historicoCampeonatosEspana.getAnyo())){
					resultListAnyoFasesTabShow.add(historicoCampeonatosEspana.getAnyo());
				}
			}
			
		}
		
		//Ordenar la clasificacion
		Collections.sort(resultListAnyoFasesTabShow, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				Integer cec1 = (Integer) o1;
				Integer cec2 = (Integer) o2;
				//1. Ordenar por anyo descendente 
				int rpuntos = cec1.compareTo(cec2);
					return rpuntos;
			}
		});
		
		Collections.reverse(resultListAnyoFasesTabShow);
		
		return resultListAnyoFasesTabShow;
	}

	public Object selected() {
		// TODO Auto-generated method stub
		return null;
	}

	public Object read(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	public String insert(Object insert) {
		// TODO Auto-generated method stub
		return null;
	}

	public String update(Object update) {
		// TODO Auto-generated method stub
		return null;
	}

	public String delete(Object delete) {
		// TODO Auto-generated method stub
		return null;
	}
		

}
