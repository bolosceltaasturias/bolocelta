package com.bolocelta.facade;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.inject.Named;

@Named
@ManagedBean
public class Tutorial2022 implements Serializable {
	
	
	private static final long serialVersionUID = 1L;

	private String urlTutorial;
	private String urlTutorialTitulo;
	
	public Tutorial2022() {
		this.urlTutorial = getTutorialCompleto();
		this.urlTutorialTitulo = getTutorialCompletoTitulo();
	}
	
	public String getUrlTutorial() {
		return urlTutorial;
	}

	public void setUrlTutorial(String urlTutorial) {
		this.urlTutorial = urlTutorial;
	}

	public String getUrlTutorialTitulo() {
		return urlTutorialTitulo;
	}

	public void setUrlTutorialTitulo(String urlTutorialTitulo) {
		this.urlTutorialTitulo = urlTutorialTitulo;
	}

	public String getTutorialCompleto(){
		return "https://www.youtube.com/embed/8UNXzY5Qi3s?start=0;autoplay=1;controls=0;showinfo=0;loop=1";
	}
	
	public String getTutorialCompletoTitulo(){
		return "DESCUBRIENDO EL BOLO CELTA";
	}

}
