package com.bolocelta.facade;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.inject.Named;

import com.bolocelta.application.Redirect;
import com.bolocelta.bbdd.readTables.LeerBoleras2022;
import com.bolocelta.entities.Boleras2022;

@Named
@RequestScoped
@ManagedBean
public class BolerasFacade2022 implements Serializable {
	
	@Inject
	private Redirect redirect;
	
	private static final long serialVersionUID = 1L;
	
	private LeerBoleras2022 leerBoleras = new LeerBoleras2022();
	
	private List<Boleras2022> resultList = null;

	public List<Boleras2022> getResultList() {
		if(resultList == null){
			resultList = (List<Boleras2022>) leerBoleras.listResult();
		}
		return resultList;
	}

	public Object selected() {
		// TODO Auto-generated method stub
		return null;
	}

	public Object read(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	public String insert(Object insert) {
		// TODO Auto-generated method stub
		return null;
	}

	public String update(Object update) {
		// TODO Auto-generated method stub
		return null;
	}

	public String delete(Object delete) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void doAcces(){
		redirect.getBoleras();
	}
	
	public Integer getMaxRowsPaginator(){
		return 10;
	}
	
	public boolean isEnabledPaginator(){
		return getResultList() != null ? (getResultList().size() > getMaxRowsPaginator()) : false;
	}
	
	
	
	

}
