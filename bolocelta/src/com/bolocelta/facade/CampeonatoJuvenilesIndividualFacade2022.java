package com.bolocelta.facade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.bolocelta.application.session.SessionState;
import com.bolocelta.bbdd.constants.Activo2022;
import com.bolocelta.bbdd.constants.FasesModelo2022;
import com.bolocelta.bbdd.constants.FasesTabShow2022;
import com.bolocelta.bbdd.createTable.CrearCampeonatoIndividualJuveniles2022;
import com.bolocelta.bbdd.readTables.LeerCampeonatoJuvenilesIndividual2022;
import com.bolocelta.entities.CampeonatoJuvenilesIndividualClasificacion2022;
import com.bolocelta.entities.Jugadores2022;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseCF2022;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseFF2022;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseSF2022;

@Named
@ConversationScoped
@ManagedBean
public class CampeonatoJuvenilesIndividualFacade2022 implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private SessionState sessionState;
	
	private LeerCampeonatoJuvenilesIndividual2022 leerCampeonato = new LeerCampeonatoJuvenilesIndividual2022();
	
	private CrearCampeonatoIndividualJuveniles2022 crearCampeonatoIndividualJuveniles = new CrearCampeonatoIndividualJuveniles2022();
	
	private List<CampeonatoJuvenilesIndividualClasificacion2022> resultListClasificacion = null;
	private List<CalendarioFaseCF2022> resultListCalendarioFaseCF = null;
	private List<CalendarioFaseSF2022> resultListCalendarioFaseSF = null;
	private List<CalendarioFaseFF2022> resultListCalendarioFaseFF = null;
	
	private String observacionesCampeonato1;
	private String observacionesCampeonato2;
	private String observacionesCampeonato3;
	private String observacionesCampeonato4;
	private String observacionesCampeonato5;
	private String observacionesCampeonato6;
	private String observacionesCampeonato7;

	public List<CampeonatoJuvenilesIndividualClasificacion2022> getResultListClasificacion() {
		if(resultListClasificacion == null){
			resultListClasificacion = (List<CampeonatoJuvenilesIndividualClasificacion2022>) leerCampeonato.listResultClasificacion();
		}
		return resultListClasificacion;
	}
	
	public List<CalendarioFaseCF2022> getResultListCalendarioFaseCF() {
		if(resultListCalendarioFaseCF == null){
			resultListCalendarioFaseCF = (List<CalendarioFaseCF2022>) leerCampeonato.listResultCalendarioFaseCF();
		}
		return resultListCalendarioFaseCF;
	}
	
	public List<CalendarioFaseSF2022> getResultListCalendarioFaseSF() {
		if(resultListCalendarioFaseSF == null){
			resultListCalendarioFaseSF = (List<CalendarioFaseSF2022>) leerCampeonato.listResultCalendarioFaseSF();
		}
		return resultListCalendarioFaseSF;
	}
	
	public List<CalendarioFaseFF2022> getResultListCalendarioFaseFF() {
		if(resultListCalendarioFaseFF == null){
			resultListCalendarioFaseFF = (List<CalendarioFaseFF2022>) leerCampeonato.listResultCalendarioFaseFF();
		}
		return resultListCalendarioFaseFF;
	}

	public Object selected() {
		// TODO Auto-generated method stub
		return null;
	}

	public Object read(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	public String insert(Object insert) {
		// TODO Auto-generated method stub
		return null;
	}

	public String update(Object update) {
		// TODO Auto-generated method stub
		return null;
	}

	public String delete(Object delete) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public List<CalendarioFaseSF2022> getResultListCalendarioByCruceDirectoSF() {
		
		List<CalendarioFaseSF2022> resultListCalendarioByCruceDirecto = new ArrayList<>();
		if(resultListCalendarioFaseSF == null){
			resultListCalendarioFaseSF = (List<CalendarioFaseSF2022>) obtenerCalendarioByCruceDirectoSF();
			return resultListCalendarioFaseSF;
		}
		return resultListCalendarioByCruceDirecto;
	}
	
	public List<CalendarioFaseFF2022> getResultListCalendarioByCruceDirectoFF() {
		
		List<CalendarioFaseFF2022> resultListCalendarioByCruceDirecto = new ArrayList<>();
		if(resultListCalendarioFaseFF == null){
			resultListCalendarioFaseFF = (List<CalendarioFaseFF2022>) obtenerCalendarioByCruceDirectoFF();
			return resultListCalendarioFaseFF;
		}
		return resultListCalendarioByCruceDirecto;
	}
	
	public List<CalendarioFaseSF2022> obtenerCalendarioByCruceDirectoSF(){
		if(resultListCalendarioFaseSF == null){
			resultListCalendarioFaseSF = (List<CalendarioFaseSF2022>) getResultListCalendarioFaseSF();
		}
		//Ordenar la calendario
		Collections.sort(resultListCalendarioFaseSF, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				CalendarioFaseSF2022 cec1 = (CalendarioFaseSF2022) o1;
				CalendarioFaseSF2022 cec2 = (CalendarioFaseSF2022) o2;
				
				int rpuntos = cec1.getOrden().compareTo(cec2.getOrden());
				return rpuntos;
			}
		});
		
		Collections.reverseOrder();
		return resultListCalendarioFaseSF;
	}
	
	public List<CalendarioFaseFF2022> obtenerCalendarioByCruceDirectoFF(){
		if(resultListCalendarioFaseFF == null){
			resultListCalendarioFaseFF = (List<CalendarioFaseFF2022>) getResultListCalendarioFaseFF();
		}
		//Ordenar la calendario
		Collections.sort(resultListCalendarioFaseFF, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				CalendarioFaseFF2022 cec1 = (CalendarioFaseFF2022) o1;
				CalendarioFaseFF2022 cec2 = (CalendarioFaseFF2022) o2;
				
				int rpuntos = cec1.getOrden().compareTo(cec2.getOrden());
				return rpuntos;
			}
		});
		
		Collections.reverseOrder();
		return resultListCalendarioFaseFF;
	}
	
	public boolean isDataCampeonato() {
		if(getResultListFasesTabShow().size() > 0){
			return true;
		}
		return false;
	}
	
	private List<FasesTabShow2022> resultListFasesTabShow = null;
	
	public List<FasesTabShow2022> getResultListFasesTabShow() {
		if(resultListFasesTabShow == null){
			resultListFasesTabShow = new ArrayList<>();
			if(obtenerCalendarioByCruceDirectoSF() != null && obtenerCalendarioByCruceDirectoSF().size() > 0){
				resultListFasesTabShow.add(FasesTabShow2022.searchFaseTabShow(FasesModelo2022.FASE_SF, null));
			}
			if(obtenerCalendarioByCruceDirectoFF() != null && obtenerCalendarioByCruceDirectoFF().size() > 0){
				resultListFasesTabShow.add(FasesTabShow2022.searchFaseTabShow(FasesModelo2022.FASE_FF, null));
				//A�adir grafico
				resultListFasesTabShow.add(FasesTabShow2022.FASE_GRAPHIC);
			}
		}
		return resultListFasesTabShow;
	}
	
	public Integer getRowsPaginatorClasificacion(){
		return getResultListClasificacion().size();
	}
	
	public Integer getTotalRowsClasificacion(){
		return getResultListClasificacion().size();
	}
	
	   public String getPermisoActualizarResultado(){
	    	if(sessionState != null && sessionState.getUserEquipo() != null && sessionState.getUserEquipo().getId().equals(0)){
    			return "BCMEGEARIJ";
    		}
	    	return "SIN PERMISO";
	    }
		
	    public void doActualizarClasificacion(CampeonatoJuvenilesIndividualClasificacion2022 cec){
	    	boolean error = false;
			if(cec.getActivo() == Activo2022.SI_NUMBER){
				//Validaciones
				
				//Si todo correcto actualizar resultado
				if(!error){
					crearCampeonatoIndividualJuveniles.actualizarClasificacion(cec);
					leerCampeonato = new LeerCampeonatoJuvenilesIndividual2022();
					resultListClasificacion = (List<CampeonatoJuvenilesIndividualClasificacion2022>) leerCampeonato.listResultClasificacion();
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha actualizado el resultado del jugador " + cec.getJugador().getNombre(), null));
				}
			}
			
		}
	    
		public void doConfirmarResultado(CampeonatoJuvenilesIndividualClasificacion2022 cec){
			boolean error = false;
			if(cec.getActivo() == Activo2022.SI_NUMBER){
				//Validaciones
				
				//Si todo correcto confirmar resultado y actualizar clasificacion
				if(!error){
					crearCampeonatoIndividualJuveniles.actualizarClasificacionConfirmar(cec);
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha confirmado el resultado de la jugador " + cec.getJugador().getNombre(), null));
					leerCampeonato = new LeerCampeonatoJuvenilesIndividual2022();
					resultListClasificacion = (List<CampeonatoJuvenilesIndividualClasificacion2022>) leerCampeonato.listResultClasificacion();
					doClasificarToCuartosFinal();
					resultListClasificacion = (List<CampeonatoJuvenilesIndividualClasificacion2022>) leerCampeonato.listResultClasificacion();
					resultListCalendarioFaseCF = (List<CalendarioFaseCF2022>) leerCampeonato.listResultCalendarioFaseCF();
					resultListCalendarioFaseSF = (List<CalendarioFaseSF2022>) leerCampeonato.listResultCalendarioFaseSF();
					resultListCalendarioFaseFF = (List<CalendarioFaseFF2022>) leerCampeonato.listResultCalendarioFaseFF();
				}
			}
			
			
		}		
		
	    public void doActualizarResultadosEnfrentamientoDirectoCF(CalendarioFaseCF2022 calendarioFaseCF){
        	boolean error = false;
        	if(calendarioFaseCF.isModificable() && calendarioFaseCF.isJugadoresEnFase()){
    			
    			Integer juegosJugador1P1 = calendarioFaseCF.getJuegosJugador1P1();
    			Integer juegosJugador1P2 = calendarioFaseCF.getJuegosJugador1P2();
    			Integer juegosJugador1P3 = calendarioFaseCF.getJuegosJugador1P3();
    			Integer juegosJugador2P1 = calendarioFaseCF.getJuegosJugador2P1();
    			Integer juegosJugador2P2 = calendarioFaseCF.getJuegosJugador2P2();
    			Integer juegosJugador2P3 = calendarioFaseCF.getJuegosJugador2P3();
    			
    			//Si todo correcto actualizar resultado
    			if(!error){
    				crearCampeonatoIndividualJuveniles.actualizarResultadosEnfrentamientoDirectoCF(calendarioFaseCF);
    				doClasificarToSemifinal();
    	    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha actualizado el resultado de Cuartos de Final.", null));
    	    		leerCampeonato = new LeerCampeonatoJuvenilesIndividual2022();
    			}
    		}else if(!calendarioFaseCF.isJugadoresEnFase()){
    			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede modificar el resultado, no hay jugadores clasificados para la fase.", null));
    		}
	    		
    	}
		
		public void doActualizarResultadosEnfrentamientoDirectoSF(CalendarioFaseSF2022 calendarioFaseSF){
        	boolean error = false;
        	if(calendarioFaseSF.isModificable() && calendarioFaseSF.isJugadoresEnFase()){
    			
    			Integer juegosJugador1P1 = calendarioFaseSF.getJuegosJugador1P1();
    			Integer juegosJugador1P2 = calendarioFaseSF.getJuegosJugador1P2();
    			Integer juegosJugador1P3 = calendarioFaseSF.getJuegosJugador1P3();
    			Integer juegosJugador2P1 = calendarioFaseSF.getJuegosJugador2P1();
    			Integer juegosJugador2P2 = calendarioFaseSF.getJuegosJugador2P2();
    			Integer juegosJugador2P3 = calendarioFaseSF.getJuegosJugador2P3();
    			
    			//Si todo correcto actualizar resultado
    			if(!error){
    				crearCampeonatoIndividualJuveniles.actualizarResultadosEnfrentamientoDirectoSF(calendarioFaseSF);
    				doClasificarToFinal();
    	    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha actualizado el resultado de Semifinales.", null));
    	    		leerCampeonato = new LeerCampeonatoJuvenilesIndividual2022();
    			}
    		}else if(!calendarioFaseSF.isJugadoresEnFase()){
    			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede modificar el resultado, no hay jugadores clasificados para la fase.", null));
    		}
	    		
    	}
	    
	    public void doActualizarResultadosEnfrentamientoDirectoFF(CalendarioFaseFF2022 calendarioFaseFF){
        	boolean error = false;
        	if(calendarioFaseFF.isModificable() && calendarioFaseFF.isJugadoresEnFase()){
    			
    			Integer juegosJugador1P1 = calendarioFaseFF.getJuegosJugador1P1();
    			Integer juegosJugador1P2 = calendarioFaseFF.getJuegosJugador1P2();
    			Integer juegosJugador1P3 = calendarioFaseFF.getJuegosJugador1P3();
    			Integer juegosJugador2P1 = calendarioFaseFF.getJuegosJugador2P1();
    			Integer juegosJugador2P2 = calendarioFaseFF.getJuegosJugador2P2();
    			Integer juegosJugador2P3 = calendarioFaseFF.getJuegosJugador2P3();
    			
    			//Si todo correcto actualizar resultado
    			if(!error){
    				crearCampeonatoIndividualJuveniles.actualizarResultadosEnfrentamientoDirectoFF(calendarioFaseFF);
    	    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha actualizado el resultado de Final.", null));
    	    		leerCampeonato = new LeerCampeonatoJuvenilesIndividual2022();
    			}
    		}else if(!calendarioFaseFF.isJugadoresEnFase()){
    			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede modificar el resultado, no hay jugadores clasificados para la fase.", null));
    		}

    	}
	    
	    public void doClasificarToCuartosFinal(){
	    	
	    	Jugadores2022 primero = extraerClasificado(1);
	    	Jugadores2022 segundo = extraerClasificado(2);
	    	Jugadores2022 tercero = extraerClasificado(3);
	    	Jugadores2022 cuarto = extraerClasificado(4);
	    	Jugadores2022 quinto = extraerClasificado(5);
	    	Jugadores2022 sexto = extraerClasificado(6);
	    	Jugadores2022 septimo = extraerClasificado(7);
	    	Jugadores2022 octavo = extraerClasificado(8);
	    	
	    	//Recuperar los semifinales
	    	List<CalendarioFaseCF2022> cuartosFinalCalendarioList = getResultListCalendarioFaseCF();
	    	//Buscar en la fase de semifinal la posicion de los clasificados del grupo y asignarlos 
	    	for (CalendarioFaseCF2022 calendarioFaseCF : cuartosFinalCalendarioList) {
				if(calendarioFaseCF.getPosicionProcedenciaJugador1() == 1){
					calendarioFaseCF.setJugador1Id(primero.getId());
					calendarioFaseCF.setJugador1(primero);
					calendarioFaseCF.setJugador2Id(octavo.getId());
					calendarioFaseCF.setJugador2(octavo);
					crearCampeonatoIndividualJuveniles.actualizarJugadorCuartosFinal(calendarioFaseCF, true, false);
				}else if(calendarioFaseCF.getPosicionProcedenciaJugador1() == 2){
					calendarioFaseCF.setJugador1Id(cuarto.getId());
					calendarioFaseCF.setJugador1(cuarto);
					calendarioFaseCF.setJugador2Id(quinto.getId());
					calendarioFaseCF.setJugador2(quinto);
					crearCampeonatoIndividualJuveniles.actualizarJugadorCuartosFinal(calendarioFaseCF, true, false);
				}else if(calendarioFaseCF.getPosicionProcedenciaJugador1() == 3){
					calendarioFaseCF.setJugador1Id(segundo.getId());
					calendarioFaseCF.setJugador1(segundo);
					calendarioFaseCF.setJugador2Id(septimo.getId());
					calendarioFaseCF.setJugador2(septimo);
					crearCampeonatoIndividualJuveniles.actualizarJugadorCuartosFinal(calendarioFaseCF, true, false);
				}else if(calendarioFaseCF.getPosicionProcedenciaJugador1() == 4){
					calendarioFaseCF.setJugador1Id(tercero.getId());
					calendarioFaseCF.setJugador1(tercero);
					calendarioFaseCF.setJugador2Id(sexto.getId());
					calendarioFaseCF.setJugador2(sexto);
					crearCampeonatoIndividualJuveniles.actualizarJugadorCuartosFinal(calendarioFaseCF, true, false);
				}
    		}
	    	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Clasificados a Cuartos de Final jugadores de la clasificacion.", null));
		}

	    public void doClasificarToSemifinal(){
	    	
	    	//Recuperar la clasificacion de las cuartos de final 
	    	List<CalendarioFaseCF2022> cuartosFinalCalendarioList = getResultListCalendarioFaseCF();
	    	//Recuperar los semifinales
	    	List<CalendarioFaseSF2022> semifinalesCalendarioList = getResultListCalendarioFaseSF();
	    	//Buscar en la fase de semifinal la posicion de los clasificados del grupo y asignarlos 
	    	for (CalendarioFaseSF2022 calendarioFaseSF : semifinalesCalendarioList) {
	    		for (CalendarioFaseCF2022 calendarioFaseCF : cuartosFinalCalendarioList) {
					if(calendarioFaseSF.getGrupoProcedenciaJugador1().equalsIgnoreCase(calendarioFaseCF.getIdCruce())){
						if(calendarioFaseCF.isGanaJugador() == 1){
							calendarioFaseSF.setJugador1Id(calendarioFaseCF.getJugador1Id());
							calendarioFaseSF.setJugador1(calendarioFaseCF.getJugador1());
							crearCampeonatoIndividualJuveniles.actualizarJugadorSemiFinal(calendarioFaseSF, true, false);
						}else if(calendarioFaseCF.isGanaJugador() == 2){
							calendarioFaseSF.setJugador1Id(calendarioFaseCF.getJugador2Id());
							calendarioFaseSF.setJugador1(calendarioFaseCF.getJugador2());
							crearCampeonatoIndividualJuveniles.actualizarJugadorSemiFinal(calendarioFaseSF, true, false);
						}
					}else  if(calendarioFaseSF.getGrupoProcedenciaJugador2().equalsIgnoreCase(calendarioFaseCF.getIdCruce())){
						if(calendarioFaseCF.isGanaJugador() == 1){
							calendarioFaseSF.setJugador2Id(calendarioFaseCF.getJugador1Id());
							calendarioFaseSF.setJugador2(calendarioFaseCF.getJugador1());
							crearCampeonatoIndividualJuveniles.actualizarJugadorSemiFinal(calendarioFaseSF, false, true);
						}else if(calendarioFaseCF.isGanaJugador() == 2){
							calendarioFaseSF.setJugador2Id(calendarioFaseCF.getJugador2Id());
							calendarioFaseSF.setJugador2(calendarioFaseCF.getJugador2());
							crearCampeonatoIndividualJuveniles.actualizarJugadorSemiFinal(calendarioFaseSF, false, true);
						}
					}
	    		}
			}
	    	
	    	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Clasificados a Semifinales jugadores de los Cuartos de final.", null));
		}
	    
	    public Jugadores2022 extraerClasificado(Integer posicion){
	    	return getResultListClasificacion().get(posicion-1).getJugador();
	    }
	    	
	    
	    public void doClasificarToFinal(){
	    	
	    	//Recuperar la clasificacion de las semifinales
	    	List<CalendarioFaseSF2022> semifinalCalendarioList = getResultListCalendarioFaseSF();
	    	//Recuperar los semifinales
	    	List<CalendarioFaseFF2022> finalCalendarioList = getResultListCalendarioFaseFF();
	    	//Buscar en la fase de semifinal la posicion de los clasificados del grupo y asignarlos 
	    	for (CalendarioFaseFF2022 calendarioFaseFF : finalCalendarioList) {
	    		for (CalendarioFaseSF2022 calendarioFaseSF : semifinalCalendarioList) {
					if(calendarioFaseFF.getGrupoProcedenciaJugador1().equalsIgnoreCase(calendarioFaseSF.getIdCruce())){
						if(calendarioFaseSF.isGanaJugador() == 1){
							calendarioFaseFF.setJugador1Id(calendarioFaseSF.getJugador1Id());
							calendarioFaseFF.setJugador1(calendarioFaseSF.getJugador1());
							crearCampeonatoIndividualJuveniles.actualizarJugadorFinal(calendarioFaseFF, true, false);
						}else if(calendarioFaseSF.isGanaJugador() == 2){
							calendarioFaseFF.setJugador1Id(calendarioFaseSF.getJugador2Id());
							calendarioFaseFF.setJugador1(calendarioFaseSF.getJugador2());
							crearCampeonatoIndividualJuveniles.actualizarJugadorFinal(calendarioFaseFF, true, false);
						}
					}else  if(calendarioFaseFF.getGrupoProcedenciaJugador2().equalsIgnoreCase(calendarioFaseSF.getIdCruce())){
						if(calendarioFaseSF.isGanaJugador() == 1){
							calendarioFaseFF.setJugador2Id(calendarioFaseSF.getJugador1Id());
							calendarioFaseFF.setJugador2(calendarioFaseSF.getJugador1());
							crearCampeonatoIndividualJuveniles.actualizarJugadorFinal(calendarioFaseFF, false, true);
						}else if(calendarioFaseSF.isGanaJugador() == 2){
							calendarioFaseFF.setJugador2Id(calendarioFaseSF.getJugador2Id());
							calendarioFaseFF.setJugador2(calendarioFaseSF.getJugador2());
							crearCampeonatoIndividualJuveniles.actualizarJugadorFinal(calendarioFaseFF, false, true);
						}
					}
	    		}
			}
	    	
	    	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Clasificados a Final jugadores de las Semifinales.", null));
	    }
	    
		public String getObservacionesCampeonato1() {
			if(this.observacionesCampeonato1 == null){
				this.observacionesCampeonato1 = leerCampeonato.getObservacionesCampeonato1();
				
			}
			return this.observacionesCampeonato1;
		}
		
		public String getObservacionesCampeonato2() {
			if(this.observacionesCampeonato2 == null){
				this.observacionesCampeonato2 = leerCampeonato.getObservacionesCampeonato2();
				
			}
			return this.observacionesCampeonato2;
		}
		
		public String getObservacionesCampeonato3() {
			if(this.observacionesCampeonato3 == null){
				this.observacionesCampeonato3 = leerCampeonato.getObservacionesCampeonato3();
				
			}
			return this.observacionesCampeonato3;
		}
		
		public String getObservacionesCampeonato4() {
			if(this.observacionesCampeonato4 == null){
				this.observacionesCampeonato4 = leerCampeonato.getObservacionesCampeonato4();
				
			}
			return this.observacionesCampeonato4;
		}
		
		public String getObservacionesCampeonato5() {
			if(this.observacionesCampeonato5 == null){
				this.observacionesCampeonato5 = leerCampeonato.getObservacionesCampeonato5();
				
			}
			return this.observacionesCampeonato5;
		}
		
		public String getObservacionesCampeonato6() {
			if(this.observacionesCampeonato6 == null){
				this.observacionesCampeonato6 = leerCampeonato.getObservacionesCampeonato6();
				
			}
			return this.observacionesCampeonato6;
		}
		
		public String getObservacionesCampeonato7() {
			if(this.observacionesCampeonato7 == null){
				this.observacionesCampeonato7 = leerCampeonato.getObservacionesCampeonato7();
				
			}
			return this.observacionesCampeonato7;
		}

}
