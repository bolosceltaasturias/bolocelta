package com.bolocelta.facade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.bolocelta.application.session.SessionState;
import com.bolocelta.bbdd.constants.Activo2022;
import com.bolocelta.bbdd.constants.FasesModelo2022;
import com.bolocelta.bbdd.constants.FasesTabShow2022;
import com.bolocelta.bbdd.createTable.CrearCampeonatoIndividualVeteranos2022;
import com.bolocelta.bbdd.readTables.LeerCampeonatoVeteranosIndividual2022;
import com.bolocelta.entities.CampeonatoVeteranosIndividualClasificacion2022;
import com.bolocelta.entities.Jugadores2022;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseFF2022;
import com.bolocelta.entities.sorteos.individual.CalendarioFaseSF2022;

@Named
@ConversationScoped
@ManagedBean
public class CampeonatoVeteranosIndividualFacade2022 implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private SessionState sessionState;
	
	private LeerCampeonatoVeteranosIndividual2022 leerCampeonato = new LeerCampeonatoVeteranosIndividual2022();
	
	private CrearCampeonatoIndividualVeteranos2022 crearCampeonatoIndividualVeteranos = new CrearCampeonatoIndividualVeteranos2022();
	
	private List<CampeonatoVeteranosIndividualClasificacion2022> resultListClasificacion = null;
	private List<CalendarioFaseSF2022> resultListCalendarioFaseSF = null;
	private List<CalendarioFaseFF2022> resultListCalendarioFaseFF = null;
	
	private String observacionesCampeonato1;
	private String observacionesCampeonato2;
	private String observacionesCampeonato3;
	private String observacionesCampeonato4;
	private String observacionesCampeonato5;
	private String observacionesCampeonato6;
	private String observacionesCampeonato7;

	public List<CampeonatoVeteranosIndividualClasificacion2022> getResultListClasificacion() {
		if(resultListClasificacion == null){
			resultListClasificacion = (List<CampeonatoVeteranosIndividualClasificacion2022>) leerCampeonato.listResultClasificacion();
		}
		return resultListClasificacion;
	}
	
	public List<CalendarioFaseSF2022> getResultListCalendarioFaseSF() {
		if(resultListCalendarioFaseSF == null){
			resultListCalendarioFaseSF = (List<CalendarioFaseSF2022>) leerCampeonato.listResultCalendarioFaseSF();
		}
		return resultListCalendarioFaseSF;
	}
	
	public List<CalendarioFaseFF2022> getResultListCalendarioFaseFF() {
		if(resultListCalendarioFaseFF == null){
			resultListCalendarioFaseFF = (List<CalendarioFaseFF2022>) leerCampeonato.listResultCalendarioFaseFF();
		}
		return resultListCalendarioFaseFF;
	}

	public Object selected() {
		// TODO Auto-generated method stub
		return null;
	}

	public Object read(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	public String insert(Object insert) {
		// TODO Auto-generated method stub
		return null;
	}

	public String update(Object update) {
		// TODO Auto-generated method stub
		return null;
	}

	public String delete(Object delete) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public List<CalendarioFaseSF2022> getResultListCalendarioByCruceDirectoSF() {
		
		List<CalendarioFaseSF2022> resultListCalendarioByCruceDirecto = new ArrayList<>();
		if(resultListCalendarioFaseSF == null){
			resultListCalendarioFaseSF = (List<CalendarioFaseSF2022>) obtenerCalendarioByCruceDirectoSF();
			return resultListCalendarioFaseSF;
		}
		return resultListCalendarioByCruceDirecto;
	}
	
	public List<CalendarioFaseFF2022> getResultListCalendarioByCruceDirectoFF() {
		
		List<CalendarioFaseFF2022> resultListCalendarioByCruceDirecto = new ArrayList<>();
		if(resultListCalendarioFaseFF == null){
			resultListCalendarioFaseFF = (List<CalendarioFaseFF2022>) obtenerCalendarioByCruceDirectoFF();
			return resultListCalendarioFaseFF;
		}
		return resultListCalendarioByCruceDirecto;
	}
	
	public List<CalendarioFaseSF2022> obtenerCalendarioByCruceDirectoSF(){
		if(resultListCalendarioFaseSF == null){
			resultListCalendarioFaseSF = (List<CalendarioFaseSF2022>) getResultListCalendarioFaseSF();
		}
		//Ordenar la calendario
		Collections.sort(resultListCalendarioFaseSF, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				CalendarioFaseSF2022 cec1 = (CalendarioFaseSF2022) o1;
				CalendarioFaseSF2022 cec2 = (CalendarioFaseSF2022) o2;
				
				int rpuntos = cec1.getOrden().compareTo(cec2.getOrden());
				return rpuntos;
			}
		});
		
		Collections.reverseOrder();
		return resultListCalendarioFaseSF;
	}
	
	public List<CalendarioFaseFF2022> obtenerCalendarioByCruceDirectoFF(){
		if(resultListCalendarioFaseFF == null){
			resultListCalendarioFaseFF = (List<CalendarioFaseFF2022>) getResultListCalendarioFaseFF();
		}
		//Ordenar la calendario
		Collections.sort(resultListCalendarioFaseFF, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				CalendarioFaseFF2022 cec1 = (CalendarioFaseFF2022) o1;
				CalendarioFaseFF2022 cec2 = (CalendarioFaseFF2022) o2;
				
				int rpuntos = cec1.getOrden().compareTo(cec2.getOrden());
				return rpuntos;
			}
		});
		
		Collections.reverseOrder();
		return resultListCalendarioFaseFF;
	}
	
	public boolean isDataCampeonato() {
		if(getResultListFasesTabShow().size() > 0){
			return true;
		}
		return false;
	}
	
	private List<FasesTabShow2022> resultListFasesTabShow = null;
	
	public List<FasesTabShow2022> getResultListFasesTabShow() {
		if(resultListFasesTabShow == null){
			resultListFasesTabShow = new ArrayList<>();
			if(obtenerCalendarioByCruceDirectoSF() != null && obtenerCalendarioByCruceDirectoSF().size() > 0){
				resultListFasesTabShow.add(FasesTabShow2022.searchFaseTabShow(FasesModelo2022.FASE_SF, null));
			}
			if(obtenerCalendarioByCruceDirectoFF() != null && obtenerCalendarioByCruceDirectoFF().size() > 0){
				resultListFasesTabShow.add(FasesTabShow2022.searchFaseTabShow(FasesModelo2022.FASE_FF, null));
				//A�adir grafico
				resultListFasesTabShow.add(FasesTabShow2022.FASE_GRAPHIC);
			}
		}
		return resultListFasesTabShow;
	}
	
	public Integer getRowsPaginatorClasificacion(){
		return getResultListClasificacion().size();
	}
	
	public Integer getTotalRowsClasificacion(){
		return getResultListClasificacion().size();
	}
	
	   public String getPermisoActualizarResultado(){
	    	if(sessionState != null && sessionState.getUserEquipo() != null && sessionState.getUserEquipo().getId().equals(0)){
    			return "BCMEGEARIV";
    		}
	    	return "SIN PERMISO";
	    }
		
	    public void doActualizarClasificacion(CampeonatoVeteranosIndividualClasificacion2022 cec){
	    	boolean error = false;
			if(cec.getActivo() == Activo2022.SI_NUMBER){
				//Validaciones
				
				//Si todo correcto actualizar resultado
				if(!error){
					crearCampeonatoIndividualVeteranos.actualizarClasificacion(cec);
					leerCampeonato = new LeerCampeonatoVeteranosIndividual2022();
					resultListClasificacion = (List<CampeonatoVeteranosIndividualClasificacion2022>) leerCampeonato.listResultClasificacion();
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha actualizado el resultado del jugador " + cec.getJugador().getNombre(), null));
				}
			}
			
		}
	    
		public void doConfirmarResultado(CampeonatoVeteranosIndividualClasificacion2022 cec){
			boolean error = false;
			if(cec.getActivo() == Activo2022.SI_NUMBER){
				//Validaciones
				
				//Si todo correcto confirmar resultado y actualizar clasificacion
				if(!error){
					crearCampeonatoIndividualVeteranos.actualizarClasificacionConfirmar(cec);
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha confirmado el resultado de la jugador " + cec.getJugador().getNombre(), null));
					leerCampeonato = new LeerCampeonatoVeteranosIndividual2022();
					resultListClasificacion = (List<CampeonatoVeteranosIndividualClasificacion2022>) leerCampeonato.listResultClasificacion();
					doClasificarToSemifinal();
					resultListClasificacion = (List<CampeonatoVeteranosIndividualClasificacion2022>) leerCampeonato.listResultClasificacion();
					resultListCalendarioFaseSF = (List<CalendarioFaseSF2022>) leerCampeonato.listResultCalendarioFaseSF();
					resultListCalendarioFaseFF = (List<CalendarioFaseFF2022>) leerCampeonato.listResultCalendarioFaseFF();
				}
			}
			
			
		}		
		
	    public void doActualizarResultadosEnfrentamientoDirectoSF(CalendarioFaseSF2022 calendarioFaseSF){
        	boolean error = false;
        	if(calendarioFaseSF.isModificable() && calendarioFaseSF.isJugadoresEnFase()){
    			
    			Integer juegosJugador1P1 = calendarioFaseSF.getJuegosJugador1P1();
    			Integer juegosJugador1P2 = calendarioFaseSF.getJuegosJugador1P2();
    			Integer juegosJugador1P3 = calendarioFaseSF.getJuegosJugador1P3();
    			Integer juegosJugador2P1 = calendarioFaseSF.getJuegosJugador2P1();
    			Integer juegosJugador2P2 = calendarioFaseSF.getJuegosJugador2P2();
    			Integer juegosJugador2P3 = calendarioFaseSF.getJuegosJugador2P3();
    			
    			//Si todo correcto actualizar resultado
    			if(!error){
    				crearCampeonatoIndividualVeteranos.actualizarResultadosEnfrentamientoDirectoSF(calendarioFaseSF);
    				doClasificarToFinal();
    	    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha actualizado el resultado de Semifinales.", null));
    	    		leerCampeonato = new LeerCampeonatoVeteranosIndividual2022();
    			}
    		}else if(!calendarioFaseSF.isJugadoresEnFase()){
    			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede modificar el resultado, no hay jugadores clasificados para la fase.", null));
    		}
	    		
    	}
	    
	    public void doActualizarResultadosEnfrentamientoDirectoFF(CalendarioFaseFF2022 calendarioFaseFF){
        	boolean error = false;
        	if(calendarioFaseFF.isModificable() && calendarioFaseFF.isJugadoresEnFase()){
    			
    			Integer juegosJugador1P1 = calendarioFaseFF.getJuegosJugador1P1();
    			Integer juegosJugador1P2 = calendarioFaseFF.getJuegosJugador1P2();
    			Integer juegosJugador1P3 = calendarioFaseFF.getJuegosJugador1P3();
    			Integer juegosJugador2P1 = calendarioFaseFF.getJuegosJugador2P1();
    			Integer juegosJugador2P2 = calendarioFaseFF.getJuegosJugador2P2();
    			Integer juegosJugador2P3 = calendarioFaseFF.getJuegosJugador2P3();
    			
    			//Si todo correcto actualizar resultado
    			if(!error){
    				crearCampeonatoIndividualVeteranos.actualizarResultadosEnfrentamientoDirectoFF(calendarioFaseFF);
    	    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha actualizado el resultado de Final.", null));
    	    		leerCampeonato = new LeerCampeonatoVeteranosIndividual2022();
    			}
    		}else if(!calendarioFaseFF.isJugadoresEnFase()){
    			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede modificar el resultado, no hay jugadores clasificados para la fase.", null));
    		}

    	}

	    public void doClasificarToSemifinal(){
	    	
	    	Jugadores2022 primero = extraerClasificado(1);
	    	Jugadores2022 segundo = extraerClasificado(2);
	    	Jugadores2022 tercero = extraerClasificado(3);
	    	Jugadores2022 cuarto = extraerClasificado(4);
	    	
	    	//Recuperar los semifinales
	    	List<CalendarioFaseSF2022> semifinalCalendarioList = getResultListCalendarioFaseSF();
	    	//Buscar en la fase de semifinal la posicion de los clasificados del grupo y asignarlos 
	    	for (CalendarioFaseSF2022 calendarioFaseSF : semifinalCalendarioList) {
				if(calendarioFaseSF.getPosicionProcedenciaJugador1() == 1){
					calendarioFaseSF.setJugador1Id(primero.getId());
					calendarioFaseSF.setJugador1(primero);
					calendarioFaseSF.setJugador2Id(cuarto.getId());
					calendarioFaseSF.setJugador2(cuarto);
					crearCampeonatoIndividualVeteranos.actualizarJugadorSemiFinal(calendarioFaseSF, true, false);
				}else if(calendarioFaseSF.getPosicionProcedenciaJugador1() == 2){
					calendarioFaseSF.setJugador1Id(segundo.getId());
					calendarioFaseSF.setJugador1(segundo);
					calendarioFaseSF.setJugador2Id(tercero.getId());
					calendarioFaseSF.setJugador2(tercero);
					crearCampeonatoIndividualVeteranos.actualizarJugadorSemiFinal(calendarioFaseSF, true, false);
				}
    		}
	    	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Clasificados a Semifinales jugadores de la clasificacion.", null));
		}
	    
	    public Jugadores2022 extraerClasificado(Integer posicion){
	    	return getResultListClasificacion().get(posicion-1).getJugador();
	    }
	    	
	    
	    public void doClasificarToFinal(){
	    	
	    	//Recuperar la clasificacion de las semifinales
	    	List<CalendarioFaseSF2022> semifinalCalendarioList = getResultListCalendarioFaseSF();
	    	//Recuperar los semifinales
	    	List<CalendarioFaseFF2022> finalCalendarioList = getResultListCalendarioFaseFF();
	    	//Buscar en la fase de semifinal la posicion de los clasificados del grupo y asignarlos 
	    	for (CalendarioFaseFF2022 calendarioFaseFF : finalCalendarioList) {
	    		for (CalendarioFaseSF2022 calendarioFaseSF : semifinalCalendarioList) {
					if(calendarioFaseFF.getGrupoProcedenciaJugador1().equalsIgnoreCase(calendarioFaseSF.getIdCruce())){
						if(calendarioFaseSF.isGanaJugador() == 1){
							calendarioFaseFF.setJugador1Id(calendarioFaseSF.getJugador1Id());
							calendarioFaseFF.setJugador1(calendarioFaseSF.getJugador1());
							crearCampeonatoIndividualVeteranos.actualizarJugadorFinal(calendarioFaseFF, true, false);
						}else if(calendarioFaseSF.isGanaJugador() == 2){
							calendarioFaseFF.setJugador1Id(calendarioFaseSF.getJugador2Id());
							calendarioFaseFF.setJugador1(calendarioFaseSF.getJugador2());
							crearCampeonatoIndividualVeteranos.actualizarJugadorFinal(calendarioFaseFF, true, false);
						}
					}else  if(calendarioFaseFF.getGrupoProcedenciaJugador2().equalsIgnoreCase(calendarioFaseSF.getIdCruce())){
						if(calendarioFaseSF.isGanaJugador() == 1){
							calendarioFaseFF.setJugador2Id(calendarioFaseSF.getJugador1Id());
							calendarioFaseFF.setJugador2(calendarioFaseSF.getJugador1());
							crearCampeonatoIndividualVeteranos.actualizarJugadorFinal(calendarioFaseFF, false, true);
						}else if(calendarioFaseSF.isGanaJugador() == 2){
							calendarioFaseFF.setJugador2Id(calendarioFaseSF.getJugador2Id());
							calendarioFaseFF.setJugador2(calendarioFaseSF.getJugador2());
							crearCampeonatoIndividualVeteranos.actualizarJugadorFinal(calendarioFaseFF, false, true);
						}
					}
	    		}
			}
	    	
	    	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Clasificados a Final jugadores de las Semifinales.", null));
	    }
	    
		public String getObservacionesCampeonato1() {
			if(this.observacionesCampeonato1 == null){
				this.observacionesCampeonato1 = leerCampeonato.getObservacionesCampeonato1();
				
			}
			return this.observacionesCampeonato1;
		}
		
		public String getObservacionesCampeonato2() {
			if(this.observacionesCampeonato2 == null){
				this.observacionesCampeonato2 = leerCampeonato.getObservacionesCampeonato2();
				
			}
			return this.observacionesCampeonato2;
		}
		
		public String getObservacionesCampeonato3() {
			if(this.observacionesCampeonato3 == null){
				this.observacionesCampeonato3 = leerCampeonato.getObservacionesCampeonato3();
				
			}
			return this.observacionesCampeonato3;
		}
		
		public String getObservacionesCampeonato4() {
			if(this.observacionesCampeonato4 == null){
				this.observacionesCampeonato4 = leerCampeonato.getObservacionesCampeonato4();
				
			}
			return this.observacionesCampeonato4;
		}
		
		public String getObservacionesCampeonato5() {
			if(this.observacionesCampeonato5 == null){
				this.observacionesCampeonato5 = leerCampeonato.getObservacionesCampeonato5();
				
			}
			return this.observacionesCampeonato5;
		}
		
		public String getObservacionesCampeonato6() {
			if(this.observacionesCampeonato6 == null){
				this.observacionesCampeonato6 = leerCampeonato.getObservacionesCampeonato6();
				
			}
			return this.observacionesCampeonato6;
		}
		
		public String getObservacionesCampeonato7() {
			if(this.observacionesCampeonato7 == null){
				this.observacionesCampeonato7 = leerCampeonato.getObservacionesCampeonato7();
				
			}
			return this.observacionesCampeonato7;
		}

}
