package com.bolocelta.facade;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.bean.ManagedBean;
import javax.imageio.ImageIO;
import javax.inject.Named;

import com.bolocelta.bbdd.constants.Ubicaciones2022;
import com.bolocelta.entities.Galerias2022;

@Named
@ConversationScoped
@ManagedBean
public class GaleriaFacade2022 implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private List<Galerias2022> resultList = null;
	
	public GaleriaFacade2022() {
		if(resultList == null){
			searchFile();
		}
	}

	public List<Galerias2022> getResultList() {
		return resultList;
	}

	public Object selected() {
		// TODO Auto-generated method stub
		return null;
	}

	public Object read(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	public String insert(Object insert) {
		// TODO Auto-generated method stub
		return null;
	}

	public String update(Object update) {
		// TODO Auto-generated method stub
		return null;
	}

	public String delete(Object delete) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Integer getMaxRowsPaginator(){
		return 10;
	}
	
	public boolean isEnabledPaginator(){
		return getResultList() != null ? (getResultList().size() > getMaxRowsPaginator()) : false;
	}
	
	public void searchFile() {
		this.resultList = new ArrayList<Galerias2022>();
		File folder = new File(Ubicaciones2022.UBICACION_BBDD_GALERIA);

		File[] files = folder.listFiles();

		Integer id = 1;
		for (File file : files) {
			resultList.add(new Galerias2022(id, Ubicaciones2022.UBICACION_BBDD_GALERIA + file.getName(), ""));
			id++;
		}
	}
	
	public BufferedImage getRecuperarImagen(String pathImage){
		BufferedImage image = null;
		File sourceimage = new File(pathImage);
        try {
			image = ImageIO.read(sourceimage);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return image;
	}
	
	

}
