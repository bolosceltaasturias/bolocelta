package com.bolocelta.application.enumerations;

public class CategoriasEnumeration2022 {
	
	public static final Integer CATEGORIA_PRIMERA = 1;
	public static final Integer CATEGORIA_SEGUNDA = 2;
	public static final Integer CATEGORIA_TERCERA = 3;
	public static final Integer CATEGORIA_FEMENINO = 4;
	public static final Integer CATEGORIA_MIXTO = 8;
	public static final Integer CATEGORIA_VETERANO = 5;
	public static final Integer CATEGORIA_JUVENIL = 6;
	public static final Integer CATEGORIA_CADETE = 10;

}
