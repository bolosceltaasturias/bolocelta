package com.bolocelta.application.enumerations;

public class FasesEnumeration2022 {
	
	public static final String FASE_CLA = "Fase Clasificación";
	public static final String FASE_I = "Fase I";
	public static final String FASE_II = "Fase II";
	public static final String FASE_OF = "Octavos de final";
	public static final String FASE_CF = "Cuartos de final";
	public static final String FASE_SF = "Semifinales";
	public static final String FASE_FC = "Final consolacion";
	public static final String FASE_FF = "Final";

}
