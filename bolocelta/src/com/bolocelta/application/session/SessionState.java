package com.bolocelta.application.session;

import java.io.Serializable;

import javax.inject.Inject;
import javax.inject.Named;

import com.bolocelta.entities.Equipos2022;
import com.bolocelta.facade.EquiposFacade2022;

@Named
@SuppressWarnings("serial")
public class SessionState extends AbstractSessionState implements Serializable{
	
	@Inject
	private EquiposFacade2022 equiposFacade;
	
	public String getUser(){
		return getIdentityInfo() != null ? getIdentityInfo().getUsuario().getUser() : null;
	}
	
	public String getUserName(){
		return getIdentityInfo() != null ? getIdentityInfo().getUsuario().getNombreCompleto() : null;
	}
	
	public boolean hasPermission(String permiso){
		return getIdentityInfo() != null ? getIdentityInfo().hasPermission(permiso) : false;
	}
	
	public Equipos2022 getUserEquipo(){
		if(getIdentityInfo() != null && getIdentityInfo().getUsuario() != null && getIdentityInfo().getUsuario().getEquipo() != null){
			return (Equipos2022) equiposFacade.read(getIdentityInfo().getUsuario().getEquipo());
		}
		return null;
	}


}
