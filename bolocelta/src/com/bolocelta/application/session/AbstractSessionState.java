package com.bolocelta.application.session;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;

import com.bolocelta.application.authenticator.Authenticator;
import com.bolocelta.application.authenticator.IdentityInfo;

@SuppressWarnings("serial")
@SessionScoped
public class AbstractSessionState implements Serializable {

	private IdentityInfo identityInfo;

	private Map<String, Object> globalObjects = new HashMap<String, Object>();

	public AbstractSessionState() {
		super();
	}

	public Object getGlobalObject(String objectName) {
		return globalObjects.get(objectName);
	}

	public void setGlobalObject(String objectName, Object object) {
		globalObjects.put(objectName, object);
	}

	public Map<String, Object> getGlobalObjects() {
		return globalObjects;
	}

	public void setGlobalObjects(Map<String, Object> globalObjects) {
		this.globalObjects = globalObjects;
	}

	public IdentityInfo getIdentityInfo() {
		return identityInfo;
	}

	public void setIdentityInfo(IdentityInfo identityInfo) {
		this.identityInfo = identityInfo;
	}

	public boolean isLogged(){
		if(getIdentityInfo() != null && !getIdentityInfo().isExpired()){
			return true;
		}
		return false;
	}

}
