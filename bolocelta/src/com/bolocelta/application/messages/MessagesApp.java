package com.bolocelta.application.messages;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@SuppressWarnings("serial")
@ApplicationScoped
@Named
public class MessagesApp implements Serializable{
	
	private static List<Message> messages = new ArrayList<>();
	
	//Tipo de errores
	public static final String SEVERITY_INFO = "INFO";
	public static final String SEVERITY_ERROR = "ERROR";
	public static final String SEVERITY_WARN = "WARN";
	
	//Login
	public static final String M_USER_REQUIRED = "Usuario requerido";
	public static final String M_PASS_REQUIRED = "Contraseña requerida";
	public static final String M_USER_INVALID = "Usuario o contraseña incorrectos";
	public static final String M_USER_VALID = "El usuario ha accedido correctamente a la aplicación";
	public static final String M_USER_DISCONNECT = "El usuario se ha desconectado correctamente de la aplicación";
	
	public List<Message> getMessages(){
		return this.messages;
	}
	
	public void clearMessages(){
		this.messages = new ArrayList<>();
	}
	
	public void addMessages(String severity, String message){
		this.messages.add(new Message(severity, message));
	}
	
	public String getInfo(){
		return SEVERITY_INFO;
	}
	
	public String getError(){
		return SEVERITY_ERROR;
	}
	
	public String getWarn(){
		return SEVERITY_WARN;
	}
	
	
}
