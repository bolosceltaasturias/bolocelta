package com.bolocelta.application.authenticator;

import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import com.bolocelta.application.Redirect;
import com.bolocelta.application.session.SessionState;
import com.bolocelta.entities.usuarios.Permisos2022;
import com.bolocelta.entities.usuarios.Usuarios2022;
import com.bolocelta.facade.usuarios.PermisosFacade2022;
import com.bolocelta.facade.usuarios.UsuariosFacade2022;

@ManagedBean
@RequestScoped
public class Authenticator implements IAuthenticator{
	
	@Inject
	private SessionState sessionState;
	@Inject
	private UsuariosFacade2022 usuariosFacade;
	@Inject
	private PermisosFacade2022 permisosFacade;

	@Inject
	private Redirect redirect;
	
	private String user;
	private String pass;
	
	public void login(){
		redirect.getLogin();
	}
	
	public void doLogin() {
		authenticate(user, pass);
		if(sessionState.getIdentityInfo() != null){
			redirect.getHome();
		}
	}
	
	public void doLogout() {
		unAuthenticate(user);
		redirect.logout();
	}
	
	@Override
	public void authenticate(String username, String password) {
		if(username == null || username.isEmpty()){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El usuario es requerido.", null));
		}
		if(password == null || password.isEmpty()){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "La contraseņa es requerida.", null));
		}
		if(username != null && password != null){
			Usuarios2022 usuario = usuariosFacade.read(username, password);
			if(usuario != null){
				Calendar date = new GregorianCalendar();
				date.add(Calendar.HOUR_OF_DAY, 1);
				IdentityInfo identityInfo = new IdentityInfo(user, date.getTime(), usuario);
				sessionState.setIdentityInfo(identityInfo);
				loadPermisos(usuario);
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Usuario ha accedido a su area personal.", null));
			}else{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Usuario o password incorrecta.", null));
			}
		}
	}

	@Override
	public void unAuthenticate(String username) {
		sessionState.setIdentityInfo(null);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Usuario ha salido de su area personal.", null));
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}
	
	public void loadPermisos(Usuarios2022 usuario){
		for(Permisos2022 permisos : permisosFacade.getResultListByRol(usuario.getRolId())){
			sessionState.getIdentityInfo().getPermisosMap().put(permisos.getPermiso(), true);
		}
	}
	
}
