package com.bolocelta.application.utils;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;

/**
 * Clase de utilidad para la descarga de archivos
 *
 */
@SuppressWarnings("serial")
@Named
@ApplicationScoped
public class Downloader2022 implements Serializable {

	/**
	 * Realiza la descarga del fichero tras recibir los bytes del fichero.
	 * 
	 * @param resourceBytes
	 * @return {@link String}
	 */
	public String downloadResource(byte[] resourceBytes) {
		return downloadResource (null, null, resourceBytes);
	}
	
	/**
	 * Realiza la descarga del fichero tras recibir los parametros
	 * 
	 * @param fileName
	 * @param contentType
	 * @param resourceBytes
	 * @return String
	 */
	public String downloadResource(String fileName, String contentType, byte[] resourceBytes) {

		HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
		if (fileName != null) {
			response.setHeader("Content-Disposition", "attachment;filename=\"" + fileName +"\"");
		}
        if (contentType != null) {
			response.setContentType(contentType);
		}
        response.setContentLength(resourceBytes.length);
        try {
			ByteArrayInputStream in = new ByteArrayInputStream(resourceBytes);
            OutputStream out = response.getOutputStream();
 
            // Copy the contents of the file to the output stream
            byte[] buffer = new byte[1024];
            int count;
            while ((count = in.read(buffer)) >= 0) {
                out.write(buffer, 0, count);
            }
            in.close();
            out.flush();
            out.close();
            FacesContext.getCurrentInstance().responseComplete();
        } catch (IOException ex) {
        	System.out.println(ex.getMessage());
        	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error descargando el fichero", null));
        }
		return null;
	}
	
	/**
	 * Realiza la descarga del fichero tras recibir los parametros
	 * 
	 * @param fileName
	 * @param contentType
	 * @param resourceBytes
	 * @return String
	 * @throws FileNotFoundException 
	 */
	public String downloadResource(String fileName, String contentType, File file) throws FileNotFoundException {
		return downloadResource(fileName, contentType, convertFileToByteArray(file));
	}
	
	/**
	 * Realiza la conversión del File a bytearray
	 * 
	 * @param file
	 * @return
	 * @throws FileNotFoundException
	 */
	public byte[] convertFileToByteArray(File file) throws FileNotFoundException{
		FileInputStream fis = null;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] buf = new byte[1024];
        try {
        	fis = new FileInputStream(file);
            for (int readNum; (readNum = fis.read(buf)) != -1;) {
                bos.write(buf, 0, readNum); //no doubt here is 0
            }
            fis.close();
        } catch (IOException ex) {
        	System.out.println(ex.getMessage());
        	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error descargando el fichero", null));
        } finally {
        	try {
		        if( fis!=null ) {
		        	fis.close();
		        }
		    } catch(IOException e) {
	        	System.out.println(e.getMessage());
	        	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error descargando el fichero", null));
		    }
        }
        return bos.toByteArray();
	}
	
	/**
	 * Realiza la descarga del fichero al path indicado
	 * 
	 * @param fichero
	 * @param name
	 * @param ext
	 * @param path
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public void downloadFileToPath(byte[] fichero, String name, String ext, String path)
			throws IOException, FileNotFoundException {
		File source = new File(path);
		source = File.createTempFile(name, ext, source);
		FileOutputStream fos = new FileOutputStream(source);
		try {
			fos.write(fichero);
		} catch (IOException e) {
			fos.close();
        	System.out.println(e.getMessage());
        	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error almacenando el fichero", null));
		} finally {
			fos.close();
		}
	}


}
