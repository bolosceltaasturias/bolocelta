package com.bolocelta.application;

import java.io.IOException;
import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
@Named
@SessionScoped
public class Redirect implements Serializable {

	private MenuView menuView = new MenuView();

	private String capturedView;

	public Redirect() {
	}

	public void getHome() {
		redirect(menuView.PAG_HOME);
	}

	public void getIndex() {
		redirect(menuView.PAG_HOME);
	}

	public void getLogin() {
		redirect(menuView.PAG_LOGIN);
	}
	
	public void getHistoria() {
		redirect(menuView.PAG_OM_HISTORIA);
	}
	
	public void getContacto() {
		redirect(menuView.PAG_OM_CONTACTO);
	}
	
	public void getNoticias() {
		redirect(menuView.PAG_OM_NOTICIAS);
	}
	
	public void getPatrocinadores() {
		redirect(menuView.PAG_OM_PATROCINADORES);
	}
	
	public void getEquipoDetalle() {
		redirect(menuView.PAG_OM_FICHA_EQUIPO);
	}
	
	public void getInscripcionIndividual() {
		redirect(menuView.PAG_OM_ADD_JUGADOR_CAMPEONATO_INDIVIDUAL);
	}
	
	public void getInscripcionParejas() {
		redirect(menuView.PAG_OM_ADD_PAREJA_CAMPEONATO_INDIVIDUAL);
	}
	
	public void getInscripcionParejasMixto() {
		redirect(menuView.PAG_OM_ADD_PAREJA_CAMPEONATO_MIXTO);
	}
	
	public void getInscripcionParejasFemenino() {
		redirect(menuView.PAG_OM_ADD_PAREJA_CAMPEONATO_FEMENINO);
	}
	
	public void getAddInscripcionParejas() {
		redirect(menuView.PAG_OM_EDIT_FICHA_PAREJA);
	}
	
	public void getAddInscripcionParejasMixto() {
		redirect(menuView.PAG_OM_EDIT_FICHA_PAREJA_MIXTO);
	}
	
	public void getAddInscripcionParejasFemenino() {
		redirect(menuView.PAG_OM_EDIT_FICHA_PAREJA_FEMENINO);
	}
	
	public void getAddCategoria() {
		redirect(menuView.PAG_OM_ADD_CATEGORIA_PAREJA);
	}
	
	public void getAddJugador1Equipo() {
		redirect(menuView.PAG_OM_ADD_JUGADOR_1_PAREJA_EQUIPO);
	}
	
	public void getAddJugador2Equipo() {
		redirect(menuView.PAG_OM_ADD_JUGADOR_2_PAREJA_EQUIPO);
	}
	
	public void getAddJugador2OtroEquipo() {
		redirect(menuView.PAG_OM_ADD_JUGADOR_PAREJA_OTRO_EQUIPO);
	}
	
	public void getAddJugador1EquipoMixto() {
		redirect(menuView.PAG_OM_ADD_JUGADOR_1_PAREJA_EQUIPO_MIXTO);
	}
	
	public void getAddJugador2EquipoMixto() {
		redirect(menuView.PAG_OM_ADD_JUGADOR_2_PAREJA_EQUIPO_MIXTO);
	}
	
	public void getAddJugador2OtroEquipoMixto() {
		redirect(menuView.PAG_OM_ADD_JUGADOR_PAREJA_OTRO_EQUIPO_MIXTO);
	}
	
	public void getAddJugador1EquipoFemenino() {
		redirect(menuView.PAG_OM_ADD_JUGADOR_1_PAREJA_EQUIPO_FEMENINO);
	}
	
	public void getAddJugador2EquipoFemenino() {
		redirect(menuView.PAG_OM_ADD_JUGADOR_2_PAREJA_EQUIPO_FEMENINO);
	}
	
	public void getAddJugador2OtroEquipoFemenino() {
		redirect(menuView.PAG_OM_ADD_JUGADOR_PAREJA_OTRO_EQUIPO_FEMENINO);
	}
	
	public void getMiEquipo() {
		redirect(menuView.PAG_OM_EDIT_FICHA_EQUIPO);
	}
	
	public void getJugadorDetalle() {
		redirect(menuView.PAG_OM_EDIT_FICHA_JUGADOR);
	}
	
	public void getBoleras() {
		redirect(menuView.PAG_OM_BOLERAS_LIST);
	}
	
	public void getCategorias() {
		redirect(menuView.PAG_OM_CATEGORIAS_LIST);
	}
	
	public void getEquipos() {
		redirect(menuView.PAG_OM_EQUIPOS_LIST);
	}
	
	public void getModalidad() {
		redirect(menuView.PAG_OM_MODALIDADES_LIST);
	}
	
	public void getCalendario() {
		redirect(menuView.PAG_OM_CALENDARIO_LIST);
	}
	
	public void getCalendarioAsturias() {
		redirect(menuView.PAG_OM_CALENDARIO_ASTURIAS_LIST);
	}
	
	public void getCalendarioEspana() {
		redirect(menuView.PAG_OM_CALENDARIO_ESPANA_LIST);
	}
	
	public void getCampeonatoEquiposPrimera() {
		redirect(menuView.PAG_OM_CAMPEONATO_EQUIPOS_PRIMERA_LIST);
	}
	
	public void getCampeonatoEquiposSegunda() {
		redirect(menuView.PAG_OM_CAMPEONATO_EQUIPOS_SEGUNDA_LIST);
	}
	
	public void getCampeonatoEquiposTercera() {
		redirect(menuView.PAG_OM_CAMPEONATO_EQUIPOS_TERCERA_LIST);
	}
	
	public void getCampeonatoFemenino() {
		redirect(menuView.PAG_HOME);
	}
	
	public void getCampeonatoIndividualPrimera() {
		redirect(menuView.PAG_HOME);
	}
	
	public void getCampeonatoIndividualSegunda() {
		redirect(menuView.PAG_HOME);
	}
	
	public void getCampeonatoIndividualTercera() {
		redirect(menuView.PAG_HOME);
	}
	
	public void getCampeonatoIndividualFemenino() {
		redirect(menuView.PAG_HOME);
	}
	
	public void getCampeonatoParejasPrimera() {
		redirect(menuView.PAG_HOME);
	}
	
	public void getCampeonatoParejasSegunda() {
		redirect(menuView.PAG_HOME);
	}
	
	public void getCampeonatoParejasTercera() {
		redirect(menuView.PAG_HOME);
	}
	
	public void getCampeonatoParejasFemenino() {
		redirect(menuView.PAG_HOME);
	}
	
	public void getCampeonatoParejasMixto() {
		redirect(menuView.PAG_HOME);
	}
	
	public void doAccessNotificaciones(){
		redirect(menuView.PAG_HOME);
	}
	
	public void doAccessDocumentos(){
		redirect(menuView.PAG_HOME);
	}
	
	public void doAccessGaleria(){
		redirect(menuView.PAG_HOME);
	}
	
	public void doAccessTV(){
		redirect(menuView.PAG_HOME);
	}
	
	public void doAccessCampeonatoMasculinoIndividualPrimera(){
		redirect(menuView.PAG_OM_ADD_CAMPEONATO_MASCULINO_INDIVIDUAL_PRIMERA);
	}
	
	public void doAccessCampeonatoMasculinoIndividualSegunda(){
		redirect(menuView.PAG_OM_ADD_CAMPEONATO_MASCULINO_INDIVIDUAL_SEGUNDA);
	}
	
	public void doAccessCampeonatoMasculinoIndividualTercera(){
		redirect(menuView.PAG_OM_ADD_CAMPEONATO_MASCULINO_INDIVIDUAL_TERCERA);
	}
	
	public void doAccessCampeonatoMasculinoIndividualFemenino(){
		redirect(menuView.PAG_OM_ADD_CAMPEONATO_FEMENINO_INDIVIDUAL);
	}
	
	public void doAccessCampeonatoMasculinoIndividualVeteranos(){
		redirect(menuView.PAG_OM_ADD_CAMPEONATO_VETERANOS_INDIVIDUAL);
	}
	
	public void doAccessCampeonatoMasculinoIndividualJuveniles(){
		redirect(menuView.PAG_OM_ADD_CAMPEONATO_JUVENILES_INDIVIDUAL);
	}
	
	public void doAccessCampeonatoMasculinoIndividualCadetes(){
		redirect(menuView.PAG_OM_ADD_CAMPEONATO_CADETES_INDIVIDUAL);
	}
	
	public void doAccessCampeonatoMasculinoParejasPrimera(){
		redirect(menuView.PAG_OM_ADD_CAMPEONATO_MASCULINO_PAREJAS_PRIMERA);
	}
	
	public void doAccessCampeonatoMasculinoParejasSegunda(){
		redirect(menuView.PAG_OM_ADD_CAMPEONATO_MASCULINO_PAREJAS_SEGUNDA);
	}
	
	public void doAccessCampeonatoMasculinoParejasTercera(){
		redirect(menuView.PAG_OM_ADD_CAMPEONATO_MASCULINO_PAREJAS_TERCERA);
	}
	
	public void doAccessCampeonatoMasculinoParejasFemenino(){
		redirect(menuView.PAG_OM_ADD_CAMPEONATO_FEMENINO_PAREJAS);
	}
	
	public void doAccessCampeonatoParejasMixto(){
		redirect(menuView.PAG_OM_ADD_CAMPEONATO_MIXTO_PAREJAS);
	}
	
	public void doAccessCampeonatoLiga(){
		redirect(menuView.PAG_OM_ADD_CAMPEONATO_LIGA);
	}


	public void redirect(String view) {
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect(view);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void redirectExternal(String url) {
		HttpServletResponse response = 
		          (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
		try {
			response.sendRedirect(url);
		} catch (IOException e) {
			// TODO Auto-generated catch block
		}
	}

	public void captureCurrentView() {
		setCapturedView(FacesContext.getCurrentInstance().getViewRoot().getViewId());
	}

	public String getCapturedView() {
		return capturedView;
	}

	public void setCapturedView(String capturedView) {
		this.capturedView = capturedView;
	}

	public void redirectToCapturedView() {
		redirect(getCapturedView());
	}
	
	public void logout() {
//		authenticator.getIdentityProvider().logout(getUsername(), getDefaultSecurityDomain(), null);
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		redirect(menuView.PAG_HOME);
		//return getHome() + "?faces-redirect=true&nocid=true";
	}
	
	
}
